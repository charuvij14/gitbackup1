<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 * 
 * The CoalaWeb traffic module was inspired by VCNT Thanks to Viktor Vogel {@link http://joomla-extensions.kubik-rubik.de/}
 * 
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
jimport('joomla.application.component.helper');
class mod_coalawebtrafficHelper {

    // Function - Counting Data
    function read(&$params) {
        $comParams = JComponentHelper::getParams('com_coalawebtraffic');
        $db = JFactory::getDbo();

        //Work out the time off set
        $config = JFactory::getConfig();
        if (version_compare(JVERSION, '3.0', '>')) {
            $siteOffset = $config->get('offset');
            date_default_timezone_set($siteOffset);
        } else {
            $siteOffset = $config->getValue('config.offset');
            date_default_timezone_set($siteOffset);
        }

        $day = date('d');
        $month = date('m');
        $year = date('Y');
        
        $daystart = mktime(0, 0, 0, $month, $day, $year);
        $monthstart = mktime(0, 0, 0, $month, 1, $year);
        $yesterdaystart = $daystart - (24 * 60 * 60);
        
        $weekDayStart = $comParams->get('week_start', 'mon');
        
        switch ($weekDayStart) {
            case 'sat':
                $weekstart = $daystart - ((date('N') + 1) * 24 * 60 * 60);
                break;
            case 'sun':
                $weekstart = $daystart - (date('N') * 24 * 60 * 60);
                break;
            case 'mon':
                $weekstart = $daystart - ((date('N') - 1) * 24 * 60 * 60);
                break;
            default:
                $weekstart = $daystart - ((date('N') - 1) * 24 * 60 * 60);
                break;
        }

        $preset = $comParams->get('preset', 0);

        //Count ongoing total
        $query = $db->getQuery(true);
        $query->select('TCOUNT');
        $query->from($db->quoteName('#__cwtraffic_total'));
        $db->setQuery($query);
        $tcount = $db->loadResult();

        // Create base to count from
        $query = $db->getQuery(true);
        $query->select('count(*)');
        $query->from($db->quoteName('#__cwtraffic'));
        $db->setQuery($query);
        $all_visitors = $db->loadResult();
        $all_visitors += $preset;
        $all_visitors += $tcount;

        //Todays Visitors
        $query = $db->getQuery(true);
        $query->select('count(*)');
        $query->from($db->quoteName('#__cwtraffic'));
        $query->where('tm > ' . $db->quote($daystart));
        $db->setQuery($query);
        $today_visitors = $db->loadResult();

        //Yesterdays Visitors
        $query = $db->getQuery(true);
        $query->select('count(*)');
        $query->from($db->quoteName('#__cwtraffic'));
        $query->where('tm > ' . $db->quote($yesterdaystart));
        $query->where('tm < ' . $db->quote($daystart));
        $db->setQuery($query);
        $yesterday_visitors = $db->loadResult();

        //This Weeks Visitors
        $query = $db->getQuery(true);
        $query->select('count(*)');
        $query->from($db->quoteName('#__cwtraffic'));
        $query->where('tm >= ' . $db->quote($weekstart));
        $db->setQuery($query);
        $week_visitors = $db->loadResult();

        //Months Visitors
        $query = $db->getQuery(true);
        $query->select('count(*)');
        $query->from($db->quoteName('#__cwtraffic'));
        $query->where('tm >= ' . $db->quote($monthstart));
        $db->setQuery($query);
        $month_visitors = $db->loadResult();

        $ret = array($all_visitors, $today_visitors, $yesterday_visitors, $week_visitors, $month_visitors);
        return ($ret);
    }


    // Who is online
    static function getRealCount() {
        $db = JFactory::getDbo();
        //Check the Who is Online table for IPs
        $query = $db->getQuery(true);
        $query->select('count(*)');
        $query->from($db->quoteName('#__cwtraffic_whoisonline'));
        $db->setQuery($query);
        $result = $db->loadResult();

        return $result;
    }
    
    static function getCountries() {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        $query->select('a.country_code, a.country_name, COUNT(1) as num');
        $query->from($db->quoteName('#__cwtraffic_whoisonline', 'a'));
        $query->where($db->quoteName('a.country_code') . ' IS NOT NULL ');
        $query->group('a.country_code, a.country_name');
        $query->order($db->quoteName('num') . ' DESC ');

        $db->setQuery($query, 0, 5);

        $result = $db->loadObjectList();
        
        return $result;
    }

    //Lets create the digital counter
    public static function getTotalImage($params, $totalNumber) {
        $digitTheme = $params->get('digit_theme');
        $digitNumber = $params->get('digit_number');
        
        $arrNumber = mod_coalawebtrafficHelper::getArrayNumber($digitNumber, $totalNumber);

        $html = '';
        if ($arrNumber) {
            foreach ($arrNumber as $number) {
                $html .= mod_coalawebtrafficHelper::getDigitImage($number, $digitTheme);
            }
        }

        return $html;
    }

    static function getArrayNumber($length, $number) {
        $strlen = strlen($number);

        $arr = array();
        $diff = $length - $strlen;

        while ($diff > 0) {
            array_push($arr, 0);
            $diff--;
        }

        $arrNumber = str_split($number);
        $arr = array_merge($arr, $arrNumber);

        return $arr;
    }

    static function getDigitImage($number, $type) {
        $html = '';
        $html .= '<img class="" src="' . JURI :: base(true) . '/media/coalawebtraffic/modules/traffic/digit-themes/' . $type . '/' . $number . '.png" alt="'. $number . '.png"/>';
        return $html;
    }

}

<?php
/* ======================================================
# Display Date and Time - Joomla! Module v2.0.7 (FREE version)
# -------------------------------------------------------
# For Joomla! 3.0
# Author: Yiannis Christodoulou (yiannis@web357.eu)
# Copyright (©) 2009-2015 Web357. All rights reserved.
# License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
# Website: https://www.web357.eu/
# Demo: http://demo.web357.eu/?item=datetime
# Support: support@web357.eu
# Last modified: 09 Dec 2015, 19:28:46
========================================================= */

defined('_JEXEC') or die;

// get helper
require_once(dirname(__FILE__).'/helper.php');
require(JModuleHelper::getLayoutPath('mod_datetime'));
<?php
/* ======================================================
# Display Date and Time - Joomla! Module v2.0.7 (FREE version)
# -------------------------------------------------------
# For Joomla! 3.0
# Author: Yiannis Christodoulou (yiannis@web357.eu)
# Copyright (©) 2009-2015 Web357. All rights reserved.
# License: GNU/GPLv3, http://www.gnu.org/licenses/gpl-3.0.html
# Website: https://www.web357.eu/
# Demo: http://demo.web357.eu/?item=datetime
# Support: support@web357.eu
# Last modified: 09 Dec 2015, 19:28:46
========================================================= */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Get Joomla Language File Name
$lg = JFactory::getLanguage();
$language = $lg->get('tag');

// Get Absolute path of Joomla
$absolute_path = getcwd();
$months = array( JText::_( 'MOD_DATETIME_JANUARY' ), JText::_( 'MOD_DATETIME_FEBRUARY' ), JText::_( 'MOD_DATETIME_MARCH' ), JText::_( 'MOD_DATETIME_APRIL' ), JText::_( 'MOD_DATETIME_MAY' ), JText::_( 'MOD_DATETIME_JUNE' ), JText::_( 'MOD_DATETIME_JULY' ), JText::_( 'MOD_DATETIME_AUGUST' ), JText::_( 'MOD_DATETIME_SEPTEMBER' ), JText::_( 'MOD_DATETIME_OCTOBER' ), JText::_( 'MOD_DATETIME_NOVEMBER' ), JText::_( 'MOD_DATETIME_DECEMBER' )); 
$days = array( JText::_( 'MOD_DATETIME_SUNDAY' ), JText::_( 'MOD_DATETIME_MONDAY' ), JText::_( 'MOD_DATETIME_TUESDAY' ), JText::_( 'MOD_DATETIME_WEDNESDAY' ), JText::_( 'MOD_DATETIME_THURSDAY' ), JText::_( 'MOD_DATETIME_FRIDAY' ), JText::_( 'MOD_DATETIME_SATURDAY' )); 
$server_now = time();

// Offset
$offset_param = $params->get('offset', '');
$config = JFactory::getConfig();
if (!empty($offset_param)):
	$offset = $offset_param;
elseif(version_compare(JVERSION,'1.6.0') < 0):
	$offset = $config->getValue('config.offset'); //j16+
else:
	$offset = $config->get('offset'); //j30+
endif;
$timezone = new DateTime('', new DateTimeZone($offset)); // example: -18000, -14400 (http://www.php.net/manual/en/datetime.getoffset.php)
$get_offset = $timezone->getOffset();
$tzoffset = $get_offset / 3600; // example: -4, -4
$local_now = strtotime($tzoffset.' hours'); //$server_now;
$day_of_the_week = gmdate("w", $local_now); // day (Monday)
$day = gmdate("j", $local_now);  // day (7)
$month_full = gmdate("F", $local_now); // month (March)
$full_month = JText::_(strtoupper($month_full)); // month (Mar) from .ini
$short_month = JText::_(strtoupper($month_full).'_SHORT'); // month (Mar) from .ini
$month = gmdate("n", $local_now); // month (3)
$year = gmdate("Y", $local_now); // year (2014)
$d = gmdate("d", $local_now); // day (07)
$m = gmdate("m", $local_now); // month (03)
$Y = gmdate("Y", $local_now); // year (2015)
$h = gmdate("H", $local_now); // hour (19)
$i = gmdate("i", $local_now); // minute (38)
$s = gmdate("s", $local_now); // second (06)
$a = gmdate("A", $local_now); // PM/AM

// Parameters
$front_text = (string)$params->get('front_text', '');
$end_text = (string)$params->get('end_text', '');
$between_text = (string)$params->get('between_text', '');
$dateformat = $params->get('dateformat', '');
$dateformat_separator = $params->get('dateformat_separator', '-');
$timeformat = $params->get('timeformat', '4');
$hour_system = (int)$params->get('hour_system', '24');
$display_pm_am = (int)$params->get('display_pm_am', '0');
$color = (string)$params->get('color', '');

// Build Date
$default_date = $Y."-".$m."-".$d;
$print_date = (!empty($dateformat)) ? date($dateformat, strtotime($default_date)) : '';

// BEGIN: DIV
echo '<div class="mod_datetime"'.(!empty($color) ? ' style="color: '.$color.';"' : '').'>';

// Front text
echo (!empty($front_text)) ? $front_text.' ' : '';

// Date format
switch ($dateformat):

	// Full Date Format
	case "dddd, d mmmm yyyyy":
		$date = $days[$day_of_the_week].", ".$day." ".$months[$month - 1]." ".$year;
	break;
	case "dddd, dd mmmm yyyyy":
		$date = $days[$day_of_the_week].", ".$d." ".$months[$month - 1]." ".$year;
	break;
	case "dddd, d mmm yyyyy":
		$date = $days[$day_of_the_week].", ".$day." ".$short_month." ".$year;
	break;
	case "dddd, dd mmm yyyyy":
		$date = $days[$day_of_the_week].", ".$d." ".$short_month." ".$year;
	break;
	case "dddd, dd-mm-yyyyy":
		$date = $days[$day_of_the_week].", ".$d."-".$m."-".$year;
	break;
	
	// Month Day, Year
	case "mmmm dd, yyyy":
		$date = $full_month." ".$d.", ".$year;
	break;
	case "mmmm d, yyyy":
		$date = $full_month." ".$day.", ".$year;
	break;
	case "mmm dd, yyyy":
		$date = $short_month." ".$d.", ".$year;
	break;
	case "mmm d, yyyy":
		$date = $short_month." ".$day.", ".$year;
	break;

	// Day Month, Year
	case "dd mmmm, yyyy":
		$date = $d." ".$full_month.", ".$year;
	break;
	case "d mmmm, yyyy":
		$date = $day." ".$full_month.", ".$year;
	break;
	case "dd mmm, yyyy":
		$date = $d." ".$short_month.", ".$year;
	break;
	case "d mmm, yyyy":
		$date = $day." ".$short_month.", ".$year;
	break;
	
	// Default
	default:
		$date = $print_date;
endswitch;

// echo date
$dateformat_separator = (!empty($dateformat_separator) && $dateformat_separator != '-') ? $dateformat_separator : '-';
$print_final_date = str_replace('-', $dateformat_separator, $date);
echo $print_final_date;

// Between text
echo (!empty($between_text)) ? $between_text.' ' : '';

// Time format
switch ($timeformat) :
	case 1: // 19:32 (static)
		echo " ".$h.":".$i;
		echo ($display_pm_am) ? " ".JText::_('MOD_DATETIME_'.$a) : '';
	break;
	case 2: // 19:32 (ajax/flashing)
	?>
    <span id="mod_datetime_<?php echo $module->id; ?>"></span>
    <script type="text/javascript">
		<!--
		zone=0
		isitlocal=true;
		ampm='';
		
		function mod_datetime_<?php echo $module->id; ?>(){
		now=new Date();
		ofst=now.getTimezoneOffset()/60;
		secs=now.getUTCSeconds();
		sec=-1.57+Math.PI*secs/30;
		mins=now.getUTCMinutes();
		min=-1.57+Math.PI*mins/30;
		hr=(isitlocal)?now.getUTCHours() + <?php echo $tzoffset; ?>:(now.getUTCHours() + parseInt(ofst)) + parseInt(zone);
		hrs=-1.575+Math.PI*hr/6+Math.PI*parseInt(now.getMinutes())/360;
		if (hr < 0) hr+=<?php echo $hour_system?>;
		if (hr > 23) hr-=<?php echo $hour_system?>;
		//if (hr > <?php echo $hour_system-1?>) hr-=<?php echo $hour_system?>;
		<?php if ($display_pm_am) : ?>
		ampm = (hr > 11) ? "<?php echo JText::_('MOD_DATETIME_PM')?>" : "<?php echo JText::_('MOD_DATETIME_AM'); ?>";
		<?php else: ?>
		ampm = (hr > 11)?"":"";
		<?php endif; ?>
		statusampm = ampm.toLowerCase();

		hr2 = hr;
		if (hr2 == 0) hr2=<?php echo $hour_system?>;//24 or 12
		(hr2 < 13)?hr2:hr2 %= <?php echo $hour_system?>;// 24 or 12
		if (hr2<10) hr2="0"+hr2
		
		var finaltime=hr2+':'+((mins < 10)?"0"+mins:mins)+' '+statusampm;
		
		document.getElementById("mod_datetime_<?php echo $module->id; ?>").innerHTML=finaltime
		setTimeout("mod_datetime_<?php echo $module->id; ?>()",1000)
		}
		mod_datetime_<?php echo $module->id; ?>()
		//-->    
    </script>
	<?php
	break;	
	case 3: // 19:32:22 (ajax/flashing)
	?>
    <span id="mod_datetime_<?php echo $module->id; ?>"></span>
    <script type="text/javascript">
		<!--
		zone=0
		isitlocal=true;
		ampm='';
		
		function mod_datetime_<?php echo $module->id; ?>()
		{
			now=new Date();//new Date((+new Date()) + (3 * 60 * 60000))//new Date();
						
			ofst=now.getTimezoneOffset()/60;
			secs=now.getUTCSeconds();
			sec=-1.57+Math.PI*secs/30;
			mins=now.getUTCMinutes();
			min=-1.57+Math.PI*mins/30;
			hr=(isitlocal)?now.getUTCHours() + <?php echo $tzoffset; ?>:(now.getUTCHours() + parseInt(ofst)) + parseInt(zone);
			hrs=-1.575+Math.PI*hr/6+Math.PI*parseInt(now.getMinutes())/360;
			if (hr < 0) hr+=<?php echo $hour_system; ?>;
			if (hr > 23) hr-=<?php echo $hour_system; ?>;
			<?php if ($display_pm_am) : ?>
			ampm = (hr > 11) ? "<?php echo JText::_('MOD_DATETIME_PM')?>" : "<?php echo JText::_('MOD_DATETIME_AM'); ?>";
			<?php else: ?>
			ampm = (hr > 11)?"":"";
			<?php endif; ?>
			statusampm = ampm.toLowerCase();
			
			hr2 = hr;
			if (hr2 == 0) hr2=<?php echo $hour_system?>;//24 or 12
			(hr2 < 13)?hr2:hr2 %= <?php echo $hour_system?>;// 24 or 12
			if (hr2<10) hr2="0"+hr2
			
			var finaltime=hr2+':'+((mins < 10)?"0"+mins:mins)+':'+((secs < 10)?"0"+secs:secs)+' '+statusampm;
			
			document.getElementById("mod_datetime_<?php echo $module->id; ?>").innerHTML=finaltime
			setTimeout("mod_datetime_<?php echo $module->id; ?>()",1000)
		}
		mod_datetime_<?php echo $module->id; ?>()
		//-->    
    </script>
	<?php
	break;
	case 4: // none
		echo "";
	break;
endswitch;

// End text
echo (!empty($end_text)) ? $end_text.' ' : '';

// END: DIV
echo '</div>';

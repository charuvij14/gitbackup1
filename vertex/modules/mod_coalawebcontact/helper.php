<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Contact Module
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Contact is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

class modCoalawebContactHelper extends JObject {

    function __construct($uniqueId, $params, $comParams) {
        $this->_uniqueid = $uniqueId;
        $myparams = $this->_getParams($params, $comParams);
        $this->_params = $myparams;
        $this->_addAssets();

        return; 
   }

    private function _getParams($params, $comParams) {
        $app = JFactory::getApplication();

        //Advanced
        $arr['uniqueid'] = $this->_uniqueid;
        $arr['moduleclass_sfx'] = htmlspecialchars($params->get('moduleclass_sfx'));
        $arr['cloakOn'] = $comParams->get('cloak_on', '1');

        //Labels
        $arr['nameLbl'] = $comParams->get('name_lbl', JTEXT::_('COM_CWCONTACT_LABEL_NAME'));
        $arr['departmentLbl'] = $comParams->get('department_lbl', JTEXT::_('COM_CWCONTACT_LABEL_DEPARTMENT'));
        $arr['departmentHint'] = $comParams->get('department_hint', JTEXT::_('COM_CWCONTACT_DEP_HINT'));
        $arr['nameHint'] = $comParams->get('name_hint', JTEXT::_('COM_CWCONTACT_NAME_HINT'));
        $arr['emailLbl'] = $comParams->get('email_lbl', JTEXT::_('COM_CWCONTACT_LABEL_EMAIL'));
        $arr['emailHint'] = $comParams->get('email_hint', JTEXT::_('COM_CWCONTACT_EMAIL_HINT'));
        $arr['messageLbl'] = $comParams->get("message_lbl", JTEXT::_('COM_CWCONTACT_LABEL_MESSAGE'));
        $arr['messageHint'] = $comParams->get('message_hint', JTEXT::_('COM_CWCONTACT_MESSAGE_HINT'));
        $arr['subjectLbl'] = $comParams->get('subject_lbl', JTEXT::_('COM_CWCONTACT_LABEL_SUBJECT'));
        $arr['subjectHint'] = $comParams->get('subject_hint', JTEXT::_('COM_CWCONTACT_SUBJECT_HINT'));
        $arr['copymeLbl'] = $comParams->get('copyme_lbl', JTEXT::_('COM_CWCONTACT_LABEL_COPYME'));
        $arr['tosLbl'] = $comParams->get('tos_lbl', JTEXT::_('COM_CWCONTACT_LABEL_TOS'));
        $arr['tosLinkLbl'] = $comParams->get('tos_link_lbl', JTEXT::_('COM_CWCONTACT_LABEL_TOS_LINK'));
        $arr['sFromTitleLbl'] = $comParams->get('sfrom_title', JTEXT::_('COM_CWCONTACT_MAIL_TITLE_FROM'));
        $arr['sFromWebLbl'] = $comParams->get('sfrom_web_lbl', JTEXT::_('COM_CWCONTACT_LABEL_SFROMWEB'));
        $arr['sFromUrlLbl'] = $comParams->get('sfrom_url_lbl', JTEXT::_('COM_CWCONTACT_LABEL_SFROMURL'));
        $arr['sFromUrlText'] = $comParams->get('sfrom_url_text', JTEXT::_('COM_CWCONTACT_TEXT_URL'));
        $arr['sByTitleLbl'] = $comParams->get('sby_title', JTEXT::_('COM_CWCONTACT_MAIL_TITLE_BY'));
        $arr['reqTitleLbl'] = $comParams->get('req_title', JTEXT::_('COM_CWCONTACT_MAIL_TITLE_REQUEST'));
        $arr['sByIpLbl'] = $comParams->get('ip_lbl', JTEXT::_('COM_CWCONTACT_LABEL_IP'));
        $arr['calFromLbl'] = $comParams->get('cal_from_lbl', JTEXT::_('COM_CWCONTACT_LABEL_CAL_FROM'));
        $arr['calToLbl'] = $comParams->get('cal_to_lbl', JTEXT::_('COM_CWCONTACT_LABEL_CAL_TO'));
        $arr['cInput1Lbl'] = $comParams->get('custom1_lbl', JTEXT::_('COM_CWCONTACT_LABEL_CUSTOM1'));
        $arr['cInput1Hint'] = $comParams->get('custom1_hint', JTEXT::_('COM_CWCONTACT_CUSTOM1_HINT'));
        $arr['cInput2Lbl'] = $comParams->get('custom2_lbl', JTEXT::_('COM_CWCONTACT_LABEL_CUSTOM2'));
        $arr['cInput2Hint'] = $comParams->get('custom2_hint', JTEXT::_('COM_CWCONTACT_CUSTOM2_HINT'));
        $arr['cInput3Lbl'] = $comParams->get('custom3_lbl', JTEXT::_('COM_CWCONTACT_LABEL_CUSTOM3'));
        $arr['cInput3Hint'] = $comParams->get('custom3_hint', JTEXT::_('COM_CWCONTACT_CUSTOM3_HINT'));
        $arr['charcountLbl'] = $comParams->get('charcount_lbl', JTEXT::_('COM_CWCONTACT_LABEL_CHARCOUNT'));
        $arr['charcountLblEnd'] = $comParams->get('charcount_lbl_end', JTEXT::_('COM_CWCONTACT_LABEL_CHARCOUNT_END'));

        // Messages
        $arr['msgRemailMissing'] = $comParams->get('msg_remail_missing', JTEXT::_('COM_CWCONTACT_MSG_REMAIL_MISSING'));
        $arr['msgEmailSent'] = $comParams->get('msg_email_sent', JTEXT::_('COM_CWCONTACT_MSG_EMAIL_SENT'));
        $arr['msgSubjectMissing'] = $comParams->get('msg_subject_missing', JTEXT::_('COM_CWCONTACT_MSG_SUBJECT_MISSING'));
        $arr['msgNameMissing'] = $comParams->get('msg_name_missing', JTEXT::_('COM_CWCONTACT_MSG_NAME_MISSING'));
        $arr['msgDepMissing'] = $comParams->get('msg_dep_missing', JTEXT::_('COM_CWCONTACT_MSG_DEP_MISSING'));
        $arr['msgEmailMissing'] = $comParams->get('msg_email_missing', JTEXT::_('COM_CWCONTACT_MSG_EMAIL_MISSING'));
        $arr['msgEmailInvalid'] = $comParams->get('msg_email_invalid', JTEXT::_('COM_CWCONTACT_MSG_EMAIL_INVALID'));
        $arr['msgDateMissing'] = $comParams->get('msg_date_missing', JTEXT::_('COM_CWCONTACT_MSG_DATE_MISSING'));
        $arr['msgMessageMissing'] = $comParams->get('msg_message_missing', JTEXT::_('COM_CWCONTACT_MSG_MESSAGE_MISSING'));
        $arr['msgCaptchaWrong'] = $comParams->get('msg_captcha_wrong', JTEXT::_('COM_CWCONTACT_MSG_CAPTCHA_WRONG'));
        $arr['msgTosMissing'] = $comParams->get('msg_tos_missing', JTEXT::_('COM_CWCONTACT_MSG_TOS_MISSING'));
        $arr['msgCInput1Missing'] = $comParams->get('msg_custom1_missing', JTEXT::_('COM_CWCONTACT_MSG_CUSTOM1_MISSING'));
        $arr['msgCInput2Missing'] = $comParams->get('msg_custom2_missing', JTEXT::_('COM_CWCONTACT_MSG_CUSTOM2_MISSING'));
        $arr['msgCInput3Missing'] = $comParams->get('msg_custom3_missing', JTEXT::_('COM_CWCONTACT_MSG_CUSTOM3_MISSING'));
        $arr['msgAsterisk'] = $comParams->get('msg_asterisk', JTEXT::_('COM_CWCONTACT_MSG_ASTERISK'));
        

        // Display Fields
        $arr['displaySubject'] = $params->get('display_subject') ? $params->get('display_subject') : $comParams->get('display_subject', 'Y');
        $arr['displayName'] = $params->get('display_name') ? $params->get('display_name') : $comParams->get('display_name', 'Y');
        $arr['displayEmail'] = $params->get('display_email') ? $params->get('display_email') : $comParams->get('display_email', 'R');
        $arr['displayMessage'] = $params->get('display_message') ? $params->get('display_message') : $comParams->get('display_message', 'Y');
        $arr['displayCopyme'] = ($params->get('display_copyme') >= '0') ? $params->get('display_copyme') : $comParams->get('display_copyme', 'N');
        $arr['displayFormat'] = ($params->get('display_date_format') >= '0') ? $params->get('display_date_format') : $comParams->get('display_date_format', '0');
        $arr['displayTos'] = $params->get('display_tos') ? $params->get('display_tos') : $comParams->get('display_tos', 'N');
        $arr['displayCInput1'] = $params->get('display_c_input1') ? $params->get('display_c_input1') : $comParams->get('display_c_input1', 'N');
        $arr['typeCInput1'] = $comParams->get('type_c_input1', 'text');
        $arr['selectCInput1'] = $this->_selectOptions($comParams->get('select_c_input1'));
        $arr['displayCInput2'] = $params->get('display_c_input2') ? $params->get('display_c_input2') : $comParams->get('display_c_input2', 'N');
        $arr['typeCInput2'] = $comParams->get('type_c_input2', 'text');
        $arr['selectCInput2'] = $this->_selectOptions($comParams->get('select_c_input2'));
        $arr['displayCInput3'] = $params->get('display_c_input3') ? $params->get('display_c_input3') : $comParams->get('display_c_input3', 'N');
        $arr['typeCInput3'] = $comParams->get('type_c_input3', 'text');
        $arr['selectCInput3'] = $this->_selectOptions($comParams->get('select_c_input3'));
        $arr['displayChar'] = ($params->get('display_char') >= '0') ? $params->get('display_char') : $comParams->get('display_char', '0');
        $arr['displayCalFrom'] = $params->get('display_cal_from') ? $params->get('display_cal_from') : $comParams->get('display_cal_from', 'N');
        $arr['displayCalTo'] = $params->get('display_cal_to') ? $params->get('display_cal_to') : $comParams->get('display_cal_to', 'N');
        $arr['displayAsteriskMsg'] = ($params->get('display_asterisk_msg') >= '0') ? $params->get('display_asterisk_msg') : $comParams->get('display_asterisk_msg', '0');

        // General Display
        $arr['recipients'] = $params->get('recipients') ? $params->get('recipients') : $comParams->get('recipients', '1');
        $arr['recipient'] = $params->get('recipient') ? $params->get('recipient') : $comParams->get('recipient');
        $arr['departments'] = $params->get('departments') && $params->get('recipients') === '2' ? $params->get('departments') : $comParams->get('departments');
        $arr['depOptions'] = $this->_depOptions($arr['departments']);
        $arr['emailCc'] = $params->get('email_cc') ? $params->get('email_cc') : $comParams->get('email_cc');
        $arr['emailBcc'] = $params->get('email_bcc') ? $params->get('email_bcc') : $comParams->get('email_bcc');
        $arr['redirectUrl'] = $params->get('redirect_url') ? $params->get('redirect_url') : $comParams->get('redirect_url', '2');
        $arr['customUrl'] = $params->get('custom_url') ? $params->get('custom_url') : $comParams->get('custom_url');
        $arr['sysMsg'] = $comParams->get('sys_msg', 'system-message-container');
        $arr['theme'] = $params->get('form_theme') ? $params->get('form_theme') : $comParams->get('form_theme', 'light-clean');
        $arr['tosUrl'] = JRoute::_(JURI::root()) . 'index.php?option=com_content&view=article&id=' . $comParams->get('tos_url') . '&tmpl=component&task=preview';
        $arr['charLimit'] = $params->get('char_limit') ? $params->get('char_limit') : $comParams->get('char_limit', '300');
        $arr['cwcLoadCss'] = $comParams->get('cwc_load_css', '1');
        $arr['submitBtn'] = $params->get('submit_btn') ? $params->get('submit_btn') : $comParams->get('submit_btn', JTEXT::_('COM_CWCONTACT_LABEL_SUB_BUTTON'));
        $arr['submitStyle'] = $params->get('submit_btn_style') ? $params->get('submit_btn_style') : $comParams->get('submit_btn_style', 'default');
        $arr['submitCustom'] = $params->get('submit_btn_custom') ? $params->get('submit_btn_custom') : $comParams->get('submit_btn_custom', '');
        $arr['submitClass'] = $arr['submitStyle'] === 'custom' ? $arr['submitCustom'] : $arr['submitStyle'];
        $arr['formWidth'] = $params->get('form_width') ? $params->get('form_width') : $comParams->get('form_width', '100');
        $arr['dateTimeFormat'] = $comParams->get('datetime_format', 'LC4');
        $arr['dFormat'] = $this->_dateFormat($arr);
        $arr['dateFormatLabel'] = $comParams->get('date_format_lbl', JTEXT::_('COM_CWCONTACT_DATETIME_FORMAT'));
        $arr['emailSubject'] = $params->get('email_subject') ? $params->get('email_subject') : $comParams->get('email_subject', JTEXT::_('COM_CWCONTACT_LABEL_SUBJECT'));
        $arr['mailFromOpt'] = $comParams->get('mail_from', '2');
        $arr['emailFormat'] = $comParams->get('email_format', 'nohtml');
        
        // Mail
        $arr['displayMailRequestA'] = $comParams->get('display_mail_request_a', '1');
        $arr['displayMailSentbyA'] = $comParams->get('display_mail_sentby_a', '1');
        $arr['displayMailSentfromA'] = $comParams->get('display_mail_sentfrom_a', '1');
        $arr['displayMailThankyou'] = $comParams->get('display_mail_thankyou', '0');
        $arr['mailThankyouTitle'] = $comParams->get('mail_thankyou_title', JTEXT::sprintf('COM_CWCONTACT_THANKYOU_TITLE', $app->getCfg('sitename')));
        $arr['mailThankyouMsg'] = $comParams->get('mail_thankyou_msg', JTEXT::_('COM_CWCONTACT_THANKYOU_MSG'));
        $arr['displayMailRequestC'] = $comParams->get('display_mail_request_c', '1');
        $arr['displayMailSentbyC'] = $comParams->get('display_mail_sentby_c', '1');
        $arr['displayMailSentfromC'] = $comParams->get('display_mail_sentfrom_c', '1');

        // Global
        $arr['sitename'] = $app->getCfg('sitename');
        $arr['mailFrom'] = $app->getCfg('mailfrom');

        // Captcha
        $arr['whichCaptcha'] = $params->get('which_captcha') ? $params->get('which_captcha') : $comParams->get('which_captcha', 'none');
        $arr['displayCapTitle'] = ($params->get('display_captcha_title') >= '0') ? $params->get('display_captcha_title') : $comParams->get('display_captcha_title', '1');
        $arr['captchaHeading'] = $comParams->get('captcha_heading', JTEXT::_('COM_CWCONTACT_CAPTCHA_HEADING'));
        $arr['captchaHint'] = $comParams->get('captcha_hint', JTEXT::_('COM_CWCONTACT_CAPTCHA_HINT'));
        $arr['bCaptchaQuestion'] = $params->get('bcaptcha_question') ? $params->get('bcaptcha_question') : $comParams->get('bcaptcha_question', JTEXT::_('COM_CWCONTACT_BCAPTCHA_DEFAULT_QUESTION'));
        $arr['bCaptchaAnswer'] = $params->get('bcaptcha_answer') ? $params->get('bcaptcha_answer') : $comParams->get('bcaptcha_answer', JTEXT::_('COM_CWCONTACT_BCAPTCHA_DEFAULT_ANSWER'));

        //Map
        $arr['displayMap'] = $params->get('display_map') != '' ? $params->get('display_map') : $comParams->get('display_map', '0');
        $arr['mapLocation'] = $params->get('map_location') ? $params->get('map_location') : $comParams->get('map_location', 'top');
        $arr['mapLat'] = $comParams->get('map_lat');
        $arr['mapLat'] = $comParams->get('map_lat');
        $arr['mapLong'] = $comParams->get('map_long');
        $arr['mapHeight'] = $params->get('map_height') ? $params->get('map_height') : $comParams->get('map_height', '400');
        $arr['mapZoom'] = $comParams->get('map_zoom', '6');
        $arr['mapTitle'] = $comParams->get('map_title');
        $arr['mapText'] = $comParams->get('map_text');
        $arr['mapPopupWidth'] = $comParams->get('map_popup_width', '300');

        return $arr;
    }

    function getRedirect($samePageUrl) {
        $homeUrl = JRoute::_(JURI::root());
        $myparams = $this->_params;

        switch ($myparams['redirectUrl']) {
            case 1: $sucessUrl = ($samePageUrl . '#' . $myparams['sysMsg']);
                break;
            case 2: $sucessUrl = ($homeUrl . '#' . $myparams['sysMsg']);
                break;
            case 3: $sucessUrl = ($myparams['customUrl'] . '#' . $myparams['sysMsg']);
                break;
            default: $sucessUrl = JRoute::_(JURI::root());
                break;
        }

        return $sucessUrl;
    }

    function checkSubject($subject) {
        $myparams = $this->_params;

        if ($myparams['displaySubject'] == 'R') {
            if (!$subject) {
                $arr['subjectError'] = $myparams['msgSubjectMissing'];
                $arr['hasError'] = true;
            } else {
                $arr['subjectError'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    function checkDep($dep) {
        $myparams = $this->_params;
        if (!$dep || $dep === '') {
            $arr['depError'] = $myparams['msgDepMissing'];
            $arr['hasError'] = true;
        } else {
            $arr['depError'] = null;
            $arr['hasError'] = false;
        }
        return $arr;
    }

    function checkName($name) {
        $myparams = $this->_params;

        if ($myparams['displayName'] == 'R') {
            if (!$name) {
                $arr['nameError'] = $myparams['msgNameMissing'];
                $arr['hasError'] = true;
            } else {
                $arr['nameError'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    function checkMessage($message) {
        $myparams = $this->_params;

        if ($myparams['displayMessage'] == 'R') {
            if (!$message) {
                $arr['messageError'] = $myparams['msgMessageMissing'];
                $arr['hasError'] = true;
            } else {
                $arr['messageError'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    function checkTos($tos) {
        $myparams = $this->_params;

        if ($myparams['displayTos'] == 'R') {
            if (!$tos) {
                $arr['tosError'] = $myparams['msgTosMissing'];
                $arr['hasError'] = true;
            } else {
                $arr['tosError'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    function checkEmail($email) {
        $myparams = $this->_params;

        if ($myparams['displayEmail'] == 'R') {
            if (!$email) {
                $arr['emailError'] = $myparams['msgEmailMissing'];
                $arr['hasError'] = true;
            } else if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/", strtolower($email))) {
                $arr['emailError'] = $myparams['msgEmailInvalid'];
                $arr['hasError'] = true;
            } else {
                $arr['emailError'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        } else if ($myparams['displayEmail'] == 'Y' && $email) {
            $regex = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/";
            if (!preg_match($regex, strtolower($email))) {
                $arr['emailError'] = $myparams['msgEmailInvalid'];
                $arr['hasError'] = true;
            } else {
                $arr['emailError'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    function checkCInput1($cInput1) {
        $myparams = $this->_params;

        if ($myparams['displayCInput1'] == 'R') {
            if (!$cInput1) {
                $arr['cInput1Error'] = $myparams['msgCInput1Missing'];
                $arr['hasError'] = true;
            } else {
                $arr['cInput1Error'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    function checkCInput2($cInput2) {
        $myparams = $this->_params;

        if ($myparams['displayCInput2'] == 'R') {
            if (!$cInput2) {
                $arr['cInput2Error'] = $myparams['msgCInput2Missing'];
                $arr['hasError'] = true;
            } else {
                $arr['cInput2Error'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    function checkCInput3($cInput3) {
        $myparams = $this->_params;

        if ($myparams['displayCInput3'] == 'R') {
            if (!$cInput3) {
                $arr['cInput3Error'] = $myparams['msgCInput3Missing'];
                $arr['hasError'] = true;
            } else {
                $arr['cInput3Error'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    function checkCalFrom($calFrom) {
        $myparams = $this->_params;

        if ($myparams['displayCalFrom'] == 'R') {
            if (!$calFrom) {
                $arr['calFromError'] = $myparams['msgDateMissing'];
                $arr['hasError'] = true;
            } else {
                $arr['calFromError'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    function checkCalTo($calTo) {
        $myparams = $this->_params;

        if ($myparams['displayCalTo'] == 'R') {
            if (!$calTo) {
                $arr['calToError'] = $myparams['msgDateMissing'];
                $arr['hasError'] = true;
            } else {
                $arr['calToError'] = null;
                $arr['hasError'] = false;
            }
            return $arr;
        }
    }

    private function _dateFormat($myparams) {

        switch ($myparams['dateTimeFormat']) {
            case 'LC1':
                $arr['dateFormat'] = JText::_('DATE_FORMAT_LC1');
                $arr['dateFormatOld'] = '%A, %e %B %Y';
                $arr['formatHint'] = JText::_('COM_CWCONTACT_DATETIME_HINT_LC1');
                break;
            case 'LC3':
                $arr['dateFormat'] = JText::_('DATE_FORMAT_LC3');
                $arr['dateFormatOld'] = '%e %B %Y';
                $arr['formatHint'] = JText::_('COM_CWCONTACT_DATETIME_HINT_LC3');
                break;
            case 'LC4':
                $arr['dateFormat'] = JText::_('DATE_FORMAT_LC4');
                $arr['dateFormatOld'] = '%Y-%m-%e';
                $arr['formatHint'] = JText::_('COM_CWCONTACT_DATETIME_HINT_LC4');
                break;
            case'JS1':
                $arr['dateFormat'] = JText::_('DATE_FORMAT_JS1');
                $arr['dateFormatOld'] = '%y-%m-%e';
                $arr['formatHint'] = JText::_('COM_CWCONTACT_DATETIME_HINT_JS1');
                break;
        }

        return $arr;
    }

    //Create our custom select lists based on CSV list
    private function _selectOptions($selectCInput) {
        $arr = explode(",", $selectCInput);
        foreach ($arr as $key => $opt) {
            $txt = $opt;
            $opt = preg_replace('/\s+/', '_', $opt);
            $arr[$key] = '<option value=' . $opt . '>' . $txt . '</option>';
        }
        $opts = implode(PHP_EOL, $arr);
        return $opts;
    }

    /**
     * 
     * @param type $depOptions
     * @return type
     * 
     */
    private function _depOptions($depOptions) {
        if (!$depOptions) {
            return null;
        }

        if (!class_exists('CryptHelper')) {
            include_once JPATH_ADMINISTRATOR . '/components/com_coalawebcontact/helpers/crypt.php';
        }
        
        $crypt = new CryptHelper();
        
        $arr = explode(",", $depOptions);
        foreach ($arr as $key => $opt) {
            list($part1, $part2) = explode(":", $opt);
            $arr[$key] = '<option value=' . $crypt->str_encrypt($part2) . '>' . $part1 . '</option>';
        }
        $opts = implode(PHP_EOL, $arr);
        return $opts;
    }

    function emailBody($fields, $extSubject, $fromUrl, $tosAnswer) {
        $myparams = $this->_params;

        // Setup email body text 
        switch ($myparams['emailFormat']) {
            case "nohtml":
                $body = '';
                if ($fields['calFrom'] && $fields['calTo']) {
                    $dates = $myparams['calFromLbl'] . ": " . $fields['calFrom'] . " - " . $fields['calTo'] . "\r\n";
                } elseif ($fields['calFrom']) {
                    $dates = $myparams['calFromLbl'] . ": " . $fields['calFrom'] . "\r\n\r\n";
                } else {
                    $dates = "";
                }
                
                if ($myparams['displayMailRequestA'] && $fields['message']) {
                    $body .= $myparams['reqTitleLbl'] . "\r\n";
                    $body .= $extSubject . "\r\n";
                    $body .= $fields['message'] ? $myparams['messageLbl'] . ": " . $fields['message'] . "\r\n" : "";
                    $body .= "\r\n";
                }
                
                if ($myparams['displayMailSentbyA']) {
                    $body .= $myparams['sByTitleLbl'] . "\r\n";
                    $body .= $fields['name'] ? $myparams['nameLbl'] . ": " . $fields['name'] . "\r\n" : "";
                    $body .= $fields['email'] ? $myparams['emailLbl'] . ": " . $fields['email'] . "\r\n" : "";
                    $body .= $fields['cInput1'] ? $myparams['cInput1Lbl'] . ": " . $fields['cInput1'] . "\r\n" : "";
                    $body .= $fields['cInput2'] ? $myparams['cInput2Lbl'] . ": " . $fields['cInput2'] . "\r\n" : "";
                    $body .= $fields['cInput3'] ? $myparams['cInput3Lbl'] . ": " . $fields['cInput3'] . "\r\n" : "";
                    $body .= $dates;
                    $body .= $fields['tos'] ? $myparams['tosLbl'] . $myparams['tosLinkLbl'] . ": " . $tosAnswer . "\r\n" : "";
                    $body .= "\r\n";
                }
                
                if ($myparams['displayMailSentfromA']) {
                    $body .= $myparams['sFromTitleLbl'] . "\r\n";
                    $body .= $fields['remoteHost'] ? $myparams['sByIpLbl'] . ": " . $fields['remoteHost'] . "\r\n" : "";
                    $body .= $myparams['sFromWebLbl'] . ": " . $myparams['sitename'] . "\r\n";
                    $body .= $fromUrl ? $myparams['sFromUrlLbl'] . ": " . $fromUrl : "";
                    $body .= "\r\n";
                }
                break;
                
            case "basic":
                $body[] = '<div>';
                
                if ($myparams['displayMailRequestA'] && $fields['message']) {
                    $body[] = '<h4>' . $myparams['reqTitleLbl'] . ':</h4>';
                    $body[] = '<ul>';
                    $body[] = $fields['subject'] ? '<li><span style="font-weight:bold;">' . $myparams['emailSubject'] . ':</span> ' . $fields['subject'] . '</li>' : '';
                    $body[] = $fields['message'] ? '<li><span style="font-weight:bold;">' . $myparams['messageLbl'] . ':</span> ' . nl2br($fields['message']) . '</li>' : '';
                    $body[] = '</ul>';
                }
                
                if ($myparams['displayMailSentbyA']) {
                    $body[] = '<h4>' . $myparams['sByTitleLbl'] . ':</h4>';
                    $body[] = '<ul>';
                    $body[] = $fields['name'] ? '<li><span style="font-weight:bold;">' . $myparams['nameLbl'] . ':</span> ' . $fields['name'] . '</li>' : '';
                    $body[] = $fields['cInput1'] ? '<li><span style="font-weight:bold;">' . $myparams['cInput1Lbl'] . ':</span> ' . $fields['cInput1'] . '</li>' : '';
                    $body[] = $fields['cInput2'] ? '<li><span style="font-weight:bold;">' . $myparams['cInput2Lbl'] . ':</span> ' . $fields['cInput2'] . '</li>' : '';
                    $body[] = $fields['cInput3'] ? '<li><span style="font-weight:bold;">' . $myparams['cInput3Lbl'] . ':</span> ' . $fields['cInput3'] . '</li>' : '';
                    $body[] = $fields['calFrom'] ? '<li><span style="font-weight:bold;">' . $myparams['calFromLbl'] . ':</span> ' . $fields['calFrom'] . '</li>' : '';
                    $body[] = $fields['calTo'] ? '<li><span style="font-weight:bold;">' . $myparams['calToLbl'] . ':</span> ' . $fields['calTo'] . '</li>' : '';
                    $body[] = $fields['tos'] ? '<li><span style="font-weight:bold;">' . $myparams['tosLbl'] . $myparams['tosLinkLbl'] . ':</span> ' . $tosAnswer . '</li>' : '';
                    $body[] = $fields['email'] ? '<li><span style="font-weight:bold;">' . $myparams['emailLbl'] . ':</span> ' . $fields['email'] . '</li>' : '';
                    $body[] = '</ul>';
                }
                
                if ($myparams['displayMailSentfromA']) {
                    $body[] = '<h4>' . $myparams['sFromTitleLbl'] . ':</h4>';
                    $body[] = '<ul>';
                    $body[] = '<li><span style="font-weight:bold;">' . $myparams['sByIpLbl'] . ':</span> ' . $fields['remoteHost'] . '</li>';
                    $body[] = '<li><span style="font-weight:bold;">' . $myparams['sFromWebLbl'] . ':</span> ' . $myparams['sitename'] . '</li>';
                    $body[] = '<li><span style="font-weight:bold;">' . $myparams['sFromUrlLbl'] . ':</span> <a href="' . $fromUrl . '" target="_blank" >' . $myparams['sFromUrlText'] . '</a></li>';
                    $body[] = '</ul>';
                }
                
                $body[] = '</div>';
                
                break;
                
            case "rich":
                $body[] = '<table style="width: 100%;">';
                $body[] = '<tbody><tr><td style="background: #ffffff; clear: both !important; display: block !important; margin: 0 auto !important; max-width: 600px !important;">';
                $body[] = '<div style="display: block; margin: 0 auto; padding: 15px;">';
                
                if ($myparams['displayMailRequestA'] && $fields['message']) {
                    $body[] = '<table style="border: 1px solid #dcdbdb; margin-bottom: 15px; width: 100%;" cellspacing="5" cellpadding="5">';
                    $body[] = '<tbody><tr><td>';
                    $body[] = '<h4 style="color: #56595a; margin-bottom: 5px;">' .$myparams['reqTitleLbl'] . ':</h4>';
                    $body[] = '<ul style="color: #56595a; list-style:none;">';
                    $body[] = $fields['subject'] ? '<li><span style="font-weight:bold;">' . $myparams['emailSubject'] . ':</span> ' . $fields['subject'] . '</li>' : '';
                    $body[] = $fields['message'] ? '<li><span style="font-weight:bold;">' . $myparams['messageLbl'] . ':</span> ' . nl2br($fields['message']) . '</li>' : '';
                    $body[] = '</ul>';
                    $body[] = '</td></tr></tbody>';
                    $body[] = '</table>';
                }

                if ($myparams['displayMailSentbyA']) {
                    $body[] = '<table style="background-color: #ebf5fa; border: 1px solid #D2DCE1; margin-bottom: 15px; width: 100%;" cellspacing="5" cellpadding="5">';
                    $body[] = '<tbody><tr><td>';
                    $body[] = '<h4 style="color: #56595a; margin-bottom: 5px;">' . $myparams['sByTitleLbl'] . ':</h4>';
                    $body[] = '<ul style="color: #56595a; list-style:none;">';
                    $body[] = $fields['name'] ? '<li>' . $myparams['nameLbl'] . ': <span style="color: #328ab3;">' . $fields['name'] . '</span></li>' : '';
                    $body[] = $fields['cInput1'] ? '<li>' . $myparams['cInput1Lbl'] . ': <span style="color: #328ab3;">' . $fields['cInput1'] . '</span></li>' : '';
                    $body[] = $fields['cInput2'] ? '<li>' . $myparams['cInput2Lbl'] . ': <span style="color: #328ab3;">' . $fields['cInput2'] . '</span></li>' : '';
                    $body[] = $fields['cInput3'] ? '<li>' . $myparams['cInput3Lbl'] . ': <span style="color: #328ab3;">' . $fields['cInput3'] . '</span></li>' : '';
                    $body[] = $fields['calFrom'] ? '<li>' . $myparams['calFromLbl'] . ': <span style="color: #328ab3;">' . $fields['calFrom'] . '</span></li>' : '';
                    $body[] = $fields['calTo'] ? '<li>' . $myparams['calToLbl'] . ': <span style="color: #328ab3;">' . $fields['calTo'] . '</span></li>' : '';
                    $body[] = $fields['tos'] ? '<li>' . $myparams['tosLbl'] . $myparams['tosLinkLbl'] . ': <span style="color: #328ab3;">' . $tosAnswer . '</span></li>' : '';
                    $body[] = $fields['email'] ? '<li>' . $myparams['emailLbl'] . ': <span style="color: #328ab3;">' . $fields['email'] . '</span></li>' : '';
                    $body[] = '</ul>';
                    $body[] = '</td></tr></tbody></table>';
                }
                
                if ($myparams['displayMailSentfromA']) {
                    $body[] = '<table style="background-color: #ebebeb; border: 1px solid #dcdbdb; width: 100%;" cellspacing="5" cellpadding="5">';
                    $body[] = '<tbody><tr><td>';
                    $body[] = '<h4 style="color: #56595a; margin-bottom: 5px;">' . $myparams['sFromTitleLbl'] . ':</h4>';
                    $body[] = '<ul style="color: #56595a; list-style:none;">';
                    $body[] = '<li>' . $myparams['sByIpLbl'] . ': <span style="color: #328ab3; ">' . $fields['remoteHost'] . '</span></li>';
                    $body[] = '<li>' . $myparams['sFromWebLbl'] . ': <span style="color: #328ab3;">' . $myparams['sitename'] . '</span></li>';
                    $body[] = '<li>' . $myparams['sFromUrlLbl'] . ': <a style="color: #328ab3; " href="' . $fromUrl . '" target="_blank" >' . $myparams['sFromUrlText'] . '</a></span></li>';
                    $body[] = '</ul>';
                    $body[] = '</td></tr></tbody>';
                    $body[] = '</table>';
                }
                
                $body[] = '</div>';
                $body[] = '</td></tr></tbody>';
                $body[] = '</table>';

                break;
        }

        return $body;
    }
    
     function copymeBody($fields, $extSubject, $fromUrl, $tosAnswer) {
        $myparams = $this->_params;

        // Setup email body text 
        switch ($myparams['emailFormat']) {
            case "nohtml":
                $body = '';
                if ($fields['calFrom'] && $fields['calTo']) {
                    $dates = $myparams['calFromLbl'] . ": " . $fields['calFrom'] . " - " . $fields['calTo'] . "\r\n";
                } elseif ($fields['calFrom']) {
                    $dates = $myparams['calFromLbl'] . ": " . $fields['calFrom'] . "\r\n\r\n";
                } else {
                    $dates = "";
                }

                if ($myparams['displayMailThankyou']) {
                    $body .= $myparams['mailThankyouTitle'] . "\r\n";
                    $body .= $myparams['mailThankyouMsg'] . "\r\n";
                    $body .= "\r\n";
                }
                
                if ($myparams['displayMailRequestC'] && $fields['message']) {
                    $body .= $myparams['reqTitleLbl'] . "\r\n";
                    $body .= $extSubject . "\r\n";
                    $body .= $fields['message'] ? $myparams['messageLbl'] . ": " . $fields['message'] . "\r\n" : "";
                    $body .= "\r\n";
                }
                
                if ($myparams['displayMailSentbyC']) {
                    $body .= $myparams['sByTitleLbl'] . "\r\n";
                    $body .= $fields['name'] ? $myparams['nameLbl'] . ": " . $fields['name'] . "\r\n" : "";
                    $body .= $fields['email'] ? $myparams['emailLbl'] . ": " . $fields['email'] . "\r\n" : "";
                    $body .= $fields['cInput1'] ? $myparams['cInput1Lbl'] . ": " . $fields['cInput1'] . "\r\n" : "";
                    $body .= $fields['cInput2'] ? $myparams['cInput2Lbl'] . ": " . $fields['cInput2'] . "\r\n" : "";
                    $body .= $fields['cInput3'] ? $myparams['cInput3Lbl'] . ": " . $fields['cInput3'] . "\r\n" : "";
                    $body .= $dates;
                    $body .= $fields['tos'] ? $myparams['tosLbl'] . $myparams['tosLinkLbl'] . ": " . $tosAnswer . "\r\n" : "";
                    $body .= "\r\n";
                }
                
                if ($myparams['displayMailSentfromC']) {
                    $body .= $myparams['sFromTitleLbl'] . "\r\n";
                    $body .= $fields['remoteHost'] ? $myparams['sByIpLbl'] . ": " . $fields['remoteHost'] . "\r\n" : "";
                    $body .= $myparams['sFromWebLbl'] . ": " . $myparams['sitename'] . "\r\n";
                    $body .= $fromUrl ? $myparams['sFromUrlLbl'] . ": " . $fromUrl : "";
                    $body .= "\r\n";
                }
                break;
                
            case "basic":
                $body[] = '<div>';
                
                if ($myparams['displayMailThankyou']) {
                    $body[] = '<h4>' . $myparams['mailThankyouTitle'] . '</h4>';
                    $body[] = '<ul>';
                    $body[] = '<li>' . $myparams['mailThankyouMsg'] . '</li>';
                    $body[] = '</ul>';
                }
                
                if ($myparams['displayMailRequestC'] && $fields['message']) {
                    $body[] = '<h4>' . $myparams['reqTitleLbl'] . ':</h4>';
                    $body[] = '<ul>';
                    $body[] = $fields['subject'] ? '<li><span style="font-weight:bold;">' . $myparams['emailSubject'] . ':</span> ' . $fields['subject'] . '</li>' : '';
                    $body[] = $fields['message'] ? '<li><span style="font-weight:bold;">' . $myparams['messageLbl'] . ':</span> ' . nl2br($fields['message']) . '</li>' : '';
                    $body[] = '</ul>';
                }
                
                if ($myparams['displayMailSentbyC']) {
                    $body[] = '<h4>' . $myparams['sByTitleLbl'] . ':</h4>';
                    $body[] = '<ul>';
                    $body[] = $fields['name'] ? '<li><span style="font-weight:bold;">' . $myparams['nameLbl'] . ':</span> ' . $fields['name'] . '</li>' : '';
                    $body[] = $fields['cInput1'] ? '<li><span style="font-weight:bold;">' . $myparams['cInput1Lbl'] . ':</span> ' . $fields['cInput1'] . '</li>' : '';
                    $body[] = $fields['cInput2'] ? '<li><span style="font-weight:bold;">' . $myparams['cInput2Lbl'] . ':</span> ' . $fields['cInput2'] . '</li>' : '';
                    $body[] = $fields['cInput3'] ? '<li><span style="font-weight:bold;">' . $myparams['cInput3Lbl'] . ':</span> ' . $fields['cInput3'] . '</li>' : '';
                    $body[] = $fields['calFrom'] ? '<li><span style="font-weight:bold;">' . $myparams['calFromLbl'] . ':</span> ' . $fields['calFrom'] . '</li>' : '';
                    $body[] = $fields['calTo'] ? '<li><span style="font-weight:bold;">' . $myparams['calToLbl'] . ':</span> ' . $fields['calTo'] . '</li>' : '';
                    $body[] = $fields['tos'] ? '<li><span style="font-weight:bold;">' . $myparams['tosLbl'] . $myparams['tosLinkLbl'] . ':</span> ' . $tosAnswer . '</li>' : '';
                    $body[] = $fields['email'] ? '<li><span style="font-weight:bold;">' . $myparams['emailLbl'] . ':</span> ' . $fields['email'] . '</li>' : '';
                    $body[] = '</ul>';
                }
                
                if ($myparams['displayMailSentfromC']) {
                    $body[] = '<h4>' . $myparams['sFromTitleLbl'] . ':</h4>';
                    $body[] = '<ul>';
                    $body[] = '<li><span style="font-weight:bold;">' . $myparams['sByIpLbl'] . ':</span> ' . $fields['remoteHost'] . '</li>';
                    $body[] = '<li><span style="font-weight:bold;">' . $myparams['sFromWebLbl'] . ':</span> ' . $myparams['sitename'] . '</li>';
                    $body[] = '<li><span style="font-weight:bold;">' . $myparams['sFromUrlLbl'] . ':</span> <a href="' . $fromUrl . '" target="_blank" >' . $myparams['sFromUrlText'] . '</a></li>';
                    $body[] = '</ul>';
                }
                
                $body[] = '</div>';
                
                break;
                
            case "rich":
                $body[] = '<table style="width: 100%;">';
                $body[] = '<tbody><tr><td style="background: #ffffff; clear: both !important; display: block !important; margin: 0 auto !important; max-width: 600px !important;">';
                $body[] = '<div style="display: block; margin: 0 auto; padding: 15px;">';
                
                if ($myparams['displayMailThankyou']) {
                    $body[] = '<table style="border: 1px solid #dcdbdb; margin-bottom: 15px; width: 100%;" cellspacing="5" cellpadding="5">';
                    $body[] = '<tbody><tr><td>';
                    $body[] = '<h4 style="color: #56595a; margin-bottom: 5px;">' . $myparams['mailThankyouTitle'] . '</h4>';
                    $body[] = '<ul style="color: #56595a; list-style:none;">';
                    $body[] = '<li>' . $myparams['mailThankyouMsg'] . '</li>';
                    $body[] = '</ul>';
                    $body[] = '</td></tr></tbody>';
                    $body[] = '</table>';
                }
                
                if ($myparams['displayMailRequestC'] && $fields['message']) {
                    $body[] = '<table style="border: 1px solid #dcdbdb; margin-bottom: 15px; width: 100%;" cellspacing="5" cellpadding="5">';
                    $body[] = '<tbody><tr><td>';
                    $body[] = '<h4 style="color: #56595a; margin-bottom: 5px;">' .$myparams['reqTitleLbl'] . ':</h4>';
                    $body[] = '<ul style="color: #56595a; list-style:none;">';
                    $body[] = $fields['subject'] ? '<li><span style="font-weight:bold;">' . $myparams['emailSubject'] . ':</span> ' . $fields['subject'] . '</li>' : '';
                    $body[] = $fields['message'] ? '<li><span style="font-weight:bold;">' . $myparams['messageLbl'] . ':</span> ' . nl2br($fields['message']) . '</li>' : '';
                    $body[] = '</ul>';
                    $body[] = '</td></tr></tbody>';
                    $body[] = '</table>';
                }

                if ($myparams['displayMailSentbyC']) {
                    $body[] = '<table style="background-color: #ebf5fa; border: 1px solid #D2DCE1; margin-bottom: 15px; width: 100%;" cellspacing="5" cellpadding="5">';
                    $body[] = '<tbody><tr><td>';
                    $body[] = '<h4 style="color: #56595a; margin-bottom: 5px;">' . $myparams['sByTitleLbl'] . ':</h4>';
                    $body[] = '<ul style="color: #56595a; list-style:none;">';
                    $body[] = $fields['name'] ? '<li>' . $myparams['nameLbl'] . ': <span style="color: #328ab3;">' . $fields['name'] . '</span></li>' : '';
                    $body[] = $fields['cInput1'] ? '<li>' . $myparams['cInput1Lbl'] . ': <span style="color: #328ab3;">' . $fields['cInput1'] . '</span></li>' : '';
                    $body[] = $fields['cInput2'] ? '<li>' . $myparams['cInput2Lbl'] . ': <span style="color: #328ab3;">' . $fields['cInput2'] . '</span></li>' : '';
                    $body[] = $fields['cInput3'] ? '<li>' . $myparams['cInput3Lbl'] . ': <span style="color: #328ab3;">' . $fields['cInput3'] . '</span></li>' : '';
                    $body[] = $fields['calFrom'] ? '<li>' . $myparams['calFromLbl'] . ': <span style="color: #328ab3;">' . $fields['calFrom'] . '</span></li>' : '';
                    $body[] = $fields['calTo'] ? '<li>' . $myparams['calToLbl'] . ': <span style="color: #328ab3;">' . $fields['calTo'] . '</span></li>' : '';
                    $body[] = $fields['tos'] ? '<li>' . $myparams['tosLbl'] . $myparams['tosLinkLbl'] . ': <span style="color: #328ab3;">' . $tosAnswer . '</span></li>' : '';
                    $body[] = $fields['email'] ? '<li>' . $myparams['emailLbl'] . ': <span style="color: #328ab3;">' . $fields['email'] . '</span></li>' : '';
                    $body[] = '</ul>';
                    $body[] = '</td></tr></tbody></table>';
                }
                
                if ($myparams['displayMailSentfromC']) {
                    $body[] = '<table style="background-color: #ebebeb; border: 1px solid #dcdbdb; width: 100%;" cellspacing="5" cellpadding="5">';
                    $body[] = '<tbody><tr><td>';
                    $body[] = '<h4 style="color: #56595a; margin-bottom: 5px;">' . $myparams['sFromTitleLbl'] . ':</h4>';
                    $body[] = '<ul style="color: #56595a; list-style:none;">';
                    $body[] = '<li>' . $myparams['sByIpLbl'] . ': <span style="color: #328ab3; ">' . $fields['remoteHost'] . '</span></li>';
                    $body[] = '<li>' . $myparams['sFromWebLbl'] . ': <span style="color: #328ab3;">' . $myparams['sitename'] . '</span></li>';
                    $body[] = '<li>' . $myparams['sFromUrlLbl'] . ': <a style="color: #328ab3; " href="' . $fromUrl . '" target="_blank" >' . $myparams['sFromUrlText'] . '</a></span></li>';
                    $body[] = '</ul>';
                    $body[] = '</td></tr></tbody>';
                    $body[] = '</table>';
                }
                
                $body[] = '</div>';
                $body[] = '</td></tr></tbody>';
                $body[] = '</table>';

                break;
        }

        return $body;
    }

    private function _addAssets() {
        $myparams = $this->_params;
        $doc = JFactory::getDocument();

        JHtml::_('jquery.framework');
        JHTML::_('behavior.formvalidation');
        
        if ($myparams['displayCalFrom'] == 'Y' || $myparams['displayCalFrom'] == 'R' || $myparams['displayCalTo'] == 'Y' || $myparams['displayCalTo'] == 'R') {
            JHTML::_('behavior.calendar');
        }

        if ($myparams['displayTos'] == 'Y' || $myparams['displayTos'] == 'R') {
           $doc->addScript(JURI::base(true) . '/media/coalaweb/modules/generic/js/popup.js');
        }

        $urlModMedia = JURI :: base(true) . '/media/coalawebcontact/components/contact/themes/' . $myparams['theme'] . '/css/';
        $urlModJs = JURI :: base(true) . '/media/coalawebcontact/components/contact/js/';

        if ($myparams['cwcLoadCss']) {
            $doc->addStyleSheet($urlModMedia . 'cw-mod-contact-' . $myparams['theme'] . '.css');
        }

        if ($myparams['cwcLoadCss'] && $myparams['submitStyle'] != 'default') {
            $doc->addStyleSheet(JURI :: base(true) . '/media/coalawebcontact/components/contact/css/cwc-buttons.css');
        }

        //Custom HTML5 Messages
        $doc->addScript($urlModJs . 'jquery.html5cvm.min.js');

        //Character count
        $doc->addScript($urlModJs . 'jquery.simplyCountable.js');

        //CoalaWeb JS Tools
        $jsTools = $this->_getJSTools();
        $jsCode = $this->_cleanCode($jsTools);
        $doc->addScriptDeclaration($jsCode);

        //Google Map
        if ($myparams['displayMap'] === '1') {
            $jsMap = $this->_getMap();
            $mapCode = $this->_cleanCode($jsMap);
            $doc->addScriptDeclaration($mapCode);
        }

        return;
    }

    private function _getJSTools() {
        $myparams = $this->_params;
        $charLimit = $myparams['charLimit'];

        $javascript[] = '//CoalaWeb Contact Module Tools ';
        $javascript[] = 'var $cw = jQuery.noConflict();';
        $javascript[] = '$cw(document).ready(function (){';
        
        //Custom messages
        $javascript[] = '$cw("#cw-mod-contact-' . $myparams['theme'] . '-fm' . $this->_uniqueid . '").html5cvm();';

        if ($myparams['displayChar']) {
            $javascript[] = '   //Character Count ';
            $javascript[] = '   $cw("#cw_mod_contact_message' . $this->_uniqueid . '").simplyCountable({';
            $javascript[] = '       maxCount:' . $charLimit . ',';
            $javascript[] = '       counter: "#cw_mod_contact_counter' . $this->_uniqueid . '",';
            $javascript[] = '   });';  
        }

        if ($myparams['displayCalFrom'] == 'Y' || $myparams['displayCalFrom'] == 'R') {
            $javascript[] = '   //Set Calendar From read only ';
            $javascript[] = '   $cw("#cw_mod_contact_cal_from' . $this->_uniqueid . '").prop("readonly", true);';
        }

        if ($myparams['displayCalTo'] == 'Y' || $myparams['displayCalTo'] == 'R') {
            $javascript[] = '   //Set Calendar To read only ';
            $javascript[] = '   $cw("#cw_mod_contact_cal_to' . $this->_uniqueid . '").prop("readonly", true);';
        }

        $javascript[] = '   //Keep Department selection on page reload ';
        $javascript[] = '   var item = window.localStorage.getItem("cw_mod_contact_dep' . $this->_uniqueid . '");';
        $javascript[] = '   if (item){$cw("select[name=cw_mod_contact_dep' . $this->_uniqueid . ']").val(item);}';
        $javascript[] = '   $cw("select[name=cw_mod_contact_dep' . $this->_uniqueid . ']").change(function() {';
        $javascript[] = '       window.localStorage.setItem("cw_mod_contact_dep' . $this->_uniqueid . '", $cw(this).val());';
        $javascript[] = '   });';

        if ($myparams['displayCInput1'] == 'Y' || $myparams['displayCInput1'] == 'R' && $myparams['typeCInput1'] === 'select') {
            $javascript[] = '   //Keep Custom 1 selection on page reload ';
            $javascript[] = '   var item = window.localStorage.getItem("cw_mod_contact_cinput1' . $this->_uniqueid . '");';
            $javascript[] = '   $cw("select[name=cw_mod_contact_cinput1' . $this->_uniqueid . ']").val(item);';
            $javascript[] = '   $cw("select[name=cw_mod_contact_cinput1' . $this->_uniqueid . ']").change(function() {';
            $javascript[] = '   window.localStorage.setItem("cw_mod_contact_cinput1' . $this->_uniqueid . '", $cw(this).val());';
            $javascript[] = '   });';
        }

        if ($myparams['displayCInput2'] == 'Y' || $myparams['displayCInput2'] == 'R' && $myparams['typeCInput2'] === 'select') {
            $javascript[] = '   //Keep Custom 2 selection on page reload ';
            $javascript[] = '   var item = window.localStorage.getItem("cw_mod_contact_cinput2' . $this->_uniqueid . '");';
            $javascript[] = '   $cw("select[name=cw_mod_contact_cinput2' . $this->_uniqueid . ']").val(item);';
            $javascript[] = '   $cw("select[name=cw_mod_contact_cinput2' . $this->_uniqueid . ']").change(function() {';
            $javascript[] = '   window.localStorage.setItem("cw_mod_contact_cinput2' . $this->_uniqueid . '", $cw(this).val());';
            $javascript[] = '   });';
        }

        if ($myparams['displayCInput3'] == 'Y' || $myparams['displayCInput3'] == 'R' && $myparams['typeCInput3'] === 'select') {
            $javascript[] = '   //Keep Custom 3 selection on page reload ';
            $javascript[] = '   var item = window.localStorage.getItem("cw_mod_contact_cinput3' . $this->_uniqueid . '");';
            $javascript[] = '   $cw("select[name=cw_mod_contact_cinput3' . $this->_uniqueid . ']").val(item);';
            $javascript[] = '   $cw("select[name=cw_mod_contact_cinput3' . $this->_uniqueid . ']").change(function() {';
            $javascript[] = '   window.localStorage.setItem("cw_mod_contact_cinput3' . $this->_uniqueid . '", $cw(this).val());';
            $javascript[] = '   });';
        }

        $javascript[] = '});';
        return implode("\n", $javascript);
    }

    //Google map
    private function _getMap() {
        $doc = JFactory::getDocument();
        $myparams = $this->_params;

        $mapZoom = $myparams['mapZoom'];
        $mapLong = $myparams['mapLong'];
        $mapLat = $myparams['mapLat'];
        $mapTitle = $myparams['mapTitle'];
        $mapPopupWidth = $myparams['mapPopupWidth'];

        //Map css
        $mapCss = '#map-canvas' . $this->_uniqueid . ' {min-height: ' . $myparams['mapHeight'] . 'px;}';
        $doc->addStyleDeclaration($mapCss);

        //Popup text
        $desc[] = '<div>';
        $desc[] = '<h1>' . $mapTitle . '</h1>';
        $desc[] = '<p>' . $myparams['mapText'] . '</p>';
        $desc[] = '</div>';
        $text = (implode(" ", $desc));

        //Lets add our scripts
        $doc->addScript("https://maps.googleapis.com/maps/api/js?v=3.exp");

        $mapjs [] = '//CoalaWeb Contact Map';
        $mapjs [] = 'jQuery(document).ready(function (){';
        $mapjs [] = '  function initialize() {';
        $mapjs [] = '      var myLatlng = new google.maps.LatLng(' . $mapLat . ',' . $mapLong . ');';
        $mapjs [] = '      var mapOptions = {';
        $mapjs [] = '          zoom: ' . $mapZoom . ',';
        $mapjs [] = '          center: myLatlng';
        $mapjs [] = '      };';
        $mapjs [] = '  var map = new google.maps.Map(document.getElementById("map-canvas' . $this->_uniqueid . '"), mapOptions);';
        $mapjs [] = '  var contentString = "' . $text . '";';
        $mapjs [] = '  var infowindow = new google.maps.InfoWindow({';
        $mapjs [] = '      content: contentString,';
        $mapjs [] = '      maxWidth: "' . $mapPopupWidth . '"';
        $mapjs [] = '  });';
        $mapjs [] = '  var marker = new google.maps.Marker({';
        $mapjs [] = '      position: myLatlng,';
        $mapjs [] = '      map: map,';
        $mapjs [] = '      title: "' . $mapTitle . '"';
        $mapjs [] = '  });';
        $mapjs [] = '  google.maps.event.addListener(marker, "click", function() {';
        $mapjs [] = '      infowindow.open(map,marker);';
        $mapjs [] = '  });';
        $mapjs [] = '}';
        $mapjs [] = '  google.maps.event.addDomListener(window, "load", initialize);';
        $mapjs [] = '});';

        return implode("\n", $mapjs);
    }

    private function _cleanCode($cjsCode) {

        // Remove comments.
        $pass1 = preg_replace('~//<!\[CDATA\[\s*|\s*//\]\]>~', '', $cjsCode);
        $pass2 = preg_replace('/(?:(?:\/\*(?:[^*]|(?:\*+[^*\/]))*\*+\/)|(?:(?<!\:|\\\)\/\/[^"\'].*))/', '', $pass1);

        // Minimize.
        $pass3 = str_replace(array("\r\n", "\r", "\n", "\t"), '', $pass2);
        $pass4 = preg_replace('/ +/', ' ', $pass3); // Replace multiple spaces with single space.
        $cjsClean = trim($pass4);  // Trim the string of leading and trailing space. 

        return $cjsClean;
    }

}

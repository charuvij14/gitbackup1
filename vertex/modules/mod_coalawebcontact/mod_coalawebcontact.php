<?php

defined('_JEXEC') or die('Restricted access');
/**
 * @package             Joomla
 * @subpackage          CoalaWeb Contact Module
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Contact is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
jimport('joomla.application.component.helper');
jimport('joomla.utilities.date');
jimport('joomla.filesystem.file');
    
// Include the syndicate functions only once
require_once dirname(__FILE__) . '/helper.php';

$doc = JFactory::getDocument();
$app = JFactory::getApplication();
$lang = JFactory::getLanguage();

//Load the module language strings
if ($lang->getTag() != 'en-GB') {
    $lang->load('mod_coalawebcontact', JPATH_SITE, 'en-GB');
}
$lang->load('mod_coalawebcontact', JPATH_SITE, null, 1);

//Load the component language strings
if ($lang->getTag() != 'en-GB') {
    $lang->load('com_coalawebcontact', JPATH_ADMINISTRATOR, 'en-GB');
}
$lang->load('com_coalawebcontact', JPATH_ADMINISTRATOR, null, 1);

// Load version.php
$version_php = JPATH_ADMINISTRATOR . '/components/com_coalawebcontact/version.php';
if (!defined('COM_CWCONTACT_VERSION') && JFile::exists($version_php)) {
    require_once $version_php;
}

//Keeping the parameters in the component keeps things clean and tidy.
$comParams = JComponentHelper::getParams('com_coalawebcontact');

//Stop conflicts with a unique ID
$uniqueId = '-cwcmod-' . $module->id;

//Lets get help and params from our helper
$helper = new modCoalawebContactHelper($uniqueId, $params, $comParams);
$myparams = $helper->_params;

// Load crypt class if needed
if ($myparams['whichCaptcha'] == 'image' || $myparams['recipients'] === '2') {
    //Include our Crypt class
    if(!class_exists('CryptHelper')){
        include_once JPATH_ADMINISTRATOR . '/components/com_coalawebcontact/helpers/crypt.php';
    }
    $crypt = new CryptHelper();
}

//Email cloak if the module is loaded into content
$cloak = JPluginHelper::isEnabled('content', 'emailcloak');
$cloakOn = $myparams['cloakOn'];

//Image captcha
if ($myparams['whichCaptcha'] == 'image') {
    $code = $crypt->str_encrypt($crypt->generateCode(5));
}

// Recaptcha
if ($myparams['whichCaptcha'] == 'recaptcha') {
    JPluginHelper::importPlugin('captcha');
    $dispatcher = JEventDispatcher::getInstance();
    $dispatcher->trigger('onInit', 'dynamic_recaptcha_'. $uniqueId);
}

/* Redirect Url */
$samePageUrl = JURI::getInstance()->toString();
$sucessUrl = $helper->getRedirect($samePageUrl);

//If Pro add from URL to email
if (COM_CWCONTACT_PRO == 1) {
    $fromUrl = $samePageUrl;
} else {
    $fromUrl = '';
}

if ($myparams['recipients'] === '1') {
    $checkRecipient = ($myparams['recipient']);
} else {
    $checkRecipient = ($myparams['depOptions']);
}

//Get input data from the form
$input = JFactory::getApplication()->input;
$subject = $input->post->get('cw_mod_contact_subject' . $uniqueId, '', 'STRING');
$name = $input->post->get('cw_mod_contact_name' . $uniqueId, '', 'STRING');
$email = $input->post->get('cw_mod_contact_email' . $uniqueId, '', 'STRING');
$message = $input->post->get('cw_mod_contact_message' . $uniqueId, '', 'HTML');
$capEnter = $input->post->get('cw_mod_contact_captcha' . $uniqueId, '', 'STRING');
$capCheck = $input->post->get('cw_mod_contact_captcha_check' . $uniqueId, '', 'STRING');
$copyme = $input->post->get('cw_mod_contact_copyme' . $uniqueId, '', 'INT');
$calTo = $input->post->get('cw_mod_contact_cal_to' . $uniqueId, '', 'STRING');
$calFrom = $input->post->get('cw_mod_contact_cal_from' . $uniqueId, '', 'STRING');
$tos = $input->post->get('cw_mod_contact_tos' . $uniqueId, '', 'INT');

if (COM_CWCONTACT_PRO == 1) {
    $remoteHost = $input->server->get('REMOTE_ADDR');
} else {
    $remoteHost = '';
}

$cInput1 = $input->post->get('cw_mod_contact_cinput1' . $uniqueId, '', 'STRING');
$cInput2 = $input->post->get('cw_mod_contact_cinput2' . $uniqueId, '', 'STRING');
$cInput3 = $input->post->get('cw_mod_contact_cinput3' . $uniqueId, '', 'STRING');
$submitted = $input->post->get('cw_mod_contact_sub' . $uniqueId, '', 'STRING');

if ($myparams['recipients'] === '2') {
//Decrypt the depart email address
    $department = $input->post->get('cw_mod_contact_dep' . $uniqueId, '', 'STRING');
    $dep = $crypt->str_decrypt($department);
}

// Array of input data
$fields = [
    'subject' => $subject,
    'name' => $name,
    'email' => $email,
    'message' => $message,
    'calTo' => $calTo,
    'calFrom' => $calFrom,
    'tos' => $tos,
    'remoteHost' => $remoteHost,
    'cInput1' => $cInput1,
    'cInput2' => $cInput2,
    'cInput3' => $cInput3
];

//Add departments to the check if needed
if ($myparams['recipients'] === '2') {
        $fields['dep'] = $dep;
}

if ($submitted) {

    //Lets do some server side php validation!
    foreach ($fields as $key => $value) {

        //We don't need to check the Remote Host.
        if ($key == 'remoteHost') {
            continue;
        }

        //Here we are using some nifty variable variables.
        $funcName = 'check' . ucfirst($key);
        ${$key . 'Check'} = $helper->$funcName($$key);
        if (${$key . 'Check'}['hasError']) {
            $hasError = true;
        }
        ${$key . 'Error'} = ${$key . 'Check'}[$key . 'Error'];
    }

    switch ($myparams['whichCaptcha']) {
        case "basic":
            if ($capEnter != $myparams['bCaptchaAnswer']) {
                $captchaError = $myparams['msgCaptchaWrong'];
                $hasError = true;
            } else {
                $captcha = $capEnter;
            }
            break;

        case "image":
            $capCompare = $crypt->str_decrypt($capCheck);
            if ($capEnter != $capCompare) {
                $captchaError = $myparams['msgCaptchaWrong'];
                $hasError = true;
            } else {
                $captcha = $capEnter;
            }
            break;

        case "recaptcha":
            // get the response, 'code' isn't actually used
            $resp = $dispatcher->trigger('onCheckAnswer', 'code');
            if (!$resp[0]) {
                $captchaError = $myparams['msgCaptchaWrong'];
                $hasError = true;
            } else {
                $resp = $resp;
            }
            break;
    }


    /*
     * No Errors? Nice lets start sending the mail.
     */
    if (!isset($hasError)) {
        $mail = JFactory::getMailer();
        $config = JFactory::getConfig();

        if ($myparams['mailFromOpt'] === '1') {
            $sender = array($email, $name);
        } else {
            $sender = $myparams['mailFrom'];
        }

        $mail->setSender($sender);
        
        //Add our recipient
        if ($myparams['recipients'] === '1') {
            $mail->addRecipient($myparams['recipient']);
        } else {
            $mail->addRecipient($dep);
        }

        if ($myparams['emailCc']) {
            $mail->addCC($myparams['emailCc']);
        }
        if ($myparams['emailBcc']) {
            $mail->addBCC($myparams['emailBcc']);
        }

        //Setup our subject.
        $extSubject = $subject ? $myparams['emailSubject']. ': ' . $subject : $myparams['emailSubject'];

        //Format our TOS answer.
        $tosAnswer = isset($tos) ? JTEXT::_('CW_YES') : JTEXT::_('CW_NO');

        //Assign our Subject
        $mail->setSubject($extSubject);

        //Build the email body
        $body = $helper->emailBody($fields, $extSubject, $fromUrl, $tosAnswer);
        
        //Build the email copyme body
        $copymeBody = $helper->copymeBody($fields, $extSubject, $fromUrl, $tosAnswer);

        if ($myparams['emailFormat'] === 'nohtml') {
            $mail->Encoding = 'base64';
            $mail->IsHTML(false);
            $mail->setBody($body);
        } else {
            $mail->Encoding = 'base64';
            $mail->IsHTML(true);
            $mail->setBody(implode("\n", $body));
        }

        //Finally send it!
        $send = $mail->Send();

        //If copy me was selected lets send that too.
        if ($copyme && $copyme == '1') {
            $mail->ClearAllRecipients();
            $mail->addRecipient($email);
            if ($myparams['emailFormat'] === 'nohtml') {
                $mail->Encoding = 'base64';
                $mail->IsHTML(false);
                $mail->setBody($copymeBody);
            } else {
                $mail->Encoding = 'base64';
                $mail->IsHTML(true);
                $mail->setBody(implode("\n", $copymeBody));
            }
            $send = $mail->send();
        }
        
        //If all went well.
        $emailSent = true;
    }
}

require JModuleHelper::getLayoutPath('mod_coalawebcontact', $params->get('layout', 'default'));

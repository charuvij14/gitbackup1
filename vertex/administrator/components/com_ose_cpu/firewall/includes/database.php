<?php
/**
 * @version     6.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @author        Updated on 30-Mar-2013 
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
if (!defined('_JEXEC') && !defined('OSE_ADMINPATH')) {
	die("Direct Access Not Allowed");
}
/* OSE Database Object */
class oseDBO {
	public function __construct() {
	}
	public static function instance($type = null) {
		static $instance;
		if (empty ($instance)) {
			if (defined('OSE_ADMINPATH') || defined('OSE_CMS_ADMIN_PATH')) {
				require_once (OSECPU_LIB_PATH . OSEDS . 'database' . OSEDS . 'OSEDatabaseMySQL.php');
				if (class_exists('SConfig'))
				{
					$config = new SConfig();
				}
				else
				{
					$config = new JConfig();
				}
				$options['host'] = $config->host;
				$options['user'] = $config->user;
				$options['password'] = $config->password;
				$options['database'] = $config->db;
				$options['prefix'] = $config->dbprefix;
				$options['select'] = true;
				unset ($config);
				$instance = new OSEDatabaseMySQL($options);
			}
		}
		return $instance;
	}
	public static function loadList($type = 'array', $key = null) {
		$db = oseDB :: instance();
		switch ($type) {
			case ('obj') :
				return $db->loadObjectList($key);
				break;
			default :
				return $db->loadAssocList($key);
				break;
		}
	}
	public static function loadItem($type = 'array', $key = null) {
		$db = oseDB :: instance();
		switch ($type) {
			case ('obj') :
				return $db->loadObject();
				break;
			default :
				return $db->loadAssoc();
				break;
		}
	}
	public function query($unlock = false) {
		$testMode = 1;
		$db = oseDB :: instance();
		if (!$db->query()) {
			if ($testMode == 1) {
				oseExit($db->getErrorMsg());
			}
			if ($unlock) {
				oseDB :: unlock();
			}
			return false;
		}
		return true;
	}
	public static function lock($query) {
		$db = oseDB :: instance();
		$db->setQuery("LOCK TABLE " . $query);
		return $db->query();
	}
	public static function unlock() {
		$db = oseDB :: instance();
		$query = "UNLOCK TABLES";
		$db->setQuery($query);
		return $db->query();
	}
}
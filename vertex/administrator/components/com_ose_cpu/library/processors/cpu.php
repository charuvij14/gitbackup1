<?php
/**
 * @version     6.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @author        Updated on 30-Mar-2013 
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
if (!defined('_JEXEC') && !defined('OSE_ADMINPATH')) {
	die('Direct Access Not Allowed');
}
if (!defined('DS')) {
	define('DS', DIRECTORY_SEPARATOR);
}
if (!class_exists('oseJSON'))
{
	require_once(dirname(dirname(__FILE__)).DS.'json'.DS.'oseJSON.php');
}
class oseCPU {
	protected $cms = null, $isAdmin=false;
	public function __construct() {
		$this->setCMS();
		$this->checkAdminAccess();
	}
	private function setCMS()
	{
		if (class_exists('JVersion'))
		{
			$this->cms = 'joomla';
		}
		else
		{
			$this->cms = 'wordpress';
		}
	}
	public function getCMS()
	{
		$cms = (string)$this->cms; 
		return $cms; 
	}
	public function getIsAdmin()
	{
		return $this->isAdmin;
	}
	private function checkAdminAccess()
	{
		if (class_exists('JVersion'))
		{
			$this->callLibClass('users', 'oseUsers');
			$oseUsers = new oseUsers($this->getCMS()); 
			$admin_groups = $oseUsers->getJoomlaAdminGroups(); 
			$user_groups = $oseUsers->getJoomlaUserGroups();
			if (count(array_intersect($user_groups, $admin_groups))>0)
			{
				$access=  true;
			}
			else
			{
				$access=  false;
			}
			$this->isAdmin = $access;
		}
		else
		{
			$this->cms = 'wordpress';
		}
	}
	public function callLibClass($folder, $classname)
	{
		require_once (OSECPU_LIB_PATH.DS.$folder . DS . $classname.'.php');
	}
	public function stylesheet($filename)
	{
		if ($this->cms=='joomla')
		{
			JHTML :: stylesheet($filename);
		}
	}
	public function script($filename)
	{
		if ($this->cms=='joomla')
		{
			JHTML :: script($filename);
		}
	}
	// Static methods; 
	public static function returnJSON ($var)
	{
		print_r(oseJSON::encode($var)); exit; 
	}
	
	public static function ajaxResponse($status, $message, $result=false)
	{
		$return['success'] = $result; 
		$return['status'] = $status;
		$return['result'] = $message;
		echo oseJSON::encode($return);
		exit;
	}
	public function getVar($name, $default)
	{
		if ($this->cms=='joomla')
		{
			return JRequest::getVar($name, $default);	
		}	
	}
	public function getInt($name, $default=null)
	{
		if ($this->cms=='joomla')
		{
			return JRequest::getInt($name, $default);	
		}	
	}
	public function getHTML($name, $default)
	{
		if ($this->cms=='joomla')
		{
			return JRequest::getVar($name, $default,'post','string', JREQUEST_ALLOWHTML);
		}	
	}
	
}	
<?php
/**
 * @version     6.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @author        Updated on 30-Mar-2013 
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
{
	die("Direct Access Not Allowed");
}
class oseFileScan {
	var $oseFile = null;
	var $db = null;
	public function __construct() {
		$this->db = JFactory :: getDBO();
	}
	
	private function getSQLCondition ($exts, $start, $limit= 150, $exclude= array())
	{
		if(!empty($exts)) {
			if (!is_array($exts))
			{
				$exts = oseJSON::decode($exts);
			}
			$exts= "('".implode("','", $exts)."')";
		}
		$exclude=(!empty($exclude)) ? " AND filepath NOT IN ('".implode("','", $exclude)."')" : '';
		$limit= empty($limit) ? '' : " LIMIT {$start}, {$limit}";
		return "`ext` IN {$exts} ".$exclude.$limit;
	} 
	
	public function getCountItemsFromDB($exts, $start, $limit= 150, $exclude= array())
	{
		$condition = $this->getSQLCondition ($exts, $start, $limit= 150, $exclude);
		$query= "SELECT COUNT(id) FROM `#__oseav_scanitems`  WHERE ". $condition; 
		$this->db->setQuery($query);
		$result= $this->db->loadResult();
		return $result;
	}
	
	public function getItemsFromDB($exts, $start, $limit= 150, $exclude= array()) {
		$condition = $this->getSQLCondition ($exts, $start, $limit= 150, $exclude);
		$query= "SELECT `id`, `filepath` FROM `#__oseav_scanitems`  WHERE `state`= 0 AND ". $condition; 
		$this->db->setQuery($query);
		$results= $this->db->loadObjectList();
		return $results;
	}
	
	function showcurrent() {
		if(isset($_SESSION['current'])) {
			echo $_SESSION['current'];
		}
	}
	public static function _in_arrayi($needle, $haystack) {
		if(empty($haystack)) {
			return;
		}
		foreach($haystack as $value) {
			if(strtolower($value) == strtolower($needle)) {
				return true;
			}
		}
		return false;
	}

	public function getTotal()
	{
		$db = JFactory::getDBO();
		$query = "SELECT count(*) as total FROM `#__oseav_scanitems` WHERE `ext` = ''";
		$db->setQuery($query);
		$total['num_folder'] = $db->loadResult();
		$query = "SELECT count(*) as total FROM `#__oseav_scanitems` WHERE `ext` = '' AND fperms >755";
		$db->setQuery($query);
		$total['num_folder_inc'] = $db->loadResult();
		$query = "SELECT count(*) as total FROM `#__oseav_scanitems` WHERE `ext` != ''";
		$db->setQuery($query);
		$total['num_file'] = $db->loadResult();
		$query = "SELECT count(*) as total FROM `#__oseav_scanitems` WHERE `ext` != '' AND fperms >644";
		$db->setQuery($query);
		$total['num_file_inc'] = $db->loadResult();
		$virusscan = oseRegistry::call('virusscan');
		$total['num_file_vsscan'] = $virusscan->getStat()->getTotalScanItems();
		return $total;
	}

	function getFixTotal()
	{
		$db = &JFactory::getDBO();
		$query = " SELECT count(*) as total FROM `#__oseav_scanitems` " . " WHERE (ext = '' AND fperms > 755)";
		$db->setQuery($query);
		$total['num_file'] = $db->loadResult();
		$query = " SELECT count(*) as total FROM `#__oseav_scanitems` " . " WHERE (ext != '' AND fperms > 644)";
		$db->setQuery($query);
		$total['num_folder'] = $db->loadResult();
		return $total;
	}

	function getScanPath()
	{
		$db = &JFactory::getDBO();
		$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` ='filescanPath' ";
		$db->setQuery($query);
		$scan_path = $db->loadResult();
		return $scan_path;
	}

	function updatePath()
	{
		$scan_path = JRequest::getVar("scan_path");
		$db = &JFactory::getDBO();
		$query = "CREATE TABLE IF NOT EXISTS `#__ose_secConfig` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `key` text NOT NULL,
				  `value` text NOT NULL,
				  PRIMARY KEY (`id`)
				) AUTO_INCREMENT=1 ;";
		$db->setQuery($query);
		$db->query();
		$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` ='fileScanpath' ";
		$db->setQuery($query);
		$result = $db->loadResult();
		if (empty($result)) {
			$query = "INSERT INTO `#__ose_secConfig` (`id`, `key`, `value`) VALUES
					(null, 'fileScanpath', '" . $db->getEscaped($scan_path) . "');";
		}
		else {
			$query = "UPDATE `#__ose_secConfig` SET `value` = '" . $db->getEscaped($scan_path) . "' WHERE `key` = 'fileScanpath';";
		}
		$db->setQuery($query);
		if ($db->query()) {
			echo $scan_path;
		}
		else {
			echo JText::_("ERROR");
		}
		exit;
	}

	function initdb($path = null)
	{
		if (empty($path)) {
			return "Scan Path not defined";
		}
		else {
			$db = JFactory::getDBO();
			$query = "TRUNCATE TABLE `#__oseav_scanitems`";
			$db->setQuery($query);
			if (!$db->query()) {
				oseExit('Mysql Query Run Error!');
			}
			$selectedItems = oseJSON::decode($path);
			foreach ($selectedItems as $selectedItem) {
				if (is_file($selectedItem)) {
					$ext = $this->oseFile->getExt($selectedItem);
					if (!$this->insert($selectedItem, $ext)) {
						$return['status'] = "Error";
						$return['error'] = JText::_("Inserting the file path to the database failed.");
						$return = oseJSON::encode($return);
						echo $return;
						exit;
					}
				}
				else {
					if (!$this->dirList($selectedItem, true)) {
						return false;
					}
				}
			}
			return true;
		}
	}

	function chkPerms($start, $limit = 300, $total)
	{
		//set_time_limit(0);
		$db = &JFactory::getDBO();
		$query = "SELECT `filepath`, `id`  FROM `#__oseav_scanitems` LIMIT {$start}, {$limit}";
		$db->setQuery($query);
		$objs = $db->loadObjectList();
		$result = array();
		foreach ($objs as $obj) {
			$fileperms = substr(decoct(fileperms($obj->filepath)), 2);
			$query = " UPDATE `#__oseav_scanitems` SET `fperms` = '{$fileperms}' " . " WHERE `id` = {$obj->id} ";
			$db->setQuery($query);
			if (!$db->query()) {
				return false;
			}
		}
		$result['total'] = $start + count($objs);
		$fixtotal = self::getFixTotal();
		$result['num_file'] = $fixtotal['num_file'];
		$result['num_folder'] = $fixtotal['num_folder'];
		if ($result['total'] >= $total) {
			$result['status'] = 'Done';
			return $result;
		}
		else {
			$result['status'] = 'Continue';
			return $result;
		}
	}

	function fixPerms($start, $limit = 300, $total)
	{
		//set_time_limit(0);
		$db = &JFactory::getDBO();
		$query = " SELECT `filepath`, `id`, `ext`  FROM `#__oseav_scanitems` " . " WHERE (ext = ''  AND fperms > 755) OR (ext != ''  AND fperms > 644) LIMIT 0, {$limit}";
		$db->setQuery($query);
		$objs = $db->loadObjectList();
		$result = array();
		foreach ($objs as $obj) {
			if ($obj->ext == '') {
				chmod($obj->filepath, 0755);
			}
			else {
				chmod($obj->filepath, 0644);
			}
			$fileperms = substr(decoct(fileperms($obj->filepath)), 2);
			$query = " UPDATE `#__oseav_scanitems` SET `fperms` = '{$fileperms}' " . " WHERE `id` = '{$obj->id}' ";
			$db->setQuery($query);
			$db->query();
		}
		$fixtotal = self::getFixTotal();
		$result['num_file'] = $fixtotal['num_file'];
		$result['num_folder'] = $fixtotal['num_folder'];
		$result['total'] = $result['num_file'] + $result['num_folder'];
		if ($result['total'] == 0) {
			$result['status'] = 'Done';
			return $result;
		}
		else {
			$result['status'] = 'Continue';
			return $result;
		}
	}

	function dirList($directory, $recurse, $extensions = array(), $excDirs = array())
	{
		set_time_limit(0);
		$count = 0;
		$countSpeed = 50;
		$files = array();
		// add the default csv and .svn
		$extensions[] = '.svn';
		$extensions[] = 'CSV';
		$extensions[] = '';
		$oseFolder = self::getFolder();
		$return = array();
		$return['error'] = null;
		if ($oseFolder->isPathReadable($directory)) {
			$folders = $oseFolder->getFoldersFromDirectory($directory, '.', $recurse, true, $excDirs);
			array_push($folders, $directory);
			foreach ($folders as $folder) {
				if ($folder != $directory) {
					$this->insert($folder, '');
				}
				$files = $oseFolder->getFilesFromFolder($folder, '.', false, true);
				foreach ($files as $file) {
					$ext = $this->oseFile->getExt($file);
					if (!$this->insert($file, $ext)) {
						$return['status'] = "Error";
						$return['error'] = JText::_("Inserting the file path to the database failed.");
						$return = oseJSON::encode($return);
						echo $return;
						exit;
					}
					else {
						//$return['error'].= $file."<br />";
						if (((++$count) % $countSpeed) == 0) {
							//$return['status']= "Continue";
							//$return['error']= $count.' Files Processed. Continue...<br />';
							//$return= oseJSON :: encode($return);
							echo $count . ' Files Processed. Continue...<br />';
							exit;
							//exit;
						}
					}
				}
			}
			return true;
		}
		else {
			$return['status'] = "Error";
			$return['error'] = JText::_("File path") . " " . $directory . " " . JText::_("is not readable");
			$return = oseJSON::encode($return);
			echo $return;
			exit;
		}
	}

	function getChildPath($path)
	{
		$oseFolder = self::getFolder();
		$folders = $oseFolder->getFoldersFromDirectory($path, '.', 0, true, array());
		$return = null;
		foreach ($folders as $id => $value) {
			$value = str_replace("//", "/", $value);
			$return .= '<input type="checkbox" name="subDir" value="' . $value . '"> ' . $value . "<br />";
		}
		return $return;
	}

	function insert($filepath, $ext)
	{
		$db = JFactory::getDBO();
		$utf8_filepath = utf8_encode($filepath);
		$filesize = "";
		$filetime = "";
		$hash = "";
		$sqlfiletime = "";
		// Clean the file patch again;
		$filepath = addslashes($filepath);
		$filepath = str_replace("//", "/", $filepath);
		$query = "Start Transaction";
		$db->setQuery($query);
		$db->Query();
		$query = " INSERT INTO `#__oseav_scanitems` (filepath, md5, state, datetime, filesize, new_md5, new_datetime, new_filesize, date_added, date_checked, ext)" . " values ('"
				. $filepath . "', '$hash', '1', '$sqlfiletime', " . intval($filesize) . ", '', 0, 0, NOW(), 0, '{$ext}')";
		$db->setQuery($query);
		if ($db->query()) {
			$_SESSION['osecount']++;
			$return = true;
		}
		else {
			$return = false;
		}
		$query = "Commit";
		$db->setQuery($query);
		$db->Query();
		return $return;
	}

	function initFileScan($etext, $virus_count, $scanDir, $excDirs = array(), $countSpeed = 50)
	{
		$logfilename = "oseav_" . date("y-m-d") . ".log";
		$curPath = dirname(__FILE__);
		$oseAVLogPath = $curPath . DS . $logfilename;
		$error_count = 0;
		
		if (file_exists($oseAVLogPath)) {
			unlink($oseAVLogPath);
		}
		if (!version_compare(phpversion(), "4.2.0", ">=")) {
			$error_count++;
			return;
		}
		$numIncDirs = count($scanDir);
		$result = array();
		$statusQuery = null;
		$resultQuery = null;
		$infectedNum = 0;
		$fileScan = oseRegistry::call('filescan');
		$virScan = oseRegistry::call('virusscan');
		$exts = $virScan->getScanExt();
		$oseFolder = $fileScan->getFolder();
		$oseFile = $fileScan->getFile(); 
		$stat = $virScan->getStat();
		$stat->truncateDetected();
		$infectedNum = 0;
		$count = 0;
		$db = JFactory::getDBO();
		// Cleant the database;
		$query = "TRUNCATE TABLE `#__oseav_scanitems`";
		$db->setQuery($query);
		if (!$db->query()) {
			$error_count++;
			oseExit('Mysql Query Run Error!');
		}
		// process all the directories
		echo '<div id="init-content-panel">' . JText::_('OSE Virus Scanning Started') . '<br/>';
		$_SESSION['osecount'] = 0;
		for ($i = 0; $i < $numIncDirs; $i++) {
			if (!isset($scanDir[$i]))
			{
				continue; 
			}
			if (is_dir($scanDir[$i])) {
				 
				$this->initDirList($scanDir[$i], 'S', $exts, $excDirs, $_SESSION['osecount']);
			}
			else
			{
				$ext = $oseFile->getExt($scanDir[$i]);
				if ((empty($exts)) or ($this->_in_arrayi($ext, $exts))) {
					$filename = $scanDir[$i];
					if (!$this->insert($scanDir[$i], $ext)) {
						$return['status'] = "Error";
						$return['error'] = JText::_("Inserting the file path to the database failed.");
						$return = oseJSON::encode($return);
						echo $return;
						exit;
					}
				}
			}	
		}
		//mysql_close();
		echo '<script>window.scrollBy(0,5000);</script>';
		echo "<script>setTimeout('self.close ()',10000);</script>";
		echo '<script>window.scrollBy(0,200);</script>';
		echo "<script>Ext.Msg.wait('You are now redirected back to the Virus Scanner section.', 'Please Wait...');</script>";
		//echo "<script>sleep(2000);</script>";
		echo "<script>window.location = 'index.php?option=com_ose_antivirus';</script>";
	}

	function initDirList($directory, $recurse, $extensions, $excDirs, $count, $countSpeed = 25)
	{
		$db = JFactory::getDBO();
		$results = array();
		$fileScan = oseRegistry::call('filescan');
		$oseFile = $fileScan->getFile();
		if ($this->_in_arrayi($directory, $excDirs)) {
			return $results;
		}
		if (is_dir($directory) && $handle = opendir($directory)) {
			while ($filename = readdir($handle)) {
				if ($filename != "." && $filename != "..") {
					if (is_dir($directory . DS . $filename)) {
						if (strstr($directory . DS . $filename, '.svn')) {
							continue;
						}
						else {
							if ($recurse == "S") {
								$this->initDirList($directory . DS . $filename, $recurse, $extensions, $excDirs, $_SESSION['osecount']);
							}
						}
					}
					else {
						$ext = $oseFile->getExt($filename);
						if ((empty($extensions)) or ($this->_in_arrayi($ext, $extensions))) {
							$filename = $directory . DS . $filename;
							if (!$this->insert($filename, $ext)) {
								$return['status'] = "Error";
								$return['error'] = JText::_("Inserting the file path to the database failed.");
								$return = oseJSON::encode($return);
								echo $return;
								exit;
							}
						}
					}
					if ($_SESSION['osecount'] != 0 && ($_SESSION['osecount'] % $countSpeed) == 0) {
						if (is_dir($directory . DS . $filename)) {
							echo JText::_("Last Scanned folder:") . ' ' . $directory . DS . $filename . '<br/>';
						}
						else {
							echo JText::_("Last Scanned file:") . ' ' . $filename . '<br/>';
						}
						echo $_SESSION['osecount'] . ' Files Processed. Continue...<br />';
						echo '<script> window.scrollBy(0,5000);</script>';
					}
				}
			}
			closedir($handle);
		}
		return $results;
	}
}
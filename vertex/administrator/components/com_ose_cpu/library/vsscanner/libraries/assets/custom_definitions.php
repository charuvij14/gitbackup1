	<?php
	if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
	{
		die("Direct Access Not Allowed");
	}
	class oseCSVirusDefinitions
	{
		var $signatures =array();
		var $patterns =array();
		function __construct()
		{
		}
		function setPatterns()
		{
		$exp = array();
		
		
		$exp[] = array( "[00000] Custom Pattern" , '/<\?php(?s)\s*if\s*\(\!isset\(.*?HTTP_USER_AGENT.*?REMOTE_ADDR.*?HTTP_HOST.*?base64_decode\(.*?urlencode\(.*?curl_close\(.*?\?>/');
		$exp[] = array( "[00000] Custom Pattern" , '#<\?php(?s)\s*if\s*\(\!isset\(.*?HTTP_USER_AGENT.*?REMOTE_ADDR.*?HTTP_HOST.*?base64_decode\(.*?urlencode\(.*?curl_close\(.*?\?>#');
		$exp[] = array( "[0000*] Custom Pattern" , '/<script(?s).*?var.*?Array.*?length.*?while.*?String.fromCharCode.*?document.write.*?<\/script>/');
		$exp[] = array( "[0000*] Custom Pattern" , '#<script(?s).*?var.*?Array.*?length.*?while.*?String.fromCharCode.*?document.write.*?<\/script>#');
		$exp[] = array( "[00000] Custom Pattern" , '/<\?php\s*if\(isset\([$].*?\)\)\s*\{\s*eval\(base64_decode\(.*?\)\).*?else\s*\{\s*die\(.*?\);\s*}\?>/');
		$exp[] = array( "[00000] Custom Pattern" , '#<\?php\s*if\(isset\([$].*?\)\)\s*\{\s*eval\(base64_decode\(.*?\)\).*?else\s*\{\s*die\(.*?\);\s*}\?>#');
	
		
		
		//$exp[]= array( "[F3009] iFrame Injection" , "/;var\s*_1O0.*?fromCharCode.*?function.*?o1.*?if.*?.*?eval\(.*?\)\);/");
		//$exp[]= array( "[F3009] iFrame Injection" , "#;var\s*_1O0.*?fromCharCode.*?function.*?o1.*?if.*?.*?eval\(.*?\)\);#");
		//$exp[]= array( "[J1021] iFrame Injection" , "/echo\s*\".*?<script.*?String\.fromCharCode.*?replace.*?split.*?try.*?catch.*?eval.*?window\.document.*?\/script>.*?\";/");
		//$exp[]= array( "[J1021] iFrame Injection" , "#echo\s*\".*?<script.*?String\.fromCharCode.*?replace.*?split.*?try.*?catch.*?eval.*?window\.document.*?\/script>.*?\";#");
		//$exp[]= array( "[J1021] iFrame Injection" , "/echo.*?[\"|\'].*?<script.*?split.*?eval.*?document.*?parseInt.*?catch.*?fromCharCode.*?<\/script>.*?[\"|\'].*?;/");
		//$exp[]= array( "[J1021] iFrame Injection" , "#echo.*?[\"|\'].*?<script.*?split.*?eval.*?document.*?parseInt.*?catch.*?fromCharCode.*?<\/script>.*?[\"|\'].*?;#");
		//$exp[]= array( "[J1022] iFrame Injection" , "/<script.*?split.*?eval.*?document.*?parseInt.*?catch.*?fromCharCode.*?<\/script>/");
		//$exp[]= array( "[J1022] iFrame Injection" , "#<script.*?split.*?eval.*?document.*?parseInt.*?catch.*?fromCharCode.*?<\/script>#");
		//$exp[]= array( "[F3009] PHP Injection" , "/[$].*?if\(isset\(.*?eval\(.*?fopen\(.*?fwrite\(.*?fclose\(.*?;\s*}/");
		//$exp[]= array( "[F3009] JS Injection" , "/echo.*?\".*?<script.*?split.*?fromCharCode.*?try[{]document.*?catch\(.*?eval.*?<\/script>.*?\".*?;/");
		//$exp[]= array( "[F3009] JS Injection" , "#echo.*?\".*?<script.*?split.*?fromCharCode.*?try[{]document.*?catch\(.*?eval.*?<\/script>.*?\".*?;#");
		//$exp[]= array( "[F3009] JS Injection" , "/echo.*?\".*?<script.*?split.*?eval.*?try.*?catch.*?length.*?fromCharCode.*?<\/script>\";/");
		//$exp[]= array( "[F3009] JS Injection" , "#echo.*?\".*?<script.*?split.*?eval.*?try.*?catch.*?length.*?fromCharCode.*?<\/script>\";#");
		//$exp[]= array( "[F3009] JS Injection" , "/ps.*?split.*?eval.*?catch.*?\"doc\".\"ument\".*?length.*?fromCharCode.*;}/");
		//$exp[]= array( "[F3009] JS Injection" , "#ps.*?split.*?eval.*?catch.*?\"doc\".\"ument\".*?length.*?fromCharCode.*;}#");
		//$exp[]= array( "[00000] Custom Pattern" , '/ument.*?catch.*?fromCh.*?String.*?eval.*?try.*?catch.*?arCode.*?/');
		//$exp[]= array( "[00000] Custom Pattern" , '#ument.*?catch.*?fromCh.*?String.*?eval.*?try.*?catch.*?arCode.*?#');
		/*
		$exp[]= array( "[00000] Custom Pattern" , '/<html>(?s).*?<head>.*?<style.*?overflow.*?hidden.*?<\/style>.*?</head>.*?<script.*?document.write\(.*?\).*?<\/script>.*?<body><iframe.*?src=.*?(\.com|\.cgi|\.ru|\.fr|\.info|\.org|\.us|\.pl|\.net|\.php|\.cfg|\.pip|\.txt|\.html|\?).*?><\/iframe>.*?<\/body>.*?<\/html>/');
		$exp[]= array( "[00000] Custom Pattern" , '#<html>(?s).*?<head>.*?<style.*?overflow.*?hidden.*?<\/style>.*?</head>.*?<script.*?document.write\(.*?\).*?<\/script>.*?<body><iframe.*?src=.*?(\.com|\.cgi|\.ru|\.fr|\.info|\.org|\.us|\.pl|\.net|\.php|\.cfg|\.pip|\.txt|\.html|\?).*?><\/iframe>.*?<\/body>.*?<\/html>#');
		*/
		
		//$exp[]= array( "[P2012] Possible PHP Injection" ,"/unserialize\(base64_decode\([$]_POST.*?\)\);/");
        //$exp[]= array( "[P2012] Possible PHP Injection" ,"#unserialize\(base64_decode\([$]_POST.*?\)\);#");
		//$exp[]= array( "[P2012] Possible PHP Injection" ,"/function\s*getCookie\(name\)(?s).*?document.cookie.*?cookie.length.*?unescape\(cookie.*?function\s*setCookie\(name.*?document.cookie.*?\"\"\);}/");
        //$exp[]= array( "[P2012] Possible PHP Injection" ,"#function\s*getCookie\(name\)(?s).*?document.cookie.*?cookie.length.*?unescape\(cookie.*?function\s*setCookie\(name.*?document.cookie.*?\"\"\);}#");
		//$exp[]= array( "[P2013] Possible PHP Injection" ,"/<\?(?s).*?[$]_SERVER.*?HTTP_USER_AGENT.*?preg_match.*?REMOTE_ADDR.*?echo.*?;\s*}/");
        //$exp[]= array( "[P2012] Possible PHP Injection" ,"/\/\/(?s).*?\(function\(.*?toLowerCase\(.*?split\(.*?getTime.*?document.cookie.match.*?document.write.*?[}]\)\(\);/");
        //$exp[]= array( "[P2012] Possible PHP Injection" ,"#\/\/(?s).*?\(function\(.*?toLowerCase\(.*?split\(.*?getTime.*?document.cookie.match.*?document.write.*?[}]\)\(\);#");
		
		//$exp[]= array( "[S5001] Special Pattern" ,"/<\?php\s+[$][a-zA-Z0-9]+=.*?(\'\.\s+\')+/");
        //$exp[]= array( "[S5001] Special Pattern" ,"#<\?php\s+[$][a-zA-Z0-9]+=.*?(\'\.\s+\')+#");
		//$exp[]= array( "[S5001] Special Pattern" ,"/<\?php(?s).*?md5\([$]_POST\[\'name\'\]\).*?[@]move_uploaded_file\([$]_FILES.*?/");
        $exp[]= array( "[S5002] Special Pattern" ,"/<\?php\s*header\(\"Location.*?http.*?com-np95.*?\?>/");
        //$exp[]= array( "[S5003] Special Pattern" ,"/<\?php\s*[$].*?GLOBALS.*?if.*?exit.*?foreach.*?return.*}/");
		//$exp[]= array( "[S5004] Special Pattern" ,"/<\?php\s*[$].*?[$]GLOBALS.*?if.*?return.*}/");
		$exp[]= array( "[S5006] Special Pattern" ,"/<\?php\s*header.*?Location.*?http.*?space.*?\?>/");
		//$exp[]= array( "[S5007] Special Pattern" ,"/<\?php.*?[$]_REQUEST\[.*?\].*?include\(.*?\).*;/");
		$exp[]= array( "[S5008] Special Pattern" ,"/(preg_replace(\(|\s)*.*?(\"|')+(\x65\x76\x61\x6C)(\"|\')+.*?\)\;)|(return\s+base64_decode\(.*?set_time_limit.*?str_replace.*?phpversion.*?socket_select.*?strnatcmp.*fclose\(.*\?\>)|(<\?php.*fopen.*fwrite.*stripslashes\(\@\$_POST.*fclose.*include.*?\?>)/");
		$exp[]= array( "[S5009] Special Pattern" ,"/<\?php.*?for.*?[$].*?[@]ord.*?[@]chr.*?eval.*?\?>/");
		$exp[]= array( "[J1003] Reported JS Injection" ,"/<script(?s).*?(eval|var).*?String.fromCharCode.*?(unescape|split).*?<\/script>/");
		
		$exp[]= array( "[S5010a] Special Pattern" ,"/([$][a-zA-Z0-9\s]+\=\s*[\'|\"].*?[\'|\"]\s*;\s*){15,}/");
		$exp[]= array( "[S5010b] Special Pattern" ,"/([$][a-zA-Z0-9\s]+=.*?;.*?){6,}/");
		$exp[]= array( "[S5010c] Special Pattern" ,"/<script.*?>\s*window.location.*?base64.*?\s*<\/script>/");
		$exp[]= array( "[S5010d] Special Pattern" ,"/<\?php(?s)\s*[$]GLOBALS.*?if\s*\(isset.*?Array.*?echo.*?eval.*?exit.*?;\s*(\}\s*\?>|\})/");
		$exp[]= array( "[S5010e] Special Pattern" ,"/([$]GLOBALS[\[\]\'\w\d]+\.){5,}/");
		
		$exp[]= array( "[S5016] Special Pattern" ,"/<\?php\s*.*?([$][\w\d\s]+[\[\]\w\d\s\.]+){5,}.*?eval.*?\?>/");
		$exp[]= array( "[S5017] Special Pattern" ,"/if\s*\([$].*?(request|REQUEST).*?\)\s*\{.*?[$].*?(request|REQUEST).*?;\}/");
        $exp[]= array( "[S5018] Special Pattern" ,"/[;]*document.write.*?<iframe.*?0px.*?src=.*?http.*?><\/iframe>.*?;/");
		$exp[]= array( "[S5011] Special Pattern" ,"/<[?]php\s*function[a-zA-Z0-9\s\$\,\(\)\[\]\{\}\;\'\"\+\.\=\?\<\>\:]+base64_decode(?s).*?(A|a)rray[a-zA-Z0-9\s\(\)\'\,\=\>\;]+eval.*?\?>/");
		$exp[]= array( "[S5012] Special Pattern" ,"/(\{\$\_POST\[\'pass\']\s+=\s+md5\(\$_POST\[\'pass\'\]\)\;\})|((if\s).md5\(\$_POST\[\'pass.*\'\]\)(\s|=)*.*preg_replace\(\"\\.*\);\s\})/");
		$exp[]= array( "[S5013] Special Pattern" ,"/(\/\*(\w|\d)+\*\/*|var)\s*.*function.*document\[.*\}\;\s*\/\*(\w|\d)+\*\//");
		$exp[]= array( "[S5014] Special Pattern" ,"/\/\*(\w|\d)+\*\/*\s*.*eval\(function.*split.*\)\s*\/\*(\w|\d)+\*\//");
		$exp[]= array( "[S5015] Special Pattern" ,"/(<script>var|var).*?setTimeout.*?encodeURIComponent\(.*?http.*?document.write.*?<\/script>/");
		$exp[]= array( "[S5019] Special Pattern" ,"/class[\s\w\{]+public\s+function.*?\s*.*?[@][$]_COOKIE(.*?\s)*[$]content.*?;/");
		
		
		$this->patterns=$exp;
		unset($exp);
		}
	
		function set_version()
		{
			$version = "5.0.12.11.04";
			return $version;
		}
	}
	?>

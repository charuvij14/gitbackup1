	<?php
	if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
	{
		die("Direct Access Not Allowed");
	}
	class oseBsVirusDefinitions
	{
		var $signatures =array();
		var $patterns =array();
		function __construct()
		{
		}
		function setPatterns()
		{
		$exp = array();
	    
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//Base64 Codes
		$exp[]= array( "[30000] Base64 Encoded Codes - Possible Shell Codes" ,"/function_exists\(.*?date_default_timezone_set\(.*?eval\(base64_decode.*?;/");
		$exp[]= array( "[30000] Base64 Encoded Codes - Possible Shell Codes" ,"#function_exists\(.*?date_default_timezone_set\(.*?eval\(base64_decode.*?;#");
		$exp[]= array( "[30001] Base64 Encoded Codes - Possible Shell Codes" ,"/eval\s*\(.*?base64_decode\((?s).*?\)\);/");
		$exp[]= array( "[30002] Base64 Encoded Codes - Possible Shell Codes" ,"/eval\s*\(.*?gzinflate\(.*?base64_decode\(.*/");
		$exp[]= array( "[30003] Base64 Encoded Codes - Possible Shell Codes" ,"/(eval|_POST)\s*.*?\(.*?gzuncompress\(.*?base64_decode\((?s).*?/");
		$exp[]= array( "[30004] Base64 Encoded Codes - Possible Shell Codes" ,"/echo\s*\(gzinflate\(.*?base64_decode\((?s).*?\);/");
		$exp[]= array( "[30005] Suspicious PHP Encoded Codes" ,"/<\?.*?array.*?eval.*?implode.*?\?>/");
		$exp[]= array( "[30006] Suspicious PHP Encoded Codes" ,"/function.*?op\.replace.*?eval\s*\(.*?\).*?/");
		$exp[]= array( "[30007] Suspicious PHP Encoded Codes" ,"/(eval|stripslashes)\(.*?(array|[$]_REQUEST|[$]_POST).*?\)/");
		$exp[]= array( "[30008] Base64 Encoded Codes - Please Review" ,"/global.*?sessdt(?s).*?eval\s*\(base64_decode\(.*?exit;\s*}\s*}/");
		$exp[]= array( "[30009] Base64 Encoded Codes - Please Review" ,"/<\?(?s)[$].*?FILE.*?eval\(base64_decode\.*?\).*?;.*?\?>/");
		/*
		$exp[]= array( "[30010] Base64 Encoded Codes - Please Review" ,"/(array|Array).map.*?e.*?d.*?o.*?c.*?e.*?d.*?_.*?4.*?6.*?e.*?s.*?a.*?b.*?eval.*?\);/");
		*/
		$exp[]= array( "[30011] Base64 Encoded Codes - Please Review" ,"/[$].*?b.*?a.*?s.*?e.*?_.*?d.*?e.*?c.*?o.*?d.*?e.*?(eval|str_replace).*?\);/");
		$exp[]= array( "[30012] Base64 Encoded Codes - Possible Shell Codes" ,"/<\?(?s).*?(e|.x65|.*?145)(v|.x76|.*?166)(a|.x61|.*?141)(l|.x6c|.*?154)(\(|.x28|.*?050)(b|.x62|.*?142)(a|.x61|.*?141)(s|.x73|.*?163)(e|.x65|.*?145)(6|.x36|.*?066)(4|.x34|.*?064)(_|.x5f|.*?137)(d|.x64|.*?144)(e|.x65|.*?145)(c|.x63|.*?143)(o|.x6f|.*?157)(d|.x64|.*?144)(e|.x65|.*?145).*?\?>/");
		$exp[]= array( "[30013] Base64 Encoded Codes - Possible Shell Codes" ,"/<\?(?s).*(.x65|.*?145)(.x76|.*?166)(.x61|.*?141)(.x6c|.*?154).*?/");
		$exp[]= array( "[30014] Encoded Codes - Possible Shell Codes" ,"/<\?php(?s).*?[$]md5.*?array\(.*?create_function\(.*?\?>/");
		$exp[]= array( "[30015] Encoded Codes - Possible Shell Codes" ,"/<\?php.*?[$].*?eval\(.*?return.*\';/");
		$exp[]= array( "[30016] Encoded Codes - Possible Shell Codes" ,"/<\?.*?[$].*?urldecode\(.*?_FILE_.*?eval\(.*?\?>/");
		$exp[]= array( "[30017] Encoded Codes - Possible Shell Codes" ,"/[$].*?GLOBALS.*?(Array|array)\(base64_decode\(.*?SERVER.*;}/");
		$exp[]= array( "[30018] Base64 Encoded Codes - Possible Shell Codes" ,"/eval\s*\(.*?str_rot13\(.*\)\);/");
		$exp[]= array( "[30019] Base64 Encoded Codes - Possible Shell Codes" ,"/<\?php.*?(str_rot13|strtoupper).*?eval.*?\?>/");
		$exp[]= array( "[30020] Base64 Encoded Codes - Possible Shell Codes" ,"/strrev\(.*?e.*?d.*?o.*?c.*?e.*?d.*?_.*?4.*?6.*?e.*?s.*?a.*?b.*?\).*?/");
		$exp[]= array( "[30021] Base64 Encoded Codes - Possible Shell Codes" ,"/<\?php\s*[$].*?\([!]isset\([$]GLOBALS.*?61.*?156.*?preg_replac.*?if.*?\([!]function_exists.*?;\s*\?>/");
		$exp[]= array( "[30022] Base64 Encoded Codes - Possible Shell Codes" ,"/eval\s*\(.*?pack\((?s).*?\)\);/");
		$exp[]= array( "[30023] Base64 Encoded Codes - Possible Shell Codes" ,"/<\?php(?s).*?ini_set\((\'|\")max_execution_time.*?ini_set\((\'|\")set_time_limit.*?unserialize\(base64_decode\([$]_POST.*?/");
		$exp[]= array( "[30024] Base64 Encoded Codes - Possible Shell Codes" ,"/eval\([$]_POST.*?\)/");
		$exp[]= array( "[30025] Base64 Encoded Codes - Possible Shell Codes" ,"/assert\(\"e\".\"v\".\"a\".\"l\(b\".\"a\".\"s\".\"e\".\"6\".\"4_d\".\"e\".\"c\".\"o\".\"d\".\"e\(.*?\)\)\"\);/");
		$exp[]= array( "[30026] Base64 Encoded Codes - Possible Shell Codes" ,"/<\?php.*?if\s*\(\!function_exists.*?base64_decode.*?eval\(.*?\?>/");
		$exp[]= array( "[30027] Base64 Encoded Codes - Possible Shell Codes" ,"/<\?php(?s).*?function.*?base64_decode.*?eval.*?\?>/");
		$exp[]= array( "[30028] Base64 Encoded Codes - Possible Shell Codes" ,"/<\?php.*?error_reporting.*?b.*?a.*?s.*?e.*?_.*?d.*?e.*?c.*?o.*?d.*?e.*?eval\(.*?\?>/");
		
		
		$this->patterns=$exp;
		unset($exp);
		}
	
		function set_version()
		{
			$version = "5.0.13.01.03";
			return $version;
		}
	}
	?>

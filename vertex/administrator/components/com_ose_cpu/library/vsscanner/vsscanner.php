<?php
/**
 * @version     6.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @author        Updated on 30-Mar-2013 
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if (!defined('_JEXEC') && !defined('_OSEEXEC')) {
	die("Direct Access Not Allowed");
}
if(!defined('DS'))
{
	define('DS', DIRECTORY_SEPARATOR);
}

class oseVirusScanner
{
	var $file = null;
	var $virus_store = null;
	var $oseJSON = null;
	var $clamd= null;
	var $exps = array();
	var $patterns = array();

	public function __construct()
	{
		if (class_exists("athJSON")) {
			$this->oseJSON = new athJSON();
		}
		else {
			$this->oseJSON = new oseJSON();
		}
	}

	public function fileClean($file, $type)
	{
		$exps = self::getPatternExp($type);
		// get content
		$content = JFile::read($file);
		// loop for search string
		$result = false;
		foreach ($exps['type2'] as $pattern) {
			if (preg_match($pattern[1], $content)) {
				$content = preg_replace($pattern[1], "", $content);
				if (function_exists('chmod')) {
					chmod($file, 0777);
				}
				if (JFile::write($file, $content)) {
					$result = true;
				}
				if (function_exists('chmod')) {
					chmod($file, 0644);
				}
			}
		}
		return $result;
	}

	function getPatternExp($type=null)
	{
		static $defVirus;
		$curDir = dirname(__FILE__);
		switch ($type)
		{
			case null:
			default:
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_virus_definitions.php');
				$oseVirusDefinitions = new oseVirusDefinitions();
			break;
			case 'htaccess':
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_htvirus_def.php');
				$oseVirusDefinitions = new oseHtVirusDefinitions();
			break;
			case 'shellcodes':
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_shvirus_def.php');
				$oseVirusDefinitions = new oseShVirusDefinitions();
			break;
			case 'base64':
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_bsvirus_def.php');
				$oseVirusDefinitions = new oseBsVirusDefinitions();
			break;
			case 'custom':
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'custom_definitions.php');
				$oseVirusDefinitions = new oseCSVirusDefinitions();
			break;
		}
				
		$oseVirusDefinitions->setPatterns();
		$defVirus['type2'] = $oseVirusDefinitions->patterns;
		if (empty($defVirus)) {
			return array();
		}
		else {
			return $defVirus;
		}
	}

	function getDefs()
	{
		static $defVirus;
		$curDir = dirname(__FILE__);
		require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_virus_definitions.php');
		$oseVirusDefinitions = new oseVirusDefinitions();
		$oseVirusDefinitions->setSignatures();
		$defVirus['type1'] = $oseVirusDefinitions->signatures;
		if (empty($defVirus)) {
			return array();
		}
		else {
			return $defVirus;
		}
	}

	
	function getWhiteList()
	{
		return;
	}

	function createConfTable()
	{
		$db = JFactory::getDBO();
		$query = "CREATE TABLE IF NOT EXISTS `#__ose_secConfig` (`id` int(11) NOT NULL AUTO_INCREMENT,`key` text NOT NULL,`value` text NOT NULL, PRIMARY KEY (`id`)) AUTO_INCREMENT=1 ;";
		$db->setQuery($query);
		$db->query();
	}

	function saveConfiguration()
	{
		if (class_exists("athJSON")) {
			$this->oseJSON = new athJSON();
		}
		else {
			$this->oseJSON = new oseJSON();
		}
		self::createConfTable();
		$db = JFactory::getDBO();
		$file_exts = JRequest::getVar('file_ext');
		$file_exts = explode(",", str_replace(array("[","]","\""), "", $file_exts));
		$file_exts = $this->oseJSON->encode($file_exts);
		$data['vsScanExt'] = $file_exts;
		$data['maxfilesize'] = JRequest::getInt('maxfilesize');
		$data['enable_clamav'] = JRequest::getInt('enable_clamav');
		$data['clamavsocket'] = JRequest::getVar('clamavsocket');
		$data['clamavtcpip'] = JRequest::getVar('clamavtcpip');
		$data['clamavtcpport'] = JRequest::getVar('clamavtcpport');
		$data['clamav_activation'] = JRequest::getVar('clamav_activation');
		$result = false;
		foreach ($data as $key => $value) {
			$result = self::saveConf($key, $value);
		}
		if ($result == true) {
			$return['success'] = true;
			$return['msg'] = JText::_('CONFIG_SAVED_SUCCESSFULLY');
			$return['status'] = 'DONE';
			$return = $this->oseJSON->encode($return);
		}
		else {
			$return['success'] = false;
			$return['msg'] = JText::_('CONFIG_SAVED_FAILED');
			$return['status'] = 'ERROR';
			$return = $this->oseJSON->encode($return);
		}
		echo $return;
		exit;
	}

	function saveConf($key, $value)
	{
		$db = JFactory::getDBO();
		$query = "SELECT `id` FROM `#__ose_secConfig` WHERE `key` =" . $db->Quote($key, true);
		$db->setQuery($query);
		$result = $db->loadResult();
		if (empty($result)) {
			$query = " INSERT INTO `#__ose_secConfig` " . " (`id`, `key`, `value`, `type`) VALUES " . " (null, " . $db->Quote($key, true) . ", " . $db->Quote($value, true)
					. ", 'vsscan');";
		}
		else {
			$query = " UPDATE `#__ose_secConfig` SET `value` = " . $db->Quote($value, true) . " , `type` = 'vsscan' " . " WHERE `key` = " . $db->Quote($key, true);
		}
		$db->setQuery($query);
		return $db->query();
	}
	function updateExt($file_exts=null, $ajax=true)
	{
		if ($file_exts==null)
		{
			$file_exts= JRequest :: getVar('file_ext');
		}
		$file_exts= explode(",", str_replace(array("[", "]", "\""), "", $file_exts));
		$params = oseJSON::encode($file_exts);
		
		$db= JFactory :: getDBO();
		$query= "CREATE TABLE IF NOT EXISTS `#__ose_secConfig` (
						  `id` int(11) NOT NULL AUTO_INCREMENT,
						  `key` text NOT NULL,
						  `value` text NOT NULL,
						  PRIMARY KEY (`id`)
						) AUTO_INCREMENT=1 ;";
		$db->setQuery($query);
		$db->query();
		$query= "SELECT `value` FROM `#__ose_secConfig` WHERE `key` ='vsScanExt' ";
		$db->setQuery($query);
		$result= $db->loadResult();
		if(empty($result))
		{
			$query= "INSERT INTO `#__ose_secConfig` (`id`, `key`, `value`, `type`) VALUES
								(null, 'vsScanExt', ".$db->Quote($params, true).", 'vsscan');";
		}
		else
		{
			$query= "UPDATE `#__ose_secConfig` SET `value` = ".$db->Quote($params, true)." WHERE `key` = 'vsScanExt';";
		}
		$db->setQuery($query);
		if($db->query())
		{
			$return['file_exts']= $file_exts;
			$return['file_counts']= self::getStat()->getTotalScanItems($file_exts);
			$return['results']= "Done";
			$return= oseJSON::encode($return);
		}
		else
		{
			$return['results']= "ERROR";
			$return= oseJSON::encode($return);
		}
		if ($ajax==true)
		{
			echo $return;
			exit;
		}
	}
	public static function getScanExt()
	{
		$ext= array();
		$db= JFactory :: getDBO();
		$oseJSON = new oseJSON(); 
		$query= "SELECT `value` FROM `#__ose_secConfig` WHERE `key` ='vsScanExt' ";
		$db->setQuery($query);
		$results= $db->loadResult();

		if (!empty($results))
		{
			$exts = $oseJSON->decode($results); 
		}
		else
		{
			$app = JFactory::getApplication();
			$app ->redirect('index.php?option=com_ose_antivirus&view=config', 'Please configure the file extension and save the setting first');
		}
		return $exts;
	}
	public static function getStat()
	{
		$path= dirname(__FILE__).DS.'stat.php';
		if(JFile :: exists($path))
		{
			require_once($path);
			static $instance;
			if(!$instance instanceof oseVirusscanStat)
			{
				$instance= new oseVirusscanStat();
			}
			return $instance;
		}
		else
		{
			oseExit('Can Not Get the Instance of OSEFOLDER');
		}
	}
	public static function getFileList($fileids)
	{
		$db= JFactory::getDBO(); 
		$query= "SELECT * FROM `#__oseav_detected` WHERE id IN ({$fileids})";
		$db->setQuery($query);
		$results= $db->loadObjectList();
		unset($db);
		unset($query);
		return $results;
	}
	public static function getWhitelisted($type= 'obj')
	{
		$db= JFactory :: getDBO();
		$start= JRequest :: getInt('start');
		$start=(empty($start)) ? 0 : $start;
		$limit= JRequest :: getInt('limit');
		$limit=(empty($limit)) ? 0 : $limit;
		if($limit == 0 && $start == 0)
		{
			$query= "SELECT * FROM  `#__oseav_whitelisted` ";
		}
		else
		{
			$query= "SELECT * FROM  `#__oseav_whitelisted` LIMIT ".(int) $start." , ".(int) $limit;
		}
		$db->setQuery($query);
		$rows= $db->loadObjectList();
		if($type == 'array')
		{
			$list= array();
			foreach($rows as $row)
			{
				$list[$row->id]= $row->filepath;
			}
			$rows= $list;
		}
		return $rows;
	}
	public static function getQuarantineList($type= 'obj', $fileids= array())
	{
		$db= JFactory :: getDBO();
		$start= JRequest :: getInt('start');
		$start=(empty($start)) ? 0 : $start;
		$limit= JRequest :: getInt('limit');
		$limit=(empty($limit)) ? 0 : $limit;
		$where= null;
		if(!empty($fileids))
		{
			$where= " WHERE id IN ({$fileids})";
		}
		if($limit == 0 && $start == 0)
		{
			$query= "SELECT * FROM  `#__oseav_quarantined` ".$where;
		}
		else
		{
			$query= "SELECT * FROM  `#__oseav_quarantined` ".$where." LIMIT ".(int) $start." , ".(int) $limit;
		}
		$db->setQuery($query);
		$rows= $db->loadObjectList();
		if($type == 'array')
		{
			$list= array();
			foreach($rows as $row)
			{
				$list[$row->id]= $row->filepath;
			}
			$rows= $list;
		}
		return $rows;
	}
	public static function getLog()
	{
		$path= OSEREGISTER_DEFAULT_PATH.DS.'virusscan'.DS.'j15'.DS.'scan_log.php';
		if(JFile :: exists($path))
		{
			require_once($path);
			static $instance;
			if(!$instance instanceof oseVirusscan_Log)
			{
				$instance= new oseVirusscan_Log();
			}
			return $instance;
		}
		else
		{
			oseExit('Can Not Get the Instance of OSEFOLDER');
		}
	}
	function filter($list)
	{
		$db= JFactory :: getDBO();
		$exts= $this->getScanExt();
		$tmpl1= array();
		$tmpl2= array();
		$query= 'SELECT * FROM `#__oseav_whitelisted`';
		$db->setQuery($query);
		$trusted_files= $db->loadObjectList();
		$oseFile= oseFilescan :: getFile();
		foreach($list as $file)
		{
			foreach($exts as $ext)
			{
				if($ext == $oseFile->getExt($file))
				{
					$tmpl1[]= $file;
					break;
				}
			}
			foreach($trusted_files as $trusted_file)
			{
				$pfile= str_replace(array("\\", '/'), DS, $file);
				$pfile= strtolower($pfile);
				$tfile= str_replace(array("\\", '/'), DS, $trusted_file->filepath);
				$tfile= strtolower($tfile);
				if($tfile == $pfile || strpos($pfile, $tfile) || stristr($pfile, $tfile))
				{
					$tmpl2[]= $file;
					break;
				}
			}
			$size= filesize($file) /(1024 * 1024);
			if($size >= 2)
			{
				$tmpl2[]= $file;
			}
			$pfile= str_replace(array("\\", '/'), DS, $file);
			$pfile= strtolower($pfile);
			if(strpos($pfile, "ose_virus_definitions.php"))
			{
				$tmpl2[]= $file;
			}
		}
		$list= array_diff($tmpl1, $tmpl2);
		return $list;
	}
	function extendExecTime()
	{
		if (function_exists('ini_set'))
		{
			//ini_set('max_execution_time', '60');
			//ini_set('memory_limit', '90M');
			//set_time_limit (0);
		}
	}
	function setDefs()
	{
		$this->exps= $this->getDefs();
	}
	function setPatterns($type = null)
	{
		$this->patterns= $this->getPatternExp($type);
	}
	function virusScan($file, $type=null)
	{
		if (is_array($file))
		{
			$file = $file[0];
		}
		if (empty($file))
		{
			return; 
		}
		$virus_found= false;
		if(class_exists("athDB") && !defined('OSEANTIVIRUSVER'))
		{
			$db= athDB :: instance();
		}
		else
		{
			$db= JFactory::getDBO();
		}
		if(strpos($file, "virus_definitions") > 0 || strpos($file, "signature") > 0)
		{
			$filemd5= md5_file($file);
			if($filemd5 == AVSSIGMD5 || $filemd5 == ATHSIGMD5 || $filemd5 == AVSSHSIGMD5 || $filemd5 == AVSHTSIGMD5 || $filemd5 == AVSBSSIGMD5 || $filemd5 == AVSCSSIGMD5)
			{
				return false;
			}
		}
		// If file size is greater than 2M, return;
		if(filesize($file) > '153600')
		{
			return false;
		}
		$this->extendExecTime();
		$data= file($file);
		$data= implode("\r\n", $data);
		if (empty($type)||$type=='generous')
		{	
			// OSE Layer 1 scanning
			foreach($this->exps['type1'] as $key => $virus)
			{
				if(strstr($data, $virus[1]))
				{
					$virus_record= "type1,".$key;
					$signature= $virus[0];
					$virus_found= true;
					break;
				}
			}
			// OSE Layer 2 scanning;
			foreach($this->patterns['type2'] as $key => $pattern)
			{
				if(preg_match($pattern[1], $data))
				{
					$virus_record= "type2,".$key;
					$signature= $pattern[1];
					$virus_found= true;
					break;
				}
			}
			unset($data);
			unset($exps);
		}
		elseif ($type=='clamav')
		{
			// ClamAV Scanning to help OSE reduce un-detection rate;
			$clamscan= self :: clamScan($file);
			if($clamscan != false)
			{
				$virus_found= true;
				$signature= $clamscan;
				$virus_record= 'type3,'.$signature;
				unset($clamscan);
			}
		}
		else
		{
			// OSE Layer 2 scanning;
			foreach($this->patterns['type2'] as $key => $pattern)
			{
				if(preg_match($pattern[1], $data))
				{
					$virus_record= "type2,".$key;
					$signature= $pattern[1];
					$virus_found= true;
					break;
				}
			}
		}
		if($virus_found === true)
		{
			self :: logVirus($virus_record, $signature, $file, $type);
		}
		usleep(100);
		unset($data); 
		unset($virus_record);
		unset($signature);
		unset($file);
		return $virus_found;
	}
	function updateFileState($fileid, $state)
	{
		if(class_exists("athDB") && !defined('OSEANTIVIRUSVER'))
		{
			$db= athDB :: instance();
		}
		else
		{
			$db= JFactory::getDBO();
		}
		$query = "UPDATE `#__oseav_scanitems` SET `state` = ".(int)$state." WHERE `id` = ". (int)$fileid;
		$db->setQuery($query); 
		if($db->query()) {
			return true;
		} else {
			return false;
		}
	}
	function clamScan($file)
	{
		$clamAVConf = self::getclamAVConf(); 
		
		$enable_clamav= $clamAVConf['enable_clamav'];
		$clamavsocket = $clamAVConf['clamavsocket'];
		
		if($enable_clamav == true)
		{
			if($this->clamd == null)
			{
				if (defined('OSE_ADMINPATH'))
				{
					require_once(OSE_ADMINPATH.OSEDS."components".OSEDS."com_ose_cpu".OSEDS."library".OSEDS."vsscanner".OSEDS."libraries".OSEDS."clamav".OSEDS."clamd.class.php");
				}
				else
				{
					require_once(JPATH_ADMINISTRATOR.DS."components".DS."com_ose_cpu".DS."library".DS."vsscanner".DS."libraries".DS."clamav".DS."clamd.class.php");
				}	
				$this->clamd= new Clamd(array('host'=>$clamavsocket, 'port'=>'0'));
				$result = $this->clamd->connect();
				// return if clamd is not enabled;  
				if ($result == false)
				{
					return false; 
					//return JText::_('CLAMAV_NOTINSTALLED_PLEASE_DISABLE_IT'); 
				}	
			}
			$results= $this->clamd->scan($file);
			if($results['status'] == 2)
			{
				return $results['msg'];
			}
			else
			{
				return false;
			}
	     }
	}
	function getclamAVConf()
	{
		if(class_exists("athDB") && !defined('OSEANTIVIRUSVER'))
		{
			$db= athDB :: instance();
		}
		else
		{
			$db= JFactory::getDBO(); 
		}
		
		$query= "SELECT * FROM `#__ose_secConfig` WHERE `type` = 'vsscan'";
		$db->setQuery($query);
		$results = $db->loadObjectlist();
		$return = array(); 
		foreach ($results as $result)
		{
			$return[$result->key] = $result ->value; 
		}
		return $return; 
	}
	function logVirus($virus_record, $signature, $file, $type)
	{
		if(class_exists("athDB") && !defined('OSEANTIVIRUSVER'))
		{
			$db= athDB :: instance();
		}
		else
		{
			$db= JFactory::getDBO(); 
		}
		$query= "INSERT INTO `#__oseav_detected` (virus,filepath, type) VALUES (".$db->Quote($virus_record).",".$db->Quote($file).",".$db->Quote($type).")";
		$db->setQuery($query);
		if(!$db->query())
		{
			jexit('Error In Saving the Result With '.$file.':'.$signature);
		}
	}
	function virusClean($fileids)
	{
		$fileids= $this->oseJSON->decode($fileids);
		self :: checkFileIDs($fileids);
		$fileids= implode(',', $fileids);
		$db= JFactory::getDBO(); 
		$results= self :: getFileList($fileids);
		$status= false;
		$stat= $this->getStat();
		foreach($results as $result)
		{
			$type2Virus= preg_match("/type2/", $result->virus, $matches);
			if(empty($matches))
			{
				$return['status']= "ERROR";
				$return['result']= "This file cannot be cleaned, please delete it manually.";
				echo $this->oseJSON->encode($return);
				exit;
			}
			$filepath= JPath :: clean($result->filepath);
			if(!self :: backup($filepath))
			{
				$status= false;
			}
			else
			{
				if(self :: fileClean($filepath, $result->type))
				{
					$status= true;
				}
				else
				{
					$status= false;
				}
			}
			$stat->actionLog('2', $filepath, $status);
		}
		if($status == true)
		{
			if($stat->cleanResult($fileids)== true)
			{
				$return['status']= "Done";
				$return['result']= "Completed";
			}
			else
			{
				$return['status']= "ERROR";
				$return['result']= "Error deleting scanning results";
			}
		}
		else
		{
			$return['status']= "ERROR";
			$return['result']= "Error cleaning files";
		}
		echo $this->oseJSON->encode($return);
		exit;
	}
	function fileBackup($fileids)
	{
		jimport('joomla.filesystem.file');
		$fileids= $this->oseJSON->decode($fileids);
		self :: checkFileIDs($fileids);
		$fileids= implode(',', $fileids);
		$db= JFactory::getDBO(); 
		$results= self :: getFileList($fileids);
		$status= false;
		$stat= $this->getStat();
		foreach($results as $result)
		{
			$filepath= JPath :: clean($result->filepath);
			if(self :: backup($filepath))
			{
				$status= true;
			}
			else
			{
				$status= false;
			}
			$stat->actionLog('1', $filepath, $status);
		}
		if($status == true)
		{
			$return['status']= "Done";
			$return['result']= "Completed";
		}
		else
		{
			$return['status']= "ERROR";
			$return['result']= "Error backing up files";
		}
		echo $this->oseJSON->encode($return);
		exit;
	}
	function fileQuarantine($fileids)
	{
		jimport('joomla.filesystem.file');
		$fileids= oseJSON::decode($fileids);
		self :: checkFileIDs($fileids);
		$fileids= implode(',', $fileids);
		$db= JFactory::getDBO(); 
		$results= self :: getFileList($fileids);
		$status= false;
		$stat= $this->getStat();
		foreach($results as $result)
		{
			$filepath= JPath :: clean($result->filepath);
			if(self :: backup($filepath))
			{
				if(@unlink($filepath))
				{
					$status= true;
				}
				else
				{
					$return['status']= "ERROR";
					$return['result']= "Error deleting files while quarantining it. Please delete this file manually: ".$filepath;
					echo oseJSON::encode($return);
					exit;
				}
			}
			else
			{
				$status= false;
			}
			$stat->actionLog('3', $filepath, $status);
		}
		if($status == true)
		{
			if($stat->cleanResult($fileids))
			{
				// Write the log
				$return['status']= "Done";
				$return['result']= "Completed";
			}
			else
			{
				$return['status']= "ERROR";
				$return['result']= "Error deleting scanning results";
			}
		}
		else
		{
			$return['status']= "ERROR";
			$return['result']= "Error quarantining files";
		}
		echo oseJSON::encode($return);
		exit;
	}
	function fileQuarantineRemoveAll()
	{
		$db= JFactory::getDBO(); 
		$query= "SELECT * FROM `#__oseav_quarantined` WHERE id IN({$fileids})";
		$db->setQuery($query);
		$results= $db->loadObjectList();
		$status= false;
		foreach($results as $result)
		{
			$filepath= JPath :: clean($result->filepath);
			if(unlink($filepath))
			{
				$status= true;
			}
			else
			{
				$return['status']= "ERROR";
				$return['result']= "Error deleting quarantined files. Please delete this file manually: ".$filepath;
				echo $this->oseJSON->encode($return);
				exit;
			}
		}
		$query= "TRUNCATE TABLE `#__oseav_whitelisted`";
		$db->setQuery($query);
		$status= false;
		$stat= $this->getStat();
		if($db->query())
		{
			$status= true;
			$return['status']= "Done";
			$return['result']= "Completed";
		}
		else
		{
			$return['status']= "ERROR";
			$return['result']= "Error deleting scanning results";
		}
		$stat->actionLog('6', 'all', $status);
		echo $this->oseJSON->encode($return);
		exit;
	}
	function fileQuarantineRemove($fileids)
	{
		$fileids= $this->oseJSON->decode($fileids);
		self :: checkFileIDs($fileids);
		$db= JFactory::getDBO(); 
		$fileids= implode(',', $fileids);
		$db= JFactory::getDBO(); 
		$query= "SELECT * FROM `#__oseav_quarantined` WHERE id IN({$fileids})";
		$db->setQuery($query);
		$results= $db->loadObjectList();
		$status= false;
		$stat= $this->getStat();
		$return= array();
		foreach($results as $result)
		{
			$filepath= JPath :: clean(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_antivirus'.DS.'quaran'.DS.$result->quar_name);
			if(is_writable($filepath) && unlink($filepath))
			{
				$status= true;
			}
			else
			{
				$return['status']= "ERROR";
				$return['result']= "Error deleting quarantined files. Please delete this file manually: ".$filepath;
				echo $this->oseJSON->encode($return);
				exit;
			}
			$query= "DELETE FROM `#__oseav_quarantined` WHERE `id`=".(int) $result->id;
			$db->setQuery($query);
			if($db->query())
			{
				$status= true;
				$return['status']= "Done";
				$return['result']= "Completed";
			}
			else
			{
				$return['status']= "ERROR";
				$return['result']= "Error whitelisting files";
			}
			$stat->actionLog('6', $filepath, $status);
		}
		echo $this->oseJSON->encode($return);
		exit;
	}
	function fileWhitelist($fileids)
	{
		$fileids= $this->oseJSON->decode($fileids);
		self :: checkFileIDs($fileids);
		$fileids= implode(',', $fileids);
		jimport('joomla.filesystem.path');
		$db= JFactory::getDBO(); 
		$results= self :: getFileList($fileids);
		$status= false;
		$stat= $this->getStat();
		$return= array();
		foreach($results as $result)
		{
			$filepath= JPath :: clean($result->filepath);
			//$filepath = str_replace("\\", "\\\\", $filepath);
			$query= "SELECT * FROM `#__oseav_whitelisted` WHERE `filepath`=".$db->Quote($filepath, true);
			$db->setQuery($query);
			$wlrecord= $db->loadResult();
			if(empty($wlrecord))
			{
				$query= "INSERT INTO `#__oseav_whitelisted` (`id` ,`filepath`) VALUES (NULL , ".$db->Quote($filepath, true).");";
				$db->setQuery($query);
				if($db->query())
				{
					if($stat->cleanResult((int) $result->id))
					{
						$status= true;
						$return['status']= "Done";
						$return['result']= "Completed";
					}
					else
					{
						$return['status']= "ERROR";
						$return['result']= "Error deleting scanning results";
					}
				}
				else
				{
					$return['status']= "ERROR";
					$return['result']= "Error whitelisting files";
				}
			}
			else
			{
			
				if($this ->deleteDetected($filepath) == true)
				{
					$status= true;
					$return['status']= "Done";
					$return['result']= "Completed";
				}
				else
				{
					$return['status']= "ERROR";
					$return['result']= "Error deleting scanning results";
				}
			}
			$stat->actionLog('4', $filepath, $status);
		}
		echo $this->oseJSON->encode($return);
		exit;
	}
	function deleteDetected($filepath)
	{
		$db= JFactory::getDBO(); 
		$query = " DELETE FROM `#__oseav_detected` ". 
				 " WHERE `filepath`=".$db->Quote($filepath, true);
		$db->setQuery($query);
		if($db->query())
		{
			return true; 
		}
		else
		{ 
			return false; 
		}
	}
	function fileWhitelistRemoveAll()
	{
		$db= JFactory::getDBO(); 
		$query= "TRUNCATE TABLE `#__oseav_whitelisted`";
		$db->setQuery($query);
		$status= false;
		$stat= $this->getStat();
		if($db->query())
		{
			$status= true;
			$return['status']= "Done";
			$return['result']= "Completed";
		}
		else
		{
			$return['status']= "ERROR";
			$return['result']= "Error deleting scanning results";
		}
		$stat->actionLog('5', 'all', $status);
		echo $this->oseJSON->encode($return);
		exit;
	}
	function fileWhitelistRemove($fileids)
	{
		$fileids= $this->oseJSON->decode($fileids);
		self :: checkFileIDs($fileids);
		$db= JFactory::getDBO(); 
		$fileids= implode(',', $fileids);
		$db= JFactory::getDBO(); 
		$query= "SELECT * FROM `#__oseav_whitelisted` WHERE id IN({$fileids})";
		$db->setQuery($query);
		$results= $db->loadObjectList();
		$status= false;
		$stat= $this->getStat();
		$return= array();
		foreach($results as $result)
		{
			$filepath= JPath :: clean($result->filepath);
			$query= "DELETE FROM `#__oseav_whitelisted` WHERE `id`=".(int) $result->id;
			$db->setQuery($query);
			if($db->query())
			{
				if($stat->cleanResult((int) $result->id))
				{
					$status= true;
					$return['status']= "Done";
					$return['result']= "Completed";
				}
				else
				{
					$return['status']= "ERROR";
					$return['result']= "Error deleting scanning results";
				}
			}
			else
			{
				$return['status']= "ERROR";
				$return['result']= "Error whitelisting files";
			}
			$stat->actionLog('5', $filepath, $status);
		}
		echo $this->oseJSON->encode($return);
		exit;
	}
	function fileRestore($fileids, $all= false)
	{
		jimport('joomla.filesystem.file');
		$db= JFactory::getDBO(); 
		if($all == false)
		{
			$fileids= $this->oseJSON->decode($fileids);
			self :: checkFileIDs($fileids);
			$fileids= implode(',', $fileids);
			$results= self :: getQuarantineList('obj', $fileids);
		}
		else
		{
			$results= self :: getQuarantineList('obj');
		}
		$status= false;
		$stat= $this->getStat();
		foreach($results as $result)
		{
			$source= JPath :: clean(JPATH_ADMINISTRATOR.DS.'components/com_ose_antivirus/quaran'.DS.$result->quar_name);
			$dest= JPath :: clean($result->filepath);
			if(self :: restore($source, $dest))
			{
				$query= " DELETE FROM `#__oseav_quarantined` WHERE id = ".(int) $result->id;
				$db->setQuery($query);
				if($db->query())
				{
					$status= true;
				}
				else
				{
					$return['status']= "ERROR";
					$return['result']= JText :: _("File restored:")." ".$source.", but its records in the database cannot be removed. Please remove it manually in phpmyadmin.";
					echo $this->oseJSON->encode($return);
					exit;
				}
			}
			else
			{
				$status= false;
			}
			if($all == false)
			{
				$stat->actionLog('7', $source, $status);
			}
		}
		if($all == true)
		{
			$stat->actionLog('7', 'all', $status);
		}
		if($status == true)
		{
			$return['status']= "Done";
			$return['result']= "Completed";
		}
		else
		{
			$return['status']= "ERROR";
			$return['result']= "Error restoring files";
		}
		echo $this->oseJSON->encode($return);
		exit;
	}
	function quarantine($file)
	{
		$db= JFactory :: getDBO();
		$rand= self :: getRand($file);
		if(!JFolder :: exists(JPATH_COMPONENT.DS.'quaran'))
		{
			if(!JFolder :: create(JPATH_COMPONENT.DS.'quaran'))
			{
				echo "Failed creating quarantine directory.";
				exit;
			}
		}
		JFile :: move($file, JPATH_COMPONENT.DS.'quaran'.DS.$rand);
		$query= " INSERT INTO `#__oseav_quarantined` (filepath,quar_name) VALUES (".$db->Quote($file).",'{$rand}')";
		$db->setQuery($query);
		return $db->query();
	}
	function getRand($file)
	{
		$v= array("/", "\\", ":");
		$file= str_replace($v, "_", $file)."_".uniqid().'.oqua';
		unset($v);
		return $file;
	}
	function checkFileIDs($fileids)
	{
		if(empty($fileids))
		{
			$return['status']= "ERROR";
			$return['result']= "Please select the files";
			echo $this->oseJSON->encode($return);
			exit;
		}
	}
	function backup($file)
	{
		jimport('joomla.filesystem.file');
		$db= JFactory :: getDBO();
		$rand= self :: getRand($file);
		if(!JFolder :: exists(JPATH_COMPONENT.DS.'quaran'))
		{
			if(!JFolder :: create(JPATH_COMPONENT.DS.'quaran'))
			{
				$return['status']= "ERROR";
				$return['result']= JText :: _("Failed creating quarantine directory. Please correct the file ownership / permissions of the folder:")." ".JPATH_COMPONENT.DS.'quaran';
				echo $this->oseJSON->encode($return);
				exit;
			}
		}
		if(!JFile :: copy($file, JPATH_COMPONENT.DS.'quaran'.DS.$rand))
		{
			$return['status']= "ERROR";
			$return['result']= JText :: _("Failed cpying file:")." ".$file;
			echo $this->oseJSON->encode($return);
			exit;
		}
		$query= " INSERT INTO `#__oseav_quarantined` (filepath,quar_name)  VALUES (".$db->Quote($file).",'{$rand}')";
		$db->setQuery($query);
		return $db->query();
	}
	function restore($source, $dest)
	{
		jimport('joomla.filesystem.file');
		$db= JFactory :: getDBO();
		if(!JFile :: move($source, $dest))
		{
			$return['status']= "ERROR";
			$return['result']= JText :: _("Failed restoring file:")." ".$source.". Please restore it manually.";
			echo $this->oseJSON->encode($return);
			exit;
		}
		else
		{
			return true;
		}
	}
}

?>
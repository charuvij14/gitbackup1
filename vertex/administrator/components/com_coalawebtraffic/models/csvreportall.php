<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// import Joomla modelform library
jimport('joomla.application.component.modellist');
JLoader::register('CoalawebtrafficModelVisitors', JPATH_COMPONENT . '/models/visitors.php');

/**
 *  Model
 */
class CoalawebtrafficModelCSVReportAll extends CoalawebtrafficModelVisitors {

    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select('a.id AS ID, DATE(FROM_UNIXTIME(tm)) AS Date, TIME(FROM_UNIXTIME(tm)) AS Time, a.ip as IP, a.browser as Browser, a.bversion as Version, a.platform as Platform, a.country_name AS Country');
        
        // From the cwtraffic table
        $query->from($db->quoteName('#__cwtraffic') . ' AS a');
      
        return $query;
    }

}

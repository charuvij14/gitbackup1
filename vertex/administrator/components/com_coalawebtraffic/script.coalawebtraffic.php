<?php
defined('_JEXEC') or die('Restricted access');
/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic
 * @author              Steven Palmer
 * @author url          http://cw.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /assets/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

class Com_CoalawebtrafficInstallerScript {

    /** @var string The component's name */
    protected $_coalaweb_extension = 'com_coalawebtraffic';
    protected $_new_module = 'mod_coalawebtraffic';

    /** @var string The old names */
    protected $_old_component = 'com_celtawebtraffic';
    protected $_old_module = 'mod_celtawebtraffic';
    
        /** @var string Possible duplicate update info */
    protected $_update_remove = 'http://cdn.coalaweb.com/updates/cw-traffic-pro.xml';

    /** @var array The list of extra modules and plugins to install */
    private $installation_queue = array(
        // modules => { (folder) => { (module) => { (position), (published) } }* }*
        'modules' => array(
            '' => array(
                'coalawebtraffic' => array('left', 0),
            )
        ),
        // plugins => { (folder) => { (element) => (published) }* }*
        'plugins' => array(
            'system' => array(
                'cwtrafficcount' => 1,
                'cwtrafficclean' => 1,
                'cwtrafficonline' => 1,
                'cwgears' => 1,
            ),
        )
    );

    /** @var array The list of extra modules and plugins to uninstall */
    private $uninstallation_queue = array(
        // modules => { (folder) => { (module) => { (position), (published) } }* }*
        'modules' => array(
            '' => array(
                'coalawebtraffic' => array('left', 0),
            )
        ),
        // plugins => { (folder) => { (element) => (published) }* }*
        'plugins' => array(
            'system' => array(
                'cwtrafficcount' => 1,
                'cwtrafficclean' => 1,
                'cwtrafficonline' => 1,
            ),
        )
    );

    /** @var array Obsolete files and folders to remove */
    private $coalawebRemoveFiles = array(
        'files' => array(
            'administrator/language/en-GB/en-GB.com_celtawebtraffic.ini',
            'administrator/language/en-GB/en-GB.com_celtawebtraffic.sys.ini',
        ),
        'folders' => array(
            'administrator/components/com_celtawebtraffic',
            'components/com_celtawebtraffic',
            'media/com_celtawebtraffic',
            'media/com_coalawebtraffic',
            'media/mod_coalawebtraffic',
        )
    );
    private $coalawebCliScripts = array(
    );

    /** @var array New files and folders to add */
    private $coalawebAddFiles = array(
        'files' => array(
        ),
        'folders' => array(
        )
    );

    /**
     * Joomla! pre-flight event
     * 
     * @param string $type Installation type (install, update, discover_install)
     * @param JInstaller $parent Parent object
     */
    public function preflight($type, $parent) {
        // Only allow to install on Joomla! 2.5.0 or later with PHP 5.3.0 or later
        if (defined('PHP_VERSION')) {
            $version = PHP_VERSION;
        } elseif (function_exists('phpversion')) {
            $version = phpversion();
        } else {
            $version = '5.0.0'; // all bets are off!
        }

        if (version_compare(JVERSION, '2.5.9', '<')) {
            $msg = "<p>You need Joomla! 2.5.9 or later to install CoalaWeb Traffic</p>";
            JError::raiseWarning(100, $msg);
            return false;
        }

        if ((version_compare(JVERSION, '3.0', '>')) && (version_compare(JVERSION, '3.2', '<'))) {
            $msg = "<p>You need Joomla! 3.2 or later to install CoalaWeb Traffic</p>";
            JLog::add($msg, JLog::WARNING, 'jerror');
            return false;
        }
                
        if (!version_compare($version, '5.3.1', 'ge')) {
            $msg = "<p>You need PHP 5.3.1 or later to install this component</p>";

            if (version_compare(JVERSION, '3.2', 'gt')) {
                JLog::add($msg, JLog::WARNING, 'jerror');
            } else {
                JError::raiseWarning(100, $msg);
            }

            return false;
        }

        // Bugfix for "Can not build admin menus"
        // Workarounds for JInstaller bugs
        if (in_array($type, array('install'))) {
            $this->_bugfixDBFunctionReturnedNoError();
        } elseif ($type != 'discover_install') {
            $this->_bugfixCantBuildAdminMenus();
            $this->_fixBrokenSQLUpdates($parent);
        }

        return true;
    }

    /**
     * Joomla! post-flight event
     * 
     * @param string $type install, update or discover_update
     * @param JInstaller $parent 
     */
    function postflight($type, $parent) {
        // Install subextensions
        $status = $this->_installSubextensions($parent);
        $this->_renameMoveParams();

        // Remove obsolete files and folders
        $this->_removeObsoleteFilesAndFolders($this->coalawebRemoveFiles);
        $this->_renameRemoveOldName();

        // Add new files and folders
        $this->_addNewFilesAndFolders($this->coalawebAddFiles);

        //Copy cli files
        $this->_copyCliFiles($parent);

        //Add default category.
        $this->_addDefaultCat($this->_coalaweb_extension);

        // Show the post-installation page
        $this->_renderPostInstallation($status, $parent);
        
        // Remove duplicate update info
        $this->_removeUpdateSite();
    }

    /**
     * Runs on uninstallation
     * 
     * @param JInstaller $parent 
     */
    function uninstall($parent) {
        // Uninstall subextensions
        $status = $this->_uninstallSubextensions($parent);

        // Show the post-uninstallation page
        $this->_renderPostUninstallation($status, $parent);
    }

    /**
     * Copies the CLI scripts into Joomla!'s cli directory
     * 
     * @param JInstaller $parent 
     */
    private function _copyCliFiles($parent) {
        if (!count($this->coalawebCliScripts))
            return;

        $src = $parent->getParent()->getPath('source');

        jimport("joomla.filesystem.file");
        jimport("joomla.filesystem.folder");

        foreach ($this->coalawebCliScripts as $script) {
            if (JFile::exists(JPATH_ROOT . '/cli/' . $script)) {
                JFile::delete(JPATH_ROOT . '/cli/' . $script);
            }
            if (JFile::exists($src . '/cli/' . $script)) {
                JFile::move($src . '/cli/' . $script, JPATH_ROOT . '/cli/' . $script);
            }
        }
    }

    /**
     * Renders the post-installation message 
     */
    private function _renderPostInstallation($status, $parent) {
        ?>

        <?php $rows = 1; ?>
        <style type="text/css">
            .coalaweb {
                font-family: "Trebuchet MS",Helvetica,sans-serif;
                font-size: 13px !important;
                font-weight: normal !important;
                color: #4D4D4D;
            }

            .coalaweb {
                border: solid #ccc 1px;
                background: #fff;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
                *border-collapse: collapse; /* IE7 and lower */
                border-spacing: 0;
                width: 95%;    
                margin: 7px 15px 15px !important;
            }

            .coalaweb tr:hover {
                background: #E8F6FE;
                -o-transition: all 0.1s ease-in-out;
                -webkit-transition: all 0.1s ease-in-out;
                -moz-transition: all 0.1s ease-in-out;
                -ms-transition: all 0.1s ease-in-out;
                transition: all 0.1s ease-in-out;     
            }    

            .coalaweb tr.row1 {
                background-color: #F0F0EE;
            }

            .coalaweb td, .coalaweb th {
                border-left: 1px solid #ccc;
                border-top: 1px solid #ccc;
                padding: 10px !important;
                text-align: left;    
            }

            .coalaweb th {
                background-image: -webkit-gradient(linear, left top, left bottom, from(#fdfdfd), to(#f4f4f4));
                background-image: -webkit-linear-gradient(top, #fdfdfd, #f4f4f4);
                background-image:    -moz-linear-gradient(top, #fdfdfd, #f4f4f4);
                background-image:     -ms-linear-gradient(top, #fdfdfd, #f4f4f4);
                background-image:      -o-linear-gradient(top, #fdfdfd, #f4f4f4);
                background-image:         linear-gradient( #fdfdfd, #f4f4f4);
                border-top: none;
                color: #333 !important;
                text-shadow: 0px 1px 1px #FFF;
                border-bottom:4px solid rgb(18, 114, 165) !important;
            }

            .coalaweb td:first-child, .coalaweb th:first-child {
                border-left: none;
            }

            .coalaweb th:first-child {
                -moz-border-radius: 3px 0 0 0;
                -webkit-border-radius: 3px 0 0 0;
                border-radius: 3px 0 0 0;
            }

            .coalaweb th:last-child {
                -moz-border-radius: 0 3px 0 0;
                -webkit-border-radius: 0 3px 0 0;
                border-radius: 0 3px 0 0;
            }

            .coalaweb th:only-child{
                -moz-border-radius: 6px 6px 0 0;
                -webkit-border-radius: 6px 6px 0 0;
                border-radius: 6px 6px 0 0;
            }

            .coalaweb tr:last-child td:first-child {
                -moz-border-radius: 0 0 0 3px;
                -webkit-border-radius: 0 0 0 3px;
                border-radius: 0 0 0 3px;
            }

            .coalaweb tr:last-child td:last-child {
                -moz-border-radius: 0 0 3px 0;
                -webkit-border-radius: 0 0 3px 0;
                border-radius: 0 0 3px 0;
            }

            .coalaweb em, .coalaweb strong {
                color:#1272A5;
                font-weight: bold;
            }
        </style>
        <link rel="stylesheet" href="../media/coalaweb/components/generic/css/com-coalaweb-base.css" type="text/css">
        <span class="cw-message">
            <p class="alert">
                <?php echo JText::_('COM_COALAWEBTRAFFIC_POST_INSTALL_MSG'); ?>
            </p>
        </span>
        <table class="coalaweb">
            <thead align="left">
                <tr>
                    <th class="title" align="left">Component</th>
                    <th width="25%">Status</th>
                </tr>
            </thead>
            <tbody>
                <tr class="row0">
                    <td class="key">
                        <?php echo JText::_('COM_COALAWEBTRAFFIC'); ?>
                    </td>
                    <td>
                        <strong style="color: green">Installed</strong>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php if (count($status->modules)) : ?>
            <table class="coalaweb">
                <thead align="left">
                    <tr>
                        <th class="title" align="left">Module</th>
                        <th width="25%">Client</th>
                        <th width="25%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($status->modules as $module) : ?>
                        <tr class="row<?php echo ($rows++ % 2); ?>">
                            <td class="key"><?php echo JText::_($module['name']); ?></td>
                            <td class="key"><?php echo ucfirst($module['client']); ?></td>
                            <td><strong style="color: <?php echo ($module['result']) ? "green" : "red" ?>"><?php echo ($module['result']) ? 'Installed' : 'Not installed'; ?></strong></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
        <?php if (count($status->plugins)) : ?>
            <table class="coalaweb">
                <thead align="left" >
                    <tr>
                        <th class="title" align="left">Plugins</th>
                        <th width="25%">Group</th>
                        <th width="25%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($status->plugins as $plugin) : ?>
                        <tr class="row<?php echo ($rows++ % 2); ?>">
                            <td class="key"><?php echo JText::_($plugin['name']); ?></td>
                            <td class="key"><?php echo ucfirst($plugin['group']); ?></td>
                            <td><strong style="color: <?php echo ($plugin['result']) ? "green" : "red" ?>"><?php echo ($plugin['result']) ? 'Installed' : 'Not installed'; ?></strong></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <?php
    }

    private function _renderPostUninstallation($status, $parent) {
        ?>
        <?php $rows = 0; ?>
        <style type="text/css">
            .coalaweb {
                font-family: "Trebuchet MS",Helvetica,sans-serif;
                font-size: 13px !important;
                font-weight: normal !important;
                color: #4D4D4D;
            }

            .coalaweb {
                border: solid #ccc 1px;
                background: #fff;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
                *border-collapse: collapse; /* IE7 and lower */
                border-spacing: 0;
                width: 95%;    
                margin: 7px 15px 15px !important;
            }

            .coalaweb tr:hover {
                background: #E8F6FE;
                -o-transition: all 0.1s ease-in-out;
                -webkit-transition: all 0.1s ease-in-out; 
                -moz-transition: all 0.1s ease-in-out;
                -ms-transition: all 0.1s ease-in-out;
                transition: all 0.1s ease-in-out;     
            }    

            .coalaweb tr.row1 {
                background-color: #F0F0EE;
            }

            .coalaweb td, .coalaweb th {
                border-left: 1px solid #ccc;
                border-top: 1px solid #ccc;
                padding: 10px !important;
                text-align: left;    
            }

            .coalaweb th {
                background-image: -webkit-gradient(linear, left top, left bottom, from(#fdfdfd), to(#f4f4f4));
                background-image: -webkit-linear-gradient(top, #fdfdfd, #f4f4f4);
                background-image:    -moz-linear-gradient(top, #fdfdfd, #f4f4f4);
                background-image:     -ms-linear-gradient(top, #fdfdfd, #f4f4f4);
                background-image:      -o-linear-gradient(top, #fdfdfd, #f4f4f4);
                background-image:         linear-gradient( #fdfdfd, #f4f4f4);
                border-top: none;
                color: #333 !important;
                text-shadow: 0px 1px 1px #FFF;
                border-bottom:4px solid rgb(18, 114, 165) !important;
            }

            .coalaweb td:first-child, .coalaweb th:first-child {
                border-left: none;
            }

            .coalaweb th:first-child {
                -moz-border-radius: 3px 0 0 0;
                -webkit-border-radius: 3px 0 0 0;
                border-radius: 3px 0 0 0;
            }

            .coalaweb th:last-child {
                -moz-border-radius: 0 3px 0 0;
                -webkit-border-radius: 0 3px 0 0;
                border-radius: 0 3px 0 0;
            }

            .coalaweb th:only-child{
                -moz-border-radius: 6px 6px 0 0;
                -webkit-border-radius: 6px 6px 0 0;
                border-radius: 6px 6px 0 0;
            }

            .coalaweb tr:last-child td:first-child {
                -moz-border-radius: 0 0 0 3px;
                -webkit-border-radius: 0 0 0 3px;
                border-radius: 0 0 0 3px;
            }

            .coalaweb tr:last-child td:last-child {
                -moz-border-radius: 0 0 3px 0;
                -webkit-border-radius: 0 0 3px 0;
                border-radius: 0 0 3px 0;
            }

            .coalaweb em, .coalaweb strong {
                color:#1272A5;
                font-weight: bold;
            }
        </style>
        <span class="cw-slider">
            <h2> CoalaWeb Traffic Uninstallation Status</h2>
        </span>
        <table class="coalaweb">
            <thead align="left">
                <tr>
                    <th class="title" align="left">Component</th>
                    <th width="25%">Status</th>
                </tr>
            </thead>
            <tbody>
                <tr class="row0">
                    <td class="key">
                        <?php echo JText::_('COM_COALAWEBTRAFFIC'); ?>
                    </td>
                    <td>
                        <strong style="color: green">Uninstalled</strong>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php if (count($status->modules)) : ?>
            <table class="coalaweb">
                <thead align="left">
                    <tr>
                        <th class="title" align="left">Modules</th>
                        <th width="25%">Client</th>
                        <th width="25%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($status->modules as $module) : ?>
                        <tr class="row<?php echo ($rows++ % 2); ?>">
                            <td class="key"><?php echo JText::_($module['name']); ?></td>
                            <td class="key"><?php echo ucfirst($module['client']); ?></td>
                            <td><strong style="color: <?php echo ($module['result']) ? "green" : "red" ?>"><?php echo ($module['result']) ? 'Uninstalled' : 'Not uninstalled'; ?></strong></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
        <?php if (count($status->plugins)) : ?>
            <table class="coalaweb">
                <thead align="left" >
                    <tr>
                        <th class="title" align="left">Plugins</th>
                        <th width="25%">Group</th>
                        <th width="25%">Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($status->plugins as $plugin) : ?>
                        <tr class="row<?php echo ($rows++ % 2); ?>">
                            <td class="key"><?php echo JText::_($plugin['name']); ?></td>
                            <td class="key"><?php echo ucfirst($plugin['group']); ?></td>
                            <td><strong style="color: <?php echo ($plugin['result']) ? "green" : "red" ?>"><?php echo ($plugin['result']) ? 'Uninstalled' : 'Not uninstalled'; ?></strong></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <?php
    }

    /**
     * CoalaWeb Traffic 0.1.5+ remove old named extensions"
     */
    private function _renameRemoveOldName() {
        $db = JFactory::getDbo();

        // Remove old component
        // Remove old #__assets records
        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__assets')
                ->where($db->qn('name') . ' = ' . $db->q($this->_old_component));
        $db->setQuery($query);
        $ids = $db->loadColumn();
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $query = $db->getQuery(true);
                $query->delete('#__assets')
                        ->where($db->qn('id') . ' = ' . $db->q($id));
                $db->setQuery($query);
                $db->query();
            }
        }
        // Remove old #__extensions records
        $query = $db->getQuery(true);
        $query->select('extension_id')
                ->from('#__extensions')
                ->where($db->qn('element') . ' = ' . $db->q($this->_old_component));
        $db->setQuery($query);
        $ids = $db->loadColumn();
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $query = $db->getQuery(true);
                $query->delete('#__extensions')
                        ->where($db->qn('extension_id') . ' = ' . $db->q($id));
                $db->setQuery($query);
                $db->query();
            }
        }

        // Remove old #__menu records
        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__menu')
                ->where($db->qn('type') . ' = ' . $db->q('component'))
                ->where($db->qn('menutype') . ' = ' . $db->q('main'))
                ->where($db->qn('link') . ' LIKE ' . $db->q('index.php?option=' . $this->_old_component));
        $db->setQuery($query);
        $ids = $db->loadColumn();
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $query = $db->getQuery(true);
                $query->delete('#__menu')
                        ->where($db->qn('id') . ' = ' . $db->q($id));
                $db->setQuery($query);
                $db->query();
            }
        }

        //Remove old module
        $status = new JObject();
        $status->modules = array();
        // Find the module ID
        $sql = $db->getQuery(true)
                ->select($db->qn('extension_id'))
                ->from($db->qn('#__extensions'))
                ->where($db->qn('element') . ' = ' . $db->q($this->_old_module))
                ->where($db->qn('type') . ' = ' . $db->q('module'));
        $db->setQuery($sql);
        $id = $db->loadResult();
        // Uninstall the module
        if ($id) {
            $installer = new JInstaller;
            $result = $installer->uninstall('module', $id, 1);
            $status->modules[] = array(
                'name' => $this->_old_module,
                'client' => 'site',
                'result' => $result
            );
        }
    }

    private function _renameMoveParams() {
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('extension_id')
                ->from('#__extensions')
                ->where($db->qn('element') . ' = ' . $db->q($this->_old_component));
        $db->setQuery($query);
        $idext = $db->loadColumn();
        if (!empty($idext)) {
            //Lets transfer the component settings from the old to the new
            $query = $db->getQuery(true);
            $query->update('#__extensions AS target');
            $query->leftJoin('#__extensions AS source ON source.element =' . $db->quote($this->_old_component));
            $query->set('target.params = source.params');
            $query->where('target.element =' . $db->quote($this->_coalaweb_extension));
            $db->setQuery($query);
            $db->query();
        }

        $query = $db->getQuery(true);
        $query->select('extension_id')
                ->from('#__extensions')
                ->where($db->qn('element') . ' = ' . $db->q($this->_old_module));
        $db->setQuery($query);
        $idmod = $db->loadColumn();
        if (!empty($idmod)) {
            //Lets transfer the module settings from the old to the new
            $query = $db->getQuery(true);
            $query->update('#__modules AS target');
            $query->leftJoin('#__modules AS source ON source.module =' . $db->quote($this->_old_module));
            $query->set('target.params = source.params');
            $query->set('target.title = source.title');
            $query->set('target.position= source.position');
            $query->set('target.published = source.published');
            $query->set('target.access = source.access');
            $query->set('target.showtitle = source.showtitle');
            $query->set('target.language = source.language');
            $query->where('target.module =' . $db->quote($this->_new_module));
            $db->setQuery($query);
            $db->query();
        }
    }

    /**
     * Joomla! 1.6+ bugfix for "DB function returned no error"
     */
    private function _bugfixDBFunctionReturnedNoError() {
        $db = JFactory::getDbo();

        // Fix broken #__assets records
        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__assets')
                ->where($db->qn('name') . ' = ' . $db->q($this->_coalaweb_extension));
        $db->setQuery($query);
        try {
            $ids = $db->loadColumn();
        } catch (Exception $exc) {
            return;
        }

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $query = $db->getQuery(true);
                $query->delete('#__assets')
                        ->where($db->qn('id') . ' = ' . $db->q($id));
                $db->setQuery($query);
                try {
                    $db->execute();
                } catch (Exception $exc) {
                    // Nothing
                }
            }
        }

        // Fix broken #__extensions records
        $query = $db->getQuery(true);
        $query->select('extension_id')
                ->from('#__extensions')
                ->where($db->qn('element') . ' = ' . $db->q($this->_coalaweb_extension));
        $db->setQuery($query);
        $ids = $db->loadColumn();
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $query = $db->getQuery(true);
                $query->delete('#__extensions')
                        ->where($db->qn('extension_id') . ' = ' . $db->q($id));
                $db->setQuery($query);

                try {
                    $db->execute();
                } catch (Exception $exc) {
                    // Nothing
                }
            }
        }

        // Fix broken #__menu records
        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__menu')
                ->where($db->qn('type') . ' = ' . $db->q('component'))
                ->where($db->qn('menutype') . ' = ' . $db->q('main'))
                ->where($db->qn('link') . ' LIKE ' . $db->q('index.php?option=' . $this->_coalaweb_extension));
        $db->setQuery($query);
        $ids = $db->loadColumn();
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $query = $db->getQuery(true);
                $query->delete('#__menu')
                        ->where($db->qn('id') . ' = ' . $db->q($id));
                $db->setQuery($query);

                try {
                    $db->execute();
                } catch (Exception $exc) {
                    // Nothing
                }
            }
        }
    }

    /**
     * Joomla! 1.6+ bugfix for "Can not build admin menus"
     */
    private function _bugfixCantBuildAdminMenus() {
        $db = JFactory::getDbo();

        // If there are multiple #__extensions record, keep one of them
        $query = $db->getQuery(true);
        $query->select('extension_id')
                ->from('#__extensions')
                ->where($db->qn('element') . ' = ' . $db->q($this->_coalaweb_extension));
        $db->setQuery($query);

        try {
            $ids = $db->loadColumn();
        } catch (Exception $exc) {
            return;
        }


        if (count($ids) > 1) {
            asort($ids);
            $extension_id = array_shift($ids); // Keep the oldest id

            foreach ($ids as $id) {
                $query = $db->getQuery(true);
                $query->delete('#__extensions')
                        ->where($db->qn('extension_id') . ' = ' . $db->q($id));
                $db->setQuery($query);

                try {
                    $db->execute();
                } catch (Exception $exc) {
                    // Nothing
                }
            }
        }

        // If there are multiple assets records, delete all except the oldest one
        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__assets')
                ->where($db->qn('name') . ' = ' . $db->q($this->_coalaweb_extension));
        $db->setQuery($query);
        $ids = $db->loadObjectList();

        if (count($ids) > 1) {
            asort($ids);
            $asset_id = array_shift($ids); // Keep the oldest id

            foreach ($ids as $id) {
                $query = $db->getQuery(true);
                $query->delete('#__assets')
                        ->where($db->qn('id') . ' = ' . $db->q($id));
                $db->setQuery($query);

                try {
                    $db->execute();
                } catch (Exception $exc) {
                    // Nothing
                }
            }
        }

        // Remove #__menu records for good measure!
        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__menu')
                ->where($db->qn('type') . ' = ' . $db->q('component'))
                ->where($db->qn('menutype') . ' = ' . $db->q('main'))
                ->where($db->qn('link') . ' LIKE ' . $db->q('index.php?option=' . $this->_coalaweb_extension));
        $db->setQuery($query);

        try {
            $ids1 = $db->loadColumn();
        } catch (Exception $exc) {
            $ids1 = array();
        }

        if (empty($ids1)) {
            $ids1 = array();
        }

        $query = $db->getQuery(true);
        $query->select('id')
                ->from('#__menu')
                ->where($db->qn('type') . ' = ' . $db->q('component'))
                ->where($db->qn('menutype') . ' = ' . $db->q('main'))
                ->where($db->qn('link') . ' LIKE ' . $db->q('index.php?option=' . $this->_coalaweb_extension . '&%'));
        $db->setQuery($query);

        try {
            $ids2 = $db->loadColumn();
        } catch (Exception $exc) {
            $ids2 = array();
        }

        if (empty($ids2)) {
            $ids2 = array();
        }

        $ids = array_merge($ids1, $ids2);

        if (!empty($ids)) {
            foreach ($ids as $id) {
                $query = $db->getQuery(true);
                $query->delete('#__menu')
                        ->where($db->qn('id') . ' = ' . $db->q($id));
                $db->setQuery($query);

                try {
                    $db->execute();
                } catch (Exception $exc) {
                    // Nothing
                }
            }
        }
    }

    /**
     * Installs subextensions (modules, plugins) bundled with the main extension
     * 
     * @param JInstaller $parent 
     * @return JObject The subextension installation status
     */
    private function _installSubextensions($parent) {
        $src = $parent->getParent()->getPath('source');

        $db = JFactory::getDbo();

        $status = new JObject();
        $status->modules = array();
        $status->plugins = array();

        // Modules installation
        if (count($this->installation_queue['modules'])) {
            foreach ($this->installation_queue['modules'] as $folder => $modules) {
                if (count($modules)) {
                    foreach ($modules as $module => $modulePreferences) {
                        // Install the module
                        if (empty($folder)) {
                            $folder = 'site';
                        }

                        $path = "$src/modules/$folder/$module";

                        if (!is_dir($path)) {
                            $path = "$src/modules/$folder/mod_$module";
                        }

                        if (!is_dir($path)) {
                            $path = "$src/modules/$module";
                        }

                        if (!is_dir($path)) {
                            $path = "$src/modules/mod_$module";
                        }

                        if (!is_dir($path)) {
                            continue;
                        }

                        // Was the module already installed?
                        $sql = $db->getQuery(true)
                                ->select('COUNT(*)')
                                ->from('#__modules')
                                ->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
                        $db->setQuery($sql);

                        try {
                            $count = $db->loadResult();
                        } catch (Exception $exc) {
                            $count = 0;
                        }

                        $installer = new JInstaller;
                        $result = $installer->install($path);
                        $status->modules[] = array(
                            'name' => 'mod_' . $module,
                            'client' => $folder,
                            'result' => $result
                        );

                        // Modify where it's published and its published state
                        if (!$count) {
                            // A. Position and state
                            list($modulePosition, $modulePublished) = $modulePreferences;

                            if ($modulePosition == 'cpanel') {
                                $modulePosition = 'icon';
                            }

                            $sql = $db->getQuery(true)
                                    ->update($db->qn('#__modules'))
                                    ->set($db->qn('position') . ' = ' . $db->q($modulePosition))
                                    ->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));

                            if ($modulePublished) {
                                $sql->set($db->qn('published') . ' = ' . $db->q('1'));
                            }

                            $db->setQuery($sql);

                            try {
                                $db->execute();
                            } catch (Exception $exc) {
                                // Nothing
                            }

                            // B. Change the ordering of back-end modules to 1 + max ordering
                            if ($folder == 'admin') {
                                try {
                                    $query = $db->getQuery(true);
                                    $query->select('MAX(' . $db->qn('ordering') . ')')
                                            ->from($db->qn('#__modules'))
                                            ->where($db->qn('position') . '=' . $db->q($modulePosition));
                                    $db->setQuery($query);
                                    $position = $db->loadResult();
                                    $position++;

                                    $query = $db->getQuery(true);
                                    $query->update($db->qn('#__modules'))
                                            ->set($db->qn('ordering') . ' = ' . $db->q($position))
                                            ->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
                                    $db->setQuery($query);
                                    $db->execute();
                                } catch (Exception $exc) {
                                    // Nothing
                                }
                            }

                            // C. Link to all pages
                            try {
                                $query = $db->getQuery(true);
                                $query->select('id')->from($db->qn('#__modules'))
                                        ->where($db->qn('module') . ' = ' . $db->q('mod_' . $module));
                                $db->setQuery($query);
                                $moduleid = $db->loadResult();

                                $query = $db->getQuery(true);
                                $query->select('*')->from($db->qn('#__modules_menu'))
                                        ->where($db->qn('moduleid') . ' = ' . $db->q($moduleid));
                                $db->setQuery($query);
                                $assignments = $db->loadObjectList();
                                $isAssigned = !empty($assignments);
                                if (!$isAssigned) {
                                    $o = (object) array(
                                                'moduleid' => $moduleid,
                                                'menuid' => 0
                                    );
                                    $db->insertObject('#__modules_menu', $o);
                                }
                            } catch (Exception $exc) {
                                // Nothing
                            }
                        }
                    }
                }
            }
        }

        // Plugins installation
        if (count($this->installation_queue['plugins'])) {
            foreach ($this->installation_queue['plugins'] as $folder => $plugins) {
                if (count($plugins)) {
                    foreach ($plugins as $plugin => $published) {
                        $path = "$src/plugins/$folder/$plugin";

                        if (!is_dir($path)) {
                            $path = "$src/plugins/$folder/plg_$plugin";
                        }

                        if (!is_dir($path)) {
                            $path = "$src/plugins/$plugin";
                        }

                        if (!is_dir($path)) {
                            $path = "$src/plugins/plg_$plugin";
                        }

                        if (!is_dir($path)) {
                            continue;
                        }

                        // Was the plugin already installed?
                        $query = $db->getQuery(true)
                                ->select('COUNT(*)')
                                ->from($db->qn('#__extensions'))
                                ->where($db->qn('element') . ' = ' . $db->q($plugin))
                                ->where($db->qn('folder') . ' = ' . $db->q($folder));
                        $db->setQuery($query);

                        try {
                            $count = $db->loadResult();
                        } catch (Exception $exc) {
                            $count = 0;
                        }

                        $installer = new JInstaller;
                        $result = $installer->install($path);

                        $status->plugins[] = array('name' => 'plg_' . $plugin, 'group' => $folder, 'result' => $result);

                        if ($published && !$count) {
                            $query = $db->getQuery(true)
                                    ->update($db->qn('#__extensions'))
                                    ->set($db->qn('enabled') . ' = ' . $db->q('1'))
                                    ->where($db->qn('element') . ' = ' . $db->q($plugin))
                                    ->where($db->qn('folder') . ' = ' . $db->q($folder));
                            $db->setQuery($query);

                            try {
                                $db->execute();
                            } catch (Exception $exc) {
                                // Nothing
                            }
                        }
                    }
                }
            }
        }

        return $status;
    }

    /**
     * Uninstalls subextensions (modules, plugins) bundled with the main extension
     * 
     * @param JInstaller $parent 
     * @return JObject The subextension uninstallation status
     */
    private function _uninstallSubextensions($parent) {
        jimport('joomla.installer.installer');

        $db = JFactory::getDBO();

        $status = new JObject();
        $status->modules = array();
        $status->plugins = array();

        $src = $parent->getParent()->getPath('source');

        // Modules uninstallation
        if (count($this->uninstallation_queue['modules'])) {
            foreach ($this->uninstallation_queue['modules'] as $folder => $modules) {
                if (count($modules)) {
                    foreach ($modules as $module => $modulePreferences) {
                        // Find the module ID
                        $sql = $db->getQuery(true)
                                ->select($db->qn('extension_id'))
                                ->from($db->qn('#__extensions'))
                                ->where($db->qn('element') . ' = ' . $db->q('mod_' . $module))
                                ->where($db->qn('type') . ' = ' . $db->q('module'));
                        $db->setQuery($sql);
                        $id = $db->loadResult();
                        // Uninstall the module
                        if ($id) {
                            $installer = new JInstaller;
                            $result = $installer->uninstall('module', $id, 1);
                            $status->modules[] = array(
                                'name' => 'mod_' . $module,
                                'client' => $folder,
                                'result' => $result
                            );
                        }
                    }
                }
            }
        }

        // Plugins uninstallation
        if (count($this->uninstallation_queue['plugins'])) {
            foreach ($this->uninstallation_queue['plugins'] as $folder => $plugins) {
                if (count($plugins)) {
                    foreach ($plugins as $plugin => $published) {
                        $sql = $db->getQuery(true)
                                ->select($db->qn('extension_id'))
                                ->from($db->qn('#__extensions'))
                                ->where($db->qn('type') . ' = ' . $db->q('plugin'))
                                ->where($db->qn('element') . ' = ' . $db->q($plugin))
                                ->where($db->qn('folder') . ' = ' . $db->q($folder));
                        $db->setQuery($sql);

                        $id = $db->loadResult();
                        if ($id) {
                            $installer = new JInstaller;
                            $result = $installer->uninstall('plugin', $id, 1);
                            $status->plugins[] = array(
                                'name' => 'plg_' . $plugin,
                                'group' => $folder,
                                'result' => $result
                            );
                        }
                    }
                }
            }
        }

        return $status;
    }

    /**
     * Removes obsolete files and folders
     * 
     * @param array $coalawebRemoveFiles 
     */
    private function _removeObsoleteFilesAndFolders($coalawebRemoveFiles) {
        // Remove files
        jimport('joomla.filesystem.file');
        if (!empty($coalawebRemoveFiles['files'])) {
            foreach ($coalawebRemoveFiles['files'] as $file) {
                $f = JPATH_ROOT . '/' . $file;
                if (!JFile::exists($f)) {
                    continue;
                }
                JFile::delete($f);
            }
        }
        // Remove folders
        jimport('joomla.filesystem.folder');
        if (!empty($coalawebRemoveFiles['folders'])) {
            foreach ($coalawebRemoveFiles['folders'] as $folder) {
                $f = JPATH_ROOT . '/' . $folder;
                if (!JFolder::exists($f)) {
                    continue;
                }
                JFolder::delete($f);
            }
        }
    }

    /**
     * Add new files and folders
     * 
     * @param array $coalawebAddFiles 
     */
    private function _addNewFilesAndFolders($coalawebAddFiles) {
        // Add files
        jimport('joomla.filesystem.file');
        if (!empty($coalawebAddFiles['files'])) {
            foreach ($coalawebAddFiles['files'] as $file) {
                $f = JPATH_ROOT . '/' . $file;
                if (JFile::exists($f)) {
                    continue;
                }
                JFile::create($f);
            }
        }
        // Add folders
        jimport('joomla.filesystem.folder');
        if (!empty($coalawebAddFiles['folders'])) {
            foreach ($coalawebAddFiles['folders'] as $folder) {
                $f = JPATH_ROOT . '/' . $folder;
                if (JFolder::exists($f)) {
                    continue;
                }
                JFolder::create($f);
            }
        }
    }

    private function _addDefaultCat($extension) {
        
        //Check if the base category already exists.
        $db = JFactory::getDBO();
        $query = $db->getQuery(true)
                ->select('COUNT(*)')
                ->from($db->qn('#__categories'))
                ->where($db->qn('extension') . ' = ' . $db->q($extension));
        $db->setQuery($query);
        $id = $db->loadResult();
        
        if (!$id) {
            // Create "uncategorised" category for component
            $basePath = JPATH_ADMINISTRATOR . '/components/com_categories';
            require_once $basePath . '/models/category.php';
            $config = array('table_path' => $basePath . '/tables');
            $catmodel = new CategoriesModelCategory($config);

            $catData = array('id' => 0, 'parent_id' => 0, 'level' => 1,
                'path' => 'uncategorised', 'extension' => $extension,
                'title' => 'Uncategorised', 'alias' => 'uncategorised',
                'description' => '', 'published' => 1, 'language' => '*');
            $status = $catmodel->save($catData);

            if (!$status) {
                JError::raiseWarning(500, JText::_('COM_COALAWEBTRAFFIC_DEFAULT_CAT_ERROR_MSG'));
            }
        }
    }
    
    /**
	 * Fixed failed install/update of database
	 * 
	 */
	private function _fixBrokenSQLUpdates($parent){
		// Get the extension ID
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);
		$query->select('extension_id')
			->from('#__extensions')
			->where($db->qn('element').' = '.$db->q($this->_coalaweb_extension));
		$db->setQuery($query);
		$eid = $db->loadResult();

		if (!$eid){
			return;
		}

		// Get the schema version
		$query = $db->getQuery(true);
		$query->select('version_id')
			->from('#__schemas')
			->where('extension_id = ' . $eid);
		$db->setQuery($query);
		$version = $db->loadResult();

		// If there is a schema version it's not a false update
		if ($version){
			return;
		}

		// Execute the installation SQL file.
		$dbDriver = strtolower($db->name);

		if ($dbDriver == 'mysqli'){
			$dbDriver = 'mysql';
		}


		// Get the name of the sql file to process
		$sqlfile = $parent->getParent()->getPath('source') . '/administrator/sql/install/' . $dbDriver . '/install.mysql.utf8.sql';
		if (file_exists($sqlfile)){
			$buffer = file_get_contents($sqlfile);
			if ($buffer === false){
				return;
			}

			$queries = JInstallerHelper::splitSql($buffer);

			if (count($queries) == 0){
				// No queries to process
				return;
			}

			// Process each query in the $queries array (split out of sql file).
			foreach ($queries as $query){
				$query = trim($query);

				if ($query != '' && $query{0} != '#'){
					$db->setQuery($query);

					if (!$db->execute()){
						JError::raiseWarning(1, JText::sprintf('JLIB_INSTALLER_ERROR_SQL_ERROR', $db->stderr(true)));

						return false;
					}
				}
			}
		}

		// Update #__schemas to the latest version.
		$path = $parent->getParent()->getPath('source') . '/administrator/sql/update/' . $dbDriver;
		$files = str_replace('.sql', '', JFolder::files($path, '\.sql$'));
		if(count($files) > 0){
			usort($files, 'version_compare');
			$version = array_pop($files);
		}else{
			$version = '0.0.1';
		}

		$query = $db->getQuery(true);
		$query->insert($db->quoteName('#__schemas'));
		$query->columns(array($db->quoteName('extension_id'), $db->quoteName('version_id')));
		$query->values($eid . ', ' . $db->quote($version));
		$db->setQuery($query);
		$db->execute();
	}
        
     private function _removeUpdateSite() {
        // Get some info on all the stuff we've gotta delete
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
                ->select(array(
                    $db->qn('s') . '.' . $db->qn('update_site_id'),
                    $db->qn('e') . '.' . $db->qn('extension_id'),
                    $db->qn('e') . '.' . $db->qn('element'),
                    $db->qn('s') . '.' . $db->qn('location'),
                ))
                ->from($db->qn('#__update_sites') . ' AS ' . $db->qn('s'))
                ->join('INNER', $db->qn('#__update_sites_extensions') . ' AS ' . $db->qn('se') . ' ON(' .
                        $db->qn('se') . '.' . $db->qn('update_site_id') . ' = ' .
                        $db->qn('s') . '.' . $db->qn('update_site_id')
                        . ')')
                ->join('INNER', $db->qn('#__extensions') . ' AS ' . $db->qn('e') . ' ON(' .
                        $db->qn('e') . '.' . $db->qn('extension_id') . ' = ' .
                        $db->qn('se') . '.' . $db->qn('extension_id')
                        . ')')
                ->where($db->qn('s') . '.' . $db->qn('type') . ' = ' . $db->q('extension'))
                ->where($db->qn('e') . '.' . $db->qn('type') . ' = ' . $db->q('component'))
                ->where($db->qn('e') . '.' . $db->qn('element') . ' = ' . $db->q($this->_coalaweb_extension))
                ->where($db->qn('s') . '.' . $db->qn('location') . ' = ' . $db->q($this->_update_remove))
        ;
        $db->setQuery($query);
        $oResult = $db->loadObject();

        // If no record is found, do nothing. We've already killed the monster!
        if (is_null($oResult)) {
            return;
        }

        // Delete the #__update_sites record
        $query = $db->getQuery(true)
                ->delete($db->qn('#__update_sites'))
                ->where($db->qn('update_site_id') . ' = ' . $db->q($oResult->update_site_id));
        $db->setQuery($query);
        try {
            $db->query();
        } catch (Exception $exc) {
            // If the query fails, don't sweat about it
        }

        // Delete the #__update_sites_extensions record
        $query = $db->getQuery(true)
                ->delete($db->qn('#__update_sites_extensions'))
                ->where($db->qn('update_site_id') . ' = ' . $db->q($oResult->update_site_id));
        $db->setQuery($query);
        try {
            $db->query();
        } catch (Exception $exc) {
            // If the query fails, don't sweat about it
        }

        // Delete the #__updates records
        $query = $db->getQuery(true)
                ->delete($db->qn('#__updates'))
                ->where($db->qn('update_site_id') . ' = ' . $db->q($oResult->update_site_id));
        $db->setQuery($query);
        try {
            $db->query();
        } catch (Exception $exc) {
         // If the query fails, don't sweat about it
        }
    }

}

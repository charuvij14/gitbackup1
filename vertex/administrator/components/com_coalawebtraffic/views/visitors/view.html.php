<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Visitors View
 */
class CoalawebtrafficViewVisitors extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->knownips = $this->get('Knownips');
        $this->pagination = $this->get('Pagination');
        
        if (version_compare(JVERSION, '3.0', '>')) {
            CoalawebtrafficHelper::addSubmenu('visitors');
        }

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            JError::raiseError(500, implode("\n", $errors));
            return false;
        }
        
        $this->addToolbar();
        if (version_compare(JVERSION, '3.0', '>')) {
            $this->sidebar = JHtmlSidebar::render();
        }
        parent::display($tpl);
    }

    /**
     * Setting the toolbar
     */
    protected function addToolBar() {
        $canDo = CoalawebtrafficHelper::getActions();
        $state = $this->get('State');
        $bar = JToolBar::getInstance('toolbar');
        
        if (version_compare(JVERSION, '3.0', '>')) {
           JToolBarHelper::title(JText::_('COM_CWTRAFFIC_TITLE_MAIN') . ' [ ' . JText::_('COM_CWTRAFFIC_TITLE_VISITORS') . ' ]', 'users'); 
        }else{
            JToolBarHelper::title(JText::_('COM_CWTRAFFIC_TITLE_MAIN') . ' [ ' . JText::_('COM_CWTRAFFIC_TITLE_VISITORS') . ' ]', 'cwt-visitors');
        }

       if (version_compare(JVERSION, '3.0', '>')) {
            $bar->appendButton('Link', 'wrench', 'COM_CWTRAFFIC_TITLE_CPANEL', 'index.php?option=com_coalawebtraffic');
        } else {
            JToolBarHelper::back('COM_CWTRAFFIC_TITLE_CPANEL', 'index.php?option=com_coalawebtraffic');
        }

        if ($canDo->get('core.delete')) {
            JToolBarHelper::divider();
            JToolBarHelper::deleteList('', 'visitors.delete', 'JTOOLBAR_DELETE');
        }
        
        if ($canDo->get('core.admin')) {
            if (version_compare(JVERSION, '3.0', '>')) {
            $bar->appendButton('Link', 'bars', 'COM_CWTRAFFIC_TITLE_REPORTALL', 'index.php?option=com_coalawebtraffic&task=visitors.csvreportall');
            } else {
            $bar->appendButton('Link', 'export', 'COM_CWTRAFFIC_TITLE_REPORTALL', 'index.php?option=com_coalawebtraffic&task=visitors.csvreportall');
            }
        }
        
        if ($canDo->get('core.admin')) {
            if (version_compare(JVERSION, '3.0', '>')) {
            $bar->appendButton('Link', 'bars', 'COM_CWTRAFFIC_TITLE_REPORTFILTER', 'index.php?option=com_coalawebtraffic&task=visitors.csvreport');
            } else {
            $bar->appendButton('Link', 'export', 'COM_CWTRAFFIC_TITLE_REPORTFILTER', 'index.php?option=com_coalawebtraffic&task=visitors.csvreport');
            }
        }
        
        if ($canDo->get('core.admin')) {
            JToolBarHelper::divider();
            JToolBarHelper::preferences('com_coalawebtraffic');
        }

        $help_url = 'http://coalaweb.com/support/documentation/item/coalaweb-traffic-guide';
        JToolBarHelper::help('COM_CWTRAFFIC_TITLE_HELP', false, $help_url);

        if (version_compare(JVERSION, '3.0', '>')) {
            JHtmlSidebar::setAction('index.php?option=com_coalawebtraffic&view=visitors');

        }
        
                
    }
    
    
    protected function getSortFields() {
        if (version_compare(JVERSION, '3.0', '>')) {
            return array(
                'a.ip' => JText::_('COM_CWTRAFFIC_VISITOR_IP'),
                'a.tm' => JText::_('COM_CWTRAFFIC_VISITOR_DATE'),
                'w.visitors' => JText::_('COM_CWTRAFFIC_IP_OWNER'),
                'a.country_name' => JText::_('COM_CWTRAFFIC_HEADER_LOCATION')
            );
        }
    }

}

<?php

defined('_JEXEC') or die('Restricted Access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
jimport('joomla.application.component.view');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

class CoalawebtrafficViewGeoupload extends JViewLegacy {

    public function display($tpl = null) {

        if ($this->geodatExist('geoip')) {
            $mymod = $this->geodatMod('geoip');
            echo JText::sprintf('COM_CWTRAFFIC_YESGEO_UPLOAD_MESSAGE', $mymod);
        } else {
            echo JText::_('COM_CWTRAFFIC_NOGEO_UPLOAD_MESSAGE');
        }
        
        if (version_compare(JVERSION, '3.0', '>')) {
            CoalawebtrafficHelper::addSubmenu('geoupload');
        }
        
        // Set the toolbar
        $this->addToolBar();
        if (version_compare(JVERSION, '3.0', '>')) {
            $this->sidebar = JHtmlSidebar::render();
        }
        $this->jsOptions['url'] = JURI::base();
        parent::display($tpl);
    }

    protected function addToolbar() {
        $canDo = CoalawebtrafficHelper::getActions();
        
        if (version_compare(JVERSION, '3.0', '>')) {
            JToolBarHelper::title(JText::_('COM_CWTRAFFIC_TITLE_MAIN') . ' [ ' . JText::_('COM_CWTRAFFIC_TITLE_GEO') . ' ]', 'contract');
        }else{
            JToolBarHelper::title(JText::_('COM_CWTRAFFIC_TITLE_MAIN') . ' [ ' . JText::_('COM_CWTRAFFIC_TITLE_GEO') . ' ]', 'cwt-geo');
        }
        
        if (version_compare(JVERSION, '3.0', '>')) {
            $bar = JToolBar::getInstance('toolbar');
            $bar->appendButton('Link', 'wrench', 'COM_CWTRAFFIC_TITLE_CPANEL', 'index.php?option=com_coalawebtraffic');
        } else {
            JToolBarHelper::back('COM_CWTRAFFIC_TITLE_CPANEL', 'index.php?option=com_coalawebtraffic');
        }

        if ($canDo->get('core.admin')) {
            if ($this->filesExist('archives')) {
                JToolBarHelper::divider();
                if (version_compare(JVERSION, '3.0', '>')) {
                    JToolBarHelper::custom('geoupload.unzip', 'box-add', 'box-add', 'Unzip', false);
                } else {
                    JToolBarHelper::custom('geoupload.unzip', 'cwt-extract', 'unzip', 'Unzip', false);
                }
            }
        }
        if ($canDo->get('core.admin')) {
            JToolBarHelper::divider();
            JToolBarHelper::preferences('com_coalawebtraffic');
        }

        $help_url = 'http://coalaweb.com/support/documentation/item/coalaweb-traffic-guide';
        JToolBarHelper::help('COM_CWTRAFFIC_TITLE_HELP', false, $help_url);
    }

    /**
     * Checks if folder + files exist in the com_coalawebtraffic tmp path
     * @param $type
     * @return bool
     */
    private function filesExist($type) {
        $path = JFactory::getConfig()->get('tmp_path') . '/com_coalawebtraffic/' . $type;
        if (JFolder::exists($path)) {
            if (JFolder::folders($path, '.', false) || JFolder::files($path, '.', false)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if geolitecity.dat file exists
     * @param $geo
     * @return bool
     */
    private function geodatExist($geo) {
        //Let check if GeoIp already exsit on the server.
        if (function_exists('geoip_record_by_name')) {
            return true;
        }
        //If not lets check ours.
        $path = JPATH_COMPONENT_ADMINISTRATOR . '/assets/' . $geo;
        if (JFolder::files($path, 'geolitecity.dat', false)) {
            return true;
        }
        return false;
    }

    /**
     * Returns modified date for file
     * @param $geo
     * @return $mod
     */
    private function geodatMod($geo) {
        
        //Let check if GeoIp already exsit on the server and reflect that in the date.
        if (function_exists('geoip_record_by_name')) {
                return date("F d Y", filemtime(geoip_db_filename(GEOIP_COUNTRY_EDITION)));
        }
       //If not lets update the date on ours.   
        $path = JPATH_COMPONENT_ADMINISTRATOR . '/assets/'. $geo;
        $mod = date("F d Y", filemtime($path . '/geolitecity.dat'));

        return $mod;
    }

    function curlInstalled() {
        if (in_array('curl', get_loaded_extensions())) {
            return true;
        } else {
            return false;
        }
    }

}
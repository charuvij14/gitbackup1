<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
// import Joomla view library
jimport('joomla.application.component.view');

class CoalawebtrafficViewKnownips extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        if (version_compare(JVERSION, '3.0', '>')) {
            CoalawebtrafficHelper::addSubmenu('knownips');
        }

        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
            JError::raiseError(500, implode("\n", $errors));
            return false;
        }

        $this->addToolbar();
        if (version_compare(JVERSION, '3.0', '>')) {
            $this->sidebar = JHtmlSidebar::render();
        }
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @since	1.6
     */
    protected function addToolbar() {
        require_once JPATH_COMPONENT . '/helpers/coalawebtraffic.php';

        $state = $this->get('State');
        $canDo = CoalawebtrafficHelper::getActions($state->get('filter.category_id'));
        $user = JFactory::getUser();

        if (version_compare(JVERSION, '3.0', '>')) {
            // Get the toolbar object instance
            $bar = JToolBar::getInstance('toolbar');
        }

        if (version_compare(JVERSION, '3.0', '>')) {
           JToolBarHelper::title(JText::_('COM_CWTRAFFIC_TITLE_MAIN') . ' [ ' . JText::_('COM_CWTRAFFIC_TITLE_KNOWNIPS') . ' ]', 'eye'); 
        }else{
            JToolBarHelper::title(JText::_('COM_CWTRAFFIC_TITLE_MAIN') . ' [ ' . JText::_('COM_CWTRAFFIC_TITLE_KNOWNIPS') . ' ]', 'cwt-knownip');
        }

        if (version_compare(JVERSION, '3.0', '>')) {
            $bar->appendButton('Link', 'wrench', 'COM_CWTRAFFIC_TITLE_CPANEL', 'index.php?option=com_coalawebtraffic');
        } else {
            JToolBarHelper::back('COM_CWTRAFFIC_TITLE_CPANEL', 'index.php?option=com_coalawebtraffic');
        }

        if (count($user->getAuthorisedCategories('com_coalawebtraffic', 'core.create')) > 0) {
            JToolBarHelper::addNew('knownip.add');
        }
        
        if ($canDo->get('core.edit')) {
            JToolBarHelper::editList('knownip.edit');
        }
        
        if ($canDo->get('core.edit.state')) {

            JToolBarHelper::divider();
            JToolBarHelper::publish('knownips.publish', 'JTOOLBAR_PUBLISH', true);
            JToolBarHelper::unpublish('knownips.unpublish', 'JTOOLBAR_UNPUBLISH', true);

            JToolBarHelper::divider();
            JToolBarHelper::archiveList('knownips.archive');
            JToolBarHelper::checkin('knownips.checkin');
        }

        if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
            JToolBarHelper::deleteList('', 'knownips.delete', 'JTOOLBAR_EMPTY_TRASH');
            JToolBarHelper::divider();
        } elseif ($canDo->get('core.edit.state')) {
            JToolBarHelper::trash('knownips.trash');
            JToolBarHelper::divider();
        }

        if (version_compare(JVERSION, '3.0', '>')) {
            // Add a batch button
            if ($user->authorise('core.create', 'com_coalawebtraffic') && $user->authorise('core.edit', 'com_coalawebtraffic') && $user->authorise('core.edit.state', 'com_coalawebtraffic')) {
                JHtml::_('bootstrap.modal', 'collapseModal');
                $title = JText::_('JTOOLBAR_BATCH');

                // Instantiate a new JLayoutFile instance and render the batch button
                $layout = new JLayoutFile('joomla.toolbar.batch');

                $dhtml = $layout->render(array('title' => $title));
                $bar->appendButton('Custom', $dhtml, 'batch');
            }
        }

        if ($canDo->get('core.admin')) {
            JToolBarHelper::preferences('com_coalawebtraffic');
            JToolBarHelper::divider();
        }

        $help_url = 'http://coalaweb.com/support/documentation/item/coalaweb-traffic-guide';
        JToolBarHelper::help('COM_CWTRAFFIC_TITLE_HELP', false, $help_url);

        if (version_compare(JVERSION, '3.0', '>')) {
            JHtmlSidebar::setAction('index.php?option=com_coalawebtraffic&view=knownips');

            JHtmlSidebar::addFilter(
                    JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_state', JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true)
            );

            JHtmlSidebar::addFilter(
                    JText::_('JOPTION_SELECT_CATEGORY'), 'filter_category_id', JHtml::_('select.options', JHtml::_('category.options', 'com_coalawebtraffic'), 'value', 'text', $this->state->get('filter.category_id'))
            );
        }
    }

    protected function getSortFields() {
        if (version_compare(JVERSION, '3.0', '>')) {
            return array(
                'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
                'a.state' => JText::_('JSTATUS'),
                'a.id' => JText::_('JGRID_HEADING_ID')
            );
        }
    }

}

<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
jimport('joomla.application.component.view');

class CoalawebtrafficViewControlpanel extends JViewLegacy {

    function display($tpl = null) {

        $canDo = CoalawebtrafficHelper::getActions();
        $model = $this->getModel();
        $countries = $model->getCountries();
        $cities = $model->getCities();
        
        $porb = $model->read();
        
        if (version_compare(JVERSION, '3.0', '>')) {
            CoalawebtrafficHelper::addSubmenu('controlpanel');
        }

        // Is this the Professional release?
        jimport('joomla.filesystem.file');
        $isPro = (COM_CWTRAFFIC_PRO == 1);
        $this->assign('isPro', $isPro);

        $version = (COM_CWTRAFFIC_VERSION);
        $this->assign('version', $version);

        $releaseDate = (COM_CWTRAFFIC_DATE);
        $this->assign('release_date', $releaseDate);

        $this->assignRef('countries', $countries);
        $this->assignRef('cities', $cities);
        $this->assignRef('porb', $porb);

        if (!$this->geodatExist('geoip')) {
            echo JText::_('COM_CWTRAFFIC_NOGEO_CPANEL_MESSAGE');
        }
        
        if (version_compare(JVERSION, '3.0', '>')) {
            JToolBarHelper::title(JText::_('COM_CWTRAFFIC_TITLE_MAIN') . ' [ ' . JText::_('COM_CWTRAFFIC_TITLE_CPANEL') . ' ]', 'cogs');
        }else{
            JToolBarHelper::title(JText::_('COM_CWTRAFFIC_TITLE_MAIN') . ' [ ' . JText::_('COM_CWTRAFFIC_TITLE_CPANEL') . ' ]', 'cwt-cpanel');
        }
        
        if ($canDo->get('core.admin')) {
            JToolBarHelper::preferences('com_coalawebtraffic');
        }

        $help_url = 'http://coalaweb.com/support/documentation/item/coalaweb-traffic-guide';
        JToolBarHelper::help('COM_CWTRAFFIC_TITLE_HELP', false, $help_url);

        parent::display($tpl);
    }

    /**
     * Checks if geolitecity.dat file exists
     * @param $geo
     * @return bool
     */
    private function geodatExist($geo) {
        //Let check if GeoIp already exsit on the server.
        if (function_exists('geoip_record_by_name')) {
            return true;
        }
        //If not lets check ours.
        $path = JPATH_SITE . '/administrator/components/com_coalawebtraffic/assets' . DS . $geo;
        jimport('joomla.filesystem.folder');
        if (JFolder::files($path, 'geolitecity.dat', false)) {
            return true;
        }
        return false;
    }

}

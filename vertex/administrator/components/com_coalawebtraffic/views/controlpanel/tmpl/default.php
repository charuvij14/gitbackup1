<?php
defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
$user = JFactory::getUser();
?>

<div id="cpanel" style="float:left;width:58%;">

    <div style="float:left;">
        <div class="icon">
            <a href="index.php?option=com_coalawebtraffic&view=visitors">
                <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_VISITORS'); ?>" src="<?php echo JURI::root() ?>/media/coalawebtraffic/components/traffic/icons/icon-48-cwt-visitors.png" />
                <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_VISITORS'); ?></span>
            </a>
        </div>
    </div>

    <div style="float:left;">
        <div class="icon">
            <a href="index.php?option=com_categories&extension=com_coalawebtraffic">
                <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_IPCATS'); ?>" src="<?php echo JURI::root() ?>/media/coalawebtraffic/components/traffic/icons/icon-48-cw-categories.png" />
                <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_IPCATS'); ?></span>
            </a>
        </div>
    </div>
    
    <div style="float:left;">
        <div class="icon">
            <a href="index.php?option=com_coalawebtraffic&view=knownips">
                <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_KNOWNIPS'); ?>" src="<?php echo JURI::root() ?>/media/coalawebtraffic/components/traffic/icons/icon-48-cwt-knownip.png" />
                <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_KNOWNIPS'); ?></span>
            </a>
        </div>
    </div>

    <div style="float:left;">
        <div class="icon">
            <a href="index.php?option=com_coalawebtraffic&view=geoupload">
                <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_GEO'); ?>" src="<?php echo JURI::root() ?>/media/coalawebtraffic/components/traffic/icons/icon-48-cwt-geo.png" />
                <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_GEO'); ?></span>
            </a>
        </div>
    </div>
    
        <div style="float:left;">
        <div class="icon">
            <a href="index.php?option=com_coalawebtraffic&task=visitors.csvreportall">
                <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_REPORT'); ?>" src="<?php echo JURI::root() ?>/media/coalawebtraffic/components/traffic/icons/icon-48-cw-report.png" />
                <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_REPORT'); ?></span>
            </a>
        </div>
    </div>

    <div style="float:left;">
        <div class="icon">
            <a onclick="Joomla.popupWindow('http://coalaweb.com/support/documentation/item/coalaweb-traffic-guide', 'Help', 700, 500, 1)" href="#">
                <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_HELP'); ?>" src="<?php echo JURI::root() ?>/media/coalawebtraffic/components/traffic/icons/icon-48-cwt-support.png" />
                <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_HELP'); ?></span>
            </a>
        </div>
    </div>
    
    <div style="float:left;">
        <div class="icon">
            <?php if (version_compare(JVERSION, '3.0', '>')) { ?>
                <a href="index.php?option=com_config&view=component&component=com_coalawebtraffic">
                    <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_OPTIONS'); ?>" src="<?php echo JURI::root() ?>/media/coalawebtraffic/components/traffic/icons/icon-48-cwt-options.png" />
                    <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_OPTIONS'); ?></span>
                </a>
            <?php } else { ?>
                <a class="modal" rel="{handler: 'iframe', size: {x: 875, y: 550}, onClose: function() {}}" href="index.php?option=com_config&view=component&component=com_coalawebtraffic&path=&tmpl=component">
                    <img alt="<?php echo JText::_('COM_CWTRAFFIC_TITLE_OPTIONS'); ?>" src="<?php echo JURI::root() ?>/media/coalawebtraffic/components/traffic/icons/icon-48-cwt-options.png" />
                    <span><?php echo JText::_('COM_CWTRAFFIC_TITLE_OPTIONS'); ?></span>
                </a>
            <?php } ?>
        </div>
    </div>


    <div class="clr"></div>
</div>
<div id="tabs" style="float:right; width:40%;">

    <?php
    $options = array(
        'onActive' => 'function(title, description){
        description.setStyle("display", "block");
        title.addClass("open").removeClass("closed");
    }',
        'onBackground' => 'function(title, description){
        description.setStyle("display", "none");
        title.addClass("closed").removeClass("open");
    }',
        'startOffset' => 0, // 0 starts on the first tab, 1 starts the second, etc...
        'useCookie' => true, // this must not be a string. Don't use quotes.
        'startTransition' => 1,
    );
    ?>

    <?php echo JHtml::_('sliders.start', 'slider_group_id', $options); ?>
    
    <?php echo JHtml::_('sliders.panel', JText::_('COM_CWTRAFFIC_SLIDER_TITLE_ABOUT'), 'slider_1_id'); ?>
    
    <span class="cw-slider">
        <?php echo JText::_('COM_CWTRAFFIC_ABOUT_DESCRIPTION'); ?>
    </span>

    <?php echo JHtml::_('sliders.panel', JText::_('COM_CWTRAFFIC_SLIDER_TITLE_STATS'), 'slider_2_id'); ?>
    
    <table class="coalaweb">
        <thead>
            <tr>
                <th width="85%">
                    <?php echo JText::_('COM_CWTRAFFIC_TITLE_VISITORS'); ?>
                </th> 
                <th>
                    <?php echo JText::_('COM_CWTRAFFIC_HEADER_COUNT'); ?>
                </th> 
            </tr>			
        </thead>

        <tr class="row0">
            <td>
                <?php 
                echo JText::_('COM_CWTRAFFIC_TODAY'); 
                ?>
            </td>
            <td>
                <strong><?php echo $this->porb[1]; ?></strong>
            </td>
        </tr>
        <tr class="row1">
            <td>
                <?php 
                echo '<span>'.JText::_('COM_CWTRAFFIC_YESTERDAY').'</span>'; 
                ?>
            </td>
            <td>
                <strong><?php echo $this->porb[2]; ?></strong>
            </td>
        </tr>
        <tr class="row0">
            <td>
                <?php echo JText::_('COM_CWTRAFFIC_WEEK'); ?>
            </td>
            <td>
                <strong><?php echo $this->porb[3]; ?></strong>
            </td>
        </tr>
        <tr class="row1">
            <td>
                <?php echo JText::_('COM_CWTRAFFIC_MONTH'); ?>
            </td>
            <td>
                <strong><?php echo $this->porb[4]; ?></strong>
            </td>
        </tr>
        <tr class="row0">
            <td>
                <?php echo JText::_('COM_CWTRAFFIC_TOTAL'); ?>
            </td>
            <td>
                <strong><?php echo $this->porb[0]; ?></strong>
            </td>
        </tr>
    </table>
    <!-- Top 5 Countries -->
    <table class="coalaweb">
        <thead>
            <tr>
                <th width="85%">
                    <?php echo JText::_('COM_CWTRAFFIC_HEADER_TOP5_COUNTRY'); ?>
                </th> 
                <th>
                    <?php echo JText::_('COM_CWTRAFFIC_HEADER_COUNT'); ?>
                </th> 
            </tr>			
        </thead>
        <?php
        $k = 0;
        for ($i = 0, $n = count($this->countries); $i < $n; $i++) {
            $row = &$this->countries[$i];
            ?>
            <tr class="<?php echo "row$k"; ?>">
                <td>
                    <?php
                    echo JHTML::_('image', 'media/coalawebtraffic/components/traffic/flags/' . $row->country_code . '.png', $row->country_code);
                    echo ' ' . $row->country_name;
                    ?>
                </td>
                <td align="center">
                    <strong><?php echo $row->num; ?></strong>
                </td>
            </tr>
            <?php
            $k = 1 - $k;
        }
        ?>
    </table>
    <!-- Top 5 Cities -->
    <table class="coalaweb">
        <thead>
            <tr>
                <th width="85%">
                    <?php echo JText::_('COM_CWTRAFFIC_HEADER_TOP5_CITY'); ?>
                </th> 
                <th width="15%">
                    <?php echo JText::_('COM_CWTRAFFIC_HEADER_COUNT'); ?>
                </th> 
            </tr>			
        </thead>
        <?php
        $k = 0;
        for ($i = 0, $n = count($this->cities); $i < $n; $i++) {
            $row = &$this->cities[$i];
            ?>
            <tr class="<?php echo "row$k"; ?>">
                <td>
                    <?php
                    echo JHTML::_('image', 'media/coalawebtraffic/components/traffic/flags/' . $row->country_code . '.png', $row->country_code);
                    echo ' ' . $row->country_name . ', ';
                    echo $row->city;
                    ?>
                </td>
                <td>
                    <strong><?php echo $row->num; ?></strong>
                </td>
            </tr>
            <?php
            $k = 1 - $k;
        }
        ?>
    </table>

    <?php echo JHtml::_('sliders.panel', JText::_('COM_CWTRAFFIC_SLIDER_TITLE_SUPPORT'), 'slider_3_id'); ?>
    
    <span class="cw-slider">
        <?php echo JText::_('COM_CWTRAFFIC_SUPPORT_DESCRIPTION'); ?>
    </span>
    
    </span><?php echo JHtml::_('sliders.panel', JText::_('COM_CWTRAFFIC_SLIDER_TITLE_VERSION'), 'slider_4_id'); ?>
    <?php
        $version = (COM_CWTRAFFIC_VERSION);
        $date = (COM_CWTRAFFIC_DATE);
        $ispro = (COM_CWTRAFFIC_PRO);
        $type = ($ispro == 1 ? JText::_('COM_CWTRAFFIC_RELEASE_TYPE_PRO') : JText::_('COM_CWTRAFFIC_RELEASE_TYPE_CORE'));
    ?>
    <div class="cw-slider">
        <div class="cw-slider">
            <h3> <?php echo JText::_('COM_CWTRAFFIC_RELEASE_TITLE'); ?> </h3>
            <ul class="cw-slider">
                <li>  <?php echo JText::_('COM_CWTRAFFIC_FIELD_RELEASE_TYPE_LABEL'); ?>  <strong><?php echo $type; ?> </strong></li>
                <li>   <?php echo JText::_('COM_CWTRAFFIC_FIELD_RELEASE_VERSION_LABEL'); ?> <strong> <?php echo $version?> </strong></li>
                <li>  <?php echo JText::_('COM_CWTRAFFIC_FIELD_RELEASE_DATE_LABEL'); ?>  <strong> <?php echo $date; ?>  </strong></li>
            </ul>
        </div>
    </div>

    <?php echo JHtml::_('sliders.end'); ?>
</div>
<div class="clr"></div>

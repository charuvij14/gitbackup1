<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
jimport('joomla.filesystem');
jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.archive');

class CoalawebtrafficControllerGeoupload extends JControllerLegacy {
    
    /**
     * @var		string	The prefix to use with controller messages.
     * @since	1.6
     */
    protected $text_prefix = 'COM_CWTRAFFIC';

    public function __construct() {
        if (!defined('DS'))
            define('DS', DIRECTORY_SEPARATOR);
        $this->path = JFactory::getConfig()->get('tmp_path') . '/com_coalawebtraffic';
        $this->pathArchive = $this->path . '/archives';
        $this->pathUnzipped = JPATH_COMPONENT_ADMINISTRATOR . DS . 'assets' . DS . 'geoip';
        parent::__construct();
    }

    public function upload() {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        $appl = JFactory::getApplication();
        $destination = $this->pathArchive . '/GeoLiteCity.dat.gz';
        $source = 'http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz';

        // create a new cURL resource
        $resource = curl_init();

        // set URL and other appropriate options
        $options = array(CURLOPT_URL => $source,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 600
        );

        curl_setopt_array($resource, $options);

        // grab URL and pass it to the browser
        $content = curl_exec($resource);

        // close cURL resource, and free up system resources
        curl_close($resource);

        $path = $this->pathArchive;

        // if the archive folder doesn't exist - create it!
        if (!JFolder::exists($path)) {
            JFolder::create($path);
        } else {
            // let us remove all previous uploads
            $archiveFiles = JFolder::files($path);

            foreach ($archiveFiles as $archive) {
                if (!JFile::delete($this->pathArchive . '/' . $archive)) {
                    echo 'could not delete' . $archive;
                }
            }
        }

        if ($content != '') {
            $fp = fopen($destination, 'w');
            $fw = fwrite($fp, $content);
            fclose($fp);
        }
        if ($fw != false) {
            $message = 'COM_CWTRAFFIC_UPLOAD_SUCCESSFUL';
        } else {
            $message = 'COM_CWTRAFFIC_UPLOAD_FAILED';
        }

        $appl->redirect('index.php?option=com_coalawebtraffic&view=geoupload', JText::_($message));

        return $fw;
    }

    public function unzip() {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
        $appl = JFactory::getApplication();
        // if folder doesn't exist - create it!
        if (!JFolder::exists($this->pathUnzipped)) {
            JFolder::create($this->pathUnzipped);
        }

        $file = JFolder::files($this->pathArchive);
        $result = JArchive::extract($this->pathArchive . '/' . $file[0], $this->pathUnzipped);

        if ($result) {
            $message = 'COM_CWTRAFFIC_UNZIP_SUCCESSFUL';
        } else {
            $message = 'COM_CWTRAFFIC_UNZIP_FAILED';
        }

        $appl->redirect('index.php?option=com_coalawebtraffic&view=geoupload', JText::_($message));

        return $result;
    }

}

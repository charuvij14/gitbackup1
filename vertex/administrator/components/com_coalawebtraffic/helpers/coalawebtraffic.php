<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  component helper.
 */
class CoalawebtrafficHelper {

    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($vName = 'controlpanel') {

        if (version_compare(JVERSION, '3.0', '>')) {
        JHtmlSidebar::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_CPANEL'), 'index.php?option=com_coalawebtraffic&view=controlpanel', $vName == 'controlpanel');
        JHtmlSidebar::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_VISITORS'), 'index.php?option=com_coalawebtraffic&view=visitors', $vName == 'visitors');
        JHtmlSidebar::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_IPCATS'), 'index.php?option=com_categories&extension=com_coalawebtraffic', $vName == 'categories');
        JHtmlSidebar::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_KNOWNIPS'), 'index.php?option=com_coalawebtraffic&view=knownips', $vName == 'knownips');
        JHtmlSidebar::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_GEO'), 'index.php?option=com_coalawebtraffic&view=geoupload', $vName == 'geoupload');
        } else {
        JSubMenuHelper::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_CPANEL'), 'index.php?option=com_coalawebtraffic&view=controlpanel', $vName == 'controlpanel');
        JSubMenuHelper::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_VISITORS'), 'index.php?option=com_coalawebtraffic&view=visitors', $vName == 'visitors');
        JSubMenuHelper::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_IPCATS'), 'index.php?option=com_categories&extension=com_coalawebtraffic', $vName == 'categories');
        JSubMenuHelper::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_KNOWNIPS'), 'index.php?option=com_coalawebtraffic&view=knownips', $vName == 'knownips');
        JSubMenuHelper::addEntry(
                JText::_('COM_CWTRAFFIC_TITLE_GEO'), 'index.php?option=com_coalawebtraffic&view=geoupload', $vName == 'geoupload');
        }
    }

    /**
     * Get the actions
     */
	public static function getActions($categoryId = 0)
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($categoryId)) {
			$assetName = 'com_coalawebtraffic';
			$level = 'component';
		} else {
			$assetName = 'com_coalawebtraffic.category.'.(int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_coalawebtraffic', $level);

		foreach ($actions as $action) {
			$result->set($action->name,	$user->authorise($action->name, $assetName));
		}

		return $result;
	}

}
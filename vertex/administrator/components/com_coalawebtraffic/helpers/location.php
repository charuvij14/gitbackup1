<?php

defined('_JEXEC') or die('Restricted access');

/**
 * @package             Joomla
 * @subpackage          CoalaWeb Traffic Component
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 *  component helper.
 */
class CoalawebtrafficHelperLocation {

    /**
     * Update Visitor info to include country and city
     */
    public static function location_update() {

        $datLocation = JPATH_ROOT . '/administrator/components/com_coalawebtraffic/assets/geoip/geolitecity.dat';

        $db = JFactory::getDbo();

        if (file_exists($datLocation)) {
            if (filesize($datLocation) != 0) {
                if (!function_exists('GeoIP_record_by_addr')) {
                    include(JPATH_ROOT . '/administrator/components/com_coalawebtraffic/assets/geoip/geoipcity.inc');
                }
                $geoip = geoip_open($datLocation, GEOIP_STANDARD);

                $query = $db->getQuery(true);
                $query->select('id, ip');
                $query->from($db->quoteName('#__cwtraffic'));
                $query->where('country_code = "" OR country_code is null');
                $db->setQuery($query);

                foreach ($db->loadObjectList() as $row) {
                        $country_code = strtolower(geoip_country_code_by_addr($geoip, $row->ip));
                        $country_name = geoip_country_name_by_addr($geoip, $row->ip);
                        $addr = GeoIP_record_by_addr($geoip, $row->ip);
                        if (!empty($addr)) {
                            $city = $addr->city;
                        }

                    if ($country_code != '' && $country_name != '') {

                        $query = $db->getQuery(true);
                        $query->update('#__cwtraffic');
                        $query->set('country_code = ' . $db->quote($country_code));
                        $query->set('country_name = ' . $db->quote($country_name));
                        $query->set('city = ' . $db->quote($city));
                        $query->where('id = ' . $row->id);
                        $db->setQuery($query);
                        $db->query();
                    }
                }

                geoip_close($geoip);

                $query = $db->getQuery(true);
                $query->update('#__cwtraffic');
                $query->set('city = NULL');
                $query->where('city = ""');
                $db->setQuery($query);
                $db->query();
            }
        }
        return;
    }

    /**
     * Update Visitor info to include country and city
     */
    public static function whoisonlineUpdate($ip) {

        $datLocation = JPATH_ROOT . '/administrator/components/com_coalawebtraffic/assets/geoip/geolitecity.dat';

        if (file_exists($datLocation)) {
            if (filesize($datLocation) != 0) {

                if (!function_exists('GeoIP_record_by_addr')) {
                    include(JPATH_ROOT . '/administrator/components/com_coalawebtraffic/assets/geoip/geoipcity.inc');
                }

                $geoip = geoip_open($datLocation, GEOIP_STANDARD);

                $country_code = strtolower(geoip_country_code_by_addr($geoip, $ip));
                $country_name = geoip_country_name_by_addr($geoip, $ip);
                $addr = GeoIP_record_by_addr($geoip, $ip);
                if (!empty($addr)) {
                    $city = $addr->city;
                } else {
                    $city = "";
                }
                
                geoip_close($geoip);
            
            }

            return array(
                'country_code' => $country_code,
                'country_name' => $country_name,
                'city' => $city
            );
        }
        return;
    }

}

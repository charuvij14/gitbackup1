<div class="col-md-12" id="unsub-lower">
    <div class="panel panel-danger plain toggle" id="jst_4">
        <!-- Start .panel -->
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="text-center">
                        <?php oLang::_('CALL_TO_ACTION_P'); ?><br>
                    </h2>

                    <p class="text-center">
                        <?php oLang::_('CALL_TO_ACTION_P2'); ?>
                    </p>

                    <h2 class="text-center">
                        <button class="btn btn-white mr5 mb10" type="button"
                                onClick="location.href='<?php oLang::_('OSE_OEM_URL_SUBSCRIBE'); ?>'">
                            <i class="im-cart6 mr5"></i> <?php oLang::_('SUBSCRIBE_NOW'); ?>
                        </button>
                    </h2>

                    <br>
                    <div class="col-md-12" id="unsub-lower-left-individualcontent">
                        <div style="margin-top:-18px; margin-left: 18px;">
                        <h2><?php echo $this->model->showSubTitle(); ?></h2>
                        <p><?php echo $this->model->showSubDesc(); ?></p>
                        </div>
                        <img style="margin-top: -1px; min-width: 900px;" src="<?php echo $this->model->showSubPic(); ?>"
                             alt="Centrora Logo"/>
                    </div>
                </div>
                <div class="col-md-6" id="unsub-lower-right">
                    <div>
                    <h2 class="text-danger"><?php oLang::_('CALL_TO_ACTION_TITLE2'); ?></h2>
                        <?php oLang::_('CALL_TO_ACTION_UL'); ?>
                        <p> <?php oLang::_('CALL_TO_ACTION_DESC2'); ?> </p>
                    </div>
                </div>
            </div>
            <!--            <div class="row">-->
            <!--                <p class="text-left">-->
            <!--                <h2 class="text-danger">--><?php //oLang::_('CALL_TO_ACTION_TITLE3'); ?><!--</h2>-->
            <!--                --><?php //oLang::_('CALL_TO_ACTION_DECS3'); ?><!--<a-->
            <!--                    href="--><?php //echo OSE_OEM_URL_PREMIUM_TUT; ?><!--"-->
            <!--                    target="_blank">--><?php //oLang::_('O_OUR_TUTORIAL'); ?><!--</a>-->
            <!--                --><?php //oLang::_('O_SUBSCRIBE_PLAN'); ?><!--.-->
            <!--                </p>-->
            <!--            </div>-->
        </div>
    </div>
</div>
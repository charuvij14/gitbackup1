<div id="oseappcontainer">
    <div class="container">
        <?php
        $this->model->showLogo();
        $this->model->showHeader();
        $authSession = $this->model->checkAuthSession();
        ?>
        <div class="content-inner">
            <div class="row ">
                <div class="col-lg-12 sortable-layout">
                    <!-- col-lg-12 start here -->
                    <div class="panel panel-primary plain">
                        <!-- Start .panel -->
                        <div class="panel-heading white-bg"></div>
                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                                <li role="presentation" class="vl-tabs active">
                                    <a data-toggle="tab" onclick="reloadBackupList()" href="#sectionBKs"><?php oLang::_('O_BK_TAB_BACKUPS') ?>
                                    </a>
                                </li>
                                <li role="presentation" class="vl-tabs ">
                                    <a data-toggle="tab" href="#sectionNew"><?php oLang::_('O_BK_TAB_NEW_BACKUP') ?>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <!-- backups section -->
                                <div id="sectionBKs" class="tab-pane fade in active">
                                    <div class="panel-body">
                                        <div class="panel">
                                            <div class="panel-header">
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="recent_backup_section">
                                                            <strong style="font-size: 20px;">Backup Lists</strong>
                                                            <?php if($authSession == false) { ?>
                                                                <div id="init_host_setup">
                                                                <div class="row">
                                                                    <div class="col-md-3">
                                                                        <label for="input_hosting_backup">You need to connect to hosting
                                                                            platform first</label>
                                                                    </div>
                                                                    <div class="col-md-7">
                                                                        <label><input type="radio" name="input_hosting_backup_init" value="cpanel" id="input_cpanel">&nbsp&nbspCpanel&nbsp&nbsp</label>
                                                                        <label><input type="radio" name="input_hosting_backup_init" value="plesk" id="input_plesk">&nbsp&nbspPlesk&nbsp&nbsp</label>
                                                                    </div>
                                                                </div>
                                                                <div id="blk-cpanel-init" class="toHide" style="display:none">
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <label for="input_server_ip_init">Server IP</label>
                                                                            </div>
                                                                            <div class="col-md-5"><input type="text" id="input_server_ip_init" name="input_server_ip_init" class="form-control" placeholder="Please Enter Server IP" required/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <label for="input_admin_username_init">Super Admin Username</label>
                                                                            </div>
                                                                            <div class="col-md-5"><input type="text" id="input_admin_username_init" name="input_admin_username_init" class="form-control" placeholder="Please Enter Super Admin Username" required/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <label for="input_admin_password_init">Super Admin Password</label>
                                                                            </div>
                                                                            <div class="col-md-5"><input type="password" id="input_admin_password_init" name="input_admin_password_init" class="form-control" placeholder="Please Enter Super Admin Password" required/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-3">
                                                                                <label for="input_admin_email_init">Notified Email</label>
                                                                            </div>
                                                                            <div class="col-md-5"><input type="email" id="input_admin_email_init" name="input_admin_email_init" class="form-control" placeholder="Please Enter Notified Email" required/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-offset-7">
                                                                                <button onclick="getRecentBackup()"
                                                                                        class="btn btn-primary">
                                                                                    Connect&nbsp<i class="fa fa-angle-right"></i>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                                <div id="recent_backup_content" style="display: none;">


                                                                </div>
                                                            <?php } else { ?>
                                                                <div id="recent_backup_content">


                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="dot-line backup-list-dotline"></div>
                                                        <div id="rencent_backup_schedule_section">
                                                            <strong style="font-size: 20px;">Scheduled Backups</strong>

                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div id = "div_scheduled_bk_content"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- new backup section -->
                                <div id="sectionNew" class="tab-pane fade ">
                                    <div class="panel-body">
                                        <div class="panel">
                                            <div class="panel-header">
                                            </div>
                                            <div class="panel-body">
                                                <ul id="breadcrumb">
                                                    <?php $icon_one = OSE_FWURL . '/public/images/num/1-white.png'; ?>
                                                    <?php $icon_two = OSE_FWURL . '/public/images/num/2-white.png'; ?>
                                                    <?php $icon_three = OSE_FWURL . '/public/images/num/3-white.png'; ?>
                                                    <li class="breadcrumb-nav"><a href="javascript:void(0)"
                                                                                  panel="#new_backup_setup"><span
                                                                class="icon icon-beaker"> </span><img
                                                                src="<?php echo $icon_one; ?>"/>&nbspSetup</a></li>
                                                    <li class="breadcrumb-nav" style="display: none;"><a
                                                            href="javascript:void(0)" panel="#new_backup_config"><span
                                                                class="icon icon-double-angle-right"></span>
                                                            <img src="<?php echo $icon_two; ?>"/>&nbspConfiguration</a>
                                                    </li>
                                                    <li class="breadcrumb-nav" style="display: none;"><a
                                                            href="javascript:void(0)" panel="#new_backup_building"><span
                                                                class="icon icon-rocket"> </span> <img
                                                                src="<?php echo $icon_three; ?>"/>&nbspBuild</a>
                                                    </li>
                                                    <li><span class="breadcrumb-span">Step 1: Connect to Cpanel/Plesk</span>
                                                    </li>
                                                </ul>
                                                <div class="dot-line"></div>
                                                <div class="col-md-offset-2">
                                                    <div class="new_backup_content" id="new_backup_setup">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <label for="input_hosting_backup">Hosting
                                                                        Platform</label>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <label><input type="radio" name="input_hosting_backup" value="cpanel" id="input_cpanel">&nbsp&nbspCpanel&nbsp&nbsp</label>
                                                                    <label><input type="radio" name="input_hosting_backup" value="plesk" id="input_plesk">&nbsp&nbspPlesk&nbsp&nbsp</label>
                                                                </div>
                                                            </div>
                                                        <?php if($authSession == false) { ?>
                                                            <div id="blk-cpanel" class="toHide" style="display:none">
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label for="input_server_ip">Server IP</label>
                                                                        </div>
                                                                        <div class="col-md-5"><input type="text" id="input_server_ip" name="input_server_ip" class="form-control" placeholder="Please Enter Server IP" required/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label for="input_admin_username">Super Admin Username</label>
                                                                        </div>
                                                                        <div class="col-md-5"><input type="text" id="input_admin_username" name="input_admin_username" class="form-control" placeholder="Please Enter Super Admin Username" required/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label for="input_admin_password">Super Admin Password</label>
                                                                        </div>
                                                                        <div class="col-md-5"><input type="password" id="input_admin_password" name="input_admin_password" class="form-control" placeholder="Please Enter Super Admin Password" required/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-3">
                                                                            <label for="input_admin_email">Notified Email</label>
                                                                        </div>
                                                                        <div class="col-md-5"><input type="email" id="input_admin_email" name="input_admin_email" class="form-control" placeholder="Please Enter Notified Email" required/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php } else { ?>
                                                            <div id="blk-cpanel" class="toHide" style="display:none">
                                                                <div id="session_exists" class="alert alert-success">
                                                                    <strong>Success!</strong> Connection to Cpanel success.
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                            <div id="blk-plesk" class="toHide" style="display:none">
                                                               TODO: plesk
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-3">
                                                                    <label for="lb_archive">Archive</label>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <label>
                                                                        <input type="checkbox" checked
                                                                               disabled="disabled">&nbsp&nbsp<i
                                                                            class="text-primary glyphicon glyphicon-duplicate"></i>&nbspFiles
                                                                    </label>
                                                                    <label>
                                                                        <input type="checkbox" checked
                                                                               disabled="disabled">&nbsp&nbsp<i
                                                                            class="text-primary glyphicon glyphicon-hdd"></i>&nbspDataBase
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-offset-7">
                                                                    <button onclick="setupBackup()"
                                                                            class="btn btn-primary">
                                                                        Next&nbsp<i class="fa fa-angle-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="new_backup_content" style="display: none;"
                                                         id="new_backup_config">

                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-md-offset-7">
                                                                    <button id = 'btn_build_backup' onclick="setConfig()"
                                                                            class="btn btn-primary">
                                                                        Build Backup&nbsp<i
                                                                            class="fa fa-angle-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="new_backup_content col-" style="display: none;"
                                                         id="new_backup_building">
                                                        <div class="row">
                                                            <div class="col-md-offset-2 col-md-10">
                                                                <h3 class="text-primary"><i
                                                                        class="fa fa-cog fa-spin fa-1x fa-fw margin-bottom"></i>&nbspBuilding
                                                                    Backup</h3>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8">
                                                                <div class="progress" style="height: 40px;">
                                                                    <div
                                                                        class="progress-bar progress-bar-striped active"
                                                                        role="progressbar" aria-valuenow="100"
                                                                        aria-valuemin="0" aria-valuemax="100"
                                                                        style="width:100%;">
                                                                    </div>
                                                                </div>
                                                                <div class="backup-loading-info">Please Wait...</div>
                                                                <div class="backup-loading-info-snippet">This may take a
                                                                    few seconds, please keep this page open until this
                                                                    backup is successfully built
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="new_backup_content" style="display: none;"
                                                         id="new_backup_building_success">
                                                        <div class="row">
                                                            <div class="col-md-offset-2 col-md-10">
                                                                <h3 class="text-success"><i
                                                                        class="fa fa-check-circle"></i>&nbspBackup&nbspSuccessful
                                                                </h3>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-offset-1 col-md-4" style="font-weight: bold;">Backup
                                                                Time:
                                                            </div>
                                                            <div class="col-md-4"><span id = 'span_easy_backup_time'
                                                                    class="text-default"
                                                                    style="font-weight: bold;">Dec, 23 2015</span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-offset-1 col-md-4" style="font-weight: bold;">
                                                                Backup Files:
                                                            </div>
                                                            <div class="col-md-6"><a href="javascript:void(0)"
                                                                                     class="text-warning"
                                                                                     title="Files"><i
                                                                        class="text-primary glyphicon glyphicon-duplicate"></i></a>&nbsp<a
                                                                    href="javascript:void(0)"
                                                                    class="text-warning"
                                                                    title="DataBase"><i
                                                                        class="text-primary glyphicon glyphicon-hdd"></i></a>

                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-md-offset-1">
                                                                <div class="btn-toolbar" role="toolbar">
                                                                    <div class="btn-group ">
                                                                        <button class="btn btn-primary" onclick="redirectNewBackUp()"><i
                                                                                class="fa fa-angle-left" ></i>&nbspNew
                                                                            Backup
                                                                        </button>
                                                                    </div>
                                                                    <div class="btn-group ">
                                                                        <button class="btn btn-primary" onclick="redirectBackupList()">&nbspBackup
                                                                            Lists&nbsp<i class="fa fa-angle-right"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </div>

                    </div>

                </div>
                <!-- End .panel -->
            </div>
                </div>
        </div>
    </div>
</div>

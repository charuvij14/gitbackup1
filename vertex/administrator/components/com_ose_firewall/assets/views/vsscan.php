<?php
oseFirewall::checkDBReady();
$status = oseFirewall::checkSubscriptionStatus(false);
$urls = oseFirewall::getDashboardURLs();
$confArray = $this->model->getConfiguration('vsscan');
$this->model->getNounce();
if (isset($confArray['data']['vsScanExt']) && !isset($confArray['data']['file_ext'])) {
    $confArray['data']['file_ext'] = $confArray['data']['vsScanExt'];
}
if ($status == true) {
    ?>
    <div id="oseappcontainer">
        <div class="container">
            <?php
            $this->model->showLogo();
            $this->model->showHeader();
            ?>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary plain ">
                        <!-- Start .panel -->
                        <div class="panel-heading"></div>
                        <div class="panel-body">
                            <div class="row">
                                <!--Scan Status-->
                                <div id="scan-window" class="col-md-12">
                                    <button class="btn btn-config btn-sm mr5 mb10 pull-right" type="button"
                                            onClick="downloadRequest('avs')"
                                            title="<?php oLang::_('O_UPDATE_VIRUS_SIGNATURE'); ?>">
                                        <i class="glyphicon glyphicon-refresh color-blue"></i>
                                    </button>
                                    <button data-target="#configModal" data-toggle="modal"
                                            class="btn btn-config btn-sm mr5 mb10 pull-right" type="button"
                                            title="<?php oLang::_('CONFIGURATION'); ?>">
                                        <i class="glyphicon glyphicon-cog color-orange"></i>
                                    </button>
                                    <button class="btn btn-config btn-sm mr5 mb10 pull-right" type="button"
                                            title="<?php oLang::_('SCHEDULE_SCANNING'); ?>"
                                            onClick="location.href='<?php echo $urls[9] . '#vscannercron'; ?>'">
                                        <i class="glyphicon glyphicon-calendar color-magenta"></i>
                                    </button>
                                    <div id='scan_progress' class="alert alert-info fade in">
                                        <div class="row">
                                            <div class="col-md-1">
                                                <div class="bg-primary alert-icon">
                                                    <i class="glyphicon glyphicon-info-sign s24"></i>
                                                </div>
                                            </div>
                                            <div id="scan-result" class="col-md-8" style="display: none;">
                                                <div id='status' class='col-md-12'>
                                                    <strong>Status </strong>

                                                    <div class="progress progress-striped active">
                                                        <div id="vs_progress" class="progress-bar" role="progressbar"
                                                             aria-valuenow=0" aria-valuemin="0" aria-valuemax="100"
                                                             style="width: 0%">
                                                            <span id="p4text"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="scanpathtext" class='col-md-12' style="display: none;">Scan
                                                    Path:
                                                    <label class="text-primary" id="selectedfile"></label>
                                                </div>
                                                <div id="last_batch"
                                                     class='col-md-12'> <?php oLang::_('LAST_SCANNED_FILE') ?>
                                                    <strong id='last_file' class="text-success"></strong>
                                                </div>
                                                <div class='col-md-12'># Scanned:
                                                    <strong id='numScanned' class="text-warning"></strong>
                                                </div>
                                                <div class='col-md-12'># Virus Files:
                                                    <a href="#scanresult"><strong id='numinfected'
                                                                                  class="text-danger"></strong></a>
                                                </div>
                                                <div id="statusresult"
                                                     class='col-md-3 col-md-offset-4 alert alert-dismissable text-center'
                                                     style="font-size:18px; display: none;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pull-right">
                                            <i class="fa fa-clock-o"></i>Last Scan:
                                            <strong id="scan-date" class="text-success"></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                </div>
                                <div class="col-md-12" id="scanbuttons">
                                    <button data-target="#scanModal" data-toggle="modal" id="customscan"
                                            class='btn btn-config btn-sm mr5 mb10'>
                                        <i class="glyphicon glyphicon-folder-close text-primary"></i> <?php oLang::_('SCAN_SPECIFIC_FOLDER') ?>
                                    </button>
                                    <button id="vscont" class='btn btn-config btn-sm mr5 mb10'>
                                        <i class="glyphicon glyphicon-play-circle text-primary"></i> <?php oLang::_('O_CONTINUE_SCAN') ?>
                                    </button>
                                    <button id="vsscan" class='btn btn-config btn-sm mr5 mb10'>
                                        <i class="glyphicon glyphicon-random text-primary"></i> <?php oLang::_('START_NEW_VIRUSSCAN') ?>
                                    </button>
                                    <button id="vsstop" class='btn btn-config btn-sm mr5 mb10' style="display: none">
                                        <i class="glyphicon glyphicon-stop color-red"></i> <?php oLang::_('STOP_VIRUSSCAN') ?>
                                    </button>
                                    <button id="vsscanSing" class='btn btn-sm mr5 mb10'
                                            title="<?php oLang::_('START_NEW_SING_VIRUSSCAN') ?>">
                                        <i class="glyphicon glyphicon-play color-green"></i> <?php oLang::_('START_NEW_SCAN') ?>
                                    </button>
                                </div>
                            </div>
                            <div class="row vsscanner-piechart">
                                <div class="pie-charts">
                                    <div class="easy-pie-chart" data-percent="0" id='easy-pie-chart-1'><span id='pie-1'>0%</span>
                                    </div>
                                    <div class="label">
                                        <?php oLang::_('O_SHELL_CODES'); ?>
                                        <div id="shell-result"></div>
                                    </div>
                                </div>
                                <div class="pie-charts red-pie">
                                    <div class="easy-pie-chart-red" data-percent="0" id='easy-pie-chart-2'><span
                                            id='pie-2'>0%</span></div>
                                    <div class="label"><?php oLang::_('O_BASE64_CODES'); ?></div>
                                </div>
                                <div class="pie-charts green-pie">
                                    <div class="easy-pie-chart-green" data-percent="0" id='easy-pie-chart-3'><span
                                            id='pie-3'>0%</span></div>
                                    <div class="label"><?php oLang::_('O_JS_INJECTION_CODES'); ?></div>
                                </div>
                                <div class="pie-charts blue-pie">
                                    <div class="easy-pie-chart-blue" data-percent="0" id='easy-pie-chart-4'><span
                                            id='pie-4'>0%</span></div>
                                    <div class="label"><?php oLang::_('O_PHP_INJECTION_CODES'); ?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="pie-charts teal-pie">
                                    <div class="easy-pie-chart-teal" data-percent="0" id='easy-pie-chart-5'><span
                                            id='pie-5'>0%</span></div>
                                    <div class="label"><?php oLang::_('O_IFRAME_INJECTION_CODES'); ?></div>
                                </div>
                                <div class="pie-charts purple-pie">
                                    <div class="easy-pie-chart-purple" data-percent="0" id='easy-pie-chart-6'><span
                                            id='pie-6'>0%</span></div>
                                    <div class="label"><?php oLang::_('O_SPAMMING_MAILER_CODES'); ?></div>
                                </div>
                                <div class="pie-charts orange-pie">
                                    <div class="easy-pie-chart-orange" data-percent="0" id='easy-pie-chart-7'><span
                                            id='pie-7'>0%</span></div>
                                    <div class="label"><?php oLang::_('O_EXEC_MAILICIOUS_CODES'); ?></div>
                                </div>
                                <div class="pie-charts lime-pie">
                                    <div class="easy-pie-chart-lime" data-percent="0" id='easy-pie-chart-8'><span
                                            id='pie-8'>0%</span></div>
                                    <div class="label"><?php oLang::_('O_OTHER_MAILICIOUS_CODES'); ?></div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6 col-md-12 sortable-layout">
                                    <div class="panel panel-default plain ">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">CPU Load</h4>
                                        </div>
                                        <div class="panel-body">
                                            <div id="line-chart-cpu" style="width: 100%; height:250px;"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12 sortable-layout">
                                    <div class="panel panel-default plain ">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">Memory Usage</h4>
                                        </div>
                                        <div class="panel-body">
                                            <div id="line-chart-memory" style="width: 100%; height:250px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Configuration Form Modal -->
        <div class="modal fade" id="configModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel2"><?php oLang::_('CONFIGURATION'); ?></h4>
                    </div>
                    <div class="modal-body">
                        <form id='configuraton-form' class="form-horizontal group-border stripped" role="form"
                              method="POST">
                            <div class="form-group">
                                <label for="file_ext"
                                       class="col-sm-4 control-label"><?php oLang::_('O_SCANNED_FILE_EXTENSIONS'); ?></label>

                                <div class="col-sm-8">
                                    <input type="text" name="file_ext"
                                           value="<?php echo (isset($confArray['data']['file_ext']) && empty($confArray['data']['file_ext'])) ? 'htm,html,shtm,shtml,css,js,php,php3,php4,php5,inc,phtml,jpg,jpeg,gif,png,bmp,c,sh,pl,perl,cgi,txt' : $confArray['data']['file_ext'] ?>"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="maxfilesize"
                                       class="col-sm-4 control-label"><?php oLang::_('MAX_FILE_SIZE'); ?></label>

                                <div class="col-sm-8">
                                    <input type="text" name="maxfilesize"
                                           value="<?php echo (isset($confArray['data']['maxfilesize']) && empty($confArray['data']['maxfilesize'])) ? '3' : $confArray['data']['maxfilesize'] ?>"
                                           class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="maxdbconn"
                                       class="col-sm-4 control-label"><?php oLang::_('MAX_DB_CONN'); ?></label>

                                <div class="col-sm-8">
                                    <input type="text" name="maxdbconn"
                                           value="<?php echo (isset($confArray['data']['maxdbconn']) && empty($confArray['data']['maxdbconn'])) ? '3' : $confArray['data']['maxdbconn'] ?>"
                                           class="form-control">
                                </div>
                            </div>
                            <input type="hidden" name="option" value="com_ose_firewall">
                            <input type="hidden" name="controller" value="avconfig">
                            <input type="hidden" name="action" value="saveConfAV">
                            <input type="hidden" name="task" value="saveConfAV">
                            <input type="hidden" name="type" value="vsscan">

                            <div class="form-group">
                                <div class="col-sm-offset-10">
                                    <button type="submit" class="btn btn-default" id='save-button'><i
                                            class="glyphicon glyphicon-save"></i> <?php oLang::_('SAVE'); ?></button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal -->
    </div>
    <?php
    include_once(dirname(__FILE__) . '/scanpath.php');
} else {
    ?>
    <div id="oseappcontainer">
        <div class="container">
            <?php
            $this->model->showLogo();
            ?>
            <div class="row" id="sub-header"
                 style="background:url('<?php echo OSE_FWURL . '/public/images/premium/p_bg.png' ?>') top center;  min-height:250px;">
                <div class="col-md-6" id="unsub-left">
                    <?php $this->model->showHeader(); ?>
                    <a href="https://www.centrora.com/malware-removal" id="leavetous">leave the work to us now</a>
                </div>
                <div class="col-md-6" id="unsub-right">
                    <?php echo $this->model->getBriefDescription(); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php
                    include_once dirname(__FILE__) . '/calltoaction.php';
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    $this->model->showFooterJs();
}
?>
var controller = "backup";
var option = "com_ose_firewall";
var disable_build_backup = 'disabled';
var ose_cms = '';
var dropboxlink = '';
var dropboxicon = '';
var needSubscription = '';
var subscriptionLink = '';
var dropboxauth = '';
var onedrivelink = '';
var onedriveicon = '';
var onedriveauth = '';
var googledrivelink = '';
var googledriveicon = '';
var googledriveauth = '';

// Create Base64 Object
var Base64 = {
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
        var t = "";
        var n, r, i, s, o, u, a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
            n = e.charCodeAt(f++);
            r = e.charCodeAt(f++);
            i = e.charCodeAt(f++);
            s = n >> 2;
            o = (n & 3) << 4 | r >> 4;
            u = (r & 15) << 2 | i >> 6;
            a = i & 63;
            if (isNaN(r)) {
                u = a = 64
            } else if (isNaN(i)) {
                a = 64
            }
            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
        }
        return t
    }, decode: function (e) {
        var t = "";
        var n, r, i;
        var s, o, u, a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (f < e.length) {
            s = this._keyStr.indexOf(e.charAt(f++));
            o = this._keyStr.indexOf(e.charAt(f++));
            u = this._keyStr.indexOf(e.charAt(f++));
            a = this._keyStr.indexOf(e.charAt(f++));
            n = s << 2 | o >> 4;
            r = (o & 15) << 4 | u >> 2;
            i = (u & 3) << 6 | a;
            t = t + String.fromCharCode(n);
            if (u != 64) {
                t = t + String.fromCharCode(r)
            }
            if (a != 64) {
                t = t + String.fromCharCode(i)
            }
        }
        t = Base64._utf8_decode(t);
        return t
    }, _utf8_encode: function (e) {
        e = e.replace(/\r\n/g, "\n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
            var r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r)
            } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128)
            } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128)
            }
        }
        return t
    }, _utf8_decode: function (e) {
        var t = "";
        var n = 0;
        var r = 0, c1 = 0, c2 = 0;
        while (n < e.length) {
            r = e.charCodeAt(n);
            if (r < 128) {
                t += String.fromCharCode(r);
                n++
            } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2
            } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3
            }
        }
        return t
    }
};

function downloadAjax(url)
{
    window.open(url, '_blank');
    window.focus();
}

function reloadBackupList()
{
    if ($('#blk-cpanel-init').length){
        window.location = 'index.php?option=com_ose_firewall&view=backup#sectionBKs';
        window.location.reload();
    } else {
        getRecentBackup();
    }
}

function backup() {
    var acclists = [];
    $("input[name='acclists[]']:checked").each(function ()
    {
        acclists.push($(this).val());
    });
    jQuery(document).ready(function ($) {

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'suitebackup',
                task: 'suitebackup',
                acclists: acclists,
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                if (typeof data.status !== 'undefined' && data.status == 'FAIL') {
                    showDialogue(data.message, O_FAIL, O_OK)
                }else /*if (typeof data.data == "number" && data.conti == 0 )*/ {
                    $("#new_backup_building").hide();
                    $("#new_backup_building_success").show();
                    $("#span_easy_backup_time").html(moment(data.backupTime).format('MMMM Do YYYY, h:mm:ss a'));
                    $("#span_easy_backup_type").html(getEazyBackupType(data.backupType));
                }
            },
            error: function (request, textStatus, thrownError) {
                hideLoading();
                showDialogue(O_BACKUP_ERROR + thrownError + "<br /><pre>" + request.responseText + "</pre>",
                    O_ERROR, O_OK);
            }
        })
    })
}

function contbackup(sourcePath, outZipPath, serializefile) {
    showLoading('Archiving files, Please wait...');
    jQuery(document).ready(function ($) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'contBackup',
                task: 'contBackup',
                sourcePath: sourcePath,
                outZipPath: outZipPath,
                serializefile: serializefile,
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                if (data.data == false) {
                    hideLoading();
                    showDialogue(O_BACKUP_FAIL, O_FAIL, O_OK);
                } else if (data.conti == 1) {
                    contbackup(data.sourcePath, data.outZipPath, data.serializefile);

                } else if (data.conti == 0) {
                    showLoading(O_BACKUP_SUCCESS);
                    hideLoading();
                    $('#backupTable').dataTable().api().ajax.reload();
                }
            },
            error: function (request, textStatus, thrownError) {
                hideLoading();
                showDialogue(O_BACKUP_ERROR + thrownError + "<br /><pre>" + request.responseText + "</pre>",
                    O_ERROR, O_OK);
            }
        })
    })
}

function setupBackup() {
    showLoading("Retrieving Data...")
    jQuery(document).ready(function ($) {
        if ($('#session_exists').length) {
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {
                    option: option,
                    controller: controller,
                    action: 'listaccts',
                    task: 'listaccts',
                    connection: 'on',
                    centnounce: $('#centnounce').val()
                },
                success: function (data) {
                    hideLoading();
                    if (typeof data.status !== 'undefined' && data.status == 'FAIL') {
                        showDialogue(data.message, O_FAIL, O_OK)
                    } else {
                        $(".breadcrumb-span").html(O_BACKUP_CREATENEW_STEP2);
                        $("#breadcrumb .breadcrumb-nav:nth-child(2)").show();
                        $(".new_backup_content").hide();
                        var i = 0;
                        var text = '<div class="form-group"> <div class="row"><div class="col-md-3"><strong style="font-size: 20px;">Choose accounts to backup</strong></div><div class="col-md-3">';
                        while (i < data.length) {
                            text += '<label><input type="checkbox" name="acclists[]" value="' + data[i] + '">&nbsp&nbsp&nbsp' + data[i] + '&nbsp&nbsp&nbsp</label><br>';
                            i++;
                        }
                        text += '</div></div></div>';
                        $('#new_backup_config').append(text);
                        $("#new_backup_config").show();
                    }
                },
                error: function (request, textStatus, thrownError) {
                    hideLoading();
                    showDialogue(O_BACKUP_ERROR + thrownError + "<br /><pre>" + request.responseText + "</pre>",
                        O_ERROR, O_OK);
                }
            })
        } else {
            var admin_email = $('#input_admin_email').val();
            var admin_username = $('#input_admin_username').val();
            var admin_password = $('#input_admin_password').val();
            var server_ip = $('#input_server_ip').val();
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {
                    option: option,
                    controller: controller,
                    action: 'listaccts',
                    task: 'listaccts',
                    admin_email: admin_email,
                    admin_username: admin_username,
                    admin_password: admin_password,
                    server_ip: server_ip,
                    connection: 'off',
                    centnounce: $('#centnounce').val()
                },
                success: function (data) {
                    hideLoading();
                    if (typeof data.status !== 'undefined' && data.status == 'FAIL') {
                        showDialogue(data.message, O_FAIL, O_OK)
                    } else {
                        $(".breadcrumb-span").html(O_BACKUP_CREATENEW_STEP2);
                        $("#breadcrumb .breadcrumb-nav:nth-child(2)").show();
                        $(".new_backup_content").hide();
                        var i = 0;
                        var text = '<div class="form-group"> <div class="row"><div class="col-md-3"><strong style="font-size: 20px;">Choose accounts to backup</strong></div><div class="col-md-3">';
                        while (i < data.length) {
                            text += '<label><input type="checkbox" name="acclists[]" value="' + data[i] + '">&nbsp&nbsp&nbsp' + data[i] + '&nbsp&nbsp&nbsp</label><br>';
                            i++;
                        }
                        text += '</div></div></div>';
                        $('#new_backup_config').append(text);
                        $("#new_backup_config").show();
                    }
                },
                error: function (request, textStatus, thrownError) {
                    hideLoading();
                    showDialogue(O_BACKUP_ERROR + thrownError + "<br /><pre>" + request.responseText + "</pre>",
                        O_ERROR, O_OK);
                }
            })
        }

      //  checkCloudAuthentication();
    });
}

function setConfig() {
    jQuery(document).ready(function ($) {
        $(".breadcrumb-span").html(O_BACKUP_CREATENEW_STEP3);
        $("#breadcrumb .breadcrumb-nav:nth-child(3)").show();
        $(".new_backup_content").hide();
        $("#new_backup_building").show();
        backup();
    });
}


function redirectNewBackUp(cms) {
    if (ose_cms == 'wordpress') {
        window.location = 'admin.php?page=ose_fw_backup#sectionNew';
        window.location.reload();
    }
    else {
        window.location = 'index.php?option=com_ose_firewall&view=backup#sectionNew';
        window.location.reload();
    }


}

function redirectBackupList(cms) {
    if (ose_cms == 'wordpress') {
        window.location = 'admin.php?page=ose_fw_backup#sectionBKs';
        window.location.reload();
    }
    else {
        window.location = 'index.php?option=com_ose_firewall&view=backup#sectionBKs';
        window.location.reload();
    }
}


//-------------advance backup-----------------
jQuery(document).ready(function ($) {
    var font = {
        onChange: function (cep, event, currentField, options) {
            if (cep) {
                var ipArray = cep.split(".");
                for (i in ipArray) {
                    if (ipArray[i] != "" && parseInt(ipArray[i]) > 255) {
                        ipArray[i] = '255';
                    }
                }
                var resultingValue = ipArray.join(".");
                $(currentField).val(resultingValue);
            }
        }
    };

    $("[name=input_hosting_backup]").click(function(){
        $('.toHide').hide();
        $("#blk-"+$(this).val()).show('slow');
    });

    $('#input_server_ip').mask("099.099.099.099", font);

    $("[name=input_hosting_backup_init]").click(function(){
        $('.toHide').hide();
        $("#blk-"+$(this).val()+'-init').show('slow');
    });

    $('#input_server_ip_init').mask("099.099.099.099", font);
});

jQuery(document).ready(function($) {
    var timezone = jstz.determine().name();
    $('#div_scheduled_bk_content').html('<span class = "text-primary"><i class="fa fa-refresh fa-spin fa-1x fa-fw margin-bottom"></i>&nbspLoading Schedule Backup Records</span>');
    if(needSubscription == 1) {
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'getNextSchedule',
                task: 'getNextSchedule',
                centnounce: $('#centnounce').val(),
                timezone: timezone
            },
            success: function (data) {
                if (data.status == "empty") {
                    var html = getScheduledBkEmptyHtml();

                }
                else {
                    var html = getScheduledBkHtml(data.schedule_time);
                }
                $('#div_scheduled_bk_content').html(html);
            },
            error: function (request, textStatus, thrownError) {
                hideLoading();
                showDialogue(thrownError + "<br /><pre>" + request.responseText + "</pre>",
                    O_ERROR, O_OK);
            }
        });
    }
    else{
        var html = getScheduledBKNeedSubHtml();
        $('#div_scheduled_bk_content').html(html);
    }
    if ($('#blk-cpanel-init').length){

    } else {
        getRecentBackup();
    }


});

function getRecentBackup()
{
    jQuery(document).ready(function ($) {
        if ($('#blk-cpanel-init').length){
            showLoading("Connecting to Host...");
            var admin_email = $('#input_admin_email_init').val();
            var admin_username = $('#input_admin_username_init').val();
            var admin_password = $('#input_admin_password_init').val();
            var server_ip = $('#input_server_ip_init').val();
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {
                    option: option,
                    controller: controller,
                    action: 'getSuiteBackupList',
                    task: 'getSuiteBackupList',
                    admin_email: admin_email,
                    admin_username: admin_username,
                    admin_password: admin_password,
                    server_ip: server_ip,
                    connection: 'off',
                    centnounce: $('#centnounce').val()
                },
                success: function (data) {
                    hideLoading();
                    if (typeof data.status !== 'undefined' && data.status == 'FAIL') {
                        showDialogue(data.message, O_FAIL, O_OK)
                    }
                    else {
                        window.location.reload();
                    }
                },
                error: function (request, textStatus, thrownError) {
                    hideLoading();
                    showDialogue(thrownError + "<br /><pre>" + request.responseText + "</pre>",
                        O_ERROR, O_OK);
                }
            });
        } else {
            $('#recent_backup_content').html('<span class = "text-primary"><i class="fa fa-refresh fa-spin fa-1x fa-fw margin-bottom"></i>&nbspLoading Last Backup Records</span>');
            $.ajax({
                type: "POST",
                url: url,
                dataType: 'json',
                data: {
                    option: option,
                    controller: controller,
                    action: 'getSuiteBackupList',
                    task: 'getSuiteBackupList',
                    connection: 'on',
                    centnounce: $('#centnounce').val()
                },
                success: function (data) {
                    if (typeof data.status !== 'undefined' && data.status == 'FAIL') {
                        showDialogue(data.message, O_FAIL, O_OK)
                    }
                    else {
                        $('#recent_backup_content').html(data);
                    }
                },
                error: function (request, textStatus, thrownError) {
                    hideLoading();
                    showDialogue(thrownError + "<br /><pre>" + request.responseText + "</pre>",
                        O_ERROR, O_OK);
                }
            });
        }
    });
}

function getScheduledBkHtml(schedule_bk_time)
{
    var html = '';
    html += '<div class="row">' +
                '<div class="col-md-2">' +
                    '<strong>Files:</strong>' +
                '</div>' +
                '<div class="col-md-10">' +
                    '<a href="javascript:void(0)" class="text-warning" title="Files">' +
                    '<i class="text-primary glyphicon glyphicon-duplicate"></i></a>&nbsp' +
                    '<a href="javascript:void(0)" class="text-warning" title="DataBase">' +
                    '<i class="text-primary glyphicon glyphicon-hdd"></i></a> ' +
                '</div> ' +
            '</div> ' +
            '<div class="row"> ' +
                '<div class="col-md-2"><strong>Types:</strong></div> ' +
                '<div class="col-md-10"> ' +
                    '<div id = "div_schedule_bk_types"><a href="javascript:void(0)" class="text-warning fa fa-unlink" title="Local Backup"></a>&nbsp'+dropboxicon + onedriveicon + googledriveicon+'</div> ' +
                '</div> ' +
            '</div> ' +
            '<div class="row"> ' +
                '<div class="col-md-2"><strong>Schedule Time:</strong></div> ' +
                '<div class="col-md-10"> ' +
                    '<div id = "div_schedule_bk_time"></div> ' +
                    '<strong class="color-green" title="'+moment(schedule_bk_time).format('llll')+'">' +
                        '<i class="fa fa-clock-o"></i>&nbsp'+moment(schedule_bk_time).startOf('second').from()+'' +
                    '</strong> ' +
                '</div> ' +
            '</div>';

    return html;
}

function getScheduledBkEmptyHtml()
{
    var html = '<strong class="text-danger">No Schedule Backup Records Found</strong>';
    return html;
}

function getScheduledBKNeedSubHtml()
{
    var html = '<strong class="text-danger">Need Subscription&nbsp <button class="btn btn-danger mr5 mb10" type="button" onClick="location.href=\''+subscriptionLink+'\'"> <i class="im-cart6 mr5"></i> Subscrip Now</button> </strong>';
    return html;
}

function getLastBackupHtml(data)
{
    var html = '<div class="backup-long-desc">' +
                    '<div class="row" style="margin-top: 10px;"> ' +
                        '<div class="col-md-offset-9"> ' +
            //     '<button class="btn btn-success" onclick="restore('+data.ID+')">Restore</button>&nbsp' +
                            '<a class="btn btn-primary" href="'+data.downloadUrl+'" >Download</a>&nbsp' +
                        '</div> ' +
                    '</div> ' +
                    '<div class="row backup-item">' +
                        '<div class="col-md-2">' +
                            '<strong>Backup Name:</strong>' +
                        '</div>' +
                        '<div class="col-md-8">' +
                            '<strong class="text-primary">'+data.fileName+'</strong>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row backup-item">' +
                        '<div class="col-md-2">' +
                            '<strong>Backup Time:</strong>' +
                        '</div> ' +
                        '<div class="col-md-8">' +
                            '<i class="fa fa-clock-o"></i>&nbsp<strong>'+moment(data.time).format('MMMM Do YYYY, h:mm:ss a')+'</strong>' +
                        '</div> ' +
                    '</div>' +
                    '<div class="row backup-item">' +
                        '<div class="col-md-2">' +
                            '<strong>Backup Platforms:</strong>' +
                        '</div>' +
                        '<div class="col-md-8">' +
                            '<a href="javascript:void(0)" class="text-warning fa fa-unlink" title="Local Backup"></a>&nbsp' +
                             dropboxicon + onedriveicon + googledriveicon +
                        '</div>' +
                    '</div>' +
                    '<div class="row backup-item"> ' +
                        '<div class="col-md-2"> ' +
                            '<strong>Backup Files:</strong>' +
                        '</div>' +
                        '<div class="col-md-8">' +
                            '<a href="javascript:void(0)" class="text-warning" title="Files"><i class="text-primary glyphicon glyphicon-duplicate"></i></a>&nbsp<a href="javascript:void(0)" class="text-warning" title="DataBase"><i class="text-primary glyphicon glyphicon-hdd"></i></a> ' +
                        '</div> ' +
                    '</div> ' +
            '</div>';
    return html;
}

function getLastBackupEmptyHtml()
{
    var html = '<strong class="text-danger">No Backup Records Found</strong>';
    return html;
}

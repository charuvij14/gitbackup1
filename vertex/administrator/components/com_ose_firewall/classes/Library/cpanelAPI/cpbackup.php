<?php
/**
 * @version     2.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Centrora Security Firewall
 * @subpackage    Open Source Excellence WordPress Firewall
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 01-Jun-2013
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2012- ... Open Source Excellence
 */
if (! defined ( 'OSE_FRAMEWORK' ) && ! defined ( 'OSEFWDIR' ) && ! defined ( '_JEXEC' )) {
	die ( 'Direct Access Not Allowed' );
}
require ('xmlapi.php');

class cpbackup
{
    public function backup($source_server_ip, $server_password, $notify_email, $acclists)
    {
        $xmlapi = new xmlapi($source_server_ip);
        $xmlapi->password_auth("root", $server_password);
        $xmlapi->set_port('2087');
        //$xmlapi->set_debug(1);//this setting will put output into the error log in the directory that you are calling script from
        $xmlapi->set_output('array');
        $userlist = $xmlapi->listaccts();
        foreach ($userlist as $v => $field)
        {
            foreach ($field as $cpuser)
            {
                if (!empty($cpuser['user']))
                {
                    foreach ($acclists as $acc) {
                        if ($cpuser['user'] == $acc) {
                            $cpanel_account = $cpuser['user'];
                            $api_args = array('homedir', '', '', '', $notify_email, '', '');
                            $response = $xmlapi->api1_query($cpanel_account, 'Fileman', 'fullbackup', $api_args);
                            foreach ($response as $v2) {
                                $result = $v2['result'];
                            }
                        }
                    }
                }
            }
        }
        return true;
    }
    public function suitebackup($server_ip, $admin_password, $admin_email, $admin_username, $acclists)
    {
        $xmlapi = new xmlapi($server_ip);
        $xmlapi->password_auth($admin_username, $admin_password);
        $xmlapi->set_port('2087');
        //$xmlapi->set_debug(1);//this setting will put output into the error log in the directory that you are calling script from
        $xmlapi->set_output('array');
        $userlist = $xmlapi->listaccts();
        foreach ($userlist as $v => $field)
        {
            foreach ($field as $cpuser)
            {
                if (!empty($cpuser['user']))
                {
                    foreach ($acclists as $acc) {
                        if ($cpuser['user'] == $acc) {
                            $cpanel_account = $cpuser['user'];
                            $api_args = array('homedir', '', '', '', $admin_email, '', '');
                            $response = $xmlapi->api1_query($cpanel_account, 'Fileman', 'fullbackup', $api_args);
                            foreach ($response as $v2) {
                                $result = $v2['result'];
                            }
                            if ($result == 1) {
                                sleep(5);
                                return true;
                            }
                        }
                    }
                }
            }
        }
    }
    public function listfullbackups($server_ip, $admin_password, $admin_email, $admin_username)
    {
        $return = array();
        $xmlapi = new xmlapi($server_ip);
        $xmlapi->password_auth($admin_username, $admin_password);
        $xmlapi->set_port('2087');
        //$xmlapi->set_debug(1);//this setting will put output into the error log in the directory that you are calling script from
        $xmlapi->set_output('array');
        $userlist = $xmlapi->listaccts();
        if (array_key_exists('error', $userlist)) {
            $return['message'] = "Wrong username or password, try again";
            $return['status'] = "FAIL";
            return $return;
        } else {
            $session=  JFactory :: getSession();
            $session->set('cpanelEmail', $admin_email);
            $session->set('cpanelPassword', $admin_password);
            $session->set('cpanelUsername', $admin_username );
            $session->set('cpanelServerIP', $server_ip);
        }
        foreach ($userlist as $v => $field)
        {
            foreach ($field as $cpuser)
            {
                if (!empty($cpuser['user']))
                {
                    $cpanel_account = $cpuser['user'];
                    $hostname = $cpuser['domain'];
                    $response = $xmlapi->api1_query($cpanel_account, 'Fileman', 'listfullbackups');
                    $result = $response['data']['result'];
                    $session = $xmlapi->create_user_session($cpanel_account, 'cpaneld');
                    $token = $session['data']['cp_security_token'];
                    $replace = 'https://'.$hostname.":2083".$token."/download" ;
                    $result = str_replace('/download', $replace, $result);
                    $result = str_replace('<a', '<a target="_blank"', $result);
                    array_push($return, $result);
                }
            }
        }
        $list = implode(" ", $return);
        return $list;
    }
    public function listaccts($server_ip, $admin_password, $admin_email, $admin_username)
    {
        $return = array();
        $xmlapi = new xmlapi($server_ip);
        $xmlapi->password_auth($admin_username, $admin_password);
        $xmlapi->set_port('2087');
        //$xmlapi->set_debug(1);//this setting will put output into the error log in the directory that you are calling script from
        $xmlapi->set_output('array');
        $userlist = $xmlapi->listaccts();
        if (array_key_exists('error', $userlist)) {
            $return['message'] = "Wrong username or password, try again";
            $return['status'] = "FAIL";
            return $return;
        } else {
            $session=  JFactory :: getSession();
            $session->set('cpanelEmail', $admin_email);
            $session->set('cpanelPassword', $admin_password);
            $session->set('cpanelUsername', $admin_username );
            $session->set('cpanelServerIP', $server_ip);
        }
        foreach ($userlist as $v => $field)
        {
            foreach ($field as $cpuser)
            {
                if (!empty($cpuser['user']))
                {
                    array_push($return, $cpuser['user']);
                }
            }
        }
        if (!empty($return)) {

        }
        return $return;
    }

    public function saveSession($server_ip, $admin_password, $admin_email, $admin_username)
    {
        $return = array();
        $xmlapi = new xmlapi($server_ip);
        $xmlapi->password_auth($admin_username, $admin_password);
        $xmlapi->set_port('2087');
        //$xmlapi->set_debug(1);//this setting will put output into the error log in the directory that you are calling script from
        $xmlapi->set_output('array');
        $userlist = $xmlapi->listaccts();
        if (array_key_exists('error', $userlist)) {
            $return['message'] = "Wrong username or password, try again";
            $return['status'] = "FAIL";
            return $return;
        } else {
            $session=  JFactory :: getSession();
            $session->set('cpanelEmail', $admin_email);
            $session->set('cpanelPassword', $admin_password);
            $session->set('cpanelUsername', $admin_username );
            $session->set('cpanelServerIP', $server_ip);
            return true;
        }
    }
}
?>
<?php
/**
 * @version     2.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Centrora Security Firewall
 * @subpackage    Open Source Excellence WordPress Firewall
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 01-Jun-2013
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * @Copyright Copyright (C) 2008 - 2012- ... Open Source Excellence
 */
if (!defined('OSE_FRAMEWORK') && !defined('OSEFWDIR') && !defined('_JEXEC')) {
    die('Direct Access Not Allowed');
}
oseFirewall::callLibClass('vsscanner', 'vsscanner');

class cfScanner extends virusScanner
{
    const CHUNK_SIZE = 2048;
    var $files = array();
    var $wordpressSites = array();
    var $joomlaSites = array();
    var $suiteSama = array();
    private $db = null;
    private $filestable = '#__osefirewall_files';
    private $vshashtable = '#__osefirewall_vshash';
    private $scanhisttablebl = '#__osefirewall_scanhist';

    public function __construct()
    {
        $this->db = oseFirewall::getDBO();
    }
    public function wpcfscan()
    {
        global $wp_version;
        $modified = array();
        $suspicious = array();
        $return = array();
        $wproot = array(
            'index.php', 'wp-activate.php', 'wp-blog-header.php', 'wp-comments-post.php', 'wp-config.php', 'wp-config-sample.php',
            'wp-cron.php', 'wp-links-opml.php', 'wp-load.php', 'wp-login.php', 'wp-mail.php', 'wp-settings.php', 'wp-signup.php',
            'wp-trackback.php', 'xmlrpc.php'
        );
        $condition = array('type' => 'S');
        $this->db->deleteRecordString($condition, $this->filestable);
        if (true) {
            unset($filehashes);
            $hashes = OSE_FWDATA . ODS . 'wpHashList' . ODS . 'hashes-' . $wp_version . '.php';
            if (file_exists($hashes)) {
                include($hashes);
            } else {
                $return['cont'] = false;
                $return['summary'] = 'hashes-' . $wp_version . '.php not found';
                $return['details'] = 'The file containing hashes of all WordPress core files currently not available, please contact centrora support team';
                $return['status'] = 'Interupted';
                return $return;
            }

            $this->recurse_directory(OSE_ABSPATH);

            foreach ($this->files as $k => $file) {
                // don't scan unmodified core files
                if (isset($filehashes[$file])) {
                    if ($filehashes[$file] == md5_file(OSE_ABSPATH . '/' . $file)) {
                        unset($this->files[$k]);
                        continue;
                    } else {
                        $file_with_filesize = $file . "---" . $this->human_filesize(filesize(OSE_ABSPATH . '/' . $file));
                        array_push($modified, $file_with_filesize);
                    }
                } else {
                    // scan suspicious files
                    list($dir, $flag) = explode('/', $file);
                    if ($dir == 'wp-includes' || $dir == 'wp-admin' || empty($flag)) {
                        if (substr($file, -4) == '.php' && !in_array($dir, $wproot)) {

                            $file_with_filesize = $file . "---" . $this->human_filesize(filesize(OSE_ABSPATH . '/' . $file));

                            array_push($suspicious, $file_with_filesize);

                            $insertFolder = array(
                                'filename' => OSE_ABSPATH . '/' . $file,
                                'ext' => 'file',
                                'type' => 'S'
                            );
                            $this->db->addData('insert', $this->filestable, '', '', $insertFolder);
                        }
                    }
                }
            }
            $count1 = sizeof($modified);
            $count2 = sizeof($suspicious);
            $detail1 = implode('<br>', $modified);
            $detail2 = implode('<br>', $suspicious);

            $return['cont'] = false;
            $return['summary'] = 'There are ' . $count1 . ' core files modified and ' . $count2 . ' suspicious files found in wp-includes/ or wp-admin/ or root directory.';
            $return['modified'] = $detail1;
            $return['suspicious'] = $detail2;
        $return['status'] = 'Completed';
            $return['count1'] = $count1;
            $return['count2'] = $count2;

            $scanList['scanlist'] = array();
            $scanList['totalscan'] = count($this->files);
            $scanList['totalvs'] = $count1 + $count2;
            $scanList['vsfilelist'] = array();
            $this->saveDBLastScanResult($scanList);

            return $return;
        }
    }

    private function recurse_directory($dir)
    {
        if ($handle = @opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != '.' && $file != '..') {
                    $file = $dir . '/' . $file;
                    if (is_dir($file)) {
                        $this->recurse_directory($file);
                    } elseif (is_file($file)) {
                        $this->files[] = str_replace(OSE_ABSPATH . '/', '', $file);
                    }
                }
            }
            closedir($handle);
        }
    }

    public function jcfscan($start = 0)
    {
        $return = array();
        $modified = array();
        $missing = array();
        $suspicious = array();
        // version information
        $version = $this->getCurrentJoomlaVersion();
        $condition = array('type' => 'S');
        $this->db->deleteRecordString($condition, $this->filestable);
        // Below stable?
        if ($this->isAlpha($version)) {
            $return['cont'] = false;
            $return['summary'] = 'Current version ' . $version . ' is not a stable version';
            $return['details'] = NO_HASHES_FOR_ALPHA;
            $return['status'] = 'Interupted';
            return $return;
        }

        try {
            $jcorefile = OSE_FWDATA . ODS . 'jHashList' . ODS . 'jcore' . $version . '.php';
            if (file_exists($jcorefile)) {
                include($jcorefile);
            } else {
                $return['cont'] = false;
                $return['summary'] = 'jcore.php file not found';
                $return['details'] = 'The file containing all Joomla core files currently not available, please contact centrora support team';
                $return['status'] = 'Interupted';
                return $return;
            }
            $hashes = OSE_FWDATA . ODS . 'jHashList' . ODS . $version . '.csv';
            if (file_exists($hashes)) {
                if ($handle = @fopen($hashes, 'r')) {

                    // set pointer to last value
                    fseek($handle, $start);

                    // read data
                    while (($data = fgetcsv($handle, self::CHUNK_SIZE, ',')) !== false) {

                        list($file_path, $file_hash) = $data;

                        $full_path = JPATH_SITE . '/' . $file_path;

                        if (file_exists($full_path)) {
                            // does this file have a wrong checksum ?
                            if (md5_file($full_path) != $file_hash) {
                                $file_with_filesize = $file_path . "---" . $this->human_filesize(filesize($full_path));
                                array_push($modified, $file_with_filesize);
                            }
                        } else {
                            // record missing files
                            $file_with_filesize = $file_path . "---" . $this->human_filesize(filesize($full_path));
                            array_push($missing, $file_with_filesize);
                        }
                    }
                    // scan suspicious files
                    $core_dir = array(
                        JPATH_SITE . '/administrator/includes', JPATH_SITE . '/includes', JPATH_SITE . '/templates/beez3',
                        JPATH_SITE . '/templates/protostar', JPATH_SITE . '/templates/system', JPATH_SITE . '/libraries',
                        JPATH_SITE . '/administrator/components/com_admin',
                    );
                    $core_root = array(
                        'index.php', 'configuration.php'
                    );
                    $files = scandir(JPATH_SITE);

                    foreach ($files as $file) {
                        if (is_file(JPATH_SITE . ODS . $file)) {

                            if (!in_array($file, $core_root) && substr($file, -4) == '.php') {

                                $file_with_filesize = $file . "---" . $this->human_filesize(filesize(JPATH_SITE . '/' . $file));
                                array_push($suspicious, $file_with_filesize);

                                $insertFolder = array(
                                    'filename' => JPATH_SITE . ODS . $file,
                                    'ext' => 'file',
                                    'type' => 'S'
                                );
                                $this->db->addData('insert', $this->filestable, '', '', $insertFolder);
                            }
                        }
                    }

                    foreach ($core_dir as $single) {
                        if (file_exists($single)) {
                            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($single)) as $filename) {

                                if ($filename->isDir()) continue;
                                $filename = str_replace(JPATH_SITE . '/', '', $filename);
                                $exclude = explode('/', $filename);
                                if ($exclude[1] == 'f0f') continue;
                                if (!in_array($filename, $jcole) && substr($filename, -4) == '.php') {
                                    $file_with_filesize = $filename . "---" . $this->human_filesize(filesize(JPATH_SITE . '/' . $filename));
                                    array_push($suspicious, $file_with_filesize);
                                }
                                closedir($single);
                            }
                        }
                    }
                    fclose($handle);

                    $count1 = sizeof($modified);
                    $count2 = sizeof($suspicious);
                    $count3 = sizeof($missing);
                    $detail1 = implode('<br>', $modified);
                    $detail2 = implode('<br>', $suspicious);
                    $detail3 = implode('<br>', $missing);

                    $return['cont'] = false;
                    $return['summary'] = 'There are ' . $count1 . ' core files modified, ' . $count2 . ' suspicious files, ' . $count3 . ' files missing.';
                    $return['modified'] = $detail1;
                    $return['missing'] = $detail3;
                    $return['suspicious'] = $detail2;
                    $return['status'] = 'Completed';
                    $return['count1'] = $count1;
                    $return['count2'] = $count2;
                    $return['count3'] = $count3;

                    $scanList['scanlist'] = array();
                    $scanList['totalscan'] = count($data);
                    $scanList['totalvs'] = $count1 + $count2 + $count3;
                    $scanList['vsfilelist'] = array();
                    $this->saveDBLastScanResult($scanList);

                    return $return;
                }
            } else {
                $return['cont'] = false;
                $return['summary'] = 'hash file ' . $version . '.csv missing';
                $return['details'] = 'The file containing hashes of all Joomla core files appears to be missing';
                $return['status'] = 'Interupted';
                return $return;
            }

        } catch (Exception $e) {
            $return['cont'] = false;
            $return['summary'] = 'There are unknown errors during scanning' . $e->getMessage();
            $return['details'] = 'Cannot scan files, possible reason is memory limit or file size too large';
            $return['status'] = 'Interupted';
            return $return;
        }
    }

    public function isAlpha($version = null)
    {
        if (is_null($version)) {
            $version = $this->getCurrentJoomlaVersion();
        }

        return preg_match('#[a-z]+#i', $version);
    }

    protected function getCurrentJoomlaVersion()
    {
        static $current = null;

        if (is_null($current)) {
            $jversion = new JVersion();
            $current = $jversion->getShortVersion();
            if (strpos($current, ' ') !== false) {
                $current = reset(explode(' ', $current));
            }
        }
        return $current;
    }

    public function getMultiSite()
    {
        $i = 0;
        $suiteFolder = array('httpdocs', 'htdocs', 'public_html');
        // scan root folder
        $roots = scandir(dirname(OSE_ABSPATH));
        foreach ($roots as $root) {
            if (is_dir(dirname(OSE_ABSPATH) . ODS . $root) && $root != '.' && $root != '..') {
                // scan layer1 folder
                $branch1s = scandir(dirname(OSE_ABSPATH) . ODS . $root);
                foreach ($branch1s as $branch1) {
                    if (is_dir(dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1) && in_array($branch1, $suiteFolder)) {
                        // scan layer2 folder
                        $branch2s = scandir(dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1);
                        foreach ($branch2s as $branch2) {
                            if (is_dir(dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1 . ODS . $branch2) && $branch2 != '.' && $branch2 != '..') {
                                // scan layer3 folder
                                $branch3s = scandir(dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1 . ODS . $branch2);

                                foreach ($branch3s as $branch3) {
                                    // detect cms type
                                    if (is_dir(dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1 . ODS . $branch2 . ODS . $branch3) && $branch3 == 'wp-includes') {

                                        $wpVersionFile = dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1 . ODS . $branch2 . ODS . $branch3 . ODS . 'version.php';

                                        if (file_exists($wpVersionFile)) {
                                            //  array_push($this->wordpressSites, $bounty);
                                            $WPoutput = ' <label>Wordpress Site: ' . $branch2 . '</label><input type="checkbox" name="wppath[]" value="' . dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1 . ODS . $branch2 . '"><br>';
                                            echo $WPoutput;
                                            $i++;
                                        }
                                    } elseif (is_dir(dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1 . ODS . $branch2 . ODS . $branch3) && $branch3 == 'libraries') {
                                        $joomlaVersionFile = dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1 . ODS . $branch2 . ODS . $branch3 . ODS . 'cms/version/version.php';
                                        if (file_exists($joomlaVersionFile)) {
                                            //    array_push($this->joomlaSites, $joomlaVersionFile);
                                            $Joutput = ' <label>Joomla site: ' . $branch2 . '</label><input type="checkbox" name="joomlapath[]" value="' . dirname(OSE_ABSPATH) . ODS . $root . ODS . $branch1 . ODS . $branch2 . '"><br>';
                                            echo $Joutput;
                                            $i++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        echo $i > 0 ? "" : NO_DETECT_WEBSITE;
    }

    public function suitecfscan($wppaths, $joomlapaths)
    {
        $condition = array('type' => 'S');
        $this->db->deleteRecordString($condition, $this->filestable);
        $vsCount = 0;
        $totalCount = 0;
        if (!empty($wppaths)) {
            foreach ($wppaths as $wppath) {
                require_once($wppath . ODS . 'wp-includes/version.php');
                $siteName = end(explode('/', $wppath));
                $this->suiteSama[$siteName] = $this->suitewpcfscan($wp_version, $wppath);
                $vsCount += $this->suiteSama[$siteName]['count1'] + $this->suiteSama[$siteName]['count2'];
                $totalCount += $this->suiteSama[$siteName]['totalScan'];
            }
        }
        if (!empty($joomlapaths)) {
            foreach ($joomlapaths as $joomlapath) {
                $versionContent = file_get_contents($joomlapath . ODS . 'libraries/cms/version/version.php');
                preg_match_all("/(RELEASE|DEV_LEVEL) = ('\d+'|'\d.\d+')/", $versionContent, $output_array);
                $joomla_version = str_replace("'", "", $output_array[2][0]) . '.' . str_replace("'", "", $output_array[2][1]);
                $siteName = end(explode('/', $joomlapath));
                $this->suiteSama[$siteName] = $this->suitejoomlacfscan($joomla_version, $joomlapath);
                $vsCount += $this->suiteSama[$siteName]['count1'] + $this->suiteSama[$siteName]['count2'] + $this->suiteSama[$siteName]['count3'];
                $totalCount += $this->suiteSama[$siteName]['totalScan'];
            }
        }
        $scanList['scanlist'] = array();
        $scanList['totalscan'] = $totalCount;
        $scanList['totalvs'] = $vsCount;
        $scanList['vsfilelist'] = array();
        $this->saveDBLastScanResult($scanList);
        return $this->suiteSama;
    }

    public function suitejoomlacfscan($joomla_version, $joomlapath)
    {
        $return = array();
        $modified = array();
        $missing = array();
        $suspicious = array();

        try {
            $jcorefile = OSE_FWDATA . ODS . 'jHashList' . ODS . 'jcore' . $joomla_version . '.php';
            if (file_exists($jcorefile)) {
                include($jcorefile);
            } else {
                $return['cont'] = false;
                $return['summary'] = 'jcore.php file not found';
                $return['details'] = 'The file containing all Joomla core files currently not available, please contact centrora support team';
                $return['status'] = 'Interupted';
                return $return;
            }
            $hashes = OSE_FWDATA . ODS . 'jHashList' . ODS . $joomla_version . '.csv';
            if (file_exists($hashes)) {
                if ($handle = @fopen($hashes, 'r')) {

                    // set pointer to last value
                    fseek($handle, 0);

                    // read data
                    while (($data = fgetcsv($handle, self::CHUNK_SIZE, ',')) !== false) {

                        list($file_path, $file_hash) = $data;

                        $full_path = $joomlapath . '/' . $file_path;

                        if (file_exists($full_path)) {
                            // does this file have a wrong checksum ?
                            if (md5_file($full_path) != $file_hash) {
                                $file_with_filesize = $file_path . "---" . $this->human_filesize(filesize($full_path));
                                array_push($modified, $file_with_filesize);
                            }
                        } else {
                            // record missing files
                            $file_with_filesize = $file_path . "---" . $this->human_filesize(filesize($full_path));
                            array_push($missing, $file_with_filesize);
                        }
                    }
                    // scan suspicious files
                    $core_dir = array(
                        $joomlapath . '/administrator/includes', $joomlapath . '/includes', $joomlapath . '/templates/beez3',
                        $joomlapath . '/templates/protostar', $joomlapath . '/templates/system', $joomlapath . '/libraries'
                    );
                    $core_root = array(
                        'index.php', 'configuration.php'
                    );
                    $files = scandir($joomlapath);

                    foreach ($files as $file) {
                        if (is_file($joomlapath . ODS . $file)) {

                            if (!in_array($file, $core_root) && substr($file, -4) == '.php') {
                                $file_with_filesize = $file . "---" . $this->human_filesize(filesize($joomlapath . ODS . $file));
                                array_push($suspicious, $file_with_filesize);
                                $insertFolder = array(
                                    'filename' => $joomlapath . ODS . $file,
                                    'ext' => 'folder',
                                    'type' => 'S'
                                );
                                $this->db->addData('insert', $this->filestable, '', '', $insertFolder);
                            }
                        }
                    }

                    foreach ($core_dir as $single) {
                        if (file_exists($single)) {
                            foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($single)) as $filename) {

                                if ($filename->isDir()) continue;
                                $filename = str_replace($joomlapath . '/', '', $filename);

                                $exclude = explode('/', $filename);
                                if ($exclude[1] == 'f0f') continue;

                                if (!in_array($filename, $jcole) && substr($filename, -4) == '.php') {
                                    $file_with_filesize = $filename . "---" . $this->human_filesize(filesize($joomlapath . ODS . $filename));
                                    array_push($suspicious, $file_with_filesize);
                                    $insertFolder = array(
                                        'filename' => $joomlapath . ODS . $file,
                                        'ext' => 'folder',
                                        'type' => 'S'
                                    );
                                    $this->db->addData('insert', $this->filestable, '', '', $insertFolder);
                                }
                                closedir($single);
                            }
                        }
                    }
                    fclose($handle);

                    $count1 = sizeof($modified);
                    $count2 = sizeof($suspicious);
                    $count3 = sizeof($missing);
                    $detail1 = implode('<br>', $modified);
                    $detail2 = implode('<br>', $suspicious);
                    $detail3 = implode('<br>', $missing);

                    $return['cont'] = false;
                    $return['summary'] = 'There are ' . $count1 . ' core files modified, ' . $count2 . ' suspicious files, ' . $count3 . ' files missing.';
                    $return['modified'] = $detail1;
                    $return['missing'] = $detail3;
                    $return['suspicious'] = $detail2;
                    $return['status'] = 'Completed';
                    $return['count1'] = $count1;
                    $return['count2'] = $count2;
                    $return['count3'] = $count3;
                    $return['totalScan'] = count($data);

                    return $return;
                }
            } else {
                $return['cont'] = false;
                $return['summary'] = 'hash file ' . $joomla_version . '.csv missing';
                $return['details'] = 'The file containing hashes of all Joomla core files appears to be missing';
                $return['status'] = 'Interupted';
                return $return;
            }

        } catch (Exception $e) {
            $return['cont'] = false;
            $return['summary'] = 'There are unknown errors during scanning '.$e->getMessage();
            $return['details'] = 'Cannot scan files, possible reason is memory limit or file size too large';
            $return['status'] = 'Interupted';
            return $return;
        }
    }

    public function suitewpcfscan($wp_version, $wppath)
    {
        $modified = array();
        $suspicious = array();
        $return = array();
        $wproot = array(
            'index.php', 'wp-activate.php', 'wp-blog-header.php', 'wp-comments-post.php', 'wp-config.php', 'wp-config-sample.php',
            'wp-cron.php', 'wp-links-opml.php', 'wp-load.php', 'wp-login.php', 'wp-mail.php', 'wp-settings.php', 'wp-signup.php',
            'wp-trackback.php', 'xmlrpc.php'
        );
        if (true) {
            unset($filehashes);
            $hashes = OSE_FWDATA . ODS . 'wpHashList' . ODS . 'hashes-' . $wp_version . '.php';
            if (file_exists($hashes)) {
                include($hashes);
            } else {
                $return['cont'] = false;
                $return['summary'] = 'hashes-' . $wp_version . '.php not found';
                $return['details'] = 'The file containing hashes of all WordPress core files currently not available, please contact centrora support team';
                $return['status'] = 'Interupted';
                return $return;
            }

            $this->recurse_directory($wppath);

            foreach ($this->files as $k => $file) {
                // don't scan unmodified core files
                if (isset($filehashes[$file])) {
                    if ($filehashes[$file] == md5_file($wppath . '/' . $file)) {
                        unset($this->files[$k]);
                        continue;
                    } else {
                        $file_with_filesize = $file . "---" . $this->human_filesize(filesize($wppath . '/' . $file));
                        array_push($modified, $file_with_filesize);
                    }
                } else {
                    // scan suspicious files
                    list($dir, $flag) = explode('/', $file);

                    if ($dir == 'wp-includes' || $dir == 'wp-admin' || empty($flag)) {
                        if (substr($file, -4) == '.php' && !in_array($dir, $wproot)) {
                            $file_with_filesize = $file . "---" . $this->human_filesize(filesize($wppath . '/' . $file));
                            array_push($suspicious, $file_with_filesize);
                            $insertFolder = array(
                                'filename' => $wppath . '/' . $file,
                                'ext' => 'folder',
                                'type' => 'S'
                            );
                            $this->db->addData('insert', $this->filestable, '', '', $insertFolder);
                        }
                    }
                }
            }
            $count1 = sizeof($modified);
            $count2 = sizeof($suspicious);
            $detail1 = implode('<br>', $modified);
            $detail2 = implode('<br>', $suspicious);

            $return['cont'] = false;
            $return['summary'] = 'There are ' . $count1 . ' core files modified and ' . $count2 . ' suspicious files found in wp-includes/ or wp-admin/ or root directory.';
            $return['modified'] = $detail1;
            $return['suspicious'] = $detail2;
            $return['status'] = 'Completed';
            $return['count1'] = $count1;
            $return['count2'] = $count2;
            $return['totalScan'] = count($this->files);
            return $return;
        }
    }

    public function catchVirusMD5()
    {
        $return = array();
        $query = "SELECT `filename` FROM " . $this->db->quoteTable($this->filestable) . " WHERE type = 'S'";
        $this->db->setQuery($query);
        $result = $this->db->loadObjectList();
        if (!empty($result)) {
            foreach ($result as $single) {

                $insertFolder = array(
                    'hash' => md5_file($single->filename),
                    'name' => 'local.' . $single->filename,
                    'type' => 0,
                    'inserted_on' => oseFirewall::getTime()
                );
                $this->db->addData('insert', $this->vshashtable, '', '', $insertFolder);
            }
        }
        $return['status'] = 'update';
        return $return;
    }

    private function saveDBLastScanResult($content)
    {
        $varValues = array('super_type' => 'cfscan',
            'sub_type' => 1,
            'content' => oseJSON::encode(array($content)),
            'inserted_on' => oseFirewall::getTime()
        );
        $this->db->addData('insert', $this->scanhisttablebl, '', '', $varValues);
    }
}

?>

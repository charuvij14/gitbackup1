<?php
/**
 * @version       1.0 +
 * @package       Open Source Excellence Marketing Software
 * @subpackage    Open Source Excellence Affiates - com_ose_affiliates
 * @author        Open Source Excellence (R) {@link  http://www.opensource-excellence.com}
 * @author        Created on 01-Oct-2011
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2010- Open Source Excellence (R)
 */
// No direct access
defined('_JEXEC') or die;
/**
 * Content component helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since		1.6
 */
if (!class_exists('JFolder'))
{
	jimport('joomla.filesystem.folder');
}
if (!class_exists('JFile'))
{
	jimport('joomla.filesystem.file');
}
class OSESoftHelper
{
	public $timezone = 'America/Los_Angeles';
	public static $extension = '';
	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	$vName	The name of the active view.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function __construct()
	{
		self::$extension = $this->getExtensionName();
		$version = new JVersion();
		$version = substr($version->getShortVersion(), 0, 3);
		if (!defined('JOOMLA16'))
		{
			$value = ($version >= '1.6') ? true : false;
			define('JOOMLA16', $value);
		}
	}
	public static function getExtensionName()
	{
		return 'com_ose_antivirus';
	}
	public static function getMenu()
	{
		$db = JFactory::getDBO();
		$query = "SELECT * FROM `#__menu` WHERE `alias` =  'OSE Anti-Virus™'";
		$db->setQuery($query);
		$results = $db->loadResult();
		if (empty($results))
		{
			$query = "UPDATE `#__menu` SET `alias` =  'OSE Anti-Virus™', `path` =  'OSE Anti-Virus™', `published`=1, `img` = '\"components/com_ose_antivirus/favicon.ico\"'  WHERE `component_id` = ( SELECT extension_id FROM `#__extensions` WHERE `element` ='com_ose_antivirus')  AND `client_id` = 1 ";
			$db->setQuery($query);
			$db->query();
		}
		self::$extension = self::getExtensionName();
		$view = JRequest::getVar('view');
		$menu = '<div class="menu-search">';
		$menu .= '<ul>';
		$menu .= '<li ';
		$menu .= ($view == 'scan') ? 'class="current"' : '';
		$menu .= '><a href="index.php?option='.self::$extension.'&view=scan">'.JText::_('Virus Scanning').'</a></li>';
		if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_fileman'.DS.'admin.ose_fileman.php'))
		{
			$menu .= '<li ';
			$menu .= ($view == 'directories') ? 'class="current"' : '';
			$menu .= '><a href="index.php?option=com_ose_fileman&view=directories">'.JText::_('File Manager').'</a></li>';
		}
		$menu .= '<li ';
		$menu .= ($view == 'quarantine') ? 'class="current"' : '';
		$menu .= '><a href="index.php?option='.self::$extension.'&view=quarantine">'.JText::_('Quarantine').'</a></li>';
		$menu .= '<li ';
		$menu .= ($view == 'report') ? 'class="current"' : '';
		$menu .= '><a href="index.php?option='.self::$extension.'&view=report">'.JText::_('Scan Report').'</a></li>';
		$menu .= '<li ';
		$menu .= ($view == 'whitelist') ? 'class="current"' : '';
		$menu .= '><a href="index.php?option='.self::$extension.'&view=whitelist">'.JText::_('Whitelist Files').'</a></li>';
		$menu .= '<li ';
		$menu .= ($view == 'clamav') ? 'class="current"' : '';
		$menu .= '><a href="index.php?option='.self::$extension.'&view=clamav">'.JText::_('ClamAV Setting (Advanced)').'</a></li>';
		$menu .= '<li ';
		$menu .= ($view == 'config') ? 'class="current"' : '';
		$menu .= '><a href="index.php?option='.self::$extension.'&view=config">'.JText::_('Configuration').'</a></li>';
		$menu .= '<li ';
		$menu .= ($view == 'aboutose') ? 'class="current"' : '';
		$menu .= '><a href="index.php?option='.self::$extension.'&view=aboutose">'.JText::_('ABOUTOSE').'</a></li>';
		$menu .= '</ul></div>';
		return $menu; 
	}
	public static function checkAdminAccess()
	{
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$db->setQuery("SELECT id FROM #__usergroups");
		$groups = $db->loadObjectList();
		$admin_groups = array();
		foreach ($groups as $group)
		{
			if (JAccess::checkGroup($group->id, 'core.login.admin'))
			{
				$admin_groups[] = $group->id;
			}
			elseif (JAccess::checkGroup($group->id, 'core.admin'))
			{
				$admin_groups[] = $group->id;
			}
		}
		$admin_groups = array_unique($admin_groups);
		$user_groups = JAccess::getGroupsByUser($user->id);
		if (count(array_intersect($user_groups, $admin_groups)) > 0)
		{
			$access = true;
		}
		else
		{
			$access = false;
		}
		return $access;
	}
	public static function getVersion()
	{
		$folder = JPATH_ADMINISTRATOR.DS.'components'.DS.self::$extension;
		if (JFolder::exists($folder))
		{
			$xmlFilesInDir = JFolder::files($folder, '.xml$');
		}
		else
		{
			$folder = JPATH_SITE.DS.'components'.DS.self::$extension;
			if (JFolder::exists($folder))
			{
				$xmlFilesInDir = JFolder::files($folder, '.xml$');
			}
			else
			{
				$xmlFilesInDir = null;
			}
		}
		$xml_items = '';
		if (count($xmlFilesInDir))
		{
			foreach ($xmlFilesInDir as $xmlfile)
			{
				if ($data = JApplicationHelper::parseXMLInstallFile($folder.DS.$xmlfile))
				{
					foreach ($data as $key => $value)
					{
						$xml_items[$key] = $value;
					}
				}
			}
		}
		if (isset($xml_items['version']) && $xml_items['version'] != '')
		{
			return $xml_items['version'];
		}
		else
		{
			return '';
		}
	}
	function ajaxResponse($status, $message, $data = null, $url = null)
	{
		$return['title'] = $status;
		$return['content'] = $message;
		$return['data'] = $data;
		$return['url'] = $url;
		echo oseJSON::encode($return);
		exit;
	}
	public static function returnMessages($status, $messages)
	{
		$result = array();
		if ($status == true)
		{
			$result['success'] = true;
			$result['status'] = 'Done';
			$result['result'] = $messages;
		}
		else
		{
			$result['success'] = false;
			$result['status'] = 'Error';
			$result['result'] = $messages;
		}
		$result = oseJSON::encode($result);
		print_r($result);
		exit;
	}
	public static function renderOSETM()
	{
		$a = base64_decode('PGRpdiBpZCA9ICJvc2Vmb290ZXIiPjxkaXYgY2xhc3M9ImZvb3Rlci10ZXh0Ij5Qb3dlcmVkIGJ5IDxhIGhyZWY9Imh0dHA6Ly93d3cub3BlbnNvdXJjZS1leGNlbGxlbmNlLmNvbS9zaG9wL29zZS1zZWN1cml0eS1zb2x1dGlvbi5odG1sIiBzdHlsZT0idGV4dC1kZWNvcmF0aW9uOiBub25lOyIgdGFyZ2V0PSJfYmxhbmsiIHRpdGxlPSJPU0UgQW50aS1WaXJ1cyI+T1NFIEFudGktVmlydXPihKIg');
		$a .= '<small><small>[Version: '.self::getVersion().']</small></small></a></div></div>';
		return $a;
	}
	public static function getFolderName($class)
	{
		$classname = explode("View", get_class($class));
		$classname = strtolower($classname[1]);
		return $classname;
	}
	public function loadCats($cats = array())
	{
		if (is_array($cats))
		{
			$i = 0;
			$return = array();
			foreach ($cats as $JCatNode)
			{
				$return[$i]->title = $JCatNode->title;
				$return[$i]->cat_id = $JCatNode->id;
				if ($JCatNode->hasChildren())
					$return[$i]->children = self::loadCats($JCatNode->getChildren());
				else
					$return[$i]->children = false;
				$i++;
			}
			return $return;
		}
		return false;
	}
	public function loadCatTree($cats = array(), $return = array())
	{
		if (is_array($cats))
		{
			$i = 0;
			$curreturn = array();
			foreach ($cats as $JCatNode)
			{
				$curreturn[$i]->title = '['.$JCatNode->id.'] '.str_repeat('-', $JCatNode->level - 1).' '.$JCatNode->title;
				$curreturn[$i]->cat_id = $JCatNode->id;
				if ($JCatNode->hasChildren())
				{
					$subreturn = self::loadCatTree($JCatNode->getChildren(), $return);
				}
				$i++;
			}
			$return = array_merge($curreturn, $return);
			//$return = array_unique($return);
			return $return;
		}
		return false;
	}
	function loadOrders($table)
	{
		$db =& JFactory::getDBO();
		$query = "SELECT CONCAT (`ordering`, ' - ', `title`) as title, `ordering` FROM `{$table}` ORDER BY `ordering` ASC";
		$db->setQuery($query);
		$results = $db->loadObjectList();
		return (!empty($results)) ? $results : null;
	}
	public static function oseGetToken()
	{
		if (JOOMLA30 == true)
		{
			$token = JSession::getFormToken();
		}
		else
		{
			$token = JUtility::getToken();
		}
		return $token;
	}
	public static function checkToken($method = 'post')
	{
		$token = self::oseGetToken();
		if (!JRequest::getVar($token, '', $method, 'alnum'))
		{
			$session = JFactory::getSession();
			if ($session->isNew())
			{
				// Redirect to login screen.
				$app = JFactory::getApplication();
				$return = JRoute::_('index.php');
				self::ajaxResponse('ERROR', JText::_('JLIB_ENVIRONMENT_SESSION_EXPIRED'));
			}
			else
			{
				self::ajaxResponse('ERROR', JText::_('Token invalid'));
			}
		}
		else
		{
			return true;
		}
	}
	public static function getToken()
	{
		$token = self::oseGetToken();
		$html = '<input type="hidden" value="1" name="'.$token.'">';
		return $html;
	}
	public function getRealIP()
	{
		$ip = false;
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		{
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ips = explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
			if ($ip)
			{
				array_unshift($ips, $ip);
				$ip = false;
			}
			for ($i = 0; $i < count($ips); $i++)
			{
				if (!preg_match("/^(10|172\.16|192\.168)\./i", $ips[$i]))
				{
					if (version_compare(phpversion(), "5.0.0", ">="))
					{
						if (ip2long($ips[$i]) != false)
						{
							$ip = $ips[$i];
							break;
						}
					}
					else
					{
						if (ip2long($ips[$i]) != - 1)
						{
							$ip = $ips[$i];
							break;
						}
					}
				}
			}
		}
		return ($ip ? $ip : $_SERVER['REMOTE_ADDR']);
	}
	public static function getPreviewMenus()
	{
		$token = self::oseGetToken();
		$logoutLink = JRoute::_('index.php?option=com_login&task=logout&'.$token.'=1');
		$hideLinks = JRequest::getBool('hidemainmenu');
		$output = '<div id="preview_menus">';
		$view = JRequest::getVar('view');
		if ($view =='scan')
		{
			$output .= '<span class="backtojoomla"><a href="#" onClick="resetDatabase()" >'.JText::_('Reset Database').'</a></span>';
		}
		if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_antihacker'.DS.'define.php'))
		{
			$output .= '<span class="backtojoomla"><a href="'.JURI::root().'administrator/index.php?option=com_ose_antihacker" >'.JText::_('OSE_ANTI_HACKER').'</a></span>';
		}
		$output .= '<span class="backtojoomla"><a href="'.JURI::root().'administrator/" >'.JText::_('BACK_TO_JOOMLA').'</a></span>';
		// Print the logout link.
		$output .= '<span class="logout">'.($hideLinks ? '' : '<a href="'.$logoutLink.'">').JText::_('JLOGOUT').($hideLinks ? '' : '</a>').'</span>';
		// Output the items.
		$output .= "</div>";
		return $output;
	}
	function TimerCheck()
	{
		if (!isset($_SESSION['targettime']))
		{
			$_SESSION['targettime'] = new DateTime('+ 10seconds');
		}
		echo $_SESSION['targettime']->format('Y-m-d H:i:s');
		$now = new DateTime;
		echo $now->format('Y-m-d H:i:s');
		$diff = $_SESSION['targettime']->diff($now);
		if ($_SESSION['targettime'] > $now)
		{
			echo $diff->format('%s seconds to go').'<br />';
			sleep(5);
			self::TimerCheck();
		}
		else
		{
			unset($_SESSION['targettime']);
			return true;
		}
	}
	public static function getDBFields($table)
	{
		$db = JFactory::getDBO();
		if (JOOMLA30)
		{
			$fields = $db->getTableColumns($table);
			$fields[$table] = $fields;
		}
		else
		{
			$fields = $db->getTableFields($table);
		}
		return $fields;
	}
	public static function randStr($length = 32, $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890')
	{
		// Length of character list
		$chars_length = (strlen($chars) - 1);
		// Start our string
		$string = $chars{rand(0, $chars_length)};
		// Generate random string
		for ($i = 1; $i < $length; $i = strlen($string))
		{
			// Grab a random character from our list
			$r = $chars{rand(0, $chars_length)};
			// Make sure the same two characters don't appear next to each other
			if ($r != $string{$i - 1})
					$string .= $r;
		}
			// Return the string
		return $string;
	}
	private function getJEDVersion () {
		if (!function_exists('allow_url_fopen') && !function_exists('ini_set'))
		{
			return 'N/A';
		}
		else if (!function_exists('allow_url_fopen') && function_exists('ini_set'))
		{
			ini_set('allow_url_fopen', 'on');
		}
		if ($this->isVersionChecked () == false)
		{
			$tmp = $this->getJEDVersionFromWeb ();
			$v = $this->getVersionChecked (); 
			if (empty($v))
			{
				$this->insertVersionChecked ($tmp[0]);
			}
			return $tmp[0];
		}
		else 
		{
			$version = $this->getVersionChecked ();
			return $version; 
		}
	}
	private function getJEDVersionFromWeb () {
		$url = "http://extensions.joomla.org/extensions/access-a-security/site-security/site-protection/8385";
		$jedContent = file_get_contents($url);
		$matches = array();
		$tmp = array(); 
		preg_match("/\<div\s*class=\"caption\"\>Version\<\/div\>\<div\s*class\=\"data\"\>\d\.\d\.\d\s*.*?\<\/div\>/", $jedContent, $matches);
		preg_match("/\d\.\d\.\d/", $matches[0], $tmp);
		return  $tmp; 
	}
	public function showLogoDiv() {
		$version =  $this -> getJEDVersion () ; 
		$header = '<div name="ose-social" id="ose-social">
					<section class="ose-page">
						<div class="social">
							<a href="https://www.opensource-excellence.com/customers/member-support-request.html" target="__blank">Support Center</a>
						</div>
						<div class="social">
							<a href="http://www.facebook.com/osexcellence" target="__blank">Like us</a>
						</div>
						<div class="social">
							<a href="http://extensions.joomla.org/extensions/access-a-security/site-security/site-protection/8385" target="__blank">Vote us in JED</a>
						</div>
						<div class="social">
							Latest Stable Version: ';
		 $header .=$version; 
		 $header .='</div>
					</section>
			</div>
			<div class="logo-labels">
			<h1>
				<a href="http://www.opensource-excellence.com" target="_blank">'.JText::_("Open Source Excellence").'</a>
			</h1>'.	self::getPreviewMenus().
		'</div>';
		echo $header; 
	}
	private function getTimeZone () {
		if (function_exists('ini_get'))
		{
			$timezone = ini_get('date.timezone');
			if (empty($timezone))
			{
				$this->setTimeZone ();  
			}
			else
			{
				$timezone = date_default_timezone_get();
				$this->setTimeZone ($timezone); 
			}
		}
	}
	private function setTimeZone ($timezone=null) {
		if (function_exists('ini_set') && empty($timezone))
		{
			ini_set('date.timezone', $this->timezone);
		}	
		else
		{
			$this->timezone = $timezone;
		}
	}
	private function isVersionChecked () {
		$db = JFactory::getDBO(); 
		$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` = 'version_checkedavs' ";
		$db->setQuery($query); 
		$date = $db->loadResult();
		if (empty($date)) {
			return false;
		}
		else
		{
			if (!class_exists('DateTimeZone'))
			{
				return true;
			}
			else
			{
				$this->getTimeZone ();
				$tz_object = new DateTimeZone($this->timezone);
				$datetime1 = new DateTime($date, $tz_object);
				$datetime2 = new DateTime("now", $tz_object);
				$interval = round(($datetime2->format('U') - $datetime1->format('U')) / (60*60*24)); 
				if ($interval >=1)
				{
					return false;
				}
				else 
				{
					return true; 
				}
			}
		}
	}
	private function getVersionChecked () {
		$db = JFactory::getDBO(); 
		$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` = 'version_oseavs' ";
		$db->setQuery($query); 
		return $db->loadResult(); 
	}
	private function insertVersionChecked ($version) {
		$db = JFactory::getDBO(); 
		$query = "INSERT INTO `#__ose_secConfig` 
				  (`id`, `key`, `value`, `type`, `default`) VALUES
				  (NULL, 'version_checkedavs', '".date("Y-m-d")."', 'global', '');";
		$db->setQuery($query); 
		$db->query();

		$query = "INSERT INTO `#__ose_secConfig` 
				  (`id`, `key`, `value`, `type`, `default`) VALUES
				  (NULL, 'version_oseavs', ". $db->Quote($version).", 'global', '');";
		$db->setQuery($query); 
		$db->query();
	}
}
?>
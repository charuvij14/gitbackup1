<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die;
/**
 * Content component helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since		1.6
 */
class OseantivirusHelper {
	public static $extension= 'com_osemsc';
	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	$vName	The name of the active view.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public static function addSubmenu($vName) {
		$db= & JFactory :: getDBO();
		jimport('joomla.version');
		$version= new JVersion();
		$version= substr($version->getShortVersion(), 0, 3);
		if($version >= '1.6') {
			$query= "SELECT * FROM `#__menu` WHERE `alias` =  'OSE Anti-Virus™'";
			$db->setQuery($query);
			$results= $db->loadResult();
			if(empty($results)) {
				$query= "UPDATE `#__menu` SET `alias` =  'OSE Anti-Virus™', `path` =  'OSE Anti-Virus™', `published`=1, `img` = '\"components/com_ose_antivirus/favicon.ico\"'  WHERE `component_id` = ( SELECT extension_id FROM `#__extensions` WHERE element ='com_ose_antivirus' ) ";
				$db->setQuery($query);
				$db->query();
			}
		}
		$vName= JRequest :: getCmd('view');
		JSubMenuHelper :: addEntry(JText :: _('Virus Scanning'), 'index.php?option=com_ose_antivirus', $vName == '');
		JSubMenuHelper :: addEntry(JText :: _('Virus Scanning Reports'), 'index.php?option=com_ose_antivirus&view=report', $vName == 'report');
		JSubMenuHelper :: addEntry(JText :: _('Whitelisted Files'), 'index.php?option=com_ose_antivirus&view=whitelist', $vName == 'whitelist');
		JSubMenuHelper :: addEntry(JText :: _('Quarantined Files'), 'index.php?option=com_ose_antivirus&view=quarantine', $vName == 'quarantine');
		/*
		JSubMenuHelper::addEntry(
			JText::_('Addon - ClamAV'),
			'index.php?option=com_ose_antivirus&view=clamav',
			$vName == 'clamav'
		);
		*/
	}
}
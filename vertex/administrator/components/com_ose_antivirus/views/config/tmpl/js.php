<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die("Direct Access Not Allowed");
?>
<script type="text/javascript" >
Ext.ns('oseATH','oseAVSConfig');
oseAVSConfig.Option = oseGetYesORNoOptions();
oseAVSConfig.enable_clamav = oseGetCombo('enable_clamav', 'ENABLE_CLAMAV_SCANNING', oseAVSConfig.Option, 450, 300, 100, 0);

oseAVSConfig.ClamActivationOption = new Array(new Array('socket', 'Use Socket'), new Array('tcpip', 'Use TCP IP'));
oseAVSConfig.ClamActivation = oseGetCombo('clamav_activation', 'CLAMAV_ACTIVATION_METHOD', oseAVSConfig.ClamActivationOption, 450, 300, 100, 'socket');

Ext.onReady(function(){
	oseAVSConfig.Form = new Ext.FormPanel({
        ref: 'form',
        id: 'oseAVSConfig_Form',
        labelAlign: 'top',
        frame:false,
        bodyStyle:'padding:10px',
        autoScroll: true,
        width: '100%',
        items: [
            {
				html:'<div class="warning">'+Joomla.JText._('SCANNED_FILE_EXTENSIONS_DESC')+'</div>',
				border: false,
            }
            ,        
			{   xtype:'textarea',
				labelWidth: 300,
				fieldLabel: Joomla.JText._('SCANNED_FILE_EXTENSIONS'),
			    name: 'file_ext',
			    id: 'file_ext',
			    anchor:'70%',
			    value: 'htm,html,shtm,shtml,css,js,php,php3,php4,php5,inc,phtml,jpg,jpeg,gif,png,bmp,c,sh,pl,perl,cgi,txt'
			},
			{   xtype:'textfield',
				labelWidth: 300,
				fieldLabel: Joomla.JText._('DO_NOT_SCAN_BIGGER_THAN'),
                name: 'maxfilesize',
                id: 'maxfilesize',
                anchor:'30%',
                regex: /[0-9]/i,
                value: 2
            },
            {
				html:'<div class="warning">'+Joomla.JText._('CLAMAV_DESC')+'</div>',
				border: false,
            },            
            oseAVSConfig.enable_clamav,
            oseAVSConfig.ClamActivation
            ,{
                xtype:'textfield',
                labelWidth: 300,
                fieldLabel: Joomla.JText._('CLAMAV_SOCKET_LOCATION'),
                name: 'clamavsocket',
                id: 'clamavsocket',
                anchor:'70%',
                value: "unix:///tmp/clamd.socket"    
            }
            ,{
                xtype:'textfield',
                labelWidth: 300,
                fieldLabel: 'ClamAV TCP IP Address',
                name: 'clamavtcpip',
                id: 'clamavtcpip',
                anchor:'70%',
                value: "127.0.0.1"    
            }
            ,{
                xtype:'textfield',
                labelWidth: 300,
                fieldLabel: 'ClamAV TCP IP Port',
                name: 'clamavtcpport',
                id: 'clamavtcpport',
                anchor:'70%',
                value: "3310"    
            }
		],
        buttons: [{
            text: 'Save',
            handler: function (){
        	oseAVSConfig.Form.getForm().submit({
							url : 'index.php' ,
							params : {
								option : 'com_ose_antivirus',
								controller:'scan',
								task:'saveConfiguration',
								type:'avscan'
							},
							method: 'POST',
							success: function ( form,action ) {
								msg = action.result;
								if (msg.status!='ERROR')
								{
									Ext.Msg.alert('Done', msg.msg);
								}
								else
								{
									Ext.Msg.alert('Error', msg.msg);
									top.render();
								}
							}
				});
            }
        }],
	  	listeners: {
			render: function(p){
				p.getForm().load({
					url : 'index.php' ,
					params : {
						option : 'com_ose_antivirus',
						controller:'scan',
						task:'getConfiguration'
					},
					method: 'POST',
					success: function (form, action ) {
						
					}
				});
			}
		}
    });

	oseAVSConfig.panel = new Ext.Panel({
		id: 'oseAVConfig-panel'
		,border: false
		,layout: 'fit'
		,items:[
			oseAVSConfig.Form	
		]
		,height: 450
		,width: '100%'
		,renderTo: 'vsconf'
	});
})
</script>
<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
$fileScan= oseRegistry :: call('filescan');
$oseFile= $fileScan->getFile();
$defVirus['type1']= $oseFile->getDefsFromXML(OSEREGISTER_DEFAULT_PATH.DS.'virusscan'.DS.'libraries'.DS.'assets'.DS.'virusDefType1.xml');
$defVirus['type2']= $oseFile->getDefsFromXML(OSEREGISTER_DEFAULT_PATH.DS.'virusscan'.DS.'libraries'.DS.'assets'.DS.'virusDefType2.xml');
?>

<script type="text/javascript">

Ext.onReady(function(){
    var vsrtoggle = Ext.get("vsrtoggle");
    vsrtoggle.on('click', function(){
 		var c = vsrtoggle.dom.checked;
		var n2 = 0;
		n=Ext.get('totalel').dom.value;
		for (i=0; i < n; i++) {
		Ext.get('cb'+i).dom.checked =c;
		n2++;
		}
    });
    Ext.get('vsrnextstart').dom.value=<?php echo $this->next_start?>;
    Ext.get('vsrprevstart').dom.value=<?php echo $this->prev_start?>;
});
</script>
<table width="100%">
	<input type="hidden" id='totalel'
		value='<?php echo count($this->items); ?>' />
	<thead>
		<tr>
			<th width="5%" class="title"><input type="checkbox" id="vsrtoggle"
				value="" />
			</th>
			<th width="5%" class="title">
			<?php echo JText::_( 'ID' ); ?>
			</th>
			<th width="40%" class="title" nowrap="nowrap">
			<?php echo JText::_('File'); ?>
			</th>
			<th width="50%" class="title" nowrap="nowrap">
			<?php echo JText::_('Virus Definition'); ?>
			</th>
		</tr>
	</thead>
	<tbody>

	<?php
		$k= 0;
		for($i= 0, $n= count($this->items); $i < $n; $i++) {
		$row= & $this->items[$i];
		$infection= explode(",", $row->virus);
		if(isset($defVirus[$infection[0]][$infection[1]]['description'])) {
			$row->virus= $defVirus[$infection[0]][$infection[1]]['description'];
		}
		?>
		<tr class="<?php echo "row$k"; ?>">
			<td valign="top">
			<?php echo 	'<input type="checkbox" value="'.$row->id.'" name="cid[]" id="cb'.$i.'"/>'; ?>
			</td>
			<td valign="top">
			<?php echo $row->id; ?>
			</td>

			<td valign="top">
			<?php echo $row->filepath; ?>
			</td>
			<td valign="top">
			<?php

			if(strstr($row->virus, "Shell Codes") || strstr($row->virus, "Shell Code")) {
				echo "<p class='report_warning'><b>[ Risk: HIGH ]</b> ".$row->virus." ".JText :: _(" [Recommended to be reviewed and deleted]")."</p>";
			}
			elseif(strstr($row->virus, "Suspicious")) {
				echo "<p class='report_low'><b>[ Risk: LOW ]</b> ".$row->virus." ".JText :: _(" [Possible false alert. Please review this file.]")."</p>";
			}
			elseif(strpos("virusscan/libraries", $row->filepath) || strpos("administrator/components/com_scan/", $row->filepath) || strpos("administrator/components/com_ose_cpu/virusscan/", $row->filepath) || strpos("com_scan/admin", $row->filepath)) {
				echo "<p class='report_low'><b>[ Risk: LOW ]</b> ".JText :: _(" This is possibly OSE Virus Definition File, Please review it and ignore it if the file is clean.")."</p>";
			} else {
				echo "<p class='report_medium'><b>[ Risk: MEDIUM ]</b> ".$row->virus."</p>";
			}
			?>
			</td>

		</tr>
		<?php 
			$k= 1 - $k;
		}
		?>
		</tbody>
</table>

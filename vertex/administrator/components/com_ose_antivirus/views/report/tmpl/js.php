<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
defined('_JEXEC') or die(';)');
?>
<script type="text/javascript" >
function oseGetAjaxSuccessWin (msg, win)
{
	if (msg.status!='ERROR')
	{
		win.show();
		win.update(msg.results);
	}
	else
	{
		Ext.Msg.alert(msg.status, msg.result);
	}
}

function viewFiledetail(id)
{
	var Viewwin = new Ext.Window({
		id: 'viewFile',
		name: 'viewFile',
		title: 'viewFile',
        width: 1024,
        height: 500,
        closeAction:'destroy',
        autoScroll:'true'
	}); 
	
	
	Ext.Ajax.request({
		url : 'index.php' ,
		params : {
			option : 'com_ose_antivirus',
			controller: 'report',
			task: 'viewfile' ,
			id: id
		},
		method: 'POST',
		success: function ( response, options ) {
			oseGetAjaxSuccessWin (Ext.decode(response.responseText), win);
			msg = Ext.decode(response.responseText); 
			var editor = CodeMirror.fromTextArea(Ext.get('codearea').dom, {
			    mode: msg.fileType,
			    lineNumbers: true,
			    readOnly: true
			});
		}
	});
}

	var win = new Ext.Window({
		id:'fileContent',
        layout:'fit',
        title: 'File Content',
        width:800,
        height:500,
        closeAction:'hide',
        collapsible:'true',
        autoScroll:'true'
	})

Ext.onReady(function(){
	Ext.ns('oseAV','oseAVinfectedFiles');
	oseAVinfectedFiles.fields = new Array('id', 'filepath', 'virus', 'view');
	oseAVinfectedFiles.store = oseGetStore('filesStore', oseAVinfectedFiles.fields, 'index.php', 'com_ose_antivirus', 'report', 'getInfected'); 
	oseAV.oseAVInfectedFiles = new Ext.grid.GridPanel({
		id: 'oseAVinfectedFiles'
		,cm: oseAVinfectedFiles.cm
		,store: oseAVinfectedFiles.store
		,selType: 'rowmodel'
		,viewConfig: {forceFit: true}
		,height: 500
		,width: 500
		,region: 'west'
		,multiSelect: true
		,margins: {top:5, right:5, bottom:5, left:3}
		,columns: [
		        	new Ext.grid.RowNumberer({header:'#'})
		            ,{id: 'id', header: Joomla.JText._('ID'),  hidden:false, dataIndex: 'id', width: 50}
		            ,{id: 'virus', header: Joomla.JText._('Virus_Rule'),  hidden:false, dataIndex: 'virus', sortable: true, width:350}
		            ,{id: 'filepath', header: Joomla.JText._('File_path'),  hidden:false, dataIndex: 'filepath', width:750}
		            ,{id: 'view', header: Joomla.JText._('Action'),  hidden:false, dataIndex: 'view', width: 50}
		        ]
		,tbar: new Ext.Toolbar({
			items:[
			{
				text:Joomla.JText._('Backup_Items'),
				handler: function(){
					Ext.Msg.confirm(Joomla.JText._('Backup_confirmation'), Joomla.JText._('Please_confirm_that_you_would_like_to_backup_the_selected_files'), function(btn, text){
					      if (btn == 'yes'){
								var sel = oseAV.oseAVInfectedFiles.getSelectionModel();
								var selections = sel.selected.items;
								vsAjax('com_ose_antivirus','vsbackup','scan', selections);
					      }
				     });
				}
			},
			{
				text:Joomla.JText._('Whitelist_Items'),
				handler: function(){
					Ext.Msg.confirm(Joomla.JText._('Whitelist_confirmation'), Joomla.JText._('Please_confirm_that_you_would_like_to_whitelist_the_selected_files'), function(btn, text){
					      if (btn == 'yes'){
								var sel = oseAV.oseAVInfectedFiles.getSelectionModel();
								var selections = sel.selected.items;
								vsAjax('com_ose_antivirus','vswhitelist','scan', selections);
					      }
				     });
				}
			},
			{
				text:Joomla.JText._('Clean_Infected_Items'),
				handler: function(){
					Ext.Msg.confirm(Joomla.JText._('Virus_Cleaning_confirmation'), Joomla.JText._('Please_confirm_that_you_would_like_to_clean_the_selected_files'), function(btn, text){
					      if (btn == 'yes'){
								var sel = oseAV.oseAVInfectedFiles.getSelectionModel();
								var selections = sel.selected.items;
								vsAjax('com_ose_antivirus','vsclean','scan', selections);
					      }
				     });
				}
			},
			{
				text:Joomla.JText._('Quarantine_Infected_Items'),
				handler: function(){
					Ext.Msg.confirm(Joomla.JText._('Quarantining_confirmation'), Joomla.JText._('Please_confirm_that_you_would_like_to_quarantine_the_selected_files'), function(btn, text){
					      if (btn == 'yes'){
								var sel = oseAV.oseAVInfectedFiles.getSelectionModel();
								var selections = sel.selected.items;
								vsAjax('com_ose_antivirus','vsquarantine','scan', selections);
					      }
				     });
				}
			},
			{
				text:Joomla.JText._('Back_to_Virus_Scanner'),
				handler: function(){
					    window.location = "index.php?option=com_ose_antivirus";
				}
			}
		   ]
		})
		,bbar:new Ext.PagingToolbar({
    		pageSize: 100,
    		store: oseAVinfectedFiles.store,
    		displayInfo: true,
    		displayMsg: Joomla.JText._('Displaying_topics')+' {0} - {1} '+Joomla.JText._('of')+' {2}',
		    emptyMsg: Joomla.JText._("No_topics_to_display")

	    })
       });

		oseAV.oseAVInfectedFiles.panel = new Ext.Panel({
		id: 'oseAVInfectedFiles-panel'
		,border: false
		,layout: 'fit'
		,items:[
			oseAV.oseAVInfectedFiles
		]
		,height: 550
		,width: '100%'
		,renderTo: 'oseAVInfectedFiles'
	});


  function vsAjax(option, task, controller,selections)
  {
	var i=0;
    filesids=new Array();
	for (i=0; i < selections.length; i++)
	{
        filesids [i] = selections[i].data.id;
	}
	filesids = Ext.encode(filesids);
	// Ajax post scanning request;
	Ext.Ajax.request({
				url : 'index.php' ,
				params : {
					option : option,
					task:task,
					controller:controller,
					filesids: filesids
				},
				method: 'POST',
				success: function ( result, request ) {
					msg = Ext.decode(result.responseText);
					if (msg.status!='ERROR')
					{
						Ext.Msg.alert(msg.status, Joomla.JText._('The_action')+task+Joomla.JText._('was_executed_successfully'));
						oseAVinfectedFiles.store.reload();
					}
					else
					{
						Ext.Msg.alert(Joomla.JText._('Error'), msg.result);
						oseAVinfectedFiles.store.reload();
					}
				}
			});
	}
});
</script>
<?php
/**
 *   * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.view');
/**
 * HTML View class for the scan Component
 *
 * @package    scan
 * @subpackage Views
 */
class ose_antivirusViewreport extends ose_antivirusView
{
	/**
	 * scans view display method
	 * @return void
	 **/
	function display($tpl = null)
	{
		$tmpl = JRequest::getVar('tmpl');
		if (empty($tmpl))
		{
			JRequest::setVar('tmpl', 'component');
		}
		$this->setEnvironment();
		$this->initScript();
		$this->loadJsFile();
		$this->loadCodeMirror ();
		$title = JText::_('OSE Anti-Virus™ Virus Scanning Report');
		$this->assignRef('title', $title);
		parent::display($tpl);
	}
	private function loadCodeMirror()
	{
		$this->oseCPU->script('components/com_ose_cpu/codemirror/lib/codemirror.js');
		$this->oseCPU->script('components/com_ose_cpu/codemirror/lib/util/foldcode.js');
		$this->oseCPU->script('components/com_ose_cpu/codemirror/mode/javascript/javascript.js');
		$this->oseCPU->script('components/com_ose_cpu/codemirror/mode/css/css.js');
		$this->oseCPU->script('components/com_ose_cpu/codemirror/mode/xml/xml.js');
		$this->oseCPU->script('components/com_ose_cpu/codemirror/mode/clike/clike.js');
		$this->oseCPU->script('components/com_ose_cpu/codemirror/mode/htmlmixed/htmlmixed.js');
		$this->oseCPU->script('components/com_ose_cpu/codemirror//mode/php/php.js');
		$this->oseCPU->script('components/com_ose_cpu/codemirror//mode/javascript/javascript.js');
		$this->oseCPU->stylesheet('components/com_ose_cpu/codemirror/lib/codemirror.css', '1.6');
	}
	private function loadJsFile()
	{
		include(JPATH_ADMINISTRATOR.DS.'components/com_ose_antivirus/views/report/tmpl/js.php');
	}
}
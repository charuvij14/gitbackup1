<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die("Direct Access Not Allowed");
jimport('joomla.application.component.view');
class ose_antivirusView extends JViewLegacy
{
	public function display($tpl = null)
	{
		$this->initCss();
		$this->loadSoftHelper();
		$document = JFactory::getDocument();
		$document->addScript(JURI::root().'media/system/js/core.js');
		parent :: display($tpl);
	}
	public function initCss()
	{
		$this->com_name= OSESoftHelper :: getExtensionName();
		$this->oseCPU -> stylesheet('administrator/components/com_ose_cpu/assets/css/backendv6.css');
		$this->oseCPU -> stylesheet('components/com_ose_cpu/ext4-ose/resources/ext-theme-neptune-all-debug.css');
		$this->oseCPU -> stylesheet('administrator/components/'.$this->com_name.'/assets/css/style.css');
	}
	public function initScript($type= 'extjs')
	{
		switch($type)
		{
			case('extjs') :
			default :
				$this->oseCPU->script('components/com_ose_cpu/ext4-ose/ext-all.js');
				$this->oseCPU->script('components/com_ose_cpu/ext4-ose/ext-theme-neptune.js');
				$this->oseCPU->script('components/com_ose_cpu/ext4-ose/resources/ux/SlidingPager.js');
				$this->oseCPU->script('components/com_ose_cpu/ext4-ose/resources/ux/SearchField.js');
				$this->oseCPU->script('components/com_ose_cpu/ext4-ose/resources/ux/MultiSelect.js');
				$this->oseCPU->script('components/com_ose_cpu/ext4-ose/resources/ux/ItemSelector.js');
				$this->oseCPU->script('components/com_ose_cpu/ext4-ose/resources/ose/elements.js');
				$this->oseCPU->script('components/com_ose_cpu/ext4-ose/resources/ose/functions.js');
				break;
		}
	}
	public function setEnvironment()
	{
		$this->oseCPU = new oseCPU();
		$this->option = 'com_ose_antivirus';
		$this->ajaxURL = 'index.php?option='.$this->option;
		$this->footer= OSESoftHelper :: renderOSETM();
		$this->preview_menus= OSESoftHelper :: getPreviewMenus();
		$this->menus= OSESoftHelper :: getMenu();
	}
	public function loadSoftHelper()
	{
		$OSESoftHelper = new OSESoftHelper();
		$footer = $OSESoftHelper->renderOSETM();
		$this->assignRef('OSESoftHelper', $OSESoftHelper);
		$this->assignRef('footer', $footer);
	}
	public function showJsHeader($controller)
	{
		$js = '<script type="text/javascript" >
				var url = "'. $this->ajaxURL.'"; 
				var option = "'.$this->option.'";
				var controller = "'.$controller.'";  
				</script>';
		echo $js; 	
	} 
}
?>
<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence AntiVirus
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.view');
/**
 * HTML View class for the scan Component
 *
 * @package    scan
 * @subpackage Views
 */
class ose_antivirusViewscan extends ose_antivirusView
{
	var $controller = 'scan';  
	/**
	 * scans view display method
	 * @return void
	 **/
	public function display($tpl = null)
	{
		$tmpl = JRequest::getVar('tmpl');
		if (empty($tmpl))
		{
			JRequest::setVar('tmpl', 'component');
		}
		$this->setEnvironment();
		$this->initScript();
		$this->loadJsFile();
		$this->setVariables ();
		$this->showJsHeader($this->controller);
		$ready = $this->checkDatabase ();
		if ($ready == false)
		{
			$this->showJsInstall();
		}  
		$title = JText::_('OSEAV_SCAN_REMOVAL');
		$this->assignRef('title', $title);
		parent::display($tpl);
	}
	private function loadJsFile()
	{
		include(JPATH_ADMINISTRATOR.DS.'components/com_ose_antivirus/views/scan/tmpl/js.php');
	}
	private function setVariables()
	{
		$model = $this->getModel("scan");
		$vstotal = $model->getTotal();
		$LastScanLog = $model->getLastScanLog();
		$exts = $model->getScanExt();
		$token = $model->getToken();
		$scanFiles = $model->getScanFiles () ; 
		$totalFiles = $model->getTotalFiles ();
		$this->assignRef('vstotal', $vstotal);
		$this->assignRef('LastScanLog', $LastScanLog);
		$this->assignRef('exts', $exts);
		$this->assignRef('token', $token);
		$this->assignRef('scanFiles', $scanFiles);
		$this->assignRef('totalFiles', $totalFiles);
		
	}
	private function checkDatabase () {
		$db= JFactory::getDBO();
		//$query = "SHOW TABLE STATUS LIKE '%oseav_scanitems' ";
		$query = "SHOW TABLE STATUS LIKE '%".$db->getPrefix()."oseav_scanitems' ";
		$db->setQuery($query);
		$result = $db->loadObjectList();
		if (COUNT($result)==0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	private function showJsInstall()
	{
		$js = '<script type="text/javascript" >
				window.onload= resetDatabaseAjax() ;  
			  </script>';
		echo $js; 	
	} 
}

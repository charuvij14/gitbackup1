<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
defined('_JEXEC') or die("Direct Access Not Allowed");
?>
<div id = "oseheader">
  <div class="container">
	<?php
		$this->OSESoftHelper->showLogoDiv();
		echo $this->menus; 
	?>
	<div class="section">
		<div id="sectionheader">
			<?php echo $this->title; ?>
			<div class="span-text">
				<?php
					if (!empty($this->vstotal)) {
						if (!empty($this->exts)) {
							echo JText::_("THERE_ARE_IN_TOTAL_OF") . " " . $this->vstotal . " " . JText::_("FILES_IN_DATABASE") ;
						}
						else {
							echo JText::_('SELECT_FILE_EXTENSION_BEFORE_SCAN');
						}
					}
					else {
						echo JText::_('PLEASE_PERFORM_DB_INITIALIZATION');
					}
				?>
			</div>
		</div>
		
		<div id = "scansetting">
			<input type="hidden" id='vstotal' value = '<?php echo $this->vstotal; ?>'/>
			<div class='center-content' id='vscp'>
					<!-- Action Panel -->
					<div class='action_panel'>
					   		<?php echo JText::_('Report'); ?>
					</div>
					<div class='stat_panel'>
						<div class='stat_heading'>
							<?php 
								echo JText::_("LAST_SCAN_TIME"). ' <div id="lstime">'.$this->LastScanLog['lstime']."</div>"; 
							?>
						</div>
						<div class='stat'>
							<?php  
								echo JText::_("NUM_CLEAN_FILES") .' <div id="lsclean">'. $this->LastScanLog['lsclean']."</div>";
							?>
						</div>
						<div class='stat'>
							<?php 
							 	echo JText::_("NUM_INFECT_FILES") . ' <div id="lsinfected">'. $this->LastScanLog['lsinfected'] ."</div>";
						 	?>
						</div>
					</div>
					<div class='action_panel'>
					   <?php 
					   	echo JText::_("CUR_SCAN_SETTING");
					   ?>
					</div>
					<div class='stat_panel'>
						<div class='stat_heading'>
							<?php 
								echo JText::_("CUR_FILE_EXT"); 
							?>
						</div>
						<div class='stat'>
							<?php 
							 	if (!empty($this->exts)) {
									$exts = implode(",", $this->exts);
									echo "<div id ='cur_fileexts'>" . $exts . '</div>';
								}
						 	?>
						</div>
						<div class='stat_heading'>
							<?php 
								echo JText::_('FOLDERS_BEING_SCANNED'); 
							?>
						</div>
						<div class='stat'>
							<?php 
							 	echo $this->totalFiles;
							 	echo '<input type="hidden" name = "totalFiles" id = "totalFiles" value = "'.$this->totalFiles.'"/>'; 
						 	?>
						</div>
						
					</div>
			</div>
			
			<div id='virusscanning'>
				<div id='button-bar'>
					<div class='button-bar-button'>						 
							  <button id="init" class='button'><?php echo JText::_('OSE_AV_STEP2') ?></button>
							  <button id="startvscan" class='button'><?php echo JText::_('OSE_AV_SCAN_VIRUS') ?></button>
							  <button id="stopvscan" class='button'><?php echo JText::_('OSE_AV_STOP') ?></button>
							  <button id="scanresults" class='button'><?php echo JText::_('OSE_AV_SCANRESULT') ?></button>
					</div>
				</div>
				<div id='progress-bar'></div> 
				<div id='scanningStatus'>
					<table >
						<tr>
							<td>Progress: </td><td><div id ="progressText">N/A</div></td>
						</tr>
						<tr>
							<td>Threats found: </td><td><div id ="threatsFound">N/A</div></td>
						</tr>
						<tr>
							<td>System load: </td><td><div id ="systemLoad">N/A</div></td>
						</tr>
						<tr>
							<td>Scanned Objects: </td><td><div id ="scannedObjects">N/A</div></td>
						</tr>
					</table>
				</div>
			</div>
		</div>		
	</div>
  </div>
</div>
<?php
	echo $this->footer;
?>						
<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');
?>
<script type="text/javascript" >
Ext.ns('oseATH','oseATHScanner');
<?php 
echo 'var scanPath = "'.JPATH_SITE.'";';
?>
function resetDatabaseAjax() {
	Mask= new Ext.LoadMask(Ext.getBody(), {msg:'Please wait...'});
	Mask.show();
	Ext.Ajax.request({
		url : url ,
		params : {
			option : option,
			task: 'resetDatabase',
			controller:controller
		},
		method: 'POST',
		success: function (response, options)
		{
			Mask.hide();
			var message = Ext.decode(response.responseText);
			Ext.Msg.show({title: message.status, msg: message.result});
		}
	});
}

function resetDatabase () {
	Ext.Msg.confirm('Confirmation','Please confirm that you would like to reinstall the database', function(btn, text){
		if (btn == 'yes'){
			resetDatabaseAjax();
	    }
     });
}

function Countdown(options) {
	  var timer,
	  instance = this,
	  seconds = options.seconds || 10,
	  updateStatus = options.onUpdateStatus || function () {},
	  counterEnd = options.onCounterEnd || function () {};

	  function decrementCounter() {
	    updateStatus(seconds);
	    if (seconds === 0) {
	      counterEnd();
	      instance.stop();
	    }
	    seconds--;
	  }

	  this.start = function () {
	    clearInterval(timer);
	    timer = 0;
	    seconds = options.seconds;
	    timer = setInterval(decrementCounter, 1000);
	  };

	  this.stop = function () {
	    clearInterval(timer);
	  };
}

function oseGetNormalTextField(name, fieldlabel, labelWidth, width)
{
	var textfield = {   
			xtype:'textfield',
	        fieldLabel: fieldlabel,
	        name: name,
	        id: name,
	        labelWidth: labelWidth,
	        width: width
    }
	return textfield; 
}
function initDBButtonUpdate (status) {
	Ext.getCmp('path').setDisabled(status);
	Ext.getCmp('initdbbutton').setDisabled(status);
}
function initDatabase (step, win, task, counter) {
	Ext.Ajax.request({
		url : 'index.php',
		params : {
			option : 'com_ose_antivirus',
			controller: 'scan',
			task: task,
			action: task,
			step : step,
			path : Ext.getCmp('path').getValue()
		},
		method: 'POST',
		success: function ( response, options ) {
			var msg  = Ext.decode(response.responseText);
			if (msg.status=='Completed')
			{
				win.update(msg.result);
				win.hide();
				location.reload(); 
			}
			else if (msg.status=='ERROR') {
				Ext.Msg.alert(msg.status, msg.result);
				initDBButtonUpdate (false);
			}
			else
			{
				oseATHScanner.pbar1.updateProgress(1, msg.summary);
				Ext.fly('scanned_files').update(msg.lastscanned);
				if (msg.cont > 0)
				{	
					if (counter < 20) 
					{
						counter ++;
						initDatabase (1, win, task, counter); 
					}
					else
					{
						counter = 0;
						Ext.MessageBox.show({ 
							msg: "Let's give the server a rest", 
							progressText: 'Wait...', 
							width:300, 
							wait:true
						}); 

						var myCounter = new Countdown({  
						    seconds:5,  // number of seconds to count down
						    onUpdateStatus: function(sec){
								Ext.MessageBox.updateProgress(0.2, sec + ' seconds left...',sec + ' seconds left...');
									
						    }, // callback for each second
						    onCounterEnd: function(){ 
						    	Ext.MessageBox.close(); 
						    	initDatabase (1, win, task, 0); 
	                        } // final action
						});
						
						myCounter.start(); 
					}	 
				}
				else
				{
					oseATHScanner.pbar1.updateProgress(0, 'Database Initialization Completed');
					initDBButtonUpdate (false);
				}	
			}
		}
	});	
}

function showWaitMsg (msg, progressText) {
	Ext.MessageBox.show({
		msg: msg,
		progressText: progressText,
		width:300,
		wait:true
	});	
}
Ext.onReady(function(){
	function oseGetProgressbar(id, text) 
	{
		var bar = new Ext.ProgressBar ({
		    id:id,
		    text:text
		});	
		return bar; 
	}
	oseATHScanner.pbar1 = oseGetProgressbar('pbar1', 'Database Initialisation Ready') ;
	oseATHScanner.initDBForm = new Ext.form.FormPanel ({
		bodyStyle: 'padding: 10px; padding-left: 20px'
		,autoScroll: true
		,autoWidth: true
	    ,border: false
	    ,labelAlign: 'left'
	    ,labelWidth: 150
	    ,buttons: [
	    {
			text: 'Initialise Database',
			id: 'initdbbutton'
			,handler: function(){
				oseATHScanner.pbar1.updateProgress(0, 'Database Initialization In Progress');
				initDBButtonUpdate (true); 
				initDatabase(-1, oseATHScanner.InitDBWin, 'initDatabase', 0); 
			}
		},
		{
			text: 'Continue',
			id: 'contdbbutton'
			,handler: function(){
				oseATHScanner.pbar1.updateProgress(0, 'Database Initialization In Progress');
				initDBButtonUpdate (true); 
				initDatabase(0, oseATHScanner.InitDBWin, 'initDatabase', 0); 
			}
		},
	    {
			text: 'Stop',
			id: 'stopdbbutton'
			,handler: function(){
				oseATHScanner.pbar1.updateProgress(0, 'Database Initialization Terminated');
				initDBButtonUpdate (false); 
				Ext.Ajax.abort(); 
			}
		},
		{
			text: 'Close',
			id: 'closebutton'
			,handler: function(){
				location.reload();  
			}
		}
		]
	    ,items:[
			oseGetNormalTextField('path', 'Scanning Path', 160, 580),
			{
				html: '<div id ="scanned_files">&nbsp;</div>'
			},
			oseATHScanner.pbar1
	    ]
	});
	
	oseATHScanner.InitDBWin = new Ext.Window({
		title: 'Database Initialization'
		,modal: true
		,width: 800
		,border: false
		,autoHeight: true
		,closeAction:'hide'
		,items: [
		      oseATHScanner.initDBForm
		]
	});	
	
    var init = Ext.get('init');
	function disablebuttons (status)
	{
		Ext.get('init').dom.disabled = status;
		Ext.get('scanresults').dom.disabled = status;
	}

	var totalFiles= Ext.get('totalFiles').getValue(); 
	if (totalFiles != undefined || totalFiles > 0)
	{
		var bar2text = 'There are in total of '+totalFiles+' files. Virus scan is ready.';
	}
	else
	{
		var bar2text = 'Please initialize database before scanning virus';
	}

	oseATHScanner.pbar2 = oseGetProgressbar('pbar2', bar2text	) ;
	oseATHScanner.pbar2.render ('progress-bar');
	
	function scanvirus(start_value, win, type, init, counter)
	{
		Ext.Ajax.request({
			url : 'index.php' ,
			params : {
				option : 'com_ose_antivirus',
				task:'vsscan',
				controller:'scan',
				start: start_value,
				type: type,
				init: init
			},
			method: 'POST',
			success: function ( result, request ) {
				Ext.MessageBox.close();
				var msg = Ext.decode(result.responseText);
				if (msg.status=='Done')
				{
					Ext.getCmp('pbar2').updateProgress(msg.progress, msg.progressText);
					Ext.getCmp('pbar2').updateText(Joomla.JText._('SCANNING_COMPLETED_TOTAL_CHECKED')+msg.total);
					disablebuttons (false);
				    win.hide();
					Ext.fly('lstime').dom.innerHTML=msg.lstime;
					Ext.fly('lsclean').dom.innerHTML=msg.lsclean;
					Ext.fly('lsinfected').dom.innerHTML=msg.lsinfected;
					if (msg.lsresults=='Virus found')
					{
						Ext.Msg.alert(Joomla.JText._('SCANNING_COMPLETED'),Joomla.JText._('TOTAL_VIRUS_FOUND')+msg.lsinfected);
					}
					else
					{
						Ext.Msg.alert(Joomla.JText._('SCANNING_COMPLETED'), Joomla.JText._('NO_VIURS_FOUND'));
					}
				}
				else
				{
					if (msg.cont == 1)
					{	
						Ext.fly('progressText').update(msg.progressText);
						Ext.fly('threatsFound').update(msg.threatsFound);
						Ext.fly('systemLoad').update(msg.systemLoad);
						Ext.fly('scannedObjects').update(msg.scannedObjects); 
						Ext.getCmp('pbar2').updateProgress(msg.progress, msg.progressText);
						if (counter < 20) 
						{
							counter ++;
							scanvirus(start_value, win, type, 0, counter); 
						}
						else
						{
							counter = 0;
							Ext.MessageBox.show({ 
								msg: "Let's give the server a rest", 
								progressText: 'Wait...', 
								width:300, 
								wait:true
							}); 

							var myCounter = new Countdown({  
							    seconds:5,  // number of seconds to count down
							    onUpdateStatus: function(sec){
									Ext.MessageBox.updateProgress(0.2, sec + ' seconds left...',sec + ' seconds left...');
										
							    }, // callback for each second
							    onCounterEnd: function(){ 
							    	Ext.MessageBox.close(); 
							    	scanvirus(start_value, win, type, 0, 0); 
		                        } // final action
							});
							
							myCounter.start(); 
						}	 

					      
					}
				}

			}
		});
	}

	oseATHScanner.form = Ext.create('Ext.form.Panel', {
		bodyStyle: 'padding: 15px;'
		,autoScroll: true
		,autoWidth: true
	    ,border: false
	    ,labelAlign: 'left'
	    ,buttons: [
	    {
			text: 'Scan'
			,handler: function(){
		    	Ext.getCmp('scanningresults').close() ;
		    	showWaitMsg ('Please wait', 'Please wait');
				var type = Ext.getCmp('virus_type').getValue(); 
		        scanvirus(0, win, type, 1, 0); 
			}
		},
		{
			text: 'Continue Previous Scan'
			,handler: function(){
		    	Ext.getCmp('scanningresults').close() ;
		    	showWaitMsg ('Please wait', 'Please wait');
				var type = Ext.getCmp('virus_type').getValue(); 
			    var start_value =0;
		        scanvirus(start_value, win, type, 0, 0); 
			}
		}
		]
	    ,items:[
    		{
			   	xtype:'combo'
				,fieldLabel:'Type of Virus Scan'
				,labelWidth: 250
				,hiddenName: 'virus_type'
				,id: 'virus_type'
				,name: 'virus_type'
				,typeAhead: true
				,triggerAction: 'all'
				,lazyRender:false
				,width: 500
				,mode: 'local'
				,value: 'general'
				,store: Ext.create('Ext.data.ArrayStore', {
					    fields: [
					       'value',
					       'text'
					    ],
					    data: [
					      	['general', 'General'],
					      	['htaccess', "htaccess Hacking"],
					      	['shellcodes', "Shell codes"],
					      	['base64', "PHP Injection (Base64 Codes)"],
					      	['custom', "Custom Virus Patterns"],
					      	['clamav', "ClamAV Anti-Virus"]
						]
					})
					,valueField: 'value'
					,displayField: 'text'
			}
	       ]
	});
	
	var win = new Ext.Window({
				bodyStyle: 'padding: 0px; padding-left: 0px',
				id:'scanningresults',
				title: 'Virus scanning window',
                layout:'fit',
                width:600,
                height:140,
                border: false,
                closeAction:'hide',
                collapsible:'true',
                autoScroll:'true',
                items: [
						oseATHScanner.form
                ]
	});
	
    init.on('click', function(){
    	oseATHScanner.InitDBWin.show();
    	Ext.getCmp('path').setValue(scanPath);
    });

    var startvscan = Ext.get('startvscan');
    startvscan.on('click',function() {
    	osevirusscan();
    });

    var stopvscan = Ext.get('stopvscan');
    stopvscan.on('click',function() {
    	disablebuttons (false);
    	Ext.Ajax.abort(); 
    });

    var scanresults = Ext.get('scanresults');
    scanresults.on('click',function() {
    	window.location = "index.php?option=com_ose_antivirus&view=report" 
    });
    
    function osevirusscan()
    {
    	Ext.getCmp('pbar2').updateText(Joomla.JText._('SCANNING_VIRUS'));
    	disablebuttons (true); 
	    win.show();
	    win.alignTo(Ext.getBody(), 't-c',  [+160, +10]);
    }
});
</script>
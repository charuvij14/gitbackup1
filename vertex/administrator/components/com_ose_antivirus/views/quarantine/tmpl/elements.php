<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
?>

<script type="text/javascript" >

Ext.onReady(function(){
    var vsrtoggle = Ext.get("vsrtoggle");
    vsrtoggle.on('click', function(){
 		var c = vsrtoggle.dom.checked;
		var n2 = 0;
		n=Ext.get('totalel').dom.value;
		for (i=0; i < n; i++) {
		Ext.get('cb'+i).dom.checked =c;
		n2++;
		}
    });
    Ext.get('vsrnextstart').dom.value=<?php echo $this->next_start?>;
    Ext.get('vsrprevstart').dom.value=<?php echo $this->prev_start?>;
});
</script>
<table width="100%">
<input type="hidden" id= 'totalel' value = '<?php echo count($this->items); ?>' />
		<thead>
			<tr>
				<th width="5%" class="title">
					<input type="checkbox" id="vsrtoggle" value=""  />
				</th>
				<th width="5%" class="title">
					<?php echo JText::_( 'ID' ); ?>
				</th>
				<th width="40%" class="title" nowrap="nowrap">
					<?php echo JText::_('File'); ?>
				</th>
				<th width="50%" class="title" nowrap="nowrap">
					<?php echo JText::_('Quarantine file name'); ?>
				</th>


			</tr>
		</thead>
		<tbody>
		<?php

$k= 0;
for($i= 0, $n= count($this->items); $i < $n; $i++) {
	$row= & $this->items[$i];
?>
			<tr class="<?php echo "row$k"; ?>">
				<td valign="top">
					<?php echo 	'<input type="checkbox" value="'.$row->id.'" name="cid[]" id="cb'.$i.'"/>'; ?>
				</td>
				<td valign="top">
					<?php echo $row->id; ?>
				</td>

				<td valign="top">
					<?php echo $row->filepath; ?>
				</td>
				<td valign="top">
					<?php

	echo $row->quar_name;
?>
				</td>

			</tr>
			<?php

	$k= 1 - $k;
}
?>
		</tbody>
	</table>

<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die("Direct Access Not Allowed");
jimport('joomla.application.component.view');
class ose_antivirusViewClamav extends ose_antivirusView {
	public function display($tpl= null) {
		$tmpl = JRequest::getVar('tmpl');
		if (empty($tmpl))
		{
			JRequest::setVar('tmpl', 'component');
		}
		$this->setEnvironment();
		$this->initScript();
		$this->loadJsFile();
		$this->setVariables();
		$title = JText :: _('OSE_ANTIVIRUS_CLAMAV');
		$this->assignRef('title', $title);
		parent :: display($tpl);
	}
	private function loadJsFile()
	{
		include(JPATH_ADMINISTRATOR.DS.'components/com_ose_antivirus/views/clamav/tmpl/js.php');
	}
	private function setVariables () {
		$model = $this->getModel('clamav');
		$clamStatus = $model ->getStatus (); 
		$this->assignRef('clamstatus', 	$clamStatus);
	}
}
<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');

?>
<script type="text/javascript" >
Ext.onReady(function(){
    var reload = Ext.get('reload');
    reload.on('click', function(){

    	var formEl = Ext.get('reloadsection').query('input');
    	params=new Object();
    	params.controller = 'clamav';
    	params.task = 'reloaddb';
    	for (i =0; i < formEl.length; i++)
		{
			if (formEl[i].value == 1 && formEl[i].name!='')
			{
				var tmp = formEl[i].name;
				params[tmp] = 1;
			}
		}
    	Ext.Ajax.request({
			url : 'index.php?option=com_ose_antivirus' ,
			params : params,
			method: 'POST',
			success: function ( result, request ) {
				var msg = Ext.decode(result.responseText);
				if (msg.status=='Done')
				{
					Ext.Msg.alert('DONE', Joomla.JText._('RELOADING_PAGE'), function(btn,txt){
						if(btn == 'ok')	{
							window.location = 'index.php?option=com_ose_antivirus&view=clamav';
						}
					});
					
					 
				}
				else
				{
					Ext.Msg.alert('ERROR', Joomla.JText._('ERROR_RELOADING_DEF'));
				}

			}
		});

		
    })   
});
</script>

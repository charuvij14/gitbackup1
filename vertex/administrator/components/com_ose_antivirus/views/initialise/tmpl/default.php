<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
// no direct access
defined('_JEXEC') or die(';)');
//include(JPATH_ADMINISTRATOR.DS.'components/com_ose_antivirus/views/initialise/tmpl/js.php');
JHTML :: _('behavior.modal');
?>
<script type="text/javascript">
	function sleep(ms)
	{
		var dt = new Date();
		dt.setTime(dt.getTime() + ms);
		while (new Date().getTime() < dt.getTime());
	}
</script>

<div id = "oseheader">
  <div class="container">
	<div class="logo-labels">
		<h1><a href="http://www.opensource-excellence.com" target= "_blank"><?php echo JText::_("Open Source Excellence"); ?></a></h1>
		<?php
			echo $this->preview_menus; 
		?>
	</div>
	
<?php
	oseSoftHelper::showmenu();
?>
	<div class="section">
		<div id="sectionheader"><?php echo $this->title; ?></div>
		<div class="grid-title">
			<?php
				echo JText :: _('DATABASE_INITIALISATION_FOR_THE_SELECTED_DIRECTORIES');
			?>
		</div>
			<?php
			$etext= '';
			$virus_count = 0;
			//$selectedItems= JRequest::getVar('selected');
			$selectedItems = array (); 
			$currentSession = JSession::getInstance('oseantivirus',array());
			$scanInfo = $currentSession->get("scanInfo");
			
			$fileScan= oseRegistry :: call('filescan');
			$fileScan->initFileScan($etext, $virus_count, $scanInfo['selected']);
			?>

	</div>
  </div>
</div>

<?php
		echo oseSoftHelper::renderOSETM();
?>					
<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.controller');
/**
 * scan default Controller
 *
 * @package    scan
 * @subpackage Controllers
 */
class ose_antivirusController extends JController {
	var $oseJSON = null;
	function __construct() {
		$this->oseJSON = new oseJSON();
		parent :: __construct();
	}
	/**
	 * Method to display the view
	 *
	 * @access	public
	 */
	function display($cachable = false, $urlparams = false) {
		if(JRequest :: getCmd('view') == '') {
			JRequest :: setVar('view', 'scan');
		}
		$this->item_type= 'scan';
		if(JRequest :: getCmd('view') != 'initialise')
		{
			//$this->hasactivated();
		}
		parent :: display();
	}
	function refererCheck()
	{
		// Referer Control -- Anti CSRF;
		$curURL= str_replace("?".$_SERVER['QUERY_STRING'], "", $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
		if(isset($_SERVER['HTTP_REFERER'])) {
			$referer= $_SERVER['HTTP_REFERER'];
		} else {
			$referer= "";
		}
		$mainframe = JFactory::getApplication();
		if (empty($referer))
		{
			//$mainframe->redirect("index.php", JText::_("Anti-CSRF Control: HTTP Referer not defined"));
		}
		else
		{
			$curURL=explode("administrator", $curURL);
			$referer = explode("administrator", str_replace(array("http://", "https://"), "", $referer));
			if ($curURL[0]!=$referer[0])
			{
				//$mainframe->redirect("index.php", JText::_("Anti-CSRF Control: HTTP Referer host does not match server host"));
			}
		}
		// Referer Control -- Anti CSRF Ends;
	}
	function hasactivated()
	{
		$db= JFactory::getDBO();
		$query = "SELECT * FROM #__ose_activation WHERE ext = 'oseavi'";
		$db->setQuery($query);
		$result = $db->loadObject();
		if (empty($result))
		{
			$view= JRequest::getVar('view');
			if ($view!='oseactivation')
			{
				$mainframe = JFactory::getApplication();
				$mainframe ->redirect('index.php?option=com_ose_antivirus&view=oseactivation');
			}
		}
		elseif ($result->id == base64_decode($result->code))
		{
			return true;
		}
		else
		{
			$view= JRequest::getVar('view');
			if ($view!='oseactivation')
			{
				$mainframe = JFactory::getApplication();
				$mainframe ->redirect('index.php?option=com_ose_antivirus&view=oseactivation');
			}
		}
	}
	function getOSEItem($modelName, $funcName)
	{
		$model= $this->getModel($modelName);
		$items= $model->$funcName();
		$result= array();
		$result['results']= $items;
		$result['total']= count($items);
		$result= $this->oseJSON->encode($result);
		print_r($result); 
		exit;
	}
	
	function getOSEFormItem($modelName, $funcName)
	{
		$model= $this->getModel($modelName);
		$items= $model->$funcName();
		$result= array();
		$result['data']= $items;
		$result['success']= true;
		$result= $this->oseJSON->encode($result);
		print_r($result); 
		exit;
	}
	
	public function aJaxReturn ($result, $status, $msg, $continue=false, $id = null) {
		$return = array (
				'success' => (boolean)$result,
				'status' => $status,
				'result' => $msg,
				'cont' => (boolean)$continue,
				'id' => (int)$id
		);
		$tmp = oseJSON::encode ($return);
		print_r($tmp); exit;
	}
}
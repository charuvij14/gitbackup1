<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
//error_reporting(0);
// Define and Call Joomla Core;
define('_JEXEC', 1);
define('JPATH_BASE', dirname(dirname(dirname(dirname(dirname(__FILE__))))));
define('DS', DIRECTORY_SEPARATOR);
require_once(JPATH_BASE.DS.'includes'.DS.'defines.php');
require_once(JPATH_BASE.DS.'includes'.DS.'framework.php');
$mainframe= & JFactory :: getApplication('site');
// Define and Call OSE Core;
define('OSEAV_CONFIG_DATE_FORMAT', 'd/m/y H:i:s');
define('OSEAV_ONLINE', true);
define('THISSIGMD5', '1f1ad1d339793b8319fcbd77476e9c8e');
define('ATHSIGMD5', '4e6bccf23963175fee871e8d9a63d197');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_cpu'.DS.'define.php');
require_once(OSECPU_B_PATH.DS.'oseregistry'.DS.'oseregistry.php');
oseRegistry :: register('registry', 'oseregistry');
oseRegistry :: call('registry');
oseRegistry :: register('filescan', 'filescan');
oseRegistry :: register('virusscan', 'virusscan');
// Include User defined Configuration File;
include_once("config.php");
// Define Configuration File;
if(class_exists("SConfig"))
{
	$JConfig= new SConfig();
}
else
{
	$JConfig= new JConfig();
}
// Check if DB is defined;
if($JConfig->db == '')
{
	echo JText :: _("Database not defined;");
	exit;
}
// Check user IP: The script can only be runned by certified IPs only
$remoteIP= getRealIP();
if(!in_array($remoteIP, $allowedIPs))
{
	echo JText :: _("Your IP is not in the allowed IP list;");
	exit;
}
$etext= ""; // initialise the email details buffer
$virus_count= 0; // the virus counter
// if we were called by the admin interface, write to our popup window
// if not, we may need to send an email
if(defined('OSEAV_ONLINE'))
{
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr" id="minwidth" >
	<head>
	<title>OSE Anti-Virus Scanner </title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<script type="text/javascript">
	function sleep(ms)
	{
		var dt = new Date();
		dt.setTime(dt.getTime() + ms);
		while (new Date().getTime() < dt.getTime());
	}
 	</script>
	<style type="text/css">
	#content-panel
	{
	  padding: 20px;
	  border: 1px solid #9c3;
	  background: #fff;
	  font-family: Verdana, Arial, Helvetica, sans-serif
	}
	.warning
	{
		color: red;
	}
	</style>

	</head>
	<body style="font-family:verdana, arial, sans-serif;font-size:11px;">
	<?php

	scan($etext, $virus_count, $scanDir, $excludeDir, $countSpeed);
	echo '</body></html>';
}
else
{
	/*
	scan($etext,$virus_count);
	if (($virus_count > 0)) and (OSEAV_CONFIG_EMAIL_TO != ''))
	{
	$ebody = _text_('COM_EYESITE_SCANNER_TOTAL').' '.$changes_count."\n";
	if ($etext == "")
		$ebody = $ebody._text_('COM_EYESITE_SCANNER_NO_NEW').' '.date(OSEAV_CONFIG_DATE_FORMAT)."\n";
	else
		$ebody = $ebody._text_('COM_EYESITE_SCANNER_CHANGES').' '.date(OSEAV_CONFIG_DATE_FORMAT).":\n".$etext."\n\n";
	if ($error_count > 0)
		$ebody = $ebody._text_('COM_EYESITE_SCANNER_ERRORS')."\n\n";
	$header = 'Content-Type: text/plain; charset="UTF-8"'."\r\nFrom: Eyesite\r\n";
	mail(OSEAV_CONFIG_EMAIL_TO, OSEAV_CONFIG_EMAIL_SUBJECT, $ebody, $header);
	}
	*/
}
// Scan the configured directory structures
function scan($etext, $virus_count, $scanDir, $excDirs, $countSpeed)
{
	$logfilename= "oseav_".date("y-m-d").".log";
	$curPath= dirname(__FILE__);
	$oseAVLogPath= $curPath.DS.$logfilename;
	$error_count= 0;
	if(file_exists($oseAVLogPath))
	{
		//unlink($oseAVLogPath);
	}
	if(!version_compare(phpversion(), "4.2.0", ">="))
	{
		$error_count++;
		return;
	}
	$numIncDirs= count($scanDir);
	$result= array();
	$statusQuery= null;
	$resultQuery= null;
	$infectedNum= 0;
	$fileScan= oseRegistry :: call('filescan');
	$virScan= oseRegistry :: call('virusscan');
	$virScan->setDefs();
	$virScan->setPatterns();
	$exts= $virScan->getScanExt();
	$oseFolder= $fileScan->getFolder();
	$stat= $virScan->getStat();
	$stat->truncateDetected();
	$infectedNum= 0;
	$count= 0;
	$db= JFactory :: getDBO();
	$oseFile= $fileScan->getFile();
	// Cleant the database;
	$query= "TRUNCATE TABLE `#__oseav_scanitems`";
	$db->setQuery($query);
	if(!$db->query())
	{
		$error_count++;
		oseExit('Mysql Query Run Error!');
	}
	// process all the directories
	// this should set all the states to either OK or CHANGED
	// and could create some with state NEW
	$_SESSION['osefoundvirus']= false;  
	echo '<div id="content-panel">'.JText :: _('OSE Virus Scanning Started').'<br/>';
	$_SESSION['osecount']=0;
	for($i= 0; $i < $numIncDirs; $i= $i +2)
	{
		//$recursive = (isset($scanDir[$i+1]))?true:false;
		$filelist= dirList($scanDir[$i], 'S', $exts, $excDirs, $_SESSION['osecount']);
	}
	$db->__destruct(); 
	
	send_email($_SESSION['osefoundvirus']); 

	echo '<script>window.scrollBy(0,5000);</script>';
	echo "<script>setTimeout('self.close ()',10000);</script>";
	echo JText :: _('Virus Scanning Completed') .'</div>';
	echo '<script>window.scrollBy(0,200);</script>';
	
}
//-------------------------------------------------------------------------------
// list matching files in a directory and return as an array
//
function dirList($directory, $recurse, $extensions, $excDirs, $count, $countSpeed=25)
{
	$results= array();
	$db=&JFactory::getDBO();
	$fileScan= oseRegistry :: call('filescan');
	$virScan= oseRegistry :: call('virusscan');
	if(_in_arrayi($directory, $excDirs))
	{
		return $results;
	}
	if($handle= opendir($directory))
	{
		while($filename= readdir($handle))
		{
			if($filename != "." && $filename != "..")
			{
				if(is_dir($directory.DS.$filename))
				{
					if(strstr($directory.DS.$filename, '.svn'))
					{
						continue;
					}
					else
					{
						if($recurse == "S")
						{
							dirList($directory.DS.$filename, $recurse, $extensions, $excDirs,  $_SESSION['osecount']);
						}
					}
				}
				else
				{
					$ext= getExt($filename);
					if((empty($extensions)) or(_in_arrayi($ext, $extensions)))
					{ // yes...
						$filename= $directory.DS.$filename; // make full pathname
						$query= "Start Transaction";
						$db->setQuery($query);
						$db->Query();
						echo $filename.'<br/>';
						$scanResult= $virScan->virusScan($filename);
						 $_SESSION['osecount']++;
						if($scanResult == true)
						{
							echo '<div style="color:#CB2208;">Virus Found in file: '.$filename.'</div>';
							$_SESSION['osefoundvirus'] = true; 
						}
						if(( $_SESSION['osecount'] % $countSpeed) == 0)
						{
							echo  $_SESSION['osecount'].' Files Processed. Continue...<br />';
							echo '<script> window.scrollBy(0,5000);</script>';
						}
						$query= "Commit";
						$db->setQuery($query);
						$db->Query();
						//$results[]= $filename; // and store in results array
					}
				}
			}
		}
		closedir($handle);
	}
	return $results;
}
//-------------------------------------------------------------------------------
// Case insensitive in_array
//
function _in_arrayi($needle, $haystack)
{
	foreach($haystack as $value)
		if(strtolower($value) == strtolower($needle))
			return true;
	return false;
}
function getExt($file)
{
	return JFile :: getExt($file);
}
function getRealIP()
{
	$ip= false;
	if(!empty($_SERVER['HTTP_CLIENT_IP']))
	{
		$ip= $_SERVER['HTTP_CLIENT_IP'];
	}
	if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
	{
		$ips= explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
		if($ip)
		{
			array_unshift($ips, $ip);
			$ip= false;
		}
		for($i= 0; $i < count($ips); $i++)
		{
			if(!preg_match("/^(10|172\.16|192\.168)\./i", $ips[$i]))
			{
				if(version_compare(phpversion(), "5.0.0", ">="))
				{
					if(ip2long($ips[$i]) != false)
					{
						$ip= $ips[$i];
						break;
					}
				}
				else
				{
					if(ip2long($ips[$i]) != -1)
					{
						$ip= $ips[$i];
						break;
					}
				}
			}
		}
	}
	return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
}

 function getAdminGids()
 {
	$db = JFactory :: getDBO();
	$db->setQuery("SELECT id FROM #__usergroups");
	$groups = $db->loadResultArray();
		
	$admin_groups = array();
	foreach ($groups as $group_id)
	{
		if (JAccess::checkGroup($group_id, 'core.login.admin'))
		{
			$admin_groups[] = $group_id;
		}
		elseif (JAccess::checkGroup($group_id, 'core.admin'))
		{
			$admin_groups[] = $group_id;
		}
	}
	$admin_groups = array_unique($admin_groups);
	return $admin_groups;  
 }

 function send_email($virusfound=false) {

		$subject = "OSE Anti-Virus (TM) Alert for [".$_SERVER['HTTP_HOST']."]";
		$logtime = date("Y-m-d, h:i:s");
		
		if ($virusfound==true)
		{
			$msg = "An virus was found on <br/>";
		}
		else
		{
			$msg = "The following webiste is scanned and confirmed clean: <br/>";
		}
		
		
		$msg .= "Host: ".$_SERVER['HTTP_HOST']."<br />";
        $msg .= "At: ".$logtime.".<br />";
		$msg .= "Please login OSE Anti-Virus backend and check the scanning logs."."<br />";
		$msg .= "__________________________<br />";
		$msg .= "OSE Anti-Virus™ Security Alert";

		$db= JFactory :: getDBO();

		if (class_exists("SConfig"))
		{
			$admingids = getAdminGids(); 
			$admingids = implode(',', $admingids); 
			
			$query = " SELECT u.email FROM `#__users` AS u "
					." INNER JOIN `#__user_usergroup_map` AS g ON g.user_id = u.id"
					." WHERE g.group_id IN ( ".$admingids." )  AND sendEmail =1"
					;
		}
		else
		{
			jimport( 'joomla.version' );
			$version = new JVersion();
			$version = substr($version->getShortVersion(),0,3);

			if($version == '1.5')
			{
				$query = " SELECT u.email FROM `#__users` AS u "
						." WHERE gid IN ( 25 ) AND sendEmail =1"
						;
			}
			else
			{
				$admingids = getAdminGids();
				$admingids = implode(',', $admingids);
							
				$query = " SELECT u.email FROM `#__users` AS u "
						." INNER JOIN `#__user_usergroup_map` AS g ON g.user_id = u.id"
						." WHERE g.group_id IN ( ".$admingids." )  AND sendEmail =1"
						;						
			}
		}
		$db->setQuery($query);
		$results= $db->loadAssocList();

		if (class_exists("JFactory"))
		{
			$mail = &JFactory::getMailer();
			foreach($results as $result) {
				$mail->addRecipient($result['email']);
			}
			$mail->setSubject($subject);
			$mail->setBody($msg);
			$mail->IsHTML(true);
			$mail->Send();
		}
		else
		{
			if (class_exists("JConfig"))
			{
				$config_var = new JConfig();
			}
            elseif (class_exists("SConfig"))
            {
            	$config_var = new SConfig();
            }
            $MailFrom = $config_var->mailfrom;
            $FromName = $config_var->fromname;
            $headers = "From: " . $FromName . " <" . $MailFrom . ">\n";
            $headers .= "Reply-To: <" . $MailFrom . ">\n";
            $headers .= "Return-Path: <" . $MailFrom . ">\n";
            $headers .= "Envelope-from: <" . $FromName . ">\n";
            $headers .= "Content-Type: text/plain; charset=UTF-8\n";
            $headers .= "MIME-Version: 1.0\n";
            foreach ($results as $result)
            {
                mail($result['email'], $subject, $msg, $headers);
            }
		}
	}
?>
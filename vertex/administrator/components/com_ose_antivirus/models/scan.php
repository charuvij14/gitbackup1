<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.model');
/**
 * scan Model
 *
 * @package    scan
 * @subpackage Models
 */
class ose_antivirusModelscan extends ose_antivirusModel
{
	var $query = null;
	var $virScan = '';
	public function __construct()
	{
		$this->virScan = new oseVirusScanner();
		parent::__construct();
	}
	public function getScanResult()
	{
		$stat = $this->virScan->getStat();
		return $stat->getResult();
	}
	public function getTotal($exts = array())
	{
		return $this->virScan->getStat()->getTotalScanItems($exts);
	}
	public function getLastScanLog()
	{
		return $this->virScan->getStat()->getLastScanLog();
	}
	public function getScanExt()
	{
		$exts = $this->virScan->getScanExt();
		if (empty($exts) || $exts[0] == null)
		{
			$exts = array('htm', 'html', 'shtm', 'shtml', 'css', 'js', 'php', 'php3', 'php4', 'php5', 'inc', 'phtml', 'jpg', 'jpeg', 'gif', 'png', 'bmp', 'c', 'sh', 'pl', 'perl', 'cgi', 'txt');
		}
		return $exts;
	}
	public function getConfiguration()
	{
		$db = JFactory::getDBO();
		$oseJSON = new oseJSON();
		$query = " SELECT * FROM `#__ose_secConfig` WHERE `type` = 'vsscan' ORDER BY `id` ";
		$db->setQuery($query);
		$results = $db->loadObjectList();
		$return = array();
		foreach ($results as $result)
		{
			if ($result->key == 'vsScanExt')
			{
				$return['file_ext'] = $oseJSON->decode($result->value);
			}
			else
			{
				if (is_numeric ( $result->value ) )
				{
					$return[$result->key] = (int)$result->value;
				}
				else
				{
					$return[$result->key] = (string)$result->value;
				}
			}
		}
		$return['id'] = '0';
		return $return;
	}
	public function getToken()
	{
		$session = JFactory::getSession();
		$token = OSESoftHelper::randStr(16);
		$session->set('ose.session.token', $token, 'oseav');
		return $token;
	}
	public function getScanFiles()
	{
		$currentSession = JSession::getInstance('oseantivirus', array());
		$scanInfo = $currentSession->get("scanInfo");
		if (!empty($scanInfo))
		{
			$return = "<ul>";
			foreach ($scanInfo['selected'] as $selected)
			{
				$fpath = str_replace('//', DS, $selected);
				$return .= "<li>".$fpath."</li>";
			}
			$return .= "</ul>";
		}
		else
		{
			$return = 'Please initialize database first'; 
		}
		return $return;
	}
	public function getTotalFiles () {
		$exts = $this->getScanExt();
		$total = $this->getTotal($exts);
		return $total;  
	}
}
?>

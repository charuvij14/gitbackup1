<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');
jimport('joomla.filesystem.file');
/**
 * scan Model
 *
 * @package    scan
 * @subpackage Models
 */
class ose_antivirusModelquarantine extends ose_antivirusModel {
	var $query= null;
	var $virScan = null;
	function __construct() {
		$this->virScan = new oseVirusScanner();
		parent :: __construct();
	}
    function getQuaFileList($start, $limit)
    {
		return $this->virScan->getQuaFileList($start, $limit);
    }

    function getTotalNumberQuaFile()
    {
		return $this->virScan->getTotalNumberQuaFile();
    }

	function getClassFiles() {
		$tmpl= new files;
		return $tmpl;
	}
	function getClassFile() {
		$tmpl= new filee;
		return $tmpl;
	}
	function _buildQuery($query) {
		$this->query= $query;
	}
	function remove() {
		$db= JFactory :: getDBO();
		$cids= JRequest :: getVar('cid', array(0), 'post', 'array');
		$cids= implode(',', $cids);
		$query= "SELECT * FROM `#__scan_result` WHERE id IN ({$cids})";
		$db->setQuery($query);
		$results= $db->loadObjectList();
		$fileScan= oseRegistry :: call('filescan');
		$virScan= oseRegistry :: call('virusscan');
		$stat= $virScan->getStat();
		$log_msg= array();
		foreach($results as $result) {
			$virScan->quarantine($result);
			if($virScan->clean()) {
				$log_msg[]= JText :: _('The Infected File[').$result->filepath.JText :: _(']  has been cleaned.');
			} else {
				$log_msg[]= JText :: _('The Infected File [').$result->filepath.JText :: _('] could not be cleaned. Please clean it manually.');
			}
		}
		// Delete the precessed data
		$query= "DELETE FROM `#__scan_result` WHERE id IN ({$cids})";
		$db->setQuery($query);
		if($db->query()) {
			// Write the log
			$stat->cleanHistory($log_msg);
			return true;
		} else {
			return false;
		}
	}
	function delete() {
		$db= JFactory :: getDBO();
		$cids= JRequest :: getVar('cid', array(0), 'post', 'array');
		$cids= implode(',', $cids);
		$query= "SELECT `filepath`, `id` FROM `#__scan_result` WHERE id IN ({$cids})";
		$db->setQuery($query);
		$results= $db->loadObjectList();
		//$fileScan = oseRegistry::call('filescan');
		//$virScan = oseRegistry::call('virusscan');
		//$stat = $virScan->getStat();
		$log_msg= array();
		foreach($results as $result) {
			$filepath= addslashes($result->filepath);
			//chmod(0777, $filepath);
			//echo addslashes($filepath);exit;
			if(unlink($filepath)) {
				$log_msg[]= JText :: _('The infected File[').$result->filepath.JText :: _(']  has been deleted.');
				// Delete the precessed data
				$query= "DELETE FROM `#__scan_result` WHERE id IN ({$result->id})";
				$db->setQuery($query);
				if($db->query()) {
					// Write the log
					//$stat->cleanHistory($log_msg);
					$result= true;
				} else {
					$result= false;
				}
			} else {
				$result= false;
				$log_msg[]= JText :: _('The infected File [').$result->filepath.JText :: _('] could not be deleted. Please delete it manually.');
			}
		}
		return $result;
	}
	function restore() {
		$db= JFactory :: getDBO();
		$cids= JRequest :: getVar('cid', array(0), '', 'array');
		$cids= implode(',', $cids);
		$query= "SELECT * FROM `#__scan_quar` WHERE id IN ({$cids})";
		$db->setQuery($query);
		$results= null;
		$results= $db->loadObjectList();
		$fileScan= oseRegistry :: call('filescan');
		$virScan= oseRegistry :: call('virusscan');
		$stat= $virScan->getStat();
		$ids= array();
		$restore_result= false;
		foreach($results as $result) {
			$result->filepath= realpath($result->filepath.DS);
			$file->receive($result->filepath);
			if($file->restore($result->quar_name)) {
				$log_msg[]= "{$result->filepath} has been restored!";
				$ids[]= $result->id;
				$restore_result= true;
			} else {
				$log_msg[]= "{$result->filepath} fail to restore!";
				$restore_result= false;
			}
		}
		// Delete the restored data
		$query= "DELETE FROM `#__scan_quar` WHERE id IN (".implode(',', $ids).")";
		$db->setQuery($query);
		$db->query();
		// write the log;
		$stat->restoreHistory($log_msg);
		return $restore_result;
	}
	function whitelist() {
		$db= JFactory :: getDBO();
		$cids= JRequest :: getVar('cid', array(0), '', 'array');
		$cids= implode(',', $cids);
		$query= "SELECT * FROM `#__scan_result` WHERE id IN ({$cids})";
		$db->setQuery($query);
		$results= $db->loadObjectList('filepath');
		foreach($results as $result) {
			$values[]= '(\''.mysql_real_escape_string($result->filepath).'\')';
		}
		if(count($values) > 0) {
			$values= implode(',', $values);
		}
		$query= "INSERT INTO `#__scan_whitelist` (filepath) VALUES ".$values;
		$db->setQuery($query);
		if($db->query()) {
			$query= "DELETE FROM `#__scan_result` WHERE id IN ({$cids})";
			$db->setQuery($query);
			return $db->query();
		} else {
			return false;
		}
	}
}
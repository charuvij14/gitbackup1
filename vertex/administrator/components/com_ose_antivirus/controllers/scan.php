<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.controller');
jimport('joomla.filesystem.path');
class ose_antivirusControllerscan extends ose_antivirusController
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	private $filestable = '#__oseav_scanitems';
	private $logstable = '#__oseav_scanlogs';
	private $malwaretable = '#__oseav_detected';
	private $file_ext = '';
	private $config = '';
	private $maxfilesize = 0;
	private $patterns = '';
	private $virScan = null;
	public function __construct()
	{
		$this->virScan = new oseVirusScanner();
		parent::__construct();
	} // function
	public function restore()
	{
		$this->setRedirect('index.php?option=com_scan&view=report&type=restore');
	}
	public function updateExt()
	{
		$this->virScan->updateExt();
	}
	public function timeDifference($timeStart, $timeEnd)
	{
		return $timeEnd - $timeStart;
	}
	public function vsscan()
	{
		$result = array();
		$statusQuery = null;
		$resultQuery = null;
		$infectedNum = 0;
		$fileScan = new oseFileScan();
		$stat = $this->virScan->getStat();
		$start = JRequest::getInt('start', 0);
		$type = JRequest::getVar('type', null);
		$init = JRequest::getInt('init', 0);
		if ($init == true)
		{
			$stat->truncateDetected();
			$stat->updateStatus();
			$infectedNum = 0;
		}
		$exts = $this->virScan->getScanExt();
		$exclude = $this->virScan->getWhitelisted('array');
		$scan_files = $fileScan->getItemsFromDB($exts, $start, '300');
		$this->virScan->setDefs();
		$this->virScan->setPatterns($type);
		if (COUNT($scan_files) == 0)
		{
			$result['status'] = 'Done';
			$result['scanResult'] = 'Scan Completed!';
			$result['start'] = '0';
			$result['total'] = $stat->getTotal();
			$result['progress'] = '1';
			$result['cont'] = '0';
			$stat->logscanHistory();
			$logs = $stat->getLastScanLog();
			$result['lstime'] = $logs['lstime'];
			$result['lsclean'] = $logs['lsclean'];
			$result['lsinfected'] = $logs['lsinfected'];
			$result['lsresults'] = $logs['lsresults'];
		}
		else
		{
			$start_date = time();
			foreach ($scan_files as $i => $scan_file)
			{
				$since_start = $this->timeDifference($start_date, time());
				if ($since_start >= 2)
				{
					break;
				}
				if (!JFile::exists($scan_file->filepath))
				{
					$this->virScan->updateFileState($scan_file->id, 1);
					continue;
				}
				if (in_array($scan_file->filepath, $exclude))
				{
					$this->virScan->updateFileState($scan_file->id, 1);
					continue;
				}
				if (filesize($scan_file->filepath) > 102400)
				{
					$this->virScan->updateFileState($scan_file->id, 1);
					continue;
				}
				else
				{
					$statusQuery .= $scan_file->filepath.'<br/>';
					$scanResult = $this->virScan->virusScan($scan_file->filepath, $type);
					$statusQuery .= $scanResult;
				}
				$this->virScan->updateFileState($scan_file->id, 1);
			}
			$scanHistory = $stat->getResult();
			$infectedNum = count($scanHistory);
			// Start to make result report
			if ($infectedNum < 1)
			{
				$resultQuery = JText::_('No suspicious files');
			}
			else
			{
				$resultQuery = $infectedNum.JText::_(' files has been infected');
			}
			$memory_usage = round(memory_get_usage(true) / (1028 * 1024), 2);
			$cpuload = sys_getloadavg();
			$completed = $stat->getCompleted();
			$total = $stat->getTotal();
			$progress = 1 - ($completed / $total);
			$result['progressText'] = "<b>".($completed)." files remaining, ".round($progress * 100, 2)."% completed </b>";
			$result['systemLoad'] = "Memory Usage: ".$memory_usage."MB, CPU load:".$cpuload[0];
			$result['status'] = 'Continue';
			$result['scannedObjects'] = $statusQuery;
			$result['start'] = $start;
			$result['total'] = (int) $total;
			$result['progress'] = $progress;
			$result['cont'] = '1';
			$result['threatsFound'] = (string) $infectedNum;
		}
		$result = $this->oseJSON->encode($result);
		echo $result;
		exit;
	}
	function vsclean()
	{
		$filesids = JRequest::getVar('filesids');
		$this->virScan->virusClean($filesids);
	}
	function vsquarantine()
	{
		$filesids = JRequest::getVar('filesids');
		$this->virScan->fileQuarantine($filesids);
	}
	function vsbackup()
	{
		$filesids = JRequest::getVar('filesids');
		$this->virScan->fileBackup($filesids);
	}
	function vswhitelist()
	{
		$filesids = JRequest::getVar('filesids');
		$this->virScan->fileWhitelist($filesids);
	}
	function vsrestore()
	{
		$filesids = JRequest::getVar('filesids');
		$this->virScan->fileRestore($filesids);
	}
	public function getConfiguration()
	{
		parent::getOSEFormItem('scan', 'getConfiguration');
	}
	public function saveConfiguration()
	{
		$this->virScan->saveConfiguration();
	}
	function setstartvalue()
	{
		$startvalue = JRequest::getInt('startvalue');
		$_SESSION['start'] = $startvalue;
		exit;
	}
	public function initDatabase()
	{
		$step = JRequest::getInt('step');
		$path = JRequest::getVar('path', null);
		if (empty($path))
		{
			$this->aJaxReturn(false, 'ERROR', "SCANNING PATH CANNOT BE EMPTY", false);
		}
		else
		{
			$this->file_ext = $this->getFileExtensions();
			$this->maxfilesize = $this->getMaxFileSize();
			$path = JPath::clean($path);
			$this->initDatabaseAction($step, $path);
		}
	}
	private function getFileExtensions()
	{
		$db = JFactory::getDBO();
		$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` = 'vsScanExt' ";
		$db->setQuery ($query);
		$result = $db->loadResult ();
		if (empty($result))
		{
			return oseJSON::decode('["htm","html","shtm","shtml","css","js","php","php3","php4","php5","inc","phtml","jpg","jpeg","gif","png","bmp","c","sh","pl","perl","cgi","txt","htaccess"]');
		}
		else
		{
			return oseJSON::decode($result);
		}
	}
	private function getMaxFileSize()
	{
		$db = JFactory::getDBO();
		$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` = 'maxfilesize' ";
		$db->setQuery ($query);
		$result = $db->loadResult ();
		if (empty($result))
		{
			return 2 * 1024 * 1024;
		}
		else
		{
			return $result * 1024 * 1024;
		}
	}
	private function clearTable()
	{
		$db = JFactory::getDBO();
		$query = "TRUNCATE TABLE `".$this->filestable."`";
		$db->setQuery($query);
		$result = $db->query();
		return $result;
	}
	public function initDatabaseAction($step, $directory)
	{
		if ($step < 0)
		{
			$this->clearTable();
			$return = $this->getReturn($directory);
		}
		else
		{
			$dirs = $this->getFolder(5);
			if (empty($dirs))
			{
				$return['cont'] = false;
				$return['folders'] = 0;
				$return['file'] = 0;
			}
			else
			{
				$return = array();
				$return['folder'] = 0;
				$return['file'] = 0;
				foreach ($dirs as $dir)
				{
					$tmp = $this->getReturn($dir->filename);
					$return['folder'] += $tmp['folder'];
					$return['file'] += $tmp['file'];
					$return['cont'] = $tmp['cont'];
					$return['lastscanned'] = 'Last scanned '.$dir->filename;
					$this->deletepathDB($dir->filename);
					unset($tmp);
				}
			}
		}
		if ($return['cont'] == true)
		{
			$return['summary'] = 'Found '.$return['folder'].' folders and '.$return['file'].' files in the last scan. Continue...';
		}
		else
		{
			$total = $this->CountFiles();
			$return['summary'] = 'Found in total of '.$total.' files';
		}
		print_r(oseJSON::encode($return));
		exit;
	}
	public function getReturn($path)
	{
		$return = $this->getFolderFiles($path);
		$return['cont'] = $this->isFolderLeft();
		return $return;
	}
	private function getFolderFiles($folder)
	{
		// Initialize variables
		$arr = array();
		$arr['folder'] = 0;
		$arr['file'] = 0;
		$false = false;
		if (!is_dir($folder))
			return $false;
		$handle = @opendir($folder);
		// If directory is not accessible, just return FALSE
		if ($handle === FALSE)
		{
			return $false;
		}
		while ((($file = @readdir($handle)) !== false))
		{
			if (($file != '.') && ($file != '..'))
			{
				$ds = ($folder == '') || ($folder == '/') || (@substr($folder, -1) == '/') || (@substr($folder, -1) == DIRECTORY_SEPARATOR) ? '' : DIRECTORY_SEPARATOR;
				$dir = $folder.$ds.$file;
				$isDir = is_dir($dir);
				if ($isDir)
				{
					$arr['folder']++;
					$this->insertData($dir, 'd');
				}
				else
				{
					$fileext = $this->getExt($dir);
					$filesize = filesize($dir);
					if (in_array($fileext, $this->file_ext))
					{
						if (!empty($this->maxfilesize))
						{
							if (filesize($dir) < $this->maxfilesize)
							{
								$arr['file']++;
								$this->insertData($dir, 'f', $fileext);
							}
						}
						else
						{
							$arr['file']++;
							$this->insertData($dir, 'f', $fileext);
						}
					}
				}
			}
		}
		@closedir($handle);
		return $arr;
	}
	private function isFolderLeft()
	{
		$db = JFactory::getDBO();
		$query = "SELECT COUNT(`id`) as count FROM `".$this->filestable."` WHERE `type` = 'd'";
		$db->setQuery($query);
		$result = $db->loadResult();
		return $result;
	}
	private function getExt($file)
	{
		$dot = strrpos($file, '.') + 1;
		return substr($file, $dot);
	}
	private function insertData($filename, $type, $fileext = '')
	{
		$result = $this->getfromDB($filename, $type, $fileext);
		if (empty($result))
		{
			$this->insertInDB($filename, $type, $fileext);
		}
	}
	private function getfromDB($filename, $type, $fileext)
	{
		$db = JFactory::getDBO();
		$query = " SELECT COUNT(`id`) as count "." FROM `{$this->filestable}` "." WHERE `filepath` = ".$db->Quote($filename)." AND `type` = ".$db->Quote($type)." AND `ext` = ".$db->Quote($fileext);
		$db->setQuery($query);
		$result = $db->loadResult();
		return $result;
	}
	public function insertInDB($filename, $type, $fileext)
	{
		$db = JFactory::getDBO();
		$query = "INSERT INTO `{$this->filestable}` (`id`, `filepath`, `type`, `checked`, `ext`) VALUES "." ('DEFAULT', ".$db->Quote($filename).", ".$db->Quote($type).",0, ".$db->Quote($fileext)."); ";
		$db->setQuery ($query);
		$db->query();
		return $db->insertid();
	}
	private function getFolder($limit)
	{
		$db = JFactory::getDBO();
		$query = "SELECT `filepath` as `filename` FROM `".$this->filestable."`"." WHERE `type` = 'd' LIMIT ".(int) $limit;
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return $result;
	}
	private function deletepathDB($path)
	{
		$db = JFactory::getDBO();
		$query = "DELETE FROM `".$this->filestable."` WHERE `type` = 'd' AND `filepath` = ".$db->Quote($path);
		$db->setQuery($query);
		return $db->query();
	}
	public function countFiles()
	{
		$db = JFactory::getDBO();
		$query = "SELECT COUNT(`id`) as count FROM `".$this->filestable."`"." WHERE `type` = 'f'";
		$db->setQuery($query);
		$result = $db->loadResult();
		return $result;
	}
	private function getFiles($limit, $status)
	{
		$db = JFactory::getDBO();
		$query = "SELECT `id`, `filename` FROM `".$this->filestable."`"." WHERE `type` = 'f' "." AND `checked` = ".(int) $status." LIMIT ".(int) $limit;
		$db->setQuery($query);
		$result = $db->loadObjectList();
		return (!empty($result)) ? $result : false;
	}
	public function setFileExts()
	{
		$this->file_ext = explode(',', trim($this->config->file_ext));
	}
	private function setMaxFileSize()
	{
		if ($this->maxfilesize > 0)
		{
			$this->maxfilesize = $this->maxfilesize * 1024 * 1024;
		}
	}
	public function resetDatabase () {
		require_once (JPATH_COMPONENT . DS . 'installer.helper.php');
		$oseInstaller = new oseInstallerHelper();
		$result = $oseInstaller -> resetDatabaes();
		$return = array();
		$return['success'] = true;
		if ($result ==true)
		{
			$return['status'] = 'SUCCESS';
			$return['result'] = 'Database successfully installed.';
		}
		else
		{
			$return['status'] = 'FAILURE';
			$return['result'] = 'Database failed to install.';
		}
		print_r(oseJSON::encode($return)); exit; 
	}
} // class
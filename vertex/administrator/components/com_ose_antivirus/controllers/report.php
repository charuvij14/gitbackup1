<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.controller');
/**
 * ssrrn_acl Controller
 *
 * @package    ssrrn_acl
 * @subpackage Controllers
 */
class ose_antivirusControllerreport extends ose_antivirusController
{
	private $virScan = null;
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	public function __construct()
	{
		$this->virScan= new oseVirusScanner ();
		parent::__construct();
	} // function
	public function getInfected()
	{
		$records = $this->virScan->getStat()->getResult();
		$results = array();
		$dir_above = substr(JPATH_ROOT, 0, strrpos(JPATH_ROOT, DS));
		if (!@is_readable($dir_above) || !is_dir($dir_above))
		{
			$rootPath = JPATH_ROOT;
		}
		else
		{
			$rootPath = $dir_above;
		}
		unset($dir_above);
		$i = 0;
		foreach ($records as $record)
		{
			$virus = explode(",", $record->virus);
			if (empty($record->type) || $record->type == 'generous')
			{
				$VSa = $this->virScan->getDefs();
				$VSb = $this->virScan->getPatternExp($record->type);
				$VSPatterns = array_merge($VSa, $VSb);
			}
			else
			{
				$VSPatterns = $this->virScan->getPatternExp($record->type);
			}
			unset($VSa);
			unset($VSb);
			if ($virus[0] != 'type3')
			{
				$record->virus = $VSPatterns[$virus[0]][$virus[1]];
				$record->virus = "[O]".$record->virus[0];
			}
			else
			{
				$record->virus = "[C]".$virus[1];
			}
			$record->view = "<a href='#' onClick='viewFiledetail({$record->id})'><img src='components/com_ose_antivirus/assets/images/page_white_magnify.png' /></a>";
		}
		$return['results'] = $records;
		$return['total'] = $this->virScan->getStat()->getDetectedTotal();
		echo $this->oseJSON->encode($return);
		exit;
	}
	public function getFileContent()
	{
		$id = JRequest::getInt('recordid');
		$file = $this->loadFilebyID ($id);
		$dir_above = substr(JPATH_ROOT, 0, strrpos(JPATH_ROOT, DS));
		if (!@is_readable($dir_above) || !is_dir($dir_above))
		{
			$rootPath = JPATH_ROOT;
		}
		else
		{
			$rootPath = $dir_above;
		}
		$file = str_replace($rootPath, '', $file);
		$content = '';
		$return['content'] = $content;
		echo $this->oseJSON->encode($return);
		exit;
	}
	private function loadFilebyID ($id)
	{
		$db = JFactory::getDBO();
		$query = "SELECT `filepath` FROM `#__oseav_detected` WHERE id =".(int) $id;
		$db->setQuery($query);
		$file = $db->loadResult();
		return $file;  
	}
	public function viewfile()
	{
		$id = JRequest::getInt('id');
		$filepath = $this->loadFilebyID ($id);
		$fileType = $this->getFileType($filepath);
		$content = '<textarea class="'.$fileType.'" style="width:100%;" name="codearea" id="codearea" rows="25" cols="120" wrap="off" >';
		if (!empty($filepath))
		{
			$content .= htmlspecialchars(file_get_contents($filepath));
		}
		else
		{
			$content .= 'Cannot read the file';
		}
		$content .= '</textarea>';
		$return = array();
		$return['results'] = $content;
		$return['id'] = 1;
		$return['fileType']= $fileType; 
		echo $this->oseJSON->encode($return);
		exit;
	}
	public function getFileType($filepath)
	{
		$s_info = pathinfo($filepath);
		$s_extension = str_replace('.', '', $s_info['extension']);
		switch (strtolower($s_extension))
		{
		case 'txt':
		case 'ini':
			$cp_lang = 'text';
			break;
		case 'cs':
			$cp_lang = 'csharp';
			break;
		case 'css':
			$cp_lang = 'css';
			break;
		case 'html':
		case 'htm':
		case 'xml':
		case 'xhtml':
			$cp_lang = 'html';
			break;
		case 'java':
			$cp_lang = 'java';
			break;
		case 'js':
			$cp_lang = 'javascript';
			break;
		case 'pl':
			$cp_lang = 'perl';
			break;
		case 'ruby':
			$cp_lang = 'ruby';
			break;
		case 'sql':
			$cp_lang = 'sql';
			break;
		case 'vb':
		case 'vbs':
			$cp_lang = 'vbscript';
			break;
		case 'php':
			$cp_lang = 'php';
			break;
		default:
			$cp_lang = 'generic';
		}
		return $cp_lang;
	}
}

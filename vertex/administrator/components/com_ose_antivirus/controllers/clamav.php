<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.controller');
class ose_antivirusControllerclamav extends ose_antivirusController
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
	public function reloaddb()
	{
		OSESoftHelper::checkToken();
		$model = $this->getModel('clamav');
		$result = array();
		if ($model->reloaddb())
		{
			OSESoftHelper::returnMessages(true, JText::_('STATUS_UPDATED_SUCCESS'));
		}
		else
		{
			OSESoftHelper::returnMessages(false, JText::_('STATUS_UPDATED_FAILED'));
		}
	}
}

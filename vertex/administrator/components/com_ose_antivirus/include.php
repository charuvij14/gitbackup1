<?php
/**
 * @version     6.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @author        Updated on 30-Mar-2013
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
require_once(JPATH_COMPONENT.DS.'define.php');
require_once(JPATH_COMPONENT.DS.'language.php');
require_once(dirname(__FILE__).DS.'helpers'.DS.'osesofthelper.php');
require_once(dirname(__FILE__).DS.'helpers'.DS.'oseantivirus.php');
if (JOOMLA30 == true)
{
	require_once(OSEAV_B_CONTROLLER.DS.'controller.php');
	require_once(OSEAV_B_MODEL.DS.'model.php');
	require_once(OSEAV_B_VIEW.DS.'view.php');
}
else
{
	require_once(JPATH_COMPONENT.DS.'legacy'.DS.'controller.php');
	require_once(JPATH_COMPONENT.DS.'legacy'.DS.'model.php');
	require_once(JPATH_COMPONENT.DS.'legacy'.DS.'view.php');
}
require_once(OSECPU_LIB_PATH.DS.'processors'.OSEDS.'cpu.php');
require_once(OSEFIREWALL_PATH.DS.'includes'.OSEDS.'database.php');
// LoadCPU Files;
$oseCPU = new oseCPU();
$oseCPU->callLibClass('json', 'oseJSON');
$oseCPU->callLibClass('vsscanner', 'vsscanner');
$oseCPU->callLibClass('vsscanner', 'filescan');
?>
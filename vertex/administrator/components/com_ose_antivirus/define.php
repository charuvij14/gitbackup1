<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
// no direct access
defined('_JEXEC') or die(';)');
ini_set("display_errors", "off");
if (!defined('_OSEEXEC')) {
	define('_OSEEXEC', true);
}
if (!defined('OSEDS')) {
	define('OSEDS', DIRECTORY_SEPARATOR);
}
// Require the base controller
define('OSEAVTITLE', 'OSE Anti-Virus™');
define('AVSSIGMD5', '9f38dae4632bbf9d7718945658006d79');
define('AVSSHSIGMD5', '749b37a8d668e8e44765d6378a812bd1');
define('AVSHTSIGMD5', '9d043c3ffc6774a9814ea74793a1c711');
define('AVSBSSIGMD5', 'e37711129ab5249e4f6e4c21489fa318');
define('AVSCSSIGMD5', 'dec37077115a20ed4b7f7315b28a2bd1');
define('ATHSIGMD5', '4e6bccf23963175fee871e8d9a63d197');
define('OSEAV', 'com_ose_antivirus');

define('OSEAVFOLDER', 'components/'.OSEAV);
define('OSEAV_B_PATH', JPATH_ADMINISTRATOR.DS.'components'.DS.OSEAV);
define('OSEAV_B_CONTROLLER', OSEAV_B_PATH.DS.'controllers');
define('OSEAV_B_MODEL', OSEAV_B_PATH.DS.'models');
define('OSEAV_B_VIEW', OSEAV_B_PATH.DS.'views');
define('OSEAV_B_EXTJS', OSEAV_B_PATH.DS.'js');

if (!defined('OSE_CMS_ADMIN_PATH')) {
	define('OSE_CMS_ADMIN_PATH', JPATH_ADMINISTRATOR);
}
if (!defined('OSE_CMS_FRONT_PATH')) {
	define('OSE_CMS_FRONT_PATH', JPATH_SITE);	
}

if (!defined('OSE_CPU_PATH')) {
	define('OSE_CPU_PATH', OSE_CMS_ADMIN_PATH . OSEDS . 'components' . OSEDS . 'com_ose_cpu');	
}
if (!defined('OSE_CPU_FRONTPATH')) {
	define('OSE_CPU_FRONTPATH', OSE_CMS_FRONT_PATH . OSEDS . 'components' . OSEDS . 'com_ose_cpu');
}

if (!defined('OSEFIREWALL_PATH')) {
	define('OSEFIREWALL_PATH', OSE_CPU_PATH . OSEDS . 'firewall');
}

if (!defined('OSECPU_LIB_PATH')) {
	define('OSECPU_LIB_PATH', OSE_CPU_PATH . OSEDS . 'library');
}

$version = new JVersion();
$version = substr($version->getShortVersion(),0,3);
if(!defined('JOOMLA16'))
{
	$value = ($version >= '1.6')?true:false;
	define('JOOMLA16',$value);
}
if(!defined('JOOMLA30'))
{
	$value = ($version >= '3.0' && $version <='5.0')?true:false;
	define('JOOMLA30',$value);
}
?>

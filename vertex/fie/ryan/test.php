<?php

	// Require genres array:
	require("genres.php");

	// Check if we are using a valid id parameter:
	if( !isset($_GET["id"]) )
		redirect();
	
	// Get a local copy:
	$id = (int)($_GET["id"]);
	
	// Check finished:
	if ( $id == count($genres) )
		redirect('true');
	
	// Check for id in range:
	if ( $id < 0 || $id > count($genres) )
		redirect();
	
?>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Estadística - <?=$genres[$id]?></title>
		<link rel="stylesheet" type="text/css" href="style.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="code.js"></script>
	</head>
	<body>
		<header>
			<h1><?echo $genres[$id]?></h1>
		</header>
		
		<audio id="song" oncanplay="can_play = true;"
						 onloadeddata="can_play = true;">
			<source src="music/song<?echo $id?>.ogg" type="audio/ogg" />
		</audio>
		
	    <img src="img/sopa<?echo $id?>.png" id="sopa" class="myBox" />
	    
	    <div id="cntrl">
	   	    <a href="test.php?id=<?=($id-1)?>">Anterior</a>
    	    <button class="myButton" onclick="runTest();">Comenzar</button>
    	    <span id="timer"></span>
    	    <a href="test.php?id=<?=($id+1)?>">Siguiente</a>
    	</div>
    	
    	<div id="home">
    		<a href="index.php">INICIO</a>
    	</div>
    	
    	<footer>
    		<a href="http://www.iesmigueldecervantes.es">I.E.S. Miguel de Cervantes</a> - 
        	<a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&view=article&id=103&Itemid=693">Estadística</a>
    	</footer>
    	    
	</body>
</html>

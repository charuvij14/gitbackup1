$(document).ready(function()
{
	// Hide words and timer:
	$("#sopa").css('visibility', 'hidden');
	$("#timer").hide();
});

// Global variable for timer:
var time_allowed = 150;
var seconds = time_allowed;
var counter;
var can_play = false;

function runTest()
{
	// Show words:
	$("#sopa").css('visibility', 'visible');
	// Play song:
	if ( can_play )
		$("#song").trigger('play');
	
	// Hide button and show timer:
	$(".myButton").hide();
	$("#timer").show();
	$("#timer").html(seconds);
	
	// Setup timeout and timer:
	setTimeout(endTest, time_allowed*1000);
	counter = setInterval(timer, 1000);
}

function endTest()
{
	// Hide words:
	$("#sopa").css('visibility', 'hidden');
	// Stop song:
	if ( can_play )
		$("#song").trigger('pause').prop("currentTime", 0);
	
	// Restore button:
	$(".myButton").show();
	
	// Clear and restart counter:
	clearInterval(counter);
	$("#timer").hide();
	seconds = time_allowed;
}

function timer()
{
	// Decrement counter:
	seconds--;
	
	// Update counter
	$("#timer").html(seconds);
}

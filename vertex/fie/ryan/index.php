﻿<?php
	
	// Require genres array:
	require("genres.php");
	
?>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Estadística</title>
		<link rel="stylesheet" type="text/css" href="style.css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script>
			$(document).ready(function()
			{
				$("#instruct>div").hide();
				$("#instruct>h3").click(function ()
				{
					$("#instruct>div").slideToggle("fast");
				});
				<?
					if ( isset($_GET["finished"]) and $_GET["finished"] == "true" )
						echo "alert('¡Gracias por participar!')";
				?>
			});
		</script>
	</head>
	<body>
		<header>
			<h1>Análisis de la influencia de la música sobre el poder de concentración</h1>
		</header>
		
			<div id="instruct" class="myBox">
				<h3>Instrucciones</h3>
				<div>
					<p>El objeto de este test es estudiar la influencia que puede tener, sobre la concentración,
					el escuchar distintos tipos de música. Para ello, se proponen una serie de ejercicios consistentes
					en encontrar el mayor número posible de palabras ocultas en una sopa de letras en un tiempo
					determinado, escuchando distintas canciones.</p>
					<p>Haz click sobre el enlace <b>Control</b> para realizar la prueba sin música. Una vez abierta
					la siguiente página, haz click sobre el botón <b>Comenzar</b>: aparecerán la sopa de letras y
					una cuenta atrás. Anota en el folio todas las palabras que encuentres. Finalizado el tiempo, la
					sopa de letras desaparecerá: haz click en <b>Siguiente</b> para pasar a la siguiente prueba,
					que procederá de la misma manera.</p>
					<p>Una vez concluidas todas las pruebas, volverás de nuevo a esta página.</p>
				</div>
			</div>
		
    	    <div id="list" class="myBox">
    	    	<ul>
    		    	<?
    		    		$i = 0;	// Track array index for file referencing		    		
        				// Treverse genres array to create links:
		        		foreach ( $genres as $item )
        				{
        					echo "<li><a href=\"test.php?id=$i\">$item</a></li>\n";
        					$i++;
        				}
        				?>
        		</ul>
        	</div>
        
        <footer>
        	<a href="http://www.iesmigueldecervantes.es">I.E.S. Miguel de Cervantes</a> - 
        	<a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&view=article&id=103&Itemid=693">Estadística</a>
        </footer>
	</body>
</html>

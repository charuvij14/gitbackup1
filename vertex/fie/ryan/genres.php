<?php

	// Create array to contain music information:
	global $genres;
	$genres = array("Control",
					"Música Clásica",
					"Rock",
					"Metal",
					"Rap",
					"Jazz",
					"Música Electrónica",
					"Pop");
	
	// Helper function:				
	function redirect ($val='')
	{
		header("Location: " . $_SERVER['server_name'] . "index.php?finished=" . $val);
		die();
	}

?>

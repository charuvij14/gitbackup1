<?php

/*****************
	GENERAL SETTINGS
*****************/
$imSettings['search']['general'] = array(
	'menu_position' => 'left',
	'defaultScope' => array(
		'0' => 'index.php',
		'3' => 'pagina-1.html',
		'5' => 'pagina-3.html',
		'12' => 'pagina-7.html'
	),
	'extendedScope' => array(
	)
);

/*****************
	PRODUCTS SEARCH
*****************/
$imSettings['search']['products'] = array(
);

/*****************
	IMAGES SEARCH
*****************/
$imSettings['search']['images'] = array(
	array(
		'title' => '',
		'description' => '',
		'location' => '',
		'src' => 'images/Lectura-4.png',
		'page' => 'pagina-2.php'
	)
);

/*****************
	VIDEOS SEARCH
*****************/
$imSettings['search']['videos'] = array(
);
$imSettings['search']['dynamicobjects'] = array(
	'20' => array(
		'Folder' => '/opt/php/iesmigueldecervantes/fie/DATA',
		'DefaultText' => '<p>hola que tal</p>',
		'Page' => 'index.php',
		'PageTitle' => 'Página de inicio',
		'ObjectTitle' => '',
		'ObjectId' => 'dynObj_02'
	)
);

// End of file search.inc.php
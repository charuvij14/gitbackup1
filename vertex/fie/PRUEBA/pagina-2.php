<?php
require_once("res/x5engine.php");
imCheckAccess('4', '');
?>
<!DOCTYPE html><!-- HTML5 -->
<html lang="es-ES" dir="ltr">
	<head>
		<title>Página 2 - Nuevo proyecto</title>
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv="ImageToolbar" content="False" /><![endif]-->
		<meta name="generator" content="Incomedia WebSite X5 Professional 11.0.3.18 - www.websitex5.com" />
		<meta name="viewport" content="width=1111" />
		<link rel="stylesheet" type="text/css" href="style/reset.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/print.css" media="print" />
		<link rel="stylesheet" type="text/css" href="style/style.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/template.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="style/menu.css" media="screen" />
		<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="style/ie.css" media="screen" /><![endif]-->
		<link rel="stylesheet" type="text/css" href="pcss/pagina-2.css" media="screen" />
		<script type="text/javascript" src="res/jquery.js?18"></script>
		<script type="text/javascript" src="res/x5engine.js?18"></script>
		<script type="text/javascript">
			x5engine.boot.push(function () { x5engine.eventBinder('body, #imHeaderBg, #imFooterBg', function() { x5engine.utils.location('pagina-2.php'); }, function () { $('body, #imHeaderBg, #imFooterBg').css('cursor', 'pointer'); }, function () { $('body, #imHeaderBg, #imFooterBg').css('cursor', 'default'); }); });
		</script>
		
	</head>
	<body>
		<div id="imHeaderBg"></div>
		<div id="imFooterBg"></div>
		<div id="imPage">
			<div id="imHeader">
				<h1 class="imHidden">Página 2 - Nuevo proyecto</h1>
				
			</div>
			<a class="imHidden" href="#imGoToCont" title="Salta el menu principal">Vaya al Contenido</a>
			<a id="imGoToMenu"></a><p class="imHidden">Menu Principal:</p>
			<div id="imMnMn" class="auto">
				<ul class="auto">
					<li id="imMnMnNode0">
						<a href="index.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página de inicio</span>
							</span>
						</a>
					</li><li id="imMnMnNode3">
						<a href="pagina-1.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página 1</span>
							</span>
						</a>
					</li><li id="imMnMnNode6">
						<span class="imMnMnFirstBg">
							<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 2<span class="imMnMnLevelImg"></span></span>
						</span>
						<ul class="auto">
							<li id="imMnMnNode12" class="imMnMnFirst">
								<a href="pagina-7.html">
									<span class="imMnMnBorder">
										<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página 7</span>
									</span>
								</a>
							</li><li id="imMnMnNode9" class="imMnMnMiddle">
								<span class="imMnMnBorder">
									<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 3</span>
								</span>
							</li><li id="imMnMnNode10" class="imMnMnMiddle">
								<span class="imMnMnBorder">
									<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 4</span>
								</span>
							</li><li id="imMnMnNode11" class="imMnMnLast">
								<span class="imMnMnBorder">
									<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 5</span>
								</span>
							</li>
						</ul>
					</li><li id="imMnMnNode5">
						<a href="pagina-3.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página 3</span>
							</span>
						</a>
					</li>
				</ul>
			</div>
			<div id="imContentGraphics"></div>
			<div id="imContent">
				<a id="imGoToCont"></a>
				<h2 id="imPgTitle">Página 2</h2>
				<div style="width: 1108px; float: left;">
					<div style="float: left; width: 554px;">
						<div id="imCell_1" class="imGrid[0, 0]"><div id="imCellStyleGraphics_1"></div><div id="imCellStyle_1"><div id="imTextObject_1">
							<div class="text-tab-content"  id="imTextObject_1_tab0" style="text-align: left;">
								<div class="text-inner">
									<span class="fs28">ESTA PÁGINA ES DE CONTENIDO PRIVADO</span><div><span class="fs28">SOLO LA PODRÁN VER USUARIOS CON ACCESO</span></div><div><span class="fs28">ESTAMOS DE PRUEBAS</span></div>
								</div>
							</div>
						
						</div>
						</div></div>
					</div>
					<div style="float: left; width: 554px;">
						<div id="imCell_2" class="imGrid[0, 0]"><div id="imCellStyleGraphics_2"></div><div id="imCellStyle_2"><img id="imObjectImage_2" src="images/Lectura-4.png" title="" alt="" height="349" width="350" /></div></div>
					</div>
					
				</div>
				<div style="width: 1108px; float: left;">
					<div style="float: left; width: 554px;">
						<div id="imCell_3" class="imGrid[1, 1]"><div id="imCellStyleGraphics_3"></div><div id="imCellStyle_3"><div id="imHTMLObject_3" class="imHTMLObject" style="height: 655px; overflow: auto; text-align: center;"><!-- Date and Time.Date Time: START -->
						
						<span style="font-weight: bold; font-style: normal; font-family:Tahoma; font-size:72pt; color: #408000"><span class="imNow"></span></span>
						
						<!-- Date and Time.Date Time: END --></div></div></div>
					</div>
					<div style="float: left; width: 554px;">
						<div id="imCell_4" class="imGrid[1, 1]"><div id="imCellStyleGraphics_4"></div><div id="imCellStyle_4"><div id="imObjectMap_4"><script>
						_jq(document).ready(function() {
						   var lat,lng;
						   var url = 'http://maps.google.com/maps/api/geocode/json?address=Granada&sensor=false';
						   _jq.getJSON(url,function(data) {
						      lat = data.results[0].geometry.location.lat;
						      lng = data.results[0].geometry.location.lng;
						      _jq('#gmapsIframe4').html('<iframe id="gmapsIframe" frameborder="0" style="width: 350px; height:350px; overflow: hidden; border: none;" src="https://maps.google.es/maps?hl=es&amp;f=q&amp;source=embed&amp;geocode=&amp;q=Granada&amp;aq=&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=Granada&amp;z=15&amp;iwloc=&amp;layer=c&amp;output=svembed&amp;cbll='+lat+','+lng+'&amp;cbp=11,0,,0,4.64"></iframe>');
						   });
						});
						</script>
						<div id="gmapsIframe4"></div></div></div></div>
					</div>
					
				</div>
				
				<div id="imFooPad" style="height: 0px; float: left;">&nbsp;</div><div id="imBtMn"><a href="index.html">Página de inicio</a> | <a href="pagina-1.html">Página 1</a> | <a href="pagina-7.html">Nivel 2</a> | <a href="pagina-3.html">Página 3</a> | <a href="imsitemap.html">Mapa general del sitio</a></div>				  
				<div class="imClear"></div>
			</div>
			<div id="imFooter">
				
			</div>
		</div>
		<span class="imHidden"><a href="#imGoToCont" title="Lea esta página de nuevo">Regreso al contenido</a> | <a href="#imGoToMenu" title="Lea este sitio de nuevo">Regreso al menu principal</a></span>
		
		<noscript class="imNoScript"><div class="alert alert-red">Para utilizar este sitio tienes que habilitar JavaScript</div></noscript>
	</body>
</html>

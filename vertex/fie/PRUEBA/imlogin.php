<?php
require_once('res/x5engine.php');

$pa = imPrivateArea::getInstance();
$pa->admin_email = '';
if (isset($_POST['imUname']) && isset($_POST['imPwd'])) {
	$result = $pa->login($_POST['imUname'], $_POST['imPwd']);
	if ($result < 0) {
		header('Location: imlogin.php?loginstatus=' . $result);
		exit();
	}
	$page = $pa->getSavedPage() ? $pa->getSavedPage() : $pa->getLandingPage();
	$pa->sessionSafeRedirect($page);
}
?><!DOCTYPE html><!-- HTML5 -->
<html lang="es-ES" dir="ltr">
	<head>
		<title>Area privada - Nuevo proyecto</title>
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv="ImageToolbar" content="False" /><![endif]-->
		<meta name="generator" content="Incomedia WebSite X5 Professional 11.0.3.18 - www.websitex5.com" />
		<meta name="viewport" content="width=1111" />
		<link rel="stylesheet" type="text/css" href="style/reset.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/print.css" media="print" />
		<link rel="stylesheet" type="text/css" href="style/style.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/template.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="style/menu.css" media="screen" />
		<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="style/ie.css" media="screen" /><![endif]-->
		
		<script type="text/javascript" src="res/jquery.js?18"></script>
		<script type="text/javascript" src="res/x5engine.js?18"></script>
		
	</head>
	<body>
		<div id="imHeaderBg"></div>
		<div id="imFooterBg"></div>
		<div id="imPage">
			<div id="imHeader">
				<h1 class="imHidden">Area privada - Nuevo proyecto</h1>
				
			</div>
			<a class="imHidden" href="#imGoToCont" title="Salta el menu principal">Vaya al Contenido</a>
			<a id="imGoToMenu"></a><p class="imHidden">Menu Principal:</p>
			<div id="imMnMn" class="auto">
				<ul class="auto">
					<li id="imMnMnNode0">
						<a href="index.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página de inicio</span>
							</span>
						</a>
					</li><li id="imMnMnNode3">
						<a href="pagina-1.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página 1</span>
							</span>
						</a>
					</li><li id="imMnMnNode6">
						<span class="imMnMnFirstBg">
							<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 2<span class="imMnMnLevelImg"></span></span>
						</span>
						<ul class="auto">
							<li id="imMnMnNode12" class="imMnMnFirst">
								<a href="pagina-7.html">
									<span class="imMnMnBorder">
										<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página 7</span>
									</span>
								</a>
							</li><li id="imMnMnNode9" class="imMnMnMiddle">
								<span class="imMnMnBorder">
									<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 3</span>
								</span>
							</li><li id="imMnMnNode10" class="imMnMnMiddle">
								<span class="imMnMnBorder">
									<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 4</span>
								</span>
							</li><li id="imMnMnNode11" class="imMnMnLast">
								<span class="imMnMnBorder">
									<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 5</span>
								</span>
							</li>
						</ul>
					</li><li id="imMnMnNode5">
						<a href="pagina-3.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página 3</span>
							</span>
						</a>
					</li>
				</ul>
			</div>
			<div id="imContentGraphics"></div>
			<div id="imContent">
				<a id="imGoToCont"></a>
				<h2 id="imPgTitle" class="imTitleMargin">Area privada</h2>
<div style="height: 15px;">&nbsp;</div>
				<div id="imLoginDescription">Para acceder a esta sección del sitio web debe introducir sus detalles de inicio de sesión.</div>
				<div class="imLogin">
					<?php
						if (isset($_GET['loginstatus']) && $pa->messageFromStatusCode($_GET['loginstatus']) != '') {
							echo '<div class="alert alert-' . ($_GET['loginstatus'] >= 0 ? 'green' : 'red') . '">' . $pa->messageFromStatusCode($_GET['loginstatus']) . '</div>';
						}
					?>
					<form method="post" action="imlogin.php" id="imLoginForm">
						<div class="imLoginBlock">
							<label for="imUname"><span>Nombre usuario:</span></label><br />
							<input type="text" name="imUname" id="imUname" class="mandatory"><br />
						</div>
						<div class="imLoginBlock">
							<label for="imPwd"><span>Contraseña:</span></label><br />
							<input type="password" name="imPwd" id="imPwd" class="mandatory"><br />
						</div>
						<div class="imLoginBlock" style="text-align: right;">
						<input type="button" onclick="location.href='pagina-2.php'" value="Registrarse" />&nbsp;
							<input type="submit" value="Intro" class="imLoginSubmit">
						</div>
					</form>
					<script type="text/javascript">x5engine.boot.push(function() { x5engine.imForm.initForm('#imLoginForm', false, { showAll: true, offlineMessage: 'En la modalidad de prueba las páginas protegidas aparecen visualizadas sin el pedido de acceso. El área reservada es activada sólo con la publicación del sitio.' }); });</script>
				</div>
				<script>$(function () { $("#imUname").focus(); });</script>				  
				<div class="imClear"></div>
			</div>
			<div id="imFooter">
				
			</div>
		</div>
		<span class="imHidden"><a href="#imGoToCont" title="Lea esta página de nuevo">Regreso al contenido</a> | <a href="#imGoToMenu" title="Lea este sitio de nuevo">Regreso al menu principal</a></span>
		
		<noscript class="imNoScript"><div class="alert alert-red">Para utilizar este sitio tienes que habilitar JavaScript</div></noscript>
	</body>
</html>

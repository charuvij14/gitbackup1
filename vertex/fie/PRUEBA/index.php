<?php require_once("res/x5engine.php"); ?>
<!DOCTYPE html><!-- HTML5 -->
<html lang="es-ES" dir="ltr">
	<head>
		<title>Nuevo proyecto</title>
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv="ImageToolbar" content="False" /><![endif]-->
		<meta name="generator" content="Incomedia WebSite X5 Professional 11.0.3.18 - www.websitex5.com" />
		<meta name="viewport" content="width=1111" />
		<link rel="stylesheet" type="text/css" href="style/reset.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/print.css" media="print" />
		<link rel="stylesheet" type="text/css" href="style/style.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/template.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="style/menu.css" media="screen" />
		<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="style/ie.css" media="screen" /><![endif]-->
		<link rel="stylesheet" type="text/css" href="pcss/index.css" media="screen" />
		<script type="text/javascript" src="res/jquery.js?18"></script>
		<script type="text/javascript" src="res/x5engine.js?18"></script>
		
	</head>
	<body>
		<div id="imHeaderBg"></div>
		<div id="imFooterBg"></div>
		<div id="imPage">
			<div id="imHeader">
				<h1 class="imHidden">Nuevo proyecto</h1>
				
			</div>
			<a class="imHidden" href="#imGoToCont" title="Salta el menu principal">Vaya al Contenido</a>
			<a id="imGoToMenu"></a><p class="imHidden">Menu Principal:</p>
			<div id="imMnMn" class="auto">
				<ul class="auto">
					<li id="imMnMnNode0" class="imMnMnCurrent">
						<a href="index.php">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página de inicio</span>
							</span>
						</a>
					</li><li id="imMnMnNode3">
						<a href="pagina-1.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página 1</span>
							</span>
						</a>
					</li><li id="imMnMnNode6">
						<span class="imMnMnFirstBg">
							<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 2<span class="imMnMnLevelImg"></span></span>
						</span>
						<ul class="auto">
							<li id="imMnMnNode12" class="imMnMnFirst">
								<a href="pagina-7.html">
									<span class="imMnMnBorder">
										<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página 7</span>
									</span>
								</a>
							</li><li id="imMnMnNode9" class="imMnMnMiddle">
								<span class="imMnMnBorder">
									<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 3</span>
								</span>
							</li><li id="imMnMnNode10" class="imMnMnMiddle">
								<span class="imMnMnBorder">
									<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 4</span>
								</span>
							</li><li id="imMnMnNode11" class="imMnMnLast">
								<span class="imMnMnBorder">
									<span class="imMnMnTxt"><span class="imMnMnImg"></span>Nivel 5</span>
								</span>
							</li>
						</ul>
					</li><li id="imMnMnNode5">
						<a href="pagina-3.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página 3</span>
							</span>
						</a>
					</li>
				</ul>
			</div>
			<div id="imContentGraphics"></div>
			<div id="imContent">
				<a id="imGoToCont"></a>
				<div style="width: 1108px; float: left;">
					<div id="imCell_1" class="imGrid[0, 0]"><div id="imCellStyleGraphics_1"></div><div id="imCellStyle_1"><div id="imTextObject_1">
						<div class="text-tab-content"  id="imTextObject_1_tab0" style="text-align: left;">
							<div class="text-inner">
								<div style="text-align: center;"><b class="fs60"><span class="cf1">Bienvenidos a&nbsp;</span></b></div><div style="text-align: center;"><b class="fs60"><span class="cf1">esta página.</span></b></div>
							</div>
						</div>
					
					</div>
					</div></div>
				</div>
				<div style="width: 1108px; float: left;">
					<div style="float: left; width: 554px;">
						<div id="imCell_2" class="imGrid[1, 1]"><div id="imCellStyleGraphics_2"></div><div id="imCellStyle_2"><!-- search-tag dynObj_02 start -->
						<div id="dynObj_02">
						<?php
							$dynObj = new DynamicObject('dynObj_02');
							$dynObj->setDefaultText('<p>hola que tal</p>');
							$dynObj->loadFromFile(pathCombine(array($imSettings['general']['public_folder'], '/opt/php/iesmigueldecervantes/fie/DATA')));
							$pa = new ImPrivateArea();
							$user = $pa->who_is_logged();
							if ($user !== false && (in_array($user['uid'], array('p8eo4pwl', '51avol9j')) || in_array_field($user['groups'], array('p8eo4pwl')) || in_array($user['uid'], $imSettings['access']['admins']))) {
								if (isset($_POST['ObjectId']) && isset($_POST['PageId']) && $_POST['ObjectId'] == '2' && $_POST['PageId'] == '0') {
									$dynObj->setContent(imFilterInput($_POST['DefaultText']));
									$dynObj->saveToFile(pathCombine(array($imSettings['general']['public_folder'], '/opt/php/iesmigueldecervantes/fie/DATA')));
									exit("<script>window.top.location.href='" . basename($_SERVER['PHP_SELF']) . "';</script>");
								}
						?>
							<div class="dynamic-object-text staticText"><?php echo $dynObj->getContent() ?></div>
							<div class="dynamic-object-text dynamicText" style="display: none;">
								<form action="<?php echo basename($_SERVER['PHP_SELF']) ?>" method="post">
									<textarea name="DefaultText" style="width: 98%; height: 190px;"><?php echo $dynObj->getContent() ?></textarea>
									<input type="hidden" name="ObjectType" value="ObjectDynamicText" />
									<input type="hidden" name="ObjectId" value="2" />
									<input type="hidden" name="PageId" value="0" />
									<div style="text-align: center;">
										<input type="submit" value="Guardar">
										<input type="button" value="Cancelar" onclick="window.top.location.reload()">
									</div>
								</form>
								<script>
									x5engine.boot.push(function () { x5engine.cms.initialize('#dynObj_02', { autoGrow: false, css: ['style/style.css', 'pcss/index.css'], imageuploadurl: 'res/imageupload.php', availableFonts: ['Arial','Arial Black','Book Antiqua','Comic Sans MS','Courier New','Georgia','Impact','Lucida Console','Lucida Sans Unicode','Palatino Linotype','Tahoma','Times New Roman','Trebuchet MS','Verdana'], defaultFontSize: 10, defaultFontFamily: 'Tahoma'}); });
								</script>
							</div>
						<?php } else { echo $dynObj->getContent(); } ?>
						</div>
						<!-- search-tag dynObj_02 end -->
						</div></div>
					</div>
					<div style="float: left; width: 554px;">
						<div style="height: 262px;">&nbsp;</div>
					</div>
					
				</div>
				
				<div id="imFooPad" style="height: 0px; float: left;">&nbsp;</div><div id="imBtMn"><a href="index.php">Página de inicio</a> | <a href="pagina-1.html">Página 1</a> | <a href="pagina-7.html">Nivel 2</a> | <a href="pagina-3.html">Página 3</a> | <a href="imsitemap.html">Mapa general del sitio</a></div>				  
				<div class="imClear"></div>
			</div>
			<div id="imFooter">
				
			</div>
		</div>
		<span class="imHidden"><a href="#imGoToCont" title="Lea esta página de nuevo">Regreso al contenido</a> | <a href="#imGoToMenu" title="Lea este sitio de nuevo">Regreso al menu principal</a></span>
		
		<noscript class="imNoScript"><div class="alert alert-red">Para utilizar este sitio tienes que habilitar JavaScript</div></noscript>
	</body>
</html>

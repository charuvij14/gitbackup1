<?php
require_once('res/x5engine.php');

if (isset($_POST['imUname']) && isset($_POST['imPwd'])) {
	$pa = new imPrivateArea();
	if (!$pa->login($_POST['imUname'], $_POST['imPwd'])) {
		header('Location: ' . @$_GET['ref'] . '?err=1');
		exit();
	}
	$page = FALSE;
	if ($pa->saved_page())
		$page = $pa->saved_page();
	else
		$page = $pa->getLandingPage();
	exit('<!DOCTYPE html><html lang="es" dir="ltr"><head><title>Loading...</title><meta http-equiv="refresh" content="1; url=' . $page . '"></head><body><p style="text-align: center;">Loading...</p></body></html>');}
?>
<!DOCTYPE html><!-- HTML5 -->
<html lang="es" dir="ltr">
	<head>
		<title>Registro automático - Diseño plantilla</title>
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv="ImageToolbar" content="False" /><![endif]-->
		<meta name="generator" content="Incomedia WebSite X5 Evolution 10.0.6.31 - www.websitex5.com" />
		<meta name="viewport" content="width=1110" />
		<link rel="stylesheet" type="text/css" href="style/reset.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/print.css" media="print" />
		<link rel="stylesheet" type="text/css" href="style/style.css" media="screen,print" />
		<link rel="stylesheet" type="text/css" href="style/template.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="style/menu.css" media="screen" />
		<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="style/ie.css" media="screen" /><![endif]-->
		
		<script type="text/javascript" src="res/jquery.js?31"></script>
		<script type="text/javascript" src="res/x5engine.js?31"></script>
		<script type="text/javascript">
			x5engine.boot.push(function () { x5engine.utils.imPreloadImages(['menu/index_h.png','menu/formacion_h.png','menu/evaluacion-revisar_h.png','menu/departamento_h.png','menu/actividades_h.png','menu/inovacion_h.png','menu/autoevaluacion_h.png','menu/novedades_h.png','menu/lomce_h.png','menu/sub.png','menu/sub_h.png','menu/sub_f.png','menu/sub_f_h.png','menu/sub_l.png','menu/sub_l_h.png','menu/sub_m.png','menu/sub_m_h.png','res/imLoad.gif','res/imClose.png']); });
		</script>
		
	</head>
	<body>
		<div id="imHeaderBg"></div>
		<div id="imFooterBg"></div>
		<div id="imPage">
			<div id="imHeader">
				<h1 class="imHidden">Registro automático - Diseño plantilla</h1>
				
			</div>
			<a class="imHidden" href="#imGoToCont" title="Salta el menu principal">Vaya al Contenido</a>
			<a id="imGoToMenu"></a><p class="imHidden">Menu Principal:</p>
			<div id="imMnMn" class="auto">
				<ul class="auto">
					<li id="imMnMnNode0">
						<a href="index.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Página de inicio</span>
							</span>
						</a>
					</li><li id="imMnMnNode7">
						<span class="imMnMnFirstBg">
							<span class="imMnMnTxt"><span class="imMnMnImg"></span>Departamento<span class="imMnMnLevelImg"></span></span>
						</span>
						<div class="auto"><ul>
						<li id="imMnMnNode3" class="imMnMnFirst">
								<a href="funciones.html">
									<span class="imMnMnBorder">
										<span class="imMnMnTxt"><span class="imMnMnImg"></span>Funciones</span>
									</span>
								</a>
							</li><li id="imMnMnNode8" class="imMnMnLast">
								<a href="composicion.html">
									<span class="imMnMnBorder">
										<span class="imMnMnTxt"><span class="imMnMnImg"></span>Composición</span>
									</span>
								</a>
							</li>
						</ul></div>
					</li><li id="imMnMnNode10">
						<span class="imMnMnFirstBg">
							<span class="imMnMnTxt"><span class="imMnMnImg"></span>Actividades<span class="imMnMnLevelImg"></span></span>
						</span>
						<div class="auto"><ul>
						<li id="imMnMnNode11" class="imMnMnFirst">
								<a href="grupos-de-trabajo.html">
									<span class="imMnMnBorder">
										<span class="imMnMnTxt"><span class="imMnMnImg"></span>Grupos de Trabajo</span>
									</span>
								</a>
							</li><li id="imMnMnNode12" class="imMnMnMiddle">
								<a href="revista-digital.html">
									<span class="imMnMnBorder">
										<span class="imMnMnTxt"><span class="imMnMnImg"></span>Revista Digital</span>
									</span>
								</a>
							</li><li id="imMnMnNode13" class="imMnMnLast">
								<a href="formulacion-1.html">
									<span class="imMnMnBorder">
										<span class="imMnMnTxt"><span class="imMnMnImg"></span>Formulación</span>
									</span>
								</a>
							</li>
						</ul></div>
					</li><li id="imMnMnNode4">
						<a href="formacion.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Formación</span>
							</span>
						</a>
					</li><li id="imMnMnNode5">
						<a href="evaluacion-revisar.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Evaluación REVISAR</span>
							</span>
						</a>
					</li><li id="imMnMnNode14">
						<a href="inovacion.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Inovación</span>
							</span>
						</a>
					</li><li id="imMnMnNode15">
						<a href="autoevaluacion.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Autoevaluación</span>
							</span>
						</a>
					</li><li id="imMnMnNode16">
						<a href="novedades.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>Novedades</span>
							</span>
						</a>
					</li><li id="imMnMnNode17">
						<a href="lomce.html">
							<span class="imMnMnFirstBg">
								<span class="imMnMnTxt"><span class="imMnMnImg"></span>LOMCE</span>
							</span>
						</a>
					</li>
				</ul>
			</div>
			<div id="imContentGraphics"></div>
			<div id="imContent">
				<a id="imGoToCont"></a>
				<h2 id="imPgTitle" class="imTitleMargin">Registro automático</h2>
<div style="height: 15px;">&nbsp;</div>
				<div id="imLoginDescription">Para acceder a esta sección del sitio web debe introducir sus detalles de inicio de sesión.</div>
				<div id="imLogin">
					<?php					if (@$_GET['err'] == 1)
						echo '<div id="imLoginError">Nombre y/o password incorrecto</div>';
					?>
					<form method="post" action="imlogin.php">
						<div class="imLoginBlock">
							<label for="imUname"><span>Nombre usuario:</span></label><br />
							<input type="text" name="imUname" id="imUname"><br />
						</div>
						<div class="imLoginBlock">
							<label for="imPwd"><span>Contraseña:</span></label><br />
							<input type="password" name="imPwd" id="imPwd"><br />
						</div>
						<div class="imLoginBlock" style="text-align: right;">
							<input type="submit" value="Intro">
						</div>
					</form>
					<script>$(document).ready($("#imUname").focus());</script>
				</div>
				  
				<div class="imClear"></div>
			</div>
			<div id="imFooter">
				
			</div>
		</div>
		<span class="imHidden"><a href="#imGoToCont" title="Lea esta página de nuevo">Regreso al contenido</a> | <a href="#imGoToMenu" title="Lea este sitio de nuevo">Regreso al menu principal</a></span>
		
	</body>
</html>

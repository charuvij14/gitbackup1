<?php

/*****************
	GENERAL SETTINGS
*****************/
$imSettings['search']['general'] = array(
	'menu_position' => 'left',
	'defaultScope' => array(
		'0' => 'index.html',
		'3' => 'funciones.html',
		'4' => 'formacion.html',
		'5' => 'evaluacion-revisar.html',
		'8' => 'composicion.html',
		'11' => 'grupos-de-trabajo.html',
		'12' => 'revista-digital.html',
		'13' => 'formulacion-1.html',
		'14' => 'inovacion.html',
		'15' => 'autoevaluacion.html',
		'16' => 'novedades.html',
		'17' => 'lomce.html'
	),
	'extendedScope' => array(
	)
);

/*****************
	PRODUCTS SEARCH
*****************/
$imSettings['search']['products'] = array(
);

/*****************
	IMAGES SEARCH
*****************/
$imSettings['search']['images'] = array(
);

/*****************
	VIDEOS SEARCH
*****************/
$imSettings['search']['videos'] = array(
);

// End of file search.inc.php
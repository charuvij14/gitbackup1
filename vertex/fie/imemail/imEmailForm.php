<?php
if(substr(basename($_SERVER['PHP_SELF']), 0, 11) == "imEmailForm") {
	include '../res/x5engine.php';
	$form = new ImForm();
	$form->setField('DEJA TU MENSAJE', $_POST['imObjectForm_13_1'], '', false);
	$form->setField('Deja tu correo y nos ponemos en contacto contigo', $_POST['imObjectForm_13_2'], '', false);

	if(@$_POST['action'] != 'check_answer') {
		if(!isset($_POST['imJsCheck']) || $_POST['imJsCheck'] != 'jsactive' || (isset($_POST['imSpProt']) && $_POST['imSpProt'] != ""))
			die(imPrintJsError());
		$form->mailToOwner($_POST['imObjectForm_13_2'] != "" ? $_POST['imObjectForm_13_2'] : 'ascjoselillo@gmail.com', 'ascjoselillo@gmail.com', 'Correo del Departamento', 'Para cualquier sugerencia ponte en contacto con este departamento', false);
		@header('Location: ../index.html');
		exit();
	} else {
		echo $form->checkAnswer(@$_POST['id'], @$_POST['answer']) ? 1 : 0;
	}
}

// End of file
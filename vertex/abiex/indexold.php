<?
	include("servidor.php");
	$local_query  = "SHOW TABLE STATUS FROM " . $dbUsernameForm . " LIKE  'tlibros'";
	$result       = mysql_query($local_query);
	$showtable    = mysql_fetch_array($result);
	$update_table = strftime("%d-%m-%Y", strtotime($showtable['Update_time']));
	$date_today = strftime("%d-%m-%Y",time());
?>
<html>
<head>
<title>::: <?=$str_version?> : <?=$str_name_centro?> : Fondos de la Biblioteca :::</title>
<link rel="stylesheet" href="styles.css" type="text/css">

<script language="JavaScript">
function ver(url, width, height, scrollbar, resizable)
{
  scrollbar_str = scrollbar ? 'yes' : 'no';
  resizable_str = resizable ? 'yes' : 'no';
  window.open(url, '', 'width='+width+',height='+height+',scrollbars='+scrollbar_str+',resizable='+resizable_str);
}

function imprimir()
{
	window.print();
}

function cerrar()
{
	window.close();
}
</script>
</head>
<body leftmargin="10" topmargin="10" marginwidth="0" marginheight="0" bgcolor=#24304A text="#C0C0C0">

<?
function dohighlight($my_sentence, $word_to_highlight) {
	
    if (isset($my_sentence) and isset($word_to_highlight)){
		$my_sentence_minus = strtolower($my_sentence);
		$word_to_highlight_minus = strtolower($word_to_highlight);
		$my_sentence_control = str_replace($word_to_highlight_minus, "OK". $word_to_highlight . "OK", $my_sentence_minus);
		if ($my_sentence_control != $my_sentence_minus) {
			$pos = strpos($my_sentence_minus, $word_to_highlight_minus);
			return substr($my_sentence,0,$pos) . "<span class=Highlight>" . substr($my_sentence,$pos,strlen($word_to_highlight)) . "</span>" . substr($my_sentence, $pos+strlen($word_to_highlight), strlen($my_sentence)-$pos-strlen($word_to_highlight)+1);
		} else {
			return $my_sentence;
		}
    } else {
       return $my_sentence;
    }  
}

function formatear ($valor) {
	$cadena = "";
	switch ($valor) 
		{
			case ($valor<10):
				$cadena = "000".$valor;
				break;
			case ($valor<100):
				$cadena = "00".$valor;
				break;
			case ($valor<1000):
				$cadena = "0".$valor;
				break;
			case ($valor<10000):
				$cadena = "0".$valor;
				break;
			case ($valor<100000):
				$cadena = "0".$valor;
				break;
			case ($valor<1000000):
				$cadena = "0".$valor;
				break;

		}
	return($cadena);
}
			
function etiqueta ($param) {
	switch ($param)
		{
			case "titulo":
					return("T�tulo");
					break;	
			case "autor":
					return("Autor");
					break; 
			case "editorial":	       	
					return("Editorial");
					break; 
			default:
					return($param); 
					break;  
		}
}
  
$nompage = "index.php"; 
$nompageiud = "info.php";
$ipagesize =  trim($_REQUEST["ipagesize"]);
if (!isset($ipagesize) or $ipagesize=="") {  
$ipagesize = 10;
}

$titulo = trim($_REQUEST["titulo"]);
$autor = trim($_REQUEST["autor"]);
$editorial = trim($_REQUEST["editorial"]);


?>  
<div align="center">
<center>
<table border="0" cellpadding="0" cellspacing="0" width="750">
<tr><td>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
    <td colspan="2" align="center" class="Patilla"><?=$str_version?> Fondos de la Biblioteca</td>
</tr>
  <tr>
    <td colspan=2 height=6></td>
  </tr>
  <tr><td>&nbsp;<font color="#C0C0C0">Hoy es <? echo $date_today; ?>
  </font>
  </td><td align="right"><a href="javascript:imprimir()"><img src="imagenes/imprime.gif" alt="Imprimir" align=absmiddle border=0><b> 
    Imprimir</b></a>
	&nbsp;<a href="JavaScript:ver('ayuda.php', 420, 360, 1, 0);"><img src="imagenes/info.gif" alt="Ayuda" align=absmiddle border=0><b> Ayuda</b></a>
	&nbsp;<a href="JavaScript:cerrar()"><img src="imagenes/logout.gif" alt="Cerrar ventana" align=absmiddle border=0><b> Cerrar</b></a>
   	
  </td></tr>
</table>

<table width="100%" cellspacing="0" cellpadding="0">
<tr><td><img src='imagenes/search.gif' align=absmiddle><b>&nbsp;<font color="#C0C0C0">Criterios de b�squeda :</font></b><font color="#C0C0C0">
	</font>
</td><td align="right" class="Font8Orange"><b>�ltima actualizaci�n del fondo: <? echo $update_table; ?></b></td></tr></table>

<table width="100%" cellspacing="0" cellpadding="2" class="Patilla2">
<form name="formulario" method="post" action="<? echo $nompage; ?>">
<tr>
<td align="right"><? echo etiqueta("titulo"); ?>&nbsp;</td>
<td><input  name="titulo" value="<? echo $titulo; ?>" class=TextBox size="40"></td>
<td align="right"><? echo etiqueta("autor"); ?>&nbsp;</td>
<td><input  name="autor" value="<? echo $autor; ?>" class=TextBox size="30"></td>  	
<td align="right"><? echo etiqueta("editorial"); ?>&nbsp;</td>
<td><input  name="editorial" value="<? echo $editorial; ?>" class=TextBox size="15"></td>
<td><input type="hidden" name="ipagesize" value="<? echo $ipagesize; ?>">
<input type="submit"  value="buscar" name="boton" align="absbottom" class=boxlook>

</td>
</tr>    
 </form>
</table>
<?    
$page = $_GET["page"];
if (!isset($page) or $page=="") {
	$inicio = 0; //N�mero del primer registro a mostrar
	$ipagecurrent = 1;
} else {
	$inicio = ($page - 1) * $ipagesize;
	$ipagecurrent = $page;
}

$field = $_REQUEST["field"];
$sort = $_REQUEST["sort"];

if (!isset($sort)) {
	$sort = "asc";
}

if (!isset($field) or $field=="") {
	// No se ha definido campo de ordenaci�n
	$strsort = "";
	$sort = "";
} else {
	if ($sort=="asc") {
		$strsort = " ORDER BY " . strtoupper($field) . " ASC";
	} else {
		$strsort = " ORDER BY " . strtoupper($field) . " DESC";
	}
}


$query = "select * from abiex WHERE 1=1";
if ($titulo!="") $query = $query . " AND titulo like '%" . $titulo . "%'";
if ($autor!="") $query = $query . " AND autor like '%" . $autor . "%'";
if ($editorial!="") $query = $query . " AND editorial like '%" . $editorial . "%'" ;
//echo $query;


$rs = mysql_query($query);
$num_total_registros = mysql_num_rows($rs);
$total_paginas = ceil($num_total_registros / $ipagesize);

if ($num_total_registros==0) {
	$ipagecurrent=0;
	$inicio = -1;
}
if ($num_total_registros < ($inicio+$ipagesize)) { 
	$final = $num_total_registros;
} else {
	$final = $inicio+$ipagesize;
}


?>
<table width="100%" cellspacing="2" cellpadding="1">
<tr><td><font color="#C0C0C0">
<? 
if ($total_paginas!=0) {

	$str_param = "";
		
	foreach ($_REQUEST as $key => $value) {
		if ($key=="titulo" or $key=="autor" or $key=="editorial" or $key=="field" or $key=="sort") {
			if (isset($value) and $value!="") {
				$str_param = $str_param . "&". $key . "=" . urlencode($value);
			}
		}
	}
	echo "N�m. de registros por p�gina: ";
		
	if ($ipagesize==10) {
		echo "<b>[10]</b> , ";
	} else {
		echo "<a href=\"" . $nompage . "?page=1&ipagesize=10$str_param\"><b>[10]</b></a> , ";
	}
	
	if ($ipagesize==20) {
		echo "<b>[20]</b> , ";
	} else {
		echo "<a href=\"" . $nompage . "?page=1&ipagesize=20$str_param\"><b>[20]</b></a> , ";
	}

	if ($ipagesize==50) {
		echo "<b>[50]</b> , ";
	} else {
		echo "<a href=\"" . $nompage . "?page=1&ipagesize=50$str_param\"><b>[50]</b></a> , ";
	}
	
	if ($ipagesize==100) {
		echo "<b>[100]</b>";
	} else {
		echo "<a href=\"" . $nompage . "?page=1&ipagesize=100$str_param\"><b>[100]</b></a>";
	}
}

?>
</font>
</td>
<td align="right"> <font color="#C0C0C0">P�gina:
<? 
	if ($total_paginas!=0) {
	
	if ($ipagecurrent==1) {
		echo "<img src='imagenes/beginx.gif' align=absmiddle border=0>&nbsp";
	} else {
		echo "<a href='$nompage?page=1&ipagesize=$ipagesize". $str_param . "'><img src='imagenes/begin.gif' align=absmiddle border=0 alt='Primera'></a>&nbsp";
	}	
	
	if ($ipagecurrent < 11) {
		echo "<img src='imagenes/pageeupx.gif' align=absmiddle border=0>&nbsp";
	} else {
		echo "<a href='$nompage?page=" . ($ipagecurrent-10) . "&ipagesize=$ipagesize". $str_param . "'><img src='imagenes/pageeup.gif' align=absmiddle border=0 alt='Anterior x10'></a>&nbsp";
	}	

	if ($ipagecurrent ==1) {
		echo "<img src='imagenes/pageupx.gif' align=absmiddle border=0> ";
	} else {
		echo "<a href='$nompage?page=" . ($ipagecurrent-1) . "&ipagesize=$ipagesize". $str_param . "'><img src='imagenes/pageup.gif' align=absmiddle border=0 alt='Anterior'></a> ";
	}	

	echo formatear($ipagecurrent) .  " de " . formatear($total_paginas) .  "&nbsp;" ;
	
	if ($ipagecurrent == $total_paginas) {
		echo "<img src='imagenes/pagedownx.gif' align=absmiddle border=0>&nbsp";
	} else {
		echo "<a href='$nompage?page=" . ($ipagecurrent+1) . "&ipagesize=$ipagesize". $str_param . "'><img src='imagenes/pagedown.gif' align=absmiddle border=0 alt='Siguiente'></a>&nbsp";
	}	

	if ($ipagecurrent > ($total_paginas-10)) {
		echo "<img src='imagenes/pageedownx.gif' align=absmiddle border=0>&nbsp";
	} else {
		echo "<a href='$nompage?page=" . ($ipagecurrent+10) . "&ipagesize=$ipagesize". $str_param . "'><img src='imagenes/pageedown.gif' align=absmiddle border=0 alt='Siguiente x10'></a>&nbsp";
	}	
	
	if ($ipagecurrent == $total_paginas) {
		echo "<img src='imagenes/endx.gif' align=absmiddle border=0>&nbsp";
	} else {
		echo "<a href='$nompage?page=" . ($total_paginas) . "&ipagesize=$ipagesize". $str_param . "'><img src='imagenes/end.gif' align=absmiddle border=0 alt='�ltima'></a>&nbsp";
	} 	
	
        } else echo "0";
	

?>

</font>

</td>
</tr>
<tr>
<td> 
<font color="#C0C0C0">Registros mostrados&nbsp;<b><? echo strval($inicio+1) . "-" . strval($final) . "</b> de <b>" . $num_total_registros; ?> </b>totales


</font>


</td>
<td align="right">
<font color="#C0C0C0">
<? 
if (($titulo!="") or ($autor!="") or ($editorial!="")) { 
	echo "Filtro aplicado a&nbsp;<b>";

	if ($titulo!="") {
		echo etiqueta ("titulo") . "&nbsp;";
	}

	if ($autor!="") {
		echo etiqueta ("autor") . "&nbsp;";
	}

	if ($editorial!="") {
		echo etiqueta ("editorial") . "&nbsp;";
	}
	echo "</b>";
} // Fin de IF del filtro
echo "&nbsp;";
if ($strsort !="") {
		if ($sort=="asc") {
			echo "Ordenaci�n ascendente por <b>" . etiqueta($field) . "</b>";
		} else {
			echo "Ordenaci�n descendente por <b>" . etiqueta($field) . "</b>";
		}
}
?>
</font>
</td>
</tr>
</table>   

<table cellSpacing=0 cellPadding=0 border=0 width="100%">
<tr><td background=imagenes/tile1.gif bgColor=#efebf7 width="13"><img height=20 src="imagenes/top_left.gif" width=13></td>
<?



$str_param2 = "";
	foreach ($_REQUEST as $key => $value) {
		//if ($key!="boton" and $key!="field" and $key!="sort" and $key!="page" and $key!="ipagesize") {
		if ($key=="titulo" or $key=="autor" or $key=="editorial") {
			if (isset($value) and $value!="") {
				$str_param2 = $str_param2 . "&". $key . "=" . urlencode($value);
			}
		}
	}
$query = $query . $strsort . " limit " . $inicio . "," . $ipagesize;
 
$campos[0]= "titulo";
$campos[1]= "autor";
$campos[2]= "editorial";

for ($i=0;$i<=2; $i++){
	if ($i==0) {
		$anchura = "50%";
	} else {
		$anchura = "25%";
	}
	echo "<td background=imagenes/tile1.gif  bgColor=#efebf7 width=\"$anchura\"><b>";
	if ($field == $campos[$i]) {
		if ($sort=="asc") {
			echo "<a href=\"" . $nompage . "?page=" . $ipagecurrent . "&ipagesize=" . $ipagesize . $str_param2 . "&field=" . $campos[$i] . "&sort=desc\">";
			echo "<img src=\"imagenes/nosort_desc2.gif\" border=0   alt=\"Asc\" width=14 height=14 align=absbottom>";
		} else {
			echo "<a href=\"" . $nompage . "?page=" . $ipagecurrent . "&ipagesize=" . $ipagesize . $str_param2 . "&field=" . $campos[$i] . "&sort=asc\">";
			echo "<img src=\"imagenes/sort_desc2.gif\" border=0   alt=\"Desc\" width=14 height=14 align=absbottom>";
		}
	} else {
			echo "<a href=\"" . $nompage . "?page=" . $ipagecurrent . "&ipagesize=" . $ipagesize . $str_param2 . "&field=" . $campos[$i] . "&sort=asc\">";
	}
	echo etiqueta($campos[$i]). "</a></b></td>";
	
} // for 
?>
<td background="imagenes/tile1.gif"  bgColor=#efebf7><img height=1  src="imagenes/spacer.gif" width=8></td>
<td background=imagenes/tile1.gif bgColor=#efebf7 width="15"><img height=20 src="imagenes/top_right.gif" width=15></td></tr>
<?
//echo $query;
$rs = mysql_query($query);
if ($inicio>-1) {
$rows_table = 0;
while ($fila = mysql_fetch_object($rs)){
	if ($rows_table % 2 == 0) {
		$color_back = "bgColor='#ffffff'";
	} else {
		$color_back = "bgColor='#FFFFDD'";
	}
	$rows_table++;
	echo "<tr><td><img height=1  src=\"imagenes/spacer.gif\" width=1></td></tr><tr><td $color_back>&nbsp;</td>";
    for ($i=0;$i<=2; $i++){
		echo "<td valign=top $color_back>";
		switch ($campos[$i]) {
			case "titulo":
				if (trim($fila->prestado)!="True") 
				  { 
 				    echo "<img src=\"imagenes/btnVide.gif\" border=0>&nbsp;"; 
				  }
				else 
				  { 
			   	    echo "<img src=\"imagenes/true.gif\" border=0>&nbsp;"; 
				  }
				echo "<a href=\"javascript:ver('info.php?ejemplar=$fila->ejemplar',470,350,0,1)\">";			 	
				echo "&nbsp;" . dohighlight($fila->titulo, $titulo) . "</a>";
				break;
			case "autor":
				echo "&nbsp;" . dohighlight($fila->autor, $autor);
  			 	break;
			case "editorial":
				echo "&nbsp;" . dohighlight($fila->editorial, $editorial);
				break;
		} // fin del switch
	} // fin del for
	   	echo "</td><td $color_back><img height=1  src=\"imagenes/spacer.gif\" width=8></td>";
		echo "<td $color_back>&nbsp;</td></tr>";
} // fin del while
}
mysql_close($enlace);
?>
<tr><td colspan="6"><img height=1  src="imagenes/spacer.gif" width=1></td></tr>
<tr><td><img height=1  src="imagenes/spacer.gif" width=1></td></tr>

                                                     
<tr><td background=imagenes/tile1.gif bgColor=#efebf7 width="13"><img  src="imagenes/bottom_left.gif" width="13"></td>
<td bgColor=#ffffff background=imagenes/bottom_center.gif colspan="4">&nbsp;</td>
<td background=imagenes/tile1.gif bgColor=#efebf7 width="15"><img src="imagenes/bottom_right.gif" width="15"></td>	
</tr>
</table>
</td></tr>
<tr><td>

</td></tr>
<tr><td>


<table border=0 cellpadding=0 cellspacing=0 width='100%'>
<tr><td>
	<p align="center">&nbsp;
<a href="<?=$str_email_centro;?>" target="_blank"><img border="0" src="imagenes/logo_ies.gif" width="215" height="87" alt="<?=$str_name_centro;?>"></a></td>
  <td align="right">&nbsp;</td>
</tr>
</table>

</td></tr>
</table>
</td></tr></table>
<a href="http://iescastillodluna.juntaextremadura.net/abiexweb" target="_blank"><?=$str_version?></a>
</center>
</div>
</body>
</html>


<? include("servidor.php") ?>
<html>
<head>
<title><?=$str_version?> :: Consulta de la biblioteca v�a Web</title>
<LINK REL="stylesheet" HREF="styles.css" TYPE="text/css">
</head>

<body   leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor=#24304A text="#C0C0C0" >

<table width="100%" cellspacing="0" cellpadding="0">

  <tr> 

    <td  height="24" bgcolor="#0066CC" align="Left" valing="middle" class="Font9BoldWhite">&nbsp;<img src="imagenes/info.gif" width="16" height="16" align="absmiddle" alt="Ayuda">&nbsp;Ayuda</td>

    <td bgcolor="#0066CC" align="right"></td>
  </tr>

</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td width="4%"><font color="#C0C0C0"><b><img border="0" src="imagenes/abies2.gif"></b></font></td>
          <td width="96%"><font color="#008080"><b><?=$str_version?></b></font><font color="#C0C0C0"> es una aplicaci�n que complementa a Abies y permite  la consulta v�a web del cat&aacute;logo de libros de la biblioteca del centro.</font></td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><font color="#00FFFF"><b>Paginaci�n:</b> </font>
			<font color="#C0C0C0"> <br>En la esquina superior derecha, bajo la barra de b&uacute;squeda, encontrar&aacute;s la botonera para moverte entre las p&aacute;ginas del listado.<br>
              1) Pulsa sobre sus distintos botones: Primera, Anteriorx10, Anterior, Siguiente, Siguientex10 y &Uacute;ltima.<br>
          2) Si deseas ampliar o reducir el n&uacute;mero de ejemplares a visualizar en cada p&aacute;gina, haz clic sobre el n&uacute;mero deseado: 10, 20, 50 &oacute; 100.<br>
		  </font></td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><font color="#00FFFF"><b>B�squeda:&nbsp;</b></font><font color="#C0C0C0"><br>
            1) Introduce en las casillas <b>Titulo</b>, <b> Autor </b> y/o <b> Editorial
            </b> la informaci�n a&nbsp;
            buscar.<br>
            2) Clic en el bot�n <b> Buscar</b> o pulsa la tecla <b>&lt;enter&gt;</b><br>
            3) Se muestra el mensaje <b>Filtro aplicado&nbsp;
            a ...</b>&nbsp; para indicar el campo o campos en los que se han establecido los
            criterios de b�squeda.<br>
            4) En el listado de ejemplares se muestra en video resaltado el campo&nbsp;coincidente con el criterio de b�squeda.<br>
            5) Para regresar a los fondos completos borra la informaci�n contenida en&nbsp;
            los criterios de b�squeda y vuelve a pulsar el bot�n &quot;<b>Buscar</b>&quot;.</font></td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><font color="#00FFFF"><b>Ordenaci�n:</b></font><font color="#C0C0C0"><br>
            1) Clic sobre un campo (<b>T�tulo, Autor </b> o<b> Editorial</b>) en el&nbsp;
            encabezado del listado para realizar&nbsp; una ordenaci�n en sentido ascendente o
            descendente por ese campo.<br>
            Sobre el encabezado se muestra el tipo de ordenaci�n que hemos&nbsp;
            configurado para el listado.</font></td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><font color="#00FFFF"><b>Informaci�n sobre el ejemplar:</b></font><font color="#C0C0C0"><br>
            1) Pulsa sobre el <b>T�tulo </b> de un ejemplar.&nbsp;<br>
            2) Se desplegar� una ventana emergente mostrando la principal informaci�n&nbsp;
            del mismo.</font></td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><font color="#00FFFF"><b>Otros:</b></font><font color="#C0C0C0"><br>
            Se indica la fecha de la <b>�ltima actualizaci�n</b> realizada en la Base de&nbsp;
            datos que contiene los fondos de la biblioteca. 
			</font> </td>
        </tr>
    </table></td>
  </tr>
</table>





&nbsp;



<table width="100%" cellspacing="0" cellpadding="0">

  <tr> 
    <td height="30" bgcolor="#98B4D4" align="right"><a href="javascript:window.close()"><img src="imagenes/logout.gif" width="14" height="14" border="0"> Cerrar</td>
  </tr>
</table>

</body>

</html>

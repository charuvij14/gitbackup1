<?php

/**
 * @package             Joomla
 * @subpackage          CW Traffic Count Plugin
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');
jimport('joomla.plugin.plugin');
include_once JPATH_ADMINISTRATOR . '/components/com_coalawebtraffic/helpers/iptools.php';
include_once JPATH_ADMINISTRATOR . '/components/com_coalawebtraffic/helpers/location.php';
if(!class_exists('Cwbrowser')){
    include_once JPATH_ADMINISTRATOR . '/components/com_coalawebtraffic/helpers/cwbrowser.php';
}

class plgSystemCwtrafficCount extends JPlugin {

    public function __construct(&$subject, $config) {
        parent::__construct($subject, $config);

        // load the CoalaWeb Traffic language file
        $lang = JFactory::getLanguage();
        if ($lang->getTag() != 'en-GB') {
            // Loads English language file as fallback (for undefined stuff in other language file)
            $lang->load('plg_system_cwtrafficcount', JPATH_ADMINISTRATOR, 'en-GB');
        }
        $lang->load('plg_system_cwtrafficcount', JPATH_ADMINISTRATOR, null, 1);

        //Load the component language strings
        if ($lang->getTag() != 'en-GB') {
            $lang->load('com_coalawebtraffic', JPATH_ADMINISTRATOR, 'en-GB');
        }
        $lang->load('com_coalawebtraffic', JPATH_ADMINISTRATOR, null, 1);
    }

    public function onAfterInitialise() {
        $app = JFactory::getApplication();
        $comParams = JComponentHelper::getParams('com_coalawebtraffic');

        if ($app->isSite()) {

            if (version_compare(JVERSION, '3.0', '>')) {
                $siteOffset = JFactory::getApplication()->getCfg('offset');
            } else {
                $config = JFactory::getConfig();
                $siteOffset = $config->getValue('config.offset');
            }

            //Let's get the visitors Browser and OS info
            //I have added CW to stop conflicts with other developers who refuse to check it the Browser class has already been called.
            $ua = new Cwbrowser();
            if (!empty($ua)) {
                $this->browser = $ua->getBrowser();
                $this->bversion = $ua->getVersion();
                $platform = $ua->getPlatform();
            } else {
                $this->browser = JText::_('COM_CWTRAFFIC_LOCATION_UNKNOWN');
                $this->bversion = JText::_('COM_CWTRAFFIC_LOCATION_UNKNOWN');
                $platform = JText::_('COM_CWTRAFFIC_LOCATION_UNKNOWN');
            }

            if (isset($_SERVER['HTTP_REFERER']) && $_SERVER['HTTP_REFERER'] != "") {
                $referer = $_SERVER['HTTP_REFERER'];
            } else {
                $referer = JText::_('COM_CWTRAFFIC_UNKNOWN');
            }

            //Current date time
            $dtnow = JFactory::getDate('now', $siteOffset);
            $now = $dtnow->toUnix(true);

            //How long should an IP be locked for.
            $locktime = $comParams->get('locktime', 60) * 60;

            //Get the users IP
            $this->ip = CoalawebtrafficHelperIptools::getUserIp();

            //Start our database queries
            $db = JFactory::getDbo();

            //Check the Knownips table for IPs that are published and shouldn't be counted
            $query = $db->getQuery(true);
            $query->select('ip');
            $query->from($db->quoteName('#__cwtraffic_knownips'));
            $query->where('state = 1 AND count = 0');
            $db->setQuery($query);
            if (version_compare(JVERSION, '3.0', '>')) {
                $ipTable = $db->loadColumn();
            } else {
                $ipTable = $db->loadResultArray();
            }

            //Check the Known Ips returned from above against current user IP
            $iplock = 0;
            if (!empty($ipTable)) {
                $iplock = CoalawebtrafficHelperIptools::ipinList($this->ip, $ipTable);
            } else {
                $iplock = 0;
            }

            //Check the Knownips table for Bots that are published and shouldn't be counted
            $query = $db->getQuery(true);
            $query->select('botname');
            $query->from($db->quoteName('#__cwtraffic_knownips'));
            $query->where('state = 1 AND count = 0');
            $db->setQuery($query);
            if (version_compare(JVERSION, '3.0', '>')) {
                $botTable = $db->loadColumn();
            } else {
                $botTable = $db->loadResultArray();
            }
            
            $agent = $_SERVER['HTTP_USER_AGENT'];
            
            $bot = 0;
            
            if (!empty($agent) && !empty($botTable)) {
                foreach ($botTable as $bot_value) {
                    if (!empty($bot_value)) {
                        if (stripos($agent, $bot_value) !== false) {
                            $bot = 1;
                            break;
                        }
                    }
                }
            } else {
                $bot = 0;
            }

            //Check with Project Honeypot
            if ($comParams->get('honeypot')) {
                $hp = self::checkHoneypot($this->ip);
            } else {
                $hp = false;
            }

            // Check if IP already exists and reload lock expired
           
            $query = $db->getQuery(true);
            $query->select('count(*)');
            $query->from($db->quoteName('#__cwtraffic'));
            $query->where('ip = ' . $db->quote($this->ip));
            $query->where('tm + ' . $db->quote($locktime) . '>' . $db->quote($now));
            $db->setQuery($query);
            $items = $db->loadResult();

            // If all is good lets count the visitor.
            if (empty($items) AND $bot == 0 AND $iplock == 0 AND $hp == false) {
                $query = $db->getQuery(true);
                $query->insert($db->quoteName('#__cwtraffic'));
                $query->columns('tm, ip, browser, bversion, platform, referer');
                $query->values($db->quote($now) . ',' . $db->quote($this->ip) . ',' . $db->quote($this->browser) . ',' . $db->quote($this->bversion) . ',' . $db->quote($platform) . ',' . $db->quote($referer));
                $db->setQuery($query);
                $db->query();
            }
            // Update countries and cities for visitors
            CoalawebtrafficHelperLocation::location_update();
            return $query;
        }
    }

    private function checkHoneypot($reqip) {
        $comParams = JComponentHelper::getParams('com_coalawebtraffic');
        $honeypotKey = $comParams->get('honeypot_key');
        $minThreat = $comParams->get('honeypot_min');
        $maxAge = $comParams->get('honeypot_max');
        $suspicious = $comParams->get('honeypot_sus');
        $searchEng = $comParams->get('honeypot_seng');
        $block = false;

        // Make sure we have an HTTP:BL  key set
        if (empty($honeypotKey)) {
            return;
        }

        // Get the IP address
        if ($reqip == '0.0.0.0') {
            return false;
        }

        if (strpos($reqip, '::') === 0) {
            $reqip = substr($reqip, strrpos($reqip, ':') + 1);
        }

        // No point continuing if we can't get an address, right?
        if (empty($reqip)) {
            return false;
        }

        // IPv6 addresses are not supported by HTTP:BL yet
        if (strpos($reqip, ":")) {
            return false;
        }

        $lookup = $honeypotKey . '.' . implode('.', array_reverse(explode ('.', $reqip ))) . '.dnsbl.httpbl.org';
        $result = explode( '.', gethostbyname($lookup));

        if (!empty($result)) {

            if ($result[0] != 127) {
                return; // Make sure it's a valid response
            }
            if (($suspicious && ($result[3] == 1)) || ($result[3] >= 2)) {
                $block = true; //Is supicious or block if requested next block anything with 2 or above. 
            }

            $block = $block && ($result[1] <= $maxAge); //Check age against extension options
            $block = $block && ($result[2] >= $minThreat); //Check max threat against extension options.
            
            if ($searchEng && $result[3] == 0) {
                $block = true; //If requested block search engine regardless of max age and min threat.
            }
        }
        return $block;
    }

}

<?php

/**
 * @package             Joomla
 * @subpackage          CW Traffic Who is Online Plugin
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');
include_once JPATH_ADMINISTRATOR . '/components/com_coalawebtraffic/helpers/iptools.php';
include_once JPATH_ADMINISTRATOR . '/components/com_coalawebtraffic/helpers/location.php';

class plgSystemCwtrafficOnline extends JPlugin {

    public function __construct(&$subject, $config) {
        parent::__construct($subject, $config);

        // load the CoalaWeb Traffic language file
        $lang = JFactory::getLanguage();
        if ($lang->getTag() != 'en-GB') {
            // Loads English language file as fallback (for undefined stuff in other language file)
            $lang->load('plg_system_cwtrafficonline', JPATH_ADMINISTRATOR, 'en-GB');
        }
        $lang->load('plg_system_cwtrafficonline', JPATH_ADMINISTRATOR, null, 1);

        //Load the component language strings
        if ($lang->getTag() != 'en-GB') {
            $lang->load('com_coalawebtraffic', JPATH_ADMINISTRATOR, 'en-GB');
        }
        $lang->load('com_coalawebtraffic', JPATH_ADMINISTRATOR, null, 1);
    }

    public function onAfterInitialise() {
        $app = JFactory::getApplication();

        if ($this->is_bot()) {
            return;
        }

        // Only render for the front end.
        if ($app->getName() !== 'site') {
            return;
        }

        //Get the users IP
        $stringIp = CoalawebtrafficHelperIptools::getUserIp();

        $intIp = ip2long($stringIp);

        $db = JFactory::getDbo();

        //Check the Who is Online table for IPs
        $query = $db->getQuery(true);
        $query->select('count(*)');
        $query->from($db->quoteName('#__cwtraffic_whoisonline'));
        $query->where('ip = ' . $db->quote($intIp));
        $db->setQuery($query);
        $inDB = $db->loadResult();

        if (!$inDB) {
            if (isset($_COOKIE['cwGeoData'])) {
                $cookie = $_COOKIE['cwGeoData'];
            }

            if (!empty($cookie)) {
                // A "geoData" cookie has been previously set by the script, so we will use it
                // Always escape any user input, including cookies:
                list($city, $countryName, $countryCode) = explode('|', strip_tags($cookie));
            } else {

                //Lets get some info on our IP
                $details = CoalawebtrafficHelperLocation::whoisonlineUpdate($stringIp);

                $city = !empty($details['city']) ? $details['city'] : "unknown city";

                $countryName = !empty($details['country_name']) ? $details['country_name'] : "unknown country";

                $countryCode = !empty($details['country_code']) ? $details['country_code'] : "xx";

                // Setting a cookie with the data, which is set to expire in a month:
                setcookie('cwGeoData', $city . '|' . $countryName . '|' . $countryCode, time() + 60 * 60 * 24 * 30, '/');
            }

            $query = $db->getQuery(true);
            $query->insert($db->quoteName('#__cwtraffic_whoisonline'));
            $query->columns('ip,country_name, country_code, city,  dt');
            $query->values($db->quote($intIp) . ',' . $db->quote($countryName) . ',' . $db->quote($countryCode) . ',' . $db->quote($city) . ',NOW()');
            $db->setQuery($query);
            $db->query();
        } else {
            // If the visitor is already online, just update the dt value of the row:
            $query = $db->getQuery(true);
            $query->update($db->quoteName('#__cwtraffic_whoisonline'));
            $query->set('dt = NOW()');
            $query->where('ip = ' . $db->quote($intIp));
            $db->setQuery($query);
            $db->query();
        }
        $query = $db->getQuery(true);
        $query->delete($db->quoteName('#__cwtraffic_whoisonline'));
        $query->where("dt < SUBTIME(NOW(),'0 0:10:0')");
        $db->setQuery($query);
        $db->query();
    }

    static function is_bot() {
        /* This function will check whether the visitor is a search engine robot */
        $botlist = array("Teoma", "alexa", "froogle", "Gigabot", "inktomi",
            "looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory",
            "Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot",
            "crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp",
            "msnbot", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz",
            "Baiduspider", "Feedfetcher-Google", "TechnoratiSnoop", "Rankivabot",
            "Mediapartners-Google", "Sogou web spider", "WebAlta Crawler", "TweetmemeBot",
            "Butterfly", "Twitturls", "Me.dium", "Twiceler");

        foreach ($botlist as $bot) {
            if (strpos($_SERVER['HTTP_USER_AGENT'], $bot) !== false) {
                return true; // Is a bot
            }
        }

        return false; // Not a bot
    }

}

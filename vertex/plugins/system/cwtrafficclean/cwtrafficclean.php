<?php

/**
 * @package             Joomla
 * @subpackage          CW Traffic Clean Plugin
 * @author              Steven Palmer
 * @author url          http://coalaweb.com
 * @author email        support@coalaweb.com
 * @license             GNU/GPL, see /files/en-GB.license.txt
 * @copyright           Copyright (c) 2015 Steven Palmer All rights reserved.
 *
 * CoalaWeb Traffic is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.helper');
jimport('joomla.plugin.plugin');

class plgSystemCwtrafficClean extends JPlugin {

    public function __construct(&$subject, $config) {
        parent::__construct($subject, $config);

        // load the CoalaWeb Traffic language file
        $lang = JFactory::getLanguage();
        if ($lang->getTag() != 'en-GB') {
            // Loads English language file as fallback (for undefined stuff in other language file)
            $lang->load('plg_system_cwtrafficclean', JPATH_ADMINISTRATOR, 'en-GB');
        }
        $lang->load('plg_system_cwtrafficclean', JPATH_ADMINISTRATOR, null, 1);

        //Load the component language strings
        if ($lang->getTag() != 'en-GB') {
            $lang->load('com_coalawebtraffic', JPATH_ADMINISTRATOR, 'en-GB');
        }
        $lang->load('com_coalawebtraffic', JPATH_ADMINISTRATOR, null, 1);
    }

    public function onAfterInitialise() {
        $comParams = JComponentHelper::getParams('com_coalawebtraffic');
        $dbClean = $comParams->get('db_clean');
        $dbKeep = $comParams->get('db_keep');
        $db = JFactory::getDbo();

        //Let check the lock time maybe the user wants loads of duplicates.
        $locktime = $comParams->get('locktime');
        if ($locktime > 0) {
            //Lets get rid of duplicates with in 10 seconds of each other (Probably Bots!)
            //We will do it in 50 record chunks to keep the queries fast.
            $query = $db->getQuery(true);
            $query->select('id, LEFT( tm, 9 ) , ip, count(*)')
                    ->from($db->quoteName('#__cwtraffic'))
                    ->group('LEFT( tm, 9 ) , ip')
                    ->having('count(*) > 1');
            $db->setQuery($query, 0, 50);
            $dups = $db->loadObjectList();

            //If we have some lets delete them and only leave 1
            if ($dups) {
                foreach ($dups as $row) {
                    $query = $db->getQuery(true);
                    $query->from($db->quoteName('#__cwtraffic'));
                    $query->delete();
                    $query->where('id = ' . $db->quote($row->id));
                    $db->setQuery($query);
                    $db->query();
                }
            }
        }

        if ($dbClean) {

            //Prepare Total table
            $query = $db->getQuery(true);
            $query->select('count(*)');
            $query->from($db->quoteName('#__cwtraffic_total'));
            $db->setQuery($query);
            $currenttotal = $db->loadResult();

            if (empty($currenttotal)) {
                $query = $db->getQuery(true);

                $columns = array('tcount');
                $values = array(0);

                $query
                        ->insert($db->quoteName('#__cwtraffic_total'))
                        ->columns($db->quoteName($columns))
                        ->values(implode(',', $values));

                $db->setQuery($query);
                $db->query();
            }

            $config = JFactory::getConfig();
            if (version_compare(JVERSION, '3.0', '>')) {
                $siteOffset = $config->get('offset');
                date_default_timezone_set($siteOffset);
            } else {
                $siteOffset = $config->getValue('config.offset');
                date_default_timezone_set($siteOffset);
            }

            $month = date('m');
            $year = date('Y');
            
            //Calculate the start of the month
            $monthstart = mktime(0, 0, 0, $month, 1, $year);
            
            switch ($dbKeep) {
                case 1:
                    $cleanstart = strtotime("-1 week", $monthstart);
                    break;
                case 2:
                    $cleanstart = strtotime("-3 months", $monthstart);
                    break;
                case 3:
                    $cleanstart = strtotime("-6 months", $monthstart);
                    break;
                case 4:
                    $cleanstart = strtotime("-12 months", $monthstart);
                    break;
                default:
                    $cleanstart = strtotime("-1 week", $monthstart);
                    break;
            }

            $query = $db->getQuery(true);
            $query->select('count(*)');
            $query->from($db->quoteName('#__cwtraffic'));
            $query->where('tm < ' . $db->quote($cleanstart));
            $db->setQuery($query);
            $oldrows = $db->loadResult();

            if (!empty($oldrows)) {
                $query = $db->getQuery(true);
                $query->update($db->quoteName('#__cwtraffic_total'));
                $query->set('tcount = tcount +' . $db->quote($oldrows));
                $db->setQuery($query);
                $db->query();

                $query = $db->getQuery(true);
                $query->from($db->quoteName('#__cwtraffic'));
                $query->delete();
                $query->where('tm < ' . $db->quote($cleanstart));
                $db->setQuery($query);
                $db->query();
            }

            return;
        }
    }

}

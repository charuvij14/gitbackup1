<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Bonsai v5.0: Consulta web de cat&aacute;logo ABIES</title>
<LINK REL="stylesheet" HREF="styles.css" TYPE="text/css">
</head>

<body>

<table width="100%" cellspacing="0" cellpadding="0">

  <tr> 

    <td  height="24" bgcolor="#0066CC" align="Left" valing="middle" class="Font9BoldWhite">&nbsp;<img src="../images/info.gif" width="16" height="16" align="absmiddle" alt="Ayuda">&nbsp;Ayuda</td>

    <td bgcolor="#0066CC" align="right"><a href="javascript:window.print();void(0)"><img src="../images/imprime.gif" align=absmiddle border=0><b> Imprimir</b></a>&nbsp;
<a href="javascript:self.parent.tb_remove();void(0)"><img src="../images/logout.gif" align=absmiddle border=0><b> Cerrar</b></a></td>
  </tr>

</table>



<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">

  <tr>

    <td>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td width="4%"><b><img border="0" src="../images/abies2.gif"></b></td>
          <td width="96%"><b>Bonsai v5.0</b> es una aplicaci�n que complementa a Abies y permite  la consulta v�a web del cat&aacute;logo de libros de la biblioteca del centro.</td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><b>Paginaci�n:</b> <br>En la esquina superior derecha, bajo la barra de b&uacute;squeda, encontrar&aacute;s la botonera para moverte entre las p&aacute;ginas del listado.<br>
              1) Pulsa sobre sus distintos botones: Primera, Anteriorx10, Anterior, Siguiente, Siguientex10 y &Uacute;ltima.<br>
          2) Si deseas ampliar o reducir el n&uacute;mero de ejemplares a visualizar en cada p&aacute;gina, haz clic sobre el n&uacute;mero deseado: 10, 20, 50 &oacute; 100.<br>
		  3) Utiliza la combinaci�n de teclas: &lt;alt&gt;+&lt;tecla&gt; y &lt;enter&gt; para moverte entre las p&aacute;ginas sin usar el rat�n: &lt;A&gt; Anterior, &lt;S&gt; Siguiente, 
		  &lt;P&gt; Primera y &lt;U&gt; �ltima.</td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><b>B�squeda:&nbsp;</b><br>
            1) Introduce en las casillas <b>Titulo</b>, <b> Autor </b> y/o <b> Editorial
            </b> la informaci�n a&nbsp;
            buscar.<br>
            2) Clic en el bot�n <b> Buscar</b> o pulsa la tecla <b>&lt;enter&gt;</b><br>
            3) Se muestra el mensaje <b>Filtro aplicado&nbsp;
            a ...</b>&nbsp; para indicar el campo o campos en los que se han establecido los
            criterios de b�squeda.<br>
            4) En el listado de ejemplares se muestra en video resaltado el campo&nbsp;coincidente con el criterio de b�squeda.<br>
            5) Para regresar a los fondos completos borra la informaci�n contenida en&nbsp;
            los criterios de b�squeda y vuelve a pulsar el bot�n &quot;<b>Buscar</b>&quot;.</td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><b>Ordenaci�n:</b><br>
            1) Clic sobre un campo (<b>T�tulo, Autor </b> o<b> Editorial</b>) en el&nbsp;
            encabezado del listado para realizar&nbsp; una ordenaci�n en sentido ascendente o
            descendente por ese campo.<br>
            Sobre el encabezado se muestra el tipo de ordenaci�n que hemos&nbsp;
            configurado para el listado.</td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><b>Detalles del ejemplar:</b><br>
            1) Pulsa sobre el <b>T�tulo </b> de un ejemplar.&nbsp;<br>
            2) Se desplegar� una ventana emergente mostrando la principal informaci�n&nbsp;
            del mismo.</td>
        </tr>
      </table>
      <table border="0" cellspacing="1" width="100%">
        <tr>
          <td><b>Otros:</b><br>
            Se indica la fecha de la <b>�ltima actualizaci�n</b> realizada en la Base de&nbsp;
            datos que contiene el cat&aacute;logo de libros de la biblioteca. </td>
        </tr>
    </table></td>
  </tr>
</table>





&nbsp;



<table width="100%" cellspacing="0" cellpadding="0">

</table>

</body>

</html>

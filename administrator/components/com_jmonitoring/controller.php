<?php
/**
 * @package		jmonitoring
 * @author		Jonathan Fuchs {@link www.inetis.ch}
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport('joomla.application.component.controller');

class jmonitoringController extends JController
{

  protected $default_view = 'jmonitorings';

	function display($cachable = false)
	{    
    // Voir le debug
    if(JRequest::getWord('task') == 'seeError')
    {
      echo $_SESSION['errorDebug'];
      jexit(); // die
    }
    
    require_once JPATH_COMPONENT . DS . 'helpers' . DS . 'jmonitoring.php';
    jmonitoringHelper::addSubmenu('message');
    parent::display($cachable);
	}// function
  	
}// class
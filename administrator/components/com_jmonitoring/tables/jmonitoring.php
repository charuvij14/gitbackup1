<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport('joomla.database.table');

class jmonitoringTablejmonitoring extends JTable
{  
  function jmonitoringTablejmonitoring(&$db) {
    parent::__construct('#__jmon_sites', 'id_site', $db);
	}
		
	function check()
	{
    if(substr($this->access_url, 0, 7) != "http://" && substr($this->access_url, 0, 8) != "https://")
      $this->access_url = "http://".$this->access_url; // complète l'URL si http n'est pas spécifié
    if(substr($this->access_url, -1) != "/")
      $this->access_url = $this->access_url."/"; // complète l'URL si le dernier slash n'est pas spécifié  
    return true;
	}
}
?>
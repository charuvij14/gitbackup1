<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport('joomla.database.table');

class jmon_logTablejmon_log extends JTable
{
  function jmon_logTablejmon_log(& $db) {
		parent::__construct('#__jmon_logs', 'id_log', $db);
	}
}
?>

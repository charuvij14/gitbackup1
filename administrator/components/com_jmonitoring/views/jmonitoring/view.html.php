<?php

/**
 * @package		jmonitoring
 * @author		Jonathan Fuchs {@link www.inetis.ch}, Jonathan Fuchs
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class jmonitoringViewjmonitoring extends JView {

  function display($tpl = null)
  {
    $state = $this->get('State');
    $item = $this->get('Item'); // Cherche les infos du site
    $form = $this->get('Form'); // Récupère les champs
    $form->bind($item);
    
    if (count($errors = $this->get('Errors'))) {
      JError::raiseError(500, implode("\n", $errors));
      return false;
    }
            
    $this->assignRef('state', $state);
    $this->assignRef('item', $item);
    $this->assignRef('form', $form);
    
    $this->addToolBar();
    
    parent::display($tpl);
  }
  
  protected function addToolBar()
  {
    $isNew = ($this->item->id_site < 1);
    $text = $isNew ? JText::_('COM_JMONITORING_NEW') : JText::_('COM_JMONITORING_EDIT');
    
    JToolBarHelper::title('JMonitoring Master - <small><small>[ ' . $text . ' ]</small></small>', 'icon_jmon');
    if(method_exists('JToolBarHelper','save2new')) JToolBarHelper::save2new('jmonitoring.save2new');
    JToolBarHelper::save('jmonitoring.save');
    JToolBarHelper::cancel('jmonitoring.cancel');
  }
}
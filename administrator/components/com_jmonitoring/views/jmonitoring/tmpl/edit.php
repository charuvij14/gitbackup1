<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}, Jonathan Fuchs
 */
// no direct access
defined('_JEXEC') or die;
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.tooltip');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
    if (task == 'jmonitoring.cancel' || document.formvalidator.isValid($('adminForm')))
			submitform(task, $('adminForm'));
		else
			alert("<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED'));?>");
	}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
  <div class="col100">
    <fieldset class="adminform">
      <legend><?php echo JText::_('JDETAILS'); ?></legend>
      <ul class="adminformlist">    
        <li><?php echo $this->form->getLabel('name'); ?>
            <?php echo $this->form->getInput('name'); ?></li>

        <li><?php echo $this->form->getLabel('catid'); ?>
            <?php echo $this->form->getInput('catid'); ?></li>

        <li><?php echo $this->form->getLabel('access_url'); ?>
            <?php echo $this->form->getInput('access_url'); ?></li>
            
        <li><?php echo $this->form->getLabel('admin_url'); ?>
            <?php echo $this->form->getInput('admin_url'); ?></li>
            
        <li><?php echo $this->form->getLabel('access_user'); ?>
            <?php echo $this->form->getInput('access_user'); ?></li>

        <li><?php echo $this->form->getLabel('access_pass'); ?>
            <?php echo $this->form->getInput('access_pass'); ?></li>

        <li><?php echo $this->form->getLabel('secret_word'); ?>
            <?php echo $this->form->getInput('secret_word'); ?></li>
        
        <li><?php echo $this->form->getLabel('word_check'); ?>
            <?php echo $this->form->getInput('word_check'); ?></li>
      </ul>
    </fieldset>
  </div>
  
  <?php
  echo JHtml::_('form.token');
  echo $this->form->getInput('id_site');
  ?>
  <input type="hidden" name="option" value="com_jmonitoring" />
  <input type="hidden" name="task" value="" />
</form>
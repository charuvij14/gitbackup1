<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}
 */
// no direct access
defined('_JEXEC') or die;
$mainframe =& JFactory::getApplication();
$str2search = $mainframe->getUserStateFromRequest( "com_jmonitoring.search", 'search', null );
$publishFilter = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_state", 'filter_state', null );
$validityFilter = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_validity", 'filter_validity', null );
$catFilter = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_category_id", 'filter_category_id', null );
?> 

<form action="index.php" method="post" name="adminForm" id="adminForm">
    <!-- FILTRES -->
    <table><tr><td width="100%">
                <?php echo JText::_('JSEARCH_FILTER_LABEL'); ?> <input type="text" value="<?php echo $str2search; ?>" name="search" id="search" class="text_area" onchange="document.adminForm.submit();"/>
                <button onclick="this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
                <button onclick="document.getElementById('search').value = null;document.getElementById('filter_state').value = null;document.getElementById('filter_category_id').value = null;document.getElementById('filter_validity').value = null;this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
            </td>
            <!-- Listes de filtres utilisant le framework Joomla! -->
            <td nowrap="nowrap">
                <select name="filter_category_id" id="filter_category_id" class="inputbox"" onchange="submitform();">
                        <option value=""><?php echo JText::_('JOPTION_SELECT_CATEGORY'); ?></option>
                            <?php echo JHtml::_('select.options', JHtml::_('category.options', 'com_jmonitoring'), 'value', 'text', $catFilter); ?>
                </select>
                <?php
                echo JHTML::_('grid.state', $publishFilter, 'Published', 'Unpublished');

                $options[] = JHTML::_("select.option", null, "- " . JText::_('COM_JMONITORING_JMON_STATE') . " -");
                $options[] = JHTML::_("select.option", 0, JText::_('COM_JMONITORING_JMON_VALID'));
                $options[] = JHTML::_("select.option", 1, JText::_('COM_JMONITORING_JMON_UNVALID'));
                echo JHTML::_("select.genericlist", $options, 'filter_validity', 'onchange="submitform();"', 'value', 'text', $validityFilter);
                ?>
            </td></tr></table>

    <!-- TABLES DES SITES -->
    <div id="editcell">
        <table class="adminlist">
            <thead>
                <tr>
                    <th width="5">
                        <?php echo JText::_('JGRID_HEADING_ID'); ?>
                    </th>
                    <th width="20">
                        <input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->items); ?>);" />
                    </th>			
                    <th>
                        <?php echo JText::_('JSITE'); ?>
                    </th>
                    <th width="50">
                        <?php echo JText::_('JPUBLISHED'); ?>
                    </th>
                    <th width="50">
                        <?php echo JText::_('COM_JMONITORING_JMON_STATE'); ?>
                    </th>
                    <th width="130">
                        <?php echo JText::_('JCATEGORY'); ?>
                    </th>
                    <th width="130">
                        <?php echo JText::_('COM_JMONITORING_JMON_LAST_DATE'); ?>
                    </th>
                    <th width="100">
                        <?php echo JText::_('COM_JMONITORING_JMON_VERSION'); ?>
                    </th>			
                    <th>
                        <?php echo JText::_('COM_JMONITORING_JMON_GOTO_SITE'); ?>
                    </th>
                </tr>			
            </thead>
            <?php
            $k = $j = 0;
            if ($this->items) {
                foreach ($this->items as $item) {
                    ?>
                    <tr class="<?php echo "row$k"; ?>">
                        <td>
                            <?php echo $j + 1; ?>
                        </td>
                        <td>
                            <?php echo JHTML::_('grid.id', $j, $item->id_site); ?>
                        </td>
                        <td>
                            <a href="<?php echo JRoute::_('index.php?option=com_jmonitoring&view=jmon_exts&amp;cid=' . (int) $item->id_site); ?>"><?php echo $item->name; ?></a>
                            <em>(<a href="<?php echo JRoute::_('index.php?option=com_jmonitoring&amp;task=jmonitoring.edit&amp;id_site=' . (int) $item->id_site); ?>"><?php echo JText::_('JACTION_EDIT'); ?></a>)</em>
                        </td>
                        <td class="center">
                            <!-- icône de publication -->
                            <?php echo JHtml::_('jgrid.published', $item->published, $j, 'jmonitorings.'); ?>
                        </td>
                        <td class="center jgrid">
                            <!-- icône d'état --> 
                            <span class="state icon-16-<?php
                              switch ($item->error) {
                                  case "0":
                                      echo "checkin";
                                      break;
                                  case "1":
                                      echo "logout";
                                      break;
                                  case "2":
                                      echo "config";
                                      break;
                              }
                            ?>">
                                <span class="text">
                                    <?php
                                    switch ($item->error) {
                                        case "0":
                                            echo JText::_('COM_JMONITORING_JMON_VALID');
                                            break;
                                        case "1":
                                            echo JText::_('COM_JMONITORING_JMON_UNVALID');
                                            break;
                                        case "2":
                                            echo JText::_('COM_JMONITORING_JMON_MAINTENANCE');
                                            break;
                                    }
                                    ?>
                                </span>
                            </span>
                        </td>
                        <td class="center">
                          <?php echo $item->cat_title; ?>
                        </td>
                        <td class="center">
                            <?php
                            if ($item->date_last_check != 0)
                                echo JHTML::DATE($item->date_last_check, JText::_('DATE_FORMAT_LC4') . " H:i");
                            ?>
                        </td> 
                        <td class="center">
                            <?php echo JText::_($item->j_version); ?>
                        </td>     					
                        <td>
                            <a target="_blank" href="<?php echo JRoute::_($item->access_url); ?>"><?php echo $item->name; ?></a>
                            <?php $adminUrl = ($item->admin_url == '')?$item->access_url.'administrator/index.php':$item->admin_url; ?>
                            <em>(<a target="_blank" href="<?php echo JRoute::_($adminUrl); ?>"><?php echo JText::_('JACTION_LOGIN_ADMIN'); ?></a>)</em>
                        </td>	
                    </tr>
        <?php
        $k = 1 - $k;
        $j++;
    }
}
?>
        </table>
    </div>

    <input type="hidden" id="id_site" name="id_site" value="" />
    <input type="hidden" name="option" value="com_jmonitoring" />
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="boxchecked" value="0" />
    <input type="hidden" name="view" value="jmonitorings" />
    <input type="hidden" name="controller" value="jmonitorings" />
    <?php echo JHTML::_('form.token'); ?>
</form>

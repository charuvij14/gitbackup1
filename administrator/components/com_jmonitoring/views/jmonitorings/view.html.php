<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}
 */
// no direct access
defined('_JEXEC') or die;
jimport('joomla.application.component.view');

class jmonitoringViewjmonitorings extends JView {

  function display($tpl = null)
  {
    JHTML::stylesheet('icon_jmon.css', 'administrator/components/com_jmonitoring/');
    JToolBarHelper::title(JText::_('COM_JMONITORING_TITLE'), 'icon_jmon');

    // Get data from the model
    $items = & $this->get('Data');
    
    $this->assignRef('items', $items);

    $this->addToolBar();

    parent::display($tpl);
  }
  
  protected function addToolBar()
  {
    if ($this->items != null) // Displayed only if websites are registered
    {
      JToolBarHelper::custom('jmonitorings.verify_checked_sites', 'apply.png', '', JText::_("COM_JMONITORING_JMON_VER"), true, false);
      JToolBarHelper::custom('jmonitorings.verify_sites', 'apply.png', '', JText::_("COM_JMONITORING_JMON_VER_ALL"), false, false);
      JToolBarHelper::divider();
      JToolBarHelper::custom('jmonitorings.getCsvFull', 'upload.png', '', JText::_("COM_JMONITORING_DB_FULLCSV"), false, false);      
      JToolBarHelper::custom('jmonitorings.getCsv', 'upload.png', '', JText::_("CSV"), false, false);
      JToolBarHelper::divider();
      JToolBarHelper::publish('jmonitorings.publish');
      JToolBarHelper::unpublish('jmonitorings.unpublish');
      JToolBarHelper::divider();
      JToolBarHelper::deleteList(JText::_('JTOOLBAR_REMOVE') . '?', 'jmonitoring.remove');
      JToolBarHelper::editList('jmonitoring.edit');
    }
    JToolBarHelper::addNew('jmonitoring.add');
    JToolBarHelper::divider();
    JToolBarHelper::preferences('com_jmonitoring',$height='400', $width='600');
  }
}
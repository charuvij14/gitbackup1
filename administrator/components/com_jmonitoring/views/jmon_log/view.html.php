<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport( 'joomla.application.component.view');

class jmonitoringViewjmon_log extends JView
{	
	function display($tpl = null)
	{		
    $item = $this->get('Item'); // Cherche les infos du log
    $form = $this->get('Form'); // Récupère les champs
		$form->bind($item); // Lie les infos du log aux champs
		
		if (count($errors = $this->get('Errors'))) {
      JError::raiseError(500, implode("\n", $errors));
      return false;
    }
    
    $this->assignRef('item', $item);
    $this->assignRef('form', $form);
    
    $this->addToolBar();
    
		parent::display($tpl);
  }// function
  
  protected function addToolBar()
  {
    $isNew = ($this->item->id_log < 1);
		
		$text = $isNew ? JText::_( 'COM_JMONITORING_New' ) : JText::_( 'COM_JMONITORING_Edit' );
		JHTML::stylesheet( 'icon_jmon.css', 'administrator/components/com_jmonitoring/');
		JToolBarHelper::title(  'jmonitoring: <small><small>[ ' . $text.' ]</small></small>', 'icon_jmon' );
		if(method_exists(JToolBarHelper,'save2new')) JToolBarHelper::save2new('jmon_log.save2new');
		JToolBarHelper::save('jmon_log.save');
		JToolBarHelper::cancel('jmon_log.cancel');
  }  
}// class

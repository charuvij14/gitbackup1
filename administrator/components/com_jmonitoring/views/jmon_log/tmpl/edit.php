<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch}
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );
?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div class="col100">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'JDETAILS' ); ?> Logs</legend>
    <ul class="adminformlist">
      <?php
          /*foreach($this->jlog->sitesList as $site)
          {
             $test[] = JHTML::_('select.option',$site->id_site, $site->name);
          }
          echo JHTML::_('select.genericlist',$test,"id_site",null,'value','text',@$this->jlog->idx_site);
          */
        ?>	      
      <li><?php echo $this->form->getLabel('idx_site'); ?>
			<?php echo $this->form->getInput('idx_site'); ?></li>
			
      <li><?php echo $this->form->getLabel('log_type'); ?>
			<?php echo $this->form->getInput('log_type'); ?></li>
			
			<li><?php echo $this->form->getLabel('log_date'); ?>
			<?php echo $this->form->getInput('log_date'); ?></li>
			
			<li><?php echo $this->form->getLabel('log_entry'); ?>
			<?php echo $this->form->getInput('log_entry'); ?></li>
			
			<li><?php echo $this->form->getLabel('log_comment'); ?>
			<?php echo $this->form->getInput('log_comment'); ?></li>
    </ul> 
     
	</fieldset>
</div>
<div class="clr"></div>

<?php echo JHTML::_( 'form.token' );
echo $this->form->getInput('id_log'); ?>
<input type="hidden" name="option" value="com_jmonitoring" />
<input type="hidden" name="task" value="seeLogs" />
<input type="hidden" name="controller" value="jmon_logs" />
</form>
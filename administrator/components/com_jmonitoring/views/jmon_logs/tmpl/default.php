<?php
/**
 * @package		jmonitoring
 * @author		Jonathan Fuchs {@link www.inetis.ch}
 */
// no direct access
defined('_JEXEC') or die(';)');
$mainframe =& JFactory::getApplication();
$str2search = $mainframe->getUserStateFromRequest( "com_jmonitoring.search", 'search', null );
$filter_verification = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_verification", 'filter_verification', null );
$filter_type = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_type", 'filter_type', null );
?> 
<a style="display:block; height: 16px; width:30px; padding-left: 18px; background: url(<?php echo JUri::root();?>administrator/templates/bluestork/images/menu/icon-16-newsfeeds.png) no-repeat left center;" href="<?php echo JUri::root();?>index.php?option=com_jmonitoring&amp;view=logs&amp;format=feed&amp;token=<?php echo $mainframe->getCfg('secret');?>">rss</a>
<form action="index.php" method="post" name="adminForm">
  <!-- FILTRES -->
  <table>
    <tr>
      <td width="100%">
        <?php echo JText::_('JSEARCH_FILTER_LABEL'); ?> : <input type="text" name="search" id="search" value="<?php echo $str2search; ?>" class="text_area" onchange="document.adminForm.submit();" title="<?php echo JText::_('COM_JMONITORING_Filter by title or enter article ID'); ?>"/>
        <button onclick="this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
        <button onclick="document.getElementById('search').value = null;document.getElementById('filter_state').value = null;document.getElementById('filter_validity').value = null;this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?></button>
      </td>
      <!-- Listes de filtres utilisant le framework Joomla! -->
      <td nowrap="nowrap">
        <?php
        $options[] = JHTML::_("select.option", null, "- " . JText::_('COM_JMONITORING_JMON_STATE') . " -");
        $options[] = JHTML::_("select.option", 1, JText::_('COM_JMONITORING_JMON_VERIFIED'));
        $options[] = JHTML::_("select.option", 0, JText::_('COM_JMONITORING_JMON_UNVERIFIED'));
        echo JHTML::_("select.genericlist", $options, 'filter_verification', 'onchange="submitform();"', 'value', 'text', $filter_verification);
        
        $optionsType[] = JHTML::_("select.option", null, "- " . JText::_('COM_JMONITORING_TYPE') . " -");
        $optionsType[] = JHTML::_("select.option", 1, JText::_('ERROR'));
        $optionsType[] = JHTML::_("select.option", 0, JText::_('COM_JMONITORING_JMON_INFO'));
        echo JHTML::_("select.genericlist", $optionsType, 'filter_type', 'onchange="submitform();"', 'value', 'text', $filter_type);
        ?>
      </td>
    </tr>
  </table>

  <!-- TABLES DES LOGS -->
  <div id="editcell">
  <table class="adminlist">
    <thead>
      <tr>
        <th width="5"><?php echo JText::_('JGRID_HEADING_ID'); ?></th>
        <th width="20"><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($this->items); ?>);" /></th>			
        <th><?php echo JText::_('JSITE'); ?></th>
        <th><?php echo JText::_('COM_JMONITORING_TYPE'); ?></th>
        <th width="50"><?php echo JText::_('COM_JMONITORING_JMON_VERIFIED'); ?></th>
        <th width="130"><?php echo JText::_('JDATE'); ?></th>	
        <th><?php echo JText::_('ERROR'); ?></th>
        <th><?php echo JText::_('COM_JMONITORING_JMON_COMMENT'); ?></th>
        <th><?php echo JText::_('COM_JMONITORING_JMON_GOTO_SITE'); ?></th>
      </tr>			
    </thead>
    <?php
    $k = 0;
    $j = 0;
    
    if ($this->items)
    foreach ($this->items as $item)
    {
      $checked = JHTML::_('grid.id', $j, $item->id_log);
      
      //ajout de http:// si pas existant dans l'adresse
      if ($item->access_url = str_replace("http://", "", $item->access_url))
        $item->access_url = "http://" . $item->access_url;

      $link = JRoute::_($item->access_url);
      ?>
      <tr class="<?php echo "row$k"; ?>">
        <td><?php echo $item->id_log; ?></td>
        <td><?php echo $checked; ?></td>
        <td><a href="<?php echo JRoute::_('index.php?option=com_jmonitoring&view=jmon_exts&amp;cid=' . (int) $item->id_site); ?>"><?php echo $item->name; ?></a></td>
        <td><center>
        <?php
        switch ($item->log_type) {
          case 0:
          echo JText::_('COM_JMONITORING_JMON_INFO');
          break;
          case 1:
          echo JText::_('ERROR');
          break;
        }
        ?></center>
        </td>
          <td class="center jgrid">
            <!-- icône d'état -->
            <a href="index.php?option=com_jmonitoring&task=jmon_logs.changeVerif&verif=<?php echo $item->log_verif ?>&lid=<?php echo $item->id_log ?>" >
              <span class="state icon-16-<?php echo ($item->log_verif) ? "checkin" : "logout" ?>">
                <span class="text"><?php echo ( $item->log_verif ) ? JText::_('COM_JMONITORING_JMON_VALID') : JText::_('COM_JMONITORING_JMON_UNVALID'); ?></span>
              </span>
            </a>                   
          </td> 
        <td><center><?php echo JHTML::DATE($item->log_date, JText::_('DATE_FORMAT_LC3') . " H:i"); ?></center></td> 
        <td><?php echo nl2br($item->log_entry); ?></td>
        <td><?php echo nl2br($item->log_comment); ?></td>		
        <td><a href="<?php echo $link; ?>"><?php echo $item->name; ?></a></td>	
      </tr>
      <?php
      $k = 1 - $k;
      $j++;
    }
    ?>
  </table>
  
  </div>
  <?php echo $this->pagination->getListFooter(); ?>
  <input type="hidden" id="id_log" name="id_log" value="" />
  <input type="hidden" name="option" value="com_jmonitoring" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="view" value="jmon_logs" />
  <input type="hidden" name="controller" value="jmon_logs" />
  <?php echo JHTML::_('form.token'); ?>
</form>
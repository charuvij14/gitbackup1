<?php
/**
 * @package		jmonitoring
 * @author		Jonathan Fuchs{@link www.inetis.ch}
 * @author		Created on June-2011
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport( 'joomla.application.component.view');

class jmonitoringViewjmon_logs extends JView
{
	protected $items;
	
	function display($tpl = null)
	{
    JHTML::stylesheet('icon_jmon.css', 'administrator/components/com_jmonitoring/');
    
    $this->items = $this->get('items');
    $this->pagination	= $this->get('Pagination');
    
    $this->addToolBar();

    parent::display($tpl);
  }
  
  protected function addToolBar()
  {
    JToolBarHelper::title(JText::_('COM_JMONITORING_TITLE'), 'icon_jmon');

    JToolBarHelper::custom('jmon_logs.getCsvLogs', 'upload.png', '', JText::_("CSV"), false, false);
    JToolBarHelper::divider();
    //bouton pour vérifier les sites choisis sur les checkboxes
    if ($this->items != null) {
        JToolBarHelper::deleteList(JText::_('JTOOLBAR_REMOVE') . '?', 'jmon_log.remove');
        JToolBarHelper::editList('jmon_log.edit');
    }
    JToolBarHelper::addNew('jmon_log.add');
  }
}
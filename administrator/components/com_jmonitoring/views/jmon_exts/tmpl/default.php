<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Jonathan Fuchs
 * @author		Created on 2011
 */
// no direct access
defined('_JEXEC') or die;

//Type d'extension
define("COM", "0");
define("MOD", "1");
define("PLG", "2");

$extensions = array();

// On recherche les éléments du site
foreach ($this->items as $item)
{
    //récupération des informations nécessaires     
    $extensions[$item->type][$item->ext_name . '_' . $item->id_ext]['site_id'] = $item->idx_site;
    $extensions[$item->type][$item->ext_name . '_' . $item->id_ext]['name'] = $item->ext_name;
    $extensions[$item->type][$item->ext_name . '_' . $item->id_ext]['date'] = $item->date;
    $extensions[$item->type][$item->ext_name . '_' . $item->id_ext]['url'] = $item->url;
    $extensions[$item->type][$item->ext_name . '_' . $item->id_ext]['version'] = $item->version;
}
///////	
// AFFICHAGE DES INFOS DU SITE 
?>
<form action="index.php" method="post" class="adminform" name="adminForm" id="adminForm">
  <div class="cpanel-left">
    <fieldset class="adminform">
      <legend><?php echo JText::_('JDETAILS'); ?></legend>
        <table class="admintable">
          <tr>
            <td width="100" align="right" class="key"><label for="name"><?php echo JText::_('COM_JMONITORING_NAME'); ?> :</label></td>
            <td><?php echo $item->name; ?></td>
          </tr>
          <tr>
            <td width="100" align="right" class="key"><label for="date"><?php echo JText::_('COM_JMONITORING_JMON_LAST_DATE'); ?> :</label></td>
            <td><?php echo JHTML::DATE($item->date_last_check, JText::_('DATE_FORMAT_LC4') . " H:i"); ?></td>
          </tr>
          <tr>
            <td width="100" align="right" class="key"><label for="publish"><?php echo JText::_('JPUBLISHED'); ?> :</label></td>
            <td class="jgrid">
              <!-- icône de publication -->
              <span class="state <?php echo ($item->state) ? "publish" : "unpublish" ?>">
                  <span class="text"><?php echo ( $item->state ) ? JText::_('JPUBLISHED') : JText::_('JUNPUBLISHED'); ?></span>
              </span>
            </td>
          </tr>
          <tr>
            <td width="100" align="right" class="key"><label for="state"><?php echo JText::_('COM_JMONITORING_JMON_STATE'); ?> :</label></td>
            <td class="jgrid">
              <!-- icône d'état -->
              <span class="state icon-16-<?php
                switch ($item->error) {
                  case "0":
                  echo "checkin";
                  break;
                  case "1":
                  echo "logout";
                  break;
                  case "2":
                  echo "config";
                  break;
                }?>">
                <span class="text"><?php
                  switch ($item->error) {
                  case "0":
                  echo JText::_('COM_JMONITORING_JMON_VALID');
                  break;
                  case "1":
                  echo JText::_('COM_JMONITORING_JMON_UNVALID');
                  break;
                  case "2":
                  echo JText::_('COM_JMONITORING_JMON_MAINTENANCE');
                  break;
                }
                ?></span>
              </span>
            </td>
          </tr>
          <tr>
            <td width="100" align="right" class="key"><label for="state"><?php echo JText::_('JCATEGORY'); ?> :</label></td>
            <td><?php echo $item->cat_title; ?></td>
          </tr>
          <tr>
            <td width="100" align="right" class="key"><label for="url"><?php echo JText::_('COM_JMONITORING_URL'); ?> :</label></td>
            <td><a href="<?php echo $item->access_url; ?>"><?php echo $item->access_url; ?></a></td>
          </tr>
          <?php
          foreach ($this->moreInfos as $key => $info) {
          echo '
          <tr>
            <td width="100" align="right" class="key"><label for="url">'.JText::_("COM_JMONITORING_".strtoupper($info->value_name)).' :</label></td>
            <td>'.JText::_($info->value_new).'</td>
          </tr>
          ';
          }
          ?>
        </table>
      </fieldset>
    </div>
  <div class="cpanel-right">
    <fieldset class="adminform">
      <legend><?php echo JText::_('COM_JMONITORING_JMON_EXTENSIONS'); ?></legend>
      <?php
      ///////
      // AFFICHAGE DES INFOS DES EXTENSIONS
      //import de la classe qui gère les sliders
      jimport('joomla.html.pane');
      $pane = &JPane::getInstance('sliders', array('allowAllClose' => true));
      
      echo $pane->startPane("content-pane");
      
      $title = JText::_('COM_JMONITORING_COMPONENTS');
      echo $pane->startPanel($title, "detail-page");
      //contenu de l'onglet
      echo showExts(COM, $extensions);
      echo $pane->endPanel();
      
      $title = JText::_('COM_JMONITORING_MODULES');
      echo $pane->startPanel($title, "params-page");
      //contenu de l'onglet
      echo showExts(MOD, $extensions);
      echo $pane->endPanel();
      
      $title = JText::_('COM_JMONITORING_PLUGINS');
      echo $pane->startPanel($title, "metadata-page");
      //contenu de l'onglet
      echo showExts(PLG, $extensions);
      echo $pane->endPanel();
      
      echo $pane->endPane();
      ?>
    </fieldset>
    <fieldset class="adminform">
      <legend><?php echo JText::_('COM_JMONITORING_JMON_PLUGINS'); ?></legend>
      <?php
      if ($this->sitePlugins != null) {
      
      jimport('joomla.html.pane');
      $paneplugin = &JPane::getInstance('sliders', array('allowAllClose' => true));
      echo $paneplugin->startPane("content-pane");
      //pour chaque plugin
      $compteur = 0;
      foreach ($this->sitePlugins as $plugin) {
      $title = $plugin->name;
      echo $paneplugin->startPanel($title, "panel" . $compteur++);
      echo '<table class="admintable"><tr><td><label for="description">Description :</label></td><td>' . $plugin->description . '</td></tr></table>';
      //list value
      echo showValues($plugin->id_plugin, $this->pluginsValues);
      //
      echo $paneplugin->endPanel();
      }
      echo $paneplugin->endPane();
      } else {
      echo JText::_('COM_JMONITORING_JMON_NO_PLUGINS');
      }
      ?>        
    </fieldset>  
  </div>

  <input type="hidden" name="option" value="com_jmonitoring" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="controller" value="jmon_exts" />
  <input type="hidden" name="cid" value="<?php echo $item->idx_site ?>" />
  <input type="hidden" name="site_name" value="<?php echo $item->name ?>" />
  <?php echo JHTML::_('form.token'); ?>
</form>

<?php

//FONTIONS
/* * ****************************************
 * 
 * ***************************************** */
function showValues($id_plugin, $pluginsValues)
{
  //entête de la table
  $output = '<table class="adminlist">
  <caption style="background-color:#f7f7f7;padding:5px"><b>Monitored values</b></caption>
  <thead>
    <tr>
      <th>Name</th>
      <th>Value</th>	
      <th>Type</th>	
      <th>Unit</th>	
    </tr>			
  </thead>';
  $j = 0;
  
  foreach ($pluginsValues as $value)
    if ($id_plugin == $value->idx_plugin) {
    $output .='
    <tr class="row'.$j.'">		
      <td>'.$value->name.'</td>
      <td>'.$value->value.'</td>
      <td>'.$value->type.'</td>
      <td>'.$value->unit.'</td>
    </tr>';
    $j = 1 - $j;
    }
    
  $output .= '</table>';
  return $output;
}

/* * ****************************************
 * gère l'affichage des extensions selon le type
 * ***************************************** */

function showExts($ext_type, $extensions)
{
  if (!empty($extensions[$ext_type]))
  {
    //Tri des extensions par ordre alphabétique	
    sort($extensions[$ext_type]);

    //entête de la table
    $output = '<table class="adminlist">
  	<thead>
  		<tr>
  			<th width="80%">'.JText::_('COM_JMONITORING_JMON_EXT_NAME').'</th>
  			<th width="80">'.JText::_('JVERSION').'</th>			
  			<th>'.JText::_('JDATE').'</th>
  		</tr>			
  	</thead>';

    // Pour chaque extension du type donné
    $j = 0;
    foreach ($extensions[$ext_type] as $key => $ext) {
        // on affiche les informations liées à l'extension
        $url = $extensions[$ext_type][$key]['url'];
        if (!preg_match('#http://#', $url))
            $url = 'http://' . $url;
        $output .='
      <tr class="row'.$j.'">		
        <td><a href="'.$url.'">'.$extensions[$ext_type][$key]['name'].'</a></td>
        <td>'.$extensions[$ext_type][$key]['version'].'</td>
        <td>'.$extensions[$ext_type][$key]['date'].'</td>
      </tr>';
        $j = 1 - $j;
    }

    return $output.'</table>';
  }
  else
    return JText::_('COM_JMONITORING_JMON_EXTENSIONS_EMPTY'); 
}
?>
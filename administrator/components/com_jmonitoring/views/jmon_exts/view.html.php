<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch} JF
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport( 'joomla.application.component.view');

class jmonitoringViewjmon_exts extends JView
{
	function display($tpl = null)
	{
		// Get data from the model
		$items = & $this->get( 'Data');
		$moreInfos = & $this->get( 'MoreInfos');
		$sitePlugins = & $this->get('SitePlugins');
		$pluginsValues = & $this->get('PluginsValues');
		
		$this->assignRef('items', $items);
		$this->assignRef('moreInfos', $moreInfos);
    $this->assignRef('sitePlugins', $sitePlugins);
    $this->assignRef('pluginsValues', $pluginsValues);

    $this->addToolBar();
    
		parent::display($tpl);
	}
	
	protected function addToolBar()
	{
	  JHTML::stylesheet( 'icon_jmon.css', 'administrator/components/com_jmonitoring/');
		JToolBarHelper::title(JText::_( 'COM_JMONITORING_TITLE' ).' - '.$this->items[0]->name, 'icon_jmon' );
		JToolBarHelper::custom( 'jmonitorings.getCsvExts', 'upload.png', '', JText::_( "CSV" ), false, false);
    JToolBarHelper::back();
	}
}

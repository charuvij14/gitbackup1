<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport( 'joomla.application.component.view');

class jmonitoringViewjmon_search extends JView
{
	function display($tpl = null)
	{
		// Get data from the model
		$items = & $this->get( 'Data');
		$this->assignRef('items',		$items);

    JHTML::stylesheet( 'icon_jmon.css', 'administrator/components/com_jmonitoring/');
		
		$this->addToolBar();
    
		parent::display($tpl);
	}
	
	protected function addToolBar()
	{
	  JToolBarHelper::title(JText::_( 'COM_JMONITORING_TITLE' ).' - '.JText::_( 'COM_JMONITORING_MENU_SEARCH' ), 'icon_jmon' );
	}
}

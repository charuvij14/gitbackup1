<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch}
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

// Récupération des valeurs de liste déjà introduites
$filter_state = JRequest::getVar('filter_state');
$filter_validity = JRequest::getVar('filter_validity');
$filter_type = JRequest::getVar('filter_type');
$state[0] = $validity[0] = $ext[0] = $state[1] = $validity[1] = $ext[1] = $ext[2] = $validity[2] = null;

//filtre d'état
if(isset($filter_state)) $state[$filter_state] = "selected='selected'";
//filtre de validité
if(isset($filter_validity)) $validity[$filter_validity] = "selected='selected'";
//filtre de type
if(isset($filter_type)) $ext[$filter_type] = "selected='selected'";

///////
    // AFFICHAGE DES CONTRÔLES DE RECHERCHE
?>
<form action="index.php" method="post" name="adminForm" id="search">
  <div id="editcell" style="width: 100%">

  <fieldset class="adminform"><legend><?php echo JText::_( 'JSEARCH_FILTER_LABEL' ); ?></legend>
    <table><tr><td>
    <fieldset class="adminform"><legend><?php echo JText::_( 'COM_JMONITORING_JMON_SITES' ); ?></legend>
    
    <!-- FILTRES -->
    <label id="search_name-lbl" for="search_name"><?php echo JText::_( 'COM_JMONITORING_JMON_SEARCH_NAME' ); ?></label>
    <input type="text" name="search_name" id="search_name" class="text_area" value="<?php echo JRequest::getVar('search_name'); ?>" title="<?php echo JText::_( 'COM_JMONITORING_Filter by title or enter article ID' );?>"/>
    
    <select name="filter_category_id" class="inputbox">
    <option value=""><?php echo JText::_('JOPTION_SELECT_CATEGORY');?></option>
      <?php echo JHtml::_('select.options', JHtml::_('category.options', 'com_jmonitoring'), 'value', 'text', JRequest::getVar('filter_category_id'));?>
    </select>
    
    <select id="filter_state" class="inputbox" size="1" name="filter_state">
      <option selected="selected" value="">- <?php echo JText::_( 'JLIB_HTML_SELECT_STATE' ); ?> -</option>
      <option <?php echo $state[1] ?> value="1"><?php echo JText::_( 'JPUBLISHED' ); ?></option>
      <option <?php echo $state[0] ?> value="0"><?php echo JText::_( 'JUNPUBLISHED' ); ?></option>
    </select>
    
    <select id="filter_validity" class="inputbox" size="1" name="filter_validity">
      <option selected="selected" value="">- <?php echo JText::_( 'COM_JMONITORING_JMON_STATE' ); ?> -</option>
      <option <?php echo $validity[0] ?> value="0"><?php echo JText::_( 'COM_JMONITORING_JMON_VALID' ); ?></option>
      <option <?php echo $validity[1] ?>value="1"><?php echo JText::_( 'COM_JMONITORING_JMON_UNVALID' ); ?></option>
      <option <?php echo $validity[2] ?>value="2"><?php echo JText::_( 'COM_JMONITORING_MAINTENANCE_LBL' ); ?></option>
    </select>  
    
    <label id="search_version-lbl" for="search_version"><?php echo JText::_( 'COM_JMONITORING_JMON_SEARCH_VERSION' ); ?></label>
    <input type="text" name="search_version" id="search_version" class="text_area" value="<?php echo JRequest::getVar('search_version'); ?>" title="<?php echo JText::_( 'COM_JMONITORING_Filter by version' );?>"/>
  
    </fieldset></td>
    <td>
    
    <fieldset class="adminform"><legend><?php echo JText::_( 'COM_JMONITORING_JMON_EXTENSIONS' ); ?></legend>
    <label id="search_ext_name-lbl" for="search_ext_name"><?php echo JText::_( 'COM_JMONITORING_JMON_SEARCH_NAME' ); ?></label>
    <input type="text" name="search_ext_name" id="search_ext_name" class="text_area" value="<?php echo JRequest::getVar('search_ext_name'); ?>" title="<?php echo JText::_( 'COM_JMONITORING_Filter by title or enter article ID' );?>"/>
    
    <select id="filter_type" class="inputbox" size="1" name="filter_type">
      <option selected="selected" value="">- <?php echo JText::_( 'COM_JMONITORING_JMON_SELECT_TYPE' ); ?> -</option>
      <option <?php echo $ext[0] ?> value="0"><?php echo JText::_( 'COM_JMONITORING_COMPONENTS' ); ?></option>
      <option <?php echo $ext[1] ?> value="1"><?php echo JText::_( 'COM_JMONITORING_MODULES' ); ?></option>
      <option <?php echo $ext[2] ?> value="2"><?php echo JText::_( 'COM_JMONITORING_PLUGINS' ); ?></option>
    </select> 
    
    <label id="search_ext_version-lbl" for="search_ext_version"><?php echo JText::_( 'COM_JMONITORING_JMON_SEARCH_VERSION' ); ?></label>
    <input type="text" name="search_ext_version" id="search_ext_version" class="text_area" value="<?php echo JRequest::getVar('search_ext_version'); ?>" title="<?php echo JText::_( 'COM_JMONITORING_Filter by version' );?>"/>
    </fieldset>
  
    </td></tr><tr><td colspan="2">
          <button onclick="this.form.submit();"><?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?></button>
          <input type="button" onclick="clearForm()" value="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"/>
    </td></tr></table>
  </fieldset>  
  <fieldset class="adminform"><legend><?php echo JText::_('COM_JMONITORING_RESULTS'); ?></legend>
  <?php  
    
    if($this->items)
    {
      ///////
      // AFFICHAGE DES INFOS DE LA RECHERCHE
      
      $exts[0] = JText::_( 'COM_JMONITORING_COMPONENT' );
      $exts[1] = JText::_( 'COM_JMONITORING_MODULE' );
      $exts[2] = JText::_( 'COM_JMONITORING_PLUGIN' );
        	
    	$results = array();
      
      foreach($this->items as $item)
        $results[$item->name][] = $item;
  
      $tableFlag = $k = 0;
      foreach($results as $key => $result)
      {
        // ENTÈTES D'ONGLETS
        $state_publish = ($result[0]->published)?  "unpublish":"publish";
        $state_icon = ($result[0]->published)? 'publish':'unpublish';  
        switch($result[0]->error)
        {
          default:
          case 0:
            $error_icon = 'checkin';
          break;
          case 1:
            $error_icon = 'logout';
          break;
          case 2:
            $error_icon = 'config';
          break;
        }
        // titre version et icones
        echo '<table class="adminlist"><tr><td><div class="jgrid">
                <b style="display:inline-table; padding: 0 5px; width:40px;">'.JText::_($result[0]->j_version).'</b>
                <span class="state '.$state_icon.'"></span>
                <span class="state icon-16-'.$error_icon.'"></span>
                <b style="display:inline-table; padding: 0 5px;"><a href="'.JRoute::_('index.php?option=com_jmonitoring&view=jmon_exts&amp;cid=' . (int) $result[0]->id_site).'">'.$result[0]->name.'</a></b>
              </div>';
        if(isset($result[0]->version))
        {
          $tableFlag = 1;
          echo '<table class="adminlist">
            	<thead><tr>
              		<th width="5">'.JText::_( '#' ).'</th>
        			    <th width="80">'.JText::_( 'COM_JMONITORING_TYPE' ).'</th>
            			<th>'. JText::_('COM_JMONITORING_JMON_EXT_NAME').'</th>
            			<th width="80">'. JText::_( 'JVERSION' ).'</th>			
            			<th width="100">'. JText::_( 'JDATE' ).'</th>
            		</tr>
            	</thead>';
          
          //contenu de l'onglet
          $j = 0;
          foreach($result as $key2 => $info)
          {
            echo '
                <tr class="row'.$j.'">		
                  <td><center>'.($key2+1).'</center></td>
                  <td><center>'.@$exts[$info->type].'</center></td>
                  <td>'.$info->ext_name.'</td>
                  <td>'.$info->version.'</td>
                  <td>'.$info->date.'</td>
                </tr>';
            $j = 1 - $j;
          }
          if($tableFlag == 1) echo "</table>";
        }
        echo '</td></tr></table>';
        $k = 1 - $k;
      }// end foreach
    }
    else
    {
      echo JText::_( 'COM_JMONITORING_JMON_NO_RESULTS' );
    }
  ?>
  </fieldset>
  </div>
  
  <input type="hidden" name="option" value="com_jmonitoring" />
  <input type="hidden" name="task" value="" />
  <input type="hidden" name="view" value="jmon_search" />
  <input type="hidden" name="boxchecked" value="0" />
  <input type="hidden" name="controller" value="jmon_search" />
  <?php JHTML::_('form.token'); ?>
</form>
<?php
$doc =& JFactory::getDocument();
$doc->addScriptDeclaration('
  function clearForm()
  {
    var formSearch = document.forms["adminForm"];

    for(i=0 ; i<formSearch.elements.length ; i++)
    {  
      if(formSearch.elements[i].type == "text" )
        formSearch.elements[i].value = null;
      else if(formSearch.elements[i].type == "select-one")
        formSearch.elements[i].value = null;
    }
  }');
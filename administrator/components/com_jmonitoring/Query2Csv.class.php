<?php
/**
* Query2Csv Generator Class
*
* @author  :  MA Razzaque Rupom <rupom_315@yahoo.com>, <rupom.bd@gmail.com>
*             Moderator, phpResource (http://groups.yahoo.com/group/phpresource/)
*             URL: http://www.rupom.info  
* @version :  1.0
* @date       06/17/2006
* Purpose  :  Generating Csv Reports from MySQL Query
*/
// no direct access
defined( '_JEXEC' ) or die( ';)' );

class Query2Csv
{   
   //To use it in PHP4, replace 'private' by 'var'
   private $csvReportData;
      
   /**
   * Generates CSV report from query
   * @param MySQL Query
   * @return none
   */          
   function generateCsvReport($query)
   {
      $db	= & JFactory::getDBO();
      $db->setQuery($query);
          
      if($resultSet = $db->loadObjectList())
      {       
         $data = array();
      	 
      	 $columns = array();
      	 
      	 $j = 0; 
         
        foreach($resultSet as $v)
        {
          foreach($v as $i=>$value)
          {
            if(!is_numeric($i))
           	{
           	   if($j==0)
           	   {
           	      $columns[] = $i;	
           	   }

           	   $data[$j][$i] = $value;         	   
            }
          }       
        $j++; 
        }
        
        //Generating report data
        $string = '';                  
        $string .= $this->createCsvHeaders($columns);
        $string .= $this->createCsvRows($data);         
                                           
        $this->csvReportData = $string;
      }//End of if_0
            	
   }//EO Method   
      
   /**
   * Gets CSV report
   * @param none
   * @return none
   */       
   function getCsvReport($csvTitle)
   {
      
      // Outputting CSV Report      
      header("Content-Type: text/csv");
      header("Expires: 0");
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0");               
      header("Content-Disposition: attachment; filename=".$csvTitle.".csv");
            
      echo $this->csvReportData;    
	
   }//EO Method
   
   /**
   * Debugs/dumps data
   * @param $dump
   * @return none
   */
   function dBug($dump)
   {
   	echo '<pre>';
   	print_r($dump);
   	echo '</pre>';
   }
    
  /** 
   * Creates CSV headers
   * @param Array report columns
   * @return String CSV header data.
   */
   function createCsvHeaders($array)
   {
   	 $string = '';	 
   	       
      for($i=0;$i<sizeof($array); $i++)
      {
         if($i == 0)
         {
            $string .= "$array[$i]";	
         }
         else
         {
            $string .= ",$array[$i]";	
         }
      }
            
      return $string."\n";
      
   }//EO Method
   
      
   /** 
   * Creates CSV rows
   * @param Array report rows
   * @return String CSV rows
   */   
   function createCsvRows($array)
   {
   	 $string = '';	 
   	 
   	 $cnt = 0;
   	  
     for($i=0; $i<sizeof($array); $i++)
     {
        
        $cnt = 0;
        
        foreach($array[$i] as $v)
        {
                       
           if($cnt == 0)
           {
              $string .= $this->filterText($v);	
           }
           else
           {
           	  $string .= ','.$this->filterText($v);	
           }
           
           $cnt++;
        }
        
        $string .= "\n";              
      
      }

      return $string;
      
   }//EO Method
   
   /**
   * Prepares CSV compatible data 
   * @param data to be filtered
   * @return filtered data
   */
   function filterText($value)
   {
      
      if(substr($value,0,1) == ',' || strpos($value,',',1))
      {         
         $value = '"'.str_replace('"','""',$value).'"';
         return $value;
      }   	
          
      return $value;
      
   }//EO Method   
   
}//EO Class
?>
CREATE TABLE IF NOT EXISTS `#__jmon_sites` (
	`id_site` int(11) NOT NULL auto_increment,
	`catid` int(11) NOT NULL default 0,
	`name` varchar(250) NOT NULL default '',
	`access_url` varchar(250) NOT NULL default '',
  `admin_url` varchar(250) NOT NULL default '',
  `secret_word` varchar(250) NOT NULL default '',
	`j_version` varchar(50) NOT NULL default 'COM_JMONITORING_JMON_UNKNOWN',
	`published` BOOL NOT NULL default 1,
	`error` BOOL NOT NULL default 0,
	`date_last_check` DATETIME NOT NULL,		
  `word_check` varchar(50) NOT NULL default '',
	PRIMARY KEY  (`id_site`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jmon_exts` (
	`id_ext` int(11) NOT NULL auto_increment,
	`idx_site` int(11) NOT NULL,
  `type` int(2) NOT NULL,
  `ext_name` varchar(250) NOT NULL default '',
  `version` varchar(100) NOT NULL default '',
  `date` varchar(100) NOT NULL default '',
  `url` varchar(100) NOT NULL default '',                           
	PRIMARY KEY  (`id_ext`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jmon_logs` (
	`id_log` int(11) NOT NULL auto_increment,
	`idx_site` int(11) NOT NULL,
	`log_type` int(2) NOT NULL,
  `log_verif` BOOL NOT NULL default 0,
  `log_entry` TEXT(500) NOT NULL default '',
  `log_comment` TEXT(500) NOT NULL default '',
  `log_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,                       
	PRIMARY KEY  (`id_log`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jmon_more_values` (
	`id_value` int(11) NOT NULL auto_increment,
	`idx_site` int(11) NOT NULL,
  `value_name` varchar(250) NOT NULL default '',
  `value_old` TEXT(500) NOT NULL default '',
  `value_new` TEXT(500) NOT NULL default '',                           
	PRIMARY KEY  (`id_value`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jmon_plugins` (
	`id_plugin` int(11) NOT NULL auto_increment,
	`idx_site` int(11) NOT NULL,
  `name` varchar(250) NOT NULL default '',
  `description` TEXT(500) NOT NULL default '',                          
	PRIMARY KEY  (`id_plugin`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__jmon_plugin_values` (
	`id_value` int(11) NOT NULL auto_increment,
	`idx_plugin` int(11) NOT NULL,
  `name` varchar(250) NOT NULL default '',
  `value` TEXT(500) NOT NULL default '', 
  `unit` varchar(250) NOT NULL default '',
  `type` varchar(250) NOT NULL default '',                         
	PRIMARY KEY  (`id_value`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
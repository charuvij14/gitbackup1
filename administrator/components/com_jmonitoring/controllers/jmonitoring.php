<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

class jmonitoringControllerjmonitoring extends JControllerForm {
  /**
   * remove record(s)
   * @return void
   */
  function save()
  {
    $item = JRequest::getVar('jform');
    if($item['id_site'] > 0) JRequest::setVar("id_site", $item['id_site']);
    parent::save();
  } 
   
  function remove()
  {
    $model = $this->getModel('jmonitoring');
    if (!$model->delete()) {
        $msg = JText::_('ERROR');
    } else {
        $msg = JText::_('COM_JMONITORING_SUCCESS');
    }

    $this->setRedirect('index.php?option=com_jmonitoring&view=jmonitorings', $msg);
  }
}
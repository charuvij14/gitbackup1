<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch}
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport('joomla.application.component.controllerform');

class jmonitoringControllerjmon_log extends JControllerForm
{
  function save()
  {
    $item = JRequest::getVar('jform');
    if($item['id_log'] > 0) JRequest::setVar("id_log", $item['id_log']);
    parent::save();
  }
/**
	 * remove record(s)
	 * @return void
	 */
	function remove()
	{
    $model = $this->getModel('jmon_log');
		if(!$model->delete()) {
			$msg = JText::_( 'ERROR' );
		} else {
			$msg = JText::_( 'COM_JMONITORING_SUCCESS' );
		}
		$this->setRedirect( 'index.php?option=com_jmonitoring&view=jmon_logs', $msg );
	}// function
}
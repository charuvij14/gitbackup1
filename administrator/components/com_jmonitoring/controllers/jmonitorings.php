<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}, Jonathan Fuchs
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');
jimport('joomla.plugin.helper');

class jmonitoringControllerjmonitorings extends JControllerAdmin
{
  public static $errorDebug;

  /**
   * constructor (registers additional tasks to methods)
   * @return void
   */
  function __construct() {
      parent::__construct();
  }

// function

  /**
   * save a record (and redirect to main page)
   * @return void
   */
  function save() {
    $model = $this->getModel('jmonitoring');
    if ($model->store($_POST)) $msg = JText::_('JLIB_APPLICATION_SAVE_SUCCESS');
    else $msg = JText::_('ERROR');

    $link = 'index.php?option=com_jmonitoring&view=jmonitorings';
    $this->setRedirect($link, $msg);
  }

// function

  /**
   * cancel editing a record
   * @return void
   */
  function cancel()
  {
    $msg = JText::_('COM_JMONITORING_OPERATION_CANCELLED');
    $this->setRedirect('index.php?option=com_jmonitoring&view=jmonitorings', $msg);
  }

// function
  //but : vérifier les sites cochés
  function verify_checked_sites() {
    //on récupère les enregistrements checkés
    $cid = JRequest::getVar('cid', array(), 'post', 'array');
    JArrayHelper::toInteger($cid);
    $cids = implode(',', $cid);
    $reqCids = ' id_site IN ( ' . $cids . ' )';

    jmonitoringControllerjmonitorings::verify_sites($reqCids);
  }

  function verify_sites($reqCids = false)
  {
    $db = & JFactory::getDBO();
    $params = JComponentHelper::getParams('com_jmonitoring'); // on charge la liste des paramètres config        
    $model = $this->getModel('jmonitorings'); // on récupère le modèle qui va permettre de charger les données
    $items = $model->getData(true, $reqCids, 1); // on charge les données des sites publiés
    
    $p = array(
      "task"=>JRequest::getCmd('task'),
      "checkFiles"=>$params->get('checkFiles', 1),
      "logs"=>$params->get('logs', 0),
      "logOnMailOnly"=>$params->get('logsmail', 0),
      "email"=>$params->get('email')
    );
    
    //chronomètre
    function microtime_float() {
      list($usec, $sec) = explode(" ", microtime());
      return ((float) $usec + (float) $sec);
    }
    
    //start du chronomètre global
    $global_time_start = microtime_float();

    //on vérifie si la tâche est le cron ou non
    if($p['task'] != "cron")
    {
      // Sécurité Check for request forgeries
      JRequest::checkToken() or jexit('Invalid Token');
      $jmonPath = JPATH_COMPONENT;
      $isCron = 0;
    } else {
      $jmonPath = JPATH_COMPONENT_ADMINISTRATOR;
      $isCron = 1;
    }
    
    include($jmonPath . DS . 'func_jmon.inc.php');
    
    //Constantes
    //Type d'extension
    define("COM", "0");
    define("MOD", "1");
    define("PLG", "2");
    //Propriétés de l'extension
    define("NAME", "0");
    define("VERSION", "1");
    define("DATE", "2");
    define("URL", "3");
    define("CHECK_FILES", "4");
    define("SERVER_VERSIONS", "5");
    define("SIZE", "1");
    
    $alertsSumary = array();

    foreach ($items as $item)
    {
      //Chronomètre du site : départ
      $time_start = microtime_float();
      //chrono
      $mtime = explode(' ', microtime());
      $time = (float) $mtime[0] + (float) $mtime[1];
      
      //path 
      $accessUrl = $item->access_url;
      if(substr($accessUrl, -1) != "/")
        $accessUrl = $accessUrl."/"; // complète l'URL si le dernier slash n'est pas spécifié  

      //Initialize the Curl session 
      $ch = curl_init(); 
      curl_setopt($ch, CURLOPT_URL, $accessUrl.'index.php?option=com_jmonitoringslave&format=json'); //Set the URL 
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser. 
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array("access" => md5($item->secret_word), "jmpluginsexvalues" => getSitePlugins($item->id_site)));
      curl_setopt($ch, CURLOPT_USERAGENT, 'JMonitoring/2.0 (+http://www.jmonitoring.com)');
      curl_setopt($ch, CURLOPT_REFERER, $accessUrl);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Expect:' ));
      
      $response = utf8_encode(curl_exec($ch)); //Execute the fetch 
      $curlerror = curl_error($ch);//s'il y a des erreurs
      $infocurl = curl_getinfo($ch); //get info
      curl_close($ch); //Close the connection 

      //Chronomètre du site : cliché à cet instant
      $mtime = explode(' ', microtime());
      $time = (float) $mtime[0] + (float) $mtime[1] - $time;
      
      //réinitialisation pour un nouveau site
      $errormsg = $infos = array();
      $errormsgStr = null;
      $access = $connectionFailed = 0; 

      // si on obtient une réponse 200 - OK
      if($infocurl["http_code"] == 200)
      {
        $connectionFailed = 0;
        $response = preg_replace("#warning.*\n#im","$1",strip_tags($response));// clean response
        $response = preg_replace("#notice.*\n#im","$1",strip_tags($response));// clean response
        $obj_response = json_decode($response); //decode the response
        
        //est-ce que l'accès est autorisé?
        if ($obj_response->status->access == false)
        {
          $_SESSION['errorDebug'] = "<h1>Debug</h1>".$response;
          $errormsg[] = $item->name . " : Access denied <a style='text-decoration: underline' class='modal' rel='{handler: \"iframe\", size: {x: 1000, y: 650}}' 
          href='index.php?option=com_jmonitoring&amp;task=seeError'>
            Debug
          </a>";
        }
        else
        {
          $access = 1;
          //suppression des anciennes valeurs
          $db->setQuery("DELETE FROM #__jmon_exts WHERE idx_site = " . $item->id_site);
          $db->query();
          
          //update Jversion
          $infos[] = updateJversion($item->id_site, $obj_response->versions->j_version);   
          unset($obj_response->versions->j_version);
          //update versions
          foreach ($obj_response->versions as $key => $value)
            if (isset($value)) $infos[] = dbUpdateValue($value, $item->id_site, $key);
          
          //extensions insertion
          $errormsg[] = makeQuery(COM, $obj_response->extensions->components, $item->id_site, $item->name);
          $errormsg[] = makeQuery(MOD, $obj_response->extensions->modules, $item->id_site, $item->name);
          $errormsg[] = makeQuery(PLG, $obj_response->extensions->plugins, $item->id_site, $item->name);
          
          //check files
          if($p['checkFiles'] == 1)
          {
            $fileError = checkFilesInfos($obj_response->filesproperties, $item->id_site);
            if(!empty($fileError)) $errormsg = array_merge($errormsg, $fileError);
          }
          
          //Word verification on homepage
          $curlCheck = checkCurl($item->word_check, $item->access_url, 0);
          if ($curlCheck == 2) $errormsg[] = JText::sprintf('COM_JMONITORING_WORD_NOT_IN_HOMEPAGE', $item->word_check) . " : " . sprintf(" %.2f", $time) . " " . JText::_('COM_JMONITORING_SECONDS');
          
          /*
          * jmonitoring plugin manager
          */
          
          $plugins = $obj_response->jmonitoringplugins; //on récupère les plugins
          $id_plugin = getLastPluginId(); //on doit recuperer l'id avant de tout effacer
          deleteSitePlugins($item->id_site); //on efface tous les plugins présents et leur valeurs;
                          
          if (count($plugins) > 0)
          {
            //on ajoute les plugins et leurs valeurs
            $errormsg[] = addSitePlugins($item->id_site, $plugins, $id_plugin);
            //on cherche les alertes
            foreach ($plugins as $plugin)
              if ($plugin->alerts != null) //test si il y a des alertes
                foreach ($plugin->alerts as $alert)
                  switch ($alert->level) //on class les alertes selon leur niveau
                  {
                    case 1:
                    $infos[] = JText::_($alert->message);
                      break;
                    case 2:
                    default:
                      $errormsg[] = JText::_($alert->message);
                    break;
                  }
          }
          //si le site distant est en maintenance. On supprime les erreurs et infos potentielles.
          if ($obj_response->status->maintenance == true)
          {
            $infos = $errormsg = null;
            $infos[] =  JText::_('COM_JMONITORING_MAINTENANCE');
          }
        }
      }
      else
      {
        if($curlerror =='') $curlerror = " : HTTP Status Code : ".$infocurl["http_code"];
        
        $errormsg[] =  "Connection failed".$curlerror;
        $connectionFailed = 1;
      }
      /*
      * Error and Information Management
      */

      //nettoie les valeurs nulles des infos et erreurs
      foreach($infos as $k => $v) if($v == null) unset($infos[$k]);
      foreach($errormsg as $k => $v) if($v == null) unset($errormsg[$k]);

      //toutes les infos et erreurs sont rassemblés dans un tableau
      $alertsSumary[$item->id_site] = array(
        "site"=>array("id"=>$item->id_site,"name"=>$item->name, "access_url"=>$item->access_url),
        "site_response"=>$obj_response,
        "informations"=>$infos,
        "errors"=>$errormsg
      );
      
      //Error
      if(count($errormsg) > 0)
      {
        foreach ($errormsg as $error)
        {      
          //on la met en page pour l'affichage
          $errormsgStr .= "<li>" . $item->name . " : " . JText::_('ERROR') . " : " . $error . "</li>";
          
          if($p['logs'] == 1) //Si la config des logs est activée
          {
            //on l'inscrit dans le log
            $errorWOutDebug = split('->', $error);
            //Si (cron et logOnMailOnly) OU (PasLogOnMailOnly)
            if(($p['task'] == "cron" && $p['logOnMailOnly'] == 1) || ($p['logOnMailOnly'] == 0))
              writeLog($errorWOutDebug[0], $item->id_site, 1);
          }
        }
        
        changeErrorTick($item->id_site, 1); // on donne un état d'erreur au site  
        JError::raiseWarning(500, $errormsgStr); //on transmet la liste d'erreurs à afficher
        @$errormsgdisplay[0] = $errormsgStr;
      }
      else// il n'y a pas d'erreurs, c'est un succès
      {
        @$errormsgdisplay[1] .= "<li>" . $item->name . " : " . JText::_('COM_JMONITORING_SUCCESS') . " : " . sprintf(" %.2f", $time) . " " . JText::_('COM_JMONITORING_SECONDS') . "</li>";
        changeErrorTick($item->id_site, 0);
        $db->setQuery("UPDATE `#__jmon_sites` SET date_last_check = '" . date("Y-m-d H:i:s") . "' WHERE `id_site` = " . $item->id_site);
        $db->query();
      }

      //Information
      if(!empty($infos))
      foreach ($infos as $info)
      {
        if ($p['logs'] == 1)
          if(($p['task'] == "cron" && $p['logOnMailOnly'] == 1) || ($p['logOnMailOnly'] == 0))//Si (cron et logOnMailOnly) OU (PasLogOnMailOnly)
            writeLog($info, $item->id_site, 0);
        
        JError::raiseNotice( 500, "<li>".$item->name." : ".$info."</li>" );
        @$errormsgdisplay[2] .= "<li>".$item->name." : ".$info."</li>";
      }
      //maintenance change tick
      if($access == 1 && $connectionFailed == 0 && $obj_response->status->maintenance == 1) 
        changeErrorTick($item->id_site, 2);
         
      //chronomètre : arrêt 
      $time = microtime_float() - $time_start;
    }
    // Fin de foreach pour chaque site

    //JMonitoring plugin alert call
    JPluginHelper::importPlugin('jmonitoringalert');
    $dispatcher = JDispatcher::getInstance();
    $result = $dispatcher->trigger('onAlertCall', array($alertsSumary));
    
    @$errormsgdisplay[1] .= "<li>" . JText::_('COM_JMONITORING_TOTAL') . " : " . sprintf(" %.2f", (microtime_float()- $global_time_start)) . " " . JText::_('COM_JMONITORING_SECONDS') . "</li>";
    
    //Si la tâche est un cron, on envoie un e-mail à l'adresse de la config
    if ($p['task'] == "cron" && $p['email'] != null)
      jmonitoringControllerjmonitorings::cronmail($errormsgdisplay, $p['email'], $params->get('parc'));

    $this->setRedirect('index.php?option=com_jmonitoring&view=jmonitorings', @$errormsgdisplay[1]);
  }

  function cronmail($errormsgdisplay, $email, $nomParc)
  {
    $isOk = JText::_('ERROR');

    if (empty($errormsgdisplay[0])) $isOk = JText::_('COM_JMONITORING_SUCCESS');
    else $errTitle = "<br/>" . JText::_('ERROR');

    if(!empty($errormsgdisplay[1])) $okTitle = "<br/>" . JText::_('COM_JMONITORING_SUCCESS');
    if(!empty($errormsgdisplay[2])) $infoTitle = "<br/>" . JText::_('COM_JMONITORING_JMON_INFO');
    
    $config = JFactory::getConfig();
    $mail = &JFactory::getMailer();
    $mail->AddAddress($email);
    $mail->setSender($config->getValue('config.mailfrom'));
    $mail->setSubject($isOk . ' : ' . $nomParc . ' - ' . JText::_('COM_JMONITORING_JMON_NOTIFICATION') . ' - ' . date("Y-m-d H:i:s"));
    $mail->setBody(JText::sprintf('COM_JMONITORING_JMON_CRON_MAIL', $nomParc) . @$errTitle . @$errormsgdisplay[0] . @$infoTitle . @$errormsgdisplay[2] . @$okTitle . @$errormsgdisplay[1]);
    $mail->IsHTML(true);
    $mail->Send();
  }

  function getCsv()
  {
    //chargement de la classe pour mettre en csv les requêtes sql
    require_once JPATH_COMPONENT . DS . 'Query2Csv.class.php';

    @ini_set('display_errors', 'Off');
    $obj = new Query2Csv();
    //queries whose output will be used as report data
    $obj->generateCsvReport("SELECT DISTINCT name as 'Site', access_url as Url, published as " . JText::_('JPUBLISHED') . ", error as " . JText::_('COM_JMONITORING_JMON_STATE') . ",
    date_last_check as '" . JText::_('COM_JMONITORING_JMON_LAST_DATE') . "', word_check as '" . JText::_('COM_JMONITORING_JMON_WORD') . "'
    FROM #__jmon_sites 
    LEFT JOIN #__jmon_more_values ON #__jmon_more_values.idx_site = #__jmon_sites.id_site");

    die($obj->getCsvReport('JMonitoring_' . date('Y.m.d')));//gets CSV report
  }

  function getCsvExts()
  {
    //chargement de la classe pour mettre en csv les requêtes sql
    require_once JPATH_COMPONENT . DS . 'Query2Csv.class.php';
    
    @ini_set('display_errors', 'Off');
    $obj = new Query2Csv();
    //queries whose output will be used as report data
    $obj->generateCsvReport("SELECT ext_name AS '" . addslashes(JText::_('COM_JMONITORING_JMON_EXT_NAME')) . "', version AS '" . JText::_('COM_JMONITORING_VERSION') . "', 
      date AS '" . JText::_('COM_JMONITORING_JMON_LAST_DATE') . "', url
      FROM #__jmon_exts
      WHERE idx_site = " . JRequest::getVar("cid"));
  
    die($obj->getCsvReport(JRequest::getVar("cid") . '_detail_' . date('Y.m.d'))); //gets CSV report
  }

  /*
   * Get all informations about sites
   */

  function getCsvFull()
  {
    //chargement de la classe pour mettre en csv les requêtes sql
    require_once JPATH_COMPONENT . DS . 'Query2Csv.class.php';
  
    @ini_set('display_errors', 'Off');
    $obj = new Query2Csv();
    
    //queries whose output will be used as report data
    $obj->generateCsvReport("SELECT e.`ext_name` as extension, e.`version`, e.`date`,
      CASE e.`type` WHEN 0 then 'component' WHEN 1 then 'module' WHEN 2 then 'plugin' ELSE 'unknown' END as type, e.`url` as Extn_url,
      s.`name` as site, s.`access_url` as Site_url, s.`j_version` as Joomla_version,
      pv.`value_new` as php_version, mv.`value_new` as mysql_version, sv.`value_new` as server_version
      FROM #__jmon_exts e
      INNER JOIN #__jmon_sites s ON e.idx_site = s.id_site
      LEFT JOIN #__jmon_more_values pv ON s.`id_site` = pv.`idx_site` and pv.`value_name` = 'php_version'
      LEFT JOIN #__jmon_more_values mv ON s.`id_site` = mv.`idx_site` and mv.`value_name` = 'mysql_version'
      LEFT JOIN #__jmon_more_values sv ON s.`id_site` = sv.`idx_site` and sv.`value_name` = 'server_version'
      ORDER BY e.ext_name ASC, e.version DESC
    ");
    
    die($obj->getCsvReport(JRequest::getVar("cid") . '_detail_' . date('Y.m.d')));//gets CSV report
  }
}
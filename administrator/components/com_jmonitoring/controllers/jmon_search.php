<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}
*/

// no direct access
defined( '_JEXEC' ) or die;

jimport('joomla.application.component.controlleradmin');

class jmonitoringControllerjmon_search extends JControllerAdmin
{
	function __construct($config = array())
	{
		parent::__construct($config);
	}// function
	
	function display()
	{
		JRequest::setVar( 'view'  , 'jmon_search');
		parent::display();
	}// function
	  	  
}//class
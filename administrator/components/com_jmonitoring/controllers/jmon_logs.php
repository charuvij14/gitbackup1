<?php

/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @author		Jonathan Fuchs {@link www.inetis.ch}
 * @author		Created on June-2009
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controlleradmin');

class jmonitoringControllerjmon_logs extends JControllerAdmin {

    function __construct($config = array()) {
        parent::__construct($config);

        // Register Extra tasks
        $this->registerTask('add', 'edit');
    }

// function

    function display() {
        parent::display();
    }

// function

    function edit() {
        JRequest::setVar('view', 'jmon_logs');
        JRequest::setVar('layout', 'form');
        JRequest::setVar('hidemainmenu', 1);
        parent::display();
    }

// function

    /**
     * save a record (and redirect to main page)
     * @return void
     */
    function save() {
        $model = $this->getModel('jmon_log');
        if ($model->store($_POST)) {
            $msg = JText::_('SUCCESS');
        } else {
            $msg = JText::_('ERROR');
        }
        // Check the table in so it can be edited.... we are done with it anyway
        $link = 'index.php?option=com_jmonitoring&task=seeLogs';
        $this->setRedirect($link, $msg);
    }

// function
    //définit l'icone de vérification à 1
    function verify_ok() {
        jmonitoringsControllerjmon_logs::changeVerifBool(1);
    }

    //définit l'icone de vérification à 0
    function verify_notok() {
        jmonitoringsControllerjmon_logs::changeVerifBool(0);
    }

    //Fonction qui permet de changer la valeur de l'icone de vérification
    function changeVerifBool($verifState) {
        $db = & JFactory::getDBO();
        $cid = JRequest::getVar('cid', array(), 'post', 'array');
        $cid = implode(',', $cid);
        $query = 'UPDATE #__jmon_logs' .
                ' SET log_verif = ' . $verifState .
                ' WHERE id_log = ' . $cid;
        $db->setQuery($query);

        if (!$db->query())
            return false;
        $this->setRedirect('index.php?option=com_jmonitoring&task=seeLogs', '');
    }
        //Fonction qui permet de changer la valeur de l'icone de vérification
    function changeVerif() {
        $db = & JFactory::getDBO();
        $lid = JRequest::getVar('lid');
        $verif = JRequest::getVar('verif');
            
        if($verif==0){
            $verif = 1;
        }else{
            $verif = 0;
        }
        $query = 'UPDATE #__jmon_logs' .
                ' SET log_verif = ' . $verif .
                ' WHERE id_log = ' . $lid;
        $db->setQuery($query);
        if (!$db->query())
            return false;
        $this->setRedirect('index.php?option=com_jmonitoring&view=jmon_logs', '');
    }

    function getCsvLogs() {
        
        //$config = & JFactory::getConfig();
        //chargement de la classe pour mettre en csv les requêtes sql
        require_once JPATH_COMPONENT . DS . 'Query2Csv.class.php';
        
        ob_start();
        ini_set('display_errors', 'Off');
        
        $db = & JFactory::getDBO();
        //$model = $this->getModel('jmon_logs');
        //$query_1 = $model->_buildQuery();
        $query_1 = "SELECT l.id_log, s.name as site, l.log_type, l.log_verif, l.log_entry, l.log_comment, l.log_date
            FROM #__jmon_logs l
            INNER JOIN #__jmon_sites s
            ON s.id_site = l.idx_site";
        $csvTitle = 'Log_' . date('Y.m.d');
        $obj = new Query2Csv();
        //generates CSV report
        $obj->generateCsvReport($query_1);
        //gets CSV report
        die($obj->getCsvReport($csvTitle));
    }

}

//class
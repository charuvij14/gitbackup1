<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch}
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport('joomla.application.component.controlleradmin');

class jmonitoringControllerjmon_exts extends JControllerAdmin
{
	function __construct($config = array())
	{
		parent::__construct($config);
	}// function
	
	function display($cachable = false)
	{
    JRequest::setVar( 'view'  , 'jmon_exts');
		parent::display($cachable);
	}// function
	  
  function getCsvExts()
	{

    //chargement de la classe pour mettre en csv les requêtes sql
    require_once JPATH_COMPONENT.DS.'Query2Csv.class.php';

    ini_set('display_errors','Off');
    $db		= & JFactory::getDBO();
    //queries whose output will be used as report data
    $query_1 = "SELECT ext_name AS '".addslashes(JText::_('COM_JMONITORING_JMON_EXT_NAME'))."', version AS '".JText::_( 'COM_JMONITORING_VERSION' )."', 
    date AS '".JText::_( 'COM_JMONITORING_DATE' )."', url
    FROM #__jmon_exts
    WHERE idx_site = ".JRequest::getVar("cid");
  
    $csvTitle = JRequest::getVar("cid").'_'.date('Y.m.d');

    $obj = new Query2Csv();
    
    //generates CSV report
    $obj->generateCsvReport($query_1);
    //gets CSV report
    die($obj->getCsvReport($csvTitle));  
	}	  
}//class
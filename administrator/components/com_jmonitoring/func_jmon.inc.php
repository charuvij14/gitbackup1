<?php
// No direct access to this file
defined('_JEXEC') or die;
/******************************************
* met à jour les champs de valeurs ajoutées
*******************************************/
function dbUpdateValue($value, $id_site, $valueName)
{
  $flag_udpate = 0;   
  $db = JFactory::getDBO();
  $db->setQuery("SELECT value_name, value_new, value_old FROM `#__jmon_more_values` WHERE idx_site = ".$id_site);
  $value_names = $db->loadObjectList();

  foreach($value_names as $name)
  {
    //Si la valeur existe déjà
    if($name->value_name == $valueName)
    {
      //on la met à jour
      $db->setQuery("UPDATE `#__jmon_more_values`
      SET value_old = value_new, value_new = '".$value."'
      WHERE idx_site = ".$id_site." AND value_name = '".$valueName."'");
      $db->query();
      $flag_udpate = 1;
      if($value != $name->value_new)
        return JText::_(strtoupper($name->value_name))." : ".JText::_( 'COM_JMONITORING_JMON_MODIFIED_FILE_VALUE' )." (".$name->value_new." -> ".$value.")"; 
    }
  }

  //si la valeur n'existe pas
  if($flag_udpate == 0)
  {
    //on la créé
    $db->setQuery("INSERT INTO `#__jmon_more_values` (`idx_site`, `value_name`, `value_old`, `value_new`) 
      VALUES ('".$id_site."', '".$valueName."', '', '".$value."')");
    $db->query();
  }
  return null;
}

/******************************************
* change l'état du voyant d'erreur
*******************************************/
function changeErrorTick($id, $state)
{
  $db =& JFactory::getDBO();
  $db->setQuery("UPDATE `#__jmon_sites` SET `error` = '".$state."' WHERE `#__jmon_sites`.`id_site` = ".$id);
  $db->query();
}

/******************************************
* Vérifie la présence du mot spécifié dans la page d'accueil
*******************************************/
function checkCurl($word, $host, $testItTwice)
{
  //Si le mot à vérifier a été défini
  if(!empty($word))
  { 
    $curlCheck = "UNKNOWN";
  
    $result = getPage($host); //Récupération de la page d'accueil
    
    //encodage en UTF-8 de la page distante
    if(!strpos($result, 'charset=utf-8' )) $result = utf8_encode($result);
    
    $result = strtolower($result);
    $word = strtolower($word);

    //Si le mot se trouve dans la page d'accueil
    if (strpos($result, $word))
    {
      $curlCheck = 1; // ok  
    }
    else
    {
      $curlCheck = 2; // erreur
      if(!$testItTwice)
        checkCurl($word, $host, 1);
    }   
    return $curlCheck;
  }
}

/******************************************
* Récupère la page spécifiée par $host avec cUrl
*******************************************/
function getPage($host)
{
  $ch = curl_init();    // initialize curl handle
  curl_setopt($ch, CURLOPT_URL, $host); // set url to post to
  curl_setopt($ch, CURLOPT_FAILONERROR, 1);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20); //timeout de connection
  curl_setopt($ch, CURLOPT_TIMEOUT, 20); // timeout de procédure
  curl_setopt($ch, CURLOPT_USERAGENT, 'JMonitoring/2.0 (+http://www.jmonitoring.com)');
  curl_setopt ($ch, CURLOPT_REFERER, $host);
  $result = curl_exec($ch); // run the whole process
  curl_close($ch);
  
  return $result;
}

/******************************************
* Ecrit une erreur dans le log
* log_type : 0 = info 1 = erreur
*******************************************/
function writeLog($error, $id_site, $log_type)
{
  $db =& JFactory::getDBO();
  $db->setQuery('INSERT INTO #__jmon_logs (idx_site, log_type, log_entry) VALUES ("'.$id_site.'", "'.$log_type.'", "'.addslashes($error).'")');
  $db->query();
}

/******************************************
* effectue une insertion d'extensions dans la base de données
*******************************************/
function makeQuery($extension_type, $extensions, $id_site, $site_name)
{
  $exts[0] = "COMPONENTS";
  $exts[1] = "MODULES";
  $exts[2] = "PLUGINS";
  
  $db = & JFactory::getDBO();

  $query = "INSERT INTO #__jmon_exts (`idx_site`, `type`, `ext_name`, `version`, `date`, `url`) VALUES ";
  $firstQuery = $query;

  foreach ($extensions as $extension)
  {
    $query .= "('" . $id_site . "', '" . $extension_type . "', 
            '" . addslashes($extension->name) . "',
            '" . addslashes($extension->version) . "',
            '" . addslashes($extension->creationdate) . "',
            '" . addslashes($extension->authorurl) . "'), ";
  }
  $finalQuery = substr($query, 0, -2) . ";"; // on enlève les caractères ', ' pour finir la requête
  if ($finalQuery != substr($firstQuery, 0, -2) . ";") //s'il y a des valeurs dans la requête
  {
    $db->setQuery($finalQuery);
    if (!$db->query())
      $retError = JText::_($exts[$extension_type]) . " " . JMON_NOT_SAVED;
  }
  return @$retError;
}
/******************************************
* Vérifie les fichiers
*******************************************/
function checkFilesInfos($files, $id_site)
{    
  if(isset($files)) // s'il y a des fichiers
  {
    $errors = array();
    foreach($files as $file)
    {
      $message = array();
      $name = str_replace("\\","/", $file->rootpath); //nom
      $size = $file->size; //taille
      $mod_date = $file->modificationtime; //date
      $checksum = $file->checksum; //Checksum
      
      //on enregistre
      $isNewSize = dbUpdateValue($size, $id_site, 'size_'.$name);
      $isNewDate = dbUpdateValue($mod_date, $id_site, 'mod_date_'.$name);
      $isNewChecksum = dbUpdateValue($checksum, $id_site, 'checksum_'.$name);
      
      if($isNewSize != null || $isNewDate != null || $isNewChecksum != null)
      {
        //On recherche les anciennes valeurs pour voir s'il y a une erreur
        $db = JFactory::getDBO();
        $db->setQuery("SELECT value_name, value_old, value_new FROM `#__jmon_more_values`
        WHERE idx_site = ".$id_site." AND (value_name LIKE 'size_".$name."' OR value_name LIKE 'mod_date_".$name."' OR value_name LIKE 'checksum_".$name."')");
        $values = $db->loadObjectList();
        
        foreach($values as $value)
          if($value->value_old != $value->value_new)
          {
            if(substr($value->value_name,0,3) == "mod") // si c'est la date de modification
              $message[] = JText::_( 'COM_JMONITORING_JMON_MODIFIED_FILE_DATE' );
            elseif(substr($value->value_name,0,5) == "check") // si c'est le Checksum
              $message[] = JText::_( 'COM_JMONITORING_JMON_MODIFIED_FILE_CHECKSUM' );
            else //c'est la taille du fichier
              $message[] = JText::_( 'COM_JMONITORING_JMON_MODIFIED_FILE_SIZE' );
          }
      }
      
      if($mod_date=="NOT_FOUND")
        $message[] = JText::_( 'COM_JMONITORING_JMON_NOT_EXISTS_FILE' );
      
      if(count($message) > 0)
        $errors[] = $name.' : '.implode(', ',$message);
    }
  }
  return $errors;
}
/*
 * 
 * Manage plugins jmonitoring
 * 
 */
/******************************************
* Efface tous les plugins et leurs valeurs correpondantes d'un site
*******************************************/
function deleteSitePlugins($id_site){
  $db = JFactory::getDBO();
  
  $db->setQuery("SELECT id_plugin FROM #__jmon_plugins WHERE idx_site = " . $id_site);
  $ids_plugins = $db->loadResultArray();
  
  foreach($ids_plugins as $key=>$value){
    $db->setQuery("DELETE FROM #__jmon_plugin_values WHERE idx_plugin = " . $value);
    $db->query(); 
  }

  $db->setQuery("DELETE FROM #__jmon_plugins WHERE idx_site = " . $id_site);
  $db->query();
}
/******************************************
* Ajoute un tableau de plugins et ses valeurs à la bd
*******************************************/
function addSitePlugins($id_site, $plugins, $id_plugin)
{
  $db = JFactory::getDBO();
  $query = "INSERT INTO #__jmon_plugins (`idx_site`, `name`, `description`) VALUES ";
  $firstQuery = $query;
  
  foreach ($plugins as $plugin)
  {
    $id_plugin++;  
    addPluginValues($id_plugin,$plugin->values);
    $query .= "('" . $id_site . "', 
            '" . addslashes($plugin->name) . "',
            '" . addslashes($plugin->description) . "'), ";
  }

  $finalQuery = substr($query, 0, -2) . ";"; //requête finale    

  if ($finalQuery != substr($firstQuery, 0, -2) . ";") { //s'il y a des valeurs dans la requête
    $db->setQuery($finalQuery);
    if (!$db->query())
      $retError = JText::_('ERROR') . " " . JMON_NOT_SAVED;
  }
  return @$retError;
}
/******************************************
* Ajoute un tableau de valeurs d'un plugin à la bd
* @param plugin_id - l'id du plugin
* @param 
*******************************************/
function addPluginValues($plugin_id, $values){
    
    $db = JFactory::getDBO();
    $query = "INSERT INTO #__jmon_plugin_values (`idx_plugin`, `name`, `value`, `unit`, `type` ) VALUES ";
    $firstQuery = $query;
    
    foreach ($values as $value)
    {
      if($value->name)
        $query .= "('" . $plugin_id . "', 
                '" . addslashes($value->name) . "',
                '" . addslashes($value->value) . "',
                '" . addslashes($value->unit) . "',
                '" . addslashes($value->type) . "'), ";
    }

    $finalQuery = substr($query, 0, -2) . ";"; //requête finale    
    
    if ($finalQuery != substr($firstQuery, 0, -2) . ";") { //s'il y a des valeurs dans la requête
        $db->setQuery($finalQuery);
        if (!$db->query())
            $retError = JText::_('ERROR') . " " . JMON_NOT_SAVED;
    }
    return @$retError;  
}
/******************************************
* Rend tous les plugins d'un site sous forme d'une chaine serializée
* les espaces entre les mots sont remplacés par la chaine '0000000000'
*******************************************/
function getSitePlugins($id_site){
    
    $jmonitoringsplugins = array();
      
    $db = JFactory::getDBO();
    $query1 = "SELECT id_plugin, name FROM #__jmon_plugins WHERE idx_site = " . $id_site;
    $db->setQuery($query1);
    $plugins = $db->loadObjectList();
    
    foreach($plugins as $plugin)
    {
        $query2 = "SELECT value FROM #__jmon_plugin_values WHERE idx_plugin = " . $plugin->id_plugin;
        $db->setQuery($query2);
        $value = $db->loadResult();
        $jmonitoringsplugins[] = array('name'=>$plugin->name,'value'=>$value);     
    }

    $jmonitoringsplugins = serialize($jmonitoringsplugins);
    $jmonitoringsplugins = str_replace(' ', '0000000000', $jmonitoringsplugins); 

    return $jmonitoringsplugins;
}
/******************************************
* Rend l'id du dernier plugin 
*******************************************/
function getLastPluginId(){
    $db = JFactory::getDBO();
    $query = "SELECT MAX(id_plugin) as max FROM #__jmon_plugins";
    $db->setQuery($query);
    $last_id = $db->loadObject();
    
    if($last_id->max==null) return 0;
    else return $last_id->max;   
}

/******************************************
* Update version joomla
*******************************************/
function updateJversion($id_site, $jversion)
{
  $flag_udpate = 0;   
  $db = JFactory::getDBO();
  //Ancienne valeur
  $db->setQuery("SELECT j_version FROM `#__jmon_sites` WHERE id_site = ".$id_site);
  $Dbj_version = $db->loadResult();
  
  //Enregistrement de la nouvelle valeur
  $db->setQuery("UPDATE `#__jmon_sites`
      SET j_version =  '".$jversion."'
      WHERE id_site = '".$id_site."'");
  $db->query();

  //si la version a changé, on met une alerte
  if($Dbj_version && $jversion != $Dbj_version)
    return JText::_("COM_JMONITORING_J_VERSION")." : ".JText::_( 'COM_JMONITORING_JMON_MODIFIED_FILE_VALUE' )." (".JTEXT::_($Dbj_version)." -> ".$jversion.")";
}
?>
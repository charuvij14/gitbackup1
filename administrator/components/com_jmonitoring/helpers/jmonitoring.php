<?php

/**
 * @package		jmonitoring
 * @author		Jonathan Fuchs {@link www.inetis.ch}
 */
// no direct access
defined('_JEXEC') or die;

class jmonitoringHelper {
  /**
   * Configure the Linkbar.
   */
  public static function addSubmenu($submenu) {
    if (JRequest::getWord('view') != "jmonitoring" && JRequest::getWord('view') != "jmon_log") {
      JSubMenuHelper::addEntry(JText::_('COM_JMONITORING_MENU_MANAGER'), 'index.php?option=com_jmonitoring&amp;view=jmonitorings', $submenu == 'messages');         
      JSubMenuHelper::addEntry(JText::_('COM_JMONITORING_MENU_LOGS'), 'index.php?option=com_jmonitoring&amp;view=jmon_logs', $submenu == 'messages');
      JSubMenuHelper::addEntry(JText::_('COM_JMONITORING_CATEGORIES'), 'index.php?option=com_categories&amp;extension=com_jmonitoring', $submenu == 'categories');
      JSubMenuHelper::addEntry(JText::_('COM_JMONITORING_MENU_SEARCH'), 'index.php?option=com_jmonitoring&amp;view=jmon_search', $submenu == 'messages');
    }
    $document = &JFactory::getDocument();
    $document->addStyleDeclaration('.icon-48-icon_jmon {background-image: url("components/com_jmonitoring/jmonitoring_picto_48x48.png");}');
  }
}
?>
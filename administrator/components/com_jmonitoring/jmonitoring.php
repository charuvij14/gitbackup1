<?php
/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch}
 */
 
// No direct access to this file
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

// If curl isn't activated, JMonitoring can't work
if(!extension_loaded('curl')) JError::raiseWarning( 500, JText::_( 'COM_JMONITORING_CURL_NOT_FOUND' ) );

$controller = JController::getInstance('jmonitoring');
$controller->registerTask( 'seeError', 'seeError' );

$controller->execute(JRequest::getCmd('task'));

$controller->redirect();
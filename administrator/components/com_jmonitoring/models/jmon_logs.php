<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch}
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport( 'joomla.application.component.modellist' );

class jmonitoringModeljmon_logs extends JModelList
{
	var $_data;
  
	protected function getListQuery()
	{    
            $mainframe =& JFactory::getApplication();
            $search = null;
            $filter_state = null;
            $join_exts = null;
            $str2search = $mainframe->getUserStateFromRequest( "com_jmonitoring.search", 'search', null );
            $filter_verification = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_verification", 'filter_verification', null );
            $filter_type = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_type", 'filter_type', null );
            
            //filtres
            if(!empty($str2search))
            {
              $where [] = "(#__jmon_sites.name LIKE '%".$str2search."%' OR #__jmon_logs.log_entry LIKE '%".$str2search."%' OR #__jmon_logs.log_comment LIKE '%".$str2search."%')";
            }

            if($filter_verification != null)
            {
              $where []= "#__jmon_logs.log_verif = ". $filter_verification;
            }

            if($filter_type != null)
            {
              $where []= "#__jmon_logs.log_type = ". $filter_type;
            }

            $query = ' 
            SELECT * FROM #__jmon_logs 
            LEFT JOIN #__jmon_sites ON #__jmon_sites.id_site = #__jmon_logs.idx_site';
            if(!empty($where)) $query.= " WHERE ".implode(' AND ', $where);
            $query.=' ORDER BY log_date DESC';       

            return $query;
	}
        
        
}

<?php

/**
 * @package		jmonitoring
 * @author		Alan Pilloud {@link www.inetis.ch} Jonathan Fuchs
 * @author		Created on june-2011
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

class jmonitoringModeljmonitoring extends JModelAdmin
{

  public function getTable($type = 'jmonitoring', $prefix = 'jmonitoringTable', $config = array()) 
	{
		return JTable::getInstance($type, $prefix, $config);
	}
  
  public function getForm($data = array(), $loadData = true)
  {
    // Get the form.
    $form = $this->loadForm('com_jmonitoring.jmonitoring', 'jmonitoring', array('control' => 'jform', 'load_data' => $loadData));
    if (empty($form)) return false;
    
    return $form;
  }

  public function getItem($pk = null)
  {
    //die("ij");
    // Initialise variables.
    $pk = (!empty($pk)) ? $pk : (int) $this->getState('jmonitoring.id');

    // Get a row instance.
    $table = $this->getTable();

    // Attempt to load the row.
    $return = $table->load($pk);

    // Check for a table object error.
    if ($return === false && $table->getError()) {
      $this->setError($table->getError());
      return false;
    }

    // Convert to the JObject before adding other data.
    $value = $table->getProperties(1);
    $value = JArrayHelper::toObject($value, 'JObject');

    return $value;
  }
  
  protected function loadFormData() 
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_jmonitoring.edit.jmonitoring.data', array());

		return $data;
	}
	  
  function delete()
  {
    $cids = JRequest::getVar('cid', array(0), 'post', 'array');

    if(count($cids))
    foreach($cids as $cid)
    {
      $db = & JFactory::getDBO();
      //suppression des enregistrement de jmon_exts
      $db->setQuery("DELETE FROM #__jmon_sites WHERE id_site = '" . $cid . "'");
      if (!$db->query()) return false;

      $db->setQuery("DELETE FROM #__jmon_exts WHERE idx_site = '" . $cid . "'");
      if (!$db->query()) return false;

      $db->setQuery("DELETE FROM #__jmon_more_values WHERE idx_site = '" . $cid . "'");
      if (!$db->query()) return false;
      
      $db->setQuery("DELETE FROM #__jmon_plugins WHERE idx_site = '" . $cid . "'");
      if (!$db->query()) return false;
    }//foreach

    return true;
  }
}

// class
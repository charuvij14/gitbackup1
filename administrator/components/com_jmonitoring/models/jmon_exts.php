<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch}
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport( 'joomla.application.component.modellist' );

class jmonitoringModelJmon_exts extends JModelList
{
	var $_data;
        var $_moreInfos;
        var $_plugins;
        var $_sitePlugins;
        var $_pluginsValues;
        
        
	function _buildQuery()
	{
    $query = ' 
    SELECT *, #__categories.title as cat_title, #__jmon_sites.published as state
    FROM #__jmon_sites
    LEFT JOIN #__categories ON #__categories.id = #__jmon_sites.catid 
    LEFT JOIN #__jmon_exts ON #__jmon_sites.id_site = #__jmon_exts.idx_site
    WHERE #__jmon_sites.id_site = '.JRequest::getVar('cid');

		return $query;
	}

	function getData()
	{
		// Lets load the data if it doesn't already exist
		if (empty( $this->_data ))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList( $query );
		}
		
		//die(print_r($this->_data));

		return $this->_data;
	}
	
	function getMoreInfos()
	{
		// Lets load the data if it doesn't already exist
		if (empty( $this->_moreInfos ))
		{
      $query = ' 
      SELECT value_new, value_name FROM #__jmon_more_values
      WHERE #__jmon_more_values.idx_site ='.JRequest::getVar('cid').' 
      AND (value_name LIKE "j_version" 
      OR value_name LIKE "php_version" 
      OR value_name LIKE "mysql_version" 
      OR value_name LIKE "server_version")';

			$this->_moreInfos = $this->_getList( $query );
		}
		
		return $this->_moreInfos;
	}

        function getPlugins(){
            if (empty( $this->_plugins))
            {
              /*
              $query = 'SELECT * 
                FROM #__jmon_plugins as p, #__jmon_plugin_values as v WHERE idx_site='.JRequest::getVar('cid').'
                AND p.id_plugin = v.idx_plugin';
              */
              
              $query= 'SELECT #__jmon_plugins.name,#__jmon_plugins.description,#__jmon_plugins.id_plugin, #__jmon_plugin_values.idx_plugin, #__jmon_plugin_values.key, #__jmon_plugin_values.value
                       FROM #__jmon_plugins
                       INNER JOIN #__jmon_plugin_values
                       ON #__jmon_plugins.id_plugin=#__jmon_plugin_values.idx_plugin
                       WHERE idx_site='.JRequest::getVar('cid').'
                       ORDER BY #__jmon_plugins.name';
              
              
              
              $this->_plugins = $this->_getList( $query );
             
            }
            
            //die(print_r($this->_plugins));
            return $this->_plugins;
        }
        function getSitePlugins(){
            if (empty( $this->_sitePlugins))
            {              
              $query= 'SELECT *
                       FROM #__jmon_plugins
                       WHERE idx_site='.JRequest::getVar('cid');
              
              $this->_sitePlugins = $this->_getList( $query );
             
            }
            return $this->_sitePlugins;
        }
        function getPluginsValues(){
            if (empty( $this->_pluginsValues))
            {              
              $query= 'SELECT *
                       FROM #__jmon_plugin_values';
              
              $this->_sitePlugins = $this->_getList( $query );
             
            }
            return $this->_sitePlugins;
        }
}

<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch}
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport( 'joomla.application.component.modeladmin' );

class jmonitoringModeljmon_log extends JModelAdmin
{
  public function getTable($type = 'jmon_log', $prefix = 'jmon_logTable', $config = array()) 
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	
	public function getForm($data = array(), $loadData = true)
  {
      // Get the form.
      $form = $this->loadForm('com_jmonitoring.jmon_log', 'jmon_log', array('control' => 'jform', 'load_data' => $loadData));
      if (empty($form)) {
          return false;
      }
      return $form;
  }
  
  public function getItem($pk = null)
	{
    // Initialise variables.
    $pk = (!empty($pk)) ? $pk : (int)$this->getState('jmon_log.id');
    $false    = false;

    // Get a row instance.
    $table = $this->getTable();
  
    // Attempt to load the row.
    $return = $table->load($pk);
  
    // Check for a table object error.
    if ($return === false && $table->getError()) {
        $this->setError($table->getError());
        return $false;
    }
  
    // Convert to the JObject before adding other data.
    $value = $table->getProperties(1);
    $value = JArrayHelper::toObject($value, 'JObject');

    return $value;
	}
	
	protected function loadFormData() 
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_jmonitoring.edit.jmon_log.data', array());

		return $data;
	}
	
  function delete()
  {
  	$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );

		if (count( $cids ))
		{
			foreach($cids as $cid) {
				$db =& JFactory::getDBO();
				//suppression des enregistrement de jmon_exts
        $db->setQuery("DELETE FROM #__jmon_logs WHERE id_log = '".$cid."'");
				if(!$db->query()) return false;
			}//foreach
		}
		return true;
  }
}// class
<?php

/**
 * @package		jmonitoring
 * @subpackage	
 * @author		Jonathan Fuchs {@link www.inetis.ch}
 * @author		Created on june-2011
 */
// no direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class jmonitoringModeljmonitorings extends JModelList {

    /**
     * jmonitorings data array
     *
     * @var array
     */
    var $_data;

    /**
     * Returns the query
     * @return string The query to be used to retrieve the rows from the database
     */
    function _buildQuery($hide_state = false, $reqCids = false, $verif = false) {
        $mainframe =& JFactory::getApplication();
        $join_exts = $filter_state = $str_where = $search = null;

        $str2search = $mainframe->getUserStateFromRequest( "com_jmonitoring.search", 'search', null );
        $publishFilter = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_state", 'filter_state', null );
        $validityFilter = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_validity", 'filter_validity', null );
        $catFilter = $mainframe->getUserStateFromRequest( "com_jmonitoring.filter_category_id", 'filter_category_id', null );

        $where = null;
        if ($reqCids != null)
            $where[] = $reqCids;

        //filtres
        if (!empty($str2search)) {
            $join_exts = "LEFT JOIN #__jmon_exts on #__jmon_sites.id_site = #__jmon_exts.idx_site";
            $where[] = "name LIKE '%" . $str2search . "%'";
        }

        if ($publishFilter != null) {
            ($publishFilter == "P") ? $publishFilter = 1 : $publishFilter = 0;
            $where[] = "#__jmon_sites.published = " . $publishFilter;
        }

        if ($validityFilter != null) {
            $where[] = "error = " . $validityFilter;
        }

        if ($catFilter != null) {
            $where[] = "catid = " . $catFilter;
        }

        if ($hide_state)
            $where[] = "#__jmon_sites.published = 1";
        
        if($verif) $where[] = "#__categories.published = 1";
        
        if (count($where) > 0)
            $str_where = " WHERE " . implode(' AND ', $where);

        $query = ' 
    SELECT DISTINCT id_site, #__jmon_sites.*, #__categories.title as cat_title
    FROM #__jmon_sites
    LEFT JOIN #__categories ON #__categories.id = #__jmon_sites.catid
    ' . $join_exts . ' 
    ' . $str_where . '
    ORDER BY name ASC';

        return $query;
    }

    /**
     * Retrieves the hello data
     * @return array Array of objects containing the data from the database
     */
    function getData($hide_state = false, $reqCids = false, $verif = false) {
        // Lets load the data if it doesn't already exist
        if (empty($this->_data)) {
            $query = $this->_buildQuery($hide_state, $reqCids, $verif);
            $this->_data = $this->_getList($query);
        }
        //echo"<pre>";print_r($this->_data); echo"</pre>"; die();
        return $this->_data;
    }

    function publish($cid, $state) {
        $db = & JFactory::getDBO();
        // Get some variables for the query
        $total = count($cid);
        $cids = implode(',', $cid);
        $query = 'UPDATE #__jmon_sites' .
                ' SET state = ' . $state .
                ' WHERE id_site IN ( ' . $cids . ' )';
        $db->setQuery($query);
        if (!$db->query()) {
            return false;
        }
        return true;
    }
    /**
     *permet d'initialiser la pagination
     */
  protected function getListQuery()
	{
		// Create a new query object.		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select('id_site');
		// From the hello table
		$query->from('#__jmon_sites');
		return $query;
	}

}

<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch}
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die;

jimport( 'joomla.application.component.model' );

class jmonitoringModelJmon_search extends JModel
{
	var $_data;

	function _buildQuery()
	{
    $search = $filter_state = $search_version = $join_exts = null;
    $where = array();
    // CHAMPS DE RECHERCHE POUR LES SITES
    $site2search = JRequest::getVar('search_name', null);
    $search_version = JRequest::getVar('search_version', null);
    $publishFilter = JRequest::getVar('filter_state', null);
    $validityFilter = JRequest::getVar('filter_validity', null);
    $catFilter = JRequest::getVar('filter_category_id', null);
    // CHAMPS DE RECHERCHE POUR LES EXTENSIONS
    $ext2search = JRequest::getVar('search_ext_name', null);   
    $search_ext_version = JRequest::getVar('search_ext_version', null);
    $typeFilter = JRequest::getVar('filter_type', null);

    /////
    //FILTRES POUR LES SITES
    if($site2search != null)
    {
      $where[] = "name LIKE '%".$site2search."%'";
      $join_exts = null;
    }
    
    if($publishFilter != null)
    {
      $where[] = "published = ". $publishFilter;
      $join_exts = null;
    }
    
    if($validityFilter != null)
    {
      $where[] = "error = ". $validityFilter;
      $join_exts = null;
    }

    if($search_version != null)
    {
      $where[] = "j_version LIKE '%".$search_version."%'";
      $join_exts = null;
    }
    if($catFilter != null)
    {
      $where[] = "catid = ".$catFilter;
      $join_exts = null;
    }    
    /////
    //FILTRES POUR LES EXTENSIONS
    if($ext2search != null)
    {
      $where[] = "ext_name LIKE '%".$ext2search."%'";
      $join_exts = "LEFT JOIN #__jmon_exts ON #__jmon_sites.id_site = #__jmon_exts.idx_site";
    }

    if($search_ext_version != null)
    {
      $where[] = "version LIKE '%".$search_ext_version."%'";
      $join_exts = "LEFT JOIN #__jmon_exts ON #__jmon_sites.id_site = #__jmon_exts.idx_site";
    }     
 
    if($typeFilter != null)
    {
      $where[] = "type LIKE '%".$typeFilter."%'";
      $join_exts = "LEFT JOIN #__jmon_exts ON #__jmon_sites.id_site = #__jmon_exts.idx_site";
    }
    
    if(count($where)>0)$where = "WHERE ".implode(' AND ',$where);

    if($join_exts != null) $where .= " ORDER BY type ASC";

    return '
    SELECT * FROM #__jmon_sites
    '.$join_exts.' '.$where;
	}

	function getData()
	{
		// Lets load the data if it doesn't already exist
		if (empty( $this->_data ))
		{
			$query = $this->_buildQuery();
			$this->_data = $this->_getList( $query );
		}
		return $this->_data;
	}

}

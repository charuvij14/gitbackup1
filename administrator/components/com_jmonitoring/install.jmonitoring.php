<?php
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.installer.installer');

//Mise à jour de 2.2 à 2.2.1, ajout du champ admin_url
$db =& JFactory::getDBO();
$db->setQuery("ALTER TABLE `#__jmon_sites` ADD `admin_url` varchar(250) NOT NULL default '' AFTER `access_url`;");
$db->query();

$config = JFactory::getConfig();
?>
<img style="float:left;margin-right:10px;" src="http://www.jmonitoring.com/templates/site_base/images/jmonitoring_logo_200.png" alt="JMonitoring" />

<div style="float:left;padding:10px;">
  <div>Thanks for using JMonitoring !<br/>The master component has been installed on your website.<br/>Your Master Website is now ready to monitor your Distant Websites</div>
</div>
<br style="clear:both"/>
  <h3>Warning</h3>
  <p>The Master Component's cron url has changed : a token is now required.<br/>Please use <?php echo JUri::root(); ?>index.php?option=com_jmonitoring&task=cron&token=<?php echo $config->getValue('secret'); ?></p>
  <h3>JMonitoring's useful links</h3>
  <ul>
    <li><a href="http://www.jmonitoring.com/en/features.html">Features</a></li>
    <li><a href="http://www.jmonitoring.com/en/guide-to-monitoring.html">Guide</a></li>
    <li><a href="http://www.jmonitoring.com/en/forum/index.html">Support forum</a></li>
    <li><a href="http://www.jmonitoring.com/en/guide-to-monitoring/developpeurs.html">Developpers's Guide for JMonitoring plugins</a></li>
  </ul>
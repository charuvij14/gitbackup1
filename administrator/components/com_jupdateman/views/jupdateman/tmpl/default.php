<?php
/**
 * View: JUpdateMan Default Template 
 * 
 * PHP4/5
 *  
 * Created on Sep 28, 2007
 * 
 * @package JUpdateMan
 * @author Sam Moffatt <pasamio@gmail.com>
 * @license GNU/GPL http://www.gnu.org/licenses/gpl.html
 * @copyright 2009 Sam Moffatt 
 * @version SVN: $Id:$
 * @see JoomlaCode Project: http://joomlacode.org/gf/project/pasamioprojects
 */
 
// no direct access
defined('_JEXEC') or die('Restricted access'); ?>
	<div align="left">
		<p> Bienvenido al componente que gestiona las actualizaciones de los packs <span style="font-weight: bold; color: black">Joomla! Spanish.</span> Mi trabajo es guiarlo a través del proceso para actualizar la instalación de Joomla! Spanish. </ p>
<p> Si usted tiene un servidor proxy, necesitará introducir sus datos en la pantalla de preferencias. Opcionalmente tambien puede seleccionar el metodo de extracción que se desea usar (los paquetes vienen comprimidos con la extensión  tar.gz)</ p>
<p> Se trata de un simple paso a paso durante el proceso, ojalá sea lo más simple posible. Estos son los pasos: </ p>
<ol>
<li> Descargar el archivo XML con los datos necesarios para actualizar y seleccionar el archivo del paquete. </ li>
<li> Descargar el archivo del paquete recomendado y la pantalla le mostrara el mensaje habitual '¿Está seguro? "  </ li>
<li> Mensaje de actualización completada </ li>
		</ol>
		<?php if($this->calculated_tmp_path != $this->config_tmp_path) : ?>
			<p style="font-weight:bold; color: orange;"> Advertencia: ruta temporal potencialmente no válido.<br /> 
				Ruta configurada: <span style="font-weight: bold; color: black"><?php echo $this->config_tmp_path ?></span><br />
				Ruta sugerida: <span style="font-weight: bold; color: black"><?php echo $this->calculated_tmp_path ?></span>
			</p>
		<?php endif; ?>
		<?php if($this->http_support || $this->curl_support) : ?>	
			<?php if(!$this->http_support && $this->current_method == JUPDATEMAN_DLMETHOD_FOPEN) : ?>
				<p style="font-weight:bold; color: red;">Nota: Usted tiene fopen seleccionado, pero el servidor no parece compatible con este método. Trate de seleccionar cURL en su lugar.</p>
			<?php endif; ?>
			<?php if(!$this->curl_support && $this->current_method == JUPDATEMAN_DLMETHOD_CURL): ?>
				<p style="font-weight:bold; color: red;">Nota: Usted ha seleccionado cURL, pero el servidor no parece compatible con este método. Trate de seleccionar fopen en su lugar.</p>
			<?php endif; ?>
			<p>Así que vamos a continuar con el proceso de actualización <a href="index.php?option=com_jupdateman&task=step1"> <h2>Descargar el archivo (XML) de actualización desde joomlaspanish.org&gt;&gt;&gt;</a></h2></p>
		<?php else: ?>
			<p>Su Joomla!, no es compatible con envolturas HTTP o cURL fopen. Esta herramienta no se puede utilizar a menos que tenga alguna de estas características.</p>
		<?php endif; ?>
	</div>
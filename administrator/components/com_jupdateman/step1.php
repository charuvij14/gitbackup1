<?php
/**
 * Joomla! Upgrade Helper
 * Step 1 - Download XML update file and display download options
 */
/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

$v = new JVersion();
$version = $v->getShortVersion();

$url = "http://joomlaspanish.org/update/packs/update.xml";

$config =& JFactory::getConfig();
$tmp_path = $config->getValue('config.tmp_path');

$params =& JComponentHelper::getParams('com_jupdateman');




$target = $tmp_path . DS . 'jupgrader.xml';

$cached_update = $params->get('cached_update', 0);

if($cached_update) {
	if(!file_exists($target)) {
		HTML_jupgrader::showError( 'falta el archivo de actualización. <a href="'. $url .'" target="_blank">descargar la actualización de definiciones del archivo</a> y ponla en tu directorio temporal /tmp como "jupgrader.xml".<br />ruta de destino: '. $target);
		return false;
	}
} else {
	$result = downloadFile($url,$target);
	if(is_object( $result )) {
		HTML_jupgrader::showError( 'Error en la descarga: '. $result->message . '('. $result->number . ')' );
		return false;
	}
}

if(!file_exists($target)) {
	HTML_jupgrader::showError( 'Update file does not exist: '. $target );
}

// Yay! file downloaded! Processing time :(
$xmlDoc = new JSimpleXML();

if (!$xmlDoc->loadFile( $target )) {
	HTML_jupgrader::showError( 'Parsing XML Document Failed!' );
	return false;
}

//$root = &$xmlDoc->documentElement;
$root = &$xmlDoc->document;

if ($root->name() != 'update') {
	HTML_jupgrader::showError( 'Parsing XML Document Failed: Not an update definition file!' );
	return false;
}
$rootattributes = $root->attributes();
$latest = $rootattributes['release'];
if($latest == $version) {
	echo "<p>No se encontraron actualizaciones . <a href='index.php?option=com_jupdateman&task=step2&target=full'>Forzar una actualización completa &gt;&gt;&gt;</a></p><br /><br /><p>Por favor, compruebe de nuevo más tarde o ver <a href='http://www.joomlaspanish.org' target='_blank'>www.joomlaspanish.org</a></p>";
	echo '</div>';
	return true;
} elseif(version_compare($latest, $version, '<')) {
	echo "<p>Está ejecutando una versión mayor de Joomla! de lo que está disponible para su descarga.</p><br /><br />";
	echo "<p>Por favor compruebe en <a href='http://www.joomlaspanish.org' target='_blank'>www.joomlaspanish.org</a> para informarse de la release .</p>";
	echo '</div>';
	return true;
}

$updater = $root->getElementByPath('updater', 1);
if(!$updater) {
	HTML_jupgrader::showError( 'No se pudo obtener la actualización del elemento. Posible actualización no válida!');
	return false;
}

$updater_attributes = $updater->attributes();

$session =& JFactory::getSession();
$session->set('jupdateman_updateurl', $updater->data());

if(version_compare($updater_attributes['minimumversion'], getComponentVersion(), '>')) 
{
	echo '<p>La versión de actualización actual es inferior a la versión mínima de esta actualización.</p>';
	echo '<p>Por favor, actualice esta extensión. Esto puede ser tratado de forma automática o se puede descargar la actualización e instalarla usted mismo.</p>';
	echo '<ul>';
	echo '<li><a href="index.php?option=com_jupdateman&task=autoupdate">Actualizar automáticamente &gt;&gt;&gt;</a></li>';
	echo '<li><a target="_blank" href="'. $updater->data() .'">Descargar e instalar el paquete de forma manual (nueva ventana) &gt;&gt;&gt;</a></li>';
	echo '</ul>';
	return false;
}

if(version_compare($updater_attributes['currentversion'], getComponentVersion(), '>')) 
{
	echo '<p>An update ('. $updater_attributes['currentversion'] .') is available for this extension. You can <a href="index.php?option=com_jupdateman&task=autoupdate">update automatically</a> or <a target="_blank" href="'. $updater->data() .'">manually download</a> and install the update.</p>';
}

echo "<p>Usted está actualmente ejecutando $version. La última versión es en la actualidad $latest.  Por favor, seleccione una descarga:</p>";
$fulldownload = '';
$patchdownload = '';

// Get the full package
$fullpackage  				= $root->getElementByPath( 'fullpackage', 1 );
$fullpackageattr 			= $fullpackage->attributes();
$fulldetails 				= new stdClass();
$fulldetails->url 			= $fullpackageattr['url'];
$fulldetails->filename 		= $fullpackageattr['filename'];
$fulldetails->filesize 		= $fullpackageattr['filesize'];
$fulldetails->md5			= $fullpackageattr['md5'];

// Find the patch package
$patches_root = $root->getElementByPath( 'patches', 1 );
if (!is_null( $patches_root ) ) {
	// Patches :D
	$patches = $patches_root->children();
	if(count($patches)) {
		// Many patches! :D
		foreach($patches as $patch) {
			$patchattr = $patch->attributes();
			if ($patchattr['version'] == $version) {
				$patchdetails->url = $patchattr['url'];
				$patchdetails->filename = $patchattr['filename'];
				$patchdetails->filesize = $patchattr['filesize'];
				$patchdetails->md5		= $patchattr['md5'];
				break;
			}
		}

	}
}

$message_element = $root->getElementByPath('message');
if($message_element) {
	$message = $message_element->data();
	if(strlen($message)) {
		echo '<p style="background: lightblue; padding: 5px; spacing: 5px; border: 1px solid black;"><b>Update Message:</b><br /> '. $message.'</p>';
	}
}
$session->set('jupdateman_fullpackage', $fulldetails);
$session->set('jupdateman_patchpackage', $patchdetails);
?>
<ul>
	<li><a
		href="index.php?option=com_jupdateman&task=step2&target=full">Paquete completo</a> (<?php echo round($fulldetails->filesize/1024/1024,2) ?>MB)<?php
	if($cached_update && !file_exists($tmp_path.DS.$fulldetails->filename)) {
		echo ' - <a href="'. $fulldetails->url .'" target="_blank">Descargar el archivo y subirlo primero a su directorio temporal</a>';
	}
	?></li>
	<?php if($patchdetails) { ?>
	<li><a
		href="index.php?option=com_jupdateman&task=step2&target=patch">Paquete con el parche
	</a> (<?php echo round($patchdetails->filesize/1024/1024,2) ?>MB)<?php
	if($cached_update && !file_exists($tmp_path.DS.$patchdetails->filename)) {
		echo ' - <a href="'. $patchdetails->url .'" target="_blank">Descargar el archivo y subirlo primero a su directorio temporal</a>';
	}
	?></li>
	<?php } ?>
</ul>
<p> Nota: El paquete parche (Patch) contiene sólo los archivos modificados y debería ser adecuado
para la mayoría de las actualizaciones. Importantes mejoras (como por ejemplo, 1.5.x para 1.6) probablemente
requiere un paquete completo. </ p>
<?php if($cached_update) : ?>
	<p style="font-weight:bold">Nota: Se utiliza el modo de caché de actualización. Usted tendrá que cargar los archivos de actualización en el directorio temporal antes de continuar.</p>
	<p>Su directorio temporal: <span style="font-style:italic"><?php echo $tmp_path ?></span></p>
	<?php
endif;
<?php
/**
 * Joomla! Upgrade Helper
 */
/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

$session =& JFactory::getSession();
switch(JRequest::getVar('target', 'patch')) {
	default:
	case 'patch':
		$details = $session->get('jupdateman_patchpackage');
		break;
	case 'full':
		$details = $session->get('jupdateman_fullpackage');
		break;
}
if(!is_object($details)) {
	$app =& JFactory::getApplication();
	$app->redirect('index.php?option=com_jupdateman&task=step1'); // back to step one if invalid session 
}
$url  = $details->url;
$file = $details->filename;
$size = $details->filesize;
$md5  = $details->md5;

@set_time_limit(0); // Make sure we don't timeout while downloading
$config =& JFactory::getConfig();
$tmp_path = $config->getValue('config.tmp_path');
$file_path = $tmp_path.DS.$file;
 
$params =& JComponentHelper::getParams('com_jupdateman');



if(!$params->get('cached_update', 0)) {
	$result = downloadFile($url,$file_path);
	if(is_object( $result )) {
		HTML_jupgrader::showError( 'fallo en la descarga: '. $result->message . '('. $result->number . ')</p>' );
		return false;
	}
} else {
	if(!file_exists($file_path)) {
		HTML_jupgrader::showError('El archivo no existe y se ejecuta en modo de caché. Sube el archivo de actualización <a href="'. $details->url .'" target="_blank">Descargar archivo aquí.</a> <br /><a href="index.php?option=com_jupdateman&task=step2">Recargar página.</a>');
		return false;
	}
}

if(strlen($md5)) {
	if(md5_file($file_path) != $md5) {
		HTML_jupgrader::showError('Error: control checksum MD5 no coincide! Borrar y volver a descargar el archivo.');
		return false;
	}
} else {
	echo '<p>Advertencia: Este archivo no tiene un hash MD5 para validar!</p>';
}

$session->set('jupdateman_filename', $file);

?>
<p>El archivo '<?php echo $file ?>' ha sido descargado de: <a href="<?php echo $url ?>" target="_blank"><?php echo $url ?></a>.</p>
<p> Si está seguro que desea utilizar este método, <a href="index.php?option=com_jupdateman&task=step3">se puede proceder con la instalación
&gt;&gt;&gt;</a></p>
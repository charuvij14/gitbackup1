<?php
/**
  * @version       1.0 +
  * @package       Open Source Excellence Marketing Software
  * @subpackage    Open Source Excellence Affiates - com_ose_affiliates
  * @author        Open Source Excellence (R) {@link  http://www.opensource-excellence.com}
  * @author        Created on 01-Oct-2011
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2010- Open Source Excellence (R)
*/
// No direct access
defined('_JEXEC') or die;
/**
 * Content component helper.
 *
 * @package		Joomla.Administrator
 * @subpackage	com_content
 * @since		1.6
 */
class OSESoftHelper
{
	public static $extension= '';
	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	$vName	The name of the active view.
	 *
	 * @return	void
	 * @since	1.6
	 */
	public function __construct()
	{
		self::$extension= self::getExtensionName();
		$version= new JVersion();
		$version= substr($version->getShortVersion(), 0, 3);
		if(!defined('JOOMLA16'))
		{
			$value=($version >= '1.6') ? true : false;
			define('JOOMLA16', $value);
		}
	}
	public static function getExtensionName()
	{
		return 'com_ose_fileman';
	}
	public static function showmenu()
	{
		self::$extension= self::getExtensionName();
		$vName= JRequest :: getCmd('view', 'tabs');
		$db= JFactory :: getDBO();
		if(JOOMLA16 == true)
		{
			$query= "SELECT * FROM `#__menu` WHERE `alias` =  'OSE File Manager™'";
			$db->setQuery($query);
			$results= $db->loadResult();
			if(empty($results))
			{
				$query= "UPDATE `#__menu` SET `alias` =  'OSE File Manager™', `path` =  'OSE File Manager™', `published`=1, `img` = '\"components/com_ose_fileman/favicon.ico\"'  WHERE `component_id` = ( SELECT extension_id FROM `#__extensions` WHERE `element` ='com_ose_fileman')  AND `client_id` = 1 ";
				$db->setQuery($query);
				$db->query();
			}
		}

		$view= JRequest :: getVar('view');
		echo '<div class="menu-search">';
		echo '<ul>';
		echo '<li ';
		echo($view == 'directories') ? 'class="current"' : '';
		echo '><a href="index.php?option='.self::$extension.'&view=directories">'.JText :: _('File Manager').'</a></li>';
		
		if (file_exists(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_antivirus'.DS.'admin.ose_antivirus.php'))
		{
			echo '<li ';
			echo($view == 'scan') ? 'class="current"' : '';
			echo '><a href="index.php?option=com_ose_antivirus&view=scan">'.JText :: _('<< Back to OSE Anti Virus').'</a></li>';
			
		}
		echo '<li ';
		echo($view == 'aboutose') ? 'class="current"' : '';
		echo '><a href="index.php?option='.self::$extension.'&view=aboutose">'.JText :: _('About OSE').'</a></li>';
		
		echo '</ul></div>';
	}
	function getVersion()
	{
		$folder= JPATH_ADMINISTRATOR.DS.'components'.DS.self::$extension;
		if(JFolder :: exists($folder))
		{
			$xmlFilesInDir= JFolder :: files($folder, '.xml$');
		}
		else
		{
			$folder= JPATH_SITE.DS.'components'.DS.$this->extension;
			if(JFolder :: exists($folder))
			{
				$xmlFilesInDir= JFolder :: files($folder, '.xml$');
			}
			else
			{
				$xmlFilesInDir= null;
			}
		}
		$xml_items= '';
		if(count($xmlFilesInDir))
		{
			foreach($xmlFilesInDir as $xmlfile)
			{
				if($data= JApplicationHelper :: parseXMLInstallFile($folder.DS.$xmlfile))
				{
					foreach($data as $key => $value)
					{
						$xml_items[$key]= $value;
					}
				}
			}
		}
		if(isset($xml_items['version']) && $xml_items['version'] != '')
		{
			return $xml_items['version'];
		}
		else
		{
			return '';
		}
	}
	function ajaxResponse($status, $message, $data= null, $url= null)
	{
		$return['title']= $status;
		$return['content']= $message;
		$return['data']= $data;
		$return['url']= $url;
		echo oseJSON :: encode($return);
		exit;
	}
	function returnMessages($status, $messages)
	{
		$result= array();
		if($status == true)
		{
			$result['success']= true;
			$result['status']= 'Done';
			$result['result']= $messages;
		}
		else
		{
			$result['success']= false;
			$result['status']= 'Error';
			$result['result']= $messages;
		}
		$result= oseJSON :: encode($result);
		oseExit($result);
	}
	public static function renderOSETM()
	{
		$a = base64_decode('PGRpdiBpZCA9ICJvc2Vmb290ZXIiPjxkaXYgY2xhc3M9ImZvb3Rlci10ZXh0Ij5Qb3dlcmVkIGJ5IDxhIGhyZWY9Imh0dHA6Ly93d3cub3BlbnNvdXJjZS1leGNlbGxlbmNlLmNvbS8iIHN0eWxlPSJ0ZXh0LWRlY29yYXRpb246IG5vbmU7IiB0YXJnZXQ9Il9ibGFuayIgdGl0bGU9Ik9TRSBGaWxlIE1hbmFnZXIiPk9TRSBGaWxlIE1hbmFnZXLihKIg');
		$a.= '<small><small>[Version: '.OSEFILEMANVERSION.']</small></small></a></div></div>';
		return $a;
	}
	public static function getFolderName($class)
	{
		$classname = explode("View", get_class($class));
		$classname = strtolower($classname[1]);
		return $classname;
	}
	public function loadCats($cats = array())
    {
        if(is_array($cats))
        {
            $i = 0;
            $return = array();
            foreach($cats as $JCatNode)
            {
                $return[$i]->title = $JCatNode->title;
                $return[$i]->cat_id = $JCatNode->id;
                if($JCatNode->hasChildren())
                    $return[$i]->children = self::loadCats($JCatNode->getChildren());
                else
                    $return[$i]->children = false;

                $i++;
            }
            return $return;
        }
        return false;
    }
	public function loadCatTree($cats = array(), $return = array() )
    {
        if(is_array($cats))
        {
            $i = 0;
            $curreturn = array();
            foreach($cats as $JCatNode)
            {
               $curreturn[$i]->title = '['.$JCatNode->id.'] '.str_repeat('-', $JCatNode->level-1).' '.$JCatNode->title;
               $curreturn[$i]->cat_id = $JCatNode->id;

                if($JCatNode->hasChildren())
                {
                	$subreturn = self::loadCats($JCatNode->getChildren(), $return);
                }
                $i++;
            }
            $return = array_merge($curreturn, $return);
            //$return = array_unique($return);
            return $return;
        }
        return false;
    }
    function loadOrders($table)
    {
		$db= JFactory :: getDBO();
		$query= "SELECT CONCAT (`ordering`, ' - ', `title`) as title, `ordering` FROM `{$table}` ORDER BY `ordering` ASC";
		$db->setQuery($query);
		$results= $db->loadObjectList();
		return (!empty($results))?$results:null;
    }

	public static function checkToken($method = 'post')
	{
		$token = $this->oseGetToken();
		if (!JRequest::getVar($token, '', $method, 'alnum'))
		{
			$session = JFactory::getSession();
			if ($session->isNew()) {
				// Redirect to login screen.
				$app = JFactory::getApplication();
				$return = JRoute::_('index.php');
				self::ajaxResponse ('ERROR', JText::_('JLIB_ENVIRONMENT_SESSION_EXPIRED'));
			} else {
				self::ajaxResponse ('ERROR', JText::_('Token invalid'));
			}
		} else {
			return true;
		}
	}
	public static function getToken()
	{
		$token = self::oseGetToken();  
		$html = '<input type="hidden" value="1" name="'.$token.'">';
		return $html;
	}
	public function changeStatus($table)
	{
		$db= JFactory :: getDBO();
		$id= JRequest :: getInt('id', 0);
		$status= JRequest :: getInt('published', 1);
		if(empty($id))
		{
			return false;
		}
		$query= "UPDATE `{$table}` SET `published` =  ". $db->Quote($status)." WHERE `id` = ". (int)$id;
		$db->setQuery($query);
		if($db->query())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public static function oseGetToken()
	{
		if (JOOMLA30==true)
		{
			$token = JSession::getFormToken() ;
		}
		else
		{
			$token = JUtility::getToken() ;
		}
		return $token; 
	}
	public static function getPreviewMenus()
	{
		$token = self::oseGetToken();
		$logoutLink = JRoute::_('index.php?option=com_login&task=logout&'. $token.'=1');
		$hideLinks	= JRequest::getBool('hidemainmenu');

		$output = '<div id="preview_menus">';
		$jedurl = 'http://extensions.joomla.org/extensions/core-enhancements/file-management/15977';
		$fbdurl = 'http://www.facebook.com/pages/OSE-File-Manager/352923958106165';
		
		$output .= '<span class="viewsite"><a href="'.$jedurl.'" target="_blank">'.JText::_('Vote us in JED').'</a></span>';
		$output .= '<span class="viewsite"><a href="'.$fbdurl.'" target="_blank">'.JText::_('Like us in Facebook').'</a></span>';
		
		$output .= '<span class="viewsite"><a href="'.JURI::root().'" target="_blank">'.JText::_('JGLOBAL_VIEW_SITE').'</a></span>';
		$output .= '<span class="backtojoomla"><a href="'.JURI::root().'administrator/" >'.JText::_('Back to Control Panel').'</a></span>';
		$output .= "</div>";
		
		return $output;
	}
	function oseadmin_check()
	{
		// Perform Administrator Permission Checking ;
		$mainframe= JFactory :: getApplication();
		$access = self::getAdminLevel(); 
		if($access == false) {
			$mainframe->redirect('index.php', "This is for administrator only");
		}
	}
	public static function getAdminLevel()
	{
		$mainframe= JFactory :: getApplication();
		$document= JFactory :: getDocument();
		$user= JFactory :: getUser();
		$database= JFactory :: getDBO();
		
		$access= true;
		if(JOOMLA16 == false) {
			$acl= JFactory :: getACL();
			$myRealGid= intval($acl->get_group_id($user->usertype));
			//check to see if there superadmin, if not redirect them.
			if($myRealGid != 25) {
				$access= false;
			}
		} else {
			$db = JFactory::getDBO();
			$db->setQuery("SELECT id FROM #__usergroups");
			$groups = $db->loadObjectList();
		
			$admin_groups = array();
			
			foreach ($groups as $group)
			{
				if (JAccess::checkGroup($group->id, 'core.login.admin'))
				{
					$admin_groups[] = $group->id;
				}
				elseif (JAccess::checkGroup($group->id, 'core.admin'))
				{
					$admin_groups[] = $group->id;
				}
			}
			$admin_groups = array_unique($admin_groups);
			$user_groups = JAccess::getGroupsByUser($user->id);
		
			if (count(array_intersect($user_groups, $admin_groups))>0)
			{
				$access=  true;
			}
			else
			{
				$access=  false;
			}
		}
		return $access; 
	}
}
?>
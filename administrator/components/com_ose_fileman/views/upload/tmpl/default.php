<?php
/**
 * @version     4.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence File Manager
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 23-Apr-2012
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */

defined('_JEXEC') or die("Direct Access Not Allowed");
?>

<div id="oseheader">
	<div class="container">
		<div class="logo-labels">
			<h1>
				<a href="http://www.opensource-excellence.com" target="_blank"><?php echo JText::_("Open Source Excellence"); ?>
				</a>
			</h1>
				<?php
					echo $this->preview_menus; 
				?>
		</div>
				<?php
					oseSoftHelper::showmenu();
				?>
	<div class="section">
		<div id="sectionheader"><?php echo $this->title; ?></div>
		<div class="grid-title"><?php echo JText::_('Please upload your files here').'. '; 
		echo JText::_("Maximum File Size =").((get_max_file_size() / 1024) / 1024)." ".JText::_("MB").'; ';
		echo JText::_("Maximum Upload Limit =").((get_max_upload_limit() / 1024) / 1024)." ". JText::_("MB");
		echo "<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"". get_max_file_size()."\" />";
		
		?> </div>
		<div id ='ose-fileman-upload'>
		<form enctype="multipart/form-data" action="index.php?option=com_ose_fileman&controller=files" method="post">
		
		<table class="adminform">
		  <?php 
		  	  for($i=0;$i<10;$i++) {
	 				$class = $i % 2 ? 'row0' : 'row1';
	 				echo "<tr class=\"$class\">";
	 				echo "<td colspan=\"2\" style = 'text-align: center'>";
	 				echo "<input name=\"userfile[]\" type=\"file\" size=\"50\" class=\"inputbox\" />"; 
	 				echo "</td>"; 
	 				echo "</tr>";
	 			}
	 	  ?>
		 	  <tr>
		 	  	<td colspan="2"  style = 'text-align: center'>
		 			<input type="checkbox" checked="checked" value="1" name="overwrite_files" id="overwrite_files" /><label for="overwrite_files"><?php echo $GLOBALS["messages"]["overwrite_files"] ; ?></label>
		 		</td>
		 	  </tr>
	 		
			  <tr class='button_bar'>
				<td colspan="2" style='text-align: right'>
					<input type="submit" id="uploadbtn" name="uploadbtn" value ="<?php echo $GLOBALS["messages"]["btnupload"]; ?>"/>
			    	<button id ="closebtn"><?php echo $GLOBALS["messages"]["btncancel"]; ?></button>
			    	<input type="hidden" name="dir" id="dir" value="<?php echo $this->dir; ?>" />
					<input type="hidden" name="file" id="file" value="<?php echo $this->fileName; ?>" />
					<input type='hidden' name='returnDir' id = 'returnDir' value = '<?php echo get_abs_item($this->dir, $this->fileName); ?>'/>
					<input type='hidden' name='task' id = 'task' value = 'upload_files'/>
			    </td>
			  </tr>
	        </table>
	      </form>  
			<?php echo OSESoftHelper::getToken();  ?>
		</div>
	</div>
 </div>
</div>


<?php
echo oseSoftHelper::renderOSETM();
?>
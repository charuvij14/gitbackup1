<?php


/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2008 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if( !defined( '_JEXEC' ) && !defined( '_VALID_MOS' ) ) die( 'Restricted access' );

if (!defined('_JEXEC')) define('_JEXEC', 1);
if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);

//Fix by el_oskaros
$jPathBase2 = str_replace(DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'index.php','',$_SERVER['SCRIPT_FILENAME']);
$jPathBase2 = str_replace(DIRECTORY_SEPARATOR.'administrator'.DIRECTORY_SEPARATOR.'index2.php','',$jPathBase2);

/* replace all possible combinations */
$jPathBase2 = str_replace('/administrator/index.php','',$jPathBase2);
$jPathBase2 = str_replace('\administrator\index.php','',$jPathBase2);
$jPathBase2 = str_replace('/administrator/index2.php','',$jPathBase2);
$jPathBase2 = str_replace('\administrator\index2.php','',$jPathBase2);

define('JPATH_BASE2',$jPathBase2);


if (!defined('JPATH_ROOT'))
    require_once (JPATH_BASE2 . DS . 'includes' . DS . 'defines.php');

if (!defined('JDEBUG'))
    @ require_once (JPATH_BASE2 . DS . 'includes' . DS . 'framework.php');

if (version_compare(JVERSION, '1.5.0', '<')) {
    $mainframe = & JFactory :: getApplication('site');
    $mainframe->initialise();
}


require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "config.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.ip2country.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.block.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.block.html.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.cache.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.config.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.goal.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.goal.html.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.helper.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.stat.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.stat.html.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.visit.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.visit.html.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.visit.history.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.visit.history.html.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.trend.html.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.db.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.html.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.log.php");
require_once (JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "admin.joomlawatch.html.php");


$joomlaWatch = new JoomlaWatch();
$joomlaWatchHTML = new JoomlaWatchHTML();
$joomlaWatchGoalHTML = new JoomlaWatchGoalHTML($joomlaWatch);
$joomlaWatchTrendHTML = new JoomlaWatchTrendHTML($joomlaWatch);

$action = @ JoomlaWatchHelper::requestGet('action');
$task = @ JoomlaWatchHelper::requestGet('task');
$option = @ JoomlaWatchHelper::requestGet('option');

switch ($task) {

    case "licenseAccepted" :
        {
            $joomlaWatch->config->setLiveSite(JURI::root());
            $joomlaWatch->config->setLicenseAccepted();
            $joomlaWatch->config->saveVersionIntoDatabase();
            break;
        }
}

if (!$joomlaWatch->config->checkLicenseAccepted()) {
    $joomlaWatchHTML->renderAdminStyles();
    echo $joomlaWatchHTML->renderAcceptLicense();
} else {
    //    $timeLeft = $joomlaWatch->config->getTrialVersionTimeLeft();
    //    if ($timeLeft > 0 || $joomlaWatch->config->isAdFree()) {

    switch ($task) {

        case "activate" :
            {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                $joomlaWatch->config->activate(JoomlaWatchHelper::requestGet('key'));
                $joomlaWatchHTML->renderAdFreeLicense();
                break;
            }

        case "graphs" :
            {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                echo $joomlaWatchTrendHTML->renderGraphsForGroup(JoomlaWatchHelper::requestGet('group'));

                break;
            }

        case "trends" :
            {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                echo $joomlaWatchTrendHTML->renderTrends();

                break;
            }

        case "credits" :
            {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                echo $joomlaWatchHTML->renderCredits();

                break;
            }
        case "goals" :
            {
                switch($action) {

                    case "insert": {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        echo $joomlaWatchGoalHTML->renderBackToGoals();
                        $joomlaWatchGoalHTML->renderGoalsInsert(@ JoomlaWatchHelper::requestGet('id'));

                        break;
                    }
                    case "save": {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        $joomlaWatch->goal->saveGoal($_POST);
                        $joomlaWatchGoalHTML->renderGoals(@ $result);

                        break;
                    }
                    case "edit": {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        echo $joomlaWatchGoalHTML->renderBackToGoals();
                        $joomlaWatchGoalHTML->renderGoalEdit(@ JoomlaWatchHelper::requestGet('goalId'));

                        break;
                    }
                    case "delete": {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        $result = $joomlaWatch->goal->deleteGoal(@ JoomlaWatchHelper::requestGet('goalId'));
                        $joomlaWatchGoalHTML->renderGoals(@ $result);

                        break;
                    }
                    case "enable": {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        $result = $joomlaWatch->goal->enableGoal(@ JoomlaWatchHelper::requestGet('goalId'));
                        $joomlaWatchGoalHTML->renderGoals(@ $result);

                        break;
                    }
                    case "disable": {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        $result = $joomlaWatch->goal->disableGoal(@ JoomlaWatchHelper::requestGet('goalId'));
                        $joomlaWatchGoalHTML->renderGoals(@ $result);

                        break;
                    }
                    default: {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        $joomlaWatchGoalHTML->renderGoals(@ $result);

                        break;
                    }

                }

                break;
            }
        case "settings" :
            {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                $joomlaWatchHTML->renderSettings(@ $result);

                break;
            }
        case "settingsSave" :
            {
                $result = $joomlaWatch->helper->saveSettings($_POST);
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                $joomlaWatchHTML->renderSettings(@ $result);

                break;
            }
        case "resetData" :
            {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                $result = $joomlaWatch->helper->resetData($_POST);
                $joomlaWatchHTML->renderResetData($result);
                break;
            }

        case "antiSpam" :
            {

                switch($action) {

                    case "toggleBlocking": {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        $ip = @ JoomlaWatchHelper::requestGet('ip');
                        $joomlaWatch->block->blockIpToggle($ip);
                        $joomlaWatchHTML->renderAntiSpam();
                        break;

                    }

                    case "save" :
                        {
                            $result = $joomlaWatch->helper->saveAntiSpamSettings($_POST);
                            $joomlaWatchHTML->renderAdminStyles();
                            $joomlaWatchHTML->renderHeader();
                            $joomlaWatchHTML->renderAntiSpam();
                            break;
                        }


                    default:  {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        $joomlaWatchHTML->renderAntiSpam();
                        break;
                    }
                }

                break;
            }



        case "status" :
            {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                $joomlaWatchHTML->renderStatus();
                break;
            }

        case "license" :
            {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                $joomlaWatchHTML->renderAdFreeLicense();
                break;
            }


        case "history" :
            {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                $joomlaWatchHTML->renderVisitsHistory();
                break;
            }

        case "emails" :
            {

                switch($action) {

                    case "save": {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        $result = $joomlaWatch->helper->saveEmailSettings($_POST);
                        $joomlaWatchHTML->renderEmails();
                        break;

                    }
                    default: {
                        $joomlaWatchHTML->renderAdminStyles();
                        $joomlaWatchHTML->renderHeader();
                        $joomlaWatchHTML->renderEmails();
                        break;
                    }
                }
                break;
            }


        default :
            if ($joomlaWatch->config->checkLicenseAccepted()) {
                $joomlaWatchHTML->renderAdminStyles();
                $joomlaWatchHTML->renderHeader();
                $joomlaWatchHTML->renderBody($option);
            } else {
                $joomlaWatchHTML->renderAdminStyles();
                echo $joomlaWatchHTML->renderAcceptLicense();
            }
            break;
    }
    /*
        } else {
            $joomlaWatchHTML->renderAdminStyles();
            $joomlaWatchHTML->renderHeader();
            $joomlaWatchHTML->renderTrialVersionInfo();
        }
    */
}

?>

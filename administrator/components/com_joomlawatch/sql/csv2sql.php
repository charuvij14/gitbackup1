<?php

$linesPerFile = 1700;
$path = "d:\\work\\cg15\\components\\com_joomlawatch\\sql\\";
$outputFilePrefix = "joomlawatch-";
$inputFileName = "ip-to-country.csv";

$inputHandle = fopen($path.$inputFileName,"rw+");
if ($inputHandle) {
    $i = 0;
    $j = 1; // starting from 1
    while (!feof($inputHandle)) {
        $buffer = fgets($inputHandle, 4096);
        if ($i % $linesPerFile == 0) {
            if (@$outputHandle) {
                fwrite($outputHandle, ";");
                fclose($outputHandle);
            }
            $outputFilename = $path.$outputFilePrefix.(int) $j.".sql";
            echo("creating ". $outputFilename." \n");
            $outputHandle = fopen($outputFilename,"w");
            $header = "INSERT INTO `#__joomlawatch_ip2c` (`start`, `end`, `country`) VALUES \n";
            fwrite($outputHandle, $header);
            $j++;
            $i = 0;
        }
        $valuesArray = split(",",$buffer);
        $output = "";
        if (sizeof($valuesArray) > 3) {
            if ($i != 0 && $i < $linesPerFile) {
                $output .= ", \n";
            }
			$valuesArray[0] = str_replace("\"","",$valuesArray[0]);
			$valuesArray[1] = str_replace("\"","",$valuesArray[1]);
			$valuesArray[2] = str_replace("\"","'",$valuesArray[2]);
            $output .= "(".$valuesArray[0].", ".$valuesArray[1].", ". $valuesArray[2].")";
            fwrite($outputHandle, $output);
        }
        $i++;
    }

    fclose($inputHandle);
	fclose($outputHandle);
}



?>
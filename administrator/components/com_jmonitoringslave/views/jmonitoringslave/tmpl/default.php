<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

$mainframe = JFactory::getApplication();
?>
<h3><?php echo JText::_('COM_JMONITORINGSLAVE_AUTHENTIFICATION')?></h3>
<p><?php echo JText::_('COM_JMONITORINGSLAVE_SECRET_KEY')?>: <input readonly="readonly" type="text" size="25" value="<?php echo $mainframe->getCfg('secret') ?>"/></p>
<h3><?php echo JText::_('COM_JMONITORINGSLAVE_CREDIT')?></h3>
<p><?php echo JText::_('COM_JMONITORINGSLAVE_THANKS_TO')?>:</p>
<ul>
  <li>Pierre-André Vullioud</li>
  <li>David Pizzotti</li>
  <li>Alan Pilloud</li>
  <li>Jonathan Fuchs</li>
  <li><?php echo JText::_('COM_JMONITORINGSLAVE_THANKS_TO_TESTERS')?></li>
</ul>
<p><?php echo JText::_('COM_JMONITORINGSLAVE_MORE_INFORMATION')?>: <a target="_blank" href="http://www.jmonitoring.com">jmonitoring.com</a></p>
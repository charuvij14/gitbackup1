<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * jmonitoringslave View
 */
class jmonitoringslaveViewJmonitoringslave extends JView
{
	/**
	 * HelloWorlds view display method
	 * @return void
	 */
	function display($tpl = null) 
	{ 
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		$this->addToolBar();
		// Display the template
		parent::display($tpl);
	}
         
	/**
	 * Setting the toolbar
	 */
	protected function addToolBar() 
	{
		JHTML::stylesheet( 'icon_jmon.css', 'administrator/components/com_jmonitoringslave/');
    JToolBarHelper::title(JText::_('JMonitoring Slave'), 'icon_jmon');
		JToolBarHelper::preferences('com_jmonitoringslave',$height='200', $width='600');
	}
}
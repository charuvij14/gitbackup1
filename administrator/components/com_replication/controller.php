<?php
/**
 * @version 2.5
 * @subpackage Components
 * @package replication
 * @copyright Copyright (C) 2007 - 2012 romain gires. All rights reserved.
 * @author		romain gires
 * @link		http://composant.gires.net/
 * @license		License GNU General Public License version 2 or later
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controller library
jimport('joomla.application.component.controller');

/**
 * General Controller of Replication component
 */
class ReplicationController extends JController
{
	/**
	 * display task
	 *
	 * @return void
	 */
	function display($cachable = false) 
	{
		// set default view if not set
		JRequest::setVar('view', JRequest::getCmd('view', 'Replications'));

		// call parent behavior
        parent::display($cachable);

		// Set the submenu
		//ReplicationHelper::addSubmenu('replication_site');
		ReplicationHelper::addSubmenu(JRequest::getCmd('view', 'Replications'));
		
	}
}

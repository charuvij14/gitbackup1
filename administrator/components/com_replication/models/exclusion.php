<?php
/**
 * @version 2.5
 * @subpackage Components
 * @package replication
 * @copyright Copyright (C) 2007 - 2012 romain gires. All rights reserved.
 * @author		romain gires
 * @link		http://composant.gires.net/
 * @license		License GNU General Public License version 2 or later
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import the Joomla modellist library
jimport('joomla.application.component.modellist');

jimport( 'joomla.application.component.helper' );

/**
 * Replications Model
 */
class ReplicationModelExclusion extends JModelList
{
	/**
	 * Returns an object list
	 *
	 * @param	string	The query
	 * @param	int		Offset
	 * @param	int		The number of records
	 * @return	array
	 * @since	1.5
	 *
	 * supprime la limit de la query
	 * 
	 */
	protected function _getList($query, $limitstart=0, $limit=0) {
		$limit=0;
		$this->_db->setQuery($query, $limitstart, $limit);
		$result = $this->_db->loadObjectList();
		return $result;
	}
	

	protected function getListQuery() 
	{
		// Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);


		// Select some fields
		$query->select('id_table,status');

		// From the Replication table
		$query->from('#__replicationexclusion');
		
		/*
		 //config de base joomla
		$mainframe = JFactory::getApplication();
		$dbprefix = $mainframe->getCfg('dbprefix');
		*/
		//config param composant
		$config = &JComponentHelper::getParams( 'com_replication' );
		$dbprefix = $config->get( 'prefix_source', 'jom_');  
		
		$prefixstrlen = strlen($dbprefix);
		$where = "`id_table` LIKE '$dbprefix%' ";
		$query->where($where);

		return $query;
	}

	/*public function getListexclusion() 
	{
		
		$db = JFactory::getDBO();
		$query = 'SELECT
				id_table AS id_table,
				status AS status
				FROM #__replicationexclusion
				ORDER BY id_table
			';
		$db->setQuery( $query );
		$rows = $db->loadObjectList();
		return $rows;
		
	}*/
	
	public function getTablelist() 
	{
		$rows = ReplicationHelper::getTablelist();
		return $this->rows = $rows;
	}
	/*public function getTablelist() 
	{
		 // Create a new query object.
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$rows = array();
		
		$db->getTableList();
		$ar_nametable = $db->loadResultArray();
		// Check for errors.
		if ($db->getErrorNum()) {
			echo $db->stderr();
			return false;
		}
        for ($i=0; $i < count($ar_nametable); $i++) {
            $val = new stdclass;
            $val->id_table  = $ar_nametable[$i];
			$status = ReplicationHelper::getTableFilterDefaut($val->id_table);
            $val->status    = $status;

			$rows[$i]=  $val;
		}
		return $this->rows = $rows;
		
	}*/
	/*
	public function getTableFilterDefaut($id_table) {
		$mainframe = JFactory::getApplication();
		$dbprefix = $mainframe->getCfg('dbprefix');
		$table = substr ( $id_table , strlen($dbprefix) );
		//	1 = 'REPLIQUER'  Defaut
		//	2 = 'REP RAPATRIER AVANT DE REPLIQUER'
		//	3 = 'REP NE PAS REPLIQUER'
		$filtre = array(
			'session'=>'3',
			'users'=>'2',
			'banner_tracks'=>'2',
			'messages'=>'2',
			'content_rating'=>'2',
			'core_log_searches'=>'2',
			'user_notes'=>'2', 
		);
		return  (array_key_exists($table, $filtre)) ? $filtre[$table]: 1;
	}*/

	function getStatuslevel( &$row )
	{
		$db = JFactory::getDBO();
		$query = 'SELECT id AS value, name AS text'
			. ' FROM #__replicationstatut'
			. ' ORDER BY id'
			;
		$db->setQuery( $query );
		$groups = $db->loadObjectList();
		return $groups;
	}
	function setStatuslevel( &$row, $groups )
	{
		$status = JHTML::_('select.genericlist', $groups, "listetable[".$row->id_table."]" , 'class="inputbox" size="1"', 'value', 'text', intval( $row->status ), '', 1 );
		return $status;
	}


}

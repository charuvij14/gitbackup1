<?php
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * RSS View class for JMonitoring component
 */
class JmonitoringViewLogs extends JView
{
	function display()
	{       
    $conf = JFactory::getConfig();
    $token = JRequest::getVar('token');

	  if($token==$conf->get('secret'))
    {
  		$doc	= JFactory::getDocument();
  		$rows		= $this->get('Items');
  
  		foreach ($rows as $row)
  		{
  			// strip html from feed item title
  			$title = $this->escape($row->log_entry);
  			$title = html_entity_decode($title, ENT_COMPAT, 'UTF-8');
  
  			
  			// url link to article
  			$link = $row->access_url;
  
  			// strip html from feed item description text
  			// TODO: Only pull fulltext if necessary (actually, just get the necessary fields).
  			$description	= $row->name.' '.$row->comment.' '.$row->date_last_check;
  			$author			= 'JMonitoring';
  			@$date			= ($row->log_date ? date('r', strtotime($row->log_date)) : '');
  
  			// load individual item creator class
  			$item = new JFeedItem();
  			$item->title		= $title;
  			$item->link			= $link;
  			$item->description	= $description;
  			$item->date			= $date;
  			$item->category		= 'jm';
  			$item->author		= $author;
  
  
  			// loads item info into rss array
  			$doc->addItem($item);
		  }
	  }
	  else
	  {
      die();
    }
	}  
}
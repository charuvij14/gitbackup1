<?php
/**
 * @version $Id: header.php 739 2008-11-16 22:07:19Z elkuku $
 * @package		jmonitoring
 * @subpackage	
 * @author		EasyJoomla {@link http://www.easy-joomla.org Easy-Joomla.org}
 * @author		Alan Pilloud {@link www.inetis.ch}, Jonathan Fuchs
 * @author		Created on 11-May-2009
 */

// no direct access
defined( '_JEXEC' ) or die( ';)' );

jimport('joomla.application.component.controller');
jimport( 'joomla.application.component.model' );
/**
 * jmonitoring default Controller
 *
 * @package    jmonitoring
 * @subpackage Controllers
 */
class jmonitoringController extends JController
{
	function cron()
	{
	  $config = JFactory::getConfig();
    if(JRequest::getVar('token','') == $config->getValue('secret'))
    {
      //on récupère les fonctions de l'administration
      require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jmonitoring'.DS.'controllers'.DS.'jmonitorings.php');
      require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_jmonitoring'.DS.'models'.DS.'jmonitorings.php');
      
      jmonitoringControllerjmonitorings::verify_sites();
    }
    else
    {
      echo "Access denied";
    }
    die();
	}
}// class
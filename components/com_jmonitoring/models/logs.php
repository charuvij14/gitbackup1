<?php
/**
 * @version		$Id: categories.php 21593 2011-06-21 02:45:51Z dextercowley $
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die;

jimport('joomla.application.component.model');


class JmonitoringModelLogs extends JModel
{
public $_context = 'com_jmonitoring.logs';
protected $_extension = 'com_jmonitoring';
  
	public function getItems()
	{

    $app = JFactory::getApplication();
		$nbItems = JRequest::getVar('limit', $app->getCfg('feed_limit'));
    
    $db =& JFactory::getDBO();
    $query = 'SELECT * FROM #__jmon_logs 
              LEFT JOIN #__jmon_sites ON #__jmon_sites.id_site = #__jmon_logs.idx_site 
              ORDER BY log_date DESC LIMIT 0,'.$nbItems;
              
    $db->setQuery($query);
    $logs = $db->loadObjectList();

		return $logs;
	}


}
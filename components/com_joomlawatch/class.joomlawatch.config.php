<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchConfig {

    var $database;
    var $subDirectory;

    function JoomlaWatchConfig() {
        $this->database = new JoomlaWatchDB();
        $this->subDirectory = rtrim(dirname( $_SERVER['PHP_SELF'] ));
    }

    function getAdminitratorDirectoryName() {
        return "administrator";
    }

    function getJavascriptDirectoryName() {
        return "components/com_joomlawatch/js";
    }

    /**
     * config
     */
    function checkPermissions() {
        $rand = $this->getRand();
        if ($rand != addslashes(strip_tags(@ JoomlaWatchHelper::requestGet('rand'))))
            die(_JW_ACCESS_DENIED);
    }


    /**
     * config
     *
     * @return unknown
     */
    function getRand() {
        $query = sprintf("select value from #__joomlawatch_config where name = 'rand' order by id desc limit 1; ");
        $rand = $this->database->resultQuery($query);
        return $rand;

    }

    /**
     * config
     */
    function isIgnored($name, $key) {
        if (!@$key) {
            return false;
        }
        $name = strtoupper($name);
        $query = sprintf("select value from #__joomlawatch_config where name='JOOMLAWATCH_IGNORE_".$name."' limit 1");
        $rowValue = $this->database->resultQuery($query);
        $exploded = explode("\n", $rowValue);
        foreach ($exploded as $value) {
            if (JoomlaWatchHelper::wildcardSearch(trim($value), $key)) {
                return true;
            }
        }
        return false;
    }


    /**
     * config
     */
    function updateHelperCountByKey($key, $value) {
        $count = $this->getCountByKey($key);

        if (@ $count) {
            $query = sprintf("update #__joomlawatch_config set value = '%s' where (name = '%s' and date = '%d')", $this->database->getEscaped($value), $this->database->getEscaped($key), (int) $date);
            $this->database->executeQuery($query);
        } else {
            $query = sprintf("insert into #__joomlawatch_config values ('', '%s', '%s')", $this->database->getEscaped($key), $this->database->getEscaped($value));
            $this->database->executeQuery($query);
        }
    }

    /**
     * config
     */

    function getConfigValue($key) {

        $query = sprintf("select value from #__joomlawatch_config where name = '%s' limit 1", $this->database->getEscaped($key));
        $value = $this->database->resultQuery($query);
        // explicit off for checkboxes
        if ($value == "Off") {
            return false;
        }
        if ($value) {
            return addslashes($value);
        }

        $value = @ constant($key);
        return $value;
    }

    /**
     * config
     */
    function saveConfigValue($key, $value) {
        $query = sprintf("select count(name) as count from #__joomlawatch_config where name = '%s' limit 1", $this->database->getEscaped($key));
        $count = $this->database->resultQuery($query);

        if ($count) { //update
            $query = sprintf("update #__joomlawatch_config set value = '%s' where name = '%s'", $this->database->getEscaped($value), $this->database->getEscaped($key));
            $this->database->executeQuery($query);
        } else { //insert
            $query = sprintf("insert into #__joomlawatch_config values ('','%s','%s')", $this->database->getEscaped($key), $this->database->getEscaped($value));
            $this->database->executeQuery($query);
        }

    }

    /**
     * config
     */
    function removeConfigValue($key, $value) {
        $query = sprintf("delete from #__joomlawatch_config where name = '%s' limit 1", $this->database->getEscaped($key));
        $count = $this->database->resultQuery($query);
    }

    /**
     * config
     */
    function getLanguage() {
        $language = $this->getConfigValue("JOOMLAWATCH_LANGUAGE");
        return $language;
    }

    /**
     * config
     */
    function checkLicenseAccepted() {
        $accepted = $this->getConfigValue("JOOMLAWATCH_LICENSE_ACCEPTED");
        if (@ $accepted) {
            return true;
        }
        return false;
    }

    function getLicenseFilePath() {
        $config = new JConfig();
        $fileName = md5($this->getLiveSite());
        return $config->tmp_path.DS.$fileName.".tmp";
    }

    function createLicenseFile() {
        $result = 0;
        if (!file_exists($this->getLicenseFilePath())) {
            $ourFileHandle = fopen($this->getLicenseFilePath(), 'w') or die("can't write to temp directory: ".$config->tmp_path);
            $result = fwrite($ourFileHandle, JoomlaWatchHelper::getServerTime());
            fclose($ourFileHandle);
        } 
        return $result;
    }

    function getTrialVersionTimeLeft() {
/*
        $ourFileHandle = fopen($this->getLicenseFilePath(), 'r') or die("can't write to temp directory: ".$config->tmp_path);
        $result = fscanf($ourFileHandle, "%d");
        fclose($ourFileHandle);
        return 30 - ((JoomlaWatchHelper::getServerTime() - $result[0]) / 3600 / 24);
*/
        return 0;
    }

    /**
     * config
     */
    function setLicenseAccepted() {
        $this->createLicenseFile();
        $this->saveConfigValue("JOOMLAWATCH_LICENSE_ACCEPTED", "1");
    }

    /**
     * config
     */
    function setLiveSite($liveSite) {
        $this->saveConfigValue("JOOMLAWATCH_LIVE_SITE", "$liveSite");
    }

    /**
     * config
     */
    function getCheckboxValue($key) {
        $setting = $this->getConfigValue($key);
        if ($setting == '1' || strtolower($setting) == 'on') {
            return true;
        }
        return false;
    }

    /**
     * If on HTTPS site, use the https links
     * @param  $originalLink
     * @return mixed
     */
    function replaceHttpByHttps($originalLink) {
        $uri =& JFactory::getURI();
        if ($uri->isSSL()) {
            return str_ireplace("http://","https://", $originalLink);
        }
        return str_ireplace("https://","http://", $originalLink);
    }

    function getLiveSite() {
        global $database;
        if (!defined('JOOMLAWATCH_LIVE_SITE')) {
            return JoomlaWatchConfig::replaceHttpByHttps($this->getConfigValue('JOOMLAWATCH_LIVE_SITE'));
        } else {
            return JoomlaWatchConfig::replaceHttpByHttps(rtrim(constant('JOOMLAWATCH_LIVE_SITE')));
        }
    }

    //TODO
    function getAdministratorPath() {
        //TODO -> should be changed, there can be other directory
        return $this->getLiveSite().$this->getAdminitratorDirectoryName()."/";
    }

    function getAdministratorIndex() {
        //TODO -> should be changed, there can be other directory
        return $this->getLiveSite().$this->getAdminitratorDirectoryName()."/index.php";
    }


    function cleanUrl($domain) {
        $domain = str_replace("http://","",$domain);
        $domain = str_replace("https://","",$domain);
        $domain = str_replace("www.","",$domain);
        return $domain;
    }

    function getDomainFromLiveSite() {
        $parsedUrl = @ parse_url(@$this->getLiveSite());
        return trim($this->cleanUrl(@$parsedUrl[host]));
    }



    /**
     * config
     */
    function isAdFree() {
        $key = md5(strrev($this->getDomainFromLiveSite()));
        if ($key == $this->getConfigValue("JOOMLAWATCH_ADFREE")) {
            return true;
        }
        return false;
    }

    /**
     * config
     */
    function activate($value) {
        $this->saveConfigValue('JOOMLAWATCH_ADFREE', $this->database->getEscaped($value));
    }

    /**
     * Important when doing an upgrade to data between versions, we need to know to which version these data belong
     * @return void
     */
    function saveVersionIntoDatabase() {
        $this->saveConfigValue('JOOMLAWATCH_VERSION', JOOMLAWATCH_VERSION);
    }

}

?>
<?php


/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2008 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

include_once("includes.php");

$joomlaWatch = new JoomlaWatch();
require_once ("lang" . DS . $joomlaWatch->config->getLanguage().".php");

$joomlaWatchHTML = new JoomlaWatchHTML();
$joomlaWatchVisitHTML = new JoomlaWatchVisitHTML($joomlaWatch);
$joomlaWatchBlockHTML = new JoomlaWatchBlockHTML($joomlaWatch);

$joomlaWatch->config->checkPermissions();

$t1 = ($joomlaWatch->helper->getServerTime()+ microtime());

$last = $joomlaWatch->visit->getLastVisitId();

echo ("$last\n\n");

include("view".DS."visits.php");

?>




<!-- rendered in <?php echo((time()+microtime())-$t1); ?>s -->


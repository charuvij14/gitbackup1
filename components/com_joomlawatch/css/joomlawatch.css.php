<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2008 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

define('_JEXEC', 1);
$dirname = dirname(__FILE__);
$dirnameExploded = explode(DIRECTORY_SEPARATOR, $dirname);
$jBasePath = "";
$omitLast = 3;
for ($i = 0; $i < sizeof($dirnameExploded) - $omitLast; $i++) {
    $jBasePath .= $dirnameExploded[$i];
    if ($i < (sizeof($dirnameExploded) - ($omitLast +1)))
        $jBasePath .= DIRECTORY_SEPARATOR;

}
define('JPATH_BASE', $jBasePath);
define('DS', DIRECTORY_SEPARATOR);

if (!defined('JPATH_ROOT'))
    require_once (JPATH_BASE . DS . 'includes' . DS . 'defines.php');
if (!defined('JDEBUG'))
    require_once (JPATH_BASE . DS . 'includes' . DS . 'framework.php');

if (version_compare(JVERSION, '1.5.0', '<')) {
    require_once (JPATH_BASE . DS . 'libraries' . DS . 'joomla' . DS . 'application' . DS . 'module' . DS . 'helper.php');
    $mainframe = & JFactory :: getApplication('site');
    $mainframe->initialise();
}

require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "config.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.block.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.cache.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.config.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.goal.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.helper.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.stat.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.visit.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.html.php");
require_once (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "class.ip2country.php");

$joomlaWatch = new JoomlaWatch();
$joomlaWatchHTML = new JoomlaWatchHTML();
$joomlaWatchHTML->renderAdminStyles();
?>

<?php


/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if( !defined( '_JEXEC' ) && !defined( '_VALID_MOS' ) ) die( 'Restricted access' );

class JoomlaWatchHTML {

    var $joomlaWatch;

    function JoomlaWatchHTML() {
        $this->joomlaWatch = new JoomlaWatch();
    }

    function renderInputElement($key, & $color, $addToDescription = "") {

        if (!@ $color) {
            $color = "#f7f7f7";
        }
        else {
            $color = "";
        }

        $value = "";
        $value = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_" . $key);
        $defaultValue = @ constant("JOOMLAWATCH_" . $key);

        $type = @ constant("TYPE_JOOMLAWATCH_" . $key);
        if ((strcmp($value, $defaultValue)) && ($type != "checkbox") && ($type != "largetext")) {
            $changed = "<i>(" . _JW_SETTINGS_DEFAULT . ": <a href=\"javascript:setElementValueById('JOOMLAWATCH_$key','$defaultValue');\">$defaultValue</a>" . ")</i>&nbsp;";
        }

        $desc = "";
        if ($type == "number" && !(is_numeric($value))) {
            $desc .= " <span style='color: red; font-weight: bold;'> WARNING: The value you entered is not a number. JoomlaWatch will not work properly!</span> ";
        }
        $desc .= constant("_JW_DESC_" . $key);
        $desc .= $addToDescription;

        $keyShortened = str_replace("JOOMLAWATCH_", "", $key);
        $output = "<tr><td style='background-color: ".$color.";' align='left'>$keyShortened</td><td style='background-color: ".$color.";' align='center'>";

        $key = "JOOMLAWATCH_" . $key;
        switch ($type) {

            case "select": {

                if ($value && $value != "Off")
                    $checked = "checked";
                else
                    $checked = "";
                $output .= "<select align='center' name='$key' id='$key' style='text-align:center;'>";

                $languages = $this->joomlaWatch->helper->getAvailableLanguages();
                foreach ($languages as $language) {
                    if ($value == $language) {
                        $selected = "selected";
                    } else {
                        $selected = "";
                    }
                    $output .= "<option align='center' style='text-align:center;' $selected>$language</option>";
                }
                $output .= "</select>";

                break;
            }
            case "checkbox": {

                if ($value && $value != "Off")
                    $checked = "checked";
                else
                    $checked = "";
                $output .= "<input type='checkbox' id='$key' name='$key' $checked/>";
                break;
            }
            case "text": {

                $output .= "<textarea id='$key' cols='15' rows='3' name='$key' style='text-align:center;'>$value</textarea>";
                break;
            }
            case "largetext": {
                $output .= "<textarea id='$key' cols='40' rows='100' name='$key' style='text-align:center;'>$value</textarea>";
                break;
            }
            default: {
                $output .= "<input type='text' id='$key' name='$key' value='$value' size='20' style='text-align:center;'/>";
                break;
            }

        }

        $output .= "</td><td style='background-color: ".$color.";' align='left'>" . @ $changed . " $desc</td></tr>";
        return $output;
    }



    function renderPrint() {
        $group = @ JoomlaWatchHelper::requestGet('group');
        $name = @ JoomlaWatchHelper::requestGet('name');
        $date = @ JoomlaWatchHelper::requestGet('date');
        $task = @ JoomlaWatchHelper::requestGet('task');
        $action = @ JoomlaWatchHelper::requestGet('action');

        $print = @ JoomlaWatchHelper::requestGet('print');
        if (@ $print) {
            $output = "<script language='Javascript'>window.print();</script>";
        } else {
            $output = ("<table width='100%'><tr><td align='right'><a href='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/trendtooltip.php?rand=" . $this->joomlaWatch->config->getRand() . "&group=$group&name=$name&date=$date&print=1' target='_blank'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/print.gif' border='0' title='" . _JW_TOOLTIP_PRINT . "'/></a></td></tr></table>");
        }
        return $output;
    }

    function renderOnlineHelp($id) {
        //TODO: should be done separated
        if (@$this->joomlaWatch) {
            $liveSite = $this->joomlaWatch->config->getLiveSite();
        } else {
            $liveSite = $this->joomlaWatch->config->getLiveSite();
        }

        $output = "&nbsp;<a href='http://www.codegravity.com/projects/joomlawatch#doc-$id' target='_blank'><img src='".$liveSite."components/com_joomlawatch/icons/help.gif' border='0' title='" . _JW_TOOLTIP_HELP . ": $id'/></a>";
        return $output;
    }

    function renderCloseWindow() {
        $output = "<div align='right'><a href='javascript:ajax_hideTooltip();'>X " . _JW_TOOLTIP_WINDOW_CLOSE . "</a></div>";
        return $output;
    }

    function renderInputField($id, $values, & $color, $required = false) {
        if (!@ $values)
            $values = "";
        if (!@ $color)
            $color = "";

        $name = @constant("_JW_GOALS_" . $id);
        if ($values)
            $value = @ $values[strtolower($id)];

        if (!@ $color)
            $color = "#f7f7f7";
        else
            $color = "";

        $desc = @constant("_JW_DESC_GOALS_" . $id);
        if ($required == true) {
            $requiredText = "<span style='color: red;'>(* required)</span>&nbsp;";
        } else {
            $requiredText = "";
        }
        $output = "<td align='right' valign='top' width='150px;' bgcolor='$color'><b>$name :</b> </td><td valign='top' bgcolor='$color'><input type='text' size='40' name='$id' value='" . @ $value . "'/></td><td style='color: gray;' valign='top' align='left' bgcolor='$color'>$requiredText<i>$desc</i></td>";
        return $output;
    }


    function renderDateControl() {
        if (@ JoomlaWatchHelper::requestGet('day')) {
            $day = @ JoomlaWatchHelper::requestGet('day');
        } else {
            $day = $this->joomlaWatch->helper->jwDateToday();
        }
        $prev = $day -1;
        $next = $day +1;
        $today = $this->joomlaWatch->helper->jwDateToday();

        include ("view".DS."datecontrol.php");
    }

    function renderAdminStyles() {
        require_once ("lang" . DS . $this->joomlaWatch->config->getLanguage().".php");
        include("view".DS."adminstyles.php");
    }


    function renderResetData($result = "") {
        if ($result) {
            echo ("<h3>" . _JW_RESET_SUCCESS . "</h3>");
        } else {
            echo ("<h3>" . _JW_RESET_ERROR . "</h3>");
        }

    }

    function renderHeader() {
        // if ($this->joomlaWatch->config->getTrialVersionTimeLeft() > 0 || $this->joomlaWatch->config->isAdFree()) {
            include("view".DS."adminheader.php");
        // }
    }


    function renderBody($option) {
        include("view".DS."adminbody.php");
    }

    function renderSettings($result = "") {
        include("view".DS."settings.php");
    }


    function renderCredits() {
        include("view".DS."credits.php");
    }

    function renderAcceptLicense() {
        include("view".DS."license.php");
    }

    function renderAdFreeLicense() {
        if ($this->joomlaWatch->config->isAdFree()) {
            include("view".DS."license-commercial.php");
        } else {
            include("view".DS."license-free.php");
        }
    }


    function renderAntiSpam() {
        $joomlaWatchBlockHTML = new JoomlaWatchBlockHTML($this->joomlaWatch);
        include("view".DS."antispam.php");
    }

    function renderStatus() {
        include("view".DS."status.php");
    }

    function renderVisitsHistory() {
        $joomlaWatchVisitHistoryHTML = new JoomlaWatchVisitHistoryHTML($this->joomlaWatch);
        include("view".DS."history.php");
    }

    function renderEmails() {
        if ($this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_REPORTS_ADDRESS") == "@") {
            $user = JoomlaWatchHelper::getUser();
            $userEmail = $user->email;
            $this->joomlaWatch->config->saveConfigValue("JOOMLAWATCH_EMAIL_REPORTS_ADDRESS", $userEmail);
        }
        $joomlaWatchStatHTML = new JoomlaWatchStatHTML($this->joomlaWatch);
        include("view".DS."emails.php");
    }

    function renderTrialVersionInfo() {
        if (!$this->joomlaWatch->config->isAdFree()) {
            $timeLeft = $this->joomlaWatch->config->getTrialVersionTimeLeft();
            if ($timeLeft > 0) {
                $output = sprintf("<div style='border: 1px solid #77ADFF; background-color: #E5EFFF; width: 100%%; text-align:center'>&nbsp; This is a 30-day Evaluation Version. Days Left: <b>%d</b>. Please purchase the lifetime <a href='http://www.codegravity.com/donate/joomlawatch/' target='_blank'>JoomlaWatch license for your domain</a> for this and upcoming versions.</div><br/>", $timeLeft);
            } else {
                echo "<h2> Your trial version has expired. Please purchase JoomlaWatch</h2>";
                include ("view".DS."license-free.php");
                return;
            }
        }
        return $output;
    }

}
?>
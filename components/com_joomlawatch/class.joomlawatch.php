<?php


/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatch {

    var $database;

    var $visit;
    var $stat;
    var $config;
    var $helper;
    var $goal;
    var $block;
    var $cache;

    function JoomlaWatch() {
        $this->stat = new JoomlaWatchStat();
        $this->config = new JoomlaWatchConfig();
        $this->helper = new JoomlaWatchHelper();
        $this->goal = new JoomlaWatchGoal();
        $this->block = new JoomlaWatchBlock();
        $this->visit = new JoomlaWatchVisit();
        $this->cache = new JoomlaWatchCache();
    }





}
?>
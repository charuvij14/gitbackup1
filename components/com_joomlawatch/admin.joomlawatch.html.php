<?php


/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version @VERSION@
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) @COPY@ by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if( !defined( '_JEXEC' ) && !defined( '_VALID_MOS' ) ) die( 'Restricted access' );

class AdminJoomlaWatchHTML {

    var $database;
    var $mosConfig_live_site;
    var $config;
    var $helper;
    var $visit;
    var $goal;

    function AdminJoomlaWatchHTML() {
        $this->database = new JoomlaWatchDB();
        $this->config = new JoomlaWatchConfig();
        $this->helper = new JoomlaWatchHelper();
        $this->goal = new JoomlaWatchGoal();
        $this->visit = new JoomlaWatchVisit();
    }

    function getRand() {
        $query = "select value from #__joomlawatch_config where name = 'rand' order by id desc limit 1";
        $rows = $this->database->objectListQuery($query);
        $row = @ $rows[0];
        $rand = @ $row->value;
        return $rand;
    }

}
?>
<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchGoalHTML {

    var $joomlaWatch;
    var $joomlaWatchHTML;
    var $visitHistory;

    function JoomlaWatchGoalHTML($joomlaWatch) {
        $this->joomlaWatch = $joomlaWatch;
        $this->joomlaWatchHTML = new JoomlaWatchHTML($joomlaWatch);
        $this->visitHistory = new JoomlaWatchVisitHistory($joomlaWatch);
    }

    /* goal */
    function renderActionButtons($id) {
        $output = "";
        $output .= "<a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals&action=edit&goalId=$id'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/edit.gif' border='0' title='" . _JW_GOALS_EDIT . "'/></a> ";
        $output .= "&nbsp;&nbsp;";
        $output .= "<a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals&action=delete&goalId=$id' onClick='return confirm(\"" . _JW_GOALS_DELETE_CONFIRM . " $id? \");'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/delete.gif' border='0' title='" . _JW_GOALS_DELETE . "'/></a> ";
        return $output;
    }

    /* goal */
    function renderCell($style, $content, $doNotTruncate = false) {
        if (!$doNotTruncate) $content = $this->joomlaWatch->helper->truncate($content, $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_TRUNCATE_GOALS'));
        $output = "<td align='center' style='$style'>$content</td>";
        return $output;
    }

    /* goal */
    function renderGoals($result = "") {
        echo ("<h2>" . _JW_GOALS_TITLE . JoomlaWatchHTML :: renderOnlineHelp(DB_KEY_GOALS) . "</h2>");
        if ($result) {
            echo (_JW_SUCCESS . "<br/>");
        }
        include("view".DS."goals.php");
    }

    /* goal */
    function renderEnabled($id, $disabled) {
        if ($disabled)
            $output = "<a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals&action=enable&goalId=$id'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/unpublished.png' border='0'/></a>";
        else
            $output = "<a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals&action=disable&goalId=$id'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/published.png' border='0'/></a>";

        return $output;
    }

    /* goal */
    function renderBackToGoals($result = "") {
        $output = "<a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals'> &lt;&lt; " . _JW_BACK . "</a>";
        return $output;
    }

    /* goal */
    function renderGoalEdit($id) {
        $values = $this->joomlaWatch->goal->getGoalById((int) $id);
        $this->renderGoalForm(_JW_GOALS_UPDATE . " $id", $values);
    }


    /* goal */
    function renderGoalsInsert($id = null) {

        $values = "";

        if (isset($id)) {
            $rows = $this->joomlaWatch->visit->getJoinedURIRowById($id);
            if (!$rows) {
                $rows = $this->visitHistory->getJoinedURIRowById($id);
            }
            $row = $rows[0];
            $values['uri_condition'] = $row->uri;
            $values['title_condition'] = $row->title;
            $values['username_condition'] = $row->username;
            $values['ip_condition'] = $row->ip;
/*
            if(sizeof($rows) > 1) {

                $previousUriRow = $rows[1];
                $values['came_from_condition'] = $previousUriRow->uri;    
            }
*/
            $values['came_from_condition'] = $row->referer;    

            $values['country_condition'] = $this->joomlaWatch->helper->countryByIp($row->ip);
        } else
            if (@JoomlaWatchHelper::requestGet('country')) {
                $country = urldecode(@JoomlaWatchHelper::requestGet('country'));
                $values['name'] = _JW_GOALS_COUNTRY.": $country";
                $values['country_condition'] = $country;
            } else
                if (@JoomlaWatchHelper::requestGet('from') && @JoomlaWatchHelper::requestGet('uri')) {
                    $from = urldecode(@JoomlaWatchHelper::requestGet('from'));
                    $to = urldecode(@JoomlaWatchHelper::requestGet('uri'));
                    $fromTitle = $this->joomlaWatch->visit->getTitleByUri($from);
                    if (@!$fromTitle) {
                        $fromTitle = $from;
                    }
                    $toTitle = $this->joomlaWatch->visit->getTitleByUri($to);
                    if (@!$toTitle) {
                        $toTitle = $to;
                    }
                    $values['name'] = _JW_STATS_FROM.": $fromTitle "." "._JW_STATS_TO.": $toTitle";
                    $values['came_from_condition'] = $from;
                    $values['uri_condition'] = $to;
                } else
                    if (@JoomlaWatchHelper::requestGet('uri')) {
                        $to = urldecode(@JoomlaWatchHelper::requestGet('uri'));
                        $toTitle = $this->joomlaWatch->visit->getTitleByUri($to);
                        if (!$toTitle) {
                            $toTitle = $to;
                        }
                        $values['name'] = _JW_STATS_TO.": $toTitle";
                        $values['uri_condition'] = $to;
                    }

        $this->renderGoalForm(_JW_GOALS_INSERT, $values);
    }

    function renderGoalForm($action, $values = "") {
        include("view".DS."goal.php");
    }



}
<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchVisitHistory extends JoomlaWatchVisit {

    var $database;
    var $config;
    var $helper;
    var $stat;
    var $block;
    var $goal;
    var $visitHistory;
    var $lastDate;

    function JoomlaWatchVisitHistory($joomlaWatch) {
        parent::JoomlaWatchVisit($joomlaWatch);
    }

    /**
     * visitor
     */
    function getVisitors() {
        $count = $this->config->getConfigValue('JOOMLAWATCH_HISTORY_MAX_VALUES');
        $actualPage = (int) JoomlaWatchHelper::requestGet('page');
        $actualRecord = $actualPage * $count;
        $limit = $actualRecord.",".$count; 

        $query = sprintf("select ip, referer, username from #__joomlawatch_history where (browser is not NULL and browser != '') order by id desc limit %s", $this->database->getEscaped($limit));
        $rows = $this->database->objectListQuery($query);
        return $rows;
    }

    /**
     * visitor
     */
    function getJoinedURIRows($ip) {
        $query = sprintf("SELECT * FROM #__joomlawatch_history LEFT JOIN #__joomlawatch_uri_history ON #__joomlawatch_history.id = #__joomlawatch_uri_history.fk where ip = '%s' ORDER BY #__joomlawatch_uri_history.timestamp desc", $ip);
        $rows = $this->database->objectListQuery($query);
        return $rows;
    }

    function getHistoryCount() {
        $query = sprintf("SELECT count(id) FROM #__joomlawatch_history");
        $rows = $this->database->resultQuery($query);
        return $rows;
    }

     /**
     * visitor
     */
    function getJoinedURIRowById($id) {
        $query = sprintf("SELECT * FROM #__joomlawatch_history LEFT JOIN #__joomlawatch_uri_history ON #__joomlawatch_history.id = #__joomlawatch_uri_history.fk where #__joomlawatch_uri_history.id = '%d' ORDER BY #__joomlawatch_uri_history.timestamp desc", (int) $id);
        $rows = $this->database->objectListQuery($query);
        return $rows;
    }


}

?>
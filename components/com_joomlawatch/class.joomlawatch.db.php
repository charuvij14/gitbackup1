<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchDB {

    var $database;

    function JoomlaWatchDB() {
        global $database;
        $this->database = & JFactory :: getDBO();
    }


    /**
     * query without a result
     *
     * @param unknown_type $query
     */
    function executeQuery($query) {
        $this->database->setQuery($query);
        $result = $this->database->query();
        if ($this->database->getErrorNum() > 0) {
            JoomlaWatchLog::error(__FILE__,__LINE__,$query);
        }
        return $result;
    }

    /**
     * Query with single result
     *
     * @param unknown_type $query
     * @return unknown
     */
    function resultQuery($query) {
        $this->database->setQuery($query);
        if ($this->database->getErrorNum() > 0) {
            JoomlaWatchLog::error(__FILE__,__LINE__,$query);
        }
        $result = $this->database->loadResult();
        return $result;
    }

    /**
     * Query with multiple results as objects
     *
     * @param unknown_type $query
     * @return unknown
     */
    function objectListQuery($query) {
        $this->database->setQuery($query);
        if ($this->database->getErrorNum() > 0) {
            JoomlaWatchLog::error(__FILE__,__LINE__,$query);
        }
        $rows = @ $this->database->loadObjectList();
        return $rows;
    }

    /**
     * Query with multiple results as asoc list
     *
     * @param unknown_type $query
     * @return unknown
     */
    function asocListQuery($query) {
        $this->database->setQuery($query);
        $result = $this->database->query();
        if ($this->database->getErrorNum() > 0) {
            JoomlaWatchLog::error(__FILE__,__LINE__,$query);
        }
        $rows = $this->database->loadAssocList();
        return $rows;
    }

    /**
     * only proxy, should be removed
     *
     * @param unknown_type $query
     */
    function setQuery($query) {
        $this->database->setQuery($query);
    }

    /**
     * only proxy, should be removed
     *
     * @param unknown_type $query
     */
    function loadResult() {
        $result = $this->database->resultQuery($query);
        if ($this->database->getErrorNum() > 0) {
            JoomlaWatchLog::error(__FILE__,__LINE__,$query);
        }
        return $result;
    }

    /**
     * only proxy, should be removed
     *
     * @param unknown_type $query
     */
    function loadObjectList() {
        $result = $this->database->objectListQuery($query);
        if ($this->database->getErrorNum() > 0) {
            JoomlaWatchLog::error(__FILE__,__LINE__,$query);
        }
        return $result;
    }

    /**
     * only proxy, should be removed
     *
     * @param unknown_type $query
     */
    function query() {
        $result = $this->database->query();
        if ($this->database->getErrorNum() > 0) {
            JoomlaWatchLog::error(__FILE__,__LINE__,$query);
        }
        return $result;
    }

    /**
     * only proxy, should be removed
     *
     * @param unknown_type $query
     */
    function getQuery() {
        $this->database->getQuery();
    }

	/**
	escape sql 
	*/
    function getEscaped($sql) {
        return $this->database->getEscaped($sql);
    }

}

?>
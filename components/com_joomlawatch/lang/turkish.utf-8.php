﻿<?php

#JoomlaWatch language file - to create a new language file, just copy the english.php to eg. german.php and place into /components/com_joomlawatch/lang/

# Main Menu
DEFINE('_JW_MENU_STATS', "Istatistikler");
DEFINE('_JW_MENU_GOALS', "Hedefler");
DEFINE('_JW_MENU_SETTINGS', "Ayarlar");
DEFINE('_JW_MENU_CREDITS', "Yapimcilar");
DEFINE('_JW_MENU_FAQ', "SSS");
DEFINE('_JW_MENU_DOCUMENTATION', "Dökümantasyon");
DEFINE('_JW_MENU_LICENSE', "Bedava Lisans");
DEFINE('_JW_MENU_DONATORS', "Bagisçilar");
DEFINE('_JW_MENU_SUPPORT', "Joomla Watch'u Destekle ve Yönetim Panelindeki Reklamlari Kaldir.");

# Left visitors real-time window
DEFINE('_JW_VISITS_VISITORS', "Son Ziyaretçiler");
DEFINE('_JW_VISITS_BOTS', "Botlar");
DEFINE('_JW_VISITS_CAME_FROM', "Geldigi Yer");
DEFINE('_JW_VISITS_MODULE_NOT_PUBLISHED', "JoomlaWatch modülü henüz yayinda olmadigi için herhangi bir istatistik verisi bulunmamaktadir. Lütfen modülü kurup yayinlayiniz.");
DEFINE('_JW_VISITS_PANE_LOADING', "Ziyaretler Yükleniyor...");

# Right stats window
DEFINE('_JW_STATS_TITLE', "Haftalik Ziyaret Istatistikleri");
DEFINE('_JW_STATS_WEEK', "hafta");
DEFINE('_JW_STATS_THIS_WEEK', "bu hafta");
DEFINE('_JW_STATS_UNIQUE', "tekil");
DEFINE('_JW_STATS_LOADS', "görüntüleme");
DEFINE('_JW_STATS_HITS', "hitler");
DEFINE('_JW_STATS_TODAY', "bugün");
DEFINE('_JW_STATS_FOR', "için");
DEFINE('_JW_STATS_ALL_TIME', "Tüm zamanlar");
DEFINE('_JW_STATS_EXPAND', "genislet");
DEFINE('_JW_STATS_COLLAPSE', "Portatif");
DEFINE('_JW_STATS_URI', "Sayfalar");
DEFINE('_JW_STATS_COUNTRIES', "Ülkeler");
DEFINE('_JW_STATS_USERS', "Kullanicilar");
DEFINE('_JW_STATS_REFERERS', "Referanslar");
DEFINE('_JW_STATS_IP', "IPler");
DEFINE('_JW_STATS_BROWSER', "Tarayicilar");
DEFINE('_JW_STATS_OS', "OS");
DEFINE('_JW_STATS_KEYWORDS', "Anahtar Kelimeler");
DEFINE('_JW_STATS_GOALS', "Hedefler");
DEFINE('_JW_STATS_TOTAL', "Toplam");
DEFINE('_JW_STATS_DAILY', "Günlük");
DEFINE('_JW_STATS_DAILY_TITLE', "Günlük Istatistikler");
DEFINE('_JW_STATS_ALL_TIME_TITLE', "Tüm zamanlarin istatistikleri");
DEFINE('_JW_STATS_LOADING', "yükleniyor...");
DEFINE('_JW_STATS_LOADING_WAIT', "yükleniyor... lütfen bekleyiniz.");
DEFINE('_JW_STATS_IP_BLOCKING_TITLE', "IP Engelleme");
DEFINE('_JW_STATS_IP_BLOCKING_ENTER', "Elle IP Girisi");
DEFINE('_JW_STATS_IP_BLOCKING_MANUALLY', "Engellemek istediginiz IP numarasini girin. (eg. 217.242.11.54 or 217.* or 217.242.* to block all IPs matching the wildcard)");
DEFINE('_JW_STATS_IP_BLOCKING_TOGGLE', "Gerçekten engellemek istiyor musun ");
DEFINE('_JW_STATS_PANE_LOADING', "Istatistikler yükleniyor...");

# Settings
DEFINE('_JW_SETTINGS_TITLE', "Ayarlar");
DEFINE('_JW_SETTINGS_DEFAULT', "Varsayilan");
DEFINE('_JW_SETTINGS_SAVE', "Kaydet");
DEFINE('_JW_SETTINGS_APPEARANCE', "Görünüm");
DEFINE('_JW_SETTINGS_FRONTEND', "Ön Uç");
DEFINE('_JW_SETTINGS_HISTORY_PERFORMANCE', "Geçmis  &amp; Performans");
DEFINE('_JW_SETTINGS_ADVANCED', "Gelismis");
DEFINE('_JW_SETTINGS_IGNORE', "Yoksay");
DEFINE('_JW_SETTINGS_BLOCKING', "Engelle");
DEFINE('_JW_SETTINGS_EXPERT', "Uzman");
DEFINE('_JW_SETTINGS_RESET_CONFIRM', "Tüm istatistik kayitlarini gerçekten silmek istiyor musun?");
DEFINE('_JW_SETTINGS_RESET_ALL', "Tümünü Sil");
DEFINE('_JW_SETTINGS_RESET_ALL_LINK', "Tüm istatistikleri sil &amp; ziyaretçi bilgisi");
DEFINE('_JW_SETTINGS_LANGUAGE', "Dil");
DEFINE('_JW_SETTINGS_SAVED', "Ayarlar kaydedildi");
DEFINE('_JW_SETTINGS_ADD_YOUR_IP', "Kendi IP numarani ekle");
DEFINE('_JW_SETTINGS_TO_THE_LIST', "listeye");

# Other / mostly general
DEFINE('_JW_TITLE', "Gerçek zamanli AJAX Joomla ekrani");
DEFINE('_JW_BACK', "Geri");
DEFINE('_JW_ACCESS_DENIED', "Bu içerigi görmeye izinli degilsin!");
DEFINE('_JW_LICENSE_AGREE', "Sözlesmeyi okudum ve kabul ediyorum &amp; sözlesme yukarida");
DEFINE('_JW_LICENSE_CONTINUE', "Devam et");
DEFINE('_JW_SUCCESS', "Islem basarili");
DEFINE('_JW_RESET_SUCCESS', "Tüm istatistik verileri ve ziyaretçi bilgileri basariyla silindi.");
DEFINE('_JW_RESET_ERROR', "Veri tamamen silinemedi bir hata var.");
DEFINE('_JW_CREDITS_TITLE', "Yapimcilar");
DEFINE('_JW_TRENDS_DAILY_WEEKLY', "Günlük ve haftalik istatistikleri");
DEFINE('_JW_AJAX_PERMISSION_DENIED_1', "AJAX permission denied: Please view this statistics from domain you specified in configuration.php of joomla - ");
DEFINE('_JW_AJAX_PERMISSION_DENIED_2', "Maybe you just forgotten www. in front of your domain name. Your javascript is trying to access ");
DEFINE('_JW_AJAX_PERMISSION_DENIED_3', "from");
DEFINE('_JW_AJAX_PERMISSION_DENIED_4', "what makes it to think it's a different domain.");

# Header
DEFINE('_JW_HEADER_DOWNLOAD', "Get the latest extension's code from");
DEFINE('_JW_HEADER_CAST_YOUR', "Cast your");
DEFINE('_JW_HEADER_VOTE', "Vote");

# Tooltips
DEFINE('_JW_TOOLTIP_CLICK', "Ipuçlarini görmek için tikla");
DEFINE('_JW_TOOLTIP_MOUSE_OVER', "Hover a mouse over to show tooltip");
DEFINE('_JW_TOOLTIP_YESTERDAY_INCREASE', "yesterday&apos;s increase");
DEFINE('_JW_TOOLTIP_HELP', "Opens online external help for");
DEFINE('_JW_TOOLTIP_WINDOW_CLOSE', "Close this window");
DEFINE('_JW_TOOLTIP_PRINT', "Print");

# Goals
DEFINE('_JW_GOALS_INSERT', "Yeni hedef gir");
DEFINE('_JW_GOALS_UPDATE', "Hedef numarasini degisitir.");
DEFINE('_JW_GOALS_ACTION', "Eylem");
DEFINE('_JW_GOALS_TITLE', "Yeni Hedef");
DEFINE('_JW_GOALS_NEW', "Yeni Hedef");
DEFINE('_JW_GOALS_RELOAD', "Tekrar Yükle");
DEFINE('_JW_GOALS_ADVANCED', "Gelismis");
DEFINE('_JW_GOALS_NAME', "Isim");
DEFINE('_JW_GOALS_ID', "id");
DEFINE('_JW_GOALS_URI_CONDITION', "URL Durumu");
DEFINE('_JW_GOALS_GET_VAR', "Alma");
DEFINE('_JW_GOALS_GET_CONDITION', "Alma Durumu");
DEFINE('_JW_GOALS_POST_VAR', "Gönderme");
DEFINE('_JW_GOALS_POST_CONDITION', "Gönderme Durumu");
DEFINE('_JW_GOALS_TITLE_CONDITION', "Baslik Durumu");
DEFINE('_JW_GOALS_USERNAME_CONDITION', "Kullanici Durumu");
DEFINE('_JW_GOALS_IP_CONDITION', "IP Durumu");
DEFINE('_JW_GOALS_CAME_FROM_CONDITION', "Gelen Kaynak Durumu");
DEFINE('_JW_GOALS_BLOCK', "Engelleme");
DEFINE('_JW_GOALS_REDIRECT', "Yönlendirilen URL");
DEFINE('_JW_GOALS_HITS', "Hitler");
DEFINE('_JW_GOALS_ENABLED', "Aktif");
DEFINE('_JW_GOALS_EDIT', "Düzenle");
DEFINE('_JW_GOALS_DELETE', "Sil");
DEFINE('_JW_GOALS_DELETE_CONFIRM', "Bu hedef için tüm istatistikler silinecek. Gerçekten silmek istiyor musun.");

# Frontend
DEFINE('_JW_FRONTEND_COUNTRIES', "Ülkeler");
DEFINE('_JW_FRONTEND_VISITORS', "Ziyaretçiler");
DEFINE('_JW_FRONTEND_TODAY', "Bugün");
DEFINE('_JW_FRONTEND_YESTERDAY', "Dün");
DEFINE('_JW_FRONTEND_THIS_WEEK', "Bu Hafta");
DEFINE('_JW_FRONTEND_LAST_WEEK', "Geçen Hafta");
DEFINE('_JW_FRONTEND_THIS_MONTH', "Bu Ay");
DEFINE('_JW_FRONTEND_LAST_MONTH', "Geçen Ay");
DEFINE('_JW_FRONTEND_TOTAL', "Toplam");

# Settings description - quite long
DEFINE('_JW_DESC_DEBUG', "JoomlaWatch is in debug mode. This way you can discover error causes. To turn it off, please change the value JOOMLAWATCH_DEBUG in /components/com_joomlawatch/config.php from 1 to 0");
DEFINE('_JW_DESC_STATS_MAX_ROWS', "Max rows to show when stats are in expanded mode.");
DEFINE('_JW_DESC_STATS_IP_HITS', "All IP addresses that have less hits in previous days than this value will be deleted from IP history.");
DEFINE('_JW_DESC_STATS_URL_HITS', "All URLs that have less hits in previous days than this value will be deleted from IP history.");
DEFINE('_JW_DESC_IGNORE_IP', "Exclude certain IP from stats. Separate with a new line. You can use wildcards here. <br/>Eg. 192.* will ignore 192.168.51.31, 192.168.16.2, etc..");
DEFINE('_JW_DESC_UPDATE_TIME_VISITS', "Visitors refresh time in miliseconds, default is 2000, be careful with this. Then reload the JoomlaWatch back-end.");
DEFINE('_JW_DESC_UPDATE_TIME_STATS', "Stats refresh time in miliseconds, default is 4000, be careful with this. Then reload the JoomlaWatch back-end.");
DEFINE('_JW_DESC_MAXID_BOTS', "How many bot visits to keep in a database.");
DEFINE('_JW_DESC_MAXID_VISITORS', "How many real visits to keep in a database.");
DEFINE('_JW_DESC_LIMIT_BOTS', "How many bots you'll see in back-end.");
DEFINE('_JW_DESC_LIMIT_VISITORS', "How many real visitors you'll see in back-end.");
DEFINE('_JW_DESC_TRUNCATE_VISITS', "Maximum characters to be shown in long titles and uris.");
DEFINE('_JW_DESC_TRUNCATE_STATS', "Maximum characters to be shown in right statistics panel.");
DEFINE('_JW_DESC_STATS_KEEP_DAYS', "Days to keep statistics in database, 0 = infinite.");
DEFINE('_JW_DESC_TIMEZONE_OFFSET', "When you are in a different timezone than your hosting server. (positive or negative value in hours)");
DEFINE('_JW_DESC_WEEK_OFFSET', "Week offset, the timestamp/(3600*24*7) gives the week number from 1.1.1970, this offset is a correction to make it start with monday ");
DEFINE('_JW_DESC_DAY_OFFSET', "Day offset, the timestamp/(3600*24) gives the day number from 1.1.1970, this offset is a correction to make it start at 00:00 ");
DEFINE('_JW_DESC_FRONTEND_HIDE_LOGO', "<b>(functional in ad-free version)</b> To use a blank 1x1px icon in front-end");
DEFINE('_JW_DESC_IP_STATS', "To enable the IP address statistics. In some countries to keep the IP in a database for a longer time is prohibited by a law. Use at your own risk.");
DEFINE('_JW_DESC_HIDE_ADS', "This setting hides the ads in the backend, if they really annoy you. By keeping them, you support the further development of this tool. Thank you");
DEFINE('_JW_DESC_TOOLTIP_ONCLICK', "Uncheck, if you want to display tooltip on mouse-over, instead of mouse click.");
DEFINE('_JW_DESC_SERVER_URI_KEY', "Default is 'REDIRECT_URL', which is standard if you use url rewriting, can be set to 'SCRIPT_URL' if it logs only index.php");
DEFINE('_JW_DESC_BLOCKING_MESSAGE', "Message that's shown to blocked user or further information why you are blocking these users.");
DEFINE('_JW_DESC_TOOLTIP_WIDTH', "Tooltip width");
DEFINE('_JW_DESC_TOOLTIP_HEIGHT', "Tooltip height");
DEFINE('_JW_DESC_TOOLTIP_URL', "You can put any URL here, to visualize the visitor's ip. The {ip} will be replaced by the visitor's ip. Eg. http://somewebsite.com/query?iplookup={ip}");
DEFINE('_JW_DESC_IGNORE_URI', "You can type any URI you want to be ignored from stats. You can use wildcards (* and ?) here. Eg.: /freel?n* ");
DEFINE('_JW_DESC_GOALS_NAME', "Specify a goal name here. This name you will see in stats.");
DEFINE('_JW_DESC_GOALS_URI_CONDITION', "Everything that is after your domain name. For http://www.codegravity.com/projects/ the URI is: /projects/ (Example to use: <b>/projects*</b>)");
DEFINE('_JW_DESC_GOALS_GET_VAR', "GET variable is a variable which you can see in the URL usually after a ? or &amp; sign. Eg. http://www.codegravity.com/index.php?<u>name</u>=peter&amp;<u>surname</u>=smith. You can use also <u>*</u> in this field to scan all the get values. (Example to use: <b>n*me</b>)");
DEFINE('_JW_DESC_GOALS_GET_CONDITION', "Here you have to specify a match for a value from the previous field. (Example to use: <b>p?t*r</b>) ");
DEFINE('_JW_DESC_GOALS_POST_VAR', "Quite similar, but we're checking for the values submitted from forms. So when you have a form on your website, that has a field &lt;input type='text' name='<u>experiences</u>' /&gt;. (Example to use: <b>exper*ces</b>)");
DEFINE('_JW_DESC_GOALS_POST_CONDITION', "A match for the value from this POST field. Eg. we want to check, whether the user has java experiences. (Example to use: <b>*java*</b>)");
DEFINE('_JW_DESC_GOALS_TITLE_CONDITION', "A title of a page that has to match. (Example to use: <b>*freelance programmers*</b>)");
DEFINE('_JW_DESC_GOALS_USERNAME_CONDITION', "A name of a logged-in user. (Example to use: <b>psmith*</b>)");
DEFINE('_JW_DESC_GOALS_IP_CONDITION', "IP that a user comes from: (Example to use: <b>201.9?.*.*</b>)");
DEFINE('_JW_DESC_GOALS_CAME_FROM_CONDITION', "An URL that the user came from. (Example to use: <b>*www.google.*</b>)");
DEFINE('_JW_DESC_GOALS_REDIRECT', "User is redirected to an URL specified by you. Has a higher priority than 'blocking': (Example to use: <b>http://www.codegravity.com/goaway.html</b>)");
DEFINE('_JW_DESC_TRUNCATE_GOALS', "How many chars to truncate in goals table");
DEFINE('_JW_DESC_FRONTEND_NO_BACKLINK', "Backlink to codegravity.com, you can disable it, but we'll appreciate you will keep it there. Thank you");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES', "Display countries total stats in the frontend module. If changed, this setting will be effective in frontend after time set in CACHE_FRONTEND_ ");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_FIRST', "If you want to swap the order of Visitors/Countries in frontend. Uncheck it, and Visitors will appear first.");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_NUM', "Number of countries to show in frontend");
DEFINE('_JW_DESC_FRONTEND_VISITORS', "Display countries visitors in the frontend module. If changed, this setting will be effective in frontend after time set in CACHE_FRONTEND_");
DEFINE('_JW_DESC_CACHE_FRONTEND_COUNTRIES', "Time in seconds to cache fetching of countries total in frontend");
DEFINE('_JW_DESC_CACHE_FRONTEND_VISITORS', "Time in seconds to cache fetching of visitors in frontend");
DEFINE('_JW_DESC_FRONTEND_VISITORS_TODAY', "To show visitors in frontend for: today. If changed, this setting will be effective in frontend after time set in CACHE_FRONTEND_...");
DEFINE('_JW_DESC_FRONTEND_VISITORS_YESTERDAY', "To show visitors in frontend for: yesterday. If changed, this setting will be effective in frontend after time set in CACHE_FRONTEND_...");
DEFINE('_JW_DESC_FRONTEND_VISITORS_THIS_WEEK', "To show visitors in frontend for: this week. If changed, this setting will be effective in frontend after time set in CACHE_FRONTEND_...");
DEFINE('_JW_DESC_FRONTEND_VISITORS_LAST_WEEK', "To show visitors in frontend for: last week. If changed, this setting will be effective in frontend after time set in CACHE_FRONTEND_...");
DEFINE('_JW_DESC_FRONTEND_VISITORS_THIS_MONTH', "To show visitors in frontend for: this month. If changed, this setting will be effective in frontend after time set in CACHE_FRONTEND_...");
DEFINE('_JW_DESC_FRONTEND_VISITORS_LAST_MONTH', "To show visitors in frontend for: last month. If changed, this setting will be effective in frontend after time set in CACHE_FRONTEND_...");
DEFINE('_JW_DESC_FRONTEND_VISITORS_TOTAL', "To show total number visitors since JoomlaWatch installation. If changed, this setting will be effective in frontend after time set in CACHE_FRONTEND_...");
DEFINE('_JW_DESC_LANGUAGE', "Language file to use. They are placed in /components/com_joomlawatch/lang/. If you want to create a brand new language file, first check the project's homepage, and if the language file is still not there, just copy the default english.php to eg. german.php and place it in this directory. Then, translate all the key values on the right.");
DEFINE('_JW_DESC_GOALS', "Goals allow you to specify special parameters. When these parameters match, the goal counter is increased. This way you can monitor whether the user has visited a specific URL, posted a specific value, has a specific username or came from a specific address. You can also block or redirect such users to some other URL.");
DEFINE('_JW_DESC_GOALS_INSERT', "In all of the fields except the name you can use the * and ? as wildcards. For example: ?ear (will match: near, tear, ..),  p*r (will match: pr, peer, pear ..) ");
DEFINE('_JW_DESC_GOALS_BLOCK', "Set to 1, if you want the visitor to be blocked. He won't see the rest of the content, just the message that he was blocked - without any redirection and his IP is added to 'blocked' stats (Example to use: <b>1</b>)");

/* new translations */
DEFINE('_JW_GOALS_COUNTRY_CONDITION', "Ülke bilgileri");
DEFINE('_JW_DESC_GOALS_COUNTRY_CONDITION', "2-letter country code in upper case (Eg: <b>TH</b>)");
DEFINE('_JW_STATS_INBOUND',"Gelen");
DEFINE('_JW_STATS_FROM',"Geldigi yer");
DEFINE('_JW_STATS_TO',"Gittigi Yer");
DEFINE('_JW_STATS_ADD_TO_GOALS',"Hedefleri Ekle");
DEFINE('_JW_VISITS_ADD_GOAL_COUNTRY',"Bu ülke için hedef ekle");
DEFINE('_JW_MENU_REPORT_BUG',"Açik ve ya yenilik rapor et");
DEFINE('_JW_GOALS_COUNTRY',"Ülke");

/* translations 1.2.11b */
DEFINE('_JW_STATS_KEYPHRASE', "Keyphrases");
DEFINE('_JW_DESC_HISTORY_MAX_VALUES', "Maximum values in history tab (Example: <i>100</i>)");

DEFINE('_JW_DESC_ONLY_LAST_URI', "In visits show only last page visited, not all");
DEFINE('_JW_DESC_HIDE_REPETITIVE_TITLE', "In visits hide repetitive sitename in visited page title");
DEFINE('_JW_DESC_HISTORY_MAX_DB_RECORDS', "Maximum nuber of visitors to keep in database for Visit History. Be careful with this setting, if you have high traffic, it can grow really fast. Always check how much data the history table contains in Status");
DEFINE('_JW_DESC_UNINSTALL_KEEP_DATA', "Keep Database Tables on uninstall. Check this option before uninstall if you are doing an upgrade and want to keep your data.");

/* email reports */
DEFINE('_JW_DESC_EMAIL_REPORTS_ENABLED', "You'll receive nightly emails with reports for previous day, which you can read in the morning");
DEFINE('_JW_DESC_EMAIL_REPORTS_ADDRESS', "Email address to which you'll receive these reports");
DEFINE('_JW_DESC_EMAIL_PERCENT_HIGHER_THAN', "Only include rows in email reports where percentage is higher than {value}. Set to 0 if you don't want to use this feature <i>(example: 5)</i>");
DEFINE('_JW_DESC_EMAIL_ONE_DAY_CHANGE_POSITIVE', "Only include <b>positive one day</b> change values in email reports higher than {value} percent. Set to 0 if you don't want to use this feature <i>(example: 5)</i>");
DEFINE('_JW_DESC_EMAIL_ONE_DAY_CHANGE_NEGATIVE', "Only include <b>negative one day</b> change values in email reports lower than {value} percent. Set to 0 if you don't want to use this feature <i>(example: -10)</i>");
DEFINE('_JW_DESC_EMAIL_SEVEN_DAY_CHANGE_POSITIVE', "Only include <b>positive seven day</b> change values in email reports higher than {value} percent. Set to 0 if you don't want to use this feature <i>(example: 2)</i>");
DEFINE('_JW_DESC_EMAIL_SEVEN_DAY_CHANGE_NEGATIVE', "Only include <b>negative seven day</b> change values in email reports lower than {value} percent. Set to 0 if you don't want to use this feature <i>(example: -13)</i>");
DEFINE('_JW_DESC_EMAIL_THIRTY_DAY_CHANGE_POSITIVE', "Only include <b>positive thirty day</b> change values in email reports higher than {value} percent. Set to 0 if you don't want to use this feature <i>(example: 2)</i>");
DEFINE('_JW_DESC_EMAIL_THIRTY_DAY_CHANGE_NEGATIVE', "Only include <b>negative thirty day</b> change values in email reports lower than {value} percent. Set to 0 if you don't want to use this feature <i>(example: -13)</i>");

DEFINE('_JW_DESC_FRONTEND_NOFOLLOW', "<b>(functional in ad-free version)</b> Enable this setting if you want to make the logo link rendered with attribute rel='nofollow' ");
DEFINE('_JW_DESC_EMAIL_NAME_TRUNCATE', "Maximum characters of email row name. Change this if your email client message window is too small");

DEFINE('_JW_MENU_HISTORY', "History");
DEFINE('_JW_MENU_EMAILS', "Emails");
DEFINE('_JW_MENU_STATUS', "Status");
DEFINE('_JW_DESC_BLOCKED',"These IPs were blocked by anti-spam");


DEFINE('_JW_HISTORY_VISITORS',"Visitors History");
DEFINE('_JW_HISTORY_SHOWING_ONLY', "Showing only %d last records.
                To change this value, go to Settings -&gt; History &amp; Performance -&gt; HISTORY_MAX_DB_RECORDS . Be careful, this setting affects load times of the data below.  ");
DEFINE('_JW_MENU_BUG', "Report Bug");
DEFINE('_JW_MENU_FEATURE', "Request Feature");

DEFINE('_JW_VISITS_CAME_FROM_KEYWORDS',"Keywords");

DEFINE('_JW_BLOCKING_UNBLOCK',"unblock");
DEFINE('_JW_STATS_KEYPHRASE ',"Keyphrase");
DEFINE('_JW_STATUS_DATABASE',"Database status");

DEFINE('_JW_STATUS_DATABASE_TABLE_NAME',"table name");
DEFINE('_JW_STATUS_DATABASE_ROWS',"rows");
DEFINE('_JW_STATUS_DATABASE_DATA',"data");
DEFINE('_JW_STATUS_DATABASE_TOTAL',"total");

DEFINE('_JW_EMAIL_REPORTS',"Email Reports");
DEFINE('_JW_EMAIL_REPORT_GENERATED',"Generated filtered email report from yesterday");
DEFINE('_JW_EMAIL_REPORTS_VALUE_FILTERS',"Email Value Filters");
DEFINE('_JW_EMAIL_REPORTS_VALUE',"value");
DEFINE('_JW_EMAIL_REPORTS_PERCENT',"percent");
DEFINE('_JW_EMAIL_REPORTS_1DAY_CHANGE',"1-day change");
DEFINE('_JW_EMAIL_REPORTS_7DAY_CHANGE',"7-day change");
DEFINE('_JW_EMAIL_REPORTS_30DAY_CHANGE',"30-day change");
DEFINE('_JW_ANTISPAM_BLOCKED',"JoomlaWatch has blocked %d spammer hits today, total: %d");
DEFINE('_JW_ANTISPAM_ADDRESSES',"Blocked IP Adresses");
DEFINE('_JW_ANTISPAM_SETTINGS',"Anti-Spam Settings");
DEFINE('_JW_TRAFFIC_AJAX',"AJAX updates traffic");


DEFINE('_JW_HISTORY_PREVIOUS',"previous");
DEFINE('_JW_HISTORY_NEXT',"next");

/** additional translation for 1.2.11 for countries in more rows */
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_MAX_COLUMNS',"Number of columns of countries");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_MAX_ROWS',"Number of rows of countries");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_NAMES',"Display country names or not");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_FLAGS_FIRST',"Display flags first, then percents");

?>
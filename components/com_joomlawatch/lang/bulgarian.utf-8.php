<?php

#JoomlaWatch language file - to create a new language file, just copy the english.php to eg. german.php and place into /components/com_joomlawatch/lang/

# Main Menu
DEFINE('_JW_MENU_STATS', "Статистика");
DEFINE('_JW_MENU_GOALS', "Цели");
DEFINE('_JW_MENU_SETTINGS', "Настройки");
DEFINE('_JW_MENU_CREDITS', "Благодарности");
DEFINE('_JW_MENU_FAQ', "Често задавани въпроси");
DEFINE('_JW_MENU_DOCUMENTATION', "Документация");
DEFINE('_JW_MENU_LICENSE', "Лиценз");
DEFINE('_JW_MENU_DONATORS', "Дарители");
DEFINE('_JW_MENU_SUPPORT', "Поддържйте JoomlaWatch и рекламите няма да се показват.");

# Left visitors real-time window
DEFINE('_JW_VISITS_VISITORS', "Последни посетители");
DEFINE('_JW_VISITS_BOTS', "Ботове");
DEFINE('_JW_VISITS_CAME_FROM', "Идва от");
DEFINE('_JW_VISITS_MODULE_NOT_PUBLISHED', "Вашият модул JoomlaWach не е публикуван.За това не са записани никакви статистически данни. Отидете в раздел модули и да зададете модул JoomlaWatch - да се публикува във всички страници");
DEFINE('_JW_VISITS_PANE_LOADING', "Зареждане...");

# Right stats window
DEFINE('_JW_STATS_TITLE', "Статистика на посещенията през седмицата");
DEFINE('_JW_STATS_WEEK', "Седмица");
DEFINE('_JW_STATS_THIS_WEEK', "Тази седмица");
DEFINE('_JW_STATS_UNIQUE', "Уникални");
DEFINE('_JW_STATS_LOADS', "Зареждания");
DEFINE('_JW_STATS_HITS', "Хитове");
DEFINE('_JW_STATS_TODAY', "Днес");
DEFINE('_JW_STATS_FOR', "за");
DEFINE('_JW_STATS_ALL_TIME', "За целият период");
DEFINE('_JW_STATS_EXPAND', "Подробно");
DEFINE('_JW_STATS_COLLAPSE', "Съкратено");
DEFINE('_JW_STATS_URI', "Страници");
DEFINE('_JW_STATS_COUNTRY', "Държави");
DEFINE('_JW_STATS_USERS', "Потребители");
DEFINE('_JW_STATS_REFERERS', "Препратки");
DEFINE('_JW_STATS_IP', "IP адреси");
DEFINE('_JW_STATS_BROWSER', "Браузер");
DEFINE('_JW_STATS_OS', "ОС");
DEFINE('_JW_STATS_KEYWORDS', "Ключови думи");
DEFINE('_JW_STATS_GOALS', "Цели");
DEFINE('_JW_STATS_TOTAL', "Общо");
DEFINE('_JW_STATS_DAILY', "За определен ден");
DEFINE('_JW_STATS_DAILY_TITLE', "Дневна статистика за");
DEFINE('_JW_STATS_ALL_TIME_TITLE', "Обща статистика");
DEFINE('_JW_STATS_LOADING', "Зареждане...");
DEFINE('_JW_STATS_LOADING_WAIT', "Зареждане... моля изчакайте");
DEFINE('_JW_STATS_IP_BLOCKING_TITLE', "Блокиран IP адрес");
DEFINE('_JW_STATS_IP_BLOCKING_ENTER', "Въведете ръчно IP адреса, който искате да блокирате.");
DEFINE('_JW_STATS_IP_BLOCKING_MANUALLY', "Напишете IP адреса, който искате да блокирате. (напр. 217.242.11.54 или 217.* или 217.242.* за блокиране на обсега след звездичката)");
DEFINE('_JW_STATS_IP_BLOCKING_TOGGLE', "Превключване на блокирани IP адреси");
DEFINE('_JW_STATS_PANE_LOADING', "Зареждам статистиката...");

# Settings
DEFINE('_JW_SETTINGS_TITLE', "Настройки");
DEFINE('_JW_SETTINGS_DEFAULT', "Стандартни");
DEFINE('_JW_SETTINGS_SAVE', "Запомни");
DEFINE('_JW_SETTINGS_APPEARANCE', "Външен вид");
DEFINE('_JW_SETTINGS_FRONTEND', "Изглед на сайта");
DEFINE('_JW_SETTINGS_HISTORY_PERFORMANCE', "История и изпълнения");
DEFINE('_JW_SETTINGS_ADVANCED', "Разширения");
DEFINE('_JW_SETTINGS_IGNORE', "Игнориране");
DEFINE('_JW_SETTINGS_BLOCKING', "Блокиране");
DEFINE('_JW_SETTINGS_EXPERT', "Експерт");
DEFINE('_JW_SETTINGS_RESET_CONFIRM', "Наистина ли искате да изтриете всички данни?");
DEFINE('_JW_SETTINGS_RESET_ALL', "Изчистване на всички данни");
DEFINE('_JW_SETTINGS_RESET_ALL_LINK', "Изчисти всички статистически данни и посетители");
DEFINE('_JW_SETTINGS_LANGUAGE', "Език");
DEFINE('_JW_SETTINGS_SAVED', "Настройките бяха съхранени");
DEFINE('_JW_SETTINGS_ADD_YOUR_IP', "Добавете Вашия IP адрес");
DEFINE('_JW_SETTINGS_TO_THE_LIST', "в списъка.");

# Other / mostly general
DEFINE('_JW_TITLE', "AJAX монитор за Joomla CMS");
DEFINE('_JW_BACK', "Назад");
DEFINE('_JW_ACCESS_DENIED', "Нямате права за достъп");
DEFINE('_JW_LICENSE_AGREE', "Съгласен съм с условията");
DEFINE('_JW_LICENSE_CONTINUE', "Продължи");
DEFINE('_JW_SUCCESS', "Операцията приключи успешно");
DEFINE('_JW_RESET_SUCCESS', "Всички статистически данни и данни за посетителите бяхя изтрити");
DEFINE('_JW_RESET_ERROR', "Данните не бяха изтрити, нещо не се получи");
DEFINE('_JW_CREDITS_TITLE', "Благодарности");
DEFINE('_JW_TRENDS_DAILY_WEEKLY', "Дневни и седмични статистически данни за:");
DEFINE('_JW_AJAX_PERMISSION_DENIED_1', "AJAX достъп е отхвърлен: Моля, разгледайте статистиката на домейна, който сте посочили в configuration.php на Джумла - ");
DEFINE('_JW_AJAX_PERMISSION_DENIED_2', "Може би просто сте забравили да въведете WWW. преди името на домейна в браузъра. Javascript търси садържание ");
DEFINE('_JW_AJAX_PERMISSION_DENIED_3', "на");
DEFINE('_JW_AJAX_PERMISSION_DENIED_4', "това го принуждава да си мисли,че че това е друг домейн.");

# Header
DEFINE('_JW_HEADER_DOWNLOAD', "Вземете най-новия код на това разширение от");
DEFINE('_JW_HEADER_CAST_YOUR', "Изпратете своя");
DEFINE('_JW_HEADER_VOTE', "Глас");

# Tooltips
DEFINE('_JW_TOOLTIP_CLICK', "Кликнете за да отворите tooltip прозореца");
DEFINE('_JW_TOOLTIP_MOUSE_OVER', "Отидете с мишката на tooltip за да отворите прозореца");
DEFINE('_JW_TOOLTIP_YESTERDAY_INCREASE', "Увеличение от вчера");
DEFINE('_JW_TOOLTIP_HELP', "Отвари онлайн помощ за");
DEFINE('_JW_TOOLTIP_WINDOW_CLOSE', "Затворете този прозорец");
DEFINE('_JW_TOOLTIP_PRINT', "Печат");

# Goals
DEFINE('_JW_GOALS_INSERT', "Добавяне на нова цел");
DEFINE('_JW_GOALS_UPDATE', "Редактиране на цел номер");
DEFINE('_JW_GOALS_ACTION', "Действие");
DEFINE('_JW_GOALS_TITLE', "Нова цел");
DEFINE('_JW_GOALS_NEW', "Нова цел");
DEFINE('_JW_GOALS_RELOAD', "Обновяване");
DEFINE('_JW_GOALS_ADVANCED', "Разширено");
DEFINE('_JW_GOALS_NAME', "Име");
DEFINE('_JW_GOALS_ID', "id");
DEFINE('_JW_GOALS_URI_CONDITION', "URI състояние");
DEFINE('_JW_GOALS_GET_VAR', "GET променлива");
DEFINE('_JW_GOALS_GET_CONDITION', "GET състояние");
DEFINE('_JW_GOALS_POST_VAR', "POST променлива");
DEFINE('_JW_GOALS_POST_CONDITION', "POST състояние");
DEFINE('_JW_GOALS_TITLE_CONDITION', "Title състояние");
DEFINE('_JW_GOALS_USERNAME_CONDITION', "Username състояние");
DEFINE('_JW_GOALS_IP_CONDITION', "IP състояние");
DEFINE('_JW_GOALS_CAME_FROM_CONDITION', "'Идва от адрес..' състояние");
DEFINE('_JW_GOALS_BLOCK', "Блокирай");
DEFINE('_JW_GOALS_REDIRECT', "Пренасочи на URL");
DEFINE('_JW_GOALS_HITS', "Посещения");
DEFINE('_JW_GOALS_ENABLED', "Разрешени");
DEFINE('_JW_GOALS_EDIT', "Редактиране");
DEFINE('_JW_GOALS_DELETE', "Изтриване");
DEFINE('_JW_GOALS_DELETE_CONFIRM', "Вие ще загубите всички съществуващи статистически данни за тази цел. Наистина ли искате да изтриете тази цел?");

# Frontend
DEFINE('_JW_FRONTEND_COUNTRIES', "Държава");
DEFINE('_JW_FRONTEND_VISITORS', "Посетители");
DEFINE('_JW_FRONTEND_TODAY', "Днес");
DEFINE('_JW_FRONTEND_YESTERDAY', "Вчера");
DEFINE('_JW_FRONTEND_THIS_WEEK', "Тази седмица");
DEFINE('_JW_FRONTEND_LAST_WEEK', "Миналата седмица");
DEFINE('_JW_FRONTEND_THIS_MONTH', "Този месец");
DEFINE('_JW_FRONTEND_LAST_MONTH', "Миналият месец");
DEFINE('_JW_FRONTEND_TOTAL', "Общо");

# Settings description - quite long
DEFINE('_JW_DESC_DEBUG', "JoomlaWatch е в debug móde. По този начин ще могат да се определят причините за грешки. За да изключите, моля, променете стойността JOOMLAWATCH_DEBUG в /components/com_joomlawatch/config.php от 1 на 0");
DEFINE('_JW_DESC_STATS_MAX_ROWS', "Максимален брой на редовете на данни след декомпресия в областта на статистиката");
DEFINE('_JW_DESC_STATS_IP_HITS', "Всички IP адреси, които имат по-малко хитове от предишните дни, ще бъдат изтрити от историята на IP адреси.");
DEFINE('_JW_DESC_STATS_URL_HITS', "Всички URL адреси, които са по-малко хитове от предишните дни, ще бъде заличена от историята URL адреси.");
DEFINE('_JW_DESC_IGNORE_IP', "Игнориране на IP адрес в статистиката.Всеки адрес отделно на нов ред. Можете да използвате звездичка. <br/>Например. 192.* ще игнорира 192.168.51.31, 192.168.16.2, итн..");
DEFINE('_JW_DESC_UPDATE_TIME_VISITS', "Продължителност на подновяване на посетителите в левия панел, в милисекунди, по подразбиране 2000, бъдете внимателни с тази настройка.За да има ефект, обновете през администрацията интерфейса в JoomlaWatch.");
DEFINE('_JW_DESC_UPDATE_TIME_STATS', "Продължителност на подновяване на статистиката в десния панел в милисекунди, стандартно 2000, бъдете внимателни с тази настройка. а да има ефект, обновете през администрацията интерфейса в JoomlaWatch.");
DEFINE('_JW_DESC_MAXID_BOTS', "Колко записа да се съхраняват в базата данни.");
DEFINE('_JW_DESC_MAXID_VISITORS', "Колко записа на посетителите да се съхраняват в базата данни.");
DEFINE('_JW_DESC_LIMIT_BOTS', "Колко записа да се видат в левия панел в администраторския интерфейс.");
DEFINE('_JW_DESC_LIMIT_VISITORS', "Колко посетителите да се виждат в левия панел в администраторския интерфейс.");
DEFINE('_JW_DESC_TRUNCATE_VISITS', "Максимум символи, които ще бъдат показани при дълги заглавия и URI адреси");
DEFINE('_JW_DESC_TRUNCATE_STATS', "Максимум символи, които ще да бъдат показани при дълги заглавия в десния статистически панел");
DEFINE('_JW_DESC_STATS_KEEP_DAYS', "Брой дни през които да се запази общата история на статистическите данни в базата данни. 0 = безкрайност.");
DEFINE('_JW_DESC_TIMEZONE_OFFSET', "Ако сте в различна часова зона от сървъра ви, на който хоствате сайта си.(Въведете положително или отрицателно число, като разликата в часова зона)");
DEFINE('_JW_DESC_WEEK_OFFSET', "Промяната в рамките на седмицата, Timestamp/(3600 * 24 * 7) връща номера на седмицата от 1.1.1970, тази промяна е корекция на седмицата, започваща с понеделник. При нормални случаи, не е необходимо да се променя.");
DEFINE('_JW_DESC_DAY_OFFSET', "Промяната в рамките на деня, Шimestamp/(3600*24) връща датата на деня от 1.1.1970, тази промяна е корекция, за да започва денят от 00:00. При нормални случаи, не е необходимо да се променя");
DEFINE('_JW_DESC_FRONTEND_HIDE_LOGO', "Заменете логото на JoomlaWatch с невидимо лого на сайта");
DEFINE('_JW_DESC_IP_STATS', "Използване на IP адрес в статистиката. В някои страни, IP адреса се счита за лични данни. Използвайте на свой собствен риск.");
DEFINE('_JW_DESC_HIDE_ADS', "This setting hides the ads in the backend, if they really annoy you. By keeping them, you support the further development of this tool. Thank you");
DEFINE('_JW_DESC_TOOLTIP_ONCLICK', "Ако искате да се покажат непроверените  в отделен прозорец  карти и схеми , поставете  курсора върху иконите, вместо да кликнете.");
DEFINE('_JW_DESC_SERVER_URI_KEY', "По подразбиране е тази настройка 'REDIRECT_URL', ако използвате url rewriting, можете да пормените на 'SCRIPT_URL' ако във вашата статистика се показва само index.php");
DEFINE('_JW_DESC_BLOCKING_MESSAGE', "Собщение, което показва допълнителна информация по някакъв повод, защо са тези потребители блокирани.");
DEFINE('_JW_DESC_TOOLTIP_WIDTH', "Ширина на прозореца в tooltip");
DEFINE('_JW_DESC_TOOLTIP_HEIGHT', "Височина на прозореца в tooltip");
DEFINE('_JW_DESC_TOOLTIP_URL', "Тук можете да напишете всякакъв адрес за визуализиране на посетителския IP адрес.{ip} ще бъде заменен от реалният IP адреса на потребителя. Например: http://somewebsite.com/query?iplookup={ip}");
DEFINE('_JW_DESC_IGNORE_URI', "Можете да въведете всякакъв URI, които искате да бъде игнориран от статистика. Можете да използвате заместващи символи (* и ?) тук. Напр. : /freel?n* ");
DEFINE('_JW_DESC_GOALS_NAME', "Посочете името на целта. Това име ще видите в статистиката.");
DEFINE('_JW_DESC_GOALS_URI_CONDITION', "Това, което се намира непосредствено след адреса на вашия домейн. За http://www.codegravity.com/projects/ е URI: /projects/ (Пример: <b>/projects*</b>)");
DEFINE('_JW_DESC_GOALS_GET_VAR', "GET променлива е обикновено това, което виждате в URL обикновено след  знака ? или & amp. Например: http://www.codegravity.com/index.php? <u> име </u> u = peter & amp <u> фамилия </u> = smith.Можете също да използвате <u> * </u> в това поле, за всички GET променливи.(Пример: <b>m*no</b>)");
DEFINE('_JW_DESC_GOALS_GET_CONDITION', "Тук можете да определите на какво да се равни променливата в предишното поле.(Пример: <b>p?t*r</b>) ");
DEFINE('_JW_DESC_GOALS_POST_VAR', "Нещо подобно, но спазваме стойностите, вписани във формуляра. Така, че ако имате на уебсайта си формуляр, който има полето за въвеждане <input type='text' name='<u>регистрация</u>' />. (Пример: <b>sk*страция</b>)");
DEFINE('_JW_DESC_GOALS_POST_CONDITION', "Стойност, която е равна на променливата в предходното POST поле. Напр. Искаме да видим дали потребителят е направил опит за въвеждане в полето стойност  на Java. (Пример: <b>*java*</b>)");
DEFINE('_JW_DESC_GOALS_TITLE_CONDITION', "Заглавие на страница, която да съответства. (Пример: <b>*freelance programmers*</b>)");
DEFINE('_JW_DESC_GOALS_USERNAME_CONDITION', "Име на влезлияг потребител, за когото целта се прилага. (Пример: <b>psmith*</b>)");
DEFINE('_JW_DESC_GOALS_IP_CONDITION', "IP който идва от адрес. (Пример: <b>201.9?.*.*</b>)");
DEFINE('_JW_DESC_GOALS_CAME_FROM_CONDITION', "URL от които посетителят идва. (Пример: <b>*www.google.*</b>)");
DEFINE('_JW_DESC_GOALS_REDIRECT', "Ако тези условия са изпълнени, можете за пренасочите потребителя  към адрес по Ваш избор. Има по-висок приоритет, отколкото 'блокиран': (Пример: <b>http://www.codegravity.com/chod-prec.html</b>)");
DEFINE('_JW_DESC_TRUNCATE_GOALS', "Колко символа максимум да се показват в таблицата за дълги имена");
DEFINE('_JW_DESC_FRONTEND_NO_BACKLINK', "Препратката до codegravity.com, можете да промените тази настройка, но оценяваме, ако тя остане.");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES', "Преглед на общата статистика в модула на сайта. Ако промените настройките, трябва да изчакате да изтече времето в CACHE_FRONTEND_  за да настъпят промените.");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_FIRST', "Ако искате  да пренаредите реда на Посетители/Държави в модула на страницата. Махнете го, статистиката на Посетителите ще се покаже на първо място,а след това Държави.");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_NUM', "Брой на Държавите, които желаете да се показват в модула на сайта");
DEFINE('_JW_DESC_FRONTEND_VISITORS', "Преглед на Държавите в модула на сайта. Ако промените настройките, трябва да изчакате да изтече времето в CACHE_FRONTEND_  за да настъпят промените.");
DEFINE('_JW_DESC_CACHE_FRONTEND_COUNTRIES', "Време в секунди колко често да се актуализират статистическите данни на Държавите в модула на сайта");
DEFINE('_JW_DESC_CACHE_FRONTEND_VISITORS', "Време в секунди колко често да се актуализира статистиката за посетителите в модула на сайта");
DEFINE('_JW_DESC_FRONTEND_VISITORS_TODAY', "Преглед на посетителите на сайта за: Днес. Ако промените настройките, трябва да изчакате да изтече времето в CACHE_FRONTEND_  за да настъпят промените.");
DEFINE('_JW_DESC_FRONTEND_VISITORS_YESTERDAY', "Преглед на посетителите на сайта за: Вчера. Ако промените настройките, трябва да изчакате да изтече времето в CACHE_FRONTEND_  за да настъпят промените.");
DEFINE('_JW_DESC_FRONTEND_VISITORS_THIS_WEEK', "Преглед на посетителите на сайта за: Тази седмица. Ако промените настройките, трябва да изчакате да изтече времето в CACHE_FRONTEND_  за да настъпят промените.");
DEFINE('_JW_DESC_FRONTEND_VISITORS_LAST_WEEK', "Преглед на посетителите на сайта за: Миналата седмиа. Ако промените настройките, трябва да изчакате да изтече времето в CACHE_FRONTEND_  за да настъпят промените.");
DEFINE('_JW_DESC_FRONTEND_VISITORS_THIS_MONTH', "Преглед на посетителите на сайта за: Този месец. Ако промените настройките, трябва да изчакате да изтече времето в CACHE_FRONTEND_  за да настъпят промените.");
DEFINE('_JW_DESC_FRONTEND_VISITORS_LAST_MONTH', "Преглед на посетителите на сайта за: Миналият месец. Ако промените настройките, трябва да изчакате да изтече времето в CACHE_FRONTEND_  за да настъпят промените.");
DEFINE('_JW_DESC_FRONTEND_VISITORS_TOTAL', "Преглед на посетителите на сайта Общо след инсталацията на JoomlaWatch. Ако промените настройките, трябва да изчакате да изтече времето в CACHE_FRONTEND_  за да настъпят промените.");
DEFINE('_JW_DESC_LANGUAGE', "Езиков файл, който се използва. Езиковите файлове се намират в /components/com_joomlawatch/lang/. За да добавите нов език, първо се уверете, че той не е вече на страниците на този проект. Ако там го няма, копирайте english.php и го преименувайте на mojjazyk.php например и го поставете на по-горния адрес. Преведете всички изрази в дясно. Най-добре е да използвате кодировка UTF-8");
DEFINE('_JW_DESC_GOALS', "Целите Ви позволяват да се определят специални параметри. Ако с тези параметри сте съгласни, производителността на брояча се увеличава. По този начин можете да наблюдавате дали един посетител е посещавал специфични URL, дали е изпратил конкретна стойност във формуляра, има специфично име или идва от някой адрес. Можете да блокирате такъв посетител или да го пренасочете към специален URL адрес.");
DEFINE('_JW_DESC_GOALS_INSERT', "Във всички полета осен имена може да използвате символите * и ?. Например: ?ear (комбинира: near, tear, ..),  p*r (комбинира: pr, peer, pear ..) ");
DEFINE('_JW_DESC_GOALS_BLOCK', "Настройте на 1, ако искате да блокирате даден посетител, който отговаря на критериите. Няма да виждате останалата част от съдържанието на сайта, само съобщение за неговото блокиране - без пренасочване, и неговия IP адрес ще бъде добавен към списъка на блокираните в статистиката. (Пример: <b>1</b>)");

/* new translations */
DEFINE('_JW_GOALS_COUNTRY_CONDITION', "Country изисквания ");
DEFINE('_JW_DESC_GOALS_COUNTRY_CONDITION', "Двубуквеният код на дадена държава (Напр.: <b>TH</b>)");
DEFINE('_JW_STATS_INTERNAL',"Нека продължим страницата");
DEFINE('_JW_STATS_FROM',"От");
DEFINE('_JW_STATS_TO',"До");
DEFINE('_JW_STATS_ADD_TO_GOALS',"Добави цел");
DEFINE('_JW_VISITS_ADD_GOAL_COUNTRY',"Добави цел за тази държава");
DEFINE('_JW_MENU_REPORT_BUG',"Напиши за грешки или дай предложение");
DEFINE('_JW_GOALS_COUNTRY',"Държава");

/* translations 1.2.8b_12 */
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_UPPERCASE',"Ако искате имената на Държавите да са изписани с главни букви на сайта (Напр.: GERMANY, UNITED KINGDOM вместо Germany, United Kingdom)");
DEFINE('_JW_DESC_CACHE_FRONTEND_USERS',"Време в секунди колко често да се актуализират статистическите данни на потребителите в сайта");
DEFINE('_JW_DESC_FRONTEND_VISITORS_TOTAL_INITIAL', "Първоначална стойност 'Общо:' на сайта. Полезно, ако сте преминали от друг статистически инструмент. (Напр. 20000). Ако не искате да използвате тази функция попълнете 0.");
DEFINE('_JW_DESC_IGNORE_USER', "Игнорирай потребитилите от този textbox. Потребителско име он-лайн. (Напр.: myself {нов ред} mark_*) ");
DEFINE('_JW_FRONTEND_USERS_MOST', "Най-активните потребители към днешна дата от общият брой");
DEFINE('_JW_DESC_SPAMWORD_BANS_ENABLED',"Включва блокиране на изброените спам думи, показани по-долу?");
DEFINE('_JW_DESC_SPAMWORD_LIST',"Най-използваните спам думи, използвани в спам ботове. Можете да използвате * и ?. (Напр.: ph?rmac*).Ако в настройките по-горе е разрешено, JoomlaWatch ще провери дали атакуващия изпратен формуляр ( HTTP POST заявка) към сайта Ви съвпада с една от тези думи. (Прилага се само за сайт под Joomla – форуми, коментари, това е доста ефективен начин за премахване на спам ботове, които се опитват да изпращат всякакви възможни формуляри.");
DEFINE('_JW_SETTINGS_ANTI_SPAM',"Anti-Spam");
DEFINE('_JW_DESC_FRONTEND_USER_LINK',"Линка, намиращ се в лицевия потребителски модул Ви позволява да посочите специфичен URL,който е отворен, когато потребителят кликне върху потребителското си име.  Трябва да съдържа низа {user}, ойто ще бъде заменен с истинското име на потребителя. (Напр. index.php?option=com_comprofiler&task=userProfile&user={user}) ");
/* translations 1.2.10 */
DEFINE('_JW_DESC_FRONTEND_NOFOLLOW', "Включете тази настройка, ако искате да направите връзка на логото с атрибута rel='nofollow'");

/* translations 1.2.11b */
DEFINE('_JW_STATS_KEYPHRASE', "Keyphrases");
DEFINE('_JW_DESC_HISTORY_MAX_VALUES', "Maximum values in history tab (Example: <i>100</i>)");

DEFINE('_JW_DESC_ONLY_LAST_URI', "In visits show only last page visited, not all");
DEFINE('_JW_DESC_HIDE_REPETITIVE_TITLE', "In visits hide repetitive sitename in visited page title");
DEFINE('_JW_DESC_HISTORY_MAX_DB_RECORDS', "Maximum nuber of visitors to keep in database for Visit History. Be careful with this setting, if you have high traffic, it can grow really fast. Always check how much data the history table contains in Status");
DEFINE('_JW_DESC_UNINSTALL_KEEP_DATA', "Keep Database Tables on uninstall. Check this option before uninstall if you are doing an upgrade and want to keep your data.");

/* email reports */
DEFINE('_JW_DESC_EMAIL_REPORTS_ENABLED', "You'll receive nightly emails with reports for previous day, which you can read in the morning");
DEFINE('_JW_DESC_EMAIL_REPORTS_ADDRESS', "Email address to which you'll receive these reports");
DEFINE('_JW_DESC_EMAIL_PERCENT_HIGHER_THAN', "Only include rows in email reports where percentage is higher than {value}. Set to 0 if you don't want to use this feature <i>(example: 5)</i>");
DEFINE('_JW_DESC_EMAIL_ONE_DAY_CHANGE_POSITIVE', "Only include <b>positive one day</b> change values in email reports higher than {value} percent. Set to 0 if you don't want to use this feature <i>(example: 5)</i>");
DEFINE('_JW_DESC_EMAIL_ONE_DAY_CHANGE_NEGATIVE', "Only include <b>negative one day</b> change values in email reports lower than {value} percent. Set to 0 if you don't want to use this feature <i>(example: -10)</i>");
DEFINE('_JW_DESC_EMAIL_SEVEN_DAY_CHANGE_POSITIVE', "Only include <b>positive seven day</b> change values in email reports higher than {value} percent. Set to 0 if you don't want to use this feature <i>(example: 2)</i>");
DEFINE('_JW_DESC_EMAIL_SEVEN_DAY_CHANGE_NEGATIVE', "Only include <b>negative seven day</b> change values in email reports lower than {value} percent. Set to 0 if you don't want to use this feature <i>(example: -13)</i>");
DEFINE('_JW_DESC_EMAIL_THIRTY_DAY_CHANGE_POSITIVE', "Only include <b>positive thirty day</b> change values in email reports higher than {value} percent. Set to 0 if you don't want to use this feature <i>(example: 2)</i>");
DEFINE('_JW_DESC_EMAIL_THIRTY_DAY_CHANGE_NEGATIVE', "Only include <b>negative thirty day</b> change values in email reports lower than {value} percent. Set to 0 if you don't want to use this feature <i>(example: -13)</i>");

DEFINE('_JW_DESC_FRONTEND_NOFOLLOW', "<b>(functional in ad-free version)</b> Enable this setting if you want to make the logo link rendered with attribute rel='nofollow' ");
DEFINE('_JW_DESC_EMAIL_NAME_TRUNCATE', "Maximum characters of email row name. Change this if your email client message window is too small");

DEFINE('_JW_MENU_HISTORY', "History");
DEFINE('_JW_MENU_EMAILS', "Emails");
DEFINE('_JW_MENU_STATUS', "Status");
DEFINE('_JW_DESC_BLOCKED',"These IPs were blocked by anti-spam");


DEFINE('_JW_HISTORY_VISITORS',"Visitors History");
DEFINE('_JW_HISTORY_SHOWING_ONLY', "Showing only %d last records.
                To change this value, go to Settings -&gt; History &amp; Performance -&gt; HISTORY_MAX_DB_RECORDS . Be careful, this setting affects load times of the data below.  ");
DEFINE('_JW_MENU_BUG', "Report Bug");
DEFINE('_JW_MENU_FEATURE', "Request Feature");

DEFINE('_JW_VISITS_CAME_FROM_KEYWORDS',"Keywords");

DEFINE('_JW_BLOCKING_UNBLOCK',"unblock");
DEFINE('_JW_STATS_KEYPHRASE ',"Keyphrase");
DEFINE('_JW_STATUS_DATABASE',"Database status");

DEFINE('_JW_STATUS_DATABASE_TABLE_NAME',"table name");
DEFINE('_JW_STATUS_DATABASE_ROWS',"rows");
DEFINE('_JW_STATUS_DATABASE_DATA',"data");
DEFINE('_JW_STATUS_DATABASE_TOTAL',"total");

DEFINE('_JW_EMAIL_REPORTS',"Email Reports");
DEFINE('_JW_EMAIL_REPORT_GENERATED',"Generated filtered email report from yesterday");
DEFINE('_JW_EMAIL_REPORTS_VALUE_FILTERS',"Email Value Filters");
DEFINE('_JW_EMAIL_REPORTS_VALUE',"value");
DEFINE('_JW_EMAIL_REPORTS_PERCENT',"percent");
DEFINE('_JW_EMAIL_REPORTS_1DAY_CHANGE',"1-day change");
DEFINE('_JW_EMAIL_REPORTS_7DAY_CHANGE',"7-day change");
DEFINE('_JW_EMAIL_REPORTS_30DAY_CHANGE',"30-day change");
DEFINE('_JW_ANTISPAM_BLOCKED',"JoomlaWatch has blocked %d spammer hits today, total: %d");
DEFINE('_JW_ANTISPAM_ADDRESSES',"Blocked IP Adresses");
DEFINE('_JW_ANTISPAM_SETTINGS',"Anti-Spam Settings");
DEFINE('_JW_TRAFFIC_AJAX',"AJAX updates traffic");


DEFINE('_JW_HISTORY_PREVIOUS',"previous");
DEFINE('_JW_HISTORY_NEXT',"next");

/** additional translation for 1.2.11 for countries in more rows */
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_MAX_COLUMNS',"Number of columns of countries");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_MAX_ROWS',"Number of rows of countries");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_NAMES',"Display country names or not");
DEFINE('_JW_DESC_FRONTEND_COUNTRIES_FLAGS_FIRST',"Display flags first, then percents");

?>
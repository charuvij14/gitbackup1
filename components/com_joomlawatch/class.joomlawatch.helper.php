<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchHelper {

    var $database;
    var $config;

    function JoomlaWatchHelper() {
        $this->database = new JoomlaWatchDB();
        $this->config = new JoomlaWatchConfig();
    }

    /**
     * helper
     */
    function getActualDateTime() {
        $date = JoomlaWatchHelper::date("d.m.Y", JoomlaWatchHelper::getServerTime());
        $time = JoomlaWatchHelper::date("H:i:s", JoomlaWatchHelper::getServerTime());
        return "$date $time";
    }

    /**
     * helper
     */
    function dayOfWeek() {
        //date_default_timezone_set(JoomlaWatchHelper::getTimezone());
        return JoomlaWatchHelper::date("w", JoomlaWatchHelper::getServerTime());
        //return JoomlaWatchHelper::date("w", $this->getServerTime());
    }

    /**
     * helper
     */
    function dayOfMonth() {
        //date_default_timezone_set(JoomlaWatchHelper::getTimezone());
        //return JoomlaWatchHelper::date("d", $this->getServerTime());
        return JoomlaWatchHelper::date("d", JoomlaWatchHelper::getServerTime());
    }

    /**
     * helper
     */
    function getIP2LocationURL($ip) {
        $url = $this->config->getConfigValue('JOOMLAWATCH_TOOLTIP_URL');
        $result = str_replace("{ip}", $ip, $url);
        //TODO : appears to be a bug in str_ireplace - doesn't work!
        return $result;
    }

    /**
     * helper
     */
    function resetData() {

        $query = sprintf("delete from #__joomlawatch");
        $result1 = $this->database->executeQuery($query);

        $query = sprintf("delete from #__joomlawatch_uri");
        $result2 = $this->database->executeQuery($query);

        $query = sprintf("delete from #__joomlawatch_info");
        $result3 = $this->database->executeQuery($query);

        $query = sprintf("delete from #__joomlawatch_blocked");
        $result4 = $this->database->executeQuery($query);

        $query = sprintf("delete from #__joomlawatch_goals");
        $result4 = $this->database->executeQuery($query);

        $query = sprintf("delete from #__joomlawatch_cache");
        $result4 = $this->database->executeQuery($query);

        $query = sprintf("delete from #__joomlawatch_uri2title");
        $result5 = $this->database->executeQuery($query);

        $query = sprintf("delete from #__joomlawatch_history");
        $result5 = $this->database->executeQuery($query);

        $query = sprintf("delete from #__joomlawatch_uri_history");
        $result5 = $this->database->executeQuery($query);

        return true;

    }


    /**
     * helper
     *
     * @return unknown
     */
    function isModulePublished() {
        $query = sprintf("select published from #__modules where module = 'mod_joomlawatch_agent' order by id desc limit 1; ");
        $published = $this->database->resultQuery($query);
        return $published;
    }

    /**
     * helper
     *
     * @return unknown
     */
    function jwDateToday() {
        $config = new JConfig();
        $offset = $config->offset;
        $daysDiff = ((date("U")) - (gmmktime(0, 0, 0, 1, 1, 1970))+  $offset*3600) / 86400;
        $diff = floor($daysDiff);
        return $diff;
    }

    /**
     * helper
     *
     * @return unknown
     */
    function getURI() {
        $redirURI = addslashes(strip_tags(@ $_SERVER[$this->config->getConfigValue('JOOMLAWATCH_SERVER_URI_KEY')]));
        $uri = addslashes(strip_tags(@ $_SERVER['REQUEST_URI']));

        if (@ $redirURI && @ substr($redirURI, -9, 9) != "index.php")
            $uri = $redirURI;

        return $uri;
    }

    /**
     * helper
     */
    function truncate($str, $len = "") {
        if (@ !$len)
            $len = $this->config->getConfigValue('JOOMLAWATCH_TRUNCATE_VISITS');

        if (strlen($str) < $len)
            return $str;
        else
            return substr($str, 0, $len) . "...";
    }
    /**
     * config
     */
    function saveSettings($post) {

        /* defines explicitely checkboxes */
        $checkboxNamesArray = array (
            'JOOMLAWATCH_FRONTEND_NO_BACKLINK',
            'JOOMLAWATCH_FRONTEND_HIDE_LOGO',
            'JOOMLAWATCH_IP_STATS',
            'JOOMLAWATCH_FRONTEND_NO_BACKLINK',
            'JOOMLAWATCH_FRONTEND_COUNTRIES',
            'JOOMLAWATCH_FRONTEND_COUNTRIES_UPPERCASE',
            'JOOMLAWATCH_FRONTEND_COUNTRIES_FIRST',
            'JOOMLAWATCH_FRONTEND_VISITORS',
            'JOOMLAWATCH_FRONTEND_VISITORS_TODAY',
            'JOOMLAWATCH_FRONTEND_VISITORS_YESTERDAY',
            'JOOMLAWATCH_FRONTEND_VISITORS_THIS_WEEK',
            'JOOMLAWATCH_FRONTEND_VISITORS_LAST_WEEK',
            'JOOMLAWATCH_FRONTEND_VISITORS_THIS_MONTH',
            'JOOMLAWATCH_FRONTEND_VISITORS_LAST_MONTH',
            'JOOMLAWATCH_FRONTEND_VISITORS_TOTAL',
            'JOOMLAWATCH_TOOLTIP_ONCLICK',
            'JOOMLAWATCH_ONLY_LAST_URI',
            'JOOMLAWATCH_HIDE_REPETITIVE_TITLE',
            'JOOMLAWATCH_UNINSTALL_KEEP_DATA',
            'JOOMLAWATCH_FRONTEND_COUNTRIES_NAMES',
            'JOOMLAWATCH_FRONTEND_COUNTRIES_FLAGS_FIRST'
        );
        JoomlaWatchHelper :: saveConfigValues($checkboxNamesArray, $post);

        return true;
    }

    /**
     * Used by both - save anti-spam and save settings
     * @param  $checkboxNamesArray
     * @param  $post
     * @return void
     */
    function saveConfigValues($checkboxNamesArray, $post) {

        foreach ($post as $key => $value) {
            if (strstr($key, "JOOMLAWATCH_")) {
                $this->config->saveConfigValue($key, $value);
            }
        }
        //hack :( explicitly save checkbox values
        foreach ($checkboxNamesArray as $key => $value) {
            if (@ !$post[$value]) { //if there is no value - checkbox unchecked
                $this->config->saveConfigValue($value, "Off");
            }
        }
        // explicitly reset chache because of frontend settings
        JoomlaWatchCache :: clearCache();

    }

    /**
     * helper
     */
    function getDateByDay($day, $format = "d.m.Y") {
        //date_default_timezone_set(JoomlaWatchHelper::getTimezone());
        //$date = JoomlaWatchHelper::date($format, $day * 3600 * 24);
        $date = JoomlaWatchHelper::date($format, $day * 3600 * 24);
        $output = $date;

        //        if ($format == "d.m.Y" && ($date == JoomlaWatchHelper::date($format, $this->getServerTime())))
        if ($format == "d.m.Y" && ($date == JoomlaWatchHelper::date($format, $day * 3600 * 24)))
            $output .= " (" . _JW_STATS_TODAY . ")";

        return $output;
    }

    function getUser() {
        jimport('joomla.utilities.date');
        $user = & JFactory::getUser();
        return $user;
    }

    function getUsername() {
        $user = & JoomlaWatchHelper::getUser();
        return $user->username;
    }

    function getTimezoneOffset() {

        // this is because of Warning: session_start() [function.session-start]: Cannot send session cookie - headers already sent
        $current_error_reporting = error_reporting();
        error_reporting(0);
        $user = & JoomlaWatchHelper::getUser();
        error_reporting($current_error_reporting);

        //	    if we have an anonymous user, then use global config, instead of the user params
        if($user->get('id')) {
            $userTimezone = $user->getParam('timezone');
        } else {
            $conf =& JFactory::getConfig();
            $userTimezone = $conf->getValue('config.offset');
        }
        /* if the user has no timezone defined, use the timezone from server configuratio */
        /*
                if (!isset($userTimezone)) {
        */
        $config = new JConfig();
        $userTimezone = $config->offset;

        if ($userTimezone && !is_numeric($userTimezone)) {
            try {
                $dtz = new DateTimeZone($userTimezone);
                $time = new DateTime('now', $dtz);
                $userTimezone = $time->format('P');
            } catch (Exception $e) {
                echo("<!-- exception ".$e->getMessage()." -->");
            }
        }

        /*
                }
        */

        $daylightSaving = 0;
        if (JOOMLAWATCH_SERVER_DAYLIGHT_SAVING) {
            $daylightSaving = date("I");
        }
        $serverTimezone = date("Z")/3600 - $daylightSaving;

        $offset = $userTimezone - $serverTimezone;
        if (JOOMLAWATCH_DEBUG) {
            echo("server timezone: $serverTimezone user timezone: $userTimezone difference: $offset daylight saving: $daylightSaving");
        }

        return $offset;
    }

    /**
     * helper
     */
    function getServerTime() {
        //        return time() + $this->config->getConfigValue('JOOMLAWATCH_TIMEZONE_OFFSET') * 3600;
        return time();
    }

    function serverTimeToLocal($time) {
        return $time + JoomlaWatchHelper::getTimezoneOffset();
    }

    function getLocalTime() {
        JoomlaWatchHelper::serverTimeToLocal(JoomlaWatchHelper::getServerTime());
    }


    /**
     * helper
     */
    // fnmatch PHP function only on UNIX :(, this replaces the wildcard search
    function wildcardSearch($pattern, $string) {
        return preg_match("#^" . strtr(preg_quote($pattern, '#'), array (
                                                                        '\*' => '.*',
                                                                        '\?' => '.'
                                                                  )) . "$#i", $string);
    }

    /**
     * helper
     *
     * @return unknown
     */
    function jwDateBySeconds($sec) {
        $date = floor($sec / 3600 / 24 + $this->config->getConfigValue('JOOMLAWATCH_DAY_OFFSET'));
        return $date;
    }

    /**
     * helper
     */
    function getDayByTimestamp($timestamp) {
        return floor( ($timestamp + $this->config->getConfigValue('JOOMLAWATCH_DAY_OFFSET')) / 24 / 3600 + $this->config->getConfigValue('JOOMLAWATCH_WEEK_OFFSET'));
    }

    /**
     * helper
     */
    function getWeekByTimestamp($timestamp) {
        return ceil(($timestamp +$this->config->getConfigValue('JOOMLAWATCH_DAY_OFFSET')) / 7 / 24 / 3600 + $this->config->getConfigValue('JOOMLAWATCH_WEEK_OFFSET'));
    }

    /**
     * helper
     */
    function getTooltipOnClick() {
        return $this->config->getCheckboxValue("JOOMLAWATCH_TOOLTIP_ONCLICK");
    }

    /**
     * helper
     * @return unknown
     */
    function getTooltipOnEvent() {
        if ($this->getTooltipOnClick()) {
            return "title='" . _JW_TOOLTIP_CLICK . "' onclick";
        } else {
            return "title='" . _JW_TOOLTIP_MOUSE_OVER . "' onmouseover";
        }
    }

    /**
     * helper
     */
    function getTooltipOnEventHide() {
        if (!$this->getTooltipOnClick()) {
            return "onClick='ajax_hideTooltip();'";
        }
        return;
    }

    /**
     * helper
     * @return unknown
     */
    function getAvailableLanguages() {
        $langDirPath = JPATH_BASE2 . DS . "components" . DS . "com_joomlawatch" . DS . "lang";

        if ($handle = @ opendir($langDirPath)) {
            while (false !== ($file = readdir($handle))) {
                if (strstr($file, ".php")) {
                    $file = str_replace(".php", "", $file);
                    $langArray[] = $file;
                }
            }
            closedir($handle);
        }
        sort($langArray);
        return @ $langArray;
    }

    /**
     * helper
     *
     * functions taken from
     * Open Web Application Security Project
     * (http://www.owasp.org)
     */
    // sanitize a string for SQL input (simple slash out quotes and slashes)
    function sanitize( $string ) {
        $db = & JFactory::getDBO();
        return $this->database->getEscaped($string); // Return the sanitized code
    }


    /**
     * helper
     */
    function countryByIp($ip) {
        if ($ip == '127.0.0.1') {
            /* ignore localhost */
            return;
        }

        $query = sprintf("select ip, country from #__joomlawatch where (ip = '%s' and country is not NULL) limit 1", $this->database->getEscaped($ip));
        $row3 = $this->database->resultQuery($query);

        if (@ !$row3->country) {

            $iplook = new ip2country($ip);
            $iplook->UseDB = true;
            $iplook->db_tablename = "#__joomlawatch_ip2c";

            if (($iplook->LookUp())) {
                $country = strtolower($iplook->Country);
                $query3 = sprintf("update #__joomlawatch set country = '%s' where ip = '%s'", $this->database->getEscaped($country), $this->database->getEscaped($ip));
                $this->database->executeQuery($query);
            }

        } else {
            $country = $row3->country;
        }

        return @ $country;
    }

    /**
     * helper
     */
    function countryCodeToCountryName($code) {
        $query = sprintf("select country from #__joomlawatch_cc2c where cc = '%s' limit 1", $this->database->getEscaped($code));
        $countryName = $this->database->resultQuery($query);
        return $countryName;
    }

    function getWeekFromTimestamp($timestamp) {
        //date_default_timezone_set(JoomlaWatchHelper::getTimezone());
        //return JoomlaWatchHelper::date("W", $timestamp);
        return JoomlaWatchHelper::date("W", $timestamp);
    }

    function getYearFromTimestamp($timestamp) {
        //date_default_timezone_set(JoomlaWatchHelper::getTimezone());
        //        return JoomlaWatchHelper::date("Y", $timestamp);
        return JoomlaWatchHelper::date("y", $timestamp);
    }

    function removeRepetitiveTitle($title) {
        if ($this->config->getCheckboxValue("JOOMLAWATCH_HIDE_REPETITIVE_TITLE")) {
            $config = new JConfig();
            $title = str_replace($config->sitename,"",$title);
        }
        return $title;
    }


    /**
     * filtering input get var
     * @param  $key
     * @return mixed
     */
    function requestGet($key) {
        if (isset($key)) {
            return JRequest::getVar($key);
        } else {
            return JRequest::get('get');
        }
    }

    /**
     * Filtering input post var
     * @param  $key
     * @return mixed
     */
    function requestPost($key) {
        if (isset($key)) {
            return JRequest::getVar($key);
        } else {
            return JRequest::get( 'post' );
        }

    }

    /**
     * Used as replacement for date to use joomla timezone
     * @param  $format
     * @param  $timestamp
     * @return string
     */
    function date($format, $timestamp) {

        $tz = JoomlaWatchHelper::getTimezoneOffset();
        return date($format, $timestamp + $tz * 3600);
    }


    /**
     * sender
     *
     * @param  $recipient
     * @param  $sender
     * @param  $subject
     * @param  $body
     * @return void
     */
    function sendEmail($recipient, $sender, $subject, $body) {

        JUtility::sendMail($recipient, $sender, $recipient, $subject, $body, 1, $cc, $bcc, $attachment, $replyto, $replytoname);
    }

    /**
     * Config
     * @param  $post
     * @return bool
     */
    function saveAntiSpamSettings($post) {

        $checkboxNamesArray = array (
            'JOOMLAWATCH_SPAMWORD_BANS_ENABLED'
        );

        JoomlaWatchHelper :: saveConfigValues($checkboxNamesArray, $post);
        return true;
    }

    /**
     * Config
     * @param  $post
     * @return bool
     */
    function saveEmailSettings($post) {

        $checkboxNamesArray = array (
            'JOOMLAWATCH_EMAIL_REPORTS_ENABLED'
        );

        JoomlaWatchHelper :: saveConfigValues($checkboxNamesArray, $post);
        return true;
    }
}


?>

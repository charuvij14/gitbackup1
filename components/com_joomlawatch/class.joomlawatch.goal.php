<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchGoal {

    var $database;
    var $helper;
    var $stat;

    function JoomlaWatchGoal() {
        $this->database = new JoomlaWatchDB();
        $this->helper = new JoomlaWatchHelper();
        $this->stat = new JoomlaWatchStat();
    }

    /**
     * goals
     */
    function getGoals() {
        $query = "select * from #__joomlawatch_goals ";
        $rows = @ $this->database->objectListQuery($query);
        return @ $rows;
    }

    /**
     * goals
     */
    function getGoalNameById($id) {
        $query = sprintf("select name from #__joomlawatch_goals where id = '%d'", (int) $id);
        $rows = @ $this->database->objectListQuery($query);
        $row = @ $rows[0];
        return @ $row->name;
    }


    /**
     * goals
     */
    function getGoalById($id) {

        //lower case id
        $id = strtolower($id);
        $query = sprintf("select * from #__joomlawatch_goals where id = '%d' limit 1 ", (int) $id);
        $rows = $this->database->asocListQuery($query);
        $row = @ $rows[0];
        return $row;

    }

    /**
     * goals
     */
    function deleteGoal($id) {
        $query = sprintf("delete from #__joomlawatch_goals where id = '%d' limit 1", (int) $id);
        //TODO delete everyting from logs as well!
        $result = $this->database->executeQuery($query);

        $query = sprintf("delete from #__joomlawatch_info where (`group`='".DB_KEY_GOALS."' and name='%d')", (int) $id);
        $this->database->executeQuery($query);
        return $result;
    }

    /**
     * goals
     */
    function enableGoal($id) {
        $query = sprintf("update #__joomlawatch_goals set disabled = NULL where id = '%d'", (int) $id);
        return $this->database->executeQuery($query);
    }

    /**
     * goals
     */
    function disableGoal($id) {
        $query = sprintf("update #__joomlawatch_goals set disabled = 1 where id = '%d'", (int) $id);
        return $this->database->executeQuery($query);
    }

    /**
     * goals
     */
    function getGoalCount($id) {
        $query = sprintf("select sum(value) as sum from #__joomlawatch_info where `group` = '".DB_KEY_GOALS."' and name = '%d'", (int) $id);
        $sum = $this->database->resultQuery($query);
        return $sum;
    }

    /**
     * goals
     */
    function saveGoal($post) {
        $id = @ $post['id'];
        if (@ $id) {
            //update
            $query = sprintf("update #__joomlawatch_goals  set ");
            $length = sizeof($post);
            if (@ $post['option']) {
                $length = $length -1;
            }
            $i = 0;
            foreach ($post as $key => $value) {
                $i++;
                if ($key == "id" || $key == "option") // ignore key and option  
                    continue;
                $key = strtolower($key);
                $query .= sprintf("%s = '%s' ", $this->database->getEscaped($key), $this->database->getEscaped($value));
                if ($i < $length -1)
                    $query .= ", ";
            }
            $query .= sprintf(" where id = '%d'", (int) $id);
            $result = $this->database->executeQuery($query);

        } else {
            // insert
            unset($post['id']); // when it comes from new goal
            $query = sprintf("insert into #__joomlawatch_goals (id, ");
            $length = sizeof($post);
            if (@ $post['option'])
                $length = $length -1;
            $i = 0;
            foreach ($post as $key => $value) {
                if ($key == "id" || $key == "option")
                    continue;
                $i++;
                $key = strtolower($key);
                $query .= sprintf("%s", $this->database->getEscaped($key));
                if ($i < $length)
                    $query .= ", ";
            }
            $query .= ") values ('',";
            $i = 0;
            foreach ($post as $key => $value) {
                if ($key == "id" || $key == "option")
                    continue;
                $i++;
                $key = strtolower($key);
                $query .= sprintf("'%s'", $this->database->getEscaped($value));
                if ($i < $length)
                    $query .= ",";
            }
            $query .= ")";
            $result = $this->database->executeQuery($query);
        }

        return $result;
    }

    /**
     * goals
     */
    function checkGoals($title, $username, $ip, $came_from, $liveSite = "") {
        global $mainframe;

        $query = sprintf("select * from #__joomlawatch_goals");
        $rows = $this->database->objectListQuery($query);

        if ($rows)
        foreach ($rows as $row) {

            $achieved = false;
            if ($row->disabled)
                continue;

            if (trim($row->uri_condition)) {
                if (@ $this->helper->wildcardSearch($row->uri_condition, trim($this->helper->getURI()))) {
                    $achieved = true;
                } else
                    continue;
            }
            if (trim($row->get_condition)) {
                if (@ $this->helper->wildcardSearch($row->get_condition, trim(JoomlaWatchHelper::requestGet($row->get_var)))) {
                    $achieved = true;
                } else
                    if ($row->get_var == "*") {
                        foreach (JoomlaWatchHelper::requestGet() as $get) {
                            if ($this->helper->wildcardSearch($row->get_condition, trim($get))) {
                                $achieved = true;
                            }
                        }
                    } else
                        continue;
            }
            if (trim($row->post_condition)) {
                if (@ $this->helper->wildcardSearch($row->post_condition, trim(JoomlaWatchHelper::requestPost($row->post_var)))) {
                    $achieved = true;
                } else
                    if ($row->post_var == "*") {
                        foreach (JoomlaWatchHelper::requestPost() as $post) {
                            if ($this->helper->wildcardSearch($row->post_condition, trim($post))) {
                                $achieved = true;
                            }
                        }
                    } else
                        continue;
            }
            if (trim($row->title_condition)) {
                if (@ $this->helper->wildcardSearch($row->title_condition, trim($title))) {
                    $achieved = true;
                } else
                    continue;
            }
            if (trim($row->username_condition)) {
                if (@ $this->helper->wildcardSearch($row->username_condition, trim($username))) {
                    $achieved = true;
                } else
                    continue;
            }
            if (trim($row->ip_condition)) {
                if (@ $this->helper->wildcardSearch($row->ip_condition, trim($ip))) {
                    $achieved = true;
                } else
                    continue;
            }
            if (trim($row->came_from_condition)) {
                if (@ $this->helper->wildcardSearch($row->came_from_condition, trim($came_from)) || $this->helper->wildcardSearch($liveSite.$row->came_from_condition, trim($came_from))) {
                    $achieved = true;
                } else
                    continue;
            }
            if (trim($row->country_condition)) {
                $country = $this->helper->countryByIp($ip);
                if (@ $this->helper->wildcardSearch($row->country_condition, trim($country))) {
                    $achieved = true;
                } else
                    continue;
            }

            if ($achieved) {
                $this->stat->increaseKeyValueInGroup(DB_KEY_GOALS, $row->id);

                if (@ $row->redirect) {
                    // for 1.0 only ?
                    mosRedirect(@ $row->redirect);
                }
                if (@ $row->block) {
                    $this->dieWithBlockingMessage($ip);
                }
            }

        }
    }

}

?>
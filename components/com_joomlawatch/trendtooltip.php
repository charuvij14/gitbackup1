<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2008 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

sleep(1); // it's a tooltip. Let's sleep for a while

include_once("includes.php");

$joomlaWatch = new JoomlaWatch();

$joomlaWatchHTML = new JoomlaWatchHTML();
$joomlaWatchTrendHTML = new JoomlaWatchTrendHTML($joomlaWatch);
$joomlaWatch->config->checkPermissions();

require ("lang" . DS . $joomlaWatch->config->getLanguage().".php");


echo $joomlaWatchHTML->renderAdminStyles();
echo $joomlaWatchHTML->renderPrint();
echo $joomlaWatchHTML->renderCloseWindow();
echo $joomlaWatchTrendHTML->renderTrends();
?>



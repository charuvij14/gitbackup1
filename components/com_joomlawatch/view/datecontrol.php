<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>


<table width='100%'>
    <tr><td align='left'><?php echo("<a href='javascript:setDay($prev)' id='$prev'>&lt;".JoomlaWatchHelper::date("d.m.Y",$prev*3600*24)."<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/calendar.gif' border='0' align='center' /></a>"); ?></td>
    <td align='center'><?php if ($day != $today)echo("<a href='javascript:setDay($today)' id='$today'>"._JW_STATS_TODAY."</a>"); ?></td>
    <td align='right'><?php if ($next <= $today) echo("<a href='javascript:setDay($next)' id='$next'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/calendar.gif' border='0' align='center' />".JoomlaWatchHelper::date("d.m.Y",$next*3600*24)."&gt;</a>"); ?></td>
    </tr>
</table>

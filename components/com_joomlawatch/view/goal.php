<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<center>
    <form action='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=goals&action=save' method='POST'>
        <?php $color=""; ?>
        <table width='80%' cellpadding='3'>
            <tr><td valign='top' align='left' colspan='3'>
                <h2><?php echo($action);?><?php echo JoomlaWatchHTML::renderOnlineHelp("goals-form"); ?></h2>
                <div align='center' style='border:1px solid black; width:600px; text-align: justify; text-justify: distribute; padding: 5px;'>
                    <?php echo _JW_DESC_GOALS_INSERT; ?>
                </div>
                <br/>
            </td></tr>
            <tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("NAME", $values, $color, true)); ?></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("URI_CONDITION", $values, $color)); ?></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("TITLE_CONDITION", $values, $color)); ?></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("USERNAME_CONDITION", $values, $color)); ?></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("IP_CONDITION", $values, $color)); ?></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("CAME_FROM_CONDITION", $values, $color)); ?></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("COUNTRY_CONDITION", $values, $color)); ?></tr>
            <tr><td></td></tr>
            <tr><td align='right' style='color: gray;'><b><?php echo(_JW_GOALS_ADVANCED."&nbsp;:");?></b></td></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("GET_VAR", $values, $color)); ?></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("GET_CONDITION", $values, $color)); ?></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("POST_VAR", $values, $color)); ?></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("POST_CONDITION", $values, $color)); ?></tr>
            <tr></tr>
            <tr><td align='right' style='color: gray;'><b><?php echo(_JW_GOALS_ACTION."&nbsp;:");?></b></td></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("BLOCK", $values, $color)); ?></tr>
            <tr><?php echo($this->joomlaWatchHTML->renderInputField("REDIRECT", $values, $color)); ?></tr>
            <tr><td></td></tr>
            <tr><td></td></tr>
            </tr>
            <tr><td colspan='3' align='center'>
                <br/><br/>
                <input type='submit' value='<?php echo $action; ?>' />
                <?php if (@$values) { ?>
                <input type='hidden' name='id' value='<?php echo @$values['id']; ?>' />
                <?php } ?>
            </td>
        </table>
    </form>
</center>
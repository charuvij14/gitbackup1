<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<link rel="stylesheet" type="text/css" href="<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/css/coda-slider.css" />
<script type="text/javascript" src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/js/jquery.js'></script>
<script type="text/javascript" src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/js/jquery-easing.1.2.js'></script>
<script type="text/javascript" src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/js/jquery-easing-compatibility.1.2.js'></script>
<script type="text/javascript" src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/js/coda-slider.1.1.1.js'></script>
<script type="text/javascript">
    jQuery.noConflict();
    jQuery(window).bind("load", function() {
        jQuery("div#slider1").codaSlider()
    });
</script>

<center>
    <form action='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=settingsSave' method='POST' id='settingsForm'>



        <div class="slider-wrap">
            <div align='left' style='text-align:left;'>
                <h2><?php echo(_JW_SETTINGS_TITLE); echo JoomlaWatchHTML::renderOnlineHelp("settings"); ?></h2>
                <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch'>&lt;&lt; <?php echo(_JW_BACK);?></a>
                | <a href="javascript:document.getElementById('settingsForm').submit();"> <?php echo(_JW_SETTINGS_SAVE);?></a>
            </div>
            <div id="slider1" class="csw">
                <div class="panelContainer">

                    <div class="panel" title="<?php echo(_JW_SETTINGS_APPEARANCE);?>">
                        <div class="wrapper">
                            <table width='90%'>

                                <tr><td colspan='3' align='left'><h3><?php echo(_JW_SETTINGS_LANGUAGE);?></h3></td></tr>
                                <?php echo JoomlaWatchHTML :: renderInputElement('LANGUAGE', $color); ?>

                                <tr><td colspan='3' align='left'><h3><?php echo(_JW_SETTINGS_APPEARANCE);?></h3></td></tr>
                                <?php echo JoomlaWatchHTML :: renderInputElement('ONLY_LAST_URI', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('HIDE_REPETITIVE_TITLE', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('TRUNCATE_VISITS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('TRUNCATE_STATS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('TRUNCATE_GOALS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('LIMIT_BOTS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('LIMIT_VISITORS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('TOOLTIP_WIDTH', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('TOOLTIP_HEIGHT', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('TOOLTIP_URL', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('TOOLTIP_ONCLICK', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('IP_STATS', $color); ?>
                            </table>
                        </div>
                    </div>
                    <div class="panel" title="<?php echo(_JW_STATS_COUNTRY);?>">
                        <div class="wrapper">
                            <table width='90%'>


                                <tr><td colspan='3' align='left'><h3><?php echo(_JW_SETTINGS_FRONTEND);?></h3></td></tr>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_HIDE_LOGO', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_NOFOLLOW', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_NO_BACKLINK', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_USER_LINK', $color); ?>

                                <tr><td colspan='3' align='left'><h3><?php echo(_JW_STATS_COUNTRY);?></h3></td></tr>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_COUNTRIES', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_COUNTRIES_FIRST', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_COUNTRIES_NAMES', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_COUNTRIES_UPPERCASE', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_COUNTRIES_FLAGS_FIRST', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_COUNTRIES_NUM', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_COUNTRIES_MAX_COLUMNS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_COUNTRIES_MAX_ROWS', $color); ?>

                            </table>
                        </div>
                    </div>

                    <div class="panel" title="<?php echo(_JW_FRONTEND_VISITORS);?>">
                        <div class="wrapper">
                            <table width='90%'>


                                <tr><td colspan='3' align='left'><h3><?php echo(_JW_SETTINGS_FRONTEND);?></h3></td></tr>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_VISITORS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_VISITORS_TODAY', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_VISITORS_YESTERDAY', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_VISITORS_THIS_WEEK', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_VISITORS_LAST_WEEK', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_VISITORS_THIS_MONTH', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_VISITORS_LAST_MONTH', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_VISITORS_TOTAL', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('FRONTEND_VISITORS_TOTAL_INITIAL', $color); ?>
                            </table>
                        </div>
                    </div>

                    <div class="panel" title="<?php echo(_JW_SETTINGS_HISTORY_PERFORMANCE);?>">
                        <div class="wrapper">
                            <table width='90%'>


                                <tr><td colspan='3' align='left'><h3><?php echo(_JW_SETTINGS_HISTORY_PERFORMANCE);?></h3></td></tr>
                                <?php echo JoomlaWatchHTML :: renderInputElement('HISTORY_MAX_VALUES', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('HISTORY_MAX_DB_RECORDS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('UPDATE_TIME_VISITS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('UPDATE_TIME_STATS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('STATS_MAX_ROWS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('STATS_IP_HITS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('MAXID_BOTS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('MAXID_VISITORS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('STATS_KEEP_DAYS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('CACHE_FRONTEND_COUNTRIES', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('CACHE_FRONTEND_VISITORS', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('CACHE_FRONTEND_USERS', $color); ?>
                            </table>
                        </div>
                    </div>

                    <div class="panel" title="<?php echo(_JW_SETTINGS_ADVANCED);?>">
                        <div class="wrapper">
                            <table width='90%'>

                                <tr><td colspan='3' align='left'><h3><?php echo(_JW_SETTINGS_IGNORE);?></h3></td></tr>
                                <?php echo JoomlaWatchHTML :: renderInputElement('UNINSTALL_KEEP_DATA', $color);?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('IGNORE_IP', $color, " <a href=\"javascript:addElementValueById('JOOMLAWATCH_IGNORE_IP','".$_SERVER['REMOTE_ADDR']."');\">"._JW_SETTINGS_ADD_YOUR_IP." ".$_SERVER['REMOTE_ADDR']." "._JW_SETTINGS_TO_THE_LIST."</a>");?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('IGNORE_URI', $color); ?>
                                <?php echo JoomlaWatchHTML :: renderInputElement('IGNORE_USER', $color); ?>

                                <tr><td colspan='3' align='left'><h3><?php echo(_JW_SETTINGS_BLOCKING);?></h3></td></tr>
                                <?php echo JoomlaWatchHTML :: renderInputElement('BLOCKING_MESSAGE', $color); ?>

                            </table>
                        </div>
                    </div>

                    <div class="panel" title="<?php echo(_JW_SETTINGS_EXPERT);?>">
                        <div class="wrapper">
                            <table width='90%'>
                                <tr><td colspan='3' align='left'><h3><?php echo(_JW_SETTINGS_EXPERT);?></h3></td></tr>
                                <?php echo JoomlaWatchHTML :: renderInputElement('SERVER_URI_KEY', $color); ?>

                                <tr><td colspan='3'>
                                    <br/><br/>
                                    <h3><?php echo(_JW_SETTINGS_RESET_ALL);?><?php echo JoomlaWatchHTML::renderOnlineHelp("reset"); ?></h3>
                                    <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=resetData' onClick="javascript:return confirm('<?php echo(_JW_SETTINGS_RESET_CONFIRM);?>')">[<?php echo(_JW_SETTINGS_RESET_ALL_LINK);?>]</a>
                                </td></tr>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <?php 		if (@$result) echo("<span style='color: green;'>"._JW_SETTINGS_SAVED."</span><br/><br/>"); ?>
        <input type='submit' name='submitForm' value=' [ <?php echo(_JW_SETTINGS_SAVE);?> ] '/>
    </form>

</center>
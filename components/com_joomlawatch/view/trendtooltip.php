<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<iframe width="<?php echo $joomlaWatch->config->getConfigValue('JOOMLAWATCH_TOOLTIP_WIDTH'); ?>" height="<?php echo $joomlaWatch->config->getConfigValue('JOOMLAWATCH_TOOLTIP_HEIGHT'); ?>" frameborder="0" marginwidth="0" marginheight="0" src="<?php echo ($joomlaWatch->helper->getIP2LocationURL(@JoomlaWatchHelper::requestGet('ip')));?>" />

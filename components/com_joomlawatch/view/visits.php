<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<span style='color: black;'>&nbsp;<?php echo ($joomlaWatch->helper->getActualDateTime()); ?></span>

<?php
if (!$joomlaWatch->helper->isModulePublished()) echo("<h4 style='color: red;'>"._JW_VISITS_MODULE_NOT_PUBLISHED."</h4>");
echo ("<br/>");
?>

<?php echo($joomlaWatchBlockHTML->renderBlockedInfo(true)); ?>

<table cellpadding='2' cellspacing='0' width='100%' border='0'>
    <tr><td colspan='8'><h3><?php echo _JW_VISITS_VISITORS; echo $joomlaWatchHTML->renderOnlineHelp("visits"); ?></h3></td></tr>
    <?php echo ($joomlaWatchVisitHTML->renderVisitors()); ?>
    <tr><td>&nbsp;</td></tr>
    <tr><td colspan='8'><h3><?php echo _JW_VISITS_BOTS; echo $joomlaWatchHTML->renderOnlineHelp("visits-bots"); ?></h3></td></tr>
    <?php echo ($joomlaWatchVisitHTML->renderBots()); ?>
</table>
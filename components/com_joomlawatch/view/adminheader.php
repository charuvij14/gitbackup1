<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<script type="text/javascript" src="<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/js/ajax-dynamic-content.js"></script>
<script type="text/javascript" src="<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/js/ajax.js"></script>
<script type="text/javascript" src="<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/js/joomlawatch.js.php?rand=<?php echo ($this->joomlaWatch->config->getRand());?>"></script>
<script type="text/javascript" src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/js/fade.js'></script>
<div align='left'>
    <table width='100%' <?php echo $this->joomlaWatch->helper->getTooltipOnEventHide();?> >
        <tr>
            <td valign='top'>

                <table>
                    <tr><td>
                        <a href='http://www.codegravity.com' target='_blank'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/joomlawatch-logo-32x32.gif' align='center' border='0'/></a>
                    </td><td>
                        <a href='http://www.codegravity.com' target='_blank' style='font-family: verdana; font-size: 14px; align:top; font-weight: bold; color: black;'>JoomlaWatch <?php echo($this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_VERSION'));?></a><br/><?php echo _JW_TITLE;?>
                    </td>
                        <td>
                            <a href='http://www.codegravity.com/projects/joomlawatch#doc' target='_blank'><?php echo _JW_MENU_DOCUMENTATION;?> <img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/external.gif' border='0'/></a> |
                            <a href='http://www.codegravity.com/projects/joomlawatch#faq' target='_blank'><?php echo _JW_MENU_FAQ;?> <img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/external.gif' border='0'/></a> |
                            <a href='http://www.codegravity.com/bug/' target='_blank' title='Report Bug'><?php echo _JW_MENU_BUG;?></a> <img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/external.gif' border='0'/></a> |
                            <a href='http://www.codegravity.com/feature/' target='_blank' title='Request Feature'><?php echo _JW_MENU_FEATURE;?></a> <img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/external.gif' border='0'/></a>
                        </td>

                    </tr>
                    <tr><td colspan='3'>
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch' ><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/map_icon.gif'/>&nbsp;<?php echo _JW_MENU_STATS;?></a> |
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=graphs'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/trend_icon.gif'/>&nbsp;Graphs</a> |
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=history'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/history.png'/>&nbsp;<?php echo _JW_MENU_HISTORY;?></a> |
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=antiSpam'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/antispam.gif'/>&nbsp;<?php echo _JW_SETTINGS_ANTI_SPAM;?></a> |
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=goals'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/goal.gif'/>&nbsp;<?php echo _JW_MENU_GOALS;?></a> |
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=emails'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/emails.png'/>&nbsp;<?php echo _JW_MENU_EMAILS;?></a> |
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=license'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/license.png'/>&nbsp;<?php echo _JW_MENU_LICENSE;?></a> |
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=status'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/status.png'/>&nbsp;<?php echo _JW_MENU_STATUS;?></a> |
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=settings'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/settings.gif'/>&nbsp;<?php echo _JW_MENU_SETTINGS;?></a> |
                        <a href='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=credits'><img src='<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/credits.png'/>&nbsp;<?php echo _JW_MENU_CREDITS;?></a>
                    </tr>
                    </tr>
                </table>
            </td>
            <td align='right' valign='top'><span style='color: gray;'><?php echo _JW_HEADER_CAST_YOUR;?> <a href='http://extensions.joomla.org/component/option,com_mtree/task,search/Itemid,35/searchword,joomlawatch/cat_id,0/' target='_blank'><?php echo _JW_HEADER_VOTE;?></a>. <?php echo _JW_HEADER_DOWNLOAD;?> <a href='http://www.codegravity.com/download/' target='_blank'>CodeGravity.com</a></span><br/><?php if (!$this->joomlaWatch->config->isAdFree()) { ?><iframe src="http://www.codegravity.com/space/?<?php echo time(); ?>" width="468" height="60" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe><?php }?></td></tr>
        <tr><td colspan='3'>
        <?php // echo($this->renderTrialVersionInfo()); ?>
        </td></tr>
    </table>
</div>
<script type="text/javascript">
	try {
	document.getElementById("toolbar-box").style.display = "none";
	} catch(e) {
	}
</script>
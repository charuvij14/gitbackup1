<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<h2><?php echo(_JW_EMAIL_REPORTS);?></h2>
<form action='<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=emails&action=save' method='POST' id='settingsForm'>
    <table width='100%'>
    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_REPORTS_ENABLED', $color); ?>
    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_REPORTS_ADDRESS', $color); ?>

    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_NAME_TRUNCATE', $color); ?>
        <tr><td><h2><?php echo(_JW_EMAIL_REPORTS_VALUE_FILTERS);?>:</h2></td></tr>
    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_PERCENT_HIGHER_THAN', $color); ?>
        <tr><td>&nbsp;</td></tr>

    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_ONE_DAY_CHANGE_POSITIVE', $color); ?>
    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_ONE_DAY_CHANGE_NEGATIVE', $color); ?>
        <tr><td>&nbsp;</td></tr>

    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_SEVEN_DAY_CHANGE_POSITIVE', $color); ?>
    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_SEVEN_DAY_CHANGE_NEGATIVE', $color); ?>
        <tr><td>&nbsp;</td></tr>

    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_THIRTY_DAY_CHANGE_POSITIVE', $color); ?>
    <?php echo JoomlaWatchHTML :: renderInputElement('EMAIL_THIRTY_DAY_CHANGE_NEGATIVE', $color); ?>

        <tr><td colspan='3'>

            <br/><br/>
        <?php if (@$result) echo("<span style='color: green;'>"._JW_SETTINGS_SAVED."</span><br/><br/>"); ?>
            <input type='submit' name='submitForm' value=' [ <?php echo(_JW_SETTINGS_SAVE);?> ] '/>

        </td>
        </tr>

    </table>

</form>
<br/>
<hr/>

<h2><?php echo(_JW_EMAIL_REPORT_GENERATED);?></h2>

<?php echo nl2br($joomlaWatchStatHTML->renderNightlyEmail()); ?>




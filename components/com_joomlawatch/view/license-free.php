<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<table width='100%'><tr><td  valign='top'>
    <h2>Please purchase the JoomlaWatch license</h2>
    Enter your registration code for domain: <b/><?php echo $this->joomlaWatch->config->getDomainFromLiveSite(); ?></b><br/><br/>
    <form action='<?php echo($this->joomlaWatch->config->getLiveSite());?>administrator/index.php?option=com_joomlawatch'>
        <input type='text' name='key' size='50'/> <input type='submit' value='activate'/>
        <input type='hidden' name='option' value='com_joomlawatch'/>
        <input type='hidden' name='task' value='activate'/>
    </form><br/>
</td>
</tr>
    <tr><td colspan='1'>
    <table>
        <tr>
            <td valign="top">
                <img src="http://www.codegravity.com/img/joomlawatch-logo.png" align="left">
            </td>
            <td valign="top">
                <ul style="padding: 10px;">
                    <li>This license applies to <strong>ONE</strong> website / domain of your choice.<br> </li>
                    <li>Please provide your <strong>domain name</strong> in the payment details</li>
                    <li>This license <strong>DOESN'T include any installation, setup or troubleshooting support</strong></li>
                    <li>There is a <strong>30 day money-back guarantee</strong>.</li>
                    <li>License applies for <b>all versions in future too</b></li>
                    <li>To purchase the license, please proceed with the paypal button below. <br>
                        After donation click "return to merchant" button). Your should be returned to the form at codegravity.com where you can fill your donation details necessary to recognize your donation and make the whole process nearly immediate.</li>
                </ul>
            </td>
        </tr>
    </table>
    </td>
        <td valign='top'>
            <h2>Why to donate? What people say?</h2>
            <ul>
                <li>
                    See the <a href='http://www.codegravity.com/donate/' target='_blank'>list of supporters</a> JoomlaWatch by country <i>(560 donations by June 2010)</i><br/><br/>
                </li>
                <li>
                    Read the extension <a href='http://extensions.joomla.org/extensions/site-management/visitors/3940' target='_blank'>reviews</a> <i>(84 reviews by June 2010)</i><br/><br/>
                </li>
            </ul>
        </td>
    
    </tr>
    <tr><td valign="top">
        <h2>Single domain license: 9 EUR</h2>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input name="cmd" value="_s-xclick" type="hidden">
            <input name="hosted_button_id" value="9456276" type="hidden">
            <input src="https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif" name="submit" alt="PayPal - The safer, easier way to pay online!" type="image" border="0">
            <img alt="" src="https://www.paypal.com/en_US/i/scr/pixel.gif" border="0" height="1" width="1">
        </form>
        <br>
        - you'll support further development with new features available for you in future<br>
        - adds posibility to remove ad panel from back end<br>
        - adds posibility to remove frontend logo<br>
        - adds posibility to remove backlink<br>
        - your donation details on donation page (optional)<br>
    </td>
        <td valign="top">
            <h2>Single domain PRO license: 15 EUR</h2>
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input name="cmd" value="_s-xclick" type="hidden">
                <input name="hosted_button_id" value="CMKXMQ8EFZ3L4" type="hidden">
                <input src="https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif" name="submit" alt="PayPal - The safer, easier way to pay online!" type="image" border="0">
                <img alt="" src="https://www.paypal.com/en_US/i/scr/pixel.gif" border="0" height="1" width="1">
            </form>
            <br>
<span style="color: red;">- prioritized when answering support questions<br>
- a <b>link to your website</b> from the donations page</span> (rel=nofollow) <b>(Your website has to use JoomlaWatch)</b>
            <br>
            - you'll support further development with new features available for you in future<br>
            - adds posibility to remove ad panel from back end<br>
            - adds posibility to remove frontend logo<br>
            - adds posibility to remove backlink<br>
            - your donation details on donation page (optional)<br>

        </td>
    </tr>
    <tr>
        <td valign="top" width="50%">
            <h2>Donate higher amount than 15 EUR</h2>
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                <input name="cmd" value="_s-xclick" type="hidden">
                <input name="hosted_button_id" value="6680661" type="hidden">
                <input src="https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif" name="submit" alt="PayPal - The safer, easier way to pay online!" type="image" border="0">
                <img alt="" src="https://www.paypal.com/en_US/i/scr/pixel.gif" border="0" height="1" width="1">
            </form>
            <br>
<span style="color: red;">
- some of users found this software even more valuable, than fixed prices above <br>
- a <b>link to your website</b> from the donations page (rel=nofollow)  <b>(Your website has to use JoomlaWatch)</b><br>
- prioritized when answering support questions</span><br>
            - you'll support further development with new features available for you in future<br>
            - adds posibility to remove ad panel from back end<br>
            - adds posibility to remove frontend logo<br>
            - adds posibility to remove backlink<br>
            - your donation details on donation page (optional)<br>
            - <b>Thank you!</b>
        </td>
        <td style="margin: 5px;" valign="top">
            <h1></h1><br>
            <i>Please don't abuse this and purchase it to get the link to your page with inappropriate content. <br>
                In such a case, we reserve a right not to display your website on a donation page and you'll get a refund.</i>
            <br><br>
            <i>UPDATE 04.12.2009: In order to follow the google webmaster guidelines - all the links have to have a rel=nofollow attribute. The intention of this page is to promote the websites whose owners supported the development of JoomlaWatch and not to pass the pagerank. An email was sent to all of PRO license owners, and everyone could apply for a refund for the donation.
            </i>
        </td>
    </tr>
    </tbody></table>




<script type="text/javascript">
    showEffect("license", "scale", 1500);
</script>

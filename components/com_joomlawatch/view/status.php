<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<br/><br/>
<h3><?php echo(_JW_STATUS_DATABASE);?></h3>

<table id='status' style='width:100%' cellpadding='5px;'>
    <tr><td>
    </td></tr>
    <tr><th align='left'><?php echo(_JW_STATUS_DATABASE_TABLE_NAME);?></th><th align='right'><?php echo(_JW_STATUS_DATABASE_ROWS);?></th><th align='right'><?php echo(_JW_STATUS_DATABASE_DATA);?></th></tr>
<?php $rows = $this->joomlaWatch->stat->getDatabaseStatus();
$total = 0;
    $j = 0;
foreach ($rows as $row) {
    $total += $row->Data_length + $row->Index_length;
    if ($j%2 == 0) {
        $color = "#f9f9f9";
    } else {
        $color = "#eeeeee";
    }
    ?>
        <tr>
            <td style='background-color: <?php echo $color;?>'><?php echo($row->Name);?></td>
            <td align='right' style='background-color: <?php echo $color;?>'><?php echo($row->Rows);?></td>
            <td align='right' style='background-color: <?php echo $color;?>'><?php echo( sprintf("%.3f",(($row->Data_length+$row->Index_length)/1024/1024)));?> MB
            </td></tr>
    <?php
$j++;
                                    }
?>
    <tr>
        <td><b><?php echo(_JW_STATUS_DATABASE_TOTAL);?>:</b></td><td></td><td align='right'><b><?php echo( sprintf("%.3f",($total/1024/1024)));?> MB</b>
    </td></tr>
</table>
<br/>
<br/>

<script type="text/javascript">
    showEffect("status", "drop", 1500);
</script>


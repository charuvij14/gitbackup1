<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<form action="<?php echo $this->joomlaWatch->config->getAdministratorPath();?>index.php?option=com_joomlawatch&task=antiSpamSave' method='POST' id='settingsForm'" method="POST">

    <div class="panel">
        <div class="wrapper">
            <table border='0' style='border: 1px solid #dddddd; padding: 2px;> 
                <tr><td colspan='4' align='left'><h2><?php echo(_JW_SETTINGS_ANTI_SPAM);?></h2></td></tr>
            <tr>
                <td valign='top' width='50%'>
                    <h3><?php echo(_JW_ANTISPAM_ADDRESSES);?>:</h3>
                <?php echo $joomlaWatchBlockHTML->renderBlockedInfo(); ?><br/><br/>

                    <table>
                    <?php echo($joomlaWatchBlockHTML->renderBlockedIPs(0, true)); ?>
                    </table>
                </td>
                <td valign='top' width='50%'>
                    <table border='0'>
                        <tr><td colspan='2'><h3><?php echo(_JW_ANTISPAM_SETTINGS);?></h3></td></tr>
                    <?php echo JoomlaWatchHTML :: renderInputElement('SPAMWORD_BANS_ENABLED', $color); ?>

                    <?php echo JoomlaWatchHTML :: renderInputElement('SPAMWORD_LIST', $color); ?>

                        <tr><td></td><td><center>
                            <br/><br/>
                            <input type='submit' value='<?php echo(_JW_SETTINGS_SAVE);?>'/>
                            <input type='hidden' name='option' value='com_joomlawatch'/>
                            <input type='hidden' name='task' value='antiSpam'/>
                            <input type='hidden' name='action' value='save'/>
                        </center>
                        </td></tr>

                    </table>
                </td>
            </tr>
            </table>
        </div>
    </div>
    <br/><br/>

</form>


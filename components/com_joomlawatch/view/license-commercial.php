<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<table id='license'>
    <tr>

        <td valign='top'>
    <img src='<?php echo $this->joomlaWatch->config->getLiveSite();?>components/com_joomlawatch/icons/joomlawatch-logo.png' />            
        </td>
        <td align='left' valign='top'>
        <h3>Ad-free version</h3>

        <h4>Thank you very much for your purchase!</h4>

        Registration key for your domain <b/><?php echo $this->joomlaWatch->config->getDomainFromLiveSite(); ?></b> is: <br/><br/>
        <input name='key' value='<?php echo($this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_ADFREE'));?>' size="50" readonly="true" style='text-align: center'/>
            <br/><br/>
            Now you can remove backlink or hide JoomlaWatch logo in frontend from Settings 
    </td></tr></table>

<script type="text/javascript">
    showEffect("license", "scale", 1500);
</script>

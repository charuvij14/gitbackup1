<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<table border="0">
<tr><td colspan='5'><h3><?php echo(_JW_STATS_ALL_TIME_TITLE); echo $joomlaWatchHTML->renderOnlineHelp("all-time-stats"); ?></h3></td></tr>
    <?php
    foreach ($keysArray as $key) {
        if ($key == 'ip' && !$joomlaWatch->config->getConfigValue('JOOMLAWATCH_IP_STATS')) {
            continue;
        }
        ?>
        <tr><td colspan='4'><u><?php echo(_JW_STATS_ALL_TIME)."&nbsp;"; echo (@constant('_JW_STATS_'.strtoupper($key))); ?></u></td></tr>
        <tr><td  valign='top'><?php echo $joomlaWatchStatHTML->renderIntValuesByName($key, @JoomlaWatchHelper::requestGet($key."_total"), true); ?></td></tr>
        <tr><td colspan='4'>&nbsp;</td></tr>
        <?php
    }
    ?>
</table>
        

<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<center>
    <script>
        function enableContinue() {
            document.getElementById('continue').disabled = !document.getElementById('accept').checked;
        }
    </script>
    <h2>JoomlaWatch <?php echo($this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_VERSION"));?> license</h2>
    <form  name='acceptForm' action='index.php?option=com_joomlawatch&task=licenseAccepted' method='POST'>
        <div style='width:600px;height:400px;overflow:auto;text-align:left;'>
            <?php echo($this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_LICENSE"));?></div>
        <br/>

        <input type='checkbox' id='accept' value='<?php echo(_JW_LICENSE_AGREE);?>' onClick='javascript:enableContinue();'/><?php echo(_JW_LICENSE_AGREE);?>&nbsp; &nbsp;
        <input type='submit' name='continue' id='continue' value='<?php echo(_JW_LICENSE_CONTINUE);?>' disabled/>

    </form>
</center>


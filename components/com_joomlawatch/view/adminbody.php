<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<script type="text/javascript">sendLastIdReq();</script>

<center>
    <table border='0' cellpadding='2' width='100%' <?php echo $this->joomlaWatch->helper->getTooltipOnEventHide(); ?> >
        <tr>
            <td id="visits" valign='top' align='left' width='80%'>
            <?php echo _JW_VISITS_PANE_LOADING; ?>
                <br/><br/>
                <div id='loading' style='width: 200px; border: 1px solid black; background-color: yellow; padding:5px; display:none;'>
                    If you are seeing the message above for too long, your live site may be wrong.
                    Open the components/com_joomlawatch/config.php
                    uncomment, and set your actual live site. Eg.:
                    define('JOOMLAWATCH_LIVE_SITE', 'http://www.codegravity.com');
                </div>
            </td>
            <td id="stats" valign='top'  align='left'>
            <?php echo _JW_STATS_PANE_LOADING; ?>
            </td>
        </tr>
    </table>
</center>
<script type='text/javascript'>
/*
    try {
    setTimeout("makeLoadingDisappear()", 5000);
    } catch (e) {
        // suppress
    }
*/
</script>



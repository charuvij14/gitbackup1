<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>


<link rel="stylesheet" href="<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/css/ajax-tooltip.css" type="text/css" />
<style type="text/css">
    H2 {font-family: verdana, helvetica, arial; font-size: 14px; }
    TR, TD { font-family: verdana, helvetica, arial; font-size:10px;}
    .tab_active {
        background-position: top center;
        background-image: url(<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/tab-on.gif);
        background-repeat: no-repeat;
        width:100px;
    }
    .tab_inactive {
        background-position: top center;
        background-image: url(<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/tab-off.gif);
        background-repeat: no-repeat;
        width:100px;
    }
    .tab_none {
        background-position: bottom center;
        background-image: url(<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/tab-none.gif);
        background-repeat: repeat-x;
    }
    #ajax_tooltipObj .ajax_tooltip_content{
        border:2px solid #317082;	/* Border width */
        left:100px;	/* Same as border thickness */
        top:0px;
        position:fixed;
        width:<?php echo($this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_TOOLTIP_WIDTH'));?>px;	/* Width of tooltip content */
        height:<?php echo($this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_TOOLTIP_HEIGHT'));?>px;	/* Height of tooltip content */
        background-color:#FFF;	/* Background color */
        padding:5px;	/* Space between border and content */
        font-size:0.8em;	/* Font size of content */
        overflow:auto;	/* Hide overflow content */
        z-index:1000001;
    }

    .internalDetailDiv {
        position:absolute;
        top: -100;
        left: -100;
        width:500;
        background-color: #eeeeee;
        border: 1px solid black;
        display: none;
    }

    .uriDetailDiv {
        position:absolute;
        top: -100;
        left: -100;
        width:500;
        background-color: #eeeeee;
        border: 1px solid black;
        display: none;
    }
    .credits {
        width:1000px;
        background-color: #DDDDDD;
        background-position: top center;
        background-image: url(<?php echo($this->joomlaWatch->config->getLiveSite());?>components/com_joomlawatch/icons/credits-background.png);
        background-repeat: repeat;
        border: 1px solid #ddd; /* this is the border. should have the same value for the links */
        
    }
</style>
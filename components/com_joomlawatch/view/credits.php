<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<table id="credits">
    <tr><td style='padding: 5px;' align='left' class='credits'>
        <h2><?php echo(_JW_CREDITS_TITLE);?></h2>
    <?php echo($this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_CREDITS'));?>
    </td></tr>
</table>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>



<script type="text/javascript">
    showEffect("credits", "fold", 1500);
</script>


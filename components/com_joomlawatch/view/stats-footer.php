<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<br/><br/>
<span style='color: #cccccc;'>JoomlaWatch &copy;2006-2009 by Matej Koval</span>

<!-- rendered in <?php echo((time()+microtime())-$t1); ?>s -->

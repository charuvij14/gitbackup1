<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>


<h3><?php echo(_JW_STATS_DAILY_TITLE)."&nbsp;"; echo $joomlaWatch->helper->getDateByDay($day);?><?php echo $joomlaWatchHTML->renderOnlineHelp("daily-stats"); ?></h3>
<?php echo $joomlaWatchHTML->renderDateControl(); ?>
<table>
    <?php
    foreach ($keysArray as $key) {
        if ($key == 'ip' && !$joomlaWatch->config->getConfigValue('JOOMLAWATCH_IP_STATS')) {
            continue;
        }
        ?>
        <tr><td colspan='4'><u><?php echo (@constant('_JW_STATS_'.strtoupper($key)))."&nbsp;"; echo(_JW_STATS_FOR)."&nbsp;"; echo $joomlaWatch->helper->getDateByDay($day);?></u></td></tr>
        <tr><td valign='top'><?php echo $joomlaWatchStatHTML->renderIntValuesByName($key, @JoomlaWatchHelper::requestGet($key), false, $day ); ?></td></tr>
        <tr><td colspan='4'>&nbsp;</td></tr>
        <?php
    }
    ?>
</table>

<table>
    <?php echo($joomlaWatchBlockHTML->renderBlockedIPs($day, @JoomlaWatchHelper::requestGet('ip_blocking_title'))); ?>
</table>
<br/><hr size='1' width='100%'/>
<?php echo $joomlaWatchHTML->renderDateControl(); ?>


<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<table cellspacing='0'>
    <tr><td colspan='5'>
        <h3><?php echo _JW_STATS_TITLE."&nbsp;"; echo($joomlaWatch->helper->getWeekFromTimestamp($week*3600*24*7)); ?>/<?php echo($joomlaWatch->helper->getYearFromTimestamp($week*3600*24*7)); ?>
            <?php echo $joomlaWatchHTML->renderOnlineHelp("visit-stats"); ?></h3>
    </td></tr>
    <tr><td colspan='5'>
        <table border='0'>
            <tr><td align='left' width='10%'><a href='javascript:setWeek(<?php echo($prevWeek);?>)' id='visits_<?php echo($prevWeek);?>'>&lt;
                <?php echo _JW_STATS_WEEK;?>&nbsp;<?php echo $joomlaWatch->helper->getWeekFromTimestamp($prevWeek*3600*24*7);?>
            </a></td>
                <td align='left'><img src='<?php echo $joomlaWatch->config->getLiveSite(); ?>components/com_joomlawatch/icons/calendar.gif' border='0' align='center' /></td>
                <td align='center' width='20%'>
                    <?php if (@$week != $thisWeek) {
                    ?>
                    <a href='javascript:setWeek(<?php echo($thisWeek);?>)' id='visits_$thisWeek'><?php echo _JW_STATS_THIS_WEEK;?></a>
                    <?php
                }
                    ?>
                </td>
                <td align='right' width='10%'><?php if ($nextWeek <= $thisWeek) echo("<img src='".$joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/calendar.gif' border='0' align='center' /></td><td width='20%' align='right'><a href='javascript:setWeek($nextWeek)' id='visits_$nextWeek'>"._JW_STATS_WEEK."&nbsp;".$joomlaWatch->helper->getWeekFromTimestamp($nextWeek*3600*24*7)."&gt;</a>"); ?></td>
            </tr>
        </table>
        <?php echo $joomlaWatchStatHTML->renderVisitsGraph($week); ?>
    <tr><td colspan='4'>

        <table width='100%'>
            <tr>
                <td align='center' class='<?php echo $joomlaWatchStatHTML->renderTabClass("0", @JoomlaWatchHelper::requestGet('tab'));?>'>
                    <?php echo $joomlaWatchStatHTML->renderSwitched("0", _JW_STATS_DAILY, @JoomlaWatchHelper::requestGet('tab')); ?>
                </td>
                <td align='center' class='<?php echo $joomlaWatchStatHTML->renderTabClass("1", @JoomlaWatchHelper::requestGet('tab'));?>'>
                    <?php echo $joomlaWatchStatHTML->renderSwitched("1", _JW_STATS_ALL_TIME, @JoomlaWatchHelper::requestGet('tab')); ?>
                </td>
                <td align='center' class='tab_none'>
                </td>
            </tr>
        </table>

</table>
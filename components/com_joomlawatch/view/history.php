<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<!--
$today = floor($joomlaWatch->helper->getServerTime()/ 24 / 3600);
$thisWeek = floor($joomlaWatch->helper->getServerTime()/ 24 / 3600 / 7);
if (@ JoomlaWatchHelper::requestGet('day'])
$day = @ JoomlaWatchHelper::requestGet('day'];
else
$day = floor($joomlaWatch->helper->getServerTime()/ 24 / 3600);

if (@ JoomlaWatchHelper::requestGet('week'])
$week = @ JoomlaWatchHelper::requestGet('week'];
else
$week = floor($joomlaWatch->helper->getServerTime()/ 24 / 3600 / 7);

$prev = $day -1;
$next = $day +1;
$prevWeek = $week -1;
$nextWeek = $week +1;
-->

<?php echo($joomlaWatchVisitHistoryHTML->renderHistoryNavigation()); ?>

<table cellpadding='2' cellspacing='0' width='100%' border='0'>
    <tr>
        <td colspan='8'><h3><?php echo _JW_HISTORY_VISITORS;?></h3>
            <div style='border: 1px solid #FFB7B7; width: 100%; background-color: #FFF5F5; padding: 2px;'>
                <?php echo sprintf(_JW_HISTORY_SHOWING_ONLY, $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_HISTORY_MAX_DB_RECORDS'));?>
            </div>
            <br/>
        </td>
    </tr>
    <tr>
        <td>
        <?php echo ($joomlaWatchVisitHistoryHTML->renderVisitors()); ?>
        </td>
    </tr>
</table>
<br/>
<?php echo($joomlaWatchVisitHistoryHTML->renderHistoryNavigation()); ?>

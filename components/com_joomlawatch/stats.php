<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2008 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

include_once("includes.php");

$joomlaWatch = new JoomlaWatch();
require_once ("lang" . DS . $joomlaWatch->config->getLanguage().".php");

$joomlaWatchHTML = new JoomlaWatchHTML();
$joomlaWatchStatHTML = new JoomlaWatchStatHTML($joomlaWatch);
$joomlaWatchBlockHTML = new JoomlaWatchBlockHTML($joomlaWatch);

$joomlaWatch->config->checkPermissions();

$t1 = ($joomlaWatch->helper->getServerTime()+ microtime());

$last = $joomlaWatch->visit->getLastVisitId();

//$params = new MosParameters("");
$thisWeek = $joomlaWatch->helper->getWeekByTimestamp($joomlaWatch->helper->getServerTime());
if (@ JoomlaWatchHelper::requestGet('day')) {
    $day = @ JoomlaWatchHelper::requestGet('day');
} else {
    $day = $joomlaWatch->helper->jwDateToday();
}

if (@ JoomlaWatchHelper::requestGet('week')) {
    $week = @ JoomlaWatchHelper::requestGet('week');
}
else {
    $week = $joomlaWatch->helper->getWeekByTimestamp($joomlaWatch->helper->getServerTime());
}

$prevWeek = $week -1;
$nextWeek = $week +1;

?>
<table border='0' cellpadding='1' cellspacing='0' width='100%'>
    <tr><td>
        <?php
        include("view".DS."stats-header.php");

        if (@JoomlaWatchHelper::requestGet('tab') == "1") {
            include("view".DS."stats-total.php");
        } else {
            include("view".DS."stats-today.php");

        }
        include("view".DS."stats-footer.php");


        ?>
    </td></tr>
</table>

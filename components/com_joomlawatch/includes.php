<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);
$jBasePath = dirname(__FILE__).DS."..".DS."..";
define('JPATH_BASE', $jBasePath);

if (!defined('JPATH_ROOT'))
    require_once (JPATH_BASE . DS . 'includes' . DS . 'defines.php');
if (!defined('JDEBUG'))
    require_once (JPATH_BASE . DS . 'includes' . DS . 'framework.php');

if (version_compare(JVERSION, '1.5.0', '<')) {
    require_once (JPATH_BASE . DS . 'libraries' . DS . 'joomla' . DS . 'application' . DS . 'module' . DS . 'helper.php');
    $mainframe = & JFactory :: getApplication('site');
    $mainframe->initialise();
}

require_once ("config.php");
require_once ("class.joomlawatch.php");
require_once ("class.joomlawatch.block.php");
require_once ("class.joomlawatch.block.html.php");
require_once ("class.joomlawatch.cache.php");
require_once ("class.joomlawatch.config.php");
require_once ("class.joomlawatch.goal.php");
require_once ("class.joomlawatch.goal.html.php");
require_once ("class.joomlawatch.helper.php");
require_once ("class.joomlawatch.stat.php");
require_once ("class.joomlawatch.stat.html.php");
require_once ("class.joomlawatch.visit.php");
require_once ("class.joomlawatch.visit.html.php");
require_once ("class.joomlawatch.visit.history.php");
require_once ("class.joomlawatch.visit.history.html.php");
require_once ("class.joomlawatch.html.php");
require_once ("class.joomlawatch.trend.html.php");
require_once ("class.joomlawatch.db.php");
require_once ("class.joomlawatch.log.php");
require_once ("class.ip2country.php");

?>

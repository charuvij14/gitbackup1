<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if( !defined( '_JEXEC' ) && !defined( '_VALID_MOS' ) ) die( 'Restricted access' );

class JoomlaWatchLog {


    function writeEntry($severity, $file, $line, $sql) {
        if (JOOMLAWATCH_DEBUG) {
            $backTrace = JoomlaWatchLog::getDebugBacktrace();
            $message = JoomlaWatchHelper::date("d.m.Y H:i:s ", JoomlaWatchHelper::getServerTime());
            $message .= "JoomlaWatch Query: \n".$sql."\n";
            $message .= "JoomlaWatch BackTrace: ". $backTrace;
            $logPath = JPATH_BASE.DS."components".DS."com_joomlawatch".DS."log".DS."joomlawatch.log.php";
            $result = error_log($message, 3, $logPath);
            if (!$result) {
                echo("Error writing log to: ".$logPath);
            }
            chmod($logPath, 0700);
        }
    }    


    function error($file, $line, $message) {

        // echo("<span style='color: red'>JoomlaWatch: SQL error has occured, please check your system logs</span>".$message);
        JoomlaWatchLog::writeEntry("ERROR", $file, $line, $message);

    }

    function info($file, $line, $message) {

        JoomlaWatchLog::writeEntry("INFO", $file, $line, $message);

    }

    function warn($file, $line, $message) {
        JoomlaWatchLog::writeEntry("WARN", $file, $line, $message);

    }


    function debug($file, $line, $message) {
        JoomlaWatchLog::writeEntry("DEBUG", $file, $line, $message);

    }

    /**
     * Debug backgrace function used from http://php.net/manual/en/function.debug-backtrace.php
     *
     * @param unknown_type $NL
     * @return unknown
     */
    function getDebugBacktrace($NL = "\n") {
        $dbgTrace = debug_backtrace();
        if (!isset($dbgMsg)) {
            $dbgMsg = "";
        }

        $dbgMsg .= $NL;
        if (isset($dbgTrace)) {
            foreach($dbgTrace as $dbgIndex => $dbgInfo) {
                $dbgMsg .= "\t at $dbgIndex  ".$dbgInfo['file']." (line {$dbgInfo['line']}) -> {$dbgInfo['function']}(".
                        join(",",$dbgInfo['args']).")$NL";
            }
        }
        $dbgMsg .= $NL;
        return $dbgMsg;
    }


}



?>
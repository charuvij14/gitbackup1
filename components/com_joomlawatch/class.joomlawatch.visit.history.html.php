<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchVisitHistoryHTML extends JoomlaWatchVisitHTML {

    var $joomlaWatch;

    function JoomlaWatchVisitHistoryHTML($joomlaWatch) {
        parent::JoomlaWatchVisitHTML($joomlaWatch);
        $this->joomlaWatch = $joomlaWatch;
        $this->visitHistory = new JoomlaWatchVisitHistory($joomlaWatch);
    }

    /* visits */
    function renderVisitors() {
        $rows = $this->visitHistory->getVisitors();
        $this->lastDate = "";
        $output = $this->renderTable($rows);

        return $output;
    }

    function getJoinedURIRows($ip) {
        return $this->visitHistory->getJoinedURIRows($ip);
    }

    function renderPageLink($page, $name, $actualPage, $disabled = false) {
        $output = "";
        if (!isset($page) || $disabled == true || (isset($actualPage) && $actualPage == $page)) {
            $output .= " ".$name." ";
        } else {
            $output .= "<a href='index.php?option=com_joomlawatch&task=history&page=".$page."'/>".$name."</a> ";
        }
        return $output;
    }

    function renderHistoryNavigation() {
        $pageFromRequest = (int) @JoomlaWatchHelper::requestGet('page');
        $increment = $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_HISTORY_MAX_VALUES');
        $count = $this->visitHistory->getHistoryCount();
        $maxPageCount = floor($count / $increment);
        $maxPages = 20;
        
        if (!$pageFromRequest) {
            $pageFromRequest = 0;
        }
        $output = "";
        $output .= "<table width='100%'><tr><td>";

        if (isset($pageFromRequest) && $pageFromRequest > 0) {
            $pagePrev = $pageFromRequest -1;
        }
        $output .= $this->renderPageLink(@$pagePrev,"&lt;&lt;"._JW_HISTORY_PREVIOUS,@$pageFromRequest);

        $output .= $this->renderPageLink(0,0, $pageFromRequest)." ... ";


        $j = 0;
        for ($i=$pageFromRequest-floor($maxPages/2); $i<$maxPageCount; $i++) {
            if ($i>0 && $i<$maxPageCount) {
                $page = $i;
                $output .= $this->renderPageLink($page,$page, $pageFromRequest);
                if ($j > $maxPages) {
                    break;
                }
                $j++;
            }
        }
        $output .= "... ".$this->renderPageLink($maxPageCount, $maxPageCount, $pageFromRequest);

        if (isset($pageFromRequest) && $pageFromRequest < $maxPageCount) {
            $pageNext = $pageFromRequest +1;
        }
        $output .= $this->renderPageLink(@$pageNext,_JW_HISTORY_NEXT."&gt;&gt;", $pageFromRequest);

        $output .= "</td></tr></table>";

        echo($output);
    }


}

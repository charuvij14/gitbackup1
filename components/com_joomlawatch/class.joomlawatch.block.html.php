<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchBlockHTML {

    var $joomlaWatch;
    var $joomlaWatchStatHTML;

    function JoomlaWatchBlockHTML($joomlaWatch) {
        $this->joomlaWatch = $joomlaWatch;
        $this->joomlaWatchStatHTML = new JoomlaWatchStatHTML($joomlaWatch);
    }

    function renderBlockedInfo($renderUpdatesTraffic = false) {

        $countToday = $this->joomlaWatch->block->getBlockedCountByDate($this->joomlaWatch->helper->jwDateToday());
        $output = "";
        if (true) {
            $output = sprintf("<br/><div style='border: 1px solid #ffff00; background-color: #ffffdd; width: 70%%;'>"._JW_ANTISPAM_BLOCKED, ((int) $countToday), ((int) $this->joomlaWatch->block->getBlockedCountTotal()));
            if ($renderUpdatesTraffic) {
                $output .= ". ";
                $output .= _JW_TRAFFIC_AJAX.sprintf(": %.3f",JoomlaWatchHelper::requestGet('traffic')/1024/1024)." MB";
            }
            $output .= "</div>";
        }

        return $output;
    }

    /* visit */
    function renderBlockedIPs($date = 0, $expanded = false) {

        $output = "";
        if (!$expanded) {
            $output .= "<a href='javascript:blockIpManually();'>".(_JW_STATS_IP_BLOCKING_ENTER)."</a><br/>";
        }


        $total = false;
        $totalString = "";
        if (!@$date ) {
            $total = true;
        }

        $count = $this->joomlaWatch->block->countBlockedIPs($date);

        if (!$count) {
            return false;
        }

        $limit = $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_STATS_MAX_ROWS');

        $output .= "<tr><th></th><th></th><th>IP</th><th>".strtolower(_JW_STATS_HITS)."</th></tr>";

        if ($count > $limit && !$expanded)  {
            $output = "<tr><td colspan='4'>".$this->joomlaWatchStatHTML->renderExpand('ip_blocking_title', $total)."</td></tr>";
        }
        if (@ $expanded) {
            $rows = $this->joomlaWatch->block->getBlockedIPs($date);
        } else {
            $rows = $this->joomlaWatch->block->getBlockedIPs($date, $limit);
        }

        if (@ $rows)
            foreach ($rows as $row) {
                $icon = "";
                $country = "";
                if (!strstr($row->ip, "*")) {
                    $country = $this->joomlaWatch->helper->countryByIp($row->ip);
                    $countryName = $this->joomlaWatch->helper->countryCodeToCountryName($country);
                    if (!$country) {
                        $country = "none";
                    }
                    $icon = "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/flags/" . @ strtolower($country) . ".png' title='$countryName' alt='$countryName'/>";
                }

                $mapsIcon = "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/map_icon.gif' border='0'  " . $this->joomlaWatch->helper->getTooltipOnEvent() . "=\"ajax_showTooltip('" . $this->joomlaWatch->config->getLiveSite() . "components/com_joomlawatch/tooltip.php?rand=" . $this->joomlaWatch->config->getRand() . "&ip=" . @ $row->ip . "',this);return false\"/>";

                $output .= "<tr><td align='center'>".$mapsIcon."</td><td align='center' title='$row->reason'>" . $icon . "</td><td align='left' title='$row->reason'>" . $row->ip . "</td><td align='center' title='$row->reason'>" . $row->hits . "</td><td>";

                if (!$expanded) {
                    $output .= "<a  id='$row->ip' href='javascript:blockIpToggle(\"$row->ip\");' title='$row->reason'>"._JW_BLOCKING_UNBLOCK."</a>";
                } else {
                    $output .= "<a  id='$row->ip' href='index.php?option=com_joomlawatch&task=antiSpam&action=toggleBlocking&ip=".$row->ip."' title='$row->reason'>"._JW_BLOCKING_UNBLOCK."</a>";
                }


                "</td></tr>";

            }
        return $output;

    }


}
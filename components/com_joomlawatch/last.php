<?php
/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.4.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2008 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

include_once("includes.php");

$joomlaWatch = new JoomlaWatch();
$joomlaWatch->config->checkPermissions();
echo $joomlaWatch->visit->getLastVisitId();

?>
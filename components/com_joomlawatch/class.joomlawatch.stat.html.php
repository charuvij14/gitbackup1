<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchStatHTML {

    var $joomlaWatch;
    var $joomlaWatchTrendHTML;

    function JoomlaWatchStatHTML($joomlaWatch) {
        $this->joomlaWatch = $joomlaWatch;
        $this->joomlaWatchTrendHTML = new JoomlaWatchTrendHTML($joomlaWatch);
    }

    function changeReferers($j, $row) {
        $groupTruncated = $this->joomlaWatch->helper->truncate($row->name, $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_TRUNCATE_STATS'));
        $row->name = "<a href='http://$row->name' title='$row->name' target='_blank'>$groupTruncated</a>";
    }

    function changeURI($j, $row) {
        $groupTruncated = $this->joomlaWatch->helper->truncate($row->name, $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_TRUNCATE_STATS'));
        $title = $this->joomlaWatch->visit->getTitleByUri($row->name);
        $uriEncoded = urlencode($row->name);
        $row->value = "<table><tr><td>".$row->value."</td><td><a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals&action=insert&uri=$uriEncoded' title='"._JW_STATS_ADD_TO_GOALS."'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/goal.gif'/ border='0'></a></td></tr></table>";

        //TODO: $row->name should respect the directory it's in

        $row->name = "<a href='$row->name' onmouseover=\"toggleDiv('uriDetailDiv$j',1);\" onmouseout=\"toggleDiv('uriDetailDiv$j',0);\">$groupTruncated</a><div id='uriDetailDiv$j' class='uriDetailDiv'><table><tr><td><b>$title</b></td></tr><tr><td><a href='".$this->joomlaWatch->config->getLiveSite()."$row->name' title='$row->name'>$row->name</a></td></tr></table></div>";
    }

    function changeBrowserOS($j, $row) {
        if ($row->name) {
            $icon = "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/" . strtolower($row->name) . ".gif' />";
        }
        return @$icon;
    }

    function changeCountry($j, $row, $frontend) {
        if ($row->name) {
            $countryName = $this->joomlaWatch->helper->countryCodeToCountryName($row->name);
            if ($frontend && $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_FRONTEND_COUNTRIES_UPPERCASE')) {
                $countryName = strtoupper($countryName);
            }
            $icon = "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/flags/" . strtolower($row->name) . ".png' title='$countryName' alt='$countryName'/>";
        }
        //TODO refactor this, looks ugly
        return array($countryName, $icon);
    }

    function changeGoals($j, $row) {
        $goalName = $this->joomlaWatch->goal->getGoalNameById($row->name);
        $groupTruncated = $this->joomlaWatch->helper->truncate($goalName, $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_TRUNCATE_STATS'));
        if (@ $row->name) {
            $row->name = "<a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals&action=edit&goalId=$row->name'>$groupTruncated</a>";
        }
    }

    function changeInternal($j, $row) {
        $inboundRow = $this->joomlaWatch->visit->getInternalNameById(@ $row->name);
        $from = @$inboundRow->from;
        $to = @$inboundRow->to;
        $fromEncoded = urlencode($from);
        $toEncoded = urlencode($to);
        $fromTruncated = $this->joomlaWatch->helper->truncate($from, $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_TRUNCATE_STATS')-5);
        $toTruncated = $this->joomlaWatch->helper->truncate($to, $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_TRUNCATE_STATS')-5);
        $row->value = "<table><tr><td>".$row->value."</td><td>
    <a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals&action=insert&from=$fromEncoded&uri=$toEncoded' title='"._JW_STATS_ADD_TO_GOALS."'>
    <img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/goal.gif'/ border='0'></a></td></tr></table>";

        $row->name = "-&gt;<a href='".$this->joomlaWatch->config->getLiveSite()."$to' target='_blank' onmouseover=\"toggleDiv('internalDetailDiv$j',1)\" onmouseout=\"toggleDiv('internalDetailDiv$j',0)\">$toTruncated</a>
        <div id='internalDetailDiv$j' class='internalDetailDiv'><table><tr><td colspan='3'>"._JW_STATS_FROM.": </td></tr><tr><td><b>".$this->joomlaWatch->visit->getTitleByUri($from)."</b></td></tr><tr><td><a href='".$this->joomlaWatch->config->getLiveSite()."/$from'>$from</a></td></tr><tr><td colspan='3'>"._JW_STATS_TO.": </td></tr><tr><td><b>".$this->joomlaWatch->visit->getTitleByUri($to)."</b></td></tr><tr><td><a href='".$this->joomlaWatch->config->getLiveSite()."/$to'>$to</a></td></tr></table></div>";


    }

    function changeUsers($j, $row) {
        $row->name = "<a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_users&task=view&search=$row->name'>$row->name</a>";
    }

    function changeKeywords($j, $row) {
        // to have characters like " instead of it's code
        $row->name = strip_tags(urldecode($row->name));
    }

    function changeIP($j, $row) {
        if (@ $row->name) {
            $mapsIcon = "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/map_icon.gif' border='0'  " . $this->joomlaWatch->helper->getTooltipOnEvent() . "=\"ajax_showTooltip('" . $this->joomlaWatch->config->getLiveSite() . "components/com_joomlawatch/tooltip.php?rand=" . $this->joomlaWatch->config->getRand() . "&ip=" . @ $row->name . "',this);return false\"/>";

            if ($this->joomlaWatch->block->getBlockedIp($row->name)) {
                $ipStrikedOut = "<s>" . $row->name . "</s>";
            } else {
                $ipStrikedOut = $row->name;
            }
            $blocked = $this->joomlaWatch->block->getBlockedIp($row->name);
            $country = "";
            $country = $this->joomlaWatch->helper->countryByIp($row->name);
            $countryName = $this->joomlaWatch->helper->countryCodeToCountryName($country);

            if (!$country) {
                $country = "none";
            }
            $ip = $row->name;
            $icon = "<table><tr><td>".$mapsIcon . "</td><td><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/flags/" . strtolower($country) . ".png' title='$countryName' alt='$countryName'/></td>";
            $row->name = "<td><a  id='$row->name' href='javascript:blockIpToggle(\"$row->name\");' style='color: black;'>" . $ipStrikedOut . "</a></td></tr></table>";

        }
    }

    //TODO need to implement caching on these keyphrases and store only id instead of full text
    function changeKeyphrase($j, $row) {
        // to have characters like " instead of it's code
        $keyPhrase = strip_tags(urldecode($row->name));
        $keyPhraseTruncated = $this->joomlaWatch->helper->truncate($keyPhrase, 15);
        $row->name = "<a href='http://www.google.com/search?q=".urlencode($keyPhrase)."' target='_blank' title='".$keyPhrase."'>".$keyPhraseTruncated."</span>";
    }


    /* visit */
    function renderIntValuesByName($groupOriginal, $expanded = false, $total = false, $date = 0, $limit = 5, $frontend = false) {

        $group = @ constant('DB_KEY_'.strtoupper($groupOriginal));

        if (!$date) {
            $date = $this->joomlaWatch->helper->jwDateToday();
        }

        if ($total) {
            $rows = $this->joomlaWatch->stat->getTotalIntValuesByName($group, $expanded, $limit);
            $count = $this->joomlaWatch->stat->getTotalCountByKey($group);
        } else {
            $rows = $this->joomlaWatch->stat->getIntValuesByName($group, $date, $expanded, $limit);
            $count = $this->joomlaWatch->stat->getCountByKeyAndDate($group, $date);
        }

        $i = 0xFF;

        $output = "";

        $j = 0;
        for ($k=0; $k<sizeof($rows); $k++) {

            $row = $rows[$k];

            $origName = $row->name;

            if (@ !$total && @!$frontend) {
                $relDiff = $this->joomlaWatch->stat->getRelDiffOfTwoDays($date -2, $date -1, $group, $row->name);
                $diffOutput = $this->joomlaWatchTrendHTML->renderDiff($group, $row->name, $date -1, $relDiff, 1, "$relDiff% - " . _JW_TOOLTIP_YESTERDAY_INCREASE);
            }

            $i -= 3;
            $color = sprintf("%x", $i) . sprintf("%x", $i) . sprintf("%x", $i);

            if ($count) {
                $percent = floor(($row->value / $count) * 1000) / 10;
            }
            else {
                $percent = 0;
            }

            switch (@$group) {

                case DB_KEY_REFERERS : {
                    $this->changeReferers($j, $row);
                    break;
                }
                case DB_KEY_URI : {
                    $this->changeURI($j, $row);
                    break;
                }
                case ($group == DB_KEY_BROWSER or $group == DB_KEY_OS) : {
                    $icon = $this->changeBrowserOS($j, $row);
                    break;
                }
                case DB_KEY_COUNTRY : {
                //TODO refactor this, looks ugly
                    $arrayCountryIcon = $this->changeCountry($j, $row, $frontend);
                    if (@$arrayCountryIcon) {
                        $countryName = $arrayCountryIcon[0];
                        $icon = $arrayCountryIcon[1];
                    }
                    break;
                }
                case DB_KEY_GOALS : {
                    $this->changeGoals($j, $row);
                    break;
                }
                case DB_KEY_INTERNAL : {
                    $this->changeInternal($j, $row);
                    break;
                }
                case DB_KEY_USERS : {
                    $this->changeUsers($j, $row);

                    break;
                }
                case DB_KEY_KEYWORDS : {
                    $this->changeKeywords($j, $row);
                    break;
                }
                case DB_KEY_IP : {
                    $this->changeIP($j, $row);
                    break;
                }

                case DB_KEY_KEYPHRASE : {
                    $this->changeKeyphrase($j, $row);
                    break;
                }
            }

            $trendsIcon = "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/trend_icon.gif' border='0'  " . $this->joomlaWatch->helper->getTooltipOnEvent() . "=\"ajax_showTooltip('" . $this->joomlaWatch->config->getLiveSite() . "components/com_joomlawatch/trendtooltip.php?rand=" . $this->joomlaWatch->config->getRand() . "&group=$group&name=$origName&date=$date',this);return false\"/>";
            $progressBarIcon = $this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/progress_bar.gif";

            $imgWidth = $this->sinusInsteadOfLinear(50, $percent);

            $color = "ffffff";
            if (@ $row->name) {
                if (!$total) {
                    $output .= "
                    <table>
                    <tr>
                        <td>" . @ $icon . "&nbsp;" . $row->name . "</td>
                        <td align='right'><table><tr><td align='right'>" . $row->value . "</td></tr></table></td>
                        <td>
                            <table border='0'>
                            <tr>
                                <td>" . @ $diffOutput . "</td>
                                <td>" . @ $trendsIcon . "</td>
                                <td><img src='$progressBarIcon' width='".$imgWidth."' height='10' /></td>
                                <td align='left'>$percent%</td>
                            </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
                    ";

                } else {
                    if (!@ $frontend) {
                        $output .= "<tr><td align='left' style='background-color: "."#".$color.";'>" . @ $icon . "&nbsp;" . $row->name . "</td><td style='background-color: #".$color.";' align='right'>" . $row->value . "</td><td style='background-color: #".$color.";'> <table><tr><td><img src='$progressBarIcon' width='".$imgWidth."' height='10' /></td><td  " . $this->joomlaWatch->helper->getTooltipOnEvent() . "=\"ajax_showTooltip('" . $this->joomlaWatch->config->getLiveSite() . "components/com_joomlawatch/trendtooltip.php?rand=" . $this->joomlaWatch->config->getRand() . "&group=$group&name=$origName&date=$date',this);return false\">$percent%</td></tr></table></td></tr>";
                    } else {
                       $output .= "<tr><td valign='top' align='right' class='joomlawatch'>$percent%</td><td valign='top' align='left' class='joomlawatch'>" . @ $icon . "&nbsp;</td><td valign='top' align='left' class='joomlawatch'>" . $countryName . "</td></tr>";
                    }
                }
            }
            $j++;
        }


        if ($count > $limit && !$frontend)
            $output = "<tr><td colspan='4'>" . $this->renderExpand($groupOriginal, $total) . "</td></tr>" . $output;

        if (@ $count && !@ $frontend)
            $output .= "<tr><td colspan='5'><b>" . _JW_STATS_TOTAL . ":</b> " . @ $count . " </td></tr>";

        return $output;
    }


    /* visit */
    function renderIntValuesByNameForFrontend($groupOriginal, $expanded = false, $total = false, $date = 0, $limit = 5, $frontend = false) {

        $group = @ constant('DB_KEY_'.strtoupper($groupOriginal));

        $maxColumns = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_FRONTEND_COUNTRIES_MAX_COLUMNS");
        $maxRows = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_FRONTEND_COUNTRIES_MAX_ROWS");
        $countryNamesDisplayed = $this->joomlaWatch->config->getCheckboxValue("JOOMLAWATCH_FRONTEND_COUNTRIES_NAMES");
        $flagsFirst = $this->joomlaWatch->config->getCheckboxValue("JOOMLAWATCH_FRONTEND_COUNTRIES_FLAGS_FIRST");

        if ($maxColumns * $maxRows > $limit) {  //override the limit if rows/columns set
            $limit = $maxColumns * $maxRows;
        }

        if (!$date) {
            $date = $this->joomlaWatch->helper->jwDateToday();
        }

        if ($total) {
            $rows = $this->joomlaWatch->stat->getTotalIntValuesByName($group, $expanded, $limit);
            $count = $this->joomlaWatch->stat->getTotalCountByKey($group);
        } else {
            $rows = $this->joomlaWatch->stat->getIntValuesByName($group, $date, $expanded, $limit);
            $count = $this->joomlaWatch->stat->getCountByKeyAndDate($group, $date);
        }

        $i = 0xFF;

        $output = "";


        $j = 0;
        for ($k=0; $k<sizeof($rows); $k++) {

            $currentColumn = $k % $maxColumns;
            $currentRow = floor($k/$maxColumns);
            $computed = $currentColumn * $maxRows + $currentRow;
            if ($computed <= ($maxColumns * $maxRows) && $currentRow < $maxRows) {
                $row = $rows[$computed];

                $origName = $row->name;

                $i -= 3;
                $color = sprintf("%x", $i) . sprintf("%x", $i) . sprintf("%x", $i);

                if ($count) {
                    $percent = floor(($row->value / $count) * 1000) / 10;
                }
                else {
                    $percent = 0;
                }

                switch (@$group) {

                    case DB_KEY_COUNTRY : {
                    //TODO refactor this, looks ugly
                        $arrayCountryIcon = $this->changeCountry($j, $row, $frontend);
                        if (@$arrayCountryIcon) {
                            $countryName = $arrayCountryIcon[0];
                            $icon = $arrayCountryIcon[1];
                        }
                        break;
                    }
                }

                if (@ $row->name) {

                    if ($currentColumn == 0) {
                        $output .= "<tr>";
                    }

                    if ($flagsFirst) {
                        $output .= "<td valign='top' align='right' class='joomlawatch'>" . @ $icon . "&nbsp;</td>";
                        $output .= "<td valign='top' align='left' class='joomlawatch'>$percent%</td>";
                    } else {
                        $output .= "<td valign='top' align='right' class='joomlawatch'>$percent%</td>";
                        $output .= "<td valign='top' align='left' class='joomlawatch'>" . @ $icon . "&nbsp;</td>";
                    }

                    if ($countryNamesDisplayed) {
                        $output .= "<td valign='top' align='left' class='joomlawatch'>" . $countryName . "</td>";
                    } else {
                        $output .= "<td valign='top' align='left' class='joomlawatch'></td>";
                    }


                    if ($currentColumn == $maxColumns-1) {
                        $output .= "</tr>";
                    }

                }
                $j++;
            }
        }


        return $output;
    }


    function renderNightlyEmail() {
        $keysArray = array('goals', 'uri', 'referers', 'keywords', 'keyphrase', 'internal', 'country', 'ip', 'users', 'browser', 'os');
        $day = $this->joomlaWatch->helper->jwDateToday();

        $output = "<table>";
        foreach ($keysArray as $key) {
            if ($key == 'ip' && !$this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_IP_STATS')) {
                continue;
            }
            $output .= nl2br($this->renderIntValuesByNameForEmail($key, true, false, $day, 10, false, true ));

            $output .= "<tr><td colspan='4'>&nbsp;</td></tr>";
        }
        $output .= "</table>";
        return $output;
    }


    function sendNightlyEmail() {

        $output = $this->renderNightlyEmail();
        $domain = $this->joomlaWatch->config->getDomainFromLiveSite();
        $email = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_REPORTS_ADDRESS");
        $date = joomlaWatchHelper::date("d.m.Y", JoomlaWatchHelper::getLocalTime() - 24*3600); // date of report from yesterday, not today);
        JoomlaWatchHelper::sendEmail("$email","$email", "JoomlaWatch report - $domain - $date", $output);

        return $output;
    }


    /* visit */
    function renderIntValuesByNameForEmail($groupOriginal, $expanded = false, $total = false, $date = 0, $limit = 5, $frontend = false) {

        $truncate = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_NAME_TRUNCATE");

        $group = @ constant('DB_KEY_'.strtoupper($groupOriginal));
        $groupTranslated = @constant ("_JW_STATS_".strtoupper($groupOriginal));

        $output = "";

        if (!$date) {
            $date = $this->joomlaWatch->helper->jwDateToday();
        }

        $onlyPercentHigherThan = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_PERCENT_HIGHER_THAN");

        $oneDayPositive = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_ONE_DAY_CHANGE_POSITIVE");
        $oneDayNegative = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_ONE_DAY_CHANGE_NEGATIVE");
        $sevenDayPositive = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_SEVEN_DAY_CHANGE_POSITIVE");
        $sevenDayNegative = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_SEVEN_DAY_CHANGE_NEGATIVE");

        $thirtyDayPositive = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_THIRTY_DAY_CHANGE_POSITIVE");
        $thirtyDayNegative = $this->joomlaWatch->config->getConfigValue("JOOMLAWATCH_EMAIL_THIRTY_DAY_CHANGE_NEGATIVE");

        $rows = $this->joomlaWatch->stat->getIntValuesByName($group, $date-1, $expanded, $limit);   // -1 because of yesterday
        $count = $this->joomlaWatch->stat->getCountByKeyAndDate($group, $date-1);   // -1 because of yesterday

        $j = 0;
        foreach ($rows as $row) {

            $origName = $row->name;

            $relOneDayDiff = $this->joomlaWatch->stat->getRelDiffOfTwoDays($date -2, $date -1, $group, $row->name); //-2, -1 because we are rendering yesterday
            $relSevenDayDiff = $this->joomlaWatch->stat->getRelDiffOfTwoDays($date -8, $date -1, $group, $row->name); //-7, -1 because we are rendering yesterday -7
            $relThirtyDayDiff = $this->joomlaWatch->stat->getRelDiffOfTwoDays($date -31, $date -1, $group, $row->name); //-30, -1 because we are rendering yesterday -30

            if ($count) {
                $percent = floor(($row->value / $count) * 1000) / 10;
            }
            else {
                $percent = 0;
            }


            if (!$onlyPercentHigherThan || $percent > $onlyPercentHigherThan) {

                /** check boundaries */
                if (
                        (
                        (!$relOneDayDiff || ($relOneDayDiff <= $oneDayNegative)) ||
                        (!$relOneDayDiff || ($relOneDayDiff >= $oneDayPositive))
                        )

                        &&
                        (
                        (!$relSevenDayDiff || ($relSevenDayDiff <= $sevenDayNegative)) ||
                        (!$relSevenDayDiff || ($relSevenDayDiff >= $sevenDayPositive))
                        )

                        &&
                        (
                        (!$relThirtyDayDiff || ($relThirtyDayDiff <= $thirtyDayNegative)) ||
                        (!$relThirtyDayDiff || ($relThirtyDayDiff >= $thirtyDayPositive))
                        )

                ) {

                    if ($relOneDayDiff > 0) {
                        $oneDayDiffColor = "green";
                        $oneDayDiffSign = "+";
                    } else {
                        $oneDayDiffColor = "red";
                        $oneDayDiffSign = "";
                    }

                    if ($relSevenDayDiff > 0) {
                        $sevenDayDiffColor = "green";
                        $sevenDayDiffSign = "+";
                    } else {
                        $sevenDayDiffColor = "red";
                        $sevenDayDiffSign = "";
                    }
                    if ($relThirtyDayDiff > 0) {
                        $thirtyDayDiffColor = "green";
                        $thirtyDayDiffSign = "+";
                    } else {
                        $thirtyDayDiffColor = "red";
                        $thirtyDayDiffSign = "";
                    }

                    switch ($group) {
                        case DB_KEY_GOALS: {
                            $row->name = $this->joomlaWatch->goal->getGoalNameById($row->name);
                            break;
                        }
                        case DB_KEY_INTERNAL: {
                            $internal = $this->joomlaWatch->visit->getInternalNameById(@ $row->name);
                            $from = $this->joomlaWatch->visit->getTitleByUri($internal->from);
                            $to = $this->joomlaWatch->visit->getTitleByUri($internal->to);
                            $internal->from = JoomlaWatchHelper::truncate($internal->from, $truncate);
                            $internal->to = JoomlaWatchHelper::truncate($internal->to, $truncate);

                            $row->name = "".$internal->from." -> <br/>".$internal->to." " ;
                            break;
                        }
                        default: {
                            $row->name = JoomlaWatchHelper::truncate($row->name, $truncate);
                            break;

                        }

                    }

                    if (@ $row->name && $relOneDayDiff != 0) {
                        $output .= sprintf("<tr><td>%s</td><td align='right'>%.1f</td><td align='right'>%.1f%%</td><td align='right' style='color: $oneDayDiffColor'>$oneDayDiffSign%.1f%%</td><td align='right' style='color: $sevenDayDiffColor'>$sevenDayDiffSign%.1f%%</td><td align='right' style='color: $thirtyDayDiffColor'>$thirtyDayDiffSign%.1f%%</td></tr>",
                            $row->name,
                            $row->value,
                            $percent,
                            $relOneDayDiff,
                            $relSevenDayDiff,
                            $relThirtyDayDiff
                        );
                        $j++;
                    }
                }
            }

        }

        if ($j) {
            $output = "<tr><td><h3>$groupTranslated</h3><td align='center'>&nbsp;<u>"._JW_EMAIL_REPORTS_VALUE."</u></td><td align='center'>&nbsp;<u>"._JW_EMAIL_REPORTS_PERCENT."</u></td><td align='center'>&nbsp;<u>"._JW_EMAIL_REPORTS_1DAY_CHANGE."</u></td><td align='center'>&nbsp;<u>"._JW_EMAIL_REPORTS_7DAY_CHANGE."</u></td><td align='center'>&nbsp;<u>"._JW_EMAIL_REPORTS_30DAY_CHANGE."</u></td></tr>".$output;
        }

        return $output;
    }


    /**
     * percent not linear but by sinusoid approaching to 100%
     * @param  $maxWidth
     * @return float
     */
    function sinusInsteadOfLinear($maxWidth, $percent) {
        $pi = 3.141592653;
        return floor($maxWidth*sin($percent/100 * $pi/2));
    }

    /**
     * stats
     * upper side visits statistics - unique/loads/hits
     * @param unknown_type $week
     * @return unknown
     */
    function renderVisitsGraph($week = 0) {
        $output = "";

        //        $today = JoomlaWatchHelper::date("d.m.Y", $this->joomlaWatch->helper->getServerTime());
        $today = JoomlaWatchHelper::date("d.m.Y", JoomlaWatchHelper::getLocalTime());
        $dateExploded = explode('.', $today);
        $dayOfWeek = $this->joomlaWatch->helper->dayOfWeek();
        $startTimestamp = ($week * 24 * 3600 * 7) - (3*24*3600);

        $i = 0xFF;
        $dateWeekStart = $this->joomlaWatch->helper->getDayByTimestamp($startTimestamp);
        $maxLoads = $this->joomlaWatch->stat->getMaxValueInGroupForWeek(DB_KEY_LOADS, DB_KEY_LOADS, $dateWeekStart);

        for ($sec = $startTimestamp; $sec < $startTimestamp+7*3600*24; $sec += 24 * 3600) {
            $i -= 3;
            $color = sprintf("%x", $i) . sprintf("%x", $i) . sprintf("%x", $i);

            if ($i % 2 == 0) {
                $color = "#f5f5f5";
            }
            else {
                $color = "#fafafa";
            }

            $percent = 0;
            $count = 0;
            /*			$date = JoomlaWatchHelper::jwDateBySeconds($sec);
                        echo("date: $date");
            */
            $date = floor($sec / 3600 / 24 + $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_DAY_OFFSET'));

            $stats['unique'] = $this->joomlaWatch->stat->getKeyValueInGroupByDate(DB_KEY_UNIQUE, DB_KEY_UNIQUE, $date);
            $stats['loads'] = $this->joomlaWatch->stat->getKeyValueInGroupByDate(DB_KEY_LOADS, DB_KEY_LOADS, $date);
            $stats['hits'] = $this->joomlaWatch->stat->getKeyValueInGroupByDate(DB_KEY_HITS, DB_KEY_HITS, $date);

            foreach ($stats as $key => $value) {

                $count = $stats['loads'];
                if ($count)
                    $percent = floor(($value / $count) * 100);

                $progressBarIcon = $this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/progress_bar_$key.gif";

                $output .= "<tr><td style='background-color: ".$color.";'>";
                $dow = JoomlaWatchHelper::date("D", $sec);
                if (@ !$once[$dow]) {
                    $output .= substr(JoomlaWatchHelper::date("d.m.Y", $sec), 0, 6) . "&nbsp;" . $dow;
                    $once[$dow] = 1;
                }
                $output .= "</td>";

                if ($maxLoads)
                    $percentWidth = floor( ($percent * $value) / $maxLoads);
                else
                    $percentWidth = $percent;

                if (@ $value) {
                    switch ($key) {
                        case 'hits' : {
                            $fontColor = "#aaaaaa";
                                $output .= "<td align='right' style='color:".$fontColor."; background-color: ".$color.";'>" . $value . "</td><td style='background-color: ".$color.";'> <table cellpadding='0' cellspacing='0' ><tr><td style='background-color: ".$color.";'></td><td style='color:".$fontColor."; background-color: ".$color.";'>&nbsp;</td></tr></table></td>";
                            break;
                        }
                        case 'loads': {
                            $fontColor = "#00C000";
                            $output .= "<td align='right' style='color:".$fontColor."; background-color: ".$color.";'>" . $value . "</td><td style='background-color: ".$color.";'> <table cellpadding='0' cellspacing='0' ><tr><td style='background-color: ".$color.";'><img src='$progressBarIcon' width='$percentWidth' height='10' /></td><td style='color:".$fontColor."; background-color: ".$color.";'></td></tr></table></td>";
                            break;
                        }
                        default: {
                            $fontColor = "black";
                            $output .= "<td align='right' style='color:".$fontColor."; background-color: ".$color.";'>" . $value . "</td><td style='background-color: ".$color.";'> <table cellpadding='0' cellspacing='0' ><tr><td style='background-color: ".$color.";'><img src='$progressBarIcon' width='$percentWidth' height='10' /></td><td style='color:".$fontColor."; background-color: ".$color.";'>&nbsp;$percent%</td></tr></table></td>";
                        }
                    }
                } else
                    $output .= "<td align='right' style='background-color: ".$color.";'></td><td align='right' style='background-color: ".$color.";'></td>";

                $output .= "</tr>";

            }

        }
        $output .= "<tr><td colspan='3' align='right'>* <span style='color:#0000FF;'>" . _JW_STATS_UNIQUE . "</span>, <span style='color:#00C000;'>" . _JW_STATS_LOADS . "</span>, <span style='color:#aaaaaa;'>" . _JW_STATS_HITS . "</span></td></tr>";

        return $output;
    }


    /* stats */
    function renderExpand($element, $total = false) {

        $elementSuffix = "";
        if (@$total) {
            $elementSuffix = "_total";
        }

        if (@ JoomlaWatchHelper::requestGet($element.$elementSuffix) == "false" || !@ JoomlaWatchHelper::requestGet($element.$elementSuffix)) {
            $operation = "expand";
        }
        else {
            $operation = "collapse";
        }

        $operationTranslated = constant("_JW_STATS_" . strtoupper($operation));
        $elementTranslated = constant("_JW_STATS_" . strtoupper($element));


        $output = "<a name='$element'></a><a href=\"javascript:expand('$element".$elementSuffix."')\" id='$element".$elementSuffix."'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/$operation.gif' border='0' alt='$operation'/>$operationTranslated&nbsp;$elementTranslated</a>";

        return $output;
    }

    /* stats */
    function renderSwitched($element, $text, $value) {
        $output = "";
        if ($element != $value) {
            $output .= "<a href=\"javascript:setStatsType('$element');\" id='$element'>$text</a>";
        } else
            $output .= "$text</a>";

        return $output;
    }

    /* stats */
    function renderTabClass($name, $value) {
        if ($name == $value) {
            return "tab_active";
        }
        else {
            return "tab_inactive";
        }
    }

    /* stats */
    function renderFrontendStats($joomlaWatch) {

        $output = "";
        $outputFirst = "";
        $outputSecond = "";
        if (@ $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_FRONTEND_COUNTRIES')) {
            $cachedItem = $this->joomlaWatch->cache->getCachedItem("CACHE_FRONTEND_COUNTRIES");
            $cachedItem = $this->joomlaWatch->config->replaceHttpByHttps($cachedItem);
            if (@ $cachedItem) {
                $outputFirst .= stripslashes($cachedItem);
            } else {

                $numberOfCountries = @ $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_FRONTEND_COUNTRIES_NUM');
                if (!$numberOfCountries) {
                    $numberOfCountries = 5;
                }

                $ctryOutput = "<table width='150' border='0' class='joomlawatch'>";
                $ctryOutputFetched = $this->renderIntValuesByNameForFrontend("country", false, true, 0, $numberOfCountries, true);
                $ctryOutput .= $ctryOutputFetched;
                $ctryOutput .= "</table>";

                if ($ctryOutputFetched) {
                    $this->joomlaWatch->cache->storeCachedItem("CACHE_FRONTEND_COUNTRIES", $ctryOutput);
                }
                $outputFirst .= $ctryOutput;
            }
        }

        if (@ $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_FRONTEND_VISITORS')) {

            $cachedItem = $this->joomlaWatch->cache->getCachedItem("CACHE_FRONTEND_VISITORS");
            $cachedItem = $this->joomlaWatch->config->replaceHttpByHttps($cachedItem);
            if (@ $cachedItem) {
                $outputSecond .= stripslashes($cachedItem);
            } else {

                $todayDate = $this->joomlaWatch->helper->jwDateToday();
                $yesterdayDate = $todayDate -1;
                $dow = $this->joomlaWatch->helper->dayOfWeek();
                $dom = $this->joomlaWatch->helper->dayOfMonth();
                $numOfDaysActualMonth = JoomlaWatchHelper::date("t", $this->joomlaWatch->helper->getServerTime());
                $numOfDaysPrevMonth = JoomlaWatchHelper::date("t", $this->joomlaWatch->helper->getServerTime() - $numOfDaysActualMonth * 24 * 3600);
                $lastMonthsDate = $todayDate - $numOfDaysActualMonth;

                $timePeriodArray = array('TODAY', 'YESTERDAY','THIS_WEEK', 'LAST_WEEK', 'THIS_MONTH','LAST_MONTH','TOTAL');

                $visOutput = "";
                $visOutput .= "<table width='150' border='0' class='joomlawatch'>";

                foreach ($timePeriodArray as $key) {

                    if (!@$this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_FRONTEND_VISITORS_'.$key)) {
                        continue;	// isn't enabled
                    }

                    switch($key) {
                        case 'TODAY': {
                            $value = $this->joomlaWatch->stat->getCountByKeyAndDate(DB_KEY_UNIQUE, $todayDate);
                            break;
                        }
                        case 'YESTERDAY': {
                            $value = $this->joomlaWatch->stat->getCountByKeyAndDate(DB_KEY_UNIQUE, $yesterdayDate);
                            break;
                        }
                        case 'THIS_WEEK': {
                            $value = $this->joomlaWatch->stat->getCountByKeyBetweenDates(DB_KEY_UNIQUE, $todayDate - $dow, $todayDate);
                            break;
                        }
                        case 'LAST_WEEK': {
                            $value = $this->joomlaWatch->stat->getCountByKeyBetweenDates(DB_KEY_UNIQUE, $todayDate - $dow -7, $todayDate - $dow);
                            break;
                        }
                        case 'THIS_MONTH': {
                            $value = $this->joomlaWatch->stat->getCountByKeyBetweenDates(DB_KEY_UNIQUE, $todayDate - $dom, $todayDate);
                            break;
                        }
                        case 'LAST_MONTH': {
                            $value = $this->joomlaWatch->stat->getCountByKeyBetweenDates(DB_KEY_UNIQUE, $lastMonthsDate - $numOfDaysPrevMonth, $lastMonthsDate);
                            break;
                        }
                        case 'TOTAL': {
                            $totalFromSettings = $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_FRONTEND_VISITORS_TOTAL_INITIAL');
                            $totalReal = $this->joomlaWatch->stat->getTotalCountByKey(DB_KEY_UNIQUE);
                            if (@ $totalFromSettings) {
                                /** use total from settings, but still append the real value as a comment */
                                $value = ($totalFromSettings + $totalReal) . "<!-- $totalReal -->";
                            } else {
                                $value = $totalReal;
                            }
                            break;
                        }

                    }
                    if (isset($value)) {
                        $visOutput .= "<tr><td align='left' valign='top' class='joomlawatch'>" . @constant("_JW_FRONTEND_".$key) . ": </td><td align='left' valign='top'> " . @ $value . "</td></tr>";
                    }
                }
                $visOutput .= "</table>";

                if (@ $value) {
                    $this->joomlaWatch->cache->storeCachedItem("CACHE_FRONTEND_VISITORS", $visOutput);
                }
                $outputSecond .= $visOutput;
            }
        }
        // to be able to swap order of Countries/Visitors
        if (@ $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_FRONTEND_COUNTRIES_FIRST')) {
            $output .= "<br/><br/>" . $outputFirst . "<br/>" . $outputSecond . "<br/><br/>";
        } else {
            $output .= "<br/><br/>" . $outputSecond . "<br/>" . $outputFirst . "<br/><br/>";
        }

        return $output;

    }

    /* stats */
    function renderFrontendUsers() {

        $output = "";
        $cachedItem = $this->joomlaWatch->cache->getCachedItem("CACHE_FRONTEND_USERS");
        if (@ $cachedItem) {
            $output .= stripslashes($cachedItem);
        } else {

            $users = $this->joomlaWatch->stat->getUsersForToday();
            $count = $this->joomlaWatch->stat->countUsersForToday();

            /** no users */
            if (@!$count) {
                return;
            }


            $output = "<h2 class='joomlaWatch'>"._JW_STATS_USERS."</h2>";

            $output .= "<table class='joomlaWatch'><tr><td><u>"._JW_FRONTEND_USERS_MOST." $count:</u></td></tr><tr><td>";
            $link = $this->joomlaWatch->config->getConfigValue('JOOMLAWATCH_FRONTEND_USER_LINK');

            $i=0;
            $userSize = sizeof($users);
            foreach ($users as $user)  {
                $i++;
                if (@$link)	{
                    $linkOutput = str_replace("{user}", $user->name, $link);
                    $output .= "<a href='".$linkOutput."'>";
                }
                $output .= htmlspecialchars($user->name);
                if (@$link)	{
                    $output .= "</a>";
                }
                if ($i<$userSize) {
                    $output .= ", ";
                }
            }
            $output .= "</td></tr></table>";

            $this->joomlaWatch->cache->storeCachedItem("CACHE_FRONTEND_USERS", $output);
        }

        return $output;
    }

    /* stat */
    function renderBackToStats($result = "") {
        $output = "<a href='".$this->config->getAdministratorIndex()."?option=com_joomlawatch'> &lt;&lt; " . _JW_BACK . "</a>";
        return $output;
    }


}

<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchTrendHTML {

    var $joomlaWatch;

    function JoomlaWatchTrendHTML($joomlaWatch) {
        $this->joomlaWatch = $joomlaWatch;
    }

    /* trend */
    function renderDiff($group, $name, $date, $relDiff, $onlyImage = 0, $title = "") {
        $diffOutput = "";

        if (!$relDiff) {
            $color = "gray";
        } else {
            if ($relDiff < 0) {
                $color = "red";
            } else {
                $color = "green";
            }
        }

        if (!$onlyImage) {
            $diffOutput .= "<table cellpadding='0' cellspacing='0'><tr><td><a href='" . @ $this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=trends&group=$group&name=$name&date=$date' style='color:$color;'>";
            $diffOutput .= "$relDiff%</a></td><td>";
        }
        $diffOutput .= "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/trend_$color.gif' border='0' title='$title'/>";
        if (!$onlyImage) {
            $diffOutput .= "</td></tr></table>";
        }

        return $diffOutput;
    }

    /* trend */
    function renderDayDiff($group, $name, $date1, $date2, $onlyImage = 0) {
        $relDiff = $this->joomlaWatch->stat->getRelDiffOfTwoDays($date1, $date2, $group, $name);
        $output = $this->renderDiff($group, $name, $date2, $relDiff, $onlyImage);
        return $output;

    }

    /* trend */
    function renderDayTrends($group, $name, $date) {

        $dbKeysArray = array(1 => 'BROWSER','COUNTRY','GOALS','HITS','INTERNAL','IP','KEYWORDS','LOADS','OS','REFERERS','UNIQUE','URI','USERS');

        $resultsArray = array ();
        $max = 0;
        $maxDate = 0;
        for ($i = $date -20; $i <= $date; $i++) {
            $value = $this->joomlaWatch->stat->getKeyValueInGroupByDate($group, $name, $i);
            if ($max < $value) {
                $max = $value;
                $maxDate = $i;
            }
            $resultsArray[$i - ($date -20)] = $value;
        }



        $nameTranslated = $name;

        switch ($group) {
            case DB_KEY_INTERNAL: {
            /* special handling for inbound */
                $inboundRow = $this->joomlaWatch->visit->getInternalNameById(@$name);
                $from = $inboundRow->from;
                $to = $inboundRow->to;

                $nameTranslated = "<br/>$from -> $to";
                break;
            }

            case DB_KEY_GOALS: {
                $nameTranslated = "<br/>".$this->joomlaWatch->goal->getGoalNameById($name);
                break;
            }
            default: {
                $nameTranslated = $name;
            }
        }


        $groupTranslated = @constant ("_JW_STATS_".strtoupper($dbKeysArray[$group]));

        $helpId = JoomlaWatchHTML::renderOnlineHelp("trends");
        $output = "<center><h2>" . _JW_TRENDS_DAILY_WEEKLY . " $groupTranslated : $nameTranslated $helpId</h2><br/>";
        $output .= "<table cellpadding='0' cellspacing='0'>";
        $output .= "<tr>";
        $progressBarIcon = $this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/progress_bar_vertical.gif";

        for ($i = $date -20; $i <= $date; $i++) {
            $value = ($resultsArray[$i - ($date -20)]);
            $percent = 0;
            if ($max) {
                $percent = floor(($value / $max) * 1000) / 10;
            }
            $output .= "<td valign='bottom' align='center'>";
            $output .= "$value<br/><img src='$progressBarIcon' height='$percent' width='10' /><br/>";
            $output .= $this->renderDayDiff($group, $name, $i -1, $i);
            $output .= $this->joomlaWatch->helper->getDateByDay($i, "d.m ") . "&nbsp;<br/>";
            $output .= $this->joomlaWatch->helper->getDateByDay($i, "D") . "<br/>";
            $output .= "</td>";

        }
        $output .= "</tr>";
        $output .= "</table>";
        $output .= "</center>";

        return $output;

    }

    /* trend */
    function renderWeekTrends($group, $name, $date) {

        $resultsArray = array ();
        $max = 0;
        $maxDate = 0;
        // first day has to be monday
        $offset = - (JoomlaWatchHelper::date("N", $date * 24 * 3600) - 1) + 7;
        for ($i = $date -20 * 7 + $offset; $i <= $date + $offset; $i += 7) {
            $value = $this->joomlaWatch->stat->getSumOfTwoDays($i, $i -7, $group, $name);
            if ($max < $value) {
                $max = $value;
                $maxDate = $i;
            }
            $resultsArray[$i - ($date -20 * 7)] = $value;
        }

        $output = "<center>";
        $output .= "<table>";
        $output .= "<tr>";
        $progressBarIcon = $this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/progress_bar_vertical_wide.gif";

        for ($i = $date -20 * 7 + $offset; $i <= $date + $offset; $i += 7) {
            $value = ($resultsArray[$i - ($date -20 * 7)]);
            $percent = 0;

            if ($max) {
                $percent = floor(($value / $max) * 1000) / 10;
            }
            $output .= "<td valign='bottom' align='center'>";
            $output .= "$value<br/><img src='$progressBarIcon' height='$percent' width='20' /><br/>";
            $relDiff = $this->joomlaWatch->stat->getRelDiffOfTwoWeeks($i, $i -7, $group, $name);
            $output .= $this->renderDiff($group, $name, $i -7, $relDiff);
            $output .= JoomlaWatchHelper::date("W/y", $i * 24 * 3600) . " &nbsp;<br/>";
            $output .= "</td>";

        }
        $output .= "</tr>";
        $output .= "</table>";
        $output .= "</center>";

        return $output;

    }

    /* trend */
    function renderTrends() {
        $group = @ JoomlaWatchHelper::requestGet('group');
        $name = @ JoomlaWatchHelper::requestGet('name');
        $date = @ JoomlaWatchHelper::requestGet('date');

        $output = "";
        $output .= "<br/><br/>";
        $output .= "<br/><br/>";
        $output .= "<br/><br/>";
        $output .= "<br/><br/>";
        $output .= $this->renderDayTrends($group, $name, $date);
        $output .= "<br/><br/>";
        $output .= $this->renderWeekTrends($group, $name, $date);
        return $output;

    }

    function renderGraphSelectionForm($group) {

        $keysArray = array('goals', 'referers', 'internal', 'keyphrase', 'keywords', 'uri',  'users', 'country', 'ip', 'browser', 'os');

        $output = "<div align='center'>
        <form action='?option=com_joomlawatch&task=graphs' method='POST'>
        <select name='group' onchange='this.form.submit()'>";
        foreach ($keysArray as $key => $value) {
            $key2 = @constant("DB_KEY_".strtoupper($keysArray[$key]));
            if ($key2 == $group) {
                $selected = "selected";
            } else {
                $selected = "";
            }

            $output .= "<option value='$key2' $selected>$value</option>";
        }
        $output .= "</select>
        </form></div>
        ";
        return $output;
    }


    /* trend */
    function renderGraphsForGroup($group = 0) {

        if (!$group) {
            $group = 3;
        }

        $rows = $this->joomlaWatch->stat->getIntValuesByName($group, $date, false, 10);

        $output = $this->renderGraphSelectionForm($group);
        if (!$rows) {
            $output .= "<br/><br/><br/><br/><div style='text-align: center'><i>Empty</i></div><br/><br/><br/><br/>";
        }

        $date = JoomlaWatchHelper::jwDateToday();


        foreach ($rows as $row) {
            $output .= "<br/><br/>";
            $output .= $this->renderDayTrends($group, $row->name, $date);
            $output .= "<br/><br/>";
            $output .= $this->renderWeekTrends($group, $row->name, $date);
            $output .= "<hr/><br/>";
        }
        $output .= $this->renderGraphSelectionForm($group);
        return $output;
    }
}

<?php

/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.0
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS'))
    die('Restricted access');

class JoomlaWatchVisitHTML {

    var $joomlaWatch;

    function JoomlaWatchVisitHTML($joomlaWatch) {
        $this->joomlaWatch = $joomlaWatch;
    }

    function getJoinedURIRows($ip) {
        return $this->joomlaWatch->visit->getJoinedURIRows($ip);
    }

    /* visits */
    function renderTable($rows, $bots = false) {

        $output = "";
        $i = 0xFF;
        foreach ($rows as $row) {

            /* reset the values from previous iteration */
            $country = "none";
            $countryName = "";
            $countryUpper = "";
            $flag = "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/flags/$country.png' title='$countryName' alt='$countryName'/>";

            if ($i > 0xCC)
                $i -= 1;
            else
                $i = 0xFF;

            $rows2 = $this->getJoinedURIRows($row->ip);
            $row2 = $rows2[0];

            if ($i > 50) {
                $color = sprintf("%x", $i) . sprintf("%x", $i) . sprintf("%x", $i);
            }

            if ($bots == true)
                $color = "ffffff";

            $country = $row2->country;

            if (!$country) {
                $country = $this->joomlaWatch->helper->countryByIp($row->ip);
            }
            if (@ $country) {
                $countryName = $this->joomlaWatch->helper->countryCodeToCountryName($country);
                $flag = "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/flags/$country.png' title='$countryName' alt='$countryName'/>";
                $countryUpper = strtoupper($country);
            }

            $userAgent = $this->joomlaWatch->visit->getBrowserByIp($row->ip);

            $browser = "";
            $os = "";
            $browserIcon = "";
            $osIcon = "";

            if (@ $userAgent) {
                $browser = $this->joomlaWatch->visit->identifyBrowser(@ $userAgent);
                if (@ $browser)
                    $browserIcon = $this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/" . strtolower($browser) . ".gif";

                if (@ $browserIcon)
                    $browser = "<img src='$browserIcon' alt='$userAgent' title='$userAgent' />";

                $os = $this->joomlaWatch->visit->identifyOs(@ $userAgent);

                if (@ $os)
                    $osIcon = $this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/" . strtolower($os) . ".gif";

                if (@ $osIcon)
                    $os = "<img src='$osIcon' alt='$userAgent' title='$userAgent'/>";
            }

            if ($bots == true && $osIcon)
                continue; // bot icon fix
            if ($bots == true) {
                $osIcon = $this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/blank.gif";
                $browserIcon = $this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/blank.gif";
                $browser = "<img src='$browserIcon' alt='$userAgent' title='$userAgent' />";
                $os = "<img src='$osIcon' alt='$userAgent' title='$userAgent'/>";
            }

            /*			if (!$row->addr) {
               $addr = getHostByAddr($row->ip);
               $query = "update #__joomlawatch set host = '".$addr."' where ip = '".$row->ip."' ";
               $this->database->setQuery($query);
               $this->database->executeQuery($query);
               }
               */
            if ($this->joomlaWatch->block->getBlockedIp($row->ip))
                $ip = "<s>" . $row->ip . "</s>";
            else
                $ip = $row->ip;

            // sometimes happens that timestamp is nothing
            if (!$rows2[0]->timestamp)
                continue;

            $username = "";
            if (@ $row->username) {
                $username = "<br/><a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_users&task=view&search=$row->username' style='color: black; text-decoration:none;'><i>" . @ $row->username . "</i></a>";
            }
            $ip = "<a id='$row->ip' href='javascript:blockIpToggle(\"$row->ip\");' style='color:black;'>" . $ip . "</a>";

            $date = JoomlaWatchHelper::date("d.m.Y", $rows2[0]->timestamp); //TODO change it to JoomlaWatchHelper
            if (isset($this->lastDate) && $this->lastDate != $date) {
                $output .= "<tr><td colspan='5'><h3>$date</h3></td></tr>";
                $this->lastDate = $date;
            }

            $mapsIcon = "<img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/map_icon.gif' border='0' " . $this->joomlaWatch->helper->getTooltipOnEvent() . "=\"ajax_showTooltip('" . $this->joomlaWatch->config->getLiveSite() . "components/com_joomlawatch/tooltip.php?rand=" . $this->joomlaWatch->config->getRand() . "&ip=$row->ip',this);return false\"/>";
            $output .= ("<tr><td valign='top' align='left' style='background-color: #$color'>" . @ $row->id . "</td>
																		<td valign='top' align='left' style='background-color: #$color;'>" . @ $mapsIcon . "</td>
																		<td valign='top' align='left' style='background-color: #$color; color: #999999;'><a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals&action=insert&country=".@$countryUpper."' style='color: #999999;' title='"._JW_VISITS_ADD_GOAL_COUNTRY."'>" . @ $countryUpper . "</a></td>
																		<td valign='top' align='left' style='background-color: #$color;'>" . @ $flag . "</td>
																		<td valign='top' align='left' style='background-color: #$color;'>$ip$username</td>
																		<td valign='top' align='left' style='background-color: #$color;'>" . @ $browser . "</td>
																		<td valign='top' align='left' style='background-color: #$color;'>" . @ $os . "</td>
																		<td valign='top' align='left' style='background-color: #$color;' width='100%'>");

            foreach ($rows2 as $row2) {

                $date = JoomlaWatchHelper::date("d.m.Y", $row2->timestamp);
                $row2->timestamp = JoomlaWatchHelper::date("H:i:s", $row2->timestamp);
                $uriTruncated = $this->joomlaWatch->helper->truncate($row2->uri);
                $row2->title = $this->joomlaWatch->helper->truncate($row2->title);
                $row2->title = $this->joomlaWatch->helper->removeRepetitiveTitle($row2->title);

                $output .= ("<div id='id$row2->id' onmouseout=\"toggleDiv('goal_".$row2->id."',0);\"  onmouseover=\"toggleDiv('goal_".$row2->id."',1);\" style='background-color: #$color'>$row2->timestamp <a href='$row2->uri' target='_blank'>$row2->title</a> $uriTruncated ");

                /** ad as goal link */
                $output .= ("
                <div id='goal_".$row2->id."' style='display: none; margin: 0px; padding: 2px' class='uriDetailDiv'>
                    <a href='javascript:blockIpToggle(\"$row->ip\");'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/block.png' /> Block</a><br/>
                    <a href='".$this->joomlaWatch->config->getAdministratorIndex()."?option=com_joomlawatch&task=goals&action=insert&id=".$row2->id."' title='Add as goal'><img src='".$this->joomlaWatch->config->getLiveSite()."components/com_joomlawatch/icons/goal.gif' /> Add as goal</a>
                </div>");
                $output .= ("</div>");


                /* show only last URI, not all by user */
                if ($this->joomlaWatch->config->getCheckboxValue('JOOMLAWATCH_ONLY_LAST_URI')) {
                    break;
                }

            }
            if (@ $row->referer) {
                $refererTruncated = $this->joomlaWatch->helper->truncate($row->referer);
                $output .= "<i style='color: gray;'> " . _JW_VISITS_CAME_FROM . ": <a href='$row->referer' target='_blank' style='color: gray;' title='$row->referer'>$refererTruncated</a></i>";
                $phrase = $this->joomlaWatch->visit->extractPhraseFromUrl($row->referer);
                if (@$phrase) {
                    $output .= "<i style='color: gray;'>"._JW_VISITS_CAME_FROM_KEYWORDS.": [<a href='$row->referer' target='_blank' style='color: gray;' title='$row->referer'>".htmlspecialchars(urldecode($phrase))."</a>]</i>";
                }
            }

            $output .= ("</td></tr>");
        }

        return $output;
    }

    /* visits */
    function renderVisitors() {
        $rows = $this->joomlaWatch->visit->getVisitors();
        $this->lastDate = "";
        $output = $this->renderTable($rows);

        return $output;
    }

    /* visits */
    function renderBots() {

        $rows = $this->joomlaWatch->visit->getBots();
        $this->lastDate = "";
        $output = $this->renderTable($rows, true);

        return $output;
    }

}

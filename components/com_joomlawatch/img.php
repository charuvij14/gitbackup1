<?php


/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

include_once("includes.php");

$joomlaWatch = new JoomlaWatch();
$joomlaWatchHTML = new JoomlaWatchHTML();

if (!JOOMLAWATCH_DEBUG) {
    header('Content-Type: image/gif');

    if ($joomlaWatch->config->isAdFree() && $joomlaWatch->config->getCheckboxValue('JOOMLAWATCH_FRONTEND_HIDE_LOGO'))
        readfile (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "icons" . DS . "joomlawatch-logo-16x16-blank.gif");
    else
        readfile (JPATH_BASE . DS . "components" . DS . "com_joomlawatch" . DS . "icons" . DS . "joomlawatch-logo-16x16.gif");

}

$redirURI = @ $_SERVER[$joomlaWatch->config->getConfigValue('JOOMLAWATCH_SERVER_URI_KEY')];
$uri = @ $_SERVER['REQUEST_URI'];

if (@ $redirURI && @ substr($redirURI, -9, 9) != "index.php")
    $uri = $redirURI;

$joomlaWatch->visit->updateVisitByBrowser($uri);

//die();
?>
<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import Joomla view library
jimport('joomla.application.component.view');
 
/**
 * jmonitoringslaveView
 */
class jmonitoringslaveViewJmonitoringslave extends JView
{
	function display($tpl = null) 
	{ 
	  global $app;
    $items = & $this->get('Data');
    echo json_encode($items);
    $app->close();
	}
}
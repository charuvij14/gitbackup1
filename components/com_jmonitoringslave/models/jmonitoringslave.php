<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modelform library
jimport('joomla.application.component.modeladmin');

/**
 * JMClient Model
 */

class jmonitoringslaveModelJmonitoringslave extends JModel {
  var $_data;
  /**
   * 	@desc		return all extensions
   * 	@param		
   * 	@return		array - array of extensions
  */
  function getData()
  {
    //component param
    $params = JComponentHelper::getParams('com_jmonitoringslave');
    $maintenance = $params->get('maintenance');
    if($maintenance == 1) $maintenance = true;
    else $maintenance = false;
    
    //the keyadmin
    $secret = JRequest::getVar('access');
    //the plugin values 
    $jmpluginsexvalues = JRequest::getVar('jmpluginsexvalues');
        
    $mainframe = JFactory::getApplication();
    $keySecret = $mainframe->getCfg('secret');
    $currentMD5 = md5($keySecret);
    $isAuthorized = ($currentMD5 == $secret) ? true:false;
    
    if ($isAuthorized == true)
    {
      $status = array('access'=>true, 'maintenance'=>$maintenance);            
      $this->_data = array(
      'status' => $status,
      'versions' => $this->getVersions(),
      'filesproperties'=>$this->getFilesProperties(),
      'extensions' => $this->getExtensions(),
      'jmonitoringplugins' => $this->getJMplugin($jmpluginsexvalues)
      );

      //format data
      return $this->_data;
    }
    else
    {            
      $this->_data = array('status' => array('access'=>false));
      //format data
      return $this->_data;
    }
  }
    
  function getExtensions()
  {
    jimport('joomla.utilities.xmlelement');	
		libxml_use_internal_errors(true); // Disable libxml errors and allow to fetch error information as needed
    
    $components = $modules = $plugins = array();

    $db = & JFactory::getDBO();
    $db->setQuery('SELECT name,type,element,folder,client_id FROM #__extensions');
    $rows = $db->loadObjectList();
    //sort data
    foreach ($rows as $row)
    {
      switch ($row->type)
      {
        case 'component':         
          if ($row->client_id == "1") $componentBaseDir = JPATH_ADMINISTRATOR . DS . "components";
          else $componentBaseDir = JPATH_SITE . DS . "components";
              
          if( $dir = @opendir($componentBaseDir.DS.$row->element.DS))
          while($file = readdir($dir))
          {
            if(preg_match('#\.xml$#i',$file)) // if it's a xml
              if($xml = simplexml_load_file($componentBaseDir.DS.$row->element.DS.$file))
                 //we don't want joomla component and we check if it's an install xml
                if($xml->authorUrl != 'www.joomla.org' && ($xml->getName() == 'install' || $xml->getName() == 'extension'))
                  $components[] = array(
                  'name' => (string) $xml->name,
                  'version' => (string) $xml->version,
                  'authorurl' => (string) $xml->authorUrl,
                  'creationdate' => (string) $xml->creationDate
                  );
          }
        break;
        
        case 'module':
          // path to module directory (admin or site)
          if ($row->client_id == "1") $moduleBaseDir = JPATH_ADMINISTRATOR . DS . "modules";
          else $moduleBaseDir = JPATH_SITE . DS . "modules";

          if($dir = @opendir($moduleBaseDir.DS.$row->element.DS)) 
          while($file = readdir($dir))
          {
            if(preg_match('#\.xml$#i',$file)) // if it's a xml
              if($xml = simplexml_load_file($moduleBaseDir.DS.$row->element.DS.$file, 'JXMLElement'))
                //we don't want joomla component and we check if it's an install xml
                if($xml->authorUrl != 'www.joomla.org' && ($xml->getName() == 'install' || $xml->getName() == 'extension'))
                  $modules[] = array(
                  'name' => (string) $xml->name,
                  'version' => (string) $xml->version,
                  'authorurl' => (string) $xml->authorUrl,
                  'creationdate' => (string) $xml->creationDate
                  );
          }
        break;
        
        case 'plugin':
          if($dir = @opendir(JPATH_ROOT.DS.'plugins'.DS.$row->folder.DS.$row->element))
          while($file = readdir($dir))
          {
            if(preg_match('#\.xml$#i',$file)) // if it's a xml
              if($xml = simplexml_load_file(JPATH_ROOT.DS.'plugins'.DS.$row->folder.DS.$row->element.DS.$file, 'JXMLElement'))
                //we don't want joomla component and we check if it's an install xml
                if($xml->authorUrl != 'www.joomla.org' && ($xml->getName() == 'install' || $xml->getName() == 'extension'))
                  $plugins[] = array(
                  'name' => (string) $xml->name,
                  'version' => (string) $xml->version,
                  'authorurl' => (string) $xml->authorUrl,
                  'creationdate' => (string) $xml->creationDate
                  );
          }
        break;
      }
      unset($dir,$file,$xml);
    }
    return array('components' => $components, 'modules' => $modules, 'plugins' => $plugins);
  }
  
  function getVersions()
  {
    $morevalues = array();
    $bd = JFactory::getDbo();
    $version = new JVersion();
    
    //some versions
    $morevalues['j_version'] = $version->getShortVersion();
    $morevalues['php_version'] = phpversion();
    $morevalues['mysql_version'] = $bd->getVersion();
    //server
    if (isset($_SERVER['SERVER_SOFTWARE'])) $serverSoft = $_SERVER['SERVER_SOFTWARE'];
    else if (($sf = getenv('SERVER_SOFTWARE'))) $serverSoft = $sf;
    else $serverSoft = "NOT_FOUND";

    $morevalues['server_version'] = $serverSoft;       
    return $morevalues;
  }

  function getFilesProperties()
  {
    $filesProperties = array();
    //files to check
    $files = array(JPATH_ROOT.DS.'index.php',
    JPATH_CONFIGURATION.DS.'configuration.php',
    JPATH_ROOT.DS.'administrator'.DS. 'index.php',
    JPATH_ROOT.DS.".htaccess"
    );
    //template
    //searching the current template name   
    $db = JFactory::getDBO();
    $query = 'SELECT DISTINCT template, client_id FROM #__template_styles WHERE home=1';
    $db->setQuery($query);
    $currentsTmpl = $db->loadObjectList();
    foreach ($currentsTmpl as $tmpl)
    {
      if ($tmpl->client_id == 0) $files[] = JPATH_ROOT.DS.'templates' . DS . $tmpl->template . DS . 'index.php';
      if ($tmpl->client_id == 1) $files[] = JPATH_ROOT.DS.'administrator'.DS.'templates' . DS . $tmpl->template . DS . 'index.php';
    }
    foreach ($files as $file)
    {
      // if the file exists
      if (file_exists($file))
      {
        $fp = fopen($file, "r");
        $fstat = fstat($fp);
        fclose($fp);
        $checksum = md5_file($file);
      } elseif($file != JPATH_ROOT.DS.".htaccess") { //If not, we say that the file can't be found
        $checksum = $fstat['size'] = $fstat['mtime'] = "NOT_FOUND";
      }
      $file = array('rootpath'=>$file, 'size'=> $fstat['size'], 'modificationtime'=>$fstat['mtime'], 'checksum'=>$checksum);
      $filesProperties[] = $file;
    }
    return $filesProperties;
  }
  
  function getJMplugin($jmpluginsexvalues)
  {    
    JPluginHelper::importPlugin('jmonitoring');
    $dispatcher = JDispatcher::getInstance();
    return $dispatcher->trigger('onMonitoringCall', $jmpluginsexvalues);      
  }
}
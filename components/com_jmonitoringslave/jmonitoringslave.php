<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
 
// import joomla controller library
jimport('joomla.application.component.controller');

// Require the base controller
require_once JPATH_COMPONENT.DS.'controller.php';
 
$controller = JController::getInstance('jmonitoringslave');
 
// Perform the Request task
$controller->execute(JRequest::getCmd('task'));
 
// Redirect if set by the controller
$controller->redirect();
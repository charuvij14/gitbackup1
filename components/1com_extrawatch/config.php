<?php

/**
 * @file
 * ExtraWatch - A real-time ajax monitor and live stats
 * @package ExtraWatch
 * @version 2.0
 * @revision 1084
 * @license http://www.gnu.org/licenses/gpl-3.0.txt     GNU General Public License v3
 * @copyright (C) 2013 by CodeGravity.com - All rights reserved!
 * @website http://www.codegravity.com
 */

/** ensure this file is being included by a parent file */
if (!defined('_JEXEC') && !defined('_VALID_MOS')) {
    die('Restricted access');
}

/* This is the main file with basic settings */


/* LIVE SITE:
* (remove //) of the following line to override the live site setting:
*/

# define('EXTRAWATCH_LIVE_SITE','/');

define('EXTRAWATCH_VERSION', "2.0");
define('EXTRAWATCH_REVISION', "1084");

define('EXTRAWATCH_DEBUG', 0);

define('EXTRAWATCH_STATS_MAX_ROWS', '20');
define('TYPE_EXTRAWATCH_STATS_MAX_ROWS', "number");

define('EXTRAWATCH_STATS_IP_HITS', '20');
define('TYPE_EXTRAWATCH_STATS_IP_HITS', "number");

define('EXTRAWATCH_STATS_URL_HITS', '20');
define('TYPE_EXTRAWATCH_STATS_URL_HITS', "number");

define('EXTRAWATCH_IGNORE_IP', '   ');
define('TYPE_EXTRAWATCH_IGNORE_IP', "text");

define('EXTRAWATCH_IGNORE_USER', '   ');
define('TYPE_EXTRAWATCH_IGNORE_USER', "text");

define('EXTRAWATCH_UPDATE_TIME_VISITS', "2000");
define('TYPE_EXTRAWATCH_UPDATE_TIME_VISITS', "number");

define('EXTRAWATCH_UPDATE_TIME_STATS', "4000");
define('TYPE_EXTRAWATCH_UPDATE_TIME_STATS', "number");

define('EXTRAWATCH_MAXID_BOTS', 40);
define('TYPE_EXTRAWATCH_MAXID_BOTS', "number");

define('EXTRAWATCH_MAXID_VISITORS', 40);
define('TYPE_EXTRAWATCH_MAXID_VISITORS', "number");

define('EXTRAWATCH_LIMIT_BOTS', 5);
define('TYPE_EXTRAWATCH_LIMIT_BOTS', "number");

define('EXTRAWATCH_LIMIT_VISITORS', 60);
define('TYPE_EXTRAWATCH_LIMIT_VISITORS', "number");

define('EXTRAWATCH_TRUNCATE_VISITS', 60);
define('TYPE_EXTRAWATCH_TRUNCATE_VISITS', "number");

define('EXTRAWATCH_TRUNCATE_STATS', 15);
define('TYPE_EXTRAWATCH_TRUNCATE_STATS', "number");

define('EXTRAWATCH_STATS_KEEP_DAYS', 90);
define('TYPE_EXTRAWATCH_STATS_KEEP_DAYS', "number");

define('EXTRAWATCH_TIMEZONE_OFFSET', '0');
define('TYPE_EXTRAWATCH_TIMEZONE_OFFSET', "number");

define('EXTRAWATCH_WEEK_OFFSET', -0.56547619);
define('TYPE_EXTRAWATCH_WEEK_OFFSET', "number");

define('EXTRAWATCH_DAY_OFFSET', 0.0416667);
define('TYPE_EXTRAWATCH_DAY_OFFSET', "number");

define('EXTRAWATCH_FRONTEND_HIDE_LOGO', 0);
define('TYPE_EXTRAWATCH_FRONTEND_HIDE_LOGO', "checkbox");

define('EXTRAWATCH_IP_STATS', 0);
define('TYPE_EXTRAWATCH_IP_STATS', "checkbox");

define('EXTRAWATCH_HIDE_ADS', 0);
define('TYPE_EXTRAWATCH_HIDE_ADS', "checkbox");

define('EXTRAWATCH_TOOLTIP_ONCLICK', 'On');
define('TYPE_EXTRAWATCH_TOOLTIP_ONCLICK', "checkbox");

define('EXTRAWATCH_SERVER_URI_KEY', 'REDIRECT_URL');

define('TYPE_EXTRAWATCH_BLOCKING_MESSAGE', "text");

define('EXTRAWATCH_TOOLTIP_WIDTH', 1000);
define('TYPE_EXTRAWATCH_TOOLTIP_WIDTH', "number");

define('EXTRAWATCH_TOOLTIP_HEIGHT', 768);
define('TYPE_EXTRAWATCH_TOOLTIP_HEIGHT', "number");

define('EXTRAWATCH_TOOLTIP_URL', "http://www.netip.de/search?query={ip}");

define('EXTRAWATCH_IGNORE_URI', '');
define('TYPE_EXTRAWATCH_IGNORE_URI', "text");

define('EXTRAWATCH_TRUNCATE_GOALS', 20);

define('EXTRAWATCH_FRONTEND_NO_BACKLINK', 0);
define('TYPE_EXTRAWATCH_FRONTEND_NO_BACKLINK', "checkbox");

define('EXTRAWATCH_FRONTEND_NOFOLLOW', 0);
define('TYPE_EXTRAWATCH_FRONTEND_NOFOLLOW', "checkbox");

define('EXTRAWATCH_FRONTEND_COUNTRIES', 1);
define('TYPE_EXTRAWATCH_FRONTEND_COUNTRIES', "checkbox");

define('EXTRAWATCH_FRONTEND_COUNTRIES_UPPERCASE', 0);
define('TYPE_EXTRAWATCH_FRONTEND_COUNTRIES_UPPERCASE', "checkbox");

define('EXTRAWATCH_FRONTEND_COUNTRIES_NUM', 10);
define('TYPE_EXTRAWATCH_FRONTEND_COUNTRIES_NUM', "number");

define('EXTRAWATCH_FRONTEND_VISITORS', 1);
define('TYPE_EXTRAWATCH_FRONTEND_VISITORS', "checkbox");

define('EXTRAWATCH_FRONTEND_USER_LINK', '');

define('EXTRAWATCH_CACHE_FRONTEND_COUNTRIES', "300");

define('EXTRAWATCH_CACHE_FRONTEND_VISITORS', "300");

define('EXTRAWATCH_CACHE_FRONTEND_USERS', "300");

define('EXTRAWATCH_FRONTEND_VISITORS_TODAY', 1);
define('TYPE_EXTRAWATCH_FRONTEND_VISITORS_TODAY', "checkbox");

define('EXTRAWATCH_FRONTEND_VISITORS_YESTERDAY', 1);
define('TYPE_EXTRAWATCH_FRONTEND_VISITORS_YESTERDAY', "checkbox");

define('EXTRAWATCH_FRONTEND_VISITORS_THIS_WEEK', 1);
define('TYPE_EXTRAWATCH_FRONTEND_VISITORS_THIS_WEEK', "checkbox");

define('EXTRAWATCH_FRONTEND_VISITORS_LAST_WEEK', 1);
define('TYPE_EXTRAWATCH_FRONTEND_VISITORS_LAST_WEEK', "checkbox");

define('EXTRAWATCH_FRONTEND_VISITORS_THIS_MONTH', 1);
define('TYPE_EXTRAWATCH_FRONTEND_VISITORS_THIS_MONTH', "checkbox");

define('EXTRAWATCH_FRONTEND_VISITORS_LAST_MONTH', 1);
define('TYPE_EXTRAWATCH_FRONTEND_VISITORS_LAST_MONTH', "checkbox");

define('EXTRAWATCH_FRONTEND_VISITORS_TOTAL', 1);
define('TYPE_EXTRAWATCH_FRONTEND_VISITORS_TOTAL', "checkbox");

define('EXTRAWATCH_FRONTEND_COUNTRIES_FIRST', 1);
define('TYPE_EXTRAWATCH_FRONTEND_COUNTRIES_FIRST', "checkbox");

define('EXTRAWATCH_SEO_RENDER_ONLY_CHANGED', FALSE);
define('TYPE_EXTRAWATCH_SEO_RENDER_ONLY_CHANGED', "checkbox");

define('EXTRAWATCH_FRONTEND_VISITORS_TOTAL_INITIAL', 0);

define('EXTRAWATCH_LICENSE_ACCEPTED', 0);

define('EXTRAWATCH_BLOCKING_MESSAGE', "Your IP address was blocked by ExtraWatch, please contact the system administrator");

define('EXTRAWATCH_SPAMWORD_LIST', "
000 per day
000 per month
000 per week
100% free
addresses on cd
adipex
advicer
affiliate program
all natural
an invitation to
any future mailing
as seen on
auto email removal
avoid bankruptcy
baccarrat
bad credit
bankruptcy
be a millionaire
be amazed
be your own boss
beneficiary
big bucks
bill 1618
billing address
billion dollars
blackjack
bllogspot
booker
brand new pager
broadcast e-mail
bulk email
burn fat
 biz opportunity
online casino
online pharmacy
online-gambling
onlinegambling-4u
only $
orders shipped by priority mail
ottawavalleyag
outstanding values
over weight
ownsthis
palm-texas-holdem-game
paxil
penis
penis enlarge
pennies a day
perpetual
pharmacy
phentermine
poker-chip
potential earnings
poze
print form signature
print out and fax
produced and sent out
pure profit
pussy
rate quote
rates slashed
real thing
reciprocal
referring url
refinance
refinance home
removal instructions
remove in the subject line
removed from future mailing
removed in subject line
removes wrinkles
rental-car-e-site
reply remove subject
reply to this message
requires initial investment
reserves the right
retirement business
reverses aging
ringtones
risk free
roulette
round the world
safeguard notice
satisfaction guaranteed
save $
save big money
save up to
score with babes
search engine listings
second mortgage
section 301
serious cash
shemale
single deck black jack
slot-machine
special promotion
stock disclaimer statement
stop balding
stop future mailing
stop snoring
student loans
texas-holdem
this e-mail complies
thorcarlson
to be removed
top-e-site
top-site
tramadol
trim-spa
ultram
vacation giveaway
valeofglamorganconservatives
very low-cost
viagra
video poker
vioxx
weight loss
wow gold
xanax
you win
you won
you're already approved
zolus
");

define('EXTRAWATCH_SPAMWORD_BANS_ENABLED', 0);
define('TYPE_EXTRAWATCH_SPAMWORD_BANS_ENABLED', "checkbox");

define('TYPE_EXTRAWATCH_SPAMWORD_LIST', "largetext");


define('EXTRAWATCH_LANGUAGE', 'english');
define('TYPE_EXTRAWATCH_LANGUAGE', "select");

define('EXTRAWATCH_HISTORY_MAX_VALUES', 20);
define('TYPE_EXTRAWATCH_HISTORY_MAX_VALUES', "text");

define('EXTRAWATCH_HISTORY_MAX_DB_RECORDS', 200);
define('TYPE_EXTRAWATCH_HISTORY_MAX_DB_RECORDS', "text");


define('EXTRAWATCH_ONLY_LAST_URI', FALSE);
define('TYPE_EXTRAWATCH_ONLY_LAST_URI', "checkbox");

define('EXTRAWATCH_HIDE_REPETITIVE_TITLE', FALSE);
define('TYPE_EXTRAWATCH_HIDE_REPETITIVE_TITLE', "checkbox");

define('EXTRAWATCH_UNINSTALL_KEEP_DATA', FALSE);
define('TYPE_EXTRAWATCH_UNINSTALL_KEEP_DATA', "checkbox");

define('EXTRAWATCH_EMAIL_REPORTS_ENABLED', FALSE);
define('TYPE_EXTRAWATCH_EMAIL_REPORTS_ENABLED', "checkbox");

define('EXTRAWATCH_EMAIL_SEO_REPORTS_ENABLED', FALSE);
define('TYPE_EXTRAWATCH_EMAIL_SEO_REPORTS_ENABLED', "checkbox");

define('EXTRAWATCH_EMAIL_REPORTS_ADDRESS', "@");

define('EXTRAWATCH_EMAIL_PERCENT_HIGHER_THAN', 0);

define('EXTRAWATCH_EMAIL_ONE_DAY_CHANGE_POSITIVE', 0);
define('EXTRAWATCH_EMAIL_ONE_DAY_CHANGE_NEGATIVE', 0);
define('EXTRAWATCH_EMAIL_SEVEN_DAY_CHANGE_POSITIVE', 0);
define('EXTRAWATCH_EMAIL_SEVEN_DAY_CHANGE_NEGATIVE', 0);
define('EXTRAWATCH_EMAIL_TWENTY_EIGHT_DAY_CHANGE_POSITIVE', 0);
define('EXTRAWATCH_EMAIL_TWENTY_EIGHT_DAY_CHANGE_NEGATIVE', 0);

define('EXTRAWATCH_EMAIL_NAME_TRUNCATE', 40);


define('EXTRAWATCH_FRONTEND_COUNTRIES_MAX_COLUMNS', 1);
define('EXTRAWATCH_FRONTEND_COUNTRIES_MAX_ROWS', 10);

define('EXTRAWATCH_FRONTEND_COUNTRIES_NAMES', 1);
define('TYPE_EXTRAWATCH_FRONTEND_COUNTRIES_NAMES', "checkbox");

define('EXTRAWATCH_FRONTEND_COUNTRIES_FLAGS_FIRST', 0);
define('TYPE_EXTRAWATCH_FRONTEND_COUNTRIES_FLAGS_FIRST', "checkbox");

define('EXTRAWATCH_FLOW_DEFAULT_OUTGOING_LINKS_COUNT', 5);
define('EXTRAWATCH_FLOW_DEFAULT_NESTING_LEVEL', 2);


define('EW_DB_KEY_BROWSER', 1);
define('EW_DB_KEY_COUNTRY', 2);
define('EW_DB_KEY_GOALS', 3);
define('EW_DB_KEY_HITS', 4);
define('EW_DB_KEY_INTERNAL', 5);
define('EW_DB_KEY_IP', 6);
define('EW_DB_KEY_KEYWORDS', 7);
define('EW_DB_KEY_LOADS', 8);
define('EW_DB_KEY_OS', 9);
define('EW_DB_KEY_REFERERS', 10);
define('EW_DB_KEY_UNIQUE', 11);
define('EW_DB_KEY_URI', 12);
define('EW_DB_KEY_USERS', 13);
define('EW_DB_KEY_KEYPHRASE', 14);
define('EW_DB_KEY_URI2KEYPHRASE', 15);

define('EW_DB_KEY_SIZE_DB', 101);
define('EW_DB_KEY_SIZE_COM', 102);
define('EW_DB_KEY_SIZE_MOD', 103);

define('EW_DB_KEY_TRAFFIC_FLOW', 16);
define('EW_DB_KEY_SEARCH_RESULT_NUM', 17);
define('EW_DB_KEY_HEATMAP', 18);

define('EXTRAWATCH_WARNING_THRESHOLD', 20);

/* number of visit a keyphrase has to get to be considered as unimportant */
define('EXTRAWATCH_UNIMPORTANT_KEYPHRASE_THRESHOLD', 5);
define('EXTRAWATCH_DAYS_TO_KEEP_UNIMPORTANT_KEYPHRASES', 7);

define('EXTRAWATCH_MAP_OPENMAP', 1);
// not using google map by default
define('EXTRAWATCH_MAP_GOOGLEMAP', 0);

$keysArray = array('goals', 'referers', 'internal', 'keyphrase', 'keywords', 'uri', 'users', 'country', 'ip', 'browser', 'os');

//upgrade.xml path
define('TEMP_EXTRAWATCH_UPDATE_FILE_URL', "http://www.codegravity.com/update/extrawatch/update.xml");

//upgrade <script> files path
define('TEMP_EXTRAWATCH_SCRIPT_POSTION', "http://localhost/joomla/upgrade/extrawatch/script/");

//zip file path including 4 files:com_extrawatch.zip,mod_EXTRAWATCH_agent.zip,mod_EXTRAWATCH_users.zip and mod_EXTRAWATCH_visitors.zip
define('TEMP_EXTRAWATCH_LASTESTZIP_POSTION', "http://www.codegravity.com/update/extrawatch/");

define('EXTRAWATCH_UNKNOWN_COUNTRY', "xx");

define('EXTRAWATCH_TABLES_TO_OPTIMIZE',
serialize(array(
        "#__extrawatch_config",
        "#__extrawatch_flow",
        "#__extrawatch_goals",
        "#__extrawatch_heatmap",
        "#__extrawatch_history",
        "#__extrawatch_info",
        "#__extrawatch_internal",
        "#__extrawatch_ip2c",
        "#__extrawatch_keyphrase",
        "#__extrawatch_uri",
        "#__extrawatch_uri2keyphrase",
        "#__extrawatch_uri2title",
        "#__extrawatch_uri_history",
        "#__extrawatch_uri_post")
));

define('EXTRAWATCH_TABLES_TO_TRUNCATE',
serialize(array(
        "#__extrawatch",
        "#__extrawatch_blocked",
        "#__extrawatch_cache",
        "#__extrawatch_flow",
        "#__extrawatch_goals",
        "#__extrawatch_heatmap",
        "#__extrawatch_history",
        "#__extrawatch_info",
        "#__extrawatch_internal",
        "#__extrawatch_keyphrase",
        "#__extrawatch_uri",
        "#__extrawatch_uri2keyphrase",
        "#__extrawatch_uri2keyphrase_pos",
        "#__extrawatch_uri2title",
        "#__extrawatch_uri_history",
        "#__extrawatch_uri_post",
    )
));

/* defines explicitely checkboxes */
define('EXTRAWATCH_CHECKBOX_NAMES_ARRAY',
serialize(array(
    'EXTRAWATCH_FRONTEND_NO_BACKLINK',
    'EXTRAWATCH_FRONTEND_NOFOLLOW',
    'EXTRAWATCH_FRONTEND_HIDE_LOGO',
    'EXTRAWATCH_IP_STATS',
    'EXTRAWATCH_FRONTEND_NO_BACKLINK',
    'EXTRAWATCH_FRONTEND_COUNTRIES',
    'EXTRAWATCH_FRONTEND_COUNTRIES_UPPERCASE',
    'EXTRAWATCH_FRONTEND_COUNTRIES_FIRST',
    'EXTRAWATCH_FRONTEND_VISITORS',
    'EXTRAWATCH_FRONTEND_VISITORS_TODAY',
    'EXTRAWATCH_FRONTEND_VISITORS_YESTERDAY',
    'EXTRAWATCH_FRONTEND_VISITORS_THIS_WEEK',
    'EXTRAWATCH_FRONTEND_VISITORS_LAST_WEEK',
    'EXTRAWATCH_FRONTEND_VISITORS_THIS_MONTH',
    'EXTRAWATCH_FRONTEND_VISITORS_LAST_MONTH',
    'EXTRAWATCH_FRONTEND_VISITORS_TOTAL',
    'EXTRAWATCH_TOOLTIP_ONCLICK',
    'EXTRAWATCH_ONLY_LAST_URI',
    'EXTRAWATCH_HIDE_REPETITIVE_TITLE',
    'EXTRAWATCH_UNINSTALL_KEEP_DATA',
    'EXTRAWATCH_FRONTEND_COUNTRIES_NAMES',
    'EXTRAWATCH_FRONTEND_COUNTRIES_FLAGS_FIRST',
    'EXTRAWATCH_EMAIL_REPORTS_ENABLED',
    'EXTRAWATCH_EMAIL_SEO_REPORTS_ENABLED'
)));

/* version 1.2.18 */
define('EXTRAWATCH_HEATMAP_KEEP_DAYS', 7);

define('EXTRAWATCH_GOALS_ALLOWED_FIELDS',
    serialize(
        array(1=>"NAME","USERNAME_INVERSED","URI_CONDITION","URI_INVERSED","GET_VAR","GET_CONDITION","GET_INVERSED","POST_VAR","POST_CONDITION","POST_INVERSED","TITLE_CONDITION","TITLE_INVERSED","USERNAME_CONDITION","IP_CONDITION","IP_INVERSED","CAME_FROM_CONDITION","CAME_FROM_INVERSED","COUNTRY_CONDITION","COUNTRY_INVERSED","BLOCK","REDIRECT")
    )
);



<?php

/**
 * @file
 * ExtraWatch - A real-time ajax monitor and live stats
 * @package ExtraWatch
 * @version @VERSION@
 * @revision @REVISION@
 * @license http://www.gnu.org/licenses/gpl-3.0.txt     GNU General Public License v3
 * @copyright (C) @YEAR@ by CodeGravity.com - All rights reserved!
 * @website http://www.codegravity.com
 */

defined('_JEXEC') or die('Restricted access');

//ExtraWatch language file - to create a new language file, just copy the english.php to eg. german.php and place into./components/com_extrawatch/lang/

// Main Menu.
DEFINE('_EW_MENU_STATS', "����������");
DEFINE('_EW_MENU_GOALS', "������������");
DEFINE('_EW_MENU_SETTINGS', "���������");
DEFINE('_EW_MENU_CREDITS', "������");
DEFINE('_EW_MENU_FAQ', "FAQ");
DEFINE('_EW_MENU_DOCUMENTATION', "������������");
DEFINE('_EW_MENU_LICENSE', "˳�����");
DEFINE('_EW_MENU_DONATORS', "��������");
DEFINE('_EW_MENU_SUPPORT', "��� ��������� ���������� ����� � ������� �������� ��� ExtraWatch ))).");

// Left visitors real-time window.
DEFINE('_EW_VISITS_VISITORS', "������ ���������");
DEFINE('_EW_VISITS_BOTS', "����");
DEFINE('_EW_VISITS_CAME_FROM', "�������� �...");
DEFINE('_EW_VISITS_MODULE_NOT_PUBLISHED', "��� ExtraWatch ������ �� �����������! ���� ���������� �������� �� ����. ��� ���������, ������ � �������� ��������� �������� � ���������� ������ ��� ��� �������, �� �� ������ ������ ����������");
DEFINE('_EW_VISITS_PANE_LOADING', "����������� ����������...");

// Right stats window.
DEFINE('_EW_STATS_TITLE', "���������� ��������� �� �������");
DEFINE('_EW_STATS_WEEK', "�������");
DEFINE('_EW_STATS_THIS_WEEK', "��� �������");
DEFINE('_EW_STATS_UNIQUE', "���������");
DEFINE('_EW_STATS_LOADS', "�����������");
DEFINE('_EW_STATS_HITS', "ճ��");
DEFINE('_EW_STATS_TODAY', "�������");
DEFINE('_EW_STATS_FOR', "��");
DEFINE('_EW_STATS_ALL_TIME', "�� ���� ���");
DEFINE('_EW_STATS_EXPAND', "����������");
DEFINE('_EW_STATS_COLLAPSE', "��������");
DEFINE('_EW_STATS_URI', "�������");
DEFINE('_EW_STATS_COUNTRY', "�����");
DEFINE('_EW_STATS_USERS', "�����������");
DEFINE('_EW_STATS_REFERERS', "��������");
DEFINE('_EW_STATS_IP', "IP ������");
DEFINE('_EW_STATS_BROWSER', "��������");
DEFINE('_EW_STATS_OS', "��");
DEFINE('_EW_STATS_KEYWORDS', "������ �����");
DEFINE('_EW_STATS_GOALS', "������������");
DEFINE('_EW_STATS_TOTAL', "������");
DEFINE('_EW_STATS_DAILY', "�� ����");
DEFINE('_EW_STATS_DAILY_TITLE', "���������� �� ����");
DEFINE('_EW_STATS_ALL_TIME_TITLE', "���������� �� ���� ���");
DEFINE('_EW_STATS_LOADING', "������������ ...");
DEFINE('_EW_STATS_LOADING_WAIT', "������������ ... ���������");
DEFINE('_EW_STATS_IP_BLOCKING_TITLE', "IP ����������");
DEFINE('_EW_STATS_IP_BLOCKING_ENTER', "������ IP ������");
DEFINE('_EW_STATS_IP_BLOCKING_MANUALLY', "������ IP ���� ������ �����������. (��������� 217.242.11.54 ��� 217 .* ��� 217.242 .* ��� ���������� ��� IP ������� �� ��������)");
DEFINE('_EW_STATS_IP_BLOCKING_TOGGLE', "�������� �����������");
DEFINE('_EW_STATS_PANE_LOADING', "����������� ���������� ...");

// Settings.
DEFINE('_EW_SETTINGS_TITLE', "�����");
DEFINE('_EW_SETTINGS_DEFAULT', "�� �������������");
DEFINE('_EW_SETTINGS_SAVE', "��������");
DEFINE('_EW_SETTINGS_APPEARANCE', "������� ������");
DEFINE('_EW_SETTINGS_FRONTEND', "Frontend");
DEFINE('_EW_SETTINGS_HISTORY_PERFORMANCE', "������ �� �������������");
DEFINE('_EW_SETTINGS_ADVANCED', "���������");
DEFINE('_EW_SETTINGS_IGNORE', "����������");
DEFINE('_EW_SETTINGS_BLOCKING', "����������");
DEFINE('_EW_SETTINGS_EXPERT', "�������");
DEFINE('_EW_SETTINGS_RESET_CONFIRM', "�� ����� ������ ������ ��� ����������?");
DEFINE('_EW_SETTINGS_RESET_ALL', "������ ���");
DEFINE('_EW_SETTINGS_RESET_ALL_LINK', "������ ��� ���������� � ��� ����������");
DEFINE('_EW_SETTINGS_LANGUAGE', "����");
DEFINE('_EW_SETTINGS_SAVED', "����� ���� ��������");
DEFINE('_EW_SETTINGS_ADD_YOUR_IP', "������ ������ ��� IP");
DEFINE('_EW_SETTINGS_TO_THE_LIST', "�� ������.");

// Other / mostly general.
DEFINE('_EW_TITLE', "AJAX ������ joomla � ��������� ���");
DEFINE('_EW_BACK', "�����");
DEFINE('_EW_ACCESS_DENIED', "������ ����������!");
DEFINE('_EW_LICENSE_AGREE', "� ������ � ���� ���������, ���������� ����");
DEFINE('_EW_LICENSE_CONTINUE', "����������");
DEFINE('_EW_SUCCESS', "�������� ������� ������");
DEFINE('_EW_RESET_SUCCESS', "��� ���������� ������ �������");
DEFINE('_EW_RESET_ERROR', "������� � �������� �����");
DEFINE('_EW_CREDITS_TITLE', "������");
DEFINE('_EW_TRENDS_DAILY_WEEKLY', "������� �� ��������� ���������� ��...");
DEFINE('_EW_AJAX_PERMISSION_DENIED_1', "AJAX ������ �����������: ���� ����� ������������ ���������� � ������, ��������� � configuration.php joomla -");
DEFINE('_EW_AJAX_PERMISSION_DENIED_2', "���� �� ������ ������� www. �� ������� ����� ��������� ����. ��� javascript ���������� �������� ������ ��");
DEFINE('_EW_AJAX_PERMISSION_DENIED_3', "�");
DEFINE('_EW_AJAX_PERMISSION_DENIED_4', "�� ����� ���� ������ �� �� ����� �����.");

// Header.
DEFINE('_EW_HEADER_DOWNLOAD', "������� ������ ��������� �");
DEFINE('_EW_HEADER_CAST_YOUR', "³���� ���");
DEFINE('_EW_HEADER_VOTE', "�����");

// Tooltips.
DEFINE('_EW_TOOLTIP_CLICK', "������ ��� ��������� ���� ��������� ����������");
DEFINE('_EW_TOOLTIP_MOUSE_OVER', "������� ����� ��� ��������� ���� ��������� ����������");
DEFINE('_EW_TOOLTIP_YESTERDAY_INCREASE', "������� ���������");
DEFINE('_EW_TOOLTIP_HELP', "³������ ������� ��-���� �������� ���");
DEFINE('_EW_TOOLTIP_WINDOW_CLOSE', "������� �� ����");
DEFINE('_EW_TOOLTIP_PRINT', "������������");

// Goals.
DEFINE('_EW_GOALS_INSERT', "�������� ���� ������������");
DEFINE('_EW_GOALS_UPDATE', "�������� no. ������������");
DEFINE('_EW_GOALS_ACTION', "ĳ�");
DEFINE('_EW_GOALS_TITLE', "���� ������������");
DEFINE('_EW_GOALS_NEW', "���� ������������");
DEFINE('_EW_GOALS_RELOAD', "���������������");
DEFINE('_EW_GOALS_ADVANCED', "���������");
DEFINE('_EW_GOALS_NAME', "��'�");
DEFINE('_EW_GOALS_ID', "id");
DEFINE('_EW_GOALS_URI_CONDITION', "URI �����");
DEFINE('_EW_GOALS_GET_VAR', "GET �����");
DEFINE('_EW_GOALS_GET_CONDITION', "GET �����");
DEFINE('_EW_GOALS_POST_VAR', "POST �����");
DEFINE('_EW_GOALS_POST_CONDITION', "POST �����");
DEFINE('_EW_GOALS_TITLE_CONDITION', "Title �����");
DEFINE('_EW_GOALS_USERNAME_CONDITION', "Username �����");
DEFINE('_EW_GOALS_IP_CONDITION', "IP �����");
DEFINE('_EW_GOALS_CAME_FROM_CONDITION', "�������� � �����");
DEFINE('_EW_GOALS_BLOCK', "����������");
DEFINE('_EW_GOALS_REDIRECT', "������������ �� URL");
DEFINE('_EW_GOALS_HITS', "ճ��");
DEFINE('_EW_GOALS_ENABLED', "��������");
DEFINE('_EW_GOALS_EDIT', "����������");
DEFINE('_EW_GOALS_DELETE', "��������");
DEFINE('_EW_GOALS_DELETE_CONFIRM', "�� �������� ��� ������� ���������� ��� ����� ������������. �� ������ ������ �������� no ����� ������������.");

// Frontend.
DEFINE('_EW_FRONTEND_COUNTRIES', "�����");
DEFINE('_EW_FRONTEND_VISITORS', "³��������");
DEFINE('_EW_FRONTEND_TODAY', "�������");
DEFINE('_EW_FRONTEND_YESTERDAY', "�����");
DEFINE('_EW_FRONTEND_THIS_WEEK', "����� �����");
DEFINE('_EW_FRONTEND_LAST_WEEK', "�������� �����");
DEFINE('_EW_FRONTEND_THIS_MONTH', "����� �����");
DEFINE('_EW_FRONTEND_LAST_MONTH', "�������� �����");
DEFINE('_EW_FRONTEND_TOTAL', "������");

// Settings description - quite long.
DEFINE('_EW_DESC_DEBUG', "ExtraWatch � ����� ������������. ����� ����� ����� ������ ������� �������.��� ��������, ����� ����� EXTRAWATCH_DEBUG � / components / com_extrawatch / config.php � 1 �� 0");
DEFINE('_EW_DESC_STATS_MAX_ROWS', "����������� ������� ����� ���������� � ���������� �����.");
DEFINE('_EW_DESC_STATS_IP_HITS', "�� IP ������, � ���� ����� ���� � �������� ��, �� �� ��������, ������ ������� � IP �����.");
DEFINE('_EW_DESC_STATS_URL_HITS', "�� URL, � ���� ����� ���� � �������� �� �� �� ��������, ������ ������� � ����� URL.");
DEFINE('_EW_DESC_IGNORE_IP', "����� ��������� ������ IP � ����������. ����� ������ ������� ���������� � ������ �����. � ��� ��������� ������ ��������������� ������ �������. ��������� 192 .* �������� 192.168.51.31, 192.168.16.2, � �.�.");
DEFINE('_EW_DESC_UPDATE_TIME_VISITS', "��� ��������� ���������� �� ���������� � ����������, �� ������������� - 2000, ������ ������� � ��� ���������. ϳ��� ���� ������� ������� ������� ExtraWatch.");
DEFINE('_EW_DESC_UPDATE_TIME_STATS', "��� ��������� �������� ���������� � ����������, �� ������������� - 4000, ������ ������� � ��� ���������. ϳ��� ���� ������� ������� ������� ExtraWatch.");
DEFINE('_EW_DESC_MAXID_BOTS', "������ ��������� ���� �������� � ��� �����.");
DEFINE('_EW_DESC_MAXID_VISITORS', "������ �������� ��������� �������� � ��� �����.");
DEFINE('_EW_DESC_LIMIT_BOTS', "������ �������� ��������� ������ ������ � ������.");
DEFINE('_EW_DESC_LIMIT_VISITORS', "������ ���� ������ ������ � ������.");
DEFINE('_EW_DESC_TRUNCATE_VISITS', "����������� ������� �����, �� ������������� � ���������� �� URL.");
DEFINE('_EW_DESC_TRUNCATE_STATS', "����������� ������� �����, �� �������������� ����� ����� ����������.");
DEFINE('_EW_DESC_STATS_KEEP_DAYS', "��� �������� ���������� � ��� �����, 0 = ����������.");
DEFINE('_EW_DESC_TIMEZONE_OFFSET', "���� �� ���������� � ����� ������� �����, �� ��� �������-������. (��������� ��� ��������� ��������)");
DEFINE('_EW_DESC_WEEK_OFFSET', "����������� �����, ���������� ������ / (3600 * 24 * 7) ���������� � ����� 1.1.1970, �� ����������� ������ ��� ���� ��� ������ � ��������.");
DEFINE('_EW_DESC_DAY_OFFSET', "����������� ���, ���������� ������ / (3600 * 24 * 7) ���������� � ��� 1.1.1970, �� ����������� ������ ��� ���� ��� ������ � 00:00.");
DEFINE('_EW_DESC_FRONTEND_HIDE_LOGO', "��������������� ������� ������ 1x1px icon � ���������� ������ ������ ExtraWatch.");
DEFINE('_EW_DESC_IP_STATS', "��� ��������� ���������� IP �����. � ������ ������ ������� ��������� IP ����� � ����� ����� ���������� �������. ������������� �� ��� ����� � �����.");
DEFINE('_EW_DESC_HIDE_ADS', "��� �������� ������� ��������� ���� � ��������, �����i�� ���� �� ��� ����� �����. ��� ��������� ���� ���������, �� ��������� ���������� ���������� ��������. ������ ������, �� ������� �� ���������.");
DEFINE('_EW_DESC_TOOLTIP_ONCLICK', "����� �� �������� ���� ������ ��� ���.���� ���� ��������� �� ���������� �����, ��� �������� �������� ��� ������� ���� �����.");
DEFINE('_EW_DESC_SERVER_URI_KEY', "�� ������������� �� 'REDIRECT_URL', �� ���������, ���� �� ������������� ���������� URL (sef), ���� ���� ������ �� 'SCRIPT_URL' ���� ��������������� ��� ����� ��� index.php");
DEFINE('_EW_DESC_BLOCKING_MESSAGE', "����������� ��� �������� ����������, ����� �� ����������� �� ��������� ExtraWatch.");
DEFINE('_EW_DESC_TOOLTIP_WIDTH', "������ popup ���� � ���.����");
DEFINE('_EW_DESC_TOOLTIP_HEIGHT', "������ popup ���� � ���.����");
DEFINE('_EW_DESC_TOOLTIP_URL', "�� ������ ������ URL Whois �������� ���, ��� ���������� ip ����������. (Ip) ���������� �� ip ��������� ��� ��������� �� ������ ��������� ���������� . ��������� http://somewebsite.com/query?iplookup = (ip ) �� ������������� �� http://www.netip.de/search?query = (ip) ");
DEFINE('_EW_DESC_IGNORE_URI', "�� ������ ������� ��� URI ���� ���� ������������ � ����������. �� ������ ��������������� ����� ���������� (* �?). ���������.: / Freel? N * ��� / guestbook, ��� / ����������������������i�_dir / do_not_log_this.html, ��� �����. php ��� ���������. php ����� ");
DEFINE('_EW_DESC_GOALS_NAME', "������������ ������������. ���� ���� ������������ � ����������.");
DEFINE('_EW_DESC_GOALS_URI_CONDITION', "��� �� ���� ������ ��������� ����. ��� http://www.codegravity.com/projects/ URI ��: / projects / (�������: <b> / projects * </b>)");
DEFINE('_EW_DESC_GOALS_GET_VAR', "GET ������ �� �����, ��� �� ������ �������� ������ � URL ���� ? ��� & �����. ��������� http: / / www.codegravity.com / index.php? <u> Name </u> = peter & <u> surname </u> = smith. �� ����� ������ ��������������� <u> * </u> � ����� ���, ��� ����������� �� ������� ��������. (���������: <b> n * me </b>) ");
DEFINE('_EW_DESC_GOALS_GET_CONDITION', "��� �� ������ ��������� ����i��� ���������� ��� �������� � ������������ ����. (���������: <b> p? T * r </b>)");
DEFINE('_EW_DESC_GOALS_POST_VAR', "���� �����, ��� �� ���������� �������� ������� � ����. ���, ���� � ��� � ����� �� ������ ���-����, � ����� � ���� &lt;input type = 'text' name = '<u> experiences </u> '/&gt;. (���������: <b> exper * ces </b>)");
DEFINE('_EW_DESC_GOALS_POST_CONDITION', "³��������� �������� � ����� POST ����. ��������� �� ������ ���������, �� � � ����������� ��䳿 java. (���������: <b> * java * </b>)");
DEFINE('_EW_DESC_GOALS_TITLE_CONDITION', "��������� �������, ��� ������� ���������. (�������: <b> * freelance programmers * </b>)");
DEFINE('_EW_DESC_GOALS_USERNAME_CONDITION', "��'� �����������, ���� �����i�����. (�������: <b> psmith * </b>)");
DEFINE('_EW_DESC_GOALS_IP_CONDITION', "IP � ����� �������� ����������: (�������: <b> 201.9 ?.*.*</b>)");
DEFINE('_EW_DESC_GOALS_CAME_FROM_CONDITION', "������ URL � ����� �������� ����������. (�������: <b> * www.google .* </b>)");
DEFINE('_EW_DESC_GOALS_REDIRECT', "���������� ������������ �� URL �������� ����. �� ���� ������� �������� �� '����������': (�������: <b> http://www.codegravity.com/goaway.html </b>)");
DEFINE('_EW_DESC_TRUNCATE_GOALS', "How many chars to truncate in goals table");
DEFINE('_EW_DESC_FRONTEND_NO_BACKLINK', "��������� �� codegravity.com, ������ �� ��������, ��� �� � ��������� ��� �� ��� ��������. ������i");
DEFINE('_EW_DESC_FRONTEND_COUNTRIES', "³���������� ������� � ����������� ������������ ���� ���������� � ��������i. ���� ������, �������� �������� ����� ���� ���� ����������� � CACHE_FRONTEND_");
DEFINE('_EW_DESC_FRONTEND_COUNTRIES_FIRST', "���� ������ ������ ��������� ����� ³�������� / ����� � ����� ��������. ����� ��������, � ³�������� ������ ������� �������.");
DEFINE('_EW_DESC_FRONTEND_COUNTRIES_NUM', "ʳ������ ���� �� ������������� � ��������");
DEFINE('_EW_DESC_FRONTEND_VISITORS', "³���������� ������� ���������� � ��������i �� ����, ������ � ������. ���� ������, �������� �������� ����� ���� ���� ����������� � CACHE_FRONTEND_");
DEFINE('_EW_DESC_CACHE_FRONTEND_COUNTRIES', "��� ����� ���� � �������� ��� ���� ���������� ������������� ������� ��������");
DEFINE('_EW_DESC_CACHE_FRONTEND_VISITORS', "��� ����� ���� � �������� ��� ���������� ������������� ������� ��������");
DEFINE('_EW_DESC_FRONTEND_VISITORS_TODAY', "³���������� ������� ���������� � ��������� ����� ���: �������. ���� ������, �������� �������� ����� ���� ���� ����������� � CACHE_FRONTEND_ ...");
DEFINE('_EW_DESC_FRONTEND_VISITORS_YESTERDAY', "³���������� ������� ���������� � �������� ����� ���: �����. ���� ������, �������� �������� ����� ���� ���� ����������� � CACHE_FRONTEND_ ..");
DEFINE('_EW_DESC_FRONTEND_VISITORS_THIS_WEEK', "³���������� ������� ���������� � �������� ����� ���: �� ����� ����. ���� ������, �������� �������� ����� ���� ���� ����������� � CACHE_FRONTEND_ ...");
DEFINE('_EW_DESC_FRONTEND_VISITORS_LAST_WEEK', "³���������� ������� ���������� � �������� ����� ���: �������� �����. ���� ������, �������� �������� ����� ���� ���� ����������� � CACHE_FRONTEND_ ...");
DEFINE('_EW_DESC_FRONTEND_VISITORS_THIS_MONTH', "³���������� ������� ���������� � �������� ����� ���: � ����� �����. ���� ������, �������� �������� ����� ���� ���� ����������� � CACHE_FRONTEND_ ...");
DEFINE('_EW_DESC_FRONTEND_VISITORS_LAST_MONTH', "³���������� ������� ���������� � �������� ����� ���: �������� �����. ���� ������, �������� �������� ����� ���� ���� ����������� � CACHE_FRONTEND_ ...");
DEFINE('_EW_DESC_FRONTEND_VISITORS_TOTAL', "³���������� ������� ���������� � �������� ����� ���: ������. ���� ������, �������� �������� ����� ���� ���� ����������� � CACHE_FRONTEND_ ...");
DEFINE('_EW_DESC_LANGUAGE', "������������ ������ ����. ���� ����������� / components / com_extrawatch / lang / ������ ������ ���� � ������������� � ��������� ��������� ...");
DEFINE('_EW_DESC_GOALS', "������������ ���������� ��� ��������� ��������� ���������. ���� �� ��������� �����, �������� ����������� ���� ������������. ����� ����� �� ������ ������������, �� ������ ���������� �������� URL, �������� ����� ��������, �� ����� ��'� ����������� ��� �������� � ����� ������. �� ������ ����� ����������� ��� �������������� ����� ������������ �� ����-���� ����� URL. ");
DEFINE('_EW_DESC_GOALS_INSERT', "� ��� �������� ���� ����� �� ������ ��������������� * �� ? �� ����� ����������. ���������: ? Ear (���� ���������: near, tear, ..), p * r (���� ���������: pr, peer, pear ..)");
DEFINE('_EW_DESC_GOALS_BLOCK', "���������� � 1, ���� �� ������, ��� �������� ��� ������������. ³� �� ���� ������ ���� ������� �����, ����� �����������, �� �� ��� ������������ - ��� ����-��� �������������, � ���� IP ������� �� '���������' ����������� ( ���������: <b> 1 </b>) ");

/* New translations */
DEFINE('_EW_GOALS_COUNTRY_CONDITION', "����� �����");
DEFINE('_EW_DESC_GOALS_COUNTRY_CONDITION', "2-��������� ��� ����� �������� �i������ (Eg: <b> TH </b>)");
DEFINE('_EW_STATS_INTERNAL', "�����i��i�");
DEFINE('_EW_STATS_FROM', "��");
DEFINE('_EW_STATS_TO', "��");
DEFINE('_EW_STATS_ADD_TO_GOALS', "������ �i��");
DEFINE('_EW_VISITS_ADD_GOAL_COUNTRY', "������ �i�� �� �i�� �����");
DEFINE('_EW_MENU_REPORT_BUG', "���i������ ��� ������� �� ���������i");
DEFINE('_EW_GOALS_COUNTRY', "�����");

/* Translations 1.2.8b_12 */
DEFINE('_EW_DESC_FRONTEND_COUNTRIES_UPPERCASE', "���� �� ������ ��� ����� ���� ���� �������� �i������ � ��������i (�������: �i�������, ��������� �����i����� ���i��� �i�������, ��������� K����i�����)");
DEFINE('_EW_DESC_CACHE_FRONTEND_USERS', "��� � �������� ��� ������� ���i��� ����������i� � ��������i");
DEFINE('_EW_DESC_FRONTEND_VISITORS_TOTAL_INITIAL', "�������� ��������, �������i � ������: � ��������i. ������� ���� �� ���������� � i���� �����i� ����������. (�������: 20000). ������i���� �� �������� 0 ���� �� �� ������������� �� ������i���.");
DEFINE('_EW_DESC_IGNORE_USER', "I��������� ����������i� ��i ������ � ����� �����i. �� ������ � �����. (�������: � (�����) mark_ *)");
DEFINE('_EW_FRONTEND_USERS_MOST', "������� ������ ����������� ������� �� �������� �������");
DEFINE('_EW_DESC_SPAMWORD_BANS_ENABLED', "�������� �������� �� ����� ��� � ������������������ ������ �������");
DEFINE('_EW_DESC_SPAMWORD_LIST', "������� ����� ������ ����-������ ����-�����. �� ������ ��������������� ����� �i��������� ���, (�������: ph? Rmac *). ���� ������������, ��� ������� ����, ��������, ExtraWatch �����i���� �� ��������� �������� ����� ( HTTP POST �����) �� ������ ���-����i � ���i������ ���� ���� �������. (�������������, ���� ����� ��������� ����� Joomla-���� - �����, ��������, ��� � ������ ���������� ������� ��� ���������� ����-����, �� ����������� ��������� �� ������ �����) ");
DEFINE('_EW_SETTINGS_ANTI_SPAM', "Anti-Spam");
DEFINE('_EW_DESC_FRONTEND_USER_LINK', "A link in the frontend Users module - allows you to specify an URL, which is open when the user clicks the user name. Must contain the string {user}, which will be replaced by the actual user name. (Eg. index.php?option=com_comprofiler&task=userProfile&user={user}) ");

/* translations 1.2.11b */
DEFINE('_EW_STATS_KEYPHRASE', "Keyphrases");
DEFINE('_EW_DESC_HISTORY_MAX_VALUES', "Maximum values in history tab (Example: <i>100</i>)");

DEFINE('_EW_DESC_ONLY_LAST_URI', "In visits show only last page visited, not all");
DEFINE('_EW_DESC_HIDE_REPETITIVE_TITLE', "In visits hide repetitive sitename in visited page title");
DEFINE('_EW_DESC_HISTORY_MAX_DB_RECORDS', "Maximum nuber of visitors to keep in database for Visit History. Be careful with this setting, if you have high traffic, it can grow really fast. Always check how much data the history table contains in Status");
DEFINE('_EW_DESC_UNINSTALL_KEEP_DATA', "Keep Database Tables on uninstall. Check this option before uninstall if you are doing an upgrade and want to keep your data.");

/* email reports */
DEFINE('_EW_DESC_EMAIL_REPORTS_ENABLED', "You'll receive nightly emails with reports for previous day, which you can read in the morning");
DEFINE('_EW_DESC_EMAIL_REPORTS_ADDRESS', "Email address to which you'll receive these reports");
DEFINE('_EW_DESC_EMAIL_PERCENT_HIGHER_THAN', "Only include rows in email reports where percentage is higher than {value}. Set to 0 if you don't want to use this feature <i>(example: 5)</i>");
DEFINE('_EW_DESC_EMAIL_ONE_DAY_CHANGE_POSITIVE', "Only include <b>positive one day</b> change values in email reports higher than {value} percent. Set to 0 if you don't want to use this feature <i>(example: 5)</i>");
DEFINE('_EW_DESC_EMAIL_ONE_DAY_CHANGE_NEGATIVE', "Only include <b>negative one day</b> change values in email reports lower than {value} percent. Set to 0 if you don't want to use this feature <i>(example: -10)</i>");
DEFINE('_EW_DESC_EMAIL_SEVEN_DAY_CHANGE_POSITIVE', "Only include <b>positive seven day</b> change values in email reports higher than {value} percent. Set to 0 if you don't want to use this feature <i>(example: 2)</i>");
DEFINE('_EW_DESC_EMAIL_SEVEN_DAY_CHANGE_NEGATIVE', "Only include <b>negative seven day</b> change values in email reports lower than {value} percent. Set to 0 if you don't want to use this feature <i>(example: -13)</i>");
DEFINE('_EW_DESC_EMAIL_TWENTY_EIGHT_DAY_CHANGE_POSITIVE', "Only include <b>positive thirty day</b> change values in email reports higher than {value} percent. Set to 0 if you don't want to use this feature <i>(example: 2)</i>");
DEFINE('_EW_DESC_EMAIL_TWENTY_EIGHT_DAY_CHANGE_NEGATIVE', "Only include <b>negative thirty day</b> change values in email reports lower than {value} percent. Set to 0 if you don't want to use this feature <i>(example: -13)</i>");

DEFINE('_EW_DESC_FRONTEND_NOFOLLOW', "<b>(functional in PRO version)</b> Enable this setting if you want to make the logo link rendered with attribute rel='nofollow' ");
DEFINE('_EW_DESC_EMAIL_NAME_TRUNCATE', "Maximum characters of email row name. Change this if your email client message window is too small");

DEFINE('_EW_MENU_HISTORY', "History");
DEFINE('_EW_MENU_EMAILS', "Emails");
DEFINE('_EW_MENU_STATUS', "Status");
DEFINE('_EW_DESC_BLOCKED', "These IPs were blocked by anti-spam");


DEFINE('_EW_HISTORY_VISITORS', "Visitors History");
DEFINE('_EW_HISTORY_SHOWING_ONLY', "Showing only %d last records.
                To change this value, go to Settings -&gt; History &amp; Performance -&gt; HISTORY_MAX_DB_RECORDS . Be careful, this setting affects load times of the data below.  ");
DEFINE('_EW_MENU_BUG', "Report Bug");
DEFINE('_EW_MENU_FEATURE', "Request Feature");

DEFINE('_EW_VISITS_CAME_FROM_KEYWORDS', "Keywords");

DEFINE('_EW_BLOCKING_UNBLOCK', "unblock");
DEFINE('_EW_STATS_KEYPHRASE ', "Keyphrase");
DEFINE('_EW_STATUS_DATABASE', "Database status");

DEFINE('_EW_STATUS_DATABASE_TABLE_NAME', "table name");
DEFINE('_EW_STATUS_DATABASE_ROWS', "rows");
DEFINE('_EW_STATUS_DATABASE_DATA', "data");
DEFINE('_EW_STATUS_DATABASE_TOTAL', "total");

DEFINE('_EW_EMAIL_REPORTS', "Email Reports");
DEFINE('_EW_EMAIL_REPORT_GENERATED', "Generated filtered email report from yesterday");
DEFINE('_EW_EMAIL_REPORTS_VALUE_FILTERS', "Email Value Filters");
DEFINE('_EW_EMAIL_REPORTS_VALUE', "value");
DEFINE('_EW_EMAIL_REPORTS_PERCENT', "percent");
DEFINE('_EW_EMAIL_REPORTS_1DAY_CHANGE', "1-day change");
DEFINE('_EW_EMAIL_REPORTS_7DAY_CHANGE', "7-day change");
DEFINE('_EW_EMAIL_REPORTS_28DAY_CHANGE', "28-day change");
DEFINE('_EW_ANTISPAM_BLOCKED', "ExtraWatch has blocked %d spammer hits today, total: %d");
DEFINE('_EW_ANTISPAM_ADDRESSES', "Blocked IP Adresses");
DEFINE('_EW_ANTISPAM_SETTINGS', "Anti-Spam Settings");
DEFINE('_EW_TRAFFIC_AJAX', "AJAX updates traffic");


DEFINE('_EW_HISTORY_PREVIOUS', "previous");
DEFINE('_EW_HISTORY_NEXT', "next");

/** additional translation for 1.2.11 for countries in more rows */
DEFINE('_EW_DESC_FRONTEND_COUNTRIES_MAX_COLUMNS', "Number of columns of countries");
DEFINE('_EW_DESC_FRONTEND_COUNTRIES_MAX_ROWS', "Number of rows of countries");
DEFINE('_EW_DESC_FRONTEND_COUNTRIES_NAMES', "Display country names or not");
DEFINE('_EW_DESC_FRONTEND_COUNTRIES_FLAGS_FIRST', "Display flags first, then percents");

/* ExtraWatch 1.2.14 translations */

DEFINE('_EW_GOALS_GET_INVERSED', "GET inversed condition");
DEFINE('_EW_GOALS_POST_INVERSED', "POST inversed condition");
DEFINE('_EW_GOALS_TITLE_INVERSED', "Title inversed condition");
DEFINE('_EW_GOALS_USERNAME_INVERSED', "Username inversed condition");
DEFINE('_EW_GOALS_CAME_FROM_INVERSED', "Came from inversed condition");

DEFINE('_EW_STATS_MAP', "Last Visit Map");
DEFINE('_EW_STATS_MAP_ENTER_KEY', "Please enter <a href='http://www.ipinfodb.com/register.php' target='_blank'>ipinfodb.com</a> key to display last visit map:");
DEFINE('_EW_STATS_MAP_STORE_KEY', "store key");
DEFINE('_EW_STATS_MAP_INVALID_KEY', "Please enter valid ipinfodb key you obtained from: <a href='http://www.ipinfodb.com/register.php' target='_blank'>ipinfodb.com</a>");

DEFINE('_EW_SIZEQUERY_BAD_REQUEST', "BAD REQUEST: ");

DEFINE('_EW_VISIT_SUBMITED_FIELDS', "Submitted form fields:");
DEFINE('_EW_VISIT_URL_PARAMETERS', "URL parameters:");
DEFINE('_EW_VISIT_ADD_PAGE', " Add page as goal");
DEFINE('_EW_VISIT_BLOCK_IP', " Block this IP Address");
DEFINE('_EW_VISIT_SUBMITED_FROM_VARIABLE', " Add this submitted form variable as goal");
DEFINE('_EW_VISIT_URL_PARAMETER_GOAL', " Add this URL parameter as goal");

DEFINE('_EW_TREND_EMPTY', "Empty");

DEFINE('_EW_NOT_NUMBER', " WARNING: The value you entered is not a number. ExtraWatch will not work properly!");
DEFINE('_EW_EVALUATION_LEFT', "&nbsp; This is a 15-day Evaluation Version. Days Left: <b>%d</b>. Please purchase the lifetime <a href='http://www.codegravity.com/donate/extrawatch/' target='_blank'>ExtraWatch license for your domain</a> for this and upcoming versions.");
DEFINE('_EW_TRIAL_VERSION_EXPIRED', " Your trial version has expired. Please purchase ExtraWatch");

DEFINE('_EW_CONFIG_LICENSE_ACTIVATED', "License activated successfully. Thank you");
DEFINE('_EW_CONFIG_LICENCE_DONT_MATCH', "<b>Error: the license key and your domain don't match.</b><br/>Did you enter the same domain name into activation form as one you see below? Please contact: info@codegravity.com");

DEFINE('_EW_VIEW_ADMINBODY_LONG_MESSAGE', "If you are seeing the message above for too long, your live site may be wrong.
                    Open the components/com_extrawatch/config.php
                    uncomment, and set your actual live site. Eg.:
                    define('EXTRAWATCH_LIVE_SITE', 'http://www.codegravity.com');");

DEFINE('_EW_ADMINBODY_LIVE_SITE', "Warning: site in your browser and live site in configuration: %s and %s don't match.");
DEFINE('_EW_ADMINBODY_SET_LIVE_SITE', "Set live site to: %s and continue...");

DEFINE('_EW_ADMINHEADER_JW', "ExtraWatch ");
DEFINE('_EW_ADMINHEADER_REMOVE_BACKLINK', "Remove Backlink");
DEFINE('_EW_ADMINHEADER_KNOWLEDGE_BASE', "Knowledge Base");
DEFINE('_EW_ADMINHEADER_FLOW', "Flow");
DEFINE('_EW_ADMINHEADER_GRAPHS', "Graphs");
DEFINE('_EW_ADMINHEADER_COMPONENTS', "Components");
DEFINE('_EW_ADMINHEADER_REVIEW', "Review");
DEFINE('_EW_ADMINHEADER_WRITE', "Write a ");

DEFINE('_EW_FLOW_TRAFFIC', "Traffic Flow");
DEFINE('_EW_FLOW_SELECT_PAGE', "Select page:");
DEFINE('_EW_FLOW_OUTG_LINKS', "Root outgoing links count:");
DEFINE('_EW_FLOW_NESTING', "Nesting level:");
DEFINE('_EW_FLOW_SCALE', "Scale:");

DEFINE('_EW_COMERCIAL_AD_FREE', "Ad-free version");
DEFINE('_EW_COMERCIAL_THANK_DONATION', "Thank you very much for your donation!");
DEFINE('_EW_COMERCIAL_REGISTRATION_KEY', "Registration key for your domain %s is: ");
DEFINE('_EW_COMERCIAL_BACKLINKS_REMOVE', "Now you can remove backlink or hide ExtraWatch logo in frontend from Settings ");


DEFINE('_EW_SIZES_LAST_CHECK', "Last check was performed on:");
DEFINE('_EW_SIZES_ADMINISTRATOR', "BLUE = Size of component/module in /administrator directory");

DEFINE('_EW_SIZECOMPONENTS_COMPONENT', "Component");
DEFINE('_EW_SIZECOMPONENTS_TOTAL', "Total:");
DEFINE('_EW_SIZECOMPONENTS_SIZE', "Size");
DEFINE('_EW_SIZECOMPONENTS_REFRESH_ALL', "Refresh All");

DEFINE('_EW_SIZEDATABASE_TABLE', "Table");
DEFINE('_EW_SIZEDATABASE_SIZE', "Size");
DEFINE('_EW_SIZEDATABASE_1DAY', "1-Day Change");
DEFINE('_EW_SIZEDATABASE_7DAY', "7-Day Change");
DEFINE('_EW_SIZEDATABASE_28DAY', "28-Day Change");
DEFINE('_EW_SIZEDATABASE_NO_DATA', "no data");
DEFINE('_EW_SIZEDATABASE_TOTAL', "Total:");

DEFINE('_EW_SIZEMODULES_REFRESH_ALL', "Refresh All");
DEFINE('_EW_SIZEMODULES_TOTAL', "Total:");
DEFINE('_EW_SIZEMODULES_MODULE', "Module");
DEFINE('_EW_SIZEMODULES_SIZE', "Size");

DEFINE('_EW_SIZES_FILES', "Files &amp; Directories");
DEFINE('_EW_SIZES_BYTES', "bytes");
DEFINE('_EW_SIZES_KB', "KB");
DEFINE('_EW_SIZES_MB', "MB");
DEFINE('_EW_SIZES_GB', "GB");
DEFINE('_EW_SIZES_REFRESH', "Refresh");

DEFINE('_EW_STATS_FOOTER', "ExtraWatch &copy;2006-@YEAR@ by CodeGravity.com");

DEFINE('_EW_STATUS_MB', "MB");

DEFINE('_EW_DESC_IPINFODB_KEY', "Last visit map ipinfodb.com key from: <a href='http://www.ipinfodb.com/register.php' target='_blank'>ipinfodb.com</a>");
DEFINE('_EW_SETTINGS_FORCE_TIMEZONE_OFFSET', "Force Timezone Offset");


/* ExtraWatch 1.2.17 translations */
DEFINE('_EW_MENU_UPDATE', "Update");
DEFINE('_EW_MENU_UPDATE_TITLE', "Backup & Upgrade");
DEFINE('_EW_ADMINHEADER_NA_IN_THIS_VERSION', "Not available in free version, please check the license tab");
DEFINE('_EW_SPAMWORD_BANS_ENABLED', "Spam Words Ban Enable");
DEFINE('_EW_SPAMWORD_LIST', "Spam Words List");
DEFINE('_EW_HIDE_REPETITIVE_TITLE', "Hide Repetitive Title");
DEFINE('_EW_TRUNCATE_VISITS', "Truncate Visits");
DEFINE('_EW_TRUNCATE_STATS', "Truncate Stats");
DEFINE('_EW_TRUNCATE_GOALS', "Truncate Goals");
DEFINE('_EW_LIMIT_BOTS', "Limit Bots");
DEFINE('_EW_LIMIT_VISITORS', "Limit Visitors");
DEFINE('_EW_TOOLTIP_WIDTH', "Tooltip Width");
DEFINE('_EW_TOOLTIP_HEIGHT', "Tooltip Height");
DEFINE('_EW_TOOLTIP_URL', "Tooltip URL");
DEFINE('_EW_TOOLTIP_ONCLICK', "Tooltip OnClick");
DEFINE('_EW_IP_STATS', "IP stats");
DEFINE('_EW_IPINFODB_KEY', "IP Info DB key ");
DEFINE('_EW_ONLY_LAST_URI', "Only Last URI ");

DEFINE('_EW_FRONTEND_HIDE_LOGO', "Front End Hide Logo ");
DEFINE('_EW_FRONTEND_NOFOLLOW', "Front End No Follow");
DEFINE('_EW_FRONTEND_NO_BACKLINK', "Front End no Back Link");
DEFINE('_EW_FRONTEND_USER_LINK', "Front User links");
DEFINE('_EW_FRONTEND_COUNTRIES_FIRST', "Front End countries first");
DEFINE('_EW_FRONTEND_COUNTRIES_NAMES', "Front End Countries Name");
DEFINE('_EW_FRONTEND_COUNTRIES_UPPERCASE', "Front End Countreis Upper case");
DEFINE('_EW_FRONTEND_COUNTRIES_FLAGS_FIRST', "Front End Countries Flag First ");
DEFINE('_EW_FRONTEND_COUNTRIES_NUM', "Front End Countries Num");
DEFINE('_EW_FRONTEND_COUNTRIES_MAX_COLUMNS', "Front End Countries Max Colums");
DEFINE('_EW_FRONTEND_COUNTRIES_MAX_ROWS', "Front End Countries Max Rows");
DEFINE('_EW_FRONTEND_VISITORS_TODAY', "Front End Visitors Today ");
DEFINE('_EW_FRONTEND_VISITORS_YESTERDAY', "Front End Visitors Yesterday ");
DEFINE('_EW_FRONTEND_VISITORS_THIS_WEEK', "Front End Visitors This week ");
DEFINE('_EW_FRONTEND_VISITORS_LAST_WEEK', "Front End Visitors Last week ");

DEFINE('_EW_FRONTEND_VISITORS_THIS_MONTH', "Front End Visitors This Month ");
DEFINE('_EW_FRONTEND_VISITORS_LAST_MONTH', "Front End Visitors Last Month");
DEFINE('_EW_FRONTEND_VISITORS_TOTAL', "Front End Hide Visitors Total");
DEFINE('_EW_FRONTEND_VISITORS_TOTAL_INITIAL	', "Front End Total Initial");
DEFINE('_EW_HISTORY_MAX_VALUES', "History Max Values");
DEFINE('_EW_HISTORY_MAX_DB_RECORDS', "History Max records");
DEFINE('_EW_UPDATE_TIME_VISITS', "Update Time Visits");
DEFINE('_EW_UPDATE_TIME_STATS', "Update Time stats");
DEFINE('_EW_STATS_MAX_ROWS', "Stats Max rows");
DEFINE('_EW_STATS_IP_HITS', "Stats IP hits");
DEFINE('_EW_MAXID_BOTS', "Max ID bots");
DEFINE('_EW_MAXID_VISITORS', "Maxid Visitors");
DEFINE('_EW_STATS_KEEP_DAYS', "Stats Keep days ");
DEFINE('_EW_CACHE_FRONTEND_COUNTRIES', "Cache Front End Countries ");
DEFINE('_EW_CACHE_FRONTEND_VISITORS', "Cache Front End Visitors ");

DEFINE('_EW_UNINSTALL_KEEP_DATA	', "Uninstall Keep Data ");
DEFINE('_EW_IGNORE_IP', "Ignore IP");
DEFINE('_EW_IGNORE_URI', "Ignore URI");
DEFINE('_EW_IGNORE_USER', "Ignore User");
DEFINE('_EW_BLOCKING_MESSAGE', "Blocking Message");
DEFINE('_EW_SERVER_URI_KEY', "Server URI key");
DEFINE('_EW_FRONTEND_VISITORS_TOTAL_INITIAL', "Front End Visitors Total Initial");
DEFINE('_EW_SIZEDATABASE_RECORDS', "Records");
/***********EDITs*****************/
DEFINE('_EW_ANTISPAM_BLOCKING_TEXT', " To make the blocking effective, you need to publish ExtraWatch agent BEFORE any content or forms. Eg. on left side in your template.
                    <br/>
                    Go to Module Manager -> ExtraWatch agent -> select position as left");
DEFINE('_EW_EMAIL_SEO_REPORTS', "SEO Reports");
DEFINE('_EW_DESC_EMAIL_SEO_REPORTS_ENABLED', "SEO Nightly email reports enabled");
DEFINE('_EW_WATCH_INSTALLATION_DEMO', "Watch installation demo");

/** JW 1.2.18 */
DEFINE('_EW_ADMINHEADER_HEATMAP', "Clicks");
DEFINE('_EW_HEATMAP_CLICKS', "clicks");
DEFINE('_EW_HEATMAP_TITLE', "title");
DEFINE('_EW_HEATMAP_CLICK_OPEN', "Click to open heatmap");
DEFINE('_EW_NO_DATA', "This section contains no data yet ...");
DEFINE('_EW_POSITION', "Search result num.");
DEFINE('_EW_BLOCKING_REASON', 'reason');
DEFINE('_EW_BLOCKING_UNAUTHORIZED_ACCESS', 'Unauthorized Access');
DEFINE('_EW_BLOCKING_BLOCKED_MANUALLY', 'Blocked manually');
DEFINE('_EW_BLOCKING_BAD_WORD', "bad word");
DEFINE('_EW_HEATMAP_OF', "of");
DEFINE('_EW_URI', "uri");
DEFINE('_EW_COUNT', "count");
DEFINE('_EW_SEO_REPORT_FOR', "SEO report for");
DEFINE('_EW_SEO_MOST_DYNAMIC_KEYPHRASES', "Most dynamic keyphrases");
DEFINE('_EW_SEO_MIN_POSITION', "min position");
DEFINE('_EW_SEO_AVG_POSITION', "average position");
DEFINE('_EW_SEO_MAX_POSITION', "max position");
DEFINE('_EW_SEO_CHANGE', "change");
DEFINE('_EW_SEO_DATE_OF_LAST_CHANGE', "date of last change");
DEFINE('_EW_SEO_TOTAL_VISITS_FROM_SEARCH_ENGINES', "Total visits by keyphrase from search engines");
DEFINE('_EW_GOAL_ADD_SUBMITTED_VALUE', "Add submitted variable as goal");
DEFINE('_EW_RENDERED_IN', "Rendered in");
DEFINE('_EW_DESC_SEO_RENDER_ONLY_CHANGED', "Render only values which were already used in previous days (have some percentage in change column)");
DEFINE('_EW_SEO_REPORT_SETTINGS', "SEO Report Settings");


DEFINE('_EW_HTTP500_ERROR', "Could not initialize ExtraWatch javascript functions! Browser could not load: \\n%s, because it returned HTTP 500 internal server error.  \\nThis usually of security restrictions by other components/plugins. Please check your .htaccess file in your root Joomla directory.  \\nIf you use Akeeba Admin tools, you need to go \ 'Akeeba htaccess maker\ ' -> \ 'Server Protection\ ' -> \ 'Exceptions\ ' -> \ 'Allow direct access to these files\ ', and add: \\n/components/com_joomlawatch/block.php \\n/components/com_joomlawatch/img.php \\n/components/com_joomlawatch/last.php \\n/components/com_joomlawatch/lastvisit.php \\n/components/com_joomlawatch/sizequery.php \\n/components/com_joomlawatch/sizequerytotal.php \\n/components/com_joomlawatch/stats.php \\n/components/com_joomlawatch/timezone.php \\n/components/com_joomlawatch/tooltip.php \\n/components/com_joomlawatch/trendtooltip.php \\n/components/com_joomlawatch/vars.php \\n/components/com_joomlawatch/visits.php \\n/components/com_joomlawatch/js/joomlawatch.js.php \\n/components/com_joomlawatch/js/maps.js.php ");

DEFINE('_EW_AGENT_NOT_PUBLISHED_ERROR_JOOMLA',"Warning: No visits are currently being recorded. Your ExtraWatch Agent module is published, but most probably in position which does not exist in your template.
                <br/><br/>
                To fix this: <br/>
                1. go to Modules section -&gt; ExtraWatch Agent <br/>
                2. change the Position to 'left' or 'footer' (or some other position).<br/>
                3. check if new visits are being recorded
                <br/><br/>
                If this won't help, you can also: <br/>
                1. set 'Show Title' to 'Yes' in  Modules section -&gt; ExtraWatch Agent <br/>
                2. try to use some other positions of ExtraWatch agent module<br/>
                3. save<br/>
                4. after reload of your main page look for 'ExtraWatch Agent' text.<br/>
                5. this way you'll make sure the Agent module is published.<br/>
                6. after that you can set 'Show Title' back to 'No'.
                ");
/** Goals/Import IP */
DEFINE('_EW_GOALS', "Goals");
DEFINE('_EW_GOALS_IMPORT', "Import goals");
DEFINE('_EW_GOALS_IMPORT_XML', "Import XML");
DEFINE('_EW_GOALS_EXPORT', "Export goals");
DEFINE('_EW_GOALS_FILENAME', "Select goals previously exported as XML");
DEFINE('_EW_ANTISPAM_IMPORT_CSV', "import anti-spam ip csv");

DEFINE('_EW_AGENT_NOT_PUBLISHED_ERROR_WORDPRESS',"Warning: No visits are being recorded. You must go to Appearance->Widgets section, <br/>find ExtraWatchAgent widget and drag&drop it to some of the containers on the right side. <br/>You can publish also other ExtraWatch modules this way.");

DEFINE('_EW_BLOCKED_BASED_ON_GOAL',"Blocked based on goal no. %s");

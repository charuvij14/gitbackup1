=== ExtraWatch Live Stats and Visitor Counter FREE ===
Contributors: matto3c
Donate link: http://www.extrawatch.com/
Tags: admin, widget, plugin, counter, anti-spam, stats, visitors, click map, countries, tracking, maps, location, geolocation, statistics, spam, flags, goals
Requires at least: 3.2.
Tested up to: 3.5.1.
Stable tag: 2.0.1084


Description: Features: Visitor Live Stats, Front-end Counters, Anti-spam, Nightly Email Reports, History, Graphs, translated in 42 world languages





== Description ==
Popular visitor live tracking component ported from Joomla. ExtraWatch allows you to watch your visitors live stats and bots in real-time from the administration back-end.

Especially their IP addresses, countries they come from, geographical location on a map, which pages they are viewing, their browser and operating system, it creates daily and all-time stats from these information plus unique, pageload and total hits statistics.
Furthermore, you can block harmful IP addresses, see blocked attempts stats, evaluate the trend charts, and create goals based on many parameters. In the front-end, it displays the top countries, user and visit information for certain periods of time.

[ExtraWatch PRO stats for WordPress video](http://www.youtube.com/watch?v=2-lUIPBAA0I)

http://www.youtube.com/watch?v=2-lUIPBAA0I&hd=1

shown on Joomla, but it's similar for wordpress


FREE version features:

(formerly known as JoomlaWatch)

Watch your visitors behaviour and optimize your website. Front-end counters with country flags, number of visits per specific time period, latest users, Live Stats, Goals, Block harmful IP addresses, Anti-spam, 38 languages.

** Free Android mobile app **
- access your stats in one-click from your mobile screen

** Front-end widgets ** 
- displays number of visitors for today, visitors for yesterday, this week counter, last week, this month, last month, total visitors 
- logged in users
- flags of visitor's coutnries


** Live Stats ** 
- displays the your visitor's visits in real-time 
- pop-up window with 3rd party URL to get more information about IP address
- IP address, browser, operating system, actual page, where user came from
- option to block user and add page to goals
- dashboard with current and previous week visit statistics
- graphs for today with goals, referers, internal page flow, keyphrases, keywords, visited pages, most active top users, countries, browsers, operating systems

** History ** 
- displays the same as Live Stats but with more tabs and paging 

** Goals ** 
- allows you to track various types of user actions, such as:
visited page, page title, username, IP address, came from, country, URL and Form parameters, option to block or redirect such users

** Graphs ** 
- section contains daily and weekly bar charts for pages, goals, users .. etc.

** Anti-spam **
- contains list of pre-defined spam words and blocked IP addresses, you can add your "bad words" into this list based on which the IP addresses will be blocked from your website

** Emails **
- an email report which is sent overnight so you don't have to log in into Joomla back-end to see how is your site performing
- nightly email report contains 1-day, 7-day, 28-day change of: 
- total visits (unique / page loads/ hits), 
- number of fulfilled goals, pages, referers, countries, browsers, operating systems
- allows you to configure nightly email reports and filter the results


** Settings **
- possibility to choose from one of these languages: Albanian, Arabic, Bengali, Brazilian Portuguese, Bulgarian, Chinese-simplified, Chinese-traditional, Croatian, Czech, Danish, Dutch, English, Estonian, French, German, Greek, Hebrew, Hindi, Hungarian, Indonesian, Italian, Japanese, Latvian, Lithuanian, Macedonian, polish, Romanian, Russian, Serbian, Slovak, Slovenian, Spanish, Swedish, Turkish, Ukrainian, Urdu, Vietnamese
- show only last visited page in live stats
- hide repetitive title (in case page title contains some prefix)
- number of total visits
- URL of 3rd party geolocation service opened on popup
- front-end user widget possibility to set link which displays most active user's info
- front-end country widget flag settings (name, upper/lower case, number of rows/columns, max rows / columns), toggle 
- front-end visitor widget settings (to show today, yesterday, this week, last week, this month, last month, total)
- history settings (max values, update time, max rows, cache settings)
- option to ignore IP, ignore page, ignore user from statistics
- blocking message which appears to blocked user
- possibility to erase all gathered data






[Easy installation](http://www.youtube.com/watch?v=793NAfidfDM)

http://www.youtube.com/watch?v=793NAfidfDM

For more information and demos please visit: http://www.extrawatch.com

- translations: brazilian portuguese, dutch, french, german, greek, russian, slovak, slovenian, spanish, swedish, czech, danish, lithuanian, polish italian, turkish, latvian, ukrainian, bulgarian

Complete description of all features:
http://www.extrawatch.com/features/

Watch the demos here:
http://www.extrawatch.com/demos/

Live working demo deployed at: 
http://www.codegravitydemo.com/wordpress/FREE/

http://www.codegravitydemo.com/wordpress/FREE/wp-admin/

username: admin
password: demo


== Installation ==
Installation of ExtraWatch into Wordpress:

1. Log in to wordpress back-end
2. Click "Plugins"
3. Click "Add new" from the top of the screen
Method1: Manual installation:
4. Click "Upload"
5. Select a .zip file and click "Install now"
6. Click "Activate plugin"
Method2: Installation from plugin directory:
4. In the Search field type "ExtraWatch"
5. You should see 2 results: ExtraWatch Live Stats and Visitor Counter FREE and PRO
6. Depending on which version you'd like to install, Click on link "Install now" and confirm the confirmation dialog
7. Click "Activate Plugin"
8. Then, the new menu item "ExtraWatch" appears on left
9. Confirm the license and / or enter the license key if you want to activate the PRO version
10. Place the widgets on the front-end:
11. Click on Appearance -> Then Widgets
12. Move the "ExtraWatch Agent / Users / Visitors" to the containers on the right side
to "Primary widget area" or any other widget area which is there. This is where the actual output will be placed
13. Reload the front-end. After few reloads / new visits, you should be able to see first data in widgets
13. Click on ExtraWatch icon in the back-end and you should be able to see new visitors in "Live Stats" section

Enjoy!


== Screenshots ==

1. Live Stats
2. Front-end modules (Users by countries, Stats per time period)
3. Graphs & Trends
4. Insert new Goal (conditions)
5. Anti-Spam and IP Blocking
6. Email Reports
7. Settings
8. Geolocation Map
9. Goals Form

== Frequently Asked Questions ==

Please read http://www.extrawatch.com/faq/

== Changelog ==

= version 2.0 =
https://code.google.com/p/extrawatch/source/list

== Upgrade Notice ==

To upgrade to the latest version, use the standard wordpress update method:
http://www.youtube.com/watch?v=FnFUo3yID5I


== Arbitrary section ==

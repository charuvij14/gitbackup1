<?php
/**
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

/**
 * Joomla! P3P Header Plugin
 *
 * @package		Joomla.Plugin
 * @subpackage	System.p3p
 */
class plgSystemOSEUpdateCheck extends JPlugin
{
	function onAfterInitialise()
	{
		$db = JFactory::getDBO(); 
		$query = "SELECT `id`, `option`, `remote_id`, `type`, `cur_version`, `new_version` FROM `#__ose_app_apps` WHERE (DATEDIFF(NOW(), `checked_date`) > 3  || `checked_date` IS NULL || `cur_version` ='' ) "; 
		$db->setQuery($query); 
		$results = $db->loadObjectList(); 
		if (empty($results)) return; 
		foreach ($results as $result)
		{
			$continue = $this->checkContinue($result->option, $result->type);
			if ($continue == true)
			{
				$CurVersion = $this->checkVersion($result->option, $result->type);
				$LstVersion = $this->checkRemoteVer($result->remote_id); 
				$time = date('Y-m-d'); 
				if ($CurVersion!=$result->cur_version || $LstVersion!=$result->new_version)
				{
					$this->updateApp($result->id, $time, $CurVersion, $LstVersion);
				}
			} 
		}
	}
	function checkContinue($extension, $type)
	{
		$type = explode('.', $type);
		switch ($type[0]) {
			default:
			case 'com':
				$folder= JPATH_ADMINISTRATOR.DS.'components'.DS.$extension;
			break;
			case 'plg':
				$folder= JPATH_SITE.DS.'plugins'.DS.$type[1].DS.$extension;
				break;
			case 'mod':
				$folder= JPATH_SITE.DS.'modules'.DS.'mod_'.$extension;
				break;
			case 'package':
				$folder= JPATH_SITE.DS;
				break;
		}
			
		if(JFolder :: exists($folder))
		{
			return true; 
		}
		else
		{
			return false;
		}
	}
	function checkVersion($extension, $type)
	{
		$type = explode('.', $type); 
		switch ($type[0]) {
			default:
			case 'com':
				$folder= JPATH_ADMINISTRATOR.DS.'components'.DS.$extension;
			break; 
			case 'plg':
				$folder= JPATH_SITE.DS.'plugins'.DS.$type[1].DS.$extension;
			break;
			case 'mod':
				$folder= JPATH_SITE.DS.'modules'.DS.'mod_'.$extension;
			break;
			case 'package':
				return OSESUITEVERSION;
				break;	
		}
			
			if(JFolder :: exists($folder))
			{
				$xmlFilesInDir= JFolder :: files($folder, '.xml$');
			}
			else
			{
				return false; 
			}
			$xml_items= '';
			if(count($xmlFilesInDir))
			{
				foreach($xmlFilesInDir as $xmlfile)
				{
					if($data= JApplicationHelper :: parseXMLInstallFile($folder.DS.$xmlfile))
					{
						foreach($data as $key => $value)
						{
							$xml_items[$key]= $value;
						}
					}
				}
			}
			if(isset($xml_items['version']) && $xml_items['version'] != '')
			{
				return $xml_items['version'];
			}
			else
			{
				return '';
			}
	}
	
	function checkRemoteVer($id)
	{
		/*
		}
		$curHost = $_SERVER['HTTP_HOST'];
		if ($curHost == 'localhost')
		{
			$error = "Version does not show on local servers";
			$this->assignRef('version',$error);
		}
		*/
		$url= "www.opensource-excellence.com";
		$req= "/version2.php?id=".(int)$id;

		$fp= fsockopen($url, 80, $errno, $errstr, 30);
		if(!$fp || $errno)
		{
			$error = "Version unknown. Connection ERROR";
			return $error;  
		}
		else
		{
			@ fputs($fp, "GET ".$req." HTTP/1.1\r\n");
			@ fputs($fp, "HOST: ".$url."\r\n");
			@ fputs($fp, "Connection: close\r\n\r\n");
				// read the body data
			$res= '';
			$headerdone= false;
			while(!feof($fp)) {
				$line= fgets($fp, 1024);
				if(strcmp($line, "\r\n") == 0) {
					// read the header
					$headerdone= true;
				} else
				if($headerdone) {
					// header has been read. now read the contents
					$res .= $line;
				}
			}
			fclose($fp);
			preg_match('/<(.*)>/i', $res, $ret);
			if(!empty($ret[0]))
			{
				return $ret[1];
			}
			else
			{
				return "Version unknown. Connection ERROR";
			}
		}
	}
	function updateApp($id, $time, $curVersion, $LstVersion)
	{
		$db = JFactory::getDBO();
		$query = " UPDATE `#__ose_app_apps` SET `cur_version` = ".$db->Quote($curVersion, true).", new_version = ".$db->Quote($LstVersion, true).", checked_date = ".$db->Quote($time, true).
				 " WHERE `id` = ". (int)$id;
		$db->setQuery($query);
		$result = $db->query(); 
		return $result; 
	}
}

<?php
/**
 * @package		Joomla.Installation
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="panel panel-default">
    <div class="panel-heading white-bg">
		<h3 class="panel-title"><?php echo JText::_('INSTL_SITE'); ?></h3>
    </div>
    <div class="panel-controls-buttons">
<?php if ($this->document->direction == 'ltr') : ?>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('filesystem');" rel="prev" title="<?php echo JText::_('JPrevious'); ?>"><?php echo JText::_('JPrevious'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="Install.submitform();" rel="next" title="<?php echo JText::_('JNext'); ?>"><?php echo JText::_('JNext'); ?></button>
<?php elseif ($this->document->direction == 'rtl') : ?>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="Install.submitform();" rel="next" title="<?php echo JText::_('JNext'); ?>"><?php echo JText::_('JNext'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('filesystem');" rel="prev" title="<?php echo JText::_('JPrevious'); ?>"><?php echo JText::_('JPrevious'); ?></button>
<?php endif; ?>
	</div>
</div>

<div class="panel panel-success">
		<form action="index.php" method="post" id="adminForm" class="form-validate">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo JText::_('INSTL_SITE_NAME_TITLE'); ?></h4>
        </div>
		<div class="m">
			<div class="install-text">
				<?php echo JText::_('INSTL_SITE_NAME_DESC'); ?>
			</div>
			<div class="install-body">
				<div class="m">
					<h4 class="title-smenu" title="<?php echo JText::_('INSTL_BASIC_SETTINGS'); ?>">
						<?php echo JText::_('INSTL_BASIC_SETTINGS'); ?>
					</h4>
					<div class="section-smenu">
						<table class="content2">
							<tr>
								<td class="item">
									<?php echo $this->form->getLabel('site_name'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('site_name'); ?>
								</td>
							</tr>
						</table>
					</div>

					<h4 class="title-smenu moofx-toggler" title="<?php echo JText::_('INSTL_SITE_META_ADVANCED_SETTINGS'); ?>">
						<a href="#"><?php echo JText::_('INSTL_SITE_META_ADVANCED_SETTINGS'); ?></a>
					</h4>
					<div class="section-smenu moofx-slider" style="display:none;">
							<table class="content2">
								<tr>
									<td title="<?php echo JText::_('INSTL_SITE_METADESC_TITLE_LABEL'); ?>">
										<?php echo $this->form->getLabel('site_metadesc'); ?>
									</td>
									<td>
										<?php echo $this->form->getInput('site_metadesc'); ?>
									</td>
								</tr>
								<tr>
									<td title="<?php echo JText::_('INSTL_SITE_METAKEYS_TITLE_LABEL'); ?>">
										<?php echo $this->form->getLabel('site_metakeys'); ?>
									</td>
									<td>
										<?php echo $this->form->getInput('site_metakeys'); ?>
									</td>
								</tr>
								<tr>
									<td title="<?php echo JText::_('INSTL_SITE_OFFLINE_TITLE_LABEL'); ?>">
										<?php echo $this->form->getLabel('site_offline'); ?>
									</td>
									<td>
										<?php echo $this->form->getInput('site_offline'); ?>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>

		</div>
		<div class="clr"></div>
		<div class="panel-heading">
					<h4 class="panel-title"><?php echo JText::_('INSTL_SITE_CONF_TITLE'); ?></h4>
		</div>
		<div class="m">
			<div class="install-text">
				<?php echo JText::_('INSTL_SITE_CONF_DESC'); ?>
			</div>
			<div class="install-body">
					<div class="m">
						<fieldset>
							<table class="content2">
								<tr>
									<td class="item">
										<?php echo $this->form->getLabel('admin_email'); ?>
									</td>
									<td>
										<?php echo $this->form->getInput('admin_email'); ?>
									</td>
								</tr>
								<tr>
									<td class="item">
										<?php echo $this->form->getLabel('admin_user'); ?>
									</td>
									<td>
										<?php echo $this->form->getInput('admin_user'); ?>
									</td>
								</tr>
								<tr>
									<td class="item">
										<?php echo $this->form->getLabel('admin_password'); ?>
									</td>
									<td>
										<?php echo $this->form->getInput('admin_password'); ?>
									</td>
								</tr>
								<tr>
									<td class="item">
										<?php echo $this->form->getLabel('admin_password2'); ?>
									</td>
									<td>
										<?php echo $this->form->getInput('admin_password2'); ?>
									</td>
								</tr>
							</table>
						</fieldset>
					</div>
					<input type="hidden" name="task" value="setup.saveconfig" />
					<?php echo JHtml::_('form.token'); ?>
					<?php echo $this->form->getInput('sample_installed'); ?>
			</div>
		</div>	
		</form>
		<div class="clr"></div>
</div>

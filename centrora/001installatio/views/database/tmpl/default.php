<?php
/**
 * @package		Joomla.Installation
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="panel panel-default">
    <div class="panel-heading white-bg">
		<h3 class="panel-title"><?php echo JText::_('INSTL_DATABASE'); ?></h3>
    </div>
    <div class="panel-controls-buttons">
<?php if ($this->document->direction == 'ltr') : ?>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('license');" rel="prev" title="<?php echo JText::_('JPrevious'); ?>"><?php echo JText::_('JPrevious'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="Install.submitform();" rel="next" title="<?php echo JText::_('JNext'); ?>"><?php echo JText::_('JNext'); ?></button>
<?php elseif ($this->document->direction == 'rtl') : ?>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="Install.submitform();" rel="next" title="<?php echo JText::_('JNext'); ?>"><?php echo JText::_('JNext'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('license');" rel="prev" title="<?php echo JText::_('JPrevious'); ?>"><?php echo JText::_('JPrevious'); ?></button>
<?php endif; ?>
	</div>
</div>
<form action="index.php" method="post" id="adminForm" class="form-validate">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo JText::_('INSTL_DATABASE_TITLE'); ?></h4>
        </div>
		<div class="m">
			<div class="install-text">
					<?php echo JText::_('INSTL_DATABASE_DESC'); ?>
			</div>
			<div class="install-body">
				<div class="m">
					<h4 class="title-smenu" title="<?php echo JText::_('Basic'); ?>">
						<?php echo JText::_('INSTL_BASIC_SETTINGS'); ?>
					</h4>
					<div class="section-smenu">
						<table class="content2 db-table">
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="2">
									<?php echo $this->form->getLabel('db_type'); ?>
									<br />
									<?php echo $this->form->getInput('db_type'); ?>
								</td>
								<td>
									<em>
									<?php echo JText::_('INSTL_DATABASE_TYPE_DESC'); ?>
									</em>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<?php echo $this->form->getLabel('db_host'); ?>
									<br />
									<?php echo $this->form->getInput('db_host'); ?>
								</td>
								<td>
									<em>
									<?php echo JText::_('INSTL_DATABASE_HOST_DESC'); ?>
									</em>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<?php echo $this->form->getLabel('db_user'); ?>
									<br />
									<?php echo $this->form->getInput('db_user'); ?>
								</td>
								<td>
									<em>
									<?php echo JText::_('INSTL_DATABASE_USER_DESC'); ?>
									</em>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<?php echo $this->form->getLabel('db_pass'); ?>
									<br />
									<?php echo $this->form->getInput('db_pass'); ?>
								</td>
								<td>
									<em>
									<?php echo JText::_('INSTL_DATABASE_PASSWORD_DESC'); ?>
									</em>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<?php echo $this->form->getLabel('db_name'); ?>
									<br />
									<?php echo $this->form->getInput('db_name'); ?>
								</td>
								<td>
									<em>
									<?php echo JText::_('INSTL_DATABASE_NAME_DESC'); ?>
									</em>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<?php echo $this->form->getLabel('db_prefix'); ?>
									<br />
									<?php echo $this->form->getInput('db_prefix'); ?>
								</td>
								<td>
									<em>
									<?php echo JText::_('INSTL_DATABASE_PREFIX_DESC'); ?>
									</em>
								</td>
							</tr>
							<tr >
								<td colspan="2">
								</td>
								<td>
									<input type="hidden" value="backup" name="jform[db_old]" id="jform_db_old0">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	<input type="hidden" name="task" value="setup.database" />
	<?php echo JHtml::_('form.token'); ?>
</form>

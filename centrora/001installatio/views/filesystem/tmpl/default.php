<?php
/**
 * @package		Joomla.Installation
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="panel panel-default">
    <div class="panel-heading white-bg">
		<h3 class="panel-title"><?php echo JText::_('INSTL_FTP'); ?></h3>
    </div>
    <div class="panel-controls-buttons">
<?php if ($this->document->direction == 'ltr') : ?>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('database');" rel="prev" title="<?php echo JText::_('JPrevious'); ?>"><?php echo JText::_('JPrevious'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="Install.submitform();" rel="next" title="<?php echo JText::_('JNext'); ?>"><?php echo JText::_('JNext'); ?></button>
<?php elseif ($this->document->direction == 'rtl') : ?>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="Install.submitform();" rel="next" title="<?php echo JText::_('JNext'); ?>"><?php echo JText::_('JNext'); ?></button>
		<button class="btn btn-success btn-sm mr5 mb10" onclick="return Install.goToPage('database');" rel="prev" title="<?php echo JText::_('JPrevious'); ?>"><?php echo JText::_('JPrevious'); ?></button>
<?php endif; ?>
	</div>
</div>

<form action="index.php" method="post" id="adminForm" class="form-validate">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo JText::_('INSTL_FTP_TITLE'); ?></h4>
        </div>
		<div class="m">
			<div class="install-text">
				<?php echo JText::_('INSTL_FTP_DESC'); ?>
			</div>
			<div class="install-body">
				<div class="m">
					<h4 class="title-smenu" title="<?php echo JText::_('INSTL_BASIC_SETTINGS'); ?>">
						<?php echo JText::_('INSTL_BASIC_SETTINGS'); ?>
					</h4>
					<div class="section-smenu">
						<table class="content2">
							<tr>
								<td>
									<?php echo $this->form->getLabel('ftp_enable'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('ftp_enable'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<?php echo $this->form->getLabel('ftp_user'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('ftp_user'); ?>
								</td>
								<td>
									<em>
									<?php echo JText::_('INSTL_FTP_USER_DESC'); ?>
									</em>
								</td>
							</tr>
							<tr>
								<td>
									<?php echo $this->form->getLabel('ftp_pass'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('ftp_pass'); ?>
								</td>
								<td>
									<em>
									<?php echo JText::_('INSTL_FTP_PASSWORD_DESC'); ?>
									</em>
								</td>
							</tr>
							<tr id="rootPath">
								<td>
									<?php echo $this->form->getLabel('ftp_root'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('ftp_root'); ?>
								</td>
							</tr>
						</table>

						<input type="button" id="findbutton" class="button" value="<?php echo JText::_('INSTL_AUTOFIND_FTP_PATH'); ?>" onclick="Install.detectFtpRoot(this);" />
						<input type="button" id="verifybutton" class="button" value="<?php echo JText::_('INSTL_VERIFY_FTP_SETTINGS'); ?>" onclick="Install.verifyFtpSettings(this);" />
						<br /><br />
					</div>

					<h4 class="title-smenu moofx-toggler" title="<?php echo JText::_('INSTL_ADVANCED_SETTINGS'); ?>">
						<a href="#"><?php echo JText::_('INSTL_ADVANCED_SETTINGS'); ?></a>
					</h4>
					<div class="section-smenu moofx-slider">
						<table class="content2">
							<tr id="host">
								<td>
									<?php echo $this->form->getLabel('ftp_host'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('ftp_host'); ?>
								</td>
							</tr>
							<tr id="port">
								<td>
									<?php echo $this->form->getLabel('ftp_port'); ?>
								</td>
								<td>
									<?php echo $this->form->getInput('ftp_port'); ?>
								</td>
							</tr>
							<tr>
								<td>
									<input type="hidden" value="1" name="jform[ftp_save]" id="jform_ftp_save1">
								</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	<input type="hidden" name="task" value="setup.filesystem" />
	<?php echo JHtml::_('form.token'); ?>
</form>

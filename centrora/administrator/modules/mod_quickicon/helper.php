<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	mod_quickicon
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * @package		Joomla.Administrator
 * @subpackage	mod_quickicon
 * @since		1.6
 */
abstract class modQuickIconHelper
{
	/**
	 * Stack to hold buttons
	 *
	 * @since	1.6
	 */
	protected static $buttons = array();

	/**
	 * Helper method to return button list.
	 *
	 * This method returns the array by reference so it can be
	 * used to add custom buttons or remove default ones.
	 *
	 * @param	JRegistry	The module parameters.
	 *
	 * @return	array	An array of buttons
	 * @since	1.6
	 */
	public static function &getButtons($params)
	{
		$key = (string)$params;
		if (!isset(self::$buttons[$key])) {
			$context = $params->get('context', 'mod_quickicon');
			if ($context == 'mod_quickicon')
			{
				// Load mod_quickicon language file in case this method is called before rendering the module
				JFactory::getLanguage()->load('mod_quickicon');
				self::$buttons[$key] = array(
					array(
											'link' => JRoute::_('index.php?option=com_ose_firewall&view=manageips'),
											'text' => '<div class="flaticon-shield"></div><div class="flaticon-shield-text">'.JText::_('Manage IP')."</div>",
											'access' => array('core.manage', 'com_ose_antihacker')
					),
					array(
											'link' => JRoute::_('index.php?option=com_ose_firewall&view=vsscan'),
											'text' => '<div class="flaticon-gun3"></div><div class="flaticon-gun3-text">'.JText::_('Virus Scanner')."</div>",
											'access' => array('core.manage', 'com_ose_antivirus')
					),
					array(
											'link' => JRoute::_('index.php?option=com_ose_firewall&view=advancerulesets'),
											'text' => '<div class="flaticon-synchronize1"></div><div class="flaticon-synchronize1-text">'.JText::_('Firewall Updates')."</div>",
											'access' => array('core.manage', 'com_ose_fileman')
					),
					array(
											'link' => JRoute::_('index.php?option=com_ose_firewall&view=backup'),
											'text' => '<div class="flaticon-control3"></div><div class="flaticon-control3-text">'.JText::_('Backup')."</div>",
											'access' => array('core.manage', 'com_ose_cpu')
					)
				);
				
			}
			else
			{
				self::$buttons[$key] = array();
			}

			// Include buttons defined by published quickicon plugins
			JPluginHelper::importPlugin('quickicon');
			$app = JFactory::getApplication();
			/*
			$arrays = (array) $app->triggerEvent('onGetIcons', array($context));

			foreach ($arrays as $response) {
				foreach ($response as $icon) {
					$default = array(
						'link' => null,
						'image' => 'header/icon-48-config.png',
						'text' => null,
						'access' => true
					);
					$icon = array_merge($default, $icon);
					if (!is_null($icon['link']) && !is_null($icon['text'])) {
						self::$buttons[$key][] = $icon;
					}
				}
			}
			*/
		}

		return self::$buttons[$key];
	}

	/**
	 * Get the alternate title for the module
	 *
	 * @param	JRegistry	The module parameters.
	 * @param	object		The module.
	 *
	 * @return	string	The alternate title for the module.
	 */
	public static function getTitle($params, $module)
	{
		$key = $params->get('context', 'mod_quickicon') . '_title';
		if (JFactory::getLanguage()->hasKey($key))
		{
			return JText::_($key);
		}
		else
		{
			return $module->title;
		}
	}
	
	private static function checkUpgraded()
	{
		$db = JFactory::getDBO(); 
		$curVerion = '5.2.1';
		$query = "SELECT `value` FROM `#__ose_secConfig` WHERE `key` = 'upgradedto'"; 
		$db->setQuery($query); 
		$result = $db->loadResult(); 
		if ($result==$curVerion)
		{
			return true; 
		}
		else
		{
			return false; 
		}
	}
}

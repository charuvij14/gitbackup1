<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	mod_login
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
?>
<form role="form" action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="form-login" class="form-horizontal mt0" novalidate="novalidate">
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input name="username" id="mod-login-username" type="text" class="form-control left-icon" size="15" />
                                    <i class="im-user s16 left-input-icon"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-12">
                                    <input type="password" placeholder="Your password" value="somepass" class="form-control left-icon" name="passwd" id="mod-login-password" size="15">
                                    <i class="im-lock s16 left-input-icon"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-8">
                                    
                                </div>
                                <!-- col-lg-12 end here -->
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-4 mb25">
                                    <!-- col-lg-12 start here -->
                                    <button type="submit" class="btn btn-default pull-right" onclick="document.getElementById('form-login').submit();">Login</button>
                                </div>
                                <!-- col-lg-12 end here -->
                            </div>
                            <input type="submit" class="hidebtn" value="<?php echo JText::_( 'MOD_LOGIN_LOGIN' ); ?>" />
							<input type="hidden" name="option" value="com_login" />
							<input type="hidden" name="task" value="login" />
							<input type="hidden" name="secret" value="<?php echo JRequest::getVar('secret'); ?>" />
							<input type="hidden" name="return" value="<?php echo $return; ?>" />
							<?php echo JHtml::_('form.token'); ?>
</form>
<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	Templates.bluestork
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access.
defined ( '_JEXEC' ) or die ();
jimport ( 'joomla.filesystem.file' );
require_once (dirname ( __FILE__ ) . '/osePanel.php');
$osePanel = new osePanel ();
$app = JFactory::getApplication ();
$doc = JFactory::getDocument ();
$doc->addStyleSheet ( 'templates/system/css/system.css' );
$doc->addStyleSheet ( 'templates/' . $this->template . '/css/template.css' );
if ($this->direction == 'rtl') {
	$doc->addStyleSheet ( 'templates/' . $this->template . '/css/template_rtl.css' );
}
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/jquery.dataTables.min.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/bootstrap.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/waitme.less.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/icons.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/main.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/v4.css' );
/**
 * Load specific language related css
 */
$lang = JFactory::getLanguage ();
$file = 'language/' . $lang->getTag () . '/' . $lang->getTag () . '.css';
if (JFile::exists ( $file )) {
	$doc->addStyleSheet ( $file );
}
if ($this->params->get ( 'textBig' )) {
	$doc->addStyleSheet ( 'templates/' . $this->template . '/css/textbig.css' );
}
if ($this->params->get ( 'highContrast' )) {
	$doc->addStyleSheet ( 'templates/' . $this->template . '/css/highcontrast.css' );
}
if (!empty($osePanel->oemClass)) {
	$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/oem/'.$osePanel->oemClass->customer_id.'/custom.css' );
}
$oseVersion = $osePanel->getVersion ();

define('ODS', DIRECTORY_SEPARATOR);
define('OSEFWDIR', dirname(dirname(dirname(__FILE__))) . '/components/com_ose_firewall/');
require_once(dirname(dirname(dirname(__FILE__))) . '/components/com_ose_firewall/assets/config/define.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/components/com_ose_firewall/classes/Library/oseFirewallJoomla.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/components/com_ose_firewall/vendor/oseframework/language/oseLanguage.php');
require_once(dirname(dirname(dirname(__FILE__))) . '/components/com_ose_firewall/public/messages/en_US.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="<?php echo  $this->language; ?>"
	lang="<?php echo  $this->language; ?>"
	dir="<?php echo  $this->direction; ?>">
<head>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans%3A400italic%2C400%2C600%2C700%7CRoboto+Slab%3A400%2C300%2C700" type="text/css" />
<jdoc:include type="head" />

<!--[if IE 7]>
<link href="templates/<?php echo  $this->template ?>/css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<!--[if gte IE 8]>
<link href="templates/<?php echo  $this->template ?>/css/ie8.css" rel="stylesheet" type="text/css" />
<![endif]-->
</head>
<body class="contentpane">
	<div class="container">
		<nav role="navigation" class="navbar navbar-default">
				    <div class="everythingOnOneLine">
						<div class="col-lg-12">
							<?php 
								if (!empty($osePanel->oemClass)) {
									echo $osePanel->oemClass->addLogo (); 
								}
								else {
							?>
							<div class="logo">
								<img width="250px" alt="Centrora Logo"
									 src="components/com_ose_firewall/public/images/topbar/whitelogo.png">
							</div>
							<?php 
								}
							?>
							<div id="versions"> <div class="version-updated"> <?php echo $osePanel->getVersion(); ?></div></div>
									<?php 
											echo $osePanel->getMenus(); 
									?>
						</div>
					</div>
					<div class="navbar-top">
						 <div class="col-lg-1 col-sm-6 col-xs-6 col-md-6">
								<div class="pull-left">
								</div>
						 </div>
						 <div class="col-lg-11 col-sm-6 col-xs-6 col-md-6">
							 <div class="pull-right">
										<ul class="userMenu ">
										<?php 
											echo $osePanel->getTopBarURL();
										?>
										</ul>
							 </div>
						</div>
					</div>
		</nav>
        <?php
	  
        oseFirewall::getmenus();
        ?>
		<jdoc:include type="message" />
		<div id="content-inner">
			<div class="row ">
				<div class="col-lg-12 sortable-layout">
					<div id="jst_0"
						class="panel panel-primary plain toggle panelClose panelRefresh">
						<div class="panel-heading white-bg">
							<h4 class="panel-title"></h4>
							<div class="panel-controls">
								<a class="panel-refresh" href="#"><i class="im-spinner12"></i></a>
								<a class="toggle panel-minimize" href="#"><i class="im-minus"></i></a>
								<a class="panel-close" href="#"><i class="im-close"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div class="dataTables_wrapper">
								<jdoc:include type="component" />
								<div class="clr"></div>
							</div>
						</div>
						<noscript>
			<?php echo  JText::_('JGLOBAL_WARNJAVASCRIPT')?>
		</noscript>
					</div>
				</div>
			</div>
		</div>

		<footer>
		<?php echo $osePanel->showFooter(); ?>
		</footer>
	</div>
<?php 
$doc->addScript('components/com_ose_firewall//public/js/jquery-1.11.1.min.js' );
$doc->addScript('components/com_ose_firewall//public/js/bootstrap.min.js' );
$doc->addScript('components/com_ose_firewall//public/js/joomla.js' );
?>		
</body>
</html>
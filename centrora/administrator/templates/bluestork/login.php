<?php
/**
 * @package		Joomla.Administrator
 * @subpackage	Templates.bluestork
 * @copyright	Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined ( '_JEXEC' ) or die ();
jimport ( 'joomla.filesystem.file' );
require_once (dirname ( __FILE__ ) . '/osePanel.php');
$osePanel = new osePanel ();
$app = JFactory::getApplication ();
$doc = JFactory::getDocument ();
$doc->addStyleSheet ( 'templates/system/css/system.css' );
$doc->addStyleSheet ( 'templates/' . $this->template . '/css/template.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/jquery.dataTables.min.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/bootstrap.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/waitme.less.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/icons.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/main.css' );
$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/v4.css' );
if ($this->direction == 'rtl') {
	$doc->addStyleSheet ( 'templates/' . $this->template . '/css/template_rtl.css' );
}
if (!empty($osePanel->oemClass)) {
	$doc->addStyleSheet ( 'components/com_ose_firewall/public/css/oem/'.$osePanel->oemClass->customer_id.'/custom.css' );
}
/**
 * Load specific language related css
 */
$lang = JFactory::getLanguage ();
$file = 'language/' . $lang->getTag () . '/' . $lang->getTag () . '.css';
if (JFile::exists ( $file )) {
	$doc->addStyleSheet ( $file );
}
$oseVersion = $osePanel->getVersion ();
JHtml::_ ( 'behavior.noframes' );

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
	xml:lang="<?php echo $this->language; ?>"
	lang="<?php echo $this->language; ?>"
	dir="<?php echo $this->direction; ?>">
<head>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans%3A400italic%2C400%2C600%2C700%7CRoboto+Slab%3A400%2C300%2C700" type="text/css" />
<jdoc:include type="head" />

<!--[if IE 7]>
<link href="templates/<?php echo  $this->template ?>/css/ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->

<script type="text/javascript">
	window.addEvent('domready', function () {
		document.getElementById('form-login').username.select();
		document.getElementById('form-login').username.focus();
	});
</script>
</head>
<body class="contentpane">
	<div class="container">
		<nav role="navigation" class="navbar navbar-default">
				    <div class="everythingOnOneLine">
						<div class="col-lg-12">
							<?php 
								if (!empty($osePanel->oemClass)) {
									echo $osePanel->oemClass->addLogo (); 
								}
								else {
							?>
							<div class="logo">
								<img width="250px" alt="Centrora Logo"
									 src="components/com_ose_firewall/public/images/topbar/whitelogo.png">
							</div>
							<?php 
								}
							?>
							<div id="versions"> <div class="version-updated"> <?php echo $oseVersion; ?></div></div>
							<div class="navbar navbar-default">
								<div class="navbar-collapse collapse navbar-responsive-collapse">
									
								</div>
							</div>
						</div>
					</div>
					<div class="navbar-top">
						 <div class="col-lg-1 col-sm-6 col-xs-6 col-md-6">
								<div class="pull-left">
								</div>
						 </div>
						 <div class="col-lg-11 col-sm-6 col-xs-6 col-md-6">
							 <div class="pull-right">
										<ul class="userMenu ">
										<?php 
											echo $osePanel->getTopBarURL();
										?>
										</ul>
							 </div>
						</div>
					</div>
		</nav>
		<div id="system-message-container"></div>
		<div class="login-page">
			<div id="login" class="animated bounceIn">
				<div class="login-wrapper">
					<div class="tab-content bn" id="myTabContent">
						<div id="log-in" class="tab-pane fade active in">
							<div class="seperator">
								<strong>Administrator Login</strong>
								<hr>
							
							</div>
							<jdoc:include type="message" />
							<jdoc:include type="component" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<noscript>
				<?php echo JText::_('JGLOBAL_WARNJAVASCRIPT')?>
		</noscript>

		<footer>
  		  		<?php echo $osePanel->showFooter(); ?>
		</footer>
	</div>
</body>
</html>

<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
if (!defined('_JEXEC') && !defined('OSE_ADMINPATH')) {
	die("Direct Access Not Allowed");
}

class oseVirusscan
{
	var $file = null;
	var $virus_store = null;
	var $oseJSON = null;

	function __construct()
	{
		$instance = self::getInstanceByVersion();
		if (class_exists("athJSON")) {
			$this->oseJSON = new athJSON();
		}
		else {
			$this->oseJSON = new oseJSON();
		}
		return $instance;
	}

	function getInstance()
	{
		$instance = self::getInstanceByVersion();
		return $instance;
	}

	public static function getInstanceByVersion()
	{
		static $instance;
		if (!empty($instance)) {
			return $instance;
		}
		if (class_exists('SConfig')) {
			if (!defined('DS')) {
				define('DS', DIRECTORY_SEPARATOR);
			}
			require_once(dirname(__FILE__) . DS . 'j15' . DS . 'j15.php');
			$instance = new oseVirusscan_J15();
		}
		else {
			jimport('joomla.version');
			$version = new JVersion();
			$version = substr($version->getShortVersion(), 0, 3);
			if ($version == '1.5') {
				require_once(dirname(__FILE__) . DS . 'j15' . DS . 'j15.php');
				$instance = new oseVirusscan_J15();
			}
			else {
				require_once(dirname(__FILE__) . DS . 'j16' . DS . 'j16.php');
				$instance = new oseVirusscan_J16();
			}
		}
		return $instance;
	}

	function __toString()
	{
		return get_class($this);
	}

	function fileClean($file, $type)
	{
		$exps = self::getPatternExp($type);
		// get content
		$content = JFile::read($file);
		// loop for search string
		$result = false;
		foreach ($exps['type2'] as $pattern) {
			if (preg_match($pattern[1], $content)) {
				$content = preg_replace($pattern[1], "", $content);
				if (function_exists('chmod')) {
					chmod($file, 0777);
				}
				if (JFile::write($file, $content)) {
					$result = true;
				}
				if (function_exists('chmod')) {
					chmod($file, 0644);
				}
			}
		}
		return $result;
	}

	function virusScan($file)
	{
		return null;
	}

	function quarantine($file)
	{
		return null;
	}

	function restore($file, $path)
	{
		return null;
	}

	function getPatternExp($type=null)
	{
		static $defVirus;
		$curDir = dirname(__FILE__);
		switch ($type)
		{
			case null:
			default:
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_virus_definitions.php');
				$oseVirusDefinitions = new oseVirusDefinitions();
			break;
			case 'htaccess':
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_htvirus_def.php');
				$oseVirusDefinitions = new oseHtVirusDefinitions();
			break;
			case 'shellcodes':
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_shvirus_def.php');
				$oseVirusDefinitions = new oseShVirusDefinitions();
			break;
			case 'base64':
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_bsvirus_def.php');
				$oseVirusDefinitions = new oseBsVirusDefinitions();
			break;
			case 'custom':
				require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'custom_definitions.php');
				$oseVirusDefinitions = new oseCSVirusDefinitions();
			break;
		}
				
		$oseVirusDefinitions->setPatterns();
		$defVirus['type2'] = $oseVirusDefinitions->patterns;
		if (empty($defVirus)) {
			return array();
		}
		else {
			return $defVirus;
		}
	}

	function getDefs()
	{
		static $defVirus;
		$curDir = dirname(__FILE__);
		require_once($curDir . DS . 'libraries' . DS . 'assets' . DS . 'ose_virus_definitions.php');
		$oseVirusDefinitions = new oseVirusDefinitions();
		$oseVirusDefinitions->setSignatures();
		$defVirus['type1'] = $oseVirusDefinitions->signatures;
		if (empty($defVirus)) {
			return array();
		}
		else {
			return $defVirus;
		}
	}

	public static function getScanExt()
	{
		return null;
	}

	function filter($list)
	{
		return;
	}

	function getWhiteList()
	{
		return;
	}

	function createConfTable()
	{
		$db = JFactory::getDBO();
		$query = "CREATE TABLE IF NOT EXISTS `#__ose_secConfig` (`id` int(11) NOT NULL AUTO_INCREMENT,`key` text NOT NULL,`value` text NOT NULL, PRIMARY KEY (`id`)) AUTO_INCREMENT=1 ;";
		$db->setQuery($query);
		$db->query();
	}

	function saveConfiguration()
	{
		if (class_exists("athJSON")) {
			$this->oseJSON = new athJSON();
		}
		else {
			$this->oseJSON = new oseJSON();
		}
		self::createConfTable();
		$db = JFactory::getDBO();
		$file_exts = JRequest::getVar('file_ext');
		$file_exts = explode(",", str_replace(array("[","]","\""), "", $file_exts));
		$file_exts = $this->oseJSON->encode($file_exts);
		$data['vsScanExt'] = $file_exts;
		$data['maxfilesize'] = JRequest::getInt('maxfilesize');
		$data['enable_clamav'] = JRequest::getInt('enable_clamav');
		$data['clamavsocket'] = JRequest::getVar('clamavsocket');
		$result = false;
		foreach ($data as $key => $value) {
			$result = self::saveConf($key, $value);
		}
		if ($result == true) {
			$return['success'] = true;
			$return['msg'] = JText::_('CONFIG_SAVED_SUCCESSFULLY');
			$return['status'] = 'DONE';
			$return = $this->oseJSON->encode($return);
		}
		else {
			$return['success'] = false;
			$return['msg'] = JText::_('CONFIG_SAVED_FAILED');
			$return['status'] = 'ERROR';
			$return = $this->oseJSON->encode($return);
		}
		echo $return;
		exit;
	}

	function saveConf($key, $value)
	{
		$db = JFactory::getDBO();
		$query = "SELECT `id` FROM `#__ose_secConfig` WHERE `key` =" . $db->Quote($key, true);
		$db->setQuery($query);
		$result = $db->loadResult();
		if (empty($result)) {
			$query = " INSERT INTO `#__ose_secConfig` " . " (`id`, `key`, `value`, `type`) VALUES " . " (null, " . $db->Quote($key, true) . ", " . $db->Quote($value, true)
					. ", 'vsscan');";
		}
		else {
			$query = " UPDATE `#__ose_secConfig` SET `value` = " . $db->Quote($value, true) . " , `type` = 'vsscan' " . " WHERE `key` = " . $db->Quote($key, true);
		}
		$db->setQuery($query);
		return $db->query();
	}
}

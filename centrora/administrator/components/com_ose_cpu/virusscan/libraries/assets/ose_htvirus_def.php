	<?php
	if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
	{
		die("Direct Access Not Allowed");
	}
	class oseHtVirusDefinitions
	{
		var $signatures =array();
		var $patterns =array();
		function __construct()
		{
		}
		function setPatterns()
		{
		$exp = array();
		
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//.htaccess Codes
	    $exp[] = array( "[H0001] HTAccess Hacking" , "/(RewriteCond\s*\%\{\n*\s*HTTP\_REFERER\}\s*\^\.\*\(google\|ask\|yahoo.*\n*\s*.*)\n*\s*(RewriteCond\s*\%\{\n*\s*HTTP\_REFERER\}\s*\^\.\*\(web\|websuche\|witch.*\n*\s*.*)\n*\s*/");
		$exp[] = array( "[H0001] HTAccess Hacking" , "#(RewriteCond\s*\%\{\n*\s*HTTP\_REFERER\}\s*\^\.\*\(google\|ask\|yahoo.*\n*\s*.*)\n*\s*(RewriteCond\s*\%\{\n*\s*HTTP\_REFERER\}\s*\^\.\*\(web\|websuche\|witch.*\n*\s*.*)\n*\s*#");
		$exp[] = array( "[H0002] HTAccess Hacking" , "/ErrorDocument(?s)\s*[0-9]00\s*http.*?(com|cgi).*?\s*/");
		$exp[] = array( "[H0002] HTAccess Hacking" , "#ErrorDocument(?s)\s*[0-9]00\s*http.*?(com|cgi).*?\s*#");
		$exp[] = array( "[H0003] HTAccess Hacking" , "/<IfModule.*?mod_rewrite(?s).*?RewriteEngine.*?HTTP_REFERER.*?RewriteRule.*?(html|php).*?<\/IfModule>/");
		$exp[] = array( "[H0003] HTAccess Hacking" , "#<IfModule.*?mod_rewrite(?s).*?RewriteEngine.*?HTTP_REFERER.*?RewriteRule.*?(html|php).*?<\/IfModule>#");
			
			
			
			$this->patterns=$exp;
			unset($exp);
		}
	
		function set_version()
		{
			$version = "5.0.13.01.03";
			return $version;
		}
	}
	?>
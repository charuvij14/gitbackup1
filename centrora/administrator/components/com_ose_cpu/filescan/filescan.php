<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
{
	die("Direct Access Not Allowed");
}
class oseFilescan {
	var $oseFile = null;
	function __construct() {}
	function __toString() {
		return get_class($this);
	}
	public static function getInstanceByVersion() {
		static $instance;
		if(!empty($instance)) {
			return $instance;
		}
		jimport('joomla.version');
		$version= new JVersion();
		$version= substr($version->getShortVersion(), 0, 3);
		if($version == '1.5') {
			require_once(dirname(__FILE__).DS.'j15'.DS.'j15.php');
			$instance= new oseFilescan_J15();
		}
		else {
			require_once(dirname(__FILE__).DS.'j16'.DS.'j16.php');
			$instance= new oseFilescan_J16();
		}
		return $instance;
	}
	function getTotal() {
		return null;
	}
	function showcurrent() {
		if(isset($_SESSION['current'])) {
			echo $_SESSION['current'];
		}
	}
	public static function _in_arrayi($needle, $haystack) {
		if(empty($haystack)) {
			return;
		}
		foreach($haystack as $value) {
			if(strtolower($value) == strtolower($needle)) {
				return true;
			}
		}
		return false;
	}
	function initdb($path) {
		return null;
	}
	function chkperms($start, $limit= 300, $total) {
		return null;
	}
	function fixperms($start, $limit= 300, $total) {
		return null;
	}
	function dirList($directory, $recurse, $extensions= array(), $excDirs= array()) {
		return null;
	}
	function insert($filepath, $ext) {
		return null;
	}
	function initDirList($directory, $recurse, $extensions, $excDirs, $count, $countSpeed=50)
	{
		return null;
	}
}
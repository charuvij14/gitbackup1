<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
{
	die("Direct Access Not Allowed");
}
class oseFolder {
	function __construct() {}
	function __toString() {}
	function getFoldersFromDirectory($folder, $filter= '.', $recurse= true, $fullpath= true, $exclude= array('.svn', 'CVS')) // iterate
	{
		if (!ini_set('memory_limit', '512M'))
		{
			echo JText::_("INI_SET function is not allowed in your hosting.");
			exit;
		}
		set_time_limit(0);
		// To check if the file / folder exitst. To avoid
		$files= $this->folders($folder, $filter, $recurse, $fullpath, $exclude);
		return $files;
	}
	function getFilesFromFolder($folder, $filter= '.', $recurse= true, $fullpath= true, $exclude= array('.svn', 'CVS')) // iterate
	{
		if (!ini_set('memory_limit', '512M'))
		{
			echo JText::_("INI_SET function is not allowed in your hosting.");
			exit;
		}
		set_time_limit(0);
		// To check if the file / folder exitst. To avoid
		$files= $this -> files($folder, $filter, $recurse, $fullpath, $exclude);
		return $files;
	}
	// Need VirusScan
	function getItemsFromDB($exts, $start, $limit= 150, $exclude= array()) {
		$db= JFactory :: getDBO();
		if(!empty($exts)) {
			if (!is_array($exts))
			{
				$exts = explode("\n", $exts);
			}
			$exts= "('".implode("','", $exts)."')";
		}
		$exclude=(!empty($exclude)) ? " AND `filepath` NOT IN ('".implode("','", $exclude)."')" : '';
		$limit= empty($limit) ? '' : " LIMIT {$start}, {$limit}";
		$query= "SELECT `filepath` FROM `#__oseav_scanitems`  WHERE `ext` IN {$exts} ".$exclude.$limit;
		$db->setQuery($query);
		$results= $db->loadObjectList();
		return $results;
	}
	function getCountItemsFromDB($exts, $start, $limit= 150, $exclude= array())
	{
		$db= JFactory :: getDBO();
		if(!empty($exts)) {
			if (!is_array($exts))
			{
				$exts = explode("\n", $exts);
			}
			$exts= "('".implode("','", $exts)."')";
		}
		$exclude=(!empty($exclude)) ? " AND filepath NOT IN ('".implode("','", $exclude)."')" : '';
		$limit= empty($limit) ? '' : " LIMIT {$start}, {$limit}";
		$query= "SELECT COUNT(id) FROM `#__oseav_scanitems`  WHERE `ext` IN {$exts} ".$exclude.$limit;
		$db->setQuery($query);
		$result= $db->loadResult();
		return $result;
	}
    function folders($path, $filter = '.', $recurse = false, $fullpath = false, $exclude = array('.svn', 'CVS'))
	{
		// Initialize variables
		$arr = array();
		// Check to make sure the path valid and clean
		$path = JPath::clean($path);
		// Is the path a folder?

		if (!is_dir($path)) {
			JError::raiseWarning(21, 'JFolder::folder: ' . JText::_('Path is not a folder'), 'Path: ' . $path);
			return false;
		}
		// read the source directory
		if($this->isPathReadable($path))
		{
		if (!$handle = opendir($path))
		{
		   $this->show_error('not_readable', $path);
		}
		while (($file = readdir($handle)) !== false)
		{
			if (($file != '.') && ($file != '..') && ($file != '.htaccess') && (!in_array($file, $exclude))) {
				$dir = $path . DS . $file;
				$isDir = is_dir($dir);
				if ($isDir) {
					// Removes filtered directories
					if (preg_match("/$filter/", $file)) {
						if ($fullpath) {
							$arr[] = $dir;
						} else {
							$arr[] = $file;
						}
					}
					if ($recurse) {
						if (is_integer($recurse)) {
							$arr2 = $this->folders($dir, $filter, $recurse - 1, $fullpath);
						} else {
							$arr2 = $this->folders($dir, $filter, $recurse, $fullpath);
						}
						$arr = array_merge($arr, $arr2);
					}
				}
			}

		}
		closedir($handle);
		asort($arr);
		return $arr;
		}
		else {
         $this->show_error('not_readable', $path);
		}
	}

	function show_error($type, $path)
	{
		$oseJSON = new oseJSON();
		switch ($type)
		{
			case 'not_readable':
			$return['status']= "Error";
			$return['type']= $type;
			$return['error']= JText::_("File path")."<br /><span>".$path."</span><br/>".JText::_("is not readable");
			$return= $oseJSON -> encode($return);
			echo $return;
			exit;
			break;
		}

	}

	function files($path, $filter = '.', $recurse = false, $fullpath = false, $exclude = array('.svn', 'CVS'))
	{
		// Initialize variables
		$arr = array();
		// Check to make sure the path valid and clean
		$path = JPath::clean($path);
		// Is the path a folder?
		if (!is_dir($path)) {
			JError::raiseWarning(21, 'JFolder::files: ' . JText::_('Path is not a folder'), 'Path: ' . $path);
			return false;
		}
		if($this->isPathReadable($path))
		{
		$handle = opendir($path);
		while (($file = readdir($handle)) !== false)
		{
			if (($file != '.') && ($file != '..') && (!in_array($file, $exclude))) {
				$dir = $path . DS . $file;
				$isDir = is_dir($dir);
				if ($isDir) {
					if ($recurse) {
						if (is_integer($recurse)) {
							$arr2 = $this->files($dir, $filter, $recurse - 1, $fullpath);
						} else {
							$arr2 = $this->files($dir, $filter, $recurse, $fullpath);
						}

						$arr = array_merge($arr, $arr2);
					}
				} else {
					if (preg_match("/$filter/", $file)) {
						if ($fullpath) {
							$arr[] = $path . DS . $file;
						} else {
							$arr[] = $file;
						}
					}
				}
			}
		}
		closedir($handle);

		asort($arr);
		return $arr;
		}
		else {
         $this->show_error('not_readable', $path);
		}

	}
	function isPathReadable($file_path)
	{
     if (is_readable($file_path))
     {
     	return true;
     }
     else
	 {return false;}
	}

	function isPathIgnored($file_path)
	{
     if (ereg('upload', $file_path))
     {
     	return true;
     }
     else
	 {return false;}
	}
}
?>

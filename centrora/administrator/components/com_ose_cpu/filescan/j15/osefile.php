<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
{
	die("Direct Access Not Allowed");
}
class oseFile {
	function __construct() {}
	function __toString() {}
	function getExt($file) {
		return JFile :: getExt($file);
	}
	public function getDefsFromXML($xmlfilepath)
    {
        if (extension_loaded('SimpleXML'))
        {
             if (file_exists($xmlfilepath))
                {
                    if (LIBXML_VERSION >= 20621)
                    {
                        $definitions = simplexml_load_file($xmlfilepath, null, LIBXML_COMPACT) ;
                    } else
                    {
                        $definitions = simplexml_load_file($xmlfilepath) ;
                    }
                }
            /*
            * In case we still don't have any filters loaded and exception
            * will be thrown
            */
            if (empty($definitions))
            {
                echo ('XML data could not be loaded. Make sure you specified the correct path.') ;
                exit;
            }
            /*
            * Now the storage will be filled with IDS_Filter objects
            */
            $data = array() ;

            $nocache = $definitions instanceof SimpleXMLElement ;
            $definitions = $nocache ? $definitions->filter : $definitions ;
            foreach ($definitions as $filter)
            {
                $id = $nocache ? (string)$filter->id : $filter['id'] ;
                $rule = $nocache ? (string)$filter->rule : $filter['rule'] ;
                $impact = $nocache ? (string)$filter->impact : $filter['impact'] ;
                $tags = $nocache ? array_values((array)$filter->tags) : $filter['tags'] ;
                $description = $nocache ? (string)$filter->description : $filter['description'] ;
                $data[] = array(
						'id' => $id,
						'rule' => $rule,
						'impact' => $impact,
						'tags' => $tags,
                    	'description' => $description
                    	);
            }
        } else
        {
            echo 'SimpleXML not loaded.' ; exit;
        }
        return $data;
    }
}
?>

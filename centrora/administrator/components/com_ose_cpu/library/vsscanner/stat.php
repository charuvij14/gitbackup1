<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
/* no direct access
This class deals with logging, and generating statistics for OSE tables;
*/
if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
{
	die("Direct Access Not Allowed");
}
class oseVirusscanStat extends oseVirusScanner{
	public function __construct() {
		
	}
	public function truncateScanlogs() {
		$db= JFactory :: getDBO();
		$query= "TRUNCATE TABLE `#__oseav_scanlogs`";
		$db->setQuery($query);
		if($db->query()) {
			return true;
		} else {
			return false;
		}
	}
	public function getLastScanLog()
	{
		$total= self::getTotalScanItems();
        $db = JFactory::getDBO();
        $query = "SELECT * FROM `#__oseav_scanlogs` ORDER BY `datetime` DESC LIMIT 1";
        $db->setQuery($query);
        $results = $db->loadObject();
        if (!empty($results))
        {
	        $return ['lsclean'] = $total-$results->file_num;
	        $return ['lstime'] = $results->datetime;
	        $return ['lsinfected'] = $results->file_num;
	        $return ['lsresults'] = $results->result;
        }
        else
        {
	        $return ['lsclean'] = 0;
	        $return ['lstime'] = "N/A";
	        $return ['lsinfected'] = 0;
	        $return ['lsresults'] = "N/A";
        }
        return $return;
	}

	public function getTotalScanItems($exts= array()) {
		if(empty($exts)) {
			$exts= parent::getScanExt();
		}
		$fileScan= new oseFileScan();
		$total= $fileScan->getCountItemsFromDB($exts, 0, 0, null);
		return $total; 
	}
	function truncateDetected() {
		$db= JFactory :: getDBO();
		$query= "TRUNCATE TABLE `#__oseav_detected`";
		$db->setQuery($query);
		if($db->query()) {
			return true;
		} else {
			return false;
		}
	}
	function updateStatus() {
		$db= JFactory :: getDBO();
		$query= "UPDATE `#__oseav_scanitems` SET `state` = 0 ";
		$db->setQuery($query);
		if($db->query()) {
			return true;
		} else {
			return false;
		}
	}
	function getDetectedTotal() {
		$db= JFactory :: getDBO();
		$query= "SELECT count(*) FROM  `#__oseav_detected`";
		$db->setQuery($query);
		return $db->loadResult();
	}
	function getWhitelistedTotal() {
		$db= JFactory :: getDBO();
		$query= "SELECT count(id) FROM  `#__oseav_whitelisted`";
		$db->setQuery($query);
		return $db->loadResult();
	}
	function getQuarantineTotal()
	{
        $db = JFactory::getDBO();
        $query = "SELECT count(id) FROM `#__oseav_quarantined`";
        $db->setQuery($query);
        $result = $db->loadResult();
        return $result;
	}
	function cleanResult($fileids){
		$db= JFactory :: getDBO();
		$query= "DELETE FROM `#__oseav_detected` WHERE id IN (".$fileids.")";
		$db->setQuery($query);
		$result = $db->query();
		return $result; 
	}
	function getResult() {
		$db= JFactory :: getDBO();
		$start=JRequest::getInt('start');
		$start = (empty($start))?0:$start;
		$limit=JRequest::getInt('limit');
		$limit = (empty($limit))?0:$limit;
		if ($limit==0 && $start==0)
		{
			$query= "SELECT * FROM  `#__oseav_detected` ";
		}
		else
		{
			$query= "SELECT * FROM  `#__oseav_detected` LIMIT ". (int)$start." , ". (int)$limit;
		}
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	function getCompleted() {
		$db= JFactory :: getDBO();
		$query= "SELECT COUNT(id) FROM `#__oseav_scanitems` WHERE state= 0 ";
		$db->setQuery($query);
		$completed = $db->loadResult();
		return $completed;
	}
	public function getTotal() {
		$db= JFactory :: getDBO();
		$query= "SELECT COUNT(id) FROM `#__oseav_scanitems` ";
		$db->setQuery($query);
		$total = $db->loadResult();
		return $total;
	}
	function logscanHistory() {
		$db= JFactory :: getDBO();
		$filenum = self::getDetectedTotal();
		if ($filenum>0)
		{
			$details= JText :: _("Virus found");
		}
		else
		{
			$details= JText :: _("No suspicious files");
		}
		$query= "INSERT INTO `#__oseav_scanlogs` (`datetime`,`file_num`,`result`,`action`) VALUES
						(Now(),'{$filenum}','{$details}','scan')";
		$db->setQuery($query);
		return $db->query();
	}
	function result2details($result) {
		$objs= $result;
		$db= JFactory :: getDBO();
		$details= array();
		if(count($objs) > 0) {
			foreach($objs as $obj) {
				$details[]= '['.$obj->virus.']'.'-'.$obj->filepath;
			}
			if(!empty($details)) {
				$details= implode("<j>", $details);
			}
		} else {
			$details= JText :: _("No virus found");
		}
		return $details;
	}
	function cleanHistory($msgs) {
		$db= JFactory :: getDBO();
		$num= count($msgs);
		if(!empty($msgs)) {
			$msg= mysql_real_escape_string(implode("<j>", $msgs));
		}
		$query= "INSERT INTO `#__oseav_scanlogs` (action,file_num,result,datetime) VALUES ('clean','{$num}','{$msg}',Now())";
		$db->setQuery($query);
		return $db->query();
	}
	/* Write the action log
	 * action value:
		 * 1-backup
		 * 2-clean
		 * 3-quarantine
		 * 4-whitelist
		 * 5-whitelist removal
		 * 6-quarantine removeal
		 * 7-restore
	*/
	function actionLog($action,$filepath, $status) {
		$db= JFactory :: getDBO();
		$query= "INSERT INTO `#__scan_action_log` (action,file, result,datetime) VALUES ('{$action}','{$filepath}','{$status}',Now())";
		$db->setQuery($query);
		return $db->query();
	}
	function restoreHistory($msgs) {
		$db= JFactory :: getDBO();
		$num= count($msgs);
		if(!empty($msgs)) {
			$msg= mysql_real_escape_string(implode("<j>", $msgs));
		}
		$query= "INSERT INTO `#__oseav_scanlogs` (action,file_num,result,datetime) VALUES ('restore','{$num}','{$msg}',Now())";
		$db->setQuery($query);
		return $db->query();
	}
}
?>
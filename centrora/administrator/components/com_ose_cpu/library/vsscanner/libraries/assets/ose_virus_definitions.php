<?php
if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
{
 die("Direct Access Not Allowed");
}
class oseVirusDefinitions
{
	var $signatures =array();
	var $patterns =array();
	function __construct()
	{
	}
	function setSignatures()	{
		$virus = array();
		
		
		$virus[] = array( 'Possible Virus Codes' , 'viagra' );
		$virus[] = array( 'Possible Virus Codes' , 'Yogyacarderlink' );
		$virus[] = array( 'Possible Virus Codes' , 'itokenv2.03.exe' );
		$virus[] = array( 'Possible Virus Codes' , 'Gabby' );
			
		$this->signatures=$virus;
		unset($virus);
	}
	function setPatterns()
	{
		$exp = array();
		
		
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
		//JS Injections
		$exp[]= array( "[J1001] Reported JS Injection" ,"/<(S|s)....(T|t)(?s).*?<\!--.*?[$][$][$].*?<\/(S|s)....(T|t)>/");
		$exp[]= array( "[J1002] Reported JS Injection" ,"/<\?(?s).*?echo.*?<script.*?eval\(function.*?String.fromCharCode.*?split.*?<\/script>\s*;.*?\?>/");
		$exp[]= array( "[J1003] Reported JS Injection" ,"/<script(?s).*?(eval|var).*?String.fromCharCode.*?(unescape|split).*?<\/script>/");
		$exp[]= array( "[J1004] Reported JS Injection" ,'/function(?s).*?var.*?document.createElement.*?iframe.*?document.body.appendChild.*?window.onload.*?;/');
		$exp[]= array( "[J1005a] Reported JS Injection" ,'/echo(?s)\s*(\"|\').*?<script.*?document.createElement.*?iframe.*?document.write.*?document.getElement.*?appendChild.*?\/script>.*?(\"|\')/');
		$exp[]= array( "[J1005b] Possible JS Injection - Please Review" ,'/<script.*?document.createElement.*?iframe.*?document.write.*?document.getElement.*?appendChild.*?\/script>/');
		$exp[]= array( "[J1005c] Possible JS Injection - Please Review" ,'/(var|function.*?var)document.createElement.*?iframe.*?document.write.*?document.getElement.*?appendChild.*;/');
		$exp[]= array( "[J1006a] Possible JS Injection" ,'/echo(?s)\s*(\"|\').*?<script.*?(try.*?window|window.*?try).*?document.body.*?(catch.*?rray|rray.*?catch|eval).*?from.*?C.*?h.*?a.*?r.*?C.*?o.*?d.*?e.*?<\/script>.*?(\"|\')/');
		$exp[]= array( "[J1006b] Possible JS Injection" ,'/<script.*?(try.*?window|window.*?try)(?s).*?document.body.*?(catch.*?rray|rray.*?catch|eval).*?from.*?C.*?h.*?a.*?r.*?C.*?o.*?d.*?e.*?<\/script>/');
		$exp[]= array( "[J1007] Reported JS Injection" ,'/<script.*?(trackalyze.js\s*\">|function\s*oS|var\s+WL=\"f3.*?b\(WL\)|var\s*source\s*=(?s).*\)|var.*?unescape\((\'|\")).*?<\/script>/');
		$exp[]= array( "[J1008] Reported JS Injection" ,'/<ad><script.*?var\s*[a-z|A-Z|0-9]{1,}=.*?\s*><\/ad>/');
		$exp[]= array( "[J1008] Reported JS Injection" ,'#<ad><script.*?var\s*[a-z|A-Z|0-9]{1,}=.*?\s*><\/ad>/#');
		$exp[]= array( "[J1009] Possible JS Injection" ,"/<script>\s*function\s*(?s).*?css.*?<\/script>/");
		$exp[]= array( "[J1009] Possible JS Injection" ,"#<script>\s*function\s*(?s).*?css.*?<\/script>#");
		$exp[]= array( "[J1010] Possible JS Injection" ,'/<script>\s*date\s*=.*?Date(?s).*\);\s*<\/script>/');
		$exp[]= array( "[J1010] Possible JS Injection" ,'#<script>\s*date\s*=.*?Date(?s).*\);\s*<\/script>#');
		$exp[]= array( "[J1011] Reported JS Injection" ,'/<script>\s*var.*?=.*?(Replace|replace).*?\);\s*<\/script>/');
		$exp[]= array( "[J1011] Reported JS Injection" ,'#<script>\s*var.*?=.*?(Replace|replace).*?\);\s*<\/script>#');
		$exp[]= array( "[J1012] Possible JS Injection" ,"/<script(?s).*?document.*?setAttribute.*?setTimeout.*?<\/script>/");
		$exp[]= array( "[J1012] Possible JS Injection" ,"#<script(?s).*?document.*?setAttribute.*?setTimeout.*?<\/script>#");
		$exp[]= array( "[J1013] Possible JS Injection" ,"/<script>(?s)\s*try\{\s*document.asd.removeChild.*?createTextNode.*?<\/script/");
		$exp[]= array( "[J1013] Possible JS Injection" ,"#<script>(?s)\s*try\{\s*document.asd.removeChild.*?createTextNode.*?<\/script#");
		$exp[]= array( "[J1014] Possible JS Injection" ,"/<script(?s).*?\:.*?\/.*?concat.*?setAttribute.*?<\/script>/");
		$exp[]= array( "[J1014] Possible JS Injection" ,"#<script(?s).*?\:.*?\/.*?concat.*?setAttribute.*?<\/script>#");
		$exp[]= array( "[J1015] Possible JS Injection" ,"/<script(?s).*?document.createTextNode.*?eval.*?<\/script>/");
		$exp[]= array( "[J1015] Possible JS Injection" ,"#<script(?s).*?document.createTextNode.*?eval.*?<\/script>#");
		$exp[]= array( "[J1016] Possible JS Injection" ,"/<script(?s).*?(if\s*\(window.*?prototype.*?catch|if\s*\(window.*?substr.*?concat.*?split|if\s*\(window.*?location.*?catch.*?split|catch.*?if\s*\(window.*?split.*?tring|prototype.*?catch.*?window).*?;<\/script>/");
		$exp[]= array( "[J1016] Possible JS Injection" ,"#<script(?s).*?(if\s*\(window.*?prototype.*?catch|if\s*\(window.*?substr.*?concat.*?split|if\s*\(window.*?location.*?catch.*?split|catch.*?if\s*\(window.*?split.*?tring|prototype.*?catch.*?window).*?;<\/script>#");
		$exp[]= array( "[J1017] Possible JS Injection" ,"/<script>(?s).*?var.*?x65.*?document.*?<\/script>/");
		$exp[]= array( "[J1017] Possible JS Injection" ,"#<script>(?s).*?var.*?x65.*?document.*?<\/script>#");
		$exp[]= array( "[J1018] Possible JS Injection" ,"/<script.*?src=\"http.*?campaignid=.*?type=tracking\"><\/script>/");
		$exp[]= array( "[J1018] Possible JS Injection" ,"#<script.*?src=\"http.*?campaignid=.*?type=tracking\"><\/script>#");
		$exp[]= array( "[J1019] Possible JS Injection" ,"/<script>(?s).*?var.*?navigator.userAgent.toLowerCase.*?document.getElementsBy.*?setAttribute.*?appendChild.*?<\/script>/");
		$exp[]= array( "[J1019] Possible JS Injection" ,"#<script>(?s).*?var.*?navigator.userAgent.toLowerCase.*?document.getElementsBy.*?setAttribute.*?appendChild.*?<\/script>#");
		$exp[]= array( "[J1020] Possible JS Injection" , "/<script.*?src=\"http.*?(\.ru|\.js).*?\"><\/script>/");
		$exp[]= array( "[J1020] Possible JS Injection" , "#<script.*?src=\"http.*?(\.ru|\.js).*?\"><\/script>#");
		$exp[]= array( "[J1021] Possible JS Injection" ,"/<script>\s*var.*?[\]u.*?[%].*?function.*?document.write\(decodeURIComponent\(.*?split.*?substr.*?return.*?<\/script>/");
        $exp[]= array( "[J1021] Possible JS Injection" ,"#<script>\s*var.*?[\]u.*?[%].*?function.*?document.write\(decodeURIComponent\(.*?split.*?substr.*?return.*?<\/script>#");
		$exp[]= array( "[J1022] Possible JS Injection" ,"/\/\/(?s).*?\(function\(.*?toLowerCase\(.*?split\(.*?getTime.*?document.cookie.match.*?document.write.*?[}]\)\(\);/");
        $exp[]= array( "[J1022] Possible JS Injection" ,"#\/\/(?s).*?\(function\(.*?toLowerCase\(.*?split\(.*?getTime.*?document.cookie.match.*?document.write.*?[}]\)\(\);#");
		$exp[]= array( "[J1023] Possible JS Injection" ,"/<script>(?s).*?function\(.*?var.*?substring.*?eval\(.*?\);.*?<\/script>/");
        $exp[]= array( "[J1023] Possible JS Injection" ,"#<script>(?s).*?function\(.*?var.*?substring.*?eval\(.*?\);.*?<\/script>#");
		$exp[]= array( "[J1024] Possible JS Injection" ,"/<script>(?s)\s*if\(document.loaded\).*?document.createElement\(.*?document.body.appendChild.*?document.createElement\(.*?iframe.*?.*?visibility[:]hidden.*?<\/script>/");
        $exp[]= array( "[J1024] Possible JS Injection" ,"#<script>(?s)\s*if\(document.loaded\).*?document.createElement\(.*?document.body.appendChild.*?document.createElement\(.*?iframe.*?.*?visibility[:]hidden.*?<\/script>#");
		
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//PHP Injetions
		$exp[]= array( "[P2001] Reported PHP Injection" ,"/print.*?file_get_contents\(base64_decode\(.*?\)\).*?;/");
		$exp[]= array( "[P2001] Reported PHP Injection" ,"#print.*?file_get_contents\(base64_decode\(.*?\)\).*?;#");
		$exp[]= array( "[P2002] Possible PHP Injection" ,"/(Buy|buy)(?s).*?(Cialis|Cheap|viagra).*?(O|o)n-?line.*?/");
		$exp[]= array( "[P2002] Possible PHP Injection" ,"#(Buy|buy)(?s).*?(Cialis|Cheap|viagra).*?(O|o)n-?line.*?#");
		$exp[]= array( "[P2003] Reported PHP Injection" ,"/<\?(?s).*?ini_set\(.*?max_execution_time.*?set_time_limit.*?get_current_user\(.*?mail\(.*?\?>/");
		$exp[]= array( "[P2003] Reported PHP Injection" ,"#<\?(?s).*?ini_set\(.*?max_execution_time.*?set_time_limit.*?get_current_user\(.*?mail\(.*?\?>#");
		$exp[]= array( "[P2004] Possible PHP Injection" ,"/<\?php\s+if\(.*?function_exists\([a-z|A-Z|0-9]{1,}\)\).*?tmp_[a-z|A-Z|0-9]{1,}.*?\?>/");
        $exp[]= array( "[P2004] Possible PHP Injection" ,"#<\?php\s+if\(.*?function_exists\([a-z|A-Z|0-9]{1,}\)\).*?tmp_[a-z|A-Z|0-9]{1,}.*?\?>#");
		$exp[]= array( "[P2005] Possible PHP Injection" ,"/<\?(?s).*?HTTP_HOST.*?HTTP_USER_AGENT.*?preg_replace.*?(ips|ip).(masks|mask).*?preg_match\(.*?\).*?header.*?http.*?(cgi|ru|fr|info|org|us|pl|net|php|cfg|pip|txt|\?).*\?>/");
        $exp[]= array( "[P2005] Possible PHP Injection" ,"#<\?(?s).*?HTTP_HOST.*?HTTP_USER_AGENT.*?preg_replace.*?(ips|ip).(masks|mask).*?preg_match\(.*?\).*?header.*?http.*?(cgi|ru|fr|info|org|us|pl|net|php|cfg|pip|txt|\?).*\?>#");
		$exp[]= array( "[P2006] Possible PHP Injection" ,"/<\!--.*?\(function\s*[a-z|A-Z]{1,}\([a-z|A-Z]{1,}\).*?(var|eval).*?unescape\(.*?\).*?;\s*-->/");
        $exp[]= array( "[P2006] Possible PHP Injection" ,"#<\!--.*?\(function\s*[a-z|A-Z]{1,}\([a-z|A-Z]{1,}\).*?(var|eval).*?unescape\(.*?\).*?;\s*-->#");
		$exp[]= array( "[P2007] Possible PHP Injection" ,"/document\s*.write\(unescape\(.*?google-analytics.com\);/");
        $exp[]= array( "[P2007] Possible PHP Injection" ,"#document\s*.write\(unescape\(.*?google-analytics.com\);#");
		$exp[]= array( "[P2008] Possible PHP Injection - Please Manually Clean The File" ,"/error_reporting\((?s).*?(die.*?(md|MD)5|(md|MD)5.*?die).*?}/");
		$exp[]= array( "[P2008] Possible PHP Injection - Please Manually Clean The File" ,"#error_reporting\((?s).*?(die.*?(md|MD)5|(md|MD)5.*?die).*?}#");
		$exp[]= array( "[P2009] Possible PHP Injection" ,"/<style>\#c[a-z0-9]{1,}.*<\/style>/");
		$exp[]= array( "[P2009] Possible PHP Injection" ,"#<style>\#c[a-z0-9]{1,}.*<\/style>#");
		$exp[]= array( "[P2010] Possible PHP Injection" ,"/[$]ware\s*array.*[$]expl\)\)\)\);\s*/");
		$exp[]= array( "[P2010] Possible PHP Injection" ,"#[$]ware\s*array.*[$]expl\)\)\)\);\s*#");
		$exp[]= array( "[P2011] Possible PHP Injection" ,"/Array.prototype.slice.call\(arguments\).*?catch.*?x6.*?return.*;}/");
		$exp[]= array( "[P2011] Possible PHP Injection" ,"#Array.prototype.slice.call\(arguments\).*?catch.*?x6.*?return.*;}#");
		$exp[]= array( "[P2012] Possible PHP Injection" ,"/[@]error_reporting\(0\);\s*if\s*\([!]isset\(.*?[$].*?\\.*?/");
        $exp[]= array( "[P2012] Possible PHP Injection" ,"#[@]error_reporting\(0\);\s*if\s*\([!]isset\(.*?[$].*?\\.*?#");
		$exp[]= array( "[P2018] Reported PHP Injection" ,"/<\?php.*?\/.*?array\(.*?function_exists\(.*?off.*?\/\s*\?>/");
		$exp[]= array( "[P2018] Reported PHP Injection" ,"#<\?php.*?\/.*?array\(.*?function_exists\(.*?off.*?\/\s*\?>#");
		$exp[]= array( "[P2019] Reported PHP Injection" ,"/<\?php.*?if.*?function_exists\(.*?curl_exec\(.*?break.*?\?>/");
		$exp[]= array( "[P2019] Reported PHP Injection" ,"#<\?php.*?if.*?function_exists\(.*?curl_exec\(.*?break.*?\?>#");
		$exp[]= array( "[P2020] Reported PHP Injection" ,"/var.*?x65.*?navigator.*?document.*?element.*;/");
		$exp[]= array( "[P2020] Reported PHP Injection" ,"#var.*?x65.*?navigator.*?document.*?element.*;#");
		$exp[]= array( "[P2021] Reported PHP Injection" ,"/<\?(?s).*?if\s*\([!]isset\(.*?HTTP_USER_AGENT.*?base64_decode\(.*?REMOTE_ADDR.*?curl_close.*?\?>/");
		$exp[]= array( "[P2021] Reported PHP Injection" ,"#<\?(?s).*?if\s*\([!]isset\(.*?HTTP_USER_AGENT.*?base64_decode\(.*?REMOTE_ADDR.*?curl_close.*?\?>#");
		$exp[]= array( "[P2022] Reported PHP Injection" , '/<\?php\s*system\(.*?(txt|php).*?\?>/');
		$exp[]= array( "[P2022] Reported PHP Injection" , '#<\?php\s*system\(.*?(txt|php).*?\?>#');
		$exp[]= array( "[P2023] Possible PHP Uploader" , '/<\?(?s).*?if\(.*?[$]_POST\[(\'|\").*?[$]_FILES\[(\'|\").*?file.*?(upload|Upload).*?\?>/');
		$exp[]= array( "[P2023] Possible PHP Uploader" , '#<\?(?s).*?if\(.*?[$]_POST\[(\'|\").*?[$]_FILES\[(\'|\").*?file.*?(upload|Upload).*?\?>#');
		$exp[]= array( "[P2024] Reported PHP Injection" ,"/<\?(?s).*?if\s*\([!]empty\(.*?HTTP_USER_AGENT.*?array.*?fopen.*?fclose.*?\?>/");
		$exp[]= array( "[P2024] Reported PHP Injection" ,"#<\?(?s).*?if\s*\([!]empty\(.*?HTTP_USER_AGENT.*?array.*?fopen.*?fclose.*?\?>#");
		$exp[]= array( "[P2025] Reported PHP Injection" ,"/if\s*\(isset\s*\([$]_GET.*?[{].*?[$].*?substr.*?foreach\s*\(array\(.*?urlencode.*?eval\(.*?\);.*?[}]/");
		$exp[]= array( "[P2025] Reported PHP Injection" ,"#if\s*\(isset\s*\([$]_GET.*?[{].*?[$].*?substr.*?foreach\s*\(array\(.*?urlencode.*?eval\(.*?\);.*?[}]#");
		$exp[]= array( "[P2026] Reported PHP Injection" , '/<script>.*?try\{.*?function\(.*?if\(document\.querySelector\).*?getElementById.*?catch.*?eval.*?<\/script>/');
		$exp[]= array( "[P2026] Reported PHP Injection" , '#<script>.*?try\{.*?function\(.*?if\(document\.querySelector\).*?getElementById.*?catch.*?eval.*?<\/script>#');
		
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//iFrame Injections
        $exp[]= array( "[F3001] Possible iFrame - Possible false alert" ,"/<iframe.*?(src=.*?visibility.*?hidden|visibility.*?hidden.*?src).*?<\/iframe>/");
        $exp[]= array( "[F3001] Possible iFrame - Possible false alert" ,"#<iframe.*?(src=.*?visibility.*?hidden|visibility.*?hidden.*?src).*?<\/iframe>#");
		$exp[]= array( "[F3002] iFrame Injection" , "/document.write.*?<iframe.*?src=.*?(\.com|\.cgi|\.ru|\.fr|\.info|\.org|\.us|\.pl|\.net|\.php|\.cfg|\.pip|\.txt|\.html|\?).*?><\/iframe>.*?;/");
		$exp[]= array( "[F3002] iFrame Injection" , "#document.write.*?<iframe.*?src=.*?(\.com|\.cgi|\.ru|\.fr|\.info|\.org|\.us|\.pl|\.net|\.php|\.cfg|\.pip|\.txt|\.html|\?).*?><\/iframe>.*?;#");
		$exp[]= array( "[F3003] iFrame Injection" , '/document.write.*?<script.*?>\s*.r(\'|\")\s*[+](?s).*?<\/script>.*?<iframe.*?>.*?\);/');
		$exp[]= array( "[F3003] iFrame Injection" , '#document.write.*?<script.*?>\s*.r(\'|\")\s*[+](?s).*?<\/script>.*?<iframe.*?>.*?\);#');
		$exp[]= array( "[F3004] Possible iFrame - Possible false alert" , '/<\!.*?><style>div.*?<iframe.*?src=.*?border=0.*?<\/iframe>.*?<\!.*?>/');
		$exp[]= array( "[F3004] Possible iFrame - Possible false alert" , '#<\!.*?><style>div.*?<iframe.*?src=.*?border=0.*?<\/iframe>.*?<\!.*?>#');
		$exp[]= array( "[F3005] iFrame Injection" ,"/<html>\s*<body>\s*<iframe\s*src=\s*(\"|\')(https\?|ftp|gopher|telnet|file|notes|ms-help):\/\/.*?<\/iframe>\s*<\/body>\s*<\/html>/");
		$exp[]= array( "[F3005] iFrame Injection" ,"#<html>\s*<body>\s*<iframe\s*src=\s*(\"|\')(https\?|ftp|gopher|telnet|file|notes|ms-help):\/\/.*?<\/iframe>\s*<\/body>\s*<\/html>#");
		$exp[]= array( "[F3006] iFrame Injection" ,'/<iframe frameborder=\"0\"\s+onload=\"if\s+\(\!this\.src\).*?<\/iframe>/');
		$exp[]= array( "[F3006] iFrame Injection" ,'#<iframe frameborder=\"0\"\s+onload=\"if\s+\(\!this\.src\).*?<\/iframe>#');
		$exp[]= array( "[F3007] Possible iFrame Injection" ,"/echo.*?\((\'|\")<iframe\s*src=.*?<\/iframe>(\'|\")\)\s*;/");
		$exp[]= array( "[F3007] Possible iFrame Injection" ,"#echo.*?\((\'|\")<iframe\s*src=.*?<\/iframe>(\'|\")\)\s*;#");
		$exp[]= array( "[F3008] Possible iFrame Injection" ,"/<iframe\s*name.*?frameborder=(0|no).*?src=\s*http.*?html.*?\/iframe>/");
		$exp[]= array( "[F3008] Possible iFrame Injection" ,"#<iframe\s*name.*?frameborder=(0|no).*?src=\s*http.*?html.*?\/iframe>#");
		$exp[]= array( "[F3009] Possible iFrame Injection" , "/if\(empty\((?s).*?body.*?hidden.*?auto.*?echo.*?}/");
		$exp[]= array( "[F3009] Possible iFrame Injection" , "#if\(empty\((?s).*?body.*?hidden.*?auto.*?echo.*?}#");
		$exp[]= array( "[F3010] iFrame Injection" , "/<script(?s).*?function\s*(dnn|nemo)ViewState\(\).*?Array\(.*?String\.fromCharCode\(.*?document.write.*?<\/script>/");
		$exp[]= array( "[F3010] iFrame Injection" , "#<script(?s).*?function\s*(dnn|nemo)ViewState\(\).*?Array\(.*?String\.fromCharCode\(.*?document.write.*?<\/script>#");

        ///////////////////////////////////////////////////////////////////////////////////////////////////////
		//Other Injections
        $exp[]= array( "[O4001] VB Script Injection" ,"/<(S|s)....(T|t)\s+(L|l)........(VB|vb).....(T|t)>(?s).*(DropFileName|dropfilename).*?<\/(S|s)....(T|t)>/");
		$exp[]= array( "[O4001] VB Script Injection" ,"#<(S|s)....(T|t)\s+(L|l)........(VB|vb).....(T|t)>(?s).*(DropFileName|dropfilename).*?<\/(S|s)....(T|t)>#");
		$exp[]= array( "[O4002] File Injection" ,"/copy\(\"(http:\/\/.*?\".*?index|.*?\.txt).*?\"\);/");
		$exp[]= array( "[O4002] File Injection" ,"#copy\(\"(http:\/\/.*?\".*?index|.*?\.txt).*?\"\);#");
		$exp[]= array( "[O4003] Spam Mailer - Consider Quarantine" ,'/set_time_limit(?s).*?ignore_user_abort.*?function\s*send_mail/');
		$exp[]= array( "[O4003] Spam Mailer - Consider Quarantine" ,'#set_time_limit(?s).*?ignore_user_abort.*?function\s*send_mail#');
		$exp[]= array( "[O4004] Mailer PHP - Please Review" ,"/[$]numemails\s+=\s+count\([$]allemails\).*?/");
		$exp[]= array( "[O4004] Mailer PHP - Please Review" ,"#[$]numemails\s+=\s+count\([$]allemails\).*?#");
		

	  	/*
		$exp[]= array( "[00041] JavaScript Injection" ,"/var\s*[a-zA-Z0-9]=[\"|']{1,};\s*.*?-{0,}.*?onload=[a-zA-Z0-9]{1,}\s*;this[.][a-zA-Z0-9]{1,}=false;/");
		$exp[]= array( "[00041] JavaScript Injection" ,"#var\s*[a-zA-Z0-9]=[\"|']{1,};\s*.*?-{0,}.*?onload=[a-zA-Z0-9]{1,}\s*;this[.][a-zA-Z0-9]{1,}=false;#");
		$exp[]= array( "[00043] JavaScript Injection" ,"/try\s*{var\s*[a-zA-Z0-9]{1,}=[\"|'][a-zA-Z0-9]{1,}[\"|']}\s*catch\([a-zA-Z0-9]{1,}\){};try\s*.*var\s*[a-zA-Z0-9]{1,}=new\s*[s|S]tring\(\);/");
        $exp[]= array( "[00043] JavaScript Injection" ,"#try\s*{var\s*[a-zA-Z0-9]{1,}=[\"|'][a-zA-Z0-9]{1,}[\"|']}\s*catch\([a-zA-Z0-9]{1,}\){};try\s*.*var\s*[a-zA-Z0-9]{1,}=new\s*[s|S]tring\(\);#");
		$exp[]= array( "[00054] Possible iFrame Injection" ,"/<iframe\s*src=.*?<\/iframe>/");
		$exp[]= array( "[00054] Possible iFrame Injection" ,"#<iframe\s*src=.*?<\/iframe>#");
		$exp[]= array( "[00057] Possible PHP Injection" ,"/<img.*src=(\"|')(http|ftp|https){1,1}(s){0,1}:\/\/.*>/");
		$exp[]= array( "[00057] Possible PHP Injection" ,"#<img.*src=(\"|')(http|ftp|https){1,1}(s){0,1}:\/\/.*>#");
		$exp[]= array( "[00057] Possible PHP Injection" ,"/<img\s*height.*src=[\"|']http.*jpg.*[\"|']>/");
		$exp[]= array( "[00057] Possible PHP Injection" ,"#<img\s*height.*src=[\"|']http.*jpg.*[\"|']>#");
		$exp[]= array( "[P2013] Reported PHP Injection" ,"/\<\?php\s*if\s*\(\!isset\([$][a-zA-Z0-9]{1,}\)\)\n+{\n+global\s*[$][a-zA-Z0-9]{1,};\n+[$][a-zA-Z0-9]{1,}\s*=\s*1;\n+([\w|\s\|\/]{1,}\n+)([$][a-zA-Z0-9]{1,}([\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{]{1,})+)\s*(\.\'\?ip\=\'\.urlencode\([$]_SERVER\[\'REMOTE_ADDR\'\]\))\s*\.(([[$][a-zA-Z0-9]{1,}|\.|[$]|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\}|\!|\t]{1,})+)\@curl_exec(([[$][a-zA-Z0-9]{1,}|\.|[$]|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\}|\!|\t]{1,})+)curl_close(([[$][a-zA-Z0-9]{1,}|\.|[$]|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\}|\!|\t]{1,})+)\?\>/");
        $exp[]= array( "[P2013] Reported PHP Injection" ,"#\<\?php\s*if\s*\(\!isset\([$][a-zA-Z0-9]{1,}\)\)\n+{\n+global\s*[$][a-zA-Z0-9]{1,};\n+[$][a-zA-Z0-9]{1,}\s*=\s*1;\n+([\w|\s\|\/]{1,}\n+)([$][a-zA-Z0-9]{1,}([\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{]{1,})+)\s*(\.\'\?ip\=\'\.urlencode\([$]_SERVER\[\'REMOTE_ADDR\'\]\))\s*\.(([[$][a-zA-Z0-9]{1,}|\.|[$]|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\}|\!|\t]{1,})+)\@curl_exec(([[$][a-zA-Z0-9]{1,}|\.|[$]|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\}|\!|\t]{1,})+)curl_close(([[$][a-zA-Z0-9]{1,}|\.|[$]|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\}|\!|\t]{1,})+)\?\>#");
       	$exp[]= array( "[P2014] Reported PHP Injection" ,"/[$]this\-\>(_checkEngine){1,}\(\);\n*(([[$][a-zA-Z0-9]{1,}|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t]{1,})+)\s*(_retrieveData)\(\)\s*\.\s*[$]\w{1,}(\s|;|\n){1,}\}/");
        $exp[]= array( "[P2014] Reported PHP Injection" ,"#[$]this\-\>(_checkEngine){1,}\(\);\n*(([[$][a-zA-Z0-9]{1,}|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t]{1,})+)\s*(_retrieveData)\(\)\s*\.\s*[$]\w{1,}(\s|;|\n){1,}\}#");
		$exp[]= array( "[P2015] Reported PHP Injection" ,"/function\s*(_checkEngine){1,}\(\)\n*\s*\{(\s|\w|\/|\n|\t|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t){1,}.*?\}/");
        $exp[]= array( "[P2015] Reported PHP Injection" ,"#function\s*(_checkEngine){1,}\(\)\n*\s*\{(\s|\w|\/|\n|\t|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t){1,}.*?\}#");
		$exp[]= array( "[P2016] Reported PHP Injection" , "/function\s*_retrieveData{1,}\(\)\n*\s*\{(\?|\s|\w|\/|\n|\t|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t){1,}.*?((\s|\;|\n|\/|\|\w|\t}){1,})+(\s|\w|\n|[$]|\=|\'|\/){1,}\?\w{1,}\=(\'|\s|\.){1,}base64\_encode\(serialize\([$][a-zA-Z0-9]{1,}\)\);(([[$][a-zA-Z0-9]{1,}|\@|\}|\\\*r|\r|\\\*n|\+|\n|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t]{1,})+)}/");
        $exp[]= array( "[P2016] Reported PHP Injection" , "#function\s*_retrieveData{1,}\(\)\n*\s*\{(\?|\s|\w|\/|\n|\t|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t){1,}.*?((\s|\;|\n|\/|\|\w|\t}){1,})+(\s|\w|\n|[$]|\=|\'|\/){1,}\?\w{1,}\=(\'|\s|\.){1,}base64\_encode\(serialize\([$][a-zA-Z0-9]{1,}\)\);(([[$][a-zA-Z0-9]{1,}|\@|\}|\\\*r|\r|\\\*n|\+|\n|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t]{1,})+)}#");
        $exp[]= array( "[P2017] Reported PHP Injection" ,"/if\s*\(\!(isset|defined)\([\"|\'][$]{0,}[a-zA-Z0-9_]{1,}[\"|\']\)\)\s*\{\n*define\(\'(\w){1,}\'\,\s\'[a-zA-Z0-9]\'\)\;\n*function\s[\w0-9_]{1,}\(\)\s*\n*{\n*(([[$][a-zA-Z0-9]{1,}|\@|\}|\\\s*r|\r|\\\s*n|\*|\+|\n|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t]{1,})+)\);\n*.*?((\"(\\\s*(x|[a-zA-Z0-9])[a-zA-Z0-9]{1,2})+\")+\){1,})\s*\{\n*((([[$][a-zA-Z0-9]{1,}|\%|\<|\@|\}|\\\s*r|\r|\\\s*n|\*|\+|\n|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t]{1,})+)(\\\s*(x|[a-zA-Z0-9])[a-zA-Z0-9]{1,2})+\"\;\n+.*).*([$]\w{1,}[\.|\=|\s|\w|\;|\n|\'|\(|\,|\_|\)|\{|\}|\@|\-|\=|\r|\"|\[|\]|\!]{1,})+Zend\@cache\:[a-zA-Z0-9\/\+\=\_\"\,\s\)\;\*]{1,}\n*\}/");
        $exp[]= array( "[P2017] Reported PHP Injection" ,"#if\s*\(\!(isset|defined)\([\"|\'][$]{0,}[a-zA-Z0-9_]{1,}[\"|\']\)\)\s*\{\n*define\(\'(\w){1,}\'\,\s\'[a-zA-Z0-9]\'\)\;\n*function\s[\w0-9_]{1,}\(\)\s*\n*{\n*(([[$][a-zA-Z0-9]{1,}|\@|\}|\\\s*r|\r|\\\s*n|\*|\+|\n|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t]{1,})+)\);\n*.*?((\"(\\\s*(x|[a-zA-Z0-9])[a-zA-Z0-9]{1,2})+\")+\){1,})\s*\{\n*((([[$][a-zA-Z0-9]{1,}|\%|\<|\@|\}|\\\s*r|\r|\\\s*n|\*|\+|\n|\.|[$]|\:|\||\-|\>|\s|\=|\w|\(|\$|\[|\]|\'|\)|;|\n|\/|\"|\,|\&|\{|\!|\t]{1,})+)(\\\s*(x|[a-zA-Z0-9])[a-zA-Z0-9]{1,2})+\"\;\n+.*).*([$]\w{1,}[\.|\=|\s|\w|\;|\n|\'|\(|\,|\_|\)|\{|\}|\@|\-|\=|\r|\"|\[|\]|\!]{1,})+Zend\@cache\:[a-zA-Z0-9\/\+\=\_\"\,\s\)\;\*]{1,}\n*\}#");
		*/

		
        $this->patterns=$exp;
		unset($exp);
	}

	function set_version()
	{
		$version = "5.0.13.01.03";
		return $version;
	}
}
?>

	<?php
	if (!defined('_JEXEC') && !defined('OSE_ADMINPATH'))
	{
		die("Direct Access Not Allowed");
	}
	class oseSHVirusDefinitions
	{
		var $signatures =array();
		var $patterns =array();
		function __construct()
		{
		}
	
		function setPatterns()
		{
		$exp = array();
		
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		//Shell Codes
		$exp[]= array( "[20001] Shell Codes" ,"/system\s*\(.*?wget\s*http.*?(php|txt|php3|php4|php5).*?\);/");
		$exp[]= array( "[20002] Possible Shell Codes" ,"/if\s*\(\s*(function_exists|is_callable)\(('|\")(shell|passthru|system|exec|popen)('|\")\s*\)\s*\)/");
		$exp[]= array( "[20003] Shell codes" ,"/[$]md5(?s).*?wp_salt.*?array.*?wp_add_filter\(.*?;/");
		$exp[]= array( "[20005] Possible Shell Codes" ,"/preg_replace\(.*?x65.*?\)\;/");
		$exp[]= array( "[20006] Possible Shell Codes" ,'/rm -rf.*?/');
		$exp[]= array( "[20007] Possible Shell Codes Injection" ,"/<\?(?s).*?[$].*?array.*?\(.*?eval\(.*?x65.*?>/");
		$exp[]= array( "[20008] Shell Codes" ,"/(r57\s*sh|c99\s*sh|fx29\s*sh|Sq\s*sh|N3t\s*she||web\s*shell|Web\s*shell|Web\s*Shell|Spawn\s*Shell|spawn\s*shell|crystal\s*shell|ijo\.\s*shell|Ynx Shell).*?/");
		$exp[]= array( "[20008] Shell Codes" ,"#(r57\s*sh|c99\s*sh|fx29\s*sh|Sq\s*sh|N3t\s*she||web\s*shell|Web\s*shell|Web\s*Shell|Spawn\s*Shell|spawn\s*shell|crystal\s*shell|ijo\.\s*shell|Ynx Shell).*?#");
		$exp[]= array( "[20009] Possible Shell Codes" ,"/ob_start.*?function.*?update.*?\);}/");
		$exp[]= array( "[20010] Possible Shell Codes" ,"/<\?(?s).*?base64_decode.*?error_reporting\(0\).*?set_time_limit\(0\).*?ini_set.*?\?>/");
		$exp[]= array( "[20011] Possible Shell Codes" ,"/<\?php(?s).*?error_reporting.*?ini_set.*?(decode|fopen.*?fclose).*?;/");
		$exp[]= array( "[20012] Possible Shell Codes" ,"/<\?php(?s).*?set_time_limit.*?afeMode.*?echo.*?hmod.*?\?>/");
		$exp[]= array( "[20013] Possible Shell Codes" ,"/(H|h)acked\s+(By|by).*?/");
		$exp[]= array( "[20014] Reported Shell Codes" ,"/<\?(?s).*?_POST.*?Content-type.*?explode\(.*?echo.*?\?>.*?<(BODY|body)>.*?<\/body>/");
		$exp[]= array( "[20015] Shell Codes" ,"/<\?php(?s).*?GET.*?(\'|\")cmd(\'|\").*?str_replace.*?system.*?\?>/");
		$exp[]= array( "[20016] Shell Codes" ,"/<\?(?s).*?.(x62|142).(x61|141).(x73|163).(x65|145).(x36|066).(x34|064).(x5f|137).(x64|144).(x65|145).(x63|143).(x6f|157).(x64|144).(x65|145).*?eval\(.*\).*(;|\?>)/");
		$exp[]= array( "[20017] Possible Shell Codes" ,"/<\?(?s).*?REMOTE_ADDR.*?(fsockopen|fopen).*?fclose.*?/");
		$exp[]= array( "[20018] Possible Shell Codes" ,"/<\?(?s).*?file_get_contents.*?preg_split.*?preg_match.*?file_put_contents.*(;|\?>)/");
		$exp[]= array( "[20019] Possible Shell Codes" ,"/<\?(?s).*?get_magic_quotes_gpc.*?php_uname.*?fopen.*?fclose.*?upload.*?\?>/");
		$exp[]= array( "[20020] Possible Shell Codes" ,"/<\?.*?(?s).*?[$]_SERVER\[(\'|\")HTTP_USER_AGENT(\'|\")\].*?[$]_SERVER\[(\'|\")HTTP_REFERER(\'|\")\].*?fopen.*?[$]filepath.*?fread.*?\?>/");
		$exp[]= array( "[20021] Possible Shell Codes Uploader" ,"/[$](?s).*?(fsockopen|fopen).*?fwrite.*?fclose.*?/");
		$this->patterns=$exp;
		unset($exp);
		}
	
		function set_version()
		{
			$version = "5.0.13.01.03";
			return $version;
		}
	}
	?>

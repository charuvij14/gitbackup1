<?php
/**
 * @version     6.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @author        Updated on 30-Mar-2013 
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 * @copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

if (!defined('_OSEEXEC') && !defined('OSE_ADMINPATH')) {
	die('Direct Access Not Allowed');
}
class oseEmail {
	private $table = '#__ose_app_email';
	private $view = '#__ose_app_adminemailmap';
	private $app = null ;
	private $oseCPU = null ;  
	public function __construct($app)
	{
		$this->oseCPU = new oseCPU(); 
		$this->app = $app; 
	}
	public function getEmailList()
	{
		$db = oseDBO::instance();
		$query = " SELECT * FROM `{$this->table}`"
				." WHERE `app` = ".$db->Quote($this->app);
		$db->setQuery($query);
		return $db->loadObjectList();	
	}
	public function getEmailListTotal()
	{
		$db = oseDBO::instance();
		$query = " SELECT COUNT(`id`) FROM `{$this->table}`"
				." WHERE `app` = ".$db->Quote($this->app);
		$db->setQuery($query);
		return $db->loadResult();
	}
	public function getAdminEmailList()
	{
		$limit = $this->oseCPU->getInt('limit', 25);
		$start = $this->oseCPU->getInt('start', 0);
		$page = $this->oseCPU->getInt('page', 1);
		$search = $this->oseCPU->getVar('search', null);
		$start = $limit * ($page-1);    
		$list = $this -> getAdminEmailListDB($search, $start, $limit); 
		return $list; 
	}
	public function getAdminEmailListDB($search, $start, $limit)
	{
		$db = OSEDBO::instance();
		$where = array(); 
		if (!empty($search))
		{
			$where[] = "`subject` LIKE ".$db->Quote('%'.$search.'%', true);
		}
		$where[] = "`app` = ".$db->Quote($this->app);
		$where = $db->implodeWhere($where);
		$query = " SELECT * FROM `{$this->view}`"
				.$where;
		$db->setQuery($query);
		return $db->loadObjectList();	
	}
	public function getEmailParams($id)
	{
		$result = $this->getEmailParamsDB($id);
		$params = oseJSON::decode($result);
		$return = array();
		$i = 0 ;
		foreach ($params as $key => $value)
		{
			$return[$i]['key'] = $key;
			$return[$i]['value'] = $value;
			$i++;
		}
		return $return;
	}
	private function getEmailParamsDB($id)
	{
		$db = oseDBO::instance();
		$where= array();
		$where[]= " `id` = ". (int)$id;
		$where= $db -> implodeWhere($where);
		$query= " SELECT `params` FROM `#__ose_app_email` " .$where;
		$db->setQuery($query);
		$result= $db->loadResult();
		return $result;  
	}
	public function getEmail($id)
	{
		$db = oseDBO::instance();
		$query = " SELECT `id`, `subject` as emailSubject, `body` as emailBody, `type` as emailType  FROM `{$this->table}` WHERE `id` = ".(int)$id;
		$db->setQuery($query);
		$item = $db->loadObject();
		return $item; 
	}
	public function saveemail($id, $emailType, $emailBody, $emailSubject)
	{
		$db = OSEDBO::instance();
	 	$varValues = array (
	 			'type' => $emailType,
				'subject' => $emailSubject,
				'body' => $emailBody
		);
		$id = $db->addData('update', '#__ose_app_email', 'id', (int)$id, $varValues);
		if ($id == true)
		{
			return $id; 
		}
		else
		{
			return false; 
		}
	}
	public function addadminemailmap($userid, $emailid)
	{
		$db = OSEDBO::instance();
		$admin_id = $this->addadminid($userid);
		$varValues = array(
					'admin_id' => (int)$admin_id,
					'email_id' => (int)$emailid
				);
		$id = $db->addData ('insert', '#__ose_app_adminrecemail', '', '', $varValues);
		return $id; 
	}
	private function addadminid($userid)
	{
		$db = OSEDBO::instance();
		$varValues = array(
					'id' => 'DEFAULT',
					'user_id' => (int)$userid
				);
		$id = $db->addData ('insert', '#__ose_app_admin', '', '', $varValues);
		return $id; 
	}
	public function deleteadminemailmap($admin_id, $email_id)
	{
		$db = OSEDBO::instance();
		return $db->deleteRecord(array('admin_id'=>$admin_id, 'email_id'=>$email_id), '#__ose_app_adminrecemail');
	}
	public function getEmailByType($type)
	{
		$db = oseDBO::instance();
		$query = " SELECT `id`, `subject` as emailSubject, `body` as emailBody, `type` as emailType  FROM `{$this->table}` WHERE `type` = ".$db->Quote($type, true)." LIMIT 1";
		$db->setQuery($query);
		$item = $db->loadObject();
		return $item; 
	}
	public function sendMail($email, $config_var)
	{
		$receiptients = $this ->getReceiptients($email->id); 
		require_once(OSECPU_LIB_PATH.DS.'emails'.DS.'oseEmailHelper.php');
		require_once(OSECPU_LIB_PATH.DS.'emails'.DS.'phpmailer'.DS.'phpmailer.php');
		require_once(OSECPU_LIB_PATH.DS.'emails'.DS.'phpmailer'.DS.'smtp.php');
		if (empty($receiptients))
		{
			return false;
		}
		foreach($receiptients as $receiptient) 
		{
			$email->body = str_replace('[user]', $receiptient->name, $email->body);
			$mailer = new PHPMailer(); 
			$mailer->From = $config_var->mailfrom;
			$mailer->FromName = $config_var->fromname;
			if ($config_var->mailer=='smtp')
			{
				$mailer->useSMTP($config_var->smtpauth, $config_var->smtphost, $config_var->smtpuser, $config_var->smtppass, $config_var->smtpsecure, $config_var->smtpport);
			}
						
			$this->addRecipient($mailer, $receiptient->email);
			$mailer->Subject = OSEMailHelper::cleanLine($email->subject);
			$mailer->Body = OSEMailHelper::cleanText($email->body);
			$mailer->IsHTML(true);
			$mailer->Send();
		}
		return true; 
	}
	private function addRecipient(&$mailer, $recipient)
	{
	
		// If the recipient is an array, add each recipient... otherwise just add the one
		if (is_array($recipient))
		{
			foreach ($recipient as $to)
			{
				$to = OSEMailHelper::cleanLine($to);
				$mailer->AddAddress($to);
			}
		}
		else
		{
			$recipient = OSEMailHelper::cleanLine($recipient);
			$mailer->AddAddress($recipient);
		}
	
	}
	private function getReceiptients($emailid)
	{
		$db = oseDBO::instance();
		$query = "SELECT `name`, `email` FROM `#__ose_app_adminemailmap` where `email_id` = ". (int)$emailid;
		$db->setQuery($query);
		$items = $db->loadObjectList();
		return $items;  
	}
	
}
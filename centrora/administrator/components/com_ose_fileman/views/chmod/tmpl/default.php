<?php
/**
 * @version     4.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence File Manager
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 23-Apr-2012
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */

defined('_JEXEC') or die("Direct Access Not Allowed");
?>

<div id="oseheader">
	<div class="container">
		<div class="logo-labels">
			<h1>
				<a href="http://www.opensource-excellence.com" target="_blank"><?php echo JText::_("Open Source Excellence"); ?>
				</a>
			</h1>
				<?php
					echo $this->preview_menus; 
				?>
		</div>
				<?php
					oseSoftHelper::showmenu();
				?>
	<div class="section">
		<div id="sectionheader"><?php echo $this->title; ?></div>
		<div class="grid-title"><?php echo JText::_('Please change your file permission here').'.'.JText::_("The file being changed is").": <div id ='fileName' >".$this->abs_item.'</div>';  ?></div>
		<div id ='ose-fileman-edit'>
		
<?php
	$mode = parse_file_perms(get_file_perms( $this->abs_item ));
	if($mode===false) {
		show_error($this->abs_item.": ".$GLOBALS["error_msg"]["permread"]);
	}
	$pos = "rwx";

	echo $GLOBALS["messages"]["actperms"]; 
	?>
	
	<table class="adminform" >

	<?php 	
	// print table with current perms & checkboxes to change
	for($i=0;$i<3;++$i) {
		echo "<tr><td>" . $GLOBALS["messages"]["miscchmod"][$i] . "</td>";
		for($j=0;$j<3;++$j) {
			echo "<td>";
			$tmp = $pos{$j};
			switch ($tmp)
			{
				case 'r':
					$tmp = JText::_('Read'); 
					break;
				case 'w':
					$tmp = JText::_('Write');
					break;
				case 'x':
					$tmp = JText::_('Execute');
					break;
			} 
			echo "<label for='".$pos{$j}.$i."'>".$tmp."&nbsp;</label>";
			echo "<input type='checkbox'";
			if($mode{(3*$i)+$j} != "-") 
			{
				echo " checked='checked'";
			}
			echo " name='".$pos{$j}.$i."' id='".$pos{$j}.$i. "' value='1' />";
			echo "</td>";
		}
		echo "</tr>";
	}
	?>	
	</table>
	<table class="adminform" >
		<tr>
				<?php
				if (is_dir ($this->abs_item))
				{
					?>
					<td colspan="2" style="float:right">
						<input name="do_recurse" id="do_recurse" type="checkbox" value="1" />
						<label for="do_recurse"><?php echo $GLOBALS["messages"]["recurse_subdirs"] ; ?> </label>
					</td>
				<?php 	
				}
				else
				{
					?>
					<td colspan="2" style="float:right">
						<input name="do_recurse" id="do_recurse" type="hidden" value="0" />
					</td>
				<?php 
				}
				?>
		</tr>
		<tr class="button_bar" >
			<td style="float:right">
				<button id = 'changebtn' name = 'changebtn'> <?php echo $GLOBALS["messages"]["btnchange"]; ?> </button>
				<button id = 'closebtn' name = 'closebtn'> <?php echo $GLOBALS["messages"]["btncancel"]; ?> </button>
				<input type="hidden" name="file" id="file" value="<?php echo stripslashes($this->fileName); ?>" />
				<input type="hidden" name="dir" id = "dir" value="<?php echo stripslashes($this->dir); ?>" />
				<input type='hidden' name='returnDir' id = 'returnDir' value = '<?php echo dirname(get_abs_item($this->dir, $this->fileName)); ?>'/>
			</td>
		</tr>		
	</table>

	
	<?php echo OSESoftHelper::getToken();  ?>
		</div>
	</div>
 </div>
</div>

<?php
	echo oseSoftHelper::renderOSETM();
?>
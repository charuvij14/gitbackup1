Ext.ns('ose.Fileman');
ose.Fileman.msg = new Ext.App();

function ajaxAction(option, controller, task, dir, file, newname, returnDir) {
	Ext.MessageBox.wait('Please wait while file is being renamed', 'Performing Actions');
	var i = 0;
	Ext.Ajax.request({
		url : 'index.php',
		params : {
			option : option,
			task : task,
			controller : controller,
			dir: dir,
			file: file, 
			newname : newname
		},
		method : 'POST',
		success : function(result, request) {
			msg = Ext.decode(result.responseText);
			if (msg.status != 'ERROR') {
				Ext.MessageBox.hide();
				Ext.Msg.alert(msg.title, msg.content, function(btn,txt)	{
					if(btn == 'ok')	{
						window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+returnDir.getValue()+'&order=name&srt=yes'
               		}
				}); 
			} else {
				Ext.MessageBox.hide();
				ose.Fileman.msg.setAlert('Error', msg.content);
				
			}
		}
	});
}

Ext.onReady(function(){
	
var changebtn = Ext.get('changebtn');
var newfileName = Ext.get('newfileName');
var oldfileName = Ext.get('file');
var dir = Ext.get('dir');
var returnDir = Ext.get('returnDir');

changebtn.on('click', function(){
		ajaxAction('com_ose_fileman', 'files', 'rename_file', dir.getValue(), oldfileName.getValue(), newfileName.getValue(), returnDir) ;
		
	});

var closebtn = Ext.get('closebtn');

closebtn.on('click', function(){
	window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+returnDir.getValue()+'&order=name&srt=yes'
});


	
});	

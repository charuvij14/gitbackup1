Ext.ns('ose.Fileman');
ose.Fileman.msg = new Ext.App();

function ajaxAction(option, controller, task, dir, returnDir) {
	Ext.MessageBox.wait('Please wait while folder is added', 'Performing Actions');
	var i = 0;
	Ext.Ajax.request({
		url : 'index.php',
		params : {
			option : option,
			task : task,
			controller : controller,
			checkdir: dir
		},
		method : 'POST',
		success : function(result, request) {
			msg = Ext.decode(result.responseText);
			if (msg.status != 'ERROR') {
				Ext.MessageBox.hide();
				Ext.Msg.confirm(msg.title, msg.content, function(btn,txt)	{
					if(btn == 'yes')	{
						window.location = 'index.php?option=com_ose_antivirus&view=initialise';
               		}
					else
					{
						window.location = 'index.php?option=com_ose_fileman&view=initcustom';
					}
				}); 
			} else {
				Ext.MessageBox.hide();
				ose.Fileman.msg.setAlert('Error', msg.content);
				
			}
		}
	});
}

Ext.onReady(function(){
	
var changebtn = Ext.get('changebtn');
var dir = Ext.get('dir');
var returnDir = Ext.get('returnDir');

changebtn.on('click', function(){
		if (dir.getValue()=='')
		{
			Ext.Msg.alert('Directory cannot be empty');
		}
		ajaxAction('com_ose_fileman', 'files', 'add_initdir', dir.getValue(), returnDir) ;
		
	});

var closebtn = Ext.get('closebtn');

closebtn.on('click', function(){
	window.location = 'index.php?option=com_ose_fileman&view=directories&dir='+returnDir.getValue()+'&order=name&srt=yes'
});


	
});	

<?php
/**
  * @version     4.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence File Manager
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 23-Apr-2012
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
// no direct access
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.modeladmin');
class OSEFILEMANModelCodes extends OSEFILEMANModel
{
	var $dir = ''; 
	var $item = ''; 
	var $fileName = '';
	
	function __construct()
	{
		parent :: __construct();
	}
	function checkPermission($dir, $item)
	{
		$this->dir = $dir;
		$this->item = $item;
		
		$mainframe = JFactory::getApplication();
		if(($GLOBALS["permissions"]&01)!=01)
		{
			show_error($GLOBALS["error_msg"]["accessfunc"]);
		}
		$fname = $this -> getFileName(); 
		if(!get_is_file($fname))
		{
			show_error($this->item.": ".$GLOBALS["error_msg"]["fileexist"]);
		}
		if(!get_show_item($this->dir, $this->item))
		{
			show_error($this->item.": ".$GLOBALS["error_msg"]["accessfile"]);
		}
		
	}
	function getFileName()
	{
		$this->fileName = get_abs_item($this->dir, $this->item);
		return $this->fileName; 
	}
	function getFileType()
	{
		// header
		$s_item=get_rel_item($this->dir,$this->item);	
		if(strlen($s_item)>50) 
		{
			$s_item="...".substr($s_item,-47);
		}
		
		$s_info = pathinfo( $s_item );
		$s_extension = str_replace('.', '', $s_info['extension'] );
		
		switch (strtolower($s_extension)) {
			case 'txt':
			case 'ini':
				$cp_lang = 'text'; break;
			case 'cs':
				$cp_lang = 'csharp'; break;
			case 'css':
				$cp_lang = 'css'; break;
			case 'html':
			case 'htm':
			case 'xml':
			case 'xhtml':
				$cp_lang = 'html'; break;
			case 'java':
				$cp_lang = 'java'; break;
			case 'js':
				$cp_lang = 'javascript'; break;
			case 'pl':
				$cp_lang = 'perl'; break;
			case 'ruby':
				$cp_lang = 'ruby'; break;
			case 'sql':
				$cp_lang = 'sql'; break;
			case 'vb':
			case 'vbs':
				$cp_lang = 'vbscript'; break;
			case 'php':
				$cp_lang = 'php'; break;
			default:
				$cp_lang = 'generic';
		}
		return $cp_lang; 
	}
	
	function getFileContent()
	{
		// Show File In TextArea
		$content = $GLOBALS['nx_File']->file_get_contents( $this->fileName );
		if( get_magic_quotes_runtime()) {
			$content = stripslashes( $content );
		}
		$content = htmlspecialchars( $content );
		return $content; 
	}
}
?>

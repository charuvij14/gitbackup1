<?php
/**
 * @version       1.0 +
 * @package       Open Source Excellence Marketing Software
 * @subpackage    Open Source Excellence Affiates - com_ose_affiliates
 * @author        Open Source Excellence (R) {@link  http://www.opensource-excellence.com}
 * @author        Created on 01-Oct-2011
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2010- Open Source Excellence (R)
 */
// no direct access
defined('_JEXEC') or die;
jimport('joomla.application.categories');
/**
 * Content Component Category Tree
 *
 * @static
 * @package		Joomla.Site
 * @subpackage	com_content
 * @since 1.6
 */
require OSEFILEMAN_B_PATH."/libraries/File_Operations.php";
require OSEFILEMAN_B_PATH."/libraries/fun_extra.php";

class OSEArrayHelper extends JArrayHelper
{
	
}

class OSEFilemanHelper
{
	public function __construct($options= array())
	{

	}

	public static function iniServerVars()
	{
		//------------------------------------------------------------------------------
		umask(0022); // Added to make created files/dirs group writable

		//ISAPI_Rewrite 3.x
		if (isset($_SERVER['HTTP_X_REWRITE_URL'])){
			$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_X_REWRITE_URL'];
		}
		//ISAPI_Rewrite 2.x w/ HTTPD.INI configuration
		else if (isset($_SERVER['HTTP_REQUEST_URI'])){
			$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_REQUEST_URI'];
			//Good to go!
		}
		//ISAPI_Rewrite isn't installed or not configured
		else{
			//Someone didn't follow the instructions!
			if(isset($_SERVER['SCRIPT_NAME']))
			$_SERVER['HTTP_REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
			else
			$_SERVER['HTTP_REQUEST_URI'] = $_SERVER['PHP_SELF'];
			if($_SERVER['QUERY_STRING']){
				$_SERVER['HTTP_REQUEST_URI'] .=  '?' . $_SERVER['QUERY_STRING'];
			}
			//WARNING: This is a workaround!
			//For guaranteed compatibility, HTTP_REQUEST_URI or HTTP_X_REWRITE_URL *MUST* be defined!
			//See product documentation for instructions!
			$_SERVER['REQUEST_URI'] = $_SERVER['HTTP_REQUEST_URI'];
		}

		// Vars
		if(isset($_SERVER)) {
			$GLOBALS['__GET']	=$_GET;
			$GLOBALS['__POST']	=$_POST;
			$GLOBALS['__SERVER'] =$_SERVER;
			$GLOBALS['__FILES']	= $_FILES;
		}
		elseif(isset($HTTP_SERVER_VARS)) {
			$GLOBALS['__GET']	=$HTTP_GET_VARS;
			$GLOBALS['__POST']	=$HTTP_POST_VARS;
			$GLOBALS['__SERVER'] =$HTTP_SERVER_VARS;
			$GLOBALS['__FILES']	=$HTTP_POST_FILES;
		}
		else {
			die("<strong>ERROR: Your PHP version is too old</strong><br/>".
			"You need at least PHP 4.0.0 to run oseFileMan; preferably PHP 4.4.4 or higher.");
		}
	}

	public static function defineGlobalVars()
	{
		// Get Item
		if(isset($_REQUEST["item"]))
		$GLOBALS["item"]=$item = stripslashes(urldecode($_REQUEST["item"]));
		else
		$GLOBALS["item"]=$item ="";

		if( !empty( $GLOBALS['__POST']["selitems"] )) {
			// Arrayfi the string 'selitems' if necessary
			if( !is_array( $GLOBALS['__POST']["selitems"] ) ) $GLOBALS['__POST']["selitems"] = array( $GLOBALS['__POST']["selitems"] );
			foreach( $GLOBALS['__POST']["selitems"] as $i => $myItem ) {
				$GLOBALS['__POST']["selitems"][$i] = urldecode( $myItem );
			}
		}

		// Get Sort
		if(isset($GLOBALS['__GET']["order"]))
		$GLOBALS["order"]=stripslashes($GLOBALS['__GET']["order"]);
		else
		$GLOBALS["order"]="name";
		if($GLOBALS["order"]=="")
		$GLOBALS["order"]=="name";

		// Get Sortorder (yes==up)
		if(isset($GLOBALS['__GET']["srt"]))
		$GLOBALS["srt"]=stripslashes($GLOBALS['__GET']["srt"]);
		else
		$GLOBALS["srt"]="yes";
		if($GLOBALS["srt"]=="")
		$GLOBALS["srt"]=="yes";
		// Get Language
		if(isset($GLOBALS['__GET']["lang"]))
		$GLOBALS["lang"]=$GLOBALS["language"]=$GLOBALS['__GET']["lang"];
		elseif(isset($GLOBALS['__POST']["lang"]))
		$GLOBALS["lang"]=$GLOBALS["language"]=$GLOBALS['__POST']["lang"];
		//------------------------------------------------------------------------------
		$arrayHelper = new OSEArrayHelper();
		/** @var $GLOBALS['file_mode'] Can be 'file' or 'ftp' */
		if( !isset( $_REQUEST['file_mode'] ) && !empty($_SESSION['file_mode'] )) {
			$GLOBALS['file_mode'] = $arrayHelper->getValue( $_SESSION, 'file_mode', 'file' );
		}
		else {
			if( @$_REQUEST['file_mode'] == 'ftp' && @$_SESSION['file_mode'] == 'file') {
				if( empty( $_SESSION['ftp_login']) && empty( $_SESSION['ftp_pass'])) {
					$mainframe->redirect( 'index2.php?option=com_osefileman&action=ftp_authentication' );
				}
				else {
					$GLOBALS['file_mode'] = $_SESSION['file_mode'] = $arrayHelper->getValue( $_REQUEST, 'file_mode', 'file' );
				}
			}
			elseif( isset( $_REQUEST['file_mode'] )) {
				$GLOBALS['file_mode'] = $_SESSION['file_mode'] = $arrayHelper->getValue( $_REQUEST, 'file_mode', 'file' );
			}
			else {
				$GLOBALS['file_mode'] = $arrayHelper->getValue( $_SESSION, 'file_mode', 'file' );
			}
		}

		// Necessary files

		// Configuration Variables
		// login to use joomlaXplorer: (true/false)
		$GLOBALS["require_login"] = false;
		//$lang = JLanguage::getInstance( $lang );
		$lang = JFactory::getLanguage();
		$filedir = $lang->getDefault();
		$GLOBALS["language"] = $filedir;
		// the filename of the QuiXplorer script: (you rarely need to change this)
		if($_SERVER['SERVER_PORT'] == 443 ) {
			$GLOBALS["script_name"] = "https://".$GLOBALS['__SERVER']['HTTP_HOST'].$GLOBALS['__SERVER']["PHP_SELF"];
		}
		elseif ($_SERVER['SERVER_PORT'] == 2087 )
		{
			$GLOBALS["script_name"] = "https://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['PHP_SELF'];
		}
		elseif ($_SERVER['SERVER_PORT'] != 80 )
		{
			$GLOBALS["script_name"] = "http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['PHP_SELF'];
		}
		else {
			$GLOBALS["script_name"] = "http://".$GLOBALS['__SERVER']['HTTP_HOST'].$GLOBALS['__SERVER']["PHP_SELF"];
		}

		// allow Zip, Tar, TGz -> Only (experimental) Zip-support
		if( function_exists("gzcompress")) {
			$GLOBALS["zip"] = $GLOBALS["tgz"] = true;
		}
		else {
			$GLOBALS["zip"] = $GLOBALS["tgz"] = false;
		}

		//------------------------------------------------------------------------------
		// Global User Variables (used when $require_login==false)

		if( strstr( JPATH_BASE, "/" )) {
			$GLOBALS["separator"] = "/";
		}
		else {
			$GLOBALS["separator"] = "\\";
		}

		// the home directory for the filemanager: (use '/', not '\' or '\\', no trailing '/')

		// !Note! This has been changed since joomlaXplorer 1.3.0
		// and now grants access to all directories for one level ABOVE this Site
		$dir_above = substr( JPATH_ROOT, 0, strrpos( JPATH_ROOT, $GLOBALS["separator"] ));

		if( !@is_readable($dir_above) || !is_dir($dir_above) ) {
			$GLOBALS["home_dir"] = JPATH_ROOT;
			// the url corresponding with the home directory: (no trailing '/')
			$GLOBALS["home_url"] = JURI::root();
		}
		else {
			$GLOBALS["home_dir"] = $dir_above;
			// the url corresponding with the home directory: (no trailing '/')
			$GLOBALS["home_url"] = substr( JURI::root(), 0, strrpos(JURI::root(), '/'));
		}

		// show hidden files in QuiXplorer: (hide files starting with '.', as in Linux/UNIX)
		$GLOBALS["show_hidden"] = true;

		// filenames not allowed to access: (uses PCRE regex syntax)
		$GLOBALS["no_access"] = "";

		// user permissions bitfield: (1=modify, 2=password, 4=admin, add the numbers)
		$GLOBALS["permissions"] = 7;
		
		$GLOBALS['nx_File'] = new nx_File();
		
		if( nx_isFTPMode()== true) {
			// Try to connect to the FTP server.    	HOST,   PORT, TIMEOUT
			$ftp_host = $arrayHelper->getValue( $_POST, 'ftp_host', 'localhost:21' );
			$url = @parse_url( 'ftp://' . $ftp_host);
			$port = empty($url['port']) ? 21 : $url['port'];
			$ftp = new Net_FTP( $url['host'], $port, 20 );
			/** @global Net_FTP $GLOBALS['FTPCONNECTION'] */
			$GLOBALS['FTPCONNECTION'] = new Net_FTP( $url['host'], $port, 20 );
			$res = $GLOBALS['FTPCONNECTION']->connect();
			if( PEAR::isError( $res )) {
				echo $res->getMessage();
				$GLOBALS['file_mode'] = $_SESSION['file_mode'] = 'file';
			}
			else {
				if( empty( $_SESSION['ftp_login']) && empty( $_SESSION['ftp_pass'])) {
					$arrayHelper->getValue( 'index2.php?option=com_osefileman&action=ftp_authentication&file_mode=file' );
				}
				$login_res = $GLOBALS['FTPCONNECTION']->login( $_SESSION['ftp_login'], $_SESSION['ftp_pass'] );
				if( PEAR::isError( $res )) {
					echo $login_res->getMessage();
					$GLOBALS['file_mode'] = $_SESSION['file_mode'] = 'file';
				}
			}
		
		}
		//------------------------------------------------------------------------------
		if($GLOBALS["require_login"]) {
			// LOGIN
		
			require OSEFILEMAN_B_PATH."/include/login.php";
		
			if($GLOBALS["action"]=="logout") {
				logout();
			} else {
				login();
			}
		}
		//------------------------------------------------------------------------------
		if( !isset( $_REQUEST['dir'] ) ) {
		
			$GLOBALS["dir"] = $dir = $arrayHelper->getValue( $_SESSION,'nx_'.$GLOBALS['file_mode'].'dir', '' );
			if( !empty( $dir )) {
				$dir = @$dir[0] == '/' ? substr( $dir, 1 ) : $dir;
			}
			$try_this = nx_isFTPMode() ? '/'.$dir : $GLOBALS['home_dir'].'/'.$dir;
			if( !empty( $dir ) && !$GLOBALS['nx_File']->file_exists( $try_this )) {
				$dir = '';
			}
		}
		else {
			$GLOBALS["dir"] = $dir = urldecode(stripslashes($arrayHelper->getValue( $_REQUEST, "dir" )));
		}
		if( $dir == 'nx_root') {
			$GLOBALS["dir"] = $dir = '';
		}
		if( nx_isFTPMode() && $dir != '' ) {
			$GLOBALS['FTPCONNECTION']->cd( $dir );
		}
		
		$abs_dir=get_abs_dir($GLOBALS["dir"]);
		if(!file_exists($GLOBALS["home_dir"])) {
			if(!file_exists($GLOBALS["home_dir"].$GLOBALS["separator"])) {
				if($GLOBALS["require_login"]) {
					$extra="<a href=\"".make_link("logout",NULL,NULL)."\">".
					$GLOBALS["messages"]["btnlogout"]."</A>";
				}
				else $extra=NULL;
				OSESoftHelper::ajaxResponse(false,$GLOBALS["error_msg"]["home"]." (".$GLOBALS["home_dir"].")".$extra);
			}
		}
		if(!down_home($abs_dir)) {
			OSESoftHelper::ajaxResponse(false,$GLOBALS["dir"]." : ".$GLOBALS["error_msg"]["abovehome"]);
			$dir = '';
		}
		if(!get_is_dir($abs_dir) && !get_is_dir($abs_dir.$GLOBALS["separator"])) {
			OSESoftHelper::ajaxResponse(false, $abs_dir." : ".$GLOBALS["error_msg"]["direxist"]);
			$dir = '';
		}
		$_SESSION['nx_'.$GLOBALS['file_mode'].'dir'] = $dir;
	}
	
	public static function includeLangFiles()
	{
		$lang = JFactory::getLanguage();
		$filedir = $lang->getDefault();
		$GLOBALS["language"] = $filedir;
		//------------------------------------------------------------------------------
		if( file_exists(OSEFILEMAN_B_PATH."/languages/".$GLOBALS["language"].".php")) {
			require OSEFILEMAN_B_PATH."/languages/".$GLOBALS["language"].".php";
		}
		else {
			require OSEFILEMAN_B_PATH."/languages/english.php";
		}
		if( file_exists(OSEFILEMAN_B_PATH."/languages/".$GLOBALS["language"]."_mimes.php")) {
			require OSEFILEMAN_B_PATH."/languages/".$GLOBALS["language"]."_mimes.php";
		}
		else {
			require OSEFILEMAN_B_PATH."/languages/english_mimes.php";
		}
	}
}
var url = ajaxurl; 
var controller = "vsscan";
var option = "com_ose_firewall";

jQuery(document).ready(function($){
	//get object with colros from plugin and store it.
    colours = $('body').data('appStart').getColors();
    var sizes = $('body').data('appStart').getSizes();

	$('#vsscan').on('click', function() {

        $("#scan-result").hide();
        $("#scanpathtext").hide();
        $('#statusresult').hide();

        showLoading ();
        scanAntivirus(-1, 'vsscan', colours);
	});
	$('#vsscanSing').on('click', function() {
        $('[id^="easy-pie-chart-"]').data('easyPieChart').update(0);
        $('[id^="easy-pie-chart-"]').attr("data-percent",0);
        $('[id^="pie-"]').html('0%');

        $("#scan-result").hide();
        $("#scanpathtext").hide();
        $('#statusresult').hide();

        initPieChartPage($, sizes.pielinewidth, sizes.piesize, 1500, colours);
        showLoading ();
        scanAntivirusSing(-3, 'vsscan', [], [], colours);
	});
	$('#vsstop').on('click', function() { 
		showLoading ('Terminating scanning process');
		location.reload(); 
	});
	$('#vscont').on('click', function() { 
		scanAntivirus (-2, 'vsscan', [], [], colours);
        $("#scan-result").hide();
        $('#statusresult').hide();
	});
});
jQuery(document).ready(function ($) {
    $("#scan-form").submit(function () {
        $('#scanpathtext').show();
        $('#selectedfile').text($('#selected_file').val());
        $('#scanModal').modal('hide');
        $('#vsstop').show();
        var checkedValue = [];
        var inputElements = document.getElementsByClassName('messageCheckbox');
        var j = 0;
        for (var i = 0; inputElements[i]; ++i) {
            if (inputElements[i].checked) {
                checkedValue[j] = inputElements[i].value;
                j++;
            }
        }
        showLoading();
        var data = $("#scan-form").serialize();
        data += '&type=' + checkedValue;
        data += '&centnounce=' + $('#centnounce').val();
        $.ajax({
            type: "POST",
            url: url,
            data: data, // serializes the form's elements.
            dataType: 'json',
            success: function (data) {

                hideLoading();
                setProgressBar(data);
                $('#last_file').html(data.last_file);
                runAllScanAntivirus('vsscan', colours, data.size, 0);
            }
        });
        return false; // avoid to execute the actual submit of the form.
    });
});
var initPlotChart = function ($, data, cpu, colours) {
	if (cpu =='')
	{
		cpu = false;
	}
	//define chart colours
    var chartColours = [colours.linechart1, colours.linechart2, colours.linechart3, colours.linechart4, colours.linechart5, colours.linechart6, colours.linechart7];
	var options = {
			grid: {
				show: true,
			    aboveData: true,
                color: colours.black,
			    labelMargin: 15,
                axisMargin: 0,
			    borderWidth: 0,
			    borderColor:null,
			    minBorderMargin: 0,
			    clickable: true, 
			    hoverable: true,
			    autoHighlight: true,
			    mouseActiveRadius: 20
			},
	        series: {
	        	grow: {active:false},
	            lines: {
            		show: true,
            		fill: true,
            		lineWidth: 2,
            		steps: false
	            	},
	            points: {
	            	show:true,
	            	radius: 4,
	            	symbol: "circle",
	            	fill: true,
                    borderColor: colours.white
	            }
	        },
        yaxis: {min: 0},
        xaxis: {min: 0},
	        legend: { position: "se" },
	        colors: chartColours,
	        shadowSize:1,
	        tooltip: true, //activate tooltip
			tooltipOpts: {
				content: "%s : %y.0",
				shifts: {
					x: -30,
					y: -50
				}
			}
	};  
	if (cpu == true)
	{
		var plot1 = $.plot($("#line-chart-cpu"),
			    [{
			    		label: "CPU Load",
			    		data: data,
                    lines: {fillColor: colours.cream},
                    points: {fillColor: colours.linechart1}
			    }], options);
	}
	else
	{
		var plot2 = $.plot($("#line-chart-memory"),
			    [{
			    		label: "Memory Usage",
			    		data: data,
                    lines: {fillColor: colours.cream},
                    points: {fillColor: colours.linechart1}
			    }], options);
	}

};

var initPieChartPage = function($, lineWidth, size, animateTime, colours) {
	$(".easy-pie-chart").easyPieChart({
        barColor: colours.dark,
        borderColor: colours.dark,
        trackColor: colours.piedark,
        scaleColor: false,
        lineCap: 'butt',
        lineWidth: lineWidth,
        size: size,
        animate: animateTime
    });
    $(".easy-pie-chart-red").easyPieChart({
        barColor: colours.red,
        borderColor: colours.red,
        trackColor: colours.piered,
        scaleColor: false,
        lineCap: 'butt',
        lineWidth: lineWidth,
        size: size,
        animate: animateTime
    });
    $(".easy-pie-chart-green").easyPieChart({
        barColor: colours.green,
        borderColor: colours.green,
        trackColor: colours.piegreen,
        scaleColor: false,
        lineCap: 'butt',
        lineWidth: lineWidth,
        size: size,
        animate: animateTime
    });
    $(".easy-pie-chart-blue").easyPieChart({
        barColor: colours.blue,
        borderColor: colours.blue,
        trackColor: colours.pieblue,
        scaleColor: false,
        lineCap: 'butt',
        lineWidth: lineWidth,
        size: size,
        animate: animateTime
    });
    $(".easy-pie-chart-teal").easyPieChart({
        barColor: colours.teal,
        borderColor: colours.teal,
        trackColor: colours.pieteal,
        scaleColor: false,
        lineCap: 'butt',
        lineWidth: lineWidth,
        size: size,
        animate: animateTime
    });
    $(".easy-pie-chart-purple").easyPieChart({
        barColor: colours.purple,
        borderColor: colours.purple,
        trackColor: colours.piepurple,
        scaleColor: false,
        lineCap: 'butt',
        lineWidth: lineWidth,
        size: size,
        animate: animateTime
    });
    $(".easy-pie-chart-orange").easyPieChart({
        barColor: colours.orange,
        borderColor: colours.orange,
        trackColor: colours.pieorange,
        scaleColor: false,
        lineCap: 'butt',
        lineWidth: lineWidth,
        size: size,
        animate: animateTime
    });
    $(".easy-pie-chart-lime").easyPieChart({
        barColor: colours.lime,
        borderColor: colours.lime,
        trackColor: colours.pielime,
        scaleColor: false,
        lineCap: 'butt',
        lineWidth: lineWidth,
        size: size,
        animate: animateTime
    });
};

function scanAntivirus(step, action, colours) {
    var checkedValue = [];
    var inputElements = document.getElementsByClassName('messageCheckbox');
    var j = 0;
    for (var i = 0; inputElements[i]; ++i) {
        if (inputElements[i].checked) {
            checkedValue[j] = inputElements[i].value;
            j++;
        }
    }
	jQuery(document).ready(function($){
        $('#vsstop').show();
		$.ajax({
	        type: "POST",
	        url: url,
	        dataType: 'json',
		    data: {
		    		option : option,
                controller: controller,
                action: action,
                task: action,
                type: checkedValue,
		    		step : step,
		    		centnounce:$('#centnounce').val()
		    },
	        success: function(data)
	        {
	        	hideLoading ();
                setProgressBar(data);
                $('#last_file').html(data.last_file);
                if ((step == -1 && data.cont == true))
	        	{
                    runAllScanAntivirus(action, colours, data.size, 0);
	        	}
	        }
	      });
	});
}

function runAllScanAntivirus(action, colours, size, process) {
    var s1 = scanVirusInd(action, 1, colours, size, process);
    //var s2 = scanVirusInd(action, cpuData, memData, 2, colours, size, process);
    //var s3 = scanVirusInd(action, cpuData, memData, 3, colours, size, process);
    //var s4 = scanVirusInd(action, cpuData, memData, 4, colours, size, process);
    //var s5 = scanVirusInd(action, cpuData, memData, 5, colours, size, process);
    //var s6 = scanVirusInd(action, cpuData, memData, 6, colours, size, process);
    //var s7 = scanVirusInd(action, cpuData, memData, 7, colours, size, process);
    //var s8 = scanVirusInd(action, cpuData, memData, 8, colours, size, process);
    ////jQuery(document).ready(function($){
    //	$.when(s1, s2, s3, s4, s5, s6, s7, s8).then(
    //		function ( v1, v2, v3, v4, v5, v6, v7, v8 ) {
    //	});
    //});
}

function scanVirusInd(action, type, colours, size, process) {
	jQuery(document).ready(function($){
		$.ajax({
	        type: "POST",
	        url: url,
	        dataType: 'json',
		    data: {
                option: option,
                controller: controller,
                action: action,
                task: action,
                process: process,
                size: size,
                step: 0,
                type: type,
                centnounce: $('#centnounce').val()
		    },
	        success: function(data)
	        {
	        	hideLoading ();
                setProgressBar(data);
                $('#last_file').html(data.last_file);
                if (size == process) {
                    return true
                } else {
                    scanVirusInd(action, type, colours, size, process + 1);

                }

            },
		    error: function(XMLHttpRequest, textStatus, errorThrown) {
                scanVirusInd(action, type, colours, size, process + 1);
		    }
	      });
	});		
}

function scanAntivirusSing(step, action, cpuData, memData, colours) {
	jQuery(document).ready(function($){
        $('#vsstop').show();
		$.ajax({
	        type: "POST",
	        url: url,
	        dataType: 'json',
		    data: {
		    		option : option, 
		    		controller:controller,
		    		action:action,
		    		task:action,
		    		step : step,
		    		centnounce:$('#centnounce').val()
		    },
	        success: function(data)
	        {
	        	hideLoading ();
	        	cpuData.push([cpuData.length, data.cpuload]);
	        	memData.push([memData.length, data.memory]);
                initPlotChart($, cpuData, true, colours);
                initPlotChart($, memData, false, colours);
	        	//$('#p4text').html(data.summary);
                setProgressBar(data);
                $('#last_file').html(data.last_file);
	        	if ((step == -2 && data.contFileScan==true)|| (step == -3 && data.contFileScan==true))
	        	{
                    scanAntivirusSing(-2, action, cpuData, memData, colours);
	        	}
	        	else if (step == -2 && data.cont==true) {
                    scanAntivirusSing(-1, action, cpuData, memData, colours);
	        	}
	        	else
	        	{
                    scanVirusSingInd(action, cpuData, memData, 1, colours);
	        	}
	        }
	      });
	});
}

function setProgressBar(status) {
    jQuery("#scan-result").show();
    if (status.completed >= 100) {
        jQuery('#vs_progress').attr({
            "aria-valuenow" : 100, style:"width: 100%"
        }).removeClass().addClass("progress-bar progress-bar-success").text(O_SCAN_COMPLETE)
            .parent().removeClass("progress-striped active").show();
        if (status.totalvs > 0){
            if (cms == 'wordpress') {
                jQuery("<a href='admin.php?page=ose_fw_scanreport'>     View Result</a>").appendTo("#statusresult");
            } else {
                jQuery("<a href='index.php?option=com_ose_firewall&view=vsreport'>     View Result</a>").appendTo("#statusresult");
            }
            jQuery('#statusresult').removeClass("alert-success").addClass("alert-danger").show();
        } else {
            jQuery('#statusresult').text(O_SCAN_COMPLETE + ': ' + O_CLEAN)
                .removeClass("alert-danger").addClass("alert-success").show();
        }
        jQuery('#vsstop').hide();

    } else if (status.completed < 100) {
        jQuery('#vs_progress').attr({
            "aria-valuenow": status.completed, style: "width: " + status.completed + "%"
        }).show().removeClass("progress-bar-success").text(status.completed + "%")
            .parent().addClass("progress-striped active").show();
    }
    jQuery('#timeUsed').html(status.timeUsed);
    jQuery('#cpu').html(status.cpuload);
    jQuery('#memory').html(status.memory);
    jQuery('#numScanned').html(status.totalscan);
    jQuery('#numinfected').html(status.totalvs);
}

function scanVirusSingInd(action, cpuData, memData, type, colours) {
	jQuery(document).ready(function($){
		$.ajax({
	        type: "POST",
	        url: url,
	        dataType: 'json',
		    data: {
		    		option : option, 
		    		controller:controller,
		    		action:action,
		    		task:action,
		    		step:0,
		    		type:type,
		    		centnounce:$('#centnounce').val()
		    },
	        success: function(data)
	        {
	        	hideLoading ();
	        	if (type==2)
	        	{
	        		cpuData.push([cpuData.length, data.cpuload]);
		        	memData.push([memData.length, data.memory]);
                    initPlotChart($, cpuData, true, colours);
                    initPlotChart($, memData, false, colours);
	        	}	
	        	//$('#p4text').html(data.summary);
                setProgressBar(data);
	        	$('#last_file').html(data.last_file);
	        	$('#easy-pie-chart-'+type).data('easyPieChart').update(data.completed);
	        	$('#easy-pie-chart-'+type).attr("data-percent",data.completed);
	        	$('#pie-'+type).html(data.completed+'%');
	        	if (data.cont==true)
	        	{
                    scanVirusSingInd(action, cpuData, memData, type, colours);
	        	}
	        	else
	        	{
	        		if (type<8) {
	        			type = type +1;
                        scanVirusSingInd(action, cpuData, memData, type, colours);
	        		}
	        		else
	        		{
	        			showLoading ('Scanning completed');
	        			hideLoading ();
	        		}
	        	}
	        },
		    error: function(XMLHttpRequest, textStatus, errorThrown) {
                scanVirusSingInd(action, cpuData, memData, type, colours);
		    }
	      });
	});		
}
function downloadRequest(type) {
    showLoading();
    jQuery(document).ready(function ($) {
        showLoading();
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: "advancerulesets",
                action: 'downloadRequest',
                task: 'downloadRequest',
                type: type,
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
                downloadSQL(type, data.downloadKey, data.version);
            }
        });
    });
}
function downloadSQL(type, downloadKey, version) {
    jQuery(document).ready(function ($) {
        showLoading();
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: "advancerulesets",
                action: 'downloadSQL',
                task: 'downloadSQL',
                type: type,
                downloadKey: downloadKey,
                version: version,
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                showLoading(data.result);
                hideLoading();
                if (data.status == "ERROR"){
                    showDialogue(O_VSPATTERN_UPDATE_FAIL, O_FAIL, O_OK);
                } else {
                    showDialogue(O_VSPATTERN_UPDATE, O_SUCCESS, O_OK);
                }
                }
        });
    });
}

jQuery(document).ready(function($){
    $('#customscan').on('click',function(){
        var element = $('#FileTreeDisplay');
        element.html( '<ul class="filetree start"><li class="wait">' + 'Generating Tree...' + '<li></ul>');
        getfilelist( element  , '' );
        element.on('click', 'LI', function() { /* monitor the click event on foldericon */
            var entry = $(this);
            var current = $(this);
            var id = 'id';
            getfiletreedisplay (entry, current, id);
            return false;
        });
        element.on('click', 'LI A', function() { /* monitor the click event on links */
            var currentfolder;
            var current = $(this);
            currentfolder = current.attr('id');
            $("#selected_file").val(currentfolder) ;
            return false;
        });

    });

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {
            option : option,
            controller:controller,
            action:'getLastScanRecord',
            task:'getLastScanRecord',
            centnounce:$('#centnounce').val()
        },
        success: function(data)
        {
            if(typeof data.scanDate !== 'undefined' && data.scanDate !== null) {
                $('#scan-date').text(
                    moment(ko.unwrap(data.scanDate)).startOf('second').from(ko.unwrap(data.serverNow))
                ).attr("title", moment(ko.unwrap(data.scanDate)).format('llll'));
            }
        }
    });
});

function resetNessesary ()
{
    jQuery(document).ready(function($){

    });
}
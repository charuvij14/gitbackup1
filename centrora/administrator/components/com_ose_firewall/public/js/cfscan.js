var url = ajaxurl;
var controller = "cfscan";
var option = "com_ose_firewall";

jQuery(document).ready(function ($) {
    $("#scan-form").submit(function () {
        $('#scanModal').modal('hide');
        showLoading();
        var data = $("#scan-form").serialize();
        data += '&centnounce=' + $('#centnounce').val();
        $.ajax({
            type: "POST",
            url: url,
            data: data, // serializes the form's elements.
            dataType: 'json',
            success: function (data) {
                hideLoading();
                for (var sitename in data) {
                    if (data.hasOwnProperty(sitename)) {
                        $('#showmenu' + sitename).html('');
                        $('#changelist' + sitename).html('');
                        $('#p4text').after('<div class="col-md-12" id="showmenu' + sitename + '"><p></p><strong>' + '<input id="btnshowmenu' + sitename + '"class="btn btn-sm" type="button" onclick=toggleChangelist("' + sitename + '"); title="Show Changelog" value="' + sitename + '" ></strong></div><div id="changelist' + sitename + '" class="changelist collapsed" style="display: none;"> <div id ="scanSummary"></div><div id ="modifiedSummary"></div><div id ="missingSummary"></div><div id ="suspiciousSummary"></div></div>');
                        var obj = data[sitename];
                        for (var prop in obj) {
                            if (obj.hasOwnProperty(prop)) {
                                switch (prop) {
                                    case 'summary' :
                                        $('#scanSummary').html('<strong>Scan Summary</strong>: <br>' + obj[prop]);
                                        break;
                                    case 'modified' :
                                        $('#modifiedSummary').html('<strong>Detected modified core files</strong>: <br>' + obj[prop]);
                                        break;
                                    case 'missing' :
                                        $('#missingSummary').html('<strong>Detected missing files</strong>: <br>' + obj[prop]);
                                        break;
                                    case 'suspicious' :
                                        $('#suspiciousSummary').html('<strong>Detected suspicious files</strong>: <br>' + obj[prop]);
                                        break;
                                }
                            }
                        }
                    }

                }
            }
        });
        return false; // avoid to execute the actual submit of the form.
    });
});

function cfscan() {
    jQuery(document).ready(function ($) {
        showLoading();
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'cfscan',
                task: 'cfscan',
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
                if (cms == 'wordpress') {
                    if (data.status == 'Completed') {
                        $('#p4text').html(data.summary);
                        if (data.count1 != 0) {
                            $('#modified').html('<strong>Detected modified core files</strong>: <br>' + data.modified);
                        }
                        if (data.count2 != 0) {
                            $('#suspicious').html('<strong>Detected suspicious files</strong>: <br>' + data.suspicious);
                        }
                    } else {
                        $('#p4text').html(data.summary);
                        $('#modified').html(data.details);
                    }
                } else {
                    if (data.status == 'Completed') {
                        $('#p4text').html(data.summary);
                        if (data.count1 != 0) {
                            $('#modified').html('<strong>Detected modified core files</strong>: <br>' + data.modified);
                        }
                        if (data.count2 != 0) {
                            $('#suspicious').html('<strong>Detected suspicious files</strong>: <br>' + data.suspicious);
                        }
                        if (data.count3 != 0) {
                            $('#missing').html('<strong>Detected missing files</strong>: <br>' + data.missing);
                        }
                    } else {
                        $('#p4text').html(data.summary);
                        $('#modified').html(data.details);
                    }
                }
            }
        });
    });
}
function toggleChangelist(sitename) {
    jQuery(document).ready(function ($) {
        var changelist = $('#changelist' + sitename);
        var showmenu = $('#btnshowmenu' + sitename);
        if (changelist.hasClass('collapsed')) {
            changelist.slideDown({duration: 300});
            changelist.removeClass('collapsed').addClass('expanded');
            showmenu.attr('title', 'Hide Changelog');
        } else if (changelist.hasClass('expanded')) {
            changelist.slideUp({duration: 300});
            changelist.removeClass('expanded').addClass('collapsed');
            showmenu.attr('title', 'Show Changelog');
        }
    });
}

function catchVirusMD5() {
    jQuery(document).ready(function ($) {
        showLoading();
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'catchVirusMD5',
                task: 'catchVirusMD5',
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
                if (data.status == 'update') {
                    showDialogue(O_VSPATTERN_UPDATE, O_SUCCESS, O_OK);
                } else {
                    showDialogue(O_VSPATTERN_UPDATE_FAIL, O_FAIL, O_OK);

                }
            }
        });
    });
}

function addToAi(id) {
    jQuery(document).ready(function ($) {
        showLoading();
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {
                option: option,
                controller: controller,
                action: 'addToAi',
                task: 'addToAi',
                id: id,
                centnounce: $('#centnounce').val()
            },
            success: function (data) {
                hideLoading();
            }
        });
    });
}
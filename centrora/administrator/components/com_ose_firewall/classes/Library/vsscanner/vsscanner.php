<?php
/**
 * @version     2.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Centrora Security Firewall
 * @subpackage    Open Source Excellence WordPress Firewall
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 01-Jun-2013
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2012- ... Open Source Excellence
 */
if (!defined('OSE_FRAMEWORK') && !defined('OSEFWDIR') && !defined('_JEXEC'))
{
 die('Direct Access Not Allowed');
}
class virusScanner {
	private $db = null;
	private $filestable = '#__osefirewall_files';
	private $logstable = '#__osefirewall_logs';
	private $malwaretable = '#__osefirewall_malware';
    private $scanhisttablebl = '#__osefirewall_scanhist';
	private $file_ext = '';
	private $config = '';
	private $maxfilesize = 0;
	private $patterns = '';
	private $size = 0;
	private $type = 0;
	private $chunksize = 50;
	private $clamd = null;
	private $vsInfo = array();
	private $last_scanned = '';
	public function __construct()
	{
		oseFirewall::loadLanguage();
		$this->db= oseFirewall::getDBO();
		$this->setConfiguration();
		$this->setFileExts();
		$this->setMaxFileSize();
		$this->setDBConn();
		$this->optimizePHP();
		$this->setClamd(); 
		oseFirewall::loadFiles(); 
	}
	private function setClamd () {
		if ($this->config->enable_clamav == 1) 
		{
			oseFirewall::callLibClass('clamd', 'clamd');
			$this->clamd = new Clamd();
		}
	}
	private function setConfiguration() {
		$config = oseFirewall::getConfiguration('vsscan');
		$this->config = (object)$config['data'];
	}
	public function setFileExts()
	{
		if (!isset($this->config->file_ext))
		{
			$this->config->file_ext = "htm,html,shtm,shtml,css,js,php,php3,php4,php5,inc,phtml,jpg,jpeg,gif,png,bmp,c,sh,pl,perl,cgi,txt";
		}
		$this->file_ext = explode(',', trim($this->config->file_ext));
	}
	private function setMaxFileSize () {
		if ($this->config->maxfilesize>0)
		{
			$this->config->maxfilesize = $this->config->maxfilesize * 1024 * 1024; 
		}
	}
	private function setDBConn () {
		if (empty($this->config->maxdbconn))
		{
			$this->config->maxdbconn = 20;
		}
	}
	private function optimizePHP () {
		
		if (function_exists('ini_set'))
		{
			ini_set('max_execution_time', 300);
			ini_set('memory_limit', '1024M');
			ini_set("pcre.recursion_limit", "524");
			set_time_limit(300);
		}	
		
	}
	public function initDatabase($step, $directory) {
		if ($step<0)
		{	
			$this->clearTable(); 
			$return = $this ->getReturn($directory);
		}
		else
		{
			$dirs = $this->getFolder(5);
			if (empty($dirs))
			{
				$return['cont']= false;
				$return['folders']= 0;
				$return['file']= 0;
			}	
			else
			{
				$return=array();
				$return ['folder'] =0;
				$return ['file'] =0;
				foreach ($dirs as $dir)
				{
					$tmp = $this ->getReturn($dir->filename);
					$return ['folder'] += $tmp['folder'];
					$return ['file'] += $tmp['file'];
					$return ['cont'] = $tmp['cont'];
					$return ['lastscanned'] = LAST_SCANNED. $dir->filename; 
					$this->deletepathDB($dir->filename);
					unset($tmp);
				}
			}	
		}
		if ($return ['cont']==true)
		{		
			$return['summary'] = OSE_SCANNING.' '.$return ['folder'].' '.OSE_FOLDERS.' '.OSE_AND.' '.$return ['file'].' '.OSE_FILES.' ';
		}
		else
		{
			$total = $this->CountFiles(); 
			$return['summary'] = OSE_ADDED.' '.OSE_INTOTAL.' '.$total.' '.OSE_FILES.'.';
		}	
		$this->db -> closeDBO(); 
		oseAjax::returnJSON($return);
	}
	private function clearTable () {
		$query = "TRUNCATE TABLE ".$this->db->quoteTable($this->filestable);
		$this->db->setQuery($query);
		$result = $this->db->query();
		return $result;
	}
	public function getReturn($path)
	{
		$return = $this->getFolderFiles($path);
		$return['cont']= $this->isFolderLeft();
		return $return; 
	}
	private function getFolderFiles($folder) {
		// Initialize variables
		$arr = array();
		$arr['folder'] = 0;
		$arr['file'] = 0;
		$false = false;
		if (!is_dir($folder))
			return $false;
		$handle = @opendir($folder);
		// If directory is not accessible, just return FALSE
		if ($handle === FALSE) {
			return $false;
		}
		while ((($file = @readdir($handle)) !== false)) {
			if (($file != '.') && ($file != '..')) {
				$ds = ($folder == '') || ($folder == '/') || (@substr($folder, -1) == '/') || (@substr($folder, -1) == DIRECTORY_SEPARATOR) ? '' : DIRECTORY_SEPARATOR;
				$dir = $folder . $ds . $file;
				$isDir = is_dir($dir);
				if ($isDir) {
					$arr['folder'] ++;
					$this->insertData($dir, 'd');
				}
				else
				{
					$fileext = $this->getExt($dir);
					$filesize= filesize($dir);
					if (in_array($fileext, $this->file_ext))
					{
						if (!empty($this->config->maxfilesize))
						{
							if(filesize($dir) < $this->config->maxfilesize)
							{
								$arr['file'] ++;
								$this->insertData($dir, 'f', $fileext);
							}
						}	
						else
						{
							$arr['file'] ++;
							$this->insertData($dir, 'f', $fileext);
						}	
					}	
				}	
			}
		}
		@closedir($handle);
		return $arr;
	}
	private function insertData($filename,$type, $fileext='')
	{
		$result = $this->getfromDB($filename, $type, $fileext);
		if (empty($result))
		{
			return $this->insertInDB($filename, $type, $fileext);
		}	
		else
		{
			$this->updateFile($result -> id, 'checked', 0);
			return $result -> id;
		}
	}
	private function getfromDB($filename, $type, $fileext) {
		$query = "SELECT `id` "
				."FROM ".$this->db->quoteTable($this->filestable)
			    ." WHERE `filename` = ".$this->db->quoteValue($filename)
			    ." AND `type` = ".$this->db->quoteValue($type)
			    ." AND `ext` = ".$this->db->quoteValue($fileext);
		$this->db->setQuery($query);
		$result = $this->db->loadObject();
		return $result;
	}
	public function insertInDB($filename, $type, $fileext) {
		$varValues = array(
						'filename' => $filename,
						'type' => $type,
						'checked' => 0,
						'patterns' => '',
						'ext' => $fileext
					);
		$id = $this->db->addData ('insert', $this->filestable, '', '', $varValues);
		return $id;
	}
	private function isFolderLeft() {
		$query = "SELECT COUNT(`id`) as count FROM ".$this->db->quoteTable($this->filestable)
				." WHERE `type` = 'd'";
		$this->db->setQuery($query);
		$result = (object)$this->db->loadObject();
		return $result->count;
	}
	private function getExt($file)
	{
		$dot = strrpos($file, '.') + 1;
		return substr($file, $dot);
	}
	private function getFolder($limit)
	{
		$query = "SELECT `filename` FROM `".$this->filestable."`"
				." WHERE `type` = 'd' LIMIT ".(int)$limit;
		$this->db->setQuery($query);
		$result = $this->db->loadObjectList();
		return $result;
	} 
	private function deletepathDB($path)
	{
		$query = "DELETE FROM `".$this->filestable."` WHERE `type` = 'd' AND `filename` = " .$this->db->quoteValue ($path); 
		$this->db->setQuery($query);
		return $this->db->query(); 
	}
	public function countFiles() {
		$query = "SELECT COUNT(`id`) as count FROM `".$this->filestable."`"
				." WHERE `type` = 'f'";
		$this->db->setQuery($query);
		$result = (object)$this->db->loadResult(); 
		return $result->count;
	}
	private function getFiles($limit, $status)
	{
		$query = "SELECT `id`, `filename` FROM `".$this->filestable."`"
				." WHERE `type` = 'f' "
				." AND `checked` = ".(int)	$status	
				." LIMIT ".(int)$limit;
		$this->db->setQuery($query);
		$result = $this->db->loadObjectList();
		return (!empty($result))?$result:false;
	}

	function stripslashes_centrora($value)
	{
		if (is_array($value)) {
			$value = array_map('stripslashes_centrora', $value);
		} elseif (is_object($value)) {
			$vars = get_object_vars($value);
			foreach ($vars as $key => $data) {
				$value->{$key} = stripslashes_centrora($data);
			}
		} elseif (is_string($value)) {
			$value = stripslashes($value);
		}

		return $value;
	}

	private function setPatterns($array)
	{
		$type = '(' . implode(',', $array) . ')';
		$query = "SELECT `id`,`patterns` FROM `#__osefirewall_vspatterns` WHERE `type_id` IN " . $type;
		$this->db->setQuery($query);
		$this->patterns = $this->db->loadArrayList();

		$filePath = OSE_FWDATA . ODS . "vsscanPath" . ODS . "pattern.php";
		file_put_contents($filePath, '<?php $pattern = ' . var_export($this->patterns, true) . ';');

	}
	private function updateAllFileStatus($status = 0)
	{
		$query = "UPDATE `".$this->filestable."` SET `checked` = ". (int)$status; 
		$this->db->setQuery($query);
		$result = $this->db->query();
		return $result;
	}

	public function vsScan($step, $type, $remote = false)
	{
		if ($step == -1)
		{
			$this->cleanMalwareData ();
			$_SESSION['completed'] = 0;
			$_SESSION['start_time'] = time();
			if (empty($type)) {
				$type = array(1, 2, 3, 4, 5, 6, 7, 8);
			} elseif (!is_array($type)) {
				$type = explode(',', $type);
			}
			$this->setPatterns($type);
			$result = $this->scanFiles();
		}
		if ($remote == false) {
			$this->db->closeDBO();
			$this->db->closeDBOFinal();
		}
		return $result;
	}
	protected function scanFiles () {
		ini_set("display_errors", "on");
		if (!empty($_POST['scanPath']))
		{
			$scanPath = trim($_POST['scanPath']);
			$this->saveFilesFromPath($scanPath);
        } else
		{
			$scanPath = oseFirewall::getScanPath();
			$this->saveFilesFromPath($scanPath);
		}
		$result = $this->showScanningStatusMsg();
		return $result;
	}
	protected function returnAjaxResults ($scanPath) {
        $path = OSE_FWDATA . ODS . "vsscanPath" . ODS . "dirList.json";
		$content = oseFile::read($path);
		$dirArray = oseJSON::decode($content);
		$result = array (); 
		if (COUNT($dirArray)==0)
		{
			$return['completed'] = 0;
			$result['success']=true;
			$result['status']='Completed';
			$result['contFileScan']=false;
			$result['content']='All files in the website have been added to the scanning queue.';
		}
		else 
		{
			$return['completed'] = 0;
			$result['success']=true;
			$result['status']='Continue';
			$result['contFileScan']=true;
			$result['content']='Continue scanning files, the last folder being scanned is: '.$scanPath;
            $this->saveDirFile (array() , true);
		}
		$result['last_file'] = $scanPath;
		$result['cont'] = true;
		$result['cpuload'] = $this->getCPULoad();;
		$result['memory'] = $this->getMemoryUsed();
		return $result;
	}
	protected function getdirList () {
        $baseScanPath = $this->getBaseScanPath();
		$path = OSE_FWDATA.ODS."vsscanPath".ODS."dirList.json";
		$content = oseFile::read($path);
		$dirArray = oseJSON::decode($content);
		$this->saveDirFile (array(), true) ;
		$start_time = time();
		while (COUNT($dirArray)>0)
		{
				$since_start = $this->timeDifference($start_time, time());
				if ($since_start>10) {
					$this->saveDirFile ($dirArray, false);
					break;
				}
				$scanPath = array_pop($dirArray);
				while (empty($scanPath)) {
					$scanPath = array_pop($dirArray);
				}
				$this->saveFilesFromPath ($scanPath, $baseScanPath);
		}
		return $scanPath;
	}
	protected function saveBaseScanPath($scanPath) {
		$filePath = OSE_FWDATA.ODS."vsscanPath".ODS."basePath.json";
        $scanPath = str_replace(' ', '', $scanPath);
		$fileContent = oseJSON::encode(array($scanPath));
		$result = oseFile::write($filePath, $fileContent);
		return $result;
	}
	protected function getBaseScanPath () {
		oseFirewall::loadJSON();
		$filePath = OSE_FWDATA.ODS."vsscanPath".ODS."basePath.json";
		if (file_exists($filePath)) {
			$content = oseFile::read($filePath);
			return oseJSON::decode($content);
		}
		else
		{
			return array();
		}
	}

	protected function saveFilesFromPath($scanPath)
	{
		oseFirewall::loadJSON();
		$dirs = array();
		$files = array ();

		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator(realpath($scanPath), RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::SELF_FIRST, RecursiveIteratorIterator::CATCH_GET_CHILD);

        foreach ($objects as $path => $dir) {
			if (is_file($path) && substr($path, -9) != '.svn-base') {
				$files[] = $path;
            }
        }

		if (!empty($files))
		{
			$this->saveFilesFile ($files);
		}
	}
	protected function emptyDirFile () {
		$filePath = OSE_FWDATA.ODS."vsscanPath".ODS."dirList.json";
		$result = oseFile::write($filePath, '');
	}
	protected function saveDirFile ($dirs, $update=false) {
		$filePath = OSE_FWDATA.ODS."vsscanPath".ODS."dirList.json";
		if (file_exists($filePath))
		{
			if ($update == false)
			{
				$contentArray= oseJSON::decode(oseFile::read ($filePath));
				$fileContent = oseJSON::encode(array_merge ($dirs, $contentArray));
				$result = oseFile::write($filePath, $fileContent);
			}
			else
			{
				$fileContent = oseJSON::encode($dirs);
				$result = oseFile::write($filePath, $fileContent);
			}
		}
		else
		{
			$fileContent = oseJSON::encode($dirs);
			$result = oseFile::write($filePath, $fileContent);
		}
	}
	protected function saveFilesFile ($files) {
//		$filePath = OSE_FWDATA.ODS."vsscanPath".ODS."path.php";
//		file_put_contents($filePath, '<?php $path_files = ' . var_export($files, true) . ';');
		$chunks = array_chunk($files, $this->chunksize);
		$this->size = count($chunks) - 1;
		for ($i = 0; $i <= $this->size; $i++) {
			$array = "array('" . implode("',\n'", $chunks[$i]) . "');";
			$filePath = OSE_FWDATA . ODS . "vsscanPath" . ODS . "path" . $i . ".php";
			file_put_contents($filePath, '<?php $path' . $i . ' = ' . $array);
		}

//		foreach ($chunks as $chunk) {
//
//		}
//		$array = "array('".implode("',\n'",$files)."');";
//		for ($i = 1; $i <= 8; $i++)
//		{
//			$filePath = OSE_FWDATA . ODS . "vsscanPath" . ODS . "path" . $i . ".php";
//			file_put_contents($filePath, '<?php $complete'.$i.' = 0; $path' . $i . ' = ' .$array);
//		}
	}

	public function vsScanInd($process, $size, $remote = false)
	{
		oseFirewall::loadJSON();
		$conn = $this->getCurrentConnection();
		if ($conn > $this->config->maxdbconn)
		{
			$result = $this->getHoldingStatus($process, $conn);
		}
		else
		{
			//ini_set("display_errors", "on");
			oseFirewall::loadFiles();
			$result = $this->showScanningResultMsg($process, $size, $remote);
		}
		if ($remote == false) {
			$this->db->closeDBO();
			$this->db->closeDBOFinal();
		}
		return $result;
	}

	private function getHoldingStatus($process, $conn)
	{
		$this->vsInfo = $this->getVsFiles($process);
		$timeUsed = $this->timeDifference($_SESSION['start_time'], time());
		$completed = $this->vsInfo->completed;
		$left = count($this->vsInfo->fileset);
		$total = $this->vsInfo->completed + $left;
		$progress = ($completed/$total);
		$return['completed'] = 'Queue';
		$return['summary'] = (round($progress, 3)*100). '% ' .oLang::_get('COMPLETED');
		$return['progress'] = "<b>Progress: ".($left)." files remaining.</b>. Time Used: ".$timeUsed." seconds<br/><br/>";
		$return['last_file'] = oLang::_get('CURRENT_DATABASE_CONNECTIONS').': '.$conn.'. '.oLang::_get('YOUR_MAX_DATABASE_CONNECTIONS').': '.$this->config->maxdbconn.'. '.oLang::_get('WAITING_DATABASE_CONNECTIONS');
		$return['cont'] = ($left > 0 )?true:false;
		$return['cpuload'] = $this->getCPULoad();;
		$return['memory'] = $this->getMemoryUsed();
		return $return;
	}
	private function getCurrentConnection () {
		$result = $this->db->getCurrentConnection ();
		return $result['Value'];
	}

	private function showScanningResultMsg($process, $size, $remote)
	{
		$return=array();
		$return['summary'] = null;
		$return['found'] = 0;
		$start_time = time();
		$result = $this->scanFileLoop($start_time, $remote, $process, $size);
		if($result == false)
		{
			return false;
		} else {
			return $result;
		}
	}

	private function scanFileLoop($start_time, $remote, $process, $size)
	{
		ini_set('display_errors', 'on');
		$this->vsInfo = $this->getVsFiles($process);
		if ($process == $size) {
			$lastfileno = count($process);
		}
		require_once(OSE_FWDATA . ODS . "vsscanPath" . ODS . "pattern.php");

		for ($i = 0; $i < count($this->vsInfo); $i++) {

			$this->last_scanned = $this->vsInfo[$i];

			//   $this->vsInfo->completed++;

			if(oseFile::exists($this->last_scanned)==false) {
				continue;
			}
			if($this->ignoredFiles($this->last_scanned )==true) {
				continue;
			}
			if (filesize($this->last_scanned)>2048000)
			{
				continue;
			}
			else
			{
				$this->scanFile($this->last_scanned, $pattern);
			}
		}


		//$this->clearFile ($process);
			//$this->clearDirFile();
		return $this->returnCompleteMsg($this->last_scanned, $process, $size, $lastfileno);

	}
	private function ignoredFiles ($file) {
		if (preg_match('/mootree\.gif/ims', $file) || ($this->checkIsMarkedAsClean($file)) )
		{
			return true;
		}
		else
		{
			return false; 
		}
	}
	private function checkIsMarkedAsClean ($file)
	{
		$query = "SELECT * FROM `$this->filestable` WHERE `type` = 'f' AND `filename` = '$file' ";
		$this->db->setQuery($query);
		$result = $this->db->loadObject();
		if (!empty($result)){
			if (($result->checked == -1) && (md5_file( $file ) == $result->content )){
				return true;
			}
		}
		return false;

	}
	private function clearFileFromArray ($index) {
		unset($_SESSION['oseFileArray'][$index]);
	}
	private function showScanningStatusMsg () {
		//$this->saveVsFilesLoop();
		$return['size'] = $this->size;
		$return['completed'] = 0;
		$return['summary'] = 'Finish creating all scanning objects';
		$return['progress'] = 'Finish creating all scanning objects';
		$return['last_file'] = 'Finish creating all scanning objects';
		$return['cont'] = true;
		$return['cpuload'] = $this->getCPULoad();;
		$return['memory'] = $this->getMemoryUsed();;
		$return['showCountFiles'] = false;
		$return['totalvs'] = 0;
		$return['totalscan'] = 0;
		return $return; 
	}
	private function showCountFilesMsg () {
		$first  = new DateTime(date('Y-m-d h:i:s'));
		if (!empty($_POST['scanPath']))
		{
			$scanPath = $_POST['scanPath'];
		}	
		else
		{
			$scanPath = oseFirewall::getScanPath();
		}
		$fileCount = $this->getNumberofFiles($scanPath);
		$timeUsed = $this->getTimeUsed ($first);  
		$memUsed = $this->getMemoryUsed (); 
		$return['completed'] = 0;
		$return['summary'] = 'There are in total of '.$fileCount.' files in your website (time used '.$timeUsed.'), the scanning will start shortly';
		$return['progress'] = 'Found '.$fileCount.' number of files';
		$return['last_file'] = '';
		$return['cont'] = true;
		$return['cpuload'] = $this->getCPULoad();;
		$return['memory'] = $memUsed;
		$return['showCountFiles'] = true;
		return $return; 
	} 
	private function getMemoryUsed () {
		$newMemoryUsage = round(memory_get_usage(true)/(1028*1024), 2);
		return $newMemoryUsage; 
	}
	private function getTimeUsed ($first) {
		$second = new DateTime(date('Y-m-d h:i:s'));
		$diff = $first->diff( $second );
		$timeUsed = $diff->format( '%H:%I:%S' );
		return $timeUsed; 
	}
	private function getNumberofFiles ($path) {
		$x = 0; 
		$oseFileArray = array (); 
		if (!empty($path)) {
			$dir_iterator = new RecursiveDirectoryIterator($path);
			$iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);
			foreach ($iterator as $fullFileName => $fileSPLObject)
			{
				if ($fullFileName != $path."/.." && $fileSPLObject->isFile() && in_array($this->getFileExtension($fullFileName), $this->file_ext)) {
					$x++;
					$oseFileArray[] = $fullFileName; 
				}
			}
		}
		$this->saveVsFiles($oseFileArray, $_SESSION['completed']);
		return $x; 
	}
	private function getFileExtension ($path) {
		$ext = pathinfo($path, PATHINFO_EXTENSION);
		return $ext;
	}

	private function returnCompleteMsg($last_file = null, $process, $size, $lastfileno)
	{
		$timeUsed = $this->timeDifference($_SESSION['start_time'], time());
		$timeUsed = gmdate("H:i:s", $timeUsed);
		if ($process == $size) {
			$return['completed'] = 100;
			$return['summary'] = ($return['completed']) . '% ' . oLang::_get('COMPLETED');
			$return['last_file'] = $last_file;
			$return['status'] = 'Completed';
			$return['cont'] = false;
			$return['cpuload'] = $this->getCPULoad();;
			$return['memory'] = $this->getMemoryUsed();
			$return['totalvs'] = $this->getNumInfectedFiles();
			$return['totalscan'] = ($process + 1) * $this->chunksize + $lastfileno;
			$return['timeUsed'] = $timeUsed . 's';
			$this->clearFile($size);
			$this->clearPattern();
			return $return;
		} else {
			$return['completed'] = round($process / $size, 3) * 100;;
			$return['summary'] = ($return['completed']) . '% ' . oLang::_get('COMPLETED');
			$return['last_file'] = $last_file;
			$return['status'] = 'Continue';
			$return['cont'] = true;
			$return['cpuload'] = $this->getCPULoad();;
			$return['memory'] = $this->getMemoryUsed();
			$return['totalvs'] = $this->getNumInfectedFiles();
			$return['totalscan'] = ($process + 1) * $this->chunksize;
			$return['timeUsed'] = $timeUsed . 's';
			return $return;
		}
	}
	private function returnAjaxMsg ($last_file=null, $type) {

		if (count($this->vsInfo->fileset) == 0)
		{
			$this->clearFile ($type);
			$return = $this->returnCompleteMsg($last_file, $type);
            if ( $return['overall'] >= 100 ) {
                $return['vsfilelist'] = $this->getVsFileNames();
                $this->saveDBLastScanResult($return);
            }
            return $return;
		}
		else
		{
			$return = array ();
			$timeUsed = $this->timeDifference($_SESSION['start_time'], time());
			$completed = $this->vsInfo->completed;
			$left = count($this->vsInfo->fileset);
			$total = $this->vsInfo->completed + $left;
            $truetotal = $total * 8;
            $lefttotal = $this->getOverallLeft();
			$progress = ($completed/$total);
			$return['completed'] = round($progress, 3)*100;
            $return['overall'] = round( ($truetotal - $lefttotal) / $truetotal, 3)*100;
			$return['summary'] = ($return['completed']). '% ' .oLang::_get('COMPLETED');
			$return['progress'] = "<b>Progress: ".($left)." files remaining.</b>. Time Used: ".$timeUsed." seconds<br/><br/>";
			$return['last_file'] = $last_file;
			$return['cont'] = ($left > 0 )?true:false;
			$return['cpuload'] = $this->getCPULoad();;
			$return['memory'] = $this->getMemoryUsed();
			$return['type'] = $type;
            $return['totalvs'] =  $this->getNumInfectedFiles();
            $return['totalscan'] = round(($truetotal - $lefttotal) / 8);
			return $return;  
		}
	}

    public function getLastScan ()
    {
        $result  = $this->getLastVsScanHist();
        $scanList = oseJSON::decode($result->content, true);
        $lastScan = array('scanDate' => $result->inserted_on
            ,'serverNow' => oseFirewall::getTime()
//            ,'content' => $scanList[0]['vsfilelist']
        );
        return $lastScan;
    }

    private function getVsFileNames ()
    {
        $db = oseFirewall::getDBO ();
        $query = "SELECT filename FROM `".$this->malwaretable."` JOIN `".$this->filestable."` ON file_id = id";
        $db->setQuery($query);
        $result = $db->loadObjectList();
        $db -> closeDBO();
        foreach ($result as $key => $value) {
            $filenames[] = $value->filename;
        }
        return $filenames;
    }
    private function saveDBLastScanResult ($content = '')
    {
        $varValues = array('super_type' => 'vsscan',
            'sub_type' => 1,
            'content' => oseJSON::encode(array($content)),
            'inserted_on' => oseFirewall::getTime()
        );
        $this->db->addData('insert', $this->scanhisttablebl, '', '', $varValues);
    }

    private function getLastVsScanHist()
    {
        $query = "SELECT * FROM " . $this->db->quoteTable($this->scanhisttablebl)
            . " WHERE inserted_on = (SELECT max(inserted_on) FROM " . $this->db->quoteTable($this->scanhisttablebl)
            . " WHERE super_type = 'vsscan' AND sub_type = 1)";
        $this->db->setQuery($query);
        $result = $this->db->loadObject();
        return $result;
    }

    private function getOverallLeft()
    {
        $lefttotal = 0 ;

        for($i=1; $i<=8; $i++)
        {
            $content = $this->getVsFiles($i);
            $lefttotal = $lefttotal + count($content->fileset);
        }

        return $lefttotal;
    }
	private function getCPULoad () {
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
		{
			$cpuload = 'N/A in Windows';
			return $cpuload;
		}
		else
		{
			$cpuload = sys_getloadavg();
			return $cpuload[0];
		} 
	}
	private function getCompleted() {
		$query= "SELECT COUNT(`id`) as `count` FROM `#__osefirewall_files` WHERE `checked` = 0 ";
		$this->db->setQuery($query);
		$result = (object) ($this->db->loadResult());
		return $result->count;
	}
	private function getTotal() {
		$query= "SELECT COUNT(`id`) as `count` FROM  `#__osefirewall_files` ";
		$this->db->setQuery($query);
		$result = (object) ($this->db->loadResult());
		return $result->count;
	}
	private function updateFile($id, $field, $value){	
		$query = " UPDATE `".$this->filestable."` SET `{$field}` = ".$this->db->quoteValue($value)
				." WHERE id = " .(int)$id;
		$this->db->setQuery ($query);
		$result = $this->db->query(); 
		return $result;
	}
	private function timeDifference($timeStart, $timeEnd){
		return $timeEnd- $timeStart; 
	}

	private function scanFile($scan_file, $array)
	{
		if (empty($scan_file))
		{
			return false; 
		}
		oseFirewall::loadFiles();
		$virus_found = false;
		$content = oseFile::read ($scan_file);

		foreach ($array as $pattern)
		{
//			$badstring = trim($pattern['badstring']);
//			if (!empty($badstring)){
//				$badstringArray = explode(',', $badstring ) ;
//				$result = $this->strpos_array($content, $badstringArray);
//			} else {
//				$result = true;
//			}
//			if ($result == true) {
			$flag = preg_split('/' . trim($pattern['patterns']) . '/im', $content, 2);
			if (count($flag) > 1) {
				$virus_found = true;
				$file_id = $this->insertData($scan_file, 'f', '');
				$this->logMalware($file_id, $pattern['id']);
				break;
				//	}
			}
		}
		usleep(100);
		return $virus_found;
	}

    private function scanWithClamAV2()
    {
		$this->config->enable_clamav = false;
		if ($this->config->enable_clamav == 1 && $virus_found == false)
		{
			$results = $this->clamd->scan($scan_file);
			if ($results['status'] == 2)
			{
				$virus_found= true;
				$file_id = $this->insertData($scan_file,'f', '');
				$pattern_id = $this->logClamVirus ($results['msg']);
				$this->logMalware($file_id, $pattern_id);
			}
		}
	}
	private function logClamVirus ($msg)
	{
		$detectedMal = $this->getClamMessage($msg);
		if (empty($detectedMal))
		{
			$db = oseFirewall::getDBO ();
			$varValues = array(
						'patterns' => $msg,
						'type_id' => 9,
						'confidence' => 100,
					);
			$id = $db->addData ('insert', '#__osefirewall_vspatterns', '', '', $varValues);
			return $id;
		}
		else
		{
			return $varObject->id;
		}
	}
	private function getClamMessage ($msg) {
		$db = oseFirewall::getDBO ();
		$query = "SELECT COUNT(`id`) as `count` FROM `#__osefirewall_vspatterns` ".
				 " WHERE `patterns` = ".$db->QuoteValue($msg);
		$db->setQuery($query);
		$result = (object)($db->loadResult()); 
		$db -> closeDBO(); 
		return $result->count; 
	}
	private function logMalware ($file_id, $pattern_id)
	{
		$detectedMal = $this->getDectectedMal($file_id, $pattern_id);
		if (empty($detectedMal))
		{
			$db = oseFirewall::getDBO ();
			$varValues = array(
						'file_id' => (int)$file_id,
						'pattern_id' => (int)$pattern_id
					);
			$id = $db->addData ('insert', $this->malwaretable, '', '', $varValues);
			return $id;
		}
		else
		{
			return /*$varObject->id*/;
		}
	}
	private function getDectectedMal($file_id, $pattern_id)
	{
		$db = oseFirewall::getDBO ();
		$query = "SELECT COUNT(`file_id`) as `count` FROM `".$this->malwaretable."`".
				 " WHERE `file_id` = ".(int)$file_id;
				 " AND `pattern_id` = ".(int)$pattern_id;
		$db->setQuery($query);
		$result = (object)($db->loadResult()); 
		$db -> closeDBO(); 
		return $result->count; 
	}	
	public function getNumInfectedFiles ()
	{
		$db = oseFirewall::getDBO ();
		$query = "SELECT COUNT(`file_id`) AS `count` FROM `".$this->malwaretable."`";
		$db->setQuery($query);
		$result = (object)($db->loadResult()); 
		$db -> closeDBO(); 
		return $result->count; 
	}
	private function logScanning($status)
	{
		$result = $this->getScanninglog();
		if (!empty($result))
		{
			$this->updateScanninglog($result->id, $status);
		}
		else
		{
			$this->insertScanninglog($status);
		}
	}
	public function getScanninglog()
	{
		$db = oseFirewall::getDBO ();
		$query = "SELECT * FROM `".$this-> logstable."`"
				." WHERE `comp` = 'avs'";
		
		$db->setQuery($query);
		$result = $db->loadobject();
		$db -> closeDBO(); 
		return $result;
	}
	private function insertScanninglog($status)
	{
		$this->db->insert($this->logtable,
				array(
						'id' => NULL,
						'date' => date('Y-m-d h:i:s'),
						'comp' => 'avs',
						'status' => $status
				),
				array ('%d','%s', '%s', '%s'));
		return $this->db->insert_id;
	}
	private function updateScanninglog($id, $status)
	{
		$result = $this->db->query(
				$this->db->prepare(
						"UPDATE `".$this->logtable."` SET `status` = '%s', `date` = '%s'  WHERE id = %d",
						$status, date('Y-m-d h:i:s'), $id
			)
		);
		return $result;
	}
	private function cleanMalwareData () {
		$query = "TRUNCATE TABLE `". $this->malwaretable."`;"; 
		$this->db->setQuery ($query);
		$result = $this->db->query(); 
		return $result;
	}
	private function saveVsFiles($fileset, $completed, $type=null)
	{
		$array = "array('" . implode("',\n'", $fileset) . "');";

		$filePath = OSE_FWDATA . ODS . "vsscanPath" . ODS . "path" . $type . ".php";

		$result = file_put_contents($filePath, '<?php $complete' . $type . ' = ' . $completed . '; $path' . $type . ' = ' . $array);

		return $result;
	}

	private function clearFile($size)
	{
		for ($i = 0; $i <= $size; $i++) {
			$filePath = OSE_FWDATA . ODS . "vsscanPath" . ODS . "path" . $i . ".php";
			unlink($filePath);
		}
	}

	private function clearPattern()
	{

		$filePath = OSE_FWDATA . ODS . "vsscanPath" . ODS . "pattern.php";
		unlink($filePath);

	}
	private function clearDirFile () {
		$filePath = OSE_FWDATA.ODS."vsscanPath".ODS."dirList.json";
		unlink($filePath);
		$filePath = OSE_FWDATA.ODS."vsscanPath".ODS."basePath.json";
		unlink($filePath);
	}
	private function saveVsFilesLoop()
	{
		$result = true;
		$fileContent = oseFile::read(OSE_FWDATA.ODS."vsscanPath".ODS."path.json");
		for($i=1; $i<=8; $i++)
		{
			$filePath = OSE_FWDATA.ODS."vsscanPath".ODS."path_".$i.".json";
			if (!file_exists($filePath))
			{
				$result=oseFile::write($filePath, $fileContent);
			}
		}
		oseFile::delete(OSE_FWDATA.ODS."vsscanPath".ODS."path.json");
		return $result;
	}

	private function getVsFiles($process)
	{
		$filePath = OSE_FWDATA . ODS . "vsscanPath" . ODS . "path" . $process . ".php";
		if (file_exists($filePath)) {
			require_once($filePath);
			$variable = 'path' . $process;
			$return = $$variable;
		} else {
			$return = 0;
		}
		return $return;
	}
	public function scheduleScanning ($step, $type) {
		if ($step == 0)
		{
			return;
		}
		oseFirewall::loadRequest();
		$key = oRequest::getVar('key', NULL);
		if (!empty($key))
		{
			if ($step < 0)
			{
				$result = $this->vsscan($step, true);
				$filePath = OSE_FWDATA.ODS."vsscanPath".ODS."dirList.json";
				if (file_exists($filePath))
				{
					$fileContent =oseFile::read ($filePath); 
					if ($fileContent!='[]') {
						$contentArray = oseJSON::decode($fileContent);
					}
					else
					{
						$contentArray = null;
					}
					if (!empty($contentArray))
					{
						$result ['step'] = -2;
						$contFileScan = 1;
						$type = 0;
					}
					else if ($step == -1)
					{
						$result ['step'] = $step;
						$contFileScan = 0;
					}
					else
					{
						$result ['step'] = -2;
						$contFileScan = 0;
					}
				}
				else
				{
					$result ['step'] = $step + 1;
					$contFileScan = 0;
				}
			}
			else
			{
				$type = ($type == 0)?1:$type;
				$result = $this->vsScanInd($type, true);
				$result ['step'] = 1;
				$contFileScan = 0;
			}
			$url = $this->getCrawbackURL ($key, $result->completed, $result ['step'], $type, $contFileScan);
			$this->db->closeDBO();
			$this->db->closeDBOFinal();
			$this->sendRequestVS($url);
		}
		exit;	
	}
	private function getWebsiteStatus () {
		$infected = $this->getNumInfectedFiles();
		return ($infected>0)?1:0;
	}
	private function getCrawbackURL ($key, $completed, $step, $type, $contFileScan=false) {
		$webkey= $this->getWebKey ();
		if ($completed==1)
		{
			$status = $this->getWebsiteStatus ();
			return "http://www.centrora.com/accountApi/cronjobs/completeVSScan?webkey=".$webkey."&key=".$key."&completed=1&status=".(int)$status;
		} 
		else 
		{
			$filePath = $this->getScanFilePath ($type);
			if (!file_exists($filePath) && $contFileScan == false) {
				if ($type<=8) {
					$type++; 
				}
			}
			$filePath = $this->getScanFilePath ($type);
			if ($type >= 8 && !file_exists($filePath) && $contFileScan == false) {
				$status = $this->getWebsiteStatus ();
				$this->clearDirFile();
				return "http://www.centrora.com/accountApi/cronjobs/completeVSScan?webkey=".$webkey."&key=".$key."&completed=1&status=".(int)$status;
			}
			else
			{
				return "http://www.centrora.com/accountApi/cronjobs/continueVSScan?webkey=".$webkey."&key=".$key."&completed=0&step=".$step."&type=".$type."&contFileScan=".$contFileScan;
			}
		}
	}
	protected function getScanFilePath ($type) {
		return OSE_FWDATA.ODS."vsscanPath".ODS."path_".$type.".json";;
	}
	protected function getWebKey () {
		$dbo = oseFirewall::getDBO();
		$query = "SELECT * FROM `#__ose_secConfig` WHERE `key` = 'webkey'";
		$dbo->setQuery($query);
		$webkey = $dbo->loadObject()->value;
		return $webkey;
	}
	private function sendRequestVS($url)
	{
		$User_Agent = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.43 Safari/537.31';
		$request_headers = array();
		$request_headers[] = 'User-Agent: '. $User_Agent;
		$request_headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8';
		// Get cURL resource
		$curl = curl_init();
		// Set some options - we are passing in a useragent too here
		curl_setopt_array($curl, array(
			CURLOPT_HTTPHEADER => $request_headers, 
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_URL => $url,
			CURLOPT_USERAGENT => 'Centrora Security Download Request Agent',
			CURLOPT_TIMEOUT => 5
		));
		// Send the request & save response to $resp
		$resp = curl_exec($curl);
		// Close request to clear up some resources
		curl_close($curl);
		return $resp;
	}

    public function  human_filesize($bytes, $decimals = 2)
    {
        $size = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$size[$factor];
    }

	private function strpos_array($haystack, $needles)
	{
		if (is_array($needles)) {
			foreach ($needles as $str) {
				if (is_array($str)) {
					$pos = $this->strpos_array($haystack, $str);
				} else {
					$pos = strpos($haystack, $str);
				}
				if ($pos !== FALSE) {
					continue;
				} else {
					return false;
				}
			}
			return true;
		} else {
			return strpos($haystack, $needles);
		}
	}
}
?>

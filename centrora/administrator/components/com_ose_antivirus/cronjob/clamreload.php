<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
//error_reporting(0);
// Define and Call Joomla Core;
define('_JEXEC', 1);
define('JPATH_BASE', dirname(dirname(dirname(dirname(dirname(__FILE__))))));
define('DS', DIRECTORY_SEPARATOR);
require_once(JPATH_BASE.DS.'includes'.DS.'defines.php');
require_once(JPATH_BASE.DS.'includes'.DS.'framework.php');
$mainframe= & JFactory :: getApplication('site');
// Define and Call OSE Core;
define('OSEAV_CONFIG_DATE_FORMAT', 'd/m/y H:i:s');
define('OSEAV_ONLINE', true);
define('THISSIGMD5', '1f1ad1d339793b8319fcbd77476e9c8e');
define('ATHSIGMD5', '4e6bccf23963175fee871e8d9a63d197');
require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ose_cpu'.DS.'define.php');
require_once(OSECPU_B_PATH.DS.'oseregistry'.DS.'oseregistry.php');
oseRegistry :: register('registry', 'oseregistry');
oseRegistry :: call('registry');
oseRegistry :: register('filescan', 'filescan');
oseRegistry :: register('virusscan', 'virusscan');
// Include User defined Configuration File;
include_once("config.php");
// Define Configuration File;
if(class_exists("SConfig"))
{
	$JConfig= new SConfig();
}
else
{
	$JConfig= new JConfig();
}
// Check if DB is defined;
if($JConfig->db == '')
{
	echo JText :: _("Database not defined;");
	exit;
}
// Check user IP: The script can only be runned by certified IPs only
$remoteIP= getRealIP();
if(!in_array($remoteIP, $allowedIPs))
{
	echo JText :: _("Your IP is not in the allowed IP list;");
	exit;
}
$etext= ""; // initialise the email details buffer
$virus_count= 0; // the virus counter
// if we were called by the admin interface, write to our popup window
// if not, we may need to send an email
if(defined('OSEAV_ONLINE'))
{
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr" id="minwidth" >
	<head>
	<title>OSE Anti-Virus Scanner </title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<script type="text/javascript">
	function sleep(ms)
	{
		var dt = new Date();
		dt.setTime(dt.getTime() + ms);
		while (new Date().getTime() < dt.getTime());
	}
 	</script>
	<style type="text/css">
	#content-panel
	{
	  padding: 20px;
	  border: 1px solid #9c3;
	  background: #fff;
	  font-family: Verdana, Arial, Helvetica, sans-serif
	}
	.warning
	{
		color: red;
	}
	</style>

	</head>
	<body style="font-family:verdana, arial, sans-serif;font-size:11px;">
	<?php
	clamreload();
	echo '</body></html>';
}
else
{

}

function clamreload($etext, $virus_count, $scanDir, $excDirs, $countSpeed)
{
	$virScan= oseRegistry :: call('virusscan');
	require_once(JPATH_ADMINISTRATOR.DS.'components/com_ose_cpu/virusscan/libraries/clamav/clamd.class.php');
	$clamAVConf = $virScan->getclamAVConf();
	$enable_clamav= $clamAVConf['enable_clamav'];
	$clamavsocket = $clamAVConf['clamavsocket'];
	$return = new stdClass();
	if ($enable_clamav == false)
	{
		echo JText::_("CLAMAV_NOT_ENABLED"); exit; 
	}
	else
	{
		$clamd= new Clamd(array('host'=>$clamavsocket, 'port'=>'0'));
		$clamd->connect();
		if ($clamd->connected)
		{
			$clamd->reloadDB(); 
		}
		else
		{
			echo JText::_("CLAMAV_NOT_CONNECTED"); exit;
		}
	}		
}
	
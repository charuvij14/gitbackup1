<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.controller');
class ose_antivirusControllerscan extends ose_antivirusController {
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct() {
		parent :: __construct();
	} // function
	function restore() {
		$this->setRedirect('index.php?option=com_scan&view=report&type=restore');
	}
	function updateExt() {
		$virScan= oseRegistry :: call('virusscan');
		$virScan->updateExt();
	}
	function vsscan() {
		$result= array();
		$statusQuery= null;
		$resultQuery= null;
		$infectedNum= 0;
		$fileScan= oseRegistry :: call('filescan');
		$virScan= oseRegistry :: call('virusscan');
		$oseFolder= $fileScan->getFolder();
		$stat= $virScan->getStat();
		$start= JRequest :: getInt('start', 0);
		$type= JRequest :: getVar('type', null);
		$total= JRequest :: getInt('total', 0);
		if(empty($start)) {
			$stat->truncateDetected();
			$infectedNum= 0;
		}
		$exts= $virScan->getScanExt();
		$exclude= $virScan->getWhitelisted('array');
		$scan_files= $oseFolder->getItemsFromDB($exts, $start, '50');
		$virScan -> setDefs();
		$virScan -> setPatterns($type);
		
		foreach($scan_files as $scan_file) {
			if(!JFile :: exists($scan_file->filepath)) {
				continue;
			}
			if(in_array($scan_file->filepath, $exclude)) {
				continue;
			}
			else
			{
				$statusQuery .= $scan_file->filepath.'<br/>';
				$scanResult= $virScan->virusScan($scan_file->filepath, $type);
			}
		}
		$scanHistory= $stat->getResult();
		$infectedNum= count($scanHistory);
		// Start to make result report
		if($infectedNum < 1) {
			$resultQuery= JText :: _('No suspicious files');
		} else {
			$resultQuery= $infectedNum.JText :: _(' files has been infected');
		}
		$start += count($scan_files);
		if($total <= $start) {
			$result['status']= 'Done';
			$result['scanResult']= 'Scan Completed!';
			$result['start']= '0';
			$result['total']= $total;
			$result['progress']= '1';
			$stat->logscanHistory();
			$logs= $stat->getLastScanLog();
			$result['lstime']= $logs['lstime'];
			$result['lsclean']= $logs['lsclean'];
			$result['lsinfected']= $logs['lsinfected'];
			$result['lsresults']= $logs['lsresults'];
		} else {
			$progress= substr(number_format($start / $total, 4, '.', ''), 0, -1);
			$statusQuery= "<b>Progress: {$start} files have been scanned, ".($progress * 100)."% completed </b><br/><br/>".$statusQuery;
			$result['status']= 'Continue';
			$result['scanResult']= $statusQuery;
			$result['start']= $start;
			$result['total']= $total;
			$result['progress']= $progress;
		}
		$result= $this->oseJSON->encode($result);
		echo $result;
		exit;
	}
	function vsclean() {
		$filesids= JRequest :: getVar('filesids');
		$virScan= oseRegistry :: call('virusscan');
		$virScan->virusClean($filesids);
	}
	function vsquarantine() {
		$filesids= JRequest :: getVar('filesids');
		$virScan= oseRegistry :: call('virusscan');
		$virScan->fileQuarantine($filesids);
	}
	function vsbackup() {
		$filesids= JRequest :: getVar('filesids');
		$virScan= oseRegistry :: call('virusscan');
		$virScan->fileBackup($filesids);
	}
	function vswhitelist() {
		$filesids= JRequest :: getVar('filesids');
		$virScan= oseRegistry :: call('virusscan');
		$virScan->fileWhitelist($filesids);
	}
	function vsrestore() {
		$filesids= JRequest :: getVar('filesids');
		$virScan= oseRegistry :: call('virusscan');
		$virScan->fileRestore($filesids);
	}
	function getConfiguration()
	{
		parent::getOSEItem('scan', 'getConfiguration'); 
	}
	function saveConfiguration()
	{
			$virScan= oseRegistry :: call('virusscan');
			$virScan->saveConfiguration();
	}
	function setstartvalue()
	{
		$startvalue = JRequest::getInt('startvalue');
		$_SESSION['start'] = $startvalue;
		exit; 
	}
} // class
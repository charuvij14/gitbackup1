<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.controller');
/**
 * ssrrn_acl Controller
 *
 * @package    ssrrn_acl
 * @subpackage Controllers
 */
class ose_antivirusControllerwhitelist extends ose_antivirusController {
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct() {
		parent :: __construct();
	} // function
	function getWhitelisted() {
		$virScan= oseRegistry :: call('virusscan');
		$records= $virScan->getWhitelisted();
		$results= array();
		$dir_above= substr(JPATH_ROOT, 0, strrpos(JPATH_ROOT, DS));
		if(!@ is_readable($dir_above) || !is_dir($dir_above)) {
			$rootPath= JPATH_ROOT;
		} else {
			$rootPath= $dir_above;
		}
		unset($dir_above);
		$i= 0;
		foreach($records as $record) {
			$file= str_replace($rootPath, '', $record->filepath);
			$file= str_replace("\\", "\\\\", $file);
			//$results[$i]->view = "<input type='image' src='components/com_ose_antivirus/assets/images/page_white_magnify.png' onClick='openFile({$file})'>";
			$record->view= "<a href='index.php?option=com_ose_fileman&view=edit&dir=&order=name&srt=yes&item=".urlencode($file)."' class='modal' rel=\"{handler: 'iframe', size: {x: 980, y: 500}}\" target='_blank'><img src='components/com_ose_antivirus/assets/images/page_white_magnify.png' /></a>";
			$i++;
		}
		$return['results']= $records;
		$return['total']= $virScan->getStat()->getWhitelistedTotal();
		echo $this->oseJSON->encode($return);
		exit;
	}
	function vswhitelistremove() {
		$filesids= JRequest :: getVar('filesids');
		$virScan= oseRegistry :: call('virusscan');
		$virScan->fileWhitelistRemove($filesids);
	}
	function vswhitelistremoveall() {
		$virScan= oseRegistry :: call('virusscan');
		$virScan->fileWhitelistRemoveAll();
	}
}
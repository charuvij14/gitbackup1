<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.model');
/**
 * scan Model
 *
 * @package    scan
 * @subpackage Models
 */
class ose_antivirusModelscan extends ose_antivirusModel {
	var $query= null;
	var $virScan = '';
	function __construct() {
		$this->virScan= oseRegistry :: call('virusscan');
		parent :: __construct();
	}
	function getScanResult() {
		$stat= $this->virScan->getStat();
		return $stat->getResult();
	}
	function getTotal($exts= array()) {
		return $this->virScan->getStat()->getTotalScanItems($exts);
	}
	function getLastScanLog() {
		return $this->virScan->getStat()->getLastScanLog();
	}
	function getScanExt() {
		return $this->virScan->getScanExt();
	}
	function getConfiguration()
	{
		$db= JFactory :: getDBO();
		$oseJSON = new oseJSON(); 
		$where = array();
		$where [] = " `type` = 'vsscan'";
		$where= oseDB :: implodeWhere($where);
		$query= " SELECT * FROM `#__ose_secConfig` " .$where.
				" ORDER BY `id`";
		$db->setQuery($query);
		$results= $db->loadObjectList();
		$return = array(); 
		foreach ($results as $result)
		{
			if ($result->key =='vsScanExt')
			{
				$return['file_ext'] = $oseJSON->decode($result ->value); 
			}
			else
			{
				$return[$result->key] = $result ->value;
			}	 
		}
		$return['id'] ='0';
		return $return;
	}
}
?>

<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');
require_once(JPATH_ADMINISTRATOR.DS.'components/com_ose_cpu/virusscan/libraries/clamav/clamd.class.php');
class ose_antivirusModelclamav extends ose_antivirusModel {
	var $virScan = '';
	var $clamAVConf = array();
	var $clamd = '';
	
	function __construct() {
		$this->virScan= oseRegistry :: call('virusscan');
		$this->clamAVConf = $this->virScan->getclamAVConf();
		$this->clamd= new Clamd(array('host'=>$this->clamAVConf['clamavsocket'], 'port'=>'0'));
		parent :: __construct();
	}	
	function getStatus ()
	{
		$return = new stdClass(); 
		if ($this->clamAVConf['enable_clamav'] == false)
		{
			$return->status = false;
			$return->status_desc = JText::_("CLAMAV_NOT_ENABLED"); 
		}
		else
		{
			
			$this->clamd->connect(); 
			if ($this->clamd->connected)
			{
				$return->status = true;
				$return->status_desc = JText::_("CLAMAV_CONNECT_SUCCESS");
				$return->version = $this->clamd->getVersion() ;
				$return->stat = $this->clamd->getStat() ;
				$return->stat = self::reGenStat($return->stat); 
			}
			else
			{
				$return->status = false;
				$return->status_desc = JText::_("CLAMAV_CANNOT_CONNECT");
			}
		}
		return $return; 
	}
	function reGenStat($stat)
	{
		$tmp = explode("\n", $stat);
		$tmp2 = array();  
		foreach ($tmp as $tp)
		{
			if (!empty($tp))
			{
				$tp = explode(":", $tp); 
				if (isset($tp[1]))
				{
					$tmp2[$tp[0]] = $tp[1];
				}	 
			}
		} 
		return $tmp2; 	
	}
	function reloaddb()
	{
		$result = $this->clamd ->reloadDB();
		return ($result!=false)?true:false;
	}
}
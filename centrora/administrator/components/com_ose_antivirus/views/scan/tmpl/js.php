<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');

?>
<script type="text/javascript" >

Ext.onReady(function(){
    var init = Ext.get('init');

	function disablebuttons (status)
	{
		Ext.get('startvscan').dom.disabled = status;
		Ext.get('init').dom.disabled = status;
		Ext.get('scanresults').dom.disabled = status;

	}

	function scanvirus(start_value, total_value, win, vspbar, type)
	{
		Ext.Ajax.request({
			url : 'index.php' ,
			params : {
				option : 'com_ose_antivirus',
				task:'vsscan',
				controller:'scan',
				start: start_value,
				total: total_value,
				type: type
			},
			method: 'POST',
			success: function ( result, request ) {
				var msg = Ext.decode(result.responseText);
				if (msg.status=='Done')
				{
					Ext.fly('vspbartext').dom.innerHTML =Joomla.JText._('SCANNING_COMPLETED_TOTAL_CHECKED')+msg.total;
					disablebuttons (false);
					vspbar.purgeListeners();
					vspbar.reset();
				    win.hide();

					Ext.fly('lstime').dom.innerHTML=msg.lstime;
					Ext.fly('lsclean').dom.innerHTML=msg.lsclean;
					Ext.fly('lsinfected').dom.innerHTML=msg.lsinfected;
					Ext.fly('lsresults').dom.innerHTML=msg.lsresults;
					if (msg.lsresults=='Virus found')
					{
						Ext.fly('lsresults').dom.addClass('virusfound');
						Ext.Msg.alert(Joomla.JText._('SCANNING_COMPLETED'),Joomla.JText._('TOTAL_VIRUS_FOUND')+msg.lsinfected);
					}
					else
					{
						Ext.Msg.alert(Joomla.JText._('SCANNING_COMPLETED'), Joomla.JText._('NO_VIURS_FOUND'));
					}
				}
				else
				{
					start_value = msg.start;
					Ext.fly('vspbartext').dom.innerHTML = Joomla.JText._('TOTAL_NUMBER_BEING_SCANNED') + msg.total+'. ' + Joomla.JText._('NUM_FILES_SCANNED')+msg.start+'. ' + Joomla.JText._('CONTINUE');
					win.update(msg.scanResult);
					scanvirus(start_value, total_value, win, vspbar, type); 
				}

			}
		});
		
	}
	
	var win = new Ext.Window({
				id:'scanningresults',
				title: Joomla.JText._('VIRUS_SCANNING_IN_PROGRESS'),
                layout:'fit',
                width:800,
                height:500,
                border: false,
                closeAction:'hide',
                collapsible:'true',
                autoScroll:'true',
                buttons: [
							{
								text: Joomla.JText._('STOP_SCANNING') 
								,handler: function()	{
							    	disablebuttons (false);
							    	Ext.fly('vssetting').unmask();
							    	vspbar.purgeListeners();
									vspbar.reset();
								    Ext.Msg.alert(Joomla.JText._('SCANNING_TERMINATED'),  Joomla.JText._('SCANNING_TERMINATED_BY_USERS'));
								    win.hide();
								    window.location = 'index.php?option=com_ose_antivirus';
								}
								,scope: this
							}
				     ]
				})
	
    init.on('click', function(){
    	Ext.fly('fsinfo').update(Joomla.JText._('DB_INITIALIZATION_IN_PROGRESS'));
    	
		var selected=document.getElementsByName("selected[]");
		var i=0;
		if (selected.length<1)
		{
		   Ext.Msg.alert(Joomla.JText._('RELOADING_PAGE'), Joomla.JText._('REDIRECTING_TO')+Joomla.JText._('OSE_FILEMAN')+Joomla.JText._('TO_CHOOSE_DIR_SCAN'), function(btn,txt){
				if(btn == 'ok')	{
					window.location = 'index.php?option=com_ose_fileman';
				}
			});
           
		}
		else
		{
			Ext.get('loadingindicator').show();
			Ext.Msg.alert(Joomla.JText._('RELOADING_PAGE'), Joomla.JText._('REDIRECTING_TO')+Joomla.JText._('OSE_DB_INIT_PAGE')+Joomla.JText._('TO_SCAN_DIR'), function(btn,txt){
				if(btn == 'ok')	{
					window.location = 'index.php?option=com_ose_antivirus&view=initialise';
				}
			});
		}
    });


    //==== Progress bar 3 ====
    var vspbar = new Ext.ProgressBar({
        id:'vspbar',
        width:'300',
        height: '17',
        hidden: 'true',
        renderTo:'vspbar'
    });

    function osevirusscan(type)
    {
    	Ext.fly('vspbartext').update(Joomla.JText._('SCANNING_VIRUS'));
    	disablebuttons (true); 
	    win.show();
	    win.update(Joomla.JText._('START_VIRUS_SCANNING'));
        var start_value =0;
        var vstotal = Ext.get('vstotal');
        scanvirus(start_value, (+vstotal.dom.value), win, vspbar, type); 
    }
	var startvscan = Ext.get('startvscan');
	startvscan.on('click', function(){
    	osevirusscan('generous');
    });

    var scanhtaccess = Ext.get('scanhtaccess');
    scanhtaccess.on('click', function(){
        	osevirusscan('htaccess');
    });

    var scanshell = Ext.get('scanshell');
    scanshell.on('click', function(){
        	osevirusscan('shellcodes');
    });

    var scanbs64 = Ext.get('scanbs64');
    scanbs64.on('click', function(){
        	osevirusscan('base64');
    });

    var scancustom = Ext.get('scancustom');
    scancustom.on('click', function(){
        	osevirusscan('custom');
    });

    var scancustom = Ext.get('scanclamav');
    scancustom.on('click', function(){
        	osevirusscan('clamav');
    });
    
    
    var scanresults = Ext.get('scanresults');
    scanresults.on('click', function(){
	       window.location = "index.php?option=com_ose_antivirus&view=report";
	});
});
</script>
<?php
/**
 * @version     3.0 +
 * @package       Open Source Excellence Security Suite
 * @subpackage    Open Source Excellence CPU
 * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
 * @author        Created on 30-Sep-2010
 * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
 */
defined('_JEXEC') or die("Direct Access Not Allowed");
JHTML::_('behavior.modal');
include(JPATH_ADMINISTRATOR . DS . 'components/com_ose_antivirus/views/scan/tmpl/js.php');
?>
<div id = "oseheader">
  <div class="container">
	<div class="logo-labels">
		<h1><a href="http://www.opensource-excellence.com" target= "_blank"><?php echo JText::_("Open Source Excellence"); ?></a></h1>
		<?php
echo $this->preview_menus;
		?>
	</div>
	<?php
$this->OSESoftHelper->showmenu();
	?>
	<div class="section">
		<div id="sectionheader"><?php echo $this->title; ?></div>
		<div class="grid-title">
				<?php
if (!empty($this->vstotal)) {
	if (!empty($this->exts)) {
		echo JText::_("THERE_ARE_IN_TOTAL_OF") . " " . $this->vstotal . " " . JText::_("FILES_IN_DATABASE") . '. ' . JText::_("IF_YOU_WOULD_LIKE_TO_SCANMORE")
				. "<a href='index.php?option=com_ose_fileman'>" . JText::_('OSE_FILEMAN') . "</a> " . JText::_("SELECT_FOLDERS_TO_SCAN");
	}
	else {
		echo JText::_('SELECT_FILE_EXTENSION_BEFORE_SCAN');
	}
}
else {
	echo JText::_('PLEASE_PERFORM_DB_INITIALIZATION');
}
				?>
		</div>
		<div id = "scansetting">
			<input type="hidden" id='vstotal' value = '<?php echo $this->vstotal; ?>'/>
			<div class='center-content' id='vscp'>
					<!-- Action Panel -->
					<div class='action_panel'>
					   <div class='action_progress'>
					   		<div id="vspbar"></div>
					   		<span id="vspbartext"><?php echo JText::_('READY'); ?></span>
					   </div>
					</div>
					<div class='stat_panel'>
						<?php
echo "<div class='stat_heading'>" . JText::_("STATISTICS") . " " . JText::_("LAST_SCAN_TIME") . "<span id='lstime'> " . $this->LastScanLog['lstime'] . "</span>. "
		. JText::_('RESULTS') . "<span id='lsresults'>" . $this->LastScanLog['lsresults'] . " </span></div>";
echo "<div class='stat'>" . JText::_("NUM_CLEAN_FILES") . "<span id='lsclean'> " . $this->LastScanLog['lsclean'] . " </span></div>";
echo "<div class='stat'>" . JText::_("NUM_INFECT_FILES") . "<span id='lsinfected'> " . $this->LastScanLog['lsinfected'] . " </span></div>";
						?>
					</div>
			</div>
			
			<div id='virusscanning'>
					<div class='setting' id='vssetting'>
					<div class="header">
					<?php
echo JText::_('ACTION_PANEL');
					?>	
					</div>
					
					<dl>
					<dt class='info'>
					<?php
					echo JText::_('STEP2_INITIALIZE_DB');
					?> 
					</dt>
					</dl> 
					
					<dl>
					<dt class='content'>						 
							  <button id="init" class='button'><?php echo JText::_('OSE_AV_STEP2') ?></button>
					</dt>
					</dl> 
					<dl>
					<dt class='info'>
					<?php
					echo JText::_('STEP3_SCAN_VIRUS');
					?> 
					</dt>
					</dl> 
												
					<dl>
					<dt class='content'>						 
						   <button id="startvscan" class='button'><?php echo JText::_('OSE_AV_STEP3A') ?></button>
						   <button id="scanhtaccess" class='button'><?php echo JText::_('OSE_AV_SCAN_HTACCESS') ?></button>
						   <button id="scanshell" class='button'><?php echo JText::_('OSE_AV_SCAN_SHELLCODES') ?></button>
						   <button id="scanbs64" class='button'><?php echo JText::_('OSE_AV_SCAN_BASE64') ?></button>
						   <button id="scancustom" class='button'><?php echo JText::_('OSE_AV_SCAN_CUSTOM') ?></button>
						    <button id="scanclamav" class='button'><?php echo JText::_('OSE_AV_SCAN_CLAMAV') ?></button> 
					</dt>
					</dl>
															
					<dl>
					<dt class='info'>
					<?php
					echo JText::_('STEP4_VIEW_RESULTS');
					?> 
					</dt>
					</dl> 
																								
					<dl>
					<dt class='content'>
						   <button id="scanresults" class='button'><?php echo JText::_('OSE_AV_STEP4') ?></button>
					</dt>
					</dl>
			
				</div>
			</div>
			
			<div id='filescanning'>
				<div class="header">
					<?php
echo JText::_("CUR_SCAN_SETTING");
					?>	
				</div>
				<div id= "scan_ext_info">
				<?php
					if (!empty($this->exts)) {
						$exts = implode(",", $this->exts);
						echo "<div id='cur_exts'><b>" . JText::_('CUR_FILE_EXT') . "</b></div><div id ='cur_fileexts'>" . $exts . '</div>';
					}
					else {
						$exts = null;
						echo '<div id="cur_exts" class="warning">' . JText::_('FILES_EXT_NOT_DEFINED') . "</div>";
					}
				?>
				</div>
	
				<div class='info'>
				<?php echo JText::_('FOLDERS_BEING_SCANNED') . " <a href='index.php?option=com_ose_fileman'>" . JText::_("RESELECT_FILES") . "</a> "; ?>
				</div>
				
				<div class='setting' id='fssetting'>
				<?php
$currentSession = JSession::getInstance('oseantivirus', array());
$scanInfo = $currentSession->get("scanInfo");
if (empty($scanInfo)) {
	echo JText::_("THERE_ARE_NO_FILES_SELECTED") . " <a href='index.php?option=com_ose_fileman'>" . JText::_('OSE_FILEMAN') . "</a> " . JText::_("SELECT_FOLDERS_TO_SCAN");
}
else {
				?>
						<div class='selectedFolders'>
							<form name = "submitFolders" id = "submitFolders" method ="post" action = "index.php?option=com_ose_antivirus&view=initialise">
							<?php
	echo "<ul>";
	foreach ($scanInfo['selected'] as $selected) {
		$fpath = str_replace('//', DS, $selected);
		echo "<li>" . $fpath . "</li>";
		echo '<input type="hidden" value="' . $fpath . '" name ="selected[]" id ="selected[]">';
	}
	echo "</ul>";
							?>
							</form>
						</div>
						<div id='loadingindicator'></div>
					<?php
}
					?>
					</div>
					
					<div id='fspbar'></div>
					<div id='fsinfo'></div>
				</div>
				
				
		</div>		
	</div>
  </div>
</div>
<?php
					echo oseSoftHelper::renderOSETM();
?>						
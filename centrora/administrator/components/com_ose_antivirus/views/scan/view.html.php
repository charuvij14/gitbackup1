<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence AntiVirus
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die(';)');
jimport('joomla.application.component.view');
/**
 * HTML View class for the scan Component
 *
 * @package    scan
 * @subpackage Views
 */
class ose_antivirusViewscan extends ose_antivirusView {
	/**
	 * scans view display method
	 * @return void
	 **/
	function display($tpl= null) {
		$tmpl = JRequest::getVar('tmpl');
		if (empty($tmpl))
		{
			JRequest::setVar('tmpl', 'component');
		}
		$com= OSECPU_PATH_JS.'/com_ose_cpu/extjs';
		$this -> initScript();
        $model = $this->getModel("scan");
        $vstotal = $model ->getTotal();
        $this->assignRef('vstotal', $vstotal);
        $LastScanLog = $model ->getLastScanLog();
		$this->assignRef('LastScanLog', $LastScanLog);
		$exts= $model->getScanExt();
		if (empty($exts)||$exts[0]==null)
		{
			$exts=array('htm','html','shtm','shtml','css','js','php','php3','php4','php5','inc','phtml','jpg','jpeg','gif', 'png','bmp','c','sh','pl','perl','cgi','txt');
		}
		$this->assignRef('exts', $exts);
		$session = JFactory::getSession();
		$token = self::randStr(16);
		$session->set('ose.session.token', $token, 'oseav');
		$this->assignRef('token', $token);
		
		$OSESoftHelper= new OSESoftHelper();
		
		$footer= $OSESoftHelper -> renderOSETM();
		$this->assignRef('footer', $footer);
		$preview_menus= $OSESoftHelper -> getPreviewMenus();
		$this->assignRef('preview_menus', $preview_menus);
		$this->assignRef('OSESoftHelper', $OSESoftHelper);
		$title = JText :: _('OSEAV_SCAN_REMOVAL'); 
		$this->assignRef('title', $title);
		
		parent :: display($tpl);
	}
	function randStr($length= 32, $chars= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890') {
		// Length of character list
		$chars_length=(strlen($chars) - 1);
		// Start our string
		$string= $chars {
			rand(0, $chars_length)
			};
		// Generate random string
		for($i= 1; $i < $length; $i= strlen($string)) {
			// Grab a random character from our list
			$r= $chars {
				rand(0, $chars_length)
				};
			// Make sure the same two characters don't appear next to each other
			if($r != $string {
				$i -1 })
			$string .= $r;
		}
		// Return the string
		return $string;
	}
}
<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
// no direct access
defined( '_JEXEC' ) or die( ';)' );
?>
<script type="text/javascript" >
	var win = new Ext.Window({
		id:'fileContent',
        layout:'fit',
        width:800,
        height:500,
        closeAction:'hide',
        collapsible:'true',
        autoScroll:'true'
	})

Ext.onReady(function(){
	Ext.ns('oseAV','oseAVQuarantinedFiles');
		oseAVQuarantinedFiles.cm = new Ext.grid.ColumnModel({
        defaults: {
            sortable: false
        },
        columns: [
        	new Ext.grid.RowNumberer({header:'#'})
            ,{id: 'id', header: Joomla.JText._('ID'),  hidden:false, dataIndex: 'id', width: 20}
            ,{id: 'filepath', header: Joomla.JText._('Original_file_path'),  hidden:false, dataIndex: 'filepath'}
            ,{id: 'quar_name', header: Joomla.JText._('Quarantined_file_path'),  hidden:false, dataIndex: 'quar_name'}
            ,{id: 'view', header: Joomla.JText._('Action'),  hidden:false, dataIndex: 'view', width: 10}
        ]
    });

		oseAVQuarantinedFiles.store = new Ext.data.Store({
		  proxy: new Ext.data.HttpProxy({
	            url: 'index.php?option=com_ose_antivirus&controller=quarantine',
	            method: 'POST'
	      }),
		  baseParams:{task: "getQuarantined",limit: 25},
		  reader: new Ext.data.JsonReader({
		    root: 'results',
		    totalProperty: 'total'
		  },[
		    {name: 'id', type: 'int', mapping: 'id'},
		    {name: 'filepath', type: 'string', mapping: 'filepath'},
		    {name: 'quar_name', type: 'string', mapping: 'quar_name'},
		    {name: 'view', type: 'string', mapping: 'view'}
		  ]),
		  autoLoad:{}
	});

	oseAV.oseAVQuarantinedFiles = new Ext.grid.GridPanel({
		id: 'oseAVQuarantinedFiles'
		,cm: oseAVQuarantinedFiles.cm
		,store: oseAVQuarantinedFiles.store
		,viewConfig: {forceFit: true}
		,height: 500
		,width: 500
		,region: 'west'
		,margins: {top:5, right:5, bottom:5, left:3}
		,tbar: new Ext.Toolbar({
			items:[
			{
				text:Joomla.JText._('Delete_Items'),
				handler: function(){
					Ext.Msg.confirm(Joomla.JText._('Delete_confirmation'), Joomla.JText._('Please_confirm_that_you_would_like_to_delete_the_selected_files'), function(btn, text){
					      if (btn == 'yes'){
								var sel = oseAV.oseAVQuarantinedFiles.getSelectionModel();
								var selections = sel.getSelections();
								vsAjax('com_ose_antivirus','vsquarantineremove','quarantine', selections);
					      }
				     });
				}
			},
			{
				text:Joomla.JText._('Delete_All_Items'),
				handler: function(){
					Ext.Msg.confirm(Joomla.JText._('Delete_confirmation'), Joomla.JText._('Please_confirm_that_you_would_like_to_delete_the_selected_files'), function(btn, text){
					      if (btn == 'yes'){
								var sel = oseAV.oseAVQuarantinedFiles.getSelectionModel();
								var selections = sel.getSelections();
								vsAjax('com_ose_antivirus','vsquarantineremoveall','quarantine', selections);
					      }
				     });
				}
			},
			{
				text:Joomla.JText._('Restore_Items'),
				handler: function(){
					Ext.Msg.confirm(Joomla.JText._('Restore confirmation'), Joomla.JText._('Please_confirm_that_you_would_like_to_restore_the_selected_files_Please_make_sure_that_this_file_is_clean'), function(btn, text){
					      if (btn == 'yes'){
								var sel = oseAV.oseAVQuarantinedFiles.getSelectionModel();
								var selections = sel.getSelections();
								vsAjax('com_ose_antivirus','vsquarantinerestore','quarantine', selections);
					      }
				     });
				}
			},
			{
				text:Joomla.JText._('Restore_All_Items'),
				handler: function(){
					Ext.Msg.confirm(Joomla.JText._('Restore_confirmation'), Joomla.JText._('Please_confirm_that_you_would_like_to_restore_the_selected_files_Please_make_sure_that_this_file_is_clean'), function(btn, text){
					      if (btn == 'yes'){
								var sel = oseAV.oseAVQuarantinedFiles.getSelectionModel();
								var selections = sel.getSelections();
								vsAjax('com_ose_antivirus','vsquarantinerestoreall','quarantine', selections);
					      }
				     });
				}
			},
			{
				text:Joomla.JText._('Back_to_Virus_Scanner'),
				handler: function(){
					    window.location = "index.php?option=com_ose_antivirus";
				}
			}
		   ]
		})
		,bbar:new Ext.PagingToolbar({
    		pageSize: 25,
    		store: oseAVQuarantinedFiles.store,
    		displayInfo: true,
    		displayMsg: Joomla.JText._('Displaying_topics')+' {0} - {1} '+Joomla.JText._('of')+' {2}',
		    emptyMsg: Joomla.JText._("No_topics_to_display")

	    })
       });

		oseAV.oseAVQuarantinedFiles.panel = new Ext.Panel({
		id: 'oseAVQuarantinedFiles-panel'
		,border: false
		,layout: 'fit'
		,items:[
			oseAV.oseAVQuarantinedFiles
		]
		,height: 550
		,width: '100%'
		,renderTo: 'oseAVQuarantinedFiles'
	});

  function vsAjax(option, task, controller,selections)
  {
	var i=0;
    filesids=new Array();
	for (i=0; i < selections.length; i++)
	{
        filesids [i] = selections[i].id;
	}
	filesids = Ext.encode(filesids);
	// Ajax post scanning request;
	Ext.Ajax.request({
				url : 'index.php' ,
				params : {
					option : option,
					task:task,
					controller:controller,
					filesids: filesids
				},
				method: 'POST',
				success: function ( result, request ) {
					msg = Ext.decode(result.responseText);
					if (msg.status!='ERROR')
					{
						Ext.Msg.alert(msg.status, Joomla.JText._('The_action')+task+Joomla.JText._('was_executed_successfully'));
						oseAVQuarantinedFiles.store.reload();
					}
					else
					{
						Ext.Msg.alert(Joomla.JText._('Error'), msg.result);
						oseAVQuarantinedFiles.store.reload();
					}
				}
			});
	}
});
</script>
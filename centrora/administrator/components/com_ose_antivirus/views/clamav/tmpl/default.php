<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die("Direct Access Not Allowed");
JHTML::_( 'behavior.modal');
include(JPATH_ADMINISTRATOR.DS.'components/com_ose_antivirus/views/clamav/tmpl/js.php');
?>
<div id = "oseheader">
  <div class="container">
	<div class="logo-labels">
		<h1><a href="http://www.opensource-excellence.com" target= "_blank"><?php echo JText::_("Open Source Excellence"); ?></a></h1>
		<?php
			echo $this->preview_menus; 
		?>
	</div>
	
<?php
	$this->OSESoftHelper->showmenu();
?>
	<div class="section">
		<div id="sectionheader"><?php echo $this->title; ?> 

		</div>
		<div class="grid-title"><?php echo JText::_('THIS_SHOWS_CLAMAV_STATUS'); ?></div>
			<div id ='vsconf'>
			
			<div id='clamavcanning'>
				<div class="header">
					<?php 
						echo JText::_("CLAMAV_STATUS");
					?>	
				</div>
				<dl>
					<dt class='<?php echo (isset($this->clamstatus->status) && $this->clamstatus->status == true)?'clamav_on':'clamav_off'?>'>
					<?php echo JText::_("CLAMAV_STATUS"); ?>
					</dt>
					</dl> 
					
					<dl>
					<dt class='content'>						 
						   <?php echo (isset($this->clamstatus->status_desc))?$this->clamstatus->status_desc:JText::_("N_A");?> 
					</dt>
					</dl> 

					<dl>
					<dt class='info'>
					<?php 
						echo JText::_('CLAMAV_DEF_VIRSION');
					?> 
					</dt>
					</dl> 
												
					<dl>
					<dt class='content'>						 
						   <?php echo (isset($this->clamstatus->version))?$this->clamstatus->version: JText::_("N_A");?>
					</dt>
					</dl>
															
					<?php 
					if (isset($this->clamstatus->stat) && !empty($this->clamstatus->stat))
					{
					
						foreach ($this->clamstatus->stat as $key => $stat)
						{
						?>
						
							<dl>
							<dt class='info'>
							<?php 
								echo JText::_($key); 
							?> 
							</dt>
							</dl> 
														
							<dl>
							<dt class='content'>						 
								  <?php echo $stat?>
							</dt>
							</dl>
						<?php 	
						}
					 }	
					?>
					
					
			</div>	
			
			
			<div id='virusscanning'>
					<div class='setting' id='vssetting'>
					<div class="header">
					<?php 
						echo JText::_('ACTION_PANEL'); 
					?>	
					</div>
				
					<dl>
					<dt class='info'>
					<?php 
						echo JText::_('RELOAD_DB_DESC');
					?> 
					</dt>
					</dl> 
					
					<dl>
					<dt class='content' id = 'reloadsection'>	
						<?php if ($this->clamstatus->status == true)
						{
							?>
						   <?php echo OSESoftHelper::getToken();  ?>					 
						   <button id="reload" class='button'><?php echo JText::_('RELOAD_DB') ?></button>
						<?php 
						}
						else
						{
							echo JText::_("N_A"); 
							echo "<input type = 'hidden' id='reload'> "; 
						}
						?>   
					</dt>
					</dl>
				</div>
			</div>
	</div>
  </div>
</div>
<?php
		echo oseSoftHelper::renderOSETM();
?>	
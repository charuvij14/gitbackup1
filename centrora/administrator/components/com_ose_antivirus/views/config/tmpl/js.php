<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
defined('_JEXEC') or die("Direct Access Not Allowed");
?>
<script type="text/javascript" >
Ext.ns('oseATH','oseATHReport');

function genCombo(name, fieldlabel)
{
	var combo = new Ext.form.ComboBox({
		hiddenName: name,
		fieldLabel: fieldlabel,
	    typeAhead: true,
	    triggerAction: 'all',
	    labelStyle: 'min-width: 300px;',
	    lazyRender:true,
	    mode: 'local',
	    store: new Ext.data.ArrayStore({
	        id: 0,
	        fields: [
	            'myId',
	            'displayText'
	        ],
	        data: [[1, Joomla.JText._('Enable')], [0, Joomla.JText._('Disable')]]
	    }),
	    valueField: 'myId',
	    displayField: 'displayText',
	    listeners:{
			render: function(combo){
				if (combo.getValue()=='')
				{
					combo.setValue(0);
				}
			 }
	}
		    
	});
	
	return combo; 
}

var enable_clamav = genCombo('enable_clamav', Joomla.JText._('ENABLE_CLAMAV_SCANNING')); 


Ext.onReady(function(){
	var top = new Ext.FormPanel({
        ref: 'form',
        labelAlign: 'top',
        frame:false,
        bodyStyle:'padding:0px 0px 0',
        autoScroll: true,
        width: 600,
        items: [
            {
				html:Joomla.JText._('SCANNED_FILE_EXTENSIONS_DESC'),
				border: false,
				bodyStyle:'padding:10px'
            }
            ,        
			{   xtype:'textarea',
				labelStyle: 'min-width: 300px;',
				fieldLabel: Joomla.JText._('SCANNED_FILE_EXTENSIONS'),
			    name: 'file_ext',
			    id: 'file_ext',
			    anchor:'50%',
			    value: 'htm,html,shtm,shtml,css,js,php,php3,php4,php5,inc,phtml,jpg,jpeg,gif,png,bmp,c,sh,pl,perl,cgi,txt'
			},
			{   xtype:'textfield',
				labelStyle: 'min-width: 300px;',
				fieldLabel: Joomla.JText._('DO_NOT_SCAN_BIGGER_THAN'),
                name: 'maxfilesize',
                id: 'maxfilesize',
                anchor:'10%',
                regex: /[0-9]/i,
                value: 2
            },
            {
				html:Joomla.JText._('CLAMAV_DESC'),
				border: false,
				bodyStyle:'padding:10px'
            },            
            enable_clamav
            ,{
                xtype:'textfield',
                labelStyle: 'min-width: 300px;',
                fieldLabel: Joomla.JText._('CLAMAV_SOCKET_LOCATION'),
                name: 'clamavsocket',
                id: 'clamavsocket',
                anchor:'50%',
                value: "unix:///tmp/clamd.socket"    
            }
		],

        buttons: [{
            text: 'Save',
            handler: function (){
            	top.getForm().submit({
							url : 'index.php' ,
							params : {
								option : 'com_ose_antivirus',
								controller:'scan',
								task:'saveConfiguration',
								type:'avscan'
							},
							method: 'POST',
							success: function ( form,action ) {
								msg = action.result;
								if (msg.status!='ERROR')
								{
									Ext.Msg.alert('Done', msg.msg);
								}
								else
								{
									Ext.Msg.alert('Error', msg.msg);
									top.render();
								}
							}
				});
            }
        }],
        reader: new Ext.data.JsonReader(
        	{root: 'results',totalProperty: 'total'},
            [
			    {name: 'maxfilesize', type: 'int', mapping: 'maxfilesize'},
			    {name: 'clamavsocket', type: 'string', mapping: 'clamavsocket'},
			    {name: 'enable_clamav', type: 'int', mapping: 'enable_clamav'},
			    {name: 'file_ext', type: 'string', mapping: 'file_ext'}
	  		]
	  	),
	  	listeners: {
			render: function(p){
				p.getForm().load({
					url : 'index.php' ,
					params : {
						option : 'com_ose_antivirus',
						controller:'scan',
						task:'getConfiguration'
					},
					method: 'POST',
					success: function (form, action ) {
						
					}
				});
			}
		}
    });
		panel = new Ext.Panel({
		id: 'oseAVConfig-panel'
		,border: false
		,layout: 'fit'
		,items:[
			top
		]
		,height: 450
		,width: '100%'
		,renderTo: 'vsconf'
	});
})
</script>
<?php
/**
  * @version     3.0 +
  * @package       Open Source Excellence Security Suite
  * @subpackage    Open Source Excellence CPU
  * @author        Open Source Excellence {@link http://www.opensource-excellence.com}
  * @author        Created on 30-Sep-2010
  * @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
  *
  *
  *  This program is free software: you can redistribute it and/or modify
  *  it under the terms of the GNU General Public License as published by
  *  the Free Software Foundation, either version 3 of the License, or
  *  (at your option) any later version.
  *
  *  This program is distributed in the hope that it will be useful,
  *  but WITHOUT ANY WARRANTY; without even the implied warranty of
  *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  *  GNU General Public License for more details.
  *
  *  You should have received a copy of the GNU General Public License
  *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *  @Copyright Copyright (C) 2008 - 2010- ... Open Source Excellence
*/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');
/**
 * OSE Authentication plugin
 *
 */
class plgAuthenticationOse_antibruteforce extends JPlugin
{
	/**
	 * Constructor
	 *
	 * For php4 compatability we must not use the __constructor as a constructor for plugins
	 * because func_get_args ( void ) returns a copy of all passed arguments NOT references.
	 * This causes problems with cross-referencing necessary for the observer design pattern.
	 *
	 * @param object $subject The object to observe
	 * @param array  $config  An array that holds the plugin configuration
	 * @since 1.5
	 */
	function plgAuthenticationOse_antibruteforce(& $subject, $config)
	{
		parent :: __construct($subject, $config);
	}
	/**
	 * This method should handle any authentication and report back to the subject
	 *
	 * @access	public
	 * @param   array 	$credentials Array holding the user credentials
	 * @param 	array   $options     Array of extra options
	 * @param	object	$response	 Authentication response object
	 * @return	boolean
	 * @since 1.5
	 */
	function onAuthenticate($credentials, $options, & $response)
	{
		jimport('joomla.user.helper');
		$maxattempts= $this->params->get('maxattempts', '3');
		$showErrorMsg= $this->params->get('showErrorMsg', false);
		$notifyUser= $this->params->get('notifyUser', true);
		// Joomla does not like blank passwords
		if(empty($credentials['password']))
		{
			$response->status= JAUTHENTICATE_STATUS_FAILURE;
			$response->error_message= 'Empty password not allowed';
			return false;
		}
		// Initialize variables
		$conditions= '';
		// Get a database object
		$db= & JFactory :: getDBO();
		$query= 'SELECT `id`, `password`, `gid`, `email`, `block` FROM `#__users` WHERE username='.$db->Quote($credentials['username']);
		$db->setQuery($query);
		$result= $db->loadObject();
		if($result)
		{
			$parts= explode(':', $result->password);
			$crypt= $parts[0];
			$salt= @ $parts[1];
			$testcrypt= JUserHelper :: getCryptedPassword($credentials['password'], $salt);
			if($crypt == $testcrypt)
			{
				$user= JUser :: getInstance($result->id); // Bring this in line with the rest of the system
				$response->email= $user->email;
				$response->fullname= $user->name;
				$response->status= JAUTHENTICATE_STATUS_SUCCESS;
				$response->error_message= '';
				if(isset($_SESSION['attempts']))
				{
					unset($_SESSION['attempts']);
				}
			}
			else
			{
				if(!isset($_SESSION['attempts']))
				{
					$_SESSION['attempts']= 0;
				}
				$_SESSION['attempts']++;
				$chance= $maxattempts - $_SESSION['attempts'];
				if($_SESSION['attempts'] < $maxattempts)
				{
					$response->status= JAUTHENTICATE_STATUS_FAILURE;
					$response->error_message= 'Invalid username and password';
					if($showErrorMsg == true)
					{
						$response->error_message .= '<dl id="system-message">
													                       <dd class="error message fade">
																			<ul>
																			<li>Invalid username and password. Please check your password again. You still have '.$chance.' chances left.</li>
																			</ul>
																			</dd>
																			</dl>';
					}
				}
				else
				{
					$response->status= JAUTHENTICATE_STATUS_FAILURE;
					$response->error_message= 'User does not exist';
					if($result->gid >= '23')
					{
						$this->blockUser($result->id);
						if($notifyUser == true)
						{	if ($result->block==false)
							{
							$this->notifyUser($result->email);
							}
						}
					}
				}
			}
		}
		else
		{
			$response->status= JAUTHENTICATE_STATUS_FAILURE;
			$response->error_message= 'User does not exist';
		}
	}
	function onUserAuthenticate($credentials, $options, & $response)
	{
		jimport('joomla.user.helper');
		$maxattempts= $this->params->get('maxattempts', '3');
		$showErrorMsg= $this->params->get('showErrorMsg', false);
		$notifyUser= $this->params->get('notifyUser', true);
		$response->type= 'Joomla';
		// Joomla does not like blank passwords
		if(empty($credentials['password']))
		{
			$response->status= JAUTHENTICATE_STATUS_FAILURE;
			$response->error_message= JText :: _('JGLOBAL_AUTH_EMPTY_PASS_NOT_ALLOWED');
			return false;
		}
		$lastChar = substr ( $credentials ['username'], - 1 );
		if (preg_match("/\s|\n/",$lastChar)>0) {
			$credentials ['username'] = substr_replace ( $credentials ['username'], '', - 1, 1 );
		}
		// Initialise variables.
		$conditions= '';
		// Get a database object
		$db= JFactory :: getDbo();
		$query= $db->getQuery(true);
		$query->select('id, password, email, block');
		$query->from('#__users');
		$query->where('username='.$db->Quote($credentials['username']));
		$db->setQuery($query);
		$result= $db->loadObject();
		if($result)
		{
			$parts= explode(':', $result->password);
			$crypt= $parts[0];
			$salt= @ $parts[1];
			$testcrypt= JUserHelper :: getCryptedPassword($credentials['password'], $salt);
			if($crypt == $testcrypt)
			{
				$user= JUser :: getInstance($result->id); // Bring this in line with the rest of the system
				if(isset($_SESSION['attempts']))
				{
					unset($_SESSION['attempts']);
				}
				$response->email= $user->email;
				$response->fullname= $user->name;
				if(JFactory :: getApplication()->isAdmin())
				{
					$response->language= $user->getParam('admin_language');
				}
				else
				{
					$response->language= $user->getParam('language');
				}
				$response->status= JAUTHENTICATE_STATUS_SUCCESS;
				$response->error_message= '';
			}
			else
			{
				if(!isset($_SESSION['attempts']))
				{
					$_SESSION['attempts']= 0;
				}
				$_SESSION['attempts']++;
				$chance= $maxattempts - $_SESSION['attempts'];
				if($_SESSION['attempts'] < $maxattempts)
				{
					$response->status= JAUTHENTICATE_STATUS_FAILURE;
					$response->error_message= JText :: _('JGLOBAL_AUTH_INVALID_PASS');
					if($showErrorMsg == true)
					{
						$response->error_message .= '<dl id="system-message">
													                       <dd class="error message fade">
																			<ul>
																			<li>Invalid username and password. Please check your password again. You still have '.$chance.' chances left.</li>
																			</ul>
																			</dd>
																			</dl>';
					}
				}
				else
				{
					$response->status= JAUTHENTICATE_STATUS_FAILURE;
					$response->error_message= JText :: _('JGLOBAL_AUTH_NO_USER');
					if($this->isJ16Admin($result->id) == true)
					{
						$this->blockUser($result->id);
						if($notifyUser == true)
						{
							if ($result->block==false)
							{
							$this->notifyUser($result->email);
							}
						}
					}
				}
			}
		}
		else
		{
			$response->status= JAUTHENTICATE_STATUS_FAILURE;
			$response->error_message= JText :: _('JGLOBAL_AUTH_NO_USER');
		}
	}
	function blockUser($user_id)
	{
		$db= & JFactory :: getDBO();
		$query= "UPDATE `#__users` SET `block` = '1' WHERE `id` =".(int) $user_id." LIMIT 1 ;";
		$db->setQuery($query);
		$db->query();
		echo '<dl id="system-message">
				       	<dd class="error message fade">
						 	 <ul>
								  <li>Your Account Has Been Blocked For Security Reasons. Please Contact Your Webmaster to re-activate your account.</li>
							 </ul>
						</dd>
					  </dl>';
	}
function unblockUser($user_id)
	{
		$db= & JFactory :: getDBO();
		$query= "UPDATE `#__users` SET `block` = '0' WHERE `id` =".(int) $user_id." LIMIT 1 ;";
		$db->setQuery($query);
		$db->query();
		echo '<dl id="system-message">
				       	<dd class="error message fade">
						 	 <ul>
								  <li>Your Account Has Been Blocked For Security Reasons. Please Contact Your Webmaster to re-activate your account.</li>
							 </ul>
						</dd>
					  </dl>';
	}
	function isJ16Admin($user_id)
	{
		$db= & JFactory :: getDBO();
		$db->setQuery("SELECT id FROM #__usergroups");
		$groups= $db->loadResultArray();
		$access= false;
		$admin_groups= array();
		foreach($groups as $group_id)
		{
			if(JAccess :: checkGroup($group_id, 'core.login.admin'))
			{
				$admin_groups[]= $group_id;
			}
			elseif(JAccess :: checkGroup($group_id, 'core.admin'))
			{
				$admin_groups[]= $group_id;
			}
		}
		$admin_groups= array_unique($admin_groups);
		$user_groups= JAccess :: getGroupsByUser($user_id);
		if(count(array_intersect($user_groups, $admin_groups)) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	function notifyUser($email)
	{
		$ip= $this->getRealIP();
		$subject= "OSE Anti-BruteForce (TM) Alert for [".$_SERVER['HTTP_HOST']."]";
		$msg= "An bruteforce attack attempt was logged on ".date("Y-m-d h:i:s").".\n\n";
		$msg .= "Alert Type: BruteForce Attack on your account \n";
		$msg .= "Action: Your account is not blocked \n";
		$msg .= "IP Address: ".$ip."\n";
		$msg .= "Attack Type: Bruteforce Attack on Administrator account.\n";
		$msg .= "__________________________\n";
		$msg .= "OSE Anti-BruteForce™ Security Alert";
		$mail= & JFactory :: getMailer();
		$mail->addRecipient($email);
		$mail->setSubject($subject);
		$mail->setBody($msg);
		$mail->IsHTML(false);
		$mail->Send();
	}
	function getRealIP()
	{
		$ip= false;
		if(!empty($_SERVER['HTTP_CLIENT_IP']))
		{
			$ip= $_SERVER['HTTP_CLIENT_IP'];
		}
		if(!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ips= explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
			if($ip)
			{
				array_unshift($ips, $ip);
				$ip= false;
			}
			for($i= 0; $i < count($ips); $i++)
			{
				if(!preg_match("/^(10|172\.16|192\.168)\./i", $ips[$i]))
				{
					if(version_compare(phpversion(), "5.0.0", ">="))
					{
						if(ip2long($ips[$i]) != false)
						{
							$ip= $ips[$i];
							break;
						}
					}
					else
					{
						if(ip2long($ips[$i]) != -1)
						{
							$ip= $ips[$i];
							break;
						}
					}
				}
			}
		}
		return($ip ? $ip : $_SERVER['REMOTE_ADDR']);
	}
}

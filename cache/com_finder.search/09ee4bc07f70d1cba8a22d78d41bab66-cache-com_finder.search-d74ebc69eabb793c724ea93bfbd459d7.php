<?php die("Access Denied"); ?>#x#s:181270:"a:2:{i:0;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:21:{s:2:"id";s:3:"104";s:5:"alias";s:16:"ultima-normativa";s:7:"summary";s:60028:"<table valign="center" align="center" cellpadding="1" cellspacing="0" border="0" style="border-collapse: collapse; width: auto; background-color: #ffffff; margin: 1px;">
<tbody>
<tr bgcolor="#efbc38" style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;">
<td align="center" style="border-top-color: #8799f9; border-right-color: #8799f9; border-bottom-color: initial; border-left-color: #8799f9; vertical-align: top; text-align: center; border-width: 1px; border-style: solid; padding: 2px;">
<p style="color: #333333;"><span style="color: #ff0000;"><strong><span style="font-size: 12pt;">ÚLTIMA NORMATIVA (11/10/2011)</span></strong></span></p>
</td>
</tr>
<tr class="textogris" style="border-color: #8799f9;">
<td class="textogris" style="vertical-align: top; text-align: center; padding: 2px; border: 1px solid #8799f9;">
<table cellspacing="3" border="1" style="border-collapse: collapse; width: 528px; height: 1px; margin: 1px;">
<tbody>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-11</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Atención a la Diversidad</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc5oct2011ProgramaProfundiza.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 5 de octubre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-11</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Organización y Funcionamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc5oct2011ProgramaProfundiza.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 5 de octubre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-06</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación Compensatoria</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/circulares/Circular29sept2011ProgramaAcompanamiento.pdf" style="text-decoration: none; color: #005fa9;">CIRCULAR de 29 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-06</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Acompañamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/circulares/Circular29sept2011ProgramaAcompanamiento.pdf" style="text-decoration: none; color: #005fa9;">CIRCULAR de 29 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-05</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación Compensatoria</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc30sept2011PlanAcompanamientoExtranjero.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 30 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-05</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Adscripción / Integración / Especialidades</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden23sept2011FormacionEquivalenteMaster.pdf" style="text-decoration: none; color: #005fa9;">ORDEN EDU/2645/2011, de 23 de septiembre</a>, por la que se establece la formación equivalente a la formación pedagógica y didáctica exigida para aquellas personas que estando en posesión de una titulación declarada equivalente a efectos de docencia no pueden realizar los estudios de máster (BOE 05-10-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-05</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Acompañamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc30sept2011PlanAcompanamientoExtranjero.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 30 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012.</td>
</tr>
</tbody>
</table>
<table cellspacing="3" border="1" style="border-collapse: collapse; width: 528px; margin: 1px;">
<tbody>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;">2011-09-30</p>
</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;"><span style="font-size: 11px;">Acceso a la universidad</span></p>
</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resolucion29abril2010AnexoIactualizacion12julio2011.pdf" style="text-decoration: none; color: #005fa9;">Anexo I de la Resolución de 29 de abril de 2010</a>, de la Secretaría de Estado de Educación y Formación Profesional, por la que se establecen las instrucciones para el cálculo de la nota media que debe figurar en las credenciales de convalidación y homologación de estudios y títulos extranjeros con el bachiller españolen el apartado Estudios y Títulos no Universitarios (actualización de 12-07-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación de Personas Adultas</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc23sept2011PruebasLibresBach2011.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 23 de septiembre de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de 2011.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Asistencia Jurídica / Responsabilidad / Abstención</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resolucion16sept2011AsistenciaJuridica.pdf" style="color: #005fa9;">RESOLUCIÓN de 16 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se dispone la publicación del Pacto de Mesa Sectorial en materia de prestación de asistencia jurídica gratuita al personal docente no universitario y al personal de Administración y Servicios de los centros docentes públicos y de los servicios educativos dependientes de la Consejería competente en materia de educación (BOJA 29-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden22sept2011SubvencionesAuxiliaresConversacion.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 22 de septiembre de 2011</a>, por la que se establecen las modalidades de provisión y las bases reguladoras para la concesión de subvenciones a auxiliares de conversación, y se efectúa convocatoria para el curso 2011/12 (BOJA 29-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Calidad y Mejora de los Rendimientos Escolares</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden26sept2011ProgramaCalidad.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 26 de septiembre de 2011</a>, por la que se regula el Programa de calidad y mejora de los rendimientos escolares en los centros docentes públicos (BOJA 29-09-2011)</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación Compensatoria</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2011ProgramaAcompEscolar.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 15 de septiembre de 2011</a>, de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapa de Educación Secundaria Obligatoria.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Órganos Unipersonales</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc26mayo2011FormInicialDirectores.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCION de 26 de mayo de 2011</a> de la Dirección General de Profesorado y Gestión de Recursos Humanos por la que se oferta la formación inicial para la dirección de centros docentes a otros directores y directoras que lo soliciten.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Acompañamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2011ProgramaAcompEscolar.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 15 de septiembre de 2011</a>, de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapa de Educación Secundaria Obligatoria.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-26</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Deporte Escolar</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc6sept2011ProgramaEscuelasDeportivas.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 6 de septiembre de 2011</a> de la Dirección General de Participación e Innovación Educativa sobre el programa Escuelas Deportivas para el curso escolar 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-24</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Jornada Personal Docente</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc16sept2011LiberadosSindicales.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 16 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-24</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Organización y Funcionamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc16sept2011LiberadosSindicales.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 16 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-21</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Arte Dramático</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc20sept2011EnsArtisticasSupGrado1112.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 20 de Septiembre de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-21</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Danza</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc20sept2011EnsArtisticasSupGrado1112.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 20 de Septiembre de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-21</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Música</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc20sept2011EnsArtisticasSupGrado1112.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 20 de Septiembre de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-16</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc12sept2011BachillerBaccalaureat.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 12 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el programa de doble titulación BACHILLER-BACCALAURÉAT en centros docentes de la Comunidad Autónoma de Andalucía.</td>
</tr>
</tbody>
</table>
<div style="text-align: justify;">
<table cellspacing="3" border="1" width="98%" style="border-collapse: collapse; width: auto; margin: 1px;">
<tbody>
<tr>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-16</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Formación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/NotaInformativa16septiembre2011FCT.pdf">NOTA INFORMATIVA de 16 de septiembre de 2011</a> de la Dirección General de Formación Profesional y Educación Permanente sobre la nueva Orden reguladora de los módulos de Formación en Centros de Trabajo y de Proyecto.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-15</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc7sept2011PruebasBachillerato.pdf">RESOLUCIÓN de 7 de septiembre de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-15</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Educación de Personas Adultas</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc7sept2011PruebasBachillerato.pdf">RESOLUCIÓN de 7 de septiembre de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-14</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Inspección Educativa: Oposiciones</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc2sept2011AdmitidosDefOposInsp.pdf">RESOLUCIÓN de 2 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas definitivas de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía convocados por Orden de 26 de abril de 2011 (BOJA 14-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-14</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Organización y Funcionamiento</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc30agosto2011CelebracionLorca.pdf">RESOLUCIÓN de 30 de agosto de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por la que se dictan instrucciones para la realización en los centros docentes de la Comunidad Autónoma Andaluza de actividades orientadas a conmemorar la figura de Federico García Lorca (BOJA 14-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-13</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Centros Docentes</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden21julio2011ModificacionCentrosPrimaria.pdf">ORDEN de 21 de julio de 2011</a>, por la que se modifican escuelas infantiles de segundo ciclo, colegios de educación primaria, colegios de educación infantil y primaria y un centro específico de educación especial, así como colegios públicos rurales (BOJA 13-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Calendario y Jornada Escolar</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Nota7sept2011CalenCentrosSupArtisticos1112.pdf">NOTA INFORMATIVA de 7 de septiembre de 2011</a> de la Dirección General de Ordenación y Evaluación Educativa sobre el calendario escolar de los centros superiores de enseñanzas artísticas para el curso 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Escuela Oficial de Idiomas</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc9sept2011CALonline1112.pdf">INSTRUCCIONES de 9 de septiembre de 2011</a>, de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc9sept2011CALonline1112.pdf">INSTRUCCIONES de 9 de septiembre de 2011</a>, de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc8sept2011IdiomaChino.pdf">INSTRUCCIONES de 8 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el programa de implantación de la Enseñanza de la Lengua China en centros docentes andaluces de Educación Primaria y Secundaria.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-07</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Escuela Oficial de Idiomas</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc28julio2011ProgramaThatsEnglish1112.pdf">INSTRUCCIONES de 28 de julio de 2011</a> de la Dirección General de Formación Profesional y Educación Permanente para el funcionamiento del programa “That´S English!” en el curso 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-07</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc2sept2011OrgFuncCentrosBilingues.pdf">INSTRUCCIONES de 2 de septiembre de 2011</a> conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente del Profesorado sobre la organización y funcionamiento de la enseñanza bilingüe para el curso 2011-2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-07</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Programas de Cualificación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden1agosto2011BasesSubvencionesPCPI.pdf">ORDEN de 1 de agosto de 2011</a>, por la que se aprueban las bases reguladoras para la concesión de subvenciones en régimen de concurrencia competitiva a Corporaciones Locales, asociaciones profesionales y organizaciones no gubernamentales para el desarrollo de los módulos obligatorios de los Programas de Cualificación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 07-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-05</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Atención a la Diversidad</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc1sept2011ProcAltasCapacidades.pdf">INSTRUCCIONES de 1 de septiembre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el procedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-05</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Educación Especial</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc1sept2011ProcAltasCapacidades.pdf">INSTRUCCIONES de 1 de septiembre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el procedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-02</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Artes Plásticas y Diseño</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden18agosto2011CurriculoCiclosArtesPlasticasCeramica.pdf">ORDEN de 18 de agosto de 2011</a>, por la que se desarrolla el currículo correspondiente a los títulos de Técnico de Artes Plásticas y Diseño en Alfarería y en Decoración Cerámica y los títulos de Técnico superior de Artes Plásticas y Diseño en Cerámica Artística, en Modelismo y Matricería Cerámica y en Recubrimientos Cerámicos, pertenecientes a la familia profesional artística de la Cerámica Artística (BOJA 02-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-31</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/ModifInstruc6julio2011PremiosBachillerato.pdf">Modificación a las INSTRUCCIONES de 6 de julio de 2011</a> de la Dirección General de Ordenación y Evaluación Educativa, sobre los premios extraordinarios de bachillerato correspondientes al curso 2010/2011.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-23</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Arte Dramático</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.juntadeandalucia.es/boja/boletines/2011/165/d/7.html">DECRETO 259/2011, de 26 de julio</a>, por el que se establecen las enseñanzas artísticas superiores de Grado en Arte Dramático en Andalucía (BOJA 23-08-2011).&nbsp;<a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto259-2011ArteDramatico.pdf">Descargar disposición en pdf (20,8 MB)</a></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-23</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Música</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.juntadeandalucia.es/boja/boletines/2011/165/d/8.html">DECRETO 260/2011, de 26 de julio</a>, por el que se establecen las enseñanzas artísticas superiores de Grado en Música en Andalucía (BOJA 23-08-2011).&nbsp;<a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto260-2011Musica.pdf">Descargar disposición en pdf (51,4 MB)</a></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-22</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Danza</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.juntadeandalucia.es/boja/boletines/2011/164/d/7.html">DECRETO 258/2011, de 26 de julio</a>, por el que se establecen las enseñanzas artísticas superiores de Grado en Danza en Andalucía (BOJA 22-08-2011).&nbsp;<a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto258-2011Danza.pdf">Descargar disposición en pdf (77 MB)</a></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-19</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Acceso a la universidad</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.iesmigueldecervantes.es/normas/acuerdos/CorreccionErroresAcuerdo11marzo2011GradoUniversidad.pdf">CORRECCIÓN de errores del Acuerdo de 11 de marzo de 2011</a>, de la Dirección General de Universidades, Comisión del Distrito Único Universitario de Andalucía, por el que se establece el procedimiento para el ingreso en los estudios universitarios de Grado (BOJA 19-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-19</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Procedimiento Disciplinario / Incompatibilidades</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden2agosto2011PotestadDisciplinariaDirectores.pdf">ORDEN de 2 de agosto de 2011</a>, por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de los centros públicos de educación no universitaria (BOJA 19-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-19</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Órganos Unipersonales</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden2agosto2011PotestadDisciplinariaDirectores.pdf">ORDEN de 2 de agosto de 2011</a>, por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de los centros públicos de educación no universitaria (BOJA 19-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-16</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Inspección Educativa: Oposiciones</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/CorreccionErrataResolucion27junio2011OposicionesInspeccion.pdf">Corrección de errata de la RESOLUCIÓN de 27 de junio de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas de ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 16-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-11</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Centros Docentes</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden20julio2011AutorizacionEnsenanzas1112.pdf">ORDEN de 20 de julio de 2011</a>, por la que se modifica la autorización de enseñanzas en centros docentes públicos a partir del curso escolar 2011/12 (BOJA 11-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Cooperación con Entidades Locales</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden6julio2011SubvencionesEscuelasMusicaDanza.pdf">ORDEN de 6 de julio de 2011</a>, por la que se establecen las bases reguladoras para la concesión de subvenciones a las Escuelas de Música y Danza dependientes de entidades locales y se efectúa su convocatoria para el año 2012 (BOJA 09-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Cultura Emprendedora</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/CorreccionErroresDecreto219CulturaEmprendedora.pdf">CORRECCIÓN de errores del Decreto 219/2011</a>, de 28 de junio, por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el sistema educativo público de Andalucía (BOJA 09-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-08</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Centros Docentes</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden15julio2011PuestosCentrosInfantil.pdf">ORDEN de 15 de julio de 2011</a>, por la que se autoriza la denominación específica, así como el número de puestos escolares de primer ciclo de educación infantil, a determinadas escuelas infantiles de titularidad municipal (BOJA 08-08-2011).</td>
</tr>
<tr>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;">2011-08-03</p>
</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/CorreccionErroresOrden15junio2011PremiosBachillerato.pdf" style="text-decoration: none; color: #005fa9;">CORRECCIÓN de errores de la Orden de 15 de junio de 2011</a>, por la que se convocan los premios extraordinarios de bachillerato correspondientes al curso académico 2010/2011 (BOJA 03-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-03</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Danza</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto253ModificaEnsenanzasProfesionalesDanza.pdf" style="text-decoration: none; color: #005fa9;">DECRETO 253/2011, de 19 de julio</a>, por el que se modifica el Decreto 240/2007, de 4 de septiembre, por el que se establece la ordenación y currículo de las enseñanzas profesionales de danza en Andalucía (BOJA 03-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-03</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticas</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden18julio2011NombramientoMaestrosPracticas.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 18 de julio de 2011</a>, por la que se hacen públicas las listas del personal seleccionado en el procedimiento selectivo para el ingreso en el Cuerpo de Maestros, y se le nombra con carácter provisional funcionario en prácticas (BOJA 03-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-02</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Leyes Ordinarias</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/Leyes/Ley27-2011ActualizacionSeguridadSocial.pdf" style="text-decoration: none; color: #005fa9;">LEY 27/2011, de 1 de agosto</a>, sobre actualización, adecuación y modernización del sistema de Seguridad Social (BOE 02-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-02</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Leyes Ordinarias</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/Leyes/Ley26-2011DerechosPersonasDiscapacidad.pdf" style="text-decoration: none; color: #005fa9;">LEY 26/2011, de 1 de agosto</a>, de adaptación normativa a la Convención Internacional sobre los Derechos de las Personas con Discapacidad (BOE 02-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Formación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/OrdenMEC18julio2011AyudasEstudiosExtranjero.pdf" style="text-decoration: none; color: #005fa9;">ORDEN EDU/2127/2011, de 18 de julio</a>, por la que se establecen las bases y se convocan ayudas para el alumnado que curse estudios en niveles no universitarios en el exterior (BOE 28-07-2011). (Plazo: hasta el 26-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Formación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/OrdenMEC15julio2011PremiosNacionalesFPGradoSuperior.pdf" style="text-decoration: none; color: #005fa9;">ORDEN EDU/2128/2011, de 15 de julio</a>, por la que se crean y regulan los Premios Nacionales de Formación Profesional de grado superior establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 28-07-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-27</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><strong>Organización y Funcionamiento</strong></td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><strong><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/AclaracionesROCsecundaria27julio2011.pdf" style="text-decoration: none; color: #005fa9;">Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria</a>, aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 27 de julio de 2011).</strong></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-26</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Conciertos Educativos</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden4julio2011ConciertosEductivos.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 4 de julio de 2011</a>, por la que se resuelve la convocatoria de la Orden que se indica para el acceso al régimen de conciertos educativos o la renovación o modificación de los mismos con centros docentes privados de la Comunidad Autónoma de Andalucía, a partir del curso académico 2011/12 (BOJA 26-07-2011).</td>
</tr>
</tbody>
</table>
</div>
<br />
<div style="text-align: center;"></div>
<div></div>
<table align="center" width="98%" cellspacing="4" border="0" class="datos" style="border-collapse: collapse; width: auto; margin: 1px;">
<tbody>
<tr>
<td colspan="2" class="cabecera" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"></td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;">2011-07-23</p>
</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/rdecre/RD879-2011TDSSalvamentoySocorrismo.pdf">REAL DECRETO 879/2011, de 24 de junio</a>, por el que se establece el título de Técnico Deportivo Superior en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BPE 23-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-22</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/rdecre/RD878-2011TDSalvamentoySocorrismo.pdf">REAL DECRETO 878/2011, de 24 de junio</a>, por el que se establece el título de Técnico Deportivo en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BOE 22-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-22</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instrucciones22julio2011GratuidadLibrosTexto.pdf">INSTRUCCIONES de 22 de julio de 2011</a>, de la Dirección General de Participación e Innovación Educativa,&nbsp;<strong>sobre el programa de gratuidad de los libros de texto para el curso escolar 2010/2011.</strong></td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-21</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden5julio2011Becas6000.pdf">ORDEN de 5 de julio de 2011</a>, conjunta de las Consejerías de Educación y Empleo, por la que se establecen las&nbsp;<strong>Bases Reguladoras de la Beca 6000</strong>, dirigida a facilitar la permanencia en el Sistema Educativo del alumnado de Bachillerato y de Ciclos Formativos de Grado Medio de Formación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 21-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-21</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto227-2011SupervisionLibrosTexto.pdf">DECRETO 227/2011 de 5 de julio</a>, por el que se regula el depósito, el registro y la supervisión de los&nbsp;<strong>libros de texto</strong>, así como el<strong> procedimiento de selección </strong>de los mismos por los centros docentes públicos de Andalucía (BOJA 21-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-15</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/AclaracionesROC-Secundaria20junio2011.pdf">Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria</a>, aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la&nbsp;<strong>Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 20 de junio de 2011).</strong></td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-14</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto219_2011PlanFomentoCulturaEmprendedora.pdf">DECRETO 219/2011, de 28 de junio</a>, por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el Sistema Educativo Público de Andalucía (BOJA 14-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-12</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden29junio2011AutorizacionCentrosBilingues.pdf">ORDEN de 29 de junio de 2011</a>, por la que se establece el procedimiento para la autorización de la enseñanza bilingüe en los centros docentes de titularidad privada (BOJA 12-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-12</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden28junio2011EnsenanzaBilingue.pdf">ORDEN de 28 de junio de 2011</a>, por la que&nbsp;<strong>se regula la enseñanza bilingüe en los centros docentes </strong>de la Comunidad Autónoma de Andalucía (BOJA 12-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-12</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc27junio2011ListasProvOposicionesInspeccion.pdf">RESOLUCIÓN de 27 de junio de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 12-07-2011).</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>";s:9:"extension";s:11:"com_content";s:10:"created_by";s:1:"0";s:8:"modified";s:19:"2012-10-25 20:28:09";s:11:"modified_by";s:1:"0";s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":0:{}}s:3:"lft";s:2:"92";s:9:"parent_id";s:3:"196";s:5:"level";s:1:"2";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":0:{}}s:4:"slug";s:20:"104:ultima-normativa";s:4:"mime";N;s:6:"layout";s:8:"category";s:10:"metaauthor";N;s:4:"path";s:49:"index.php?option=com_content&view=category&id=104";s:6:"weight";d:1.3067599999999999;s:7:"link_id";i:431;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:4:"link";i:4;s:7:"metakey";i:5;s:8:"metadesc";i:6;s:10:"metaauthor";i:7;s:6:"author";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:2:{s:4:"Type";a:1:{s:8:"Category";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:8:"Category";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:49:"index.php?option=com_content&view=category&id=104";s:5:"route";s:49:"index.php?option=com_content&view=category&id=104";s:5:"title";s:17:"ÚLTIMA NORMATIVA";s:11:"description";s:22348:"ÚLTIMA NORMATIVA (11/10/2011) 2011-10-11 Atención a la Diversidad INSTRUCCIONES de 5 de octubre de 2011 de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012.style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> 2011-10-11 Organización y Funcionamiento INSTRUCCIONES de 5 de octubre de 2011 de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012. 2011-10-06 Educación Compensatoria CIRCULAR de 29 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar.#9a9542;"> 2011-10-06 Programa de Acompañamiento CIRCULAR de 29 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar. 2011-10-05 Educación Compensatoria INSTRUCCIONES de 30 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012. 2011-10-05arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> Funcionarios Docentes: Adscripción / Integración / Especialidades ORDEN EDU/2645/2011, de 23 de septiembre , por la que se establece la formación equivalente a la formación pedagógica y didáctica exigida para aquellas personas que estando en posesión de una titulación declarada equivalente a efectos de docencia no pueden realizar los estudios de máster (BOE 05-10-2011). 2011-10-05 Programa de Acompañamiento INSTRUCCIONES de 30 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012.1px solid #9a9542;"> 2011-09-30 Acceso a la universidad Anexo I de la Resolución de 29 de abril de 2010 , de la Secretaría de Estado de Educación y Formación Profesional, por la que se establecen las instrucciones para el cálculo de la nota media que debe figurar en las credenciales de convalidación y homologación de estudios y títulos extranjeros con el bachiller españolen el apartado Estudios y Títulos no Universitarios (actualización de 12-07-2011). 2011-09-29 Educación de Personas Adultas INSTRUCCIONES de 23 de septiembre de 2011 , de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de2011. 2011-09-29 Funcionarios Docentes: Asistencia Jurídica / Responsabilidad / Abstención RESOLUCIÓN de 16 de septiembre de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se dispone la publicación del Pacto de Mesa Sectorial en materia de prestación de asistencia jurídica gratuita al personal docente no universitario y al personal de Administración y Servicios de los centros docentes públicos y de los servicios educativos dependientes de la Consejería competente en materia de educación (BOJA 29-09-2011). 2011-09-29 Plan de Plurilingüismo ORDEN de 22 de septiembre de 2011 , por la que se establecen las modalidades de provisión y las bases reguladoras para laconcesión de subvenciones a auxiliares de conversación, y se efectúa convocatoria para el curso 2011/12 (BOJA 29-09-2011). 2011-09-29 Programa de Calidad y Mejora de los Rendimientos Escolares ORDEN de 26 de septiembre de 2011 , por la que se regula el Programa de calidad y mejora de los rendimientos escolares en los centros docentes públicos (BOJA 29-09-2011) 2011-09-28 Educación Compensatoria INSTRUCCIONES de 15 de septiembre de 2011 , de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapade Educación Secundaria Obligatoria. 2011-09-28 Órganos Unipersonales INSTRUCCION de 26 de mayo de 2011 de la Dirección General de Profesorado y Gestión de Recursos Humanos por la que se oferta la formación inicial para la dirección de centros docentes a otros directores y directoras que lo soliciten. 2011-09-28 Programa de Acompañamiento INSTRUCCIONES de 15 de septiembre de 2011 , de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapa de Educación SecundariaObligatoria. 2011-09-26 Deporte Escolar INSTRUCCIONES de 6 de septiembre de 2011 de la Dirección General de Participación e Innovación Educativa sobre el programa Escuelas Deportivas para el curso escolar 2011/2012. 2011-09-24 Jornada Personal Docente INSTRUCCIONES de 16 de septiembre de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial. 2011-09-24text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> Organización y Funcionamiento INSTRUCCIONES de 16 de septiembre de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial. 2011-09-21 Arte Dramático INSTRUCCIONES de 20 de Septiembre de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012. 2011-09-21 Danzafont: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> INSTRUCCIONES de 20 de Septiembre de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012. 2011-09-21 Música INSTRUCCIONES de 20 de Septiembre de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012. 2011-09-16 Bachilleratohref="http://www.adideandalucia.es/normas/instruc/Instruc12sept2011BachillerBaccalaureat.pdf" style="text-decoration: none; color: #005fa9;"> INSTRUCCIONES de 12 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el programa de doble titulación BACHILLER-BACCALAURÉAT en centros docentes de la Comunidad Autónoma de Andalucía. 2011-09-16 Formación Profesional Inicial NOTA INFORMATIVA de 16 de septiembre de 2011 de la Dirección General de Formación Profesional y Educación Permanente sobre la nueva Orden reguladora de los módulos de Formación en Centros de Trabajo y de Proyecto. 2011-09-15 Bachillerato RESOLUCIÓN de 7 de septiembre de 2011 , de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011).padding: 2px; border: 1px solid #9a9542;"> 2011-09-15 Educación de Personas Adultas RESOLUCIÓN de 7 de septiembre de 2011 , de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011). 2011-09-14 Inspección Educativa: Oposiciones RESOLUCIÓN de 2 de septiembre de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas definitivas de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía convocados por Orden de 26 de abril de 2011 (BOJA 14-09-2011). 2011-09-14 Organización y Funcionamiento RESOLUCIÓN de 30 deagosto de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, por la que se dictan instrucciones para la realización en los centros docentes de la Comunidad Autónoma Andaluza de actividades orientadas a conmemorar la figura de Federico García Lorca (BOJA 14-09-2011). 2011-09-13 Centros Docentes ORDEN de 21 de julio de 2011 , por la que se modifican escuelas infantiles de segundo ciclo, colegios de educación primaria, colegios de educación infantil y primaria y un centro específico de educación especial, así como colegios públicos rurales (BOJA 13-09-2011). 2011-09-09 Calendario y Jornada Escolar NOTA INFORMATIVA de 7 de septiembre de 2011 de la Dirección General de Ordenación y Evaluación Educativa sobre el calendario escolar de los centros superiores de enseñanzas artísticas para el curso 2011/2012. 2011-09-09 Escuela Oficial de Idiomaspadding: 2px; border: 1px solid #9a9542;"> INSTRUCCIONES de 9 de septiembre de 2011 , de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento. 2011-09-09 Plan de Plurilingüismo INSTRUCCIONES de 9 de septiembre de 2011 , de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento. 2011-09-09 Plan de Plurilingüismohref="http://www.adideandalucia.es/normas/instruc/Instruc8sept2011IdiomaChino.pdf"> INSTRUCCIONES de 8 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el programa de implantación de la Enseñanza de la Lengua China en centros docentes andaluces de Educación Primaria y Secundaria. 2011-09-07 Escuela Oficial de Idiomas INSTRUCCIONES de 28 de julio de 2011 de la Dirección General de Formación Profesional y Educación Permanente para el funcionamiento del programa “That´S English!” en el curso 2011/2012. 2011-09-07 Plan de Plurilingüismo INSTRUCCIONES de 2 de septiembre de 2011 conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente del Profesorado sobre la organización y funcionamiento de la enseñanza bilingüe para el curso 2011-2012. 2011-09-07 Programas de CualificaciónProfesional Inicial ORDEN de 1 de agosto de 2011 , por la que se aprueban las bases reguladoras para la concesión de subvenciones en régimen de concurrencia competitiva a Corporaciones Locales, asociaciones profesionales y organizaciones no gubernamentales para el desarrollo de los módulos obligatorios de los Programas de Cualificación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 07-08-2011). 2011-09-05 Atención a la Diversidad INSTRUCCIONES de 1 de septiembre de 2011 de la Dirección General de Participación e Innovación Educativa por las que se regula el procedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales. 2011-09-05 Educación Especial INSTRUCCIONES de 1 de septiembre de 2011 de la Dirección General de Participación e Innovación Educativa por las que se regula elprocedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales. 2011-09-02 Artes Plásticas y Diseño ORDEN de 18 de agosto de 2011 , por la que se desarrolla el currículo correspondiente a los títulos de Técnico de Artes Plásticas y Diseño en Alfarería y en Decoración Cerámica y los títulos de Técnico superior de Artes Plásticas y Diseño en Cerámica Artística, en Modelismo y Matricería Cerámica y en Recubrimientos Cerámicos, pertenecientes a la familia profesional artística de la Cerámica Artística (BOJA 02-09-2011). 2011-08-31 Bachillerato Modificación a las INSTRUCCIONES de 6 de julio de 2011 de la Dirección General de Ordenación y Evaluación Educativa, sobre los premios extraordinarios de bachillerato correspondientes al curso 2010/2011. 2011-08-23 Arte Dramáticostyle="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"> DECRETO 259/2011, de 26 de julio , por el que se establecen las enseñanzas artísticas superiores de Grado en Arte Dramático en Andalucía (BOJA 23-08-2011). Descargar disposición en pdf (20,8 MB) 2011-08-23 Música DECRETO 260/2011, de 26 de julio , por el que se establecen las enseñanzas artísticas superiores de Grado en Música en Andalucía (BOJA 23-08-2011). Descargar disposición en pdf (51,4 MB) 2011-08-22 Danza DECRETO 258/2011, de 26 de julio , por el que se establecen las enseñanzas artísticas superiores de Grado en Danza en Andalucía (BOJA 22-08-2011). Descargar disposición en pdf (77 MB)1px solid #9a9542;"> 2011-08-19 Acceso a la universidad CORRECCIÓN de errores del Acuerdo de 11 de marzo de 2011 , de la Dirección General de Universidades, Comisión del Distrito Único Universitario de Andalucía, por el que se establece el procedimiento para el ingreso en los estudios universitarios de Grado (BOJA 19-08-2011). 2011-08-19 Funcionarios Docentes: Procedimiento Disciplinario / Incompatibilidades ORDEN de 2 de agosto de 2011 , por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de los centros públicos de educación no universitaria (BOJA 19-08-2011). 2011-08-19 Órganos Unipersonales ORDEN de 2 de agosto de 2011 , por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de loscentros públicos de educación no universitaria (BOJA 19-08-2011). 2011-08-16 Inspección Educativa: Oposiciones Corrección de errata de la RESOLUCIÓN de 27 de junio de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas de ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 16-08-2011). 2011-08-11 Centros Docentes ORDEN de 20 de julio de 2011 , por la que se modifica la autorización de enseñanzas en centros docentes públicos a partir del curso escolar 2011/12 (BOJA 11-08-2011). 2011-08-09 Cooperación con Entidades Localeshref="http://www.adideandalucia.es/normas/ordenes/Orden6julio2011SubvencionesEscuelasMusicaDanza.pdf"> ORDEN de 6 de julio de 2011 , por la que se establecen las bases reguladoras para la concesión de subvenciones a las Escuelas de Música y Danza dependientes de entidades locales y se efectúa su convocatoria para el año 2012 (BOJA 09-08-2011). 2011-08-09 Cultura Emprendedora CORRECCIÓN de errores del Decreto 219/2011 , de 28 de junio, por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el sistema educativo público de Andalucía (BOJA 09-08-2011). 2011-08-08 Centros Docentes ORDEN de 15 de julio de 2011 , por la que se autoriza la denominación específica, así como el número de puestos escolares de primer ciclo de educación infantil, a determinadas escuelas infantiles de titularidad municipal (BOJA 08-08-2011). 2011-08-03normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> Bachillerato CORRECCIÓN de errores de la Orden de 15 de junio de 2011 , por la que se convocan los premios extraordinarios de bachillerato correspondientes al curso académico 2010/2011 (BOJA 03-08-2011). 2011-08-03 Danza DECRETO 253/2011, de 19 de julio , por el que se modifica el Decreto 240/2007, de 4 de septiembre, por el que se establece la ordenación y currículo de las enseñanzas profesionales de danza en Andalucía (BOJA 03-08-2011). 2011-08-03 Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticasnormal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> ORDEN de 18 de julio de 2011 , por la que se hacen públicas las listas del personal seleccionado en el procedimiento selectivo para el ingreso en el Cuerpo de Maestros, y se le nombra con carácter provisional funcionario en prácticas (BOJA 03-08-2011). 2011-08-02 Leyes Ordinarias LEY 27/2011, de 1 de agosto , sobre actualización, adecuación y modernización del sistema de Seguridad Social (BOE 02-08-2011). 2011-08-02 Leyes Ordinarias LEY 26/2011, de1 de agosto , de adaptación normativa a la Convención Internacional sobre los Derechos de las Personas con Discapacidad (BOE 02-08-2011). 2011-07-28 Formación Profesional Inicial ORDEN EDU/2127/2011, de 18 de julio , por la que se establecen las bases y se convocan ayudas para el alumnado que curse estudios en niveles no universitarios en el exterior (BOE 28-07-2011). (Plazo: hasta el 26-09-2011). 2011-07-28 Formación Profesional Inicial ORDEN EDU/2128/2011, de 15 de julio , por la que se crean y regulan los Premios Nacionales de Formación Profesional de grado superior establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 28-07-2011).width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> 2011-07-27 Organización y Funcionamiento Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria , aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 27 de julio de 2011). 2011-07-26 Conciertos Educativos ORDEN de 4 de julio de 2011 , por la que se resuelve la convocatoria de la Orden que se indica para el acceso al régimen de conciertos educativos o la renovación o modificación de los mismos con centros docentes privados de la ComunidadAutónoma de Andalucía, a partir del curso académico 2011/12 (BOJA 26-07-2011). 2011-07-23 REAL DECRETO 879/2011, de 24 de junio , por el que se establece el título de Técnico Deportivo Superior en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BPE 23-07-2011). 2011-07-22 REAL DECRETO 878/2011, de 24 de junio , por el que se establece el título de Técnico Deportivo en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BOE 22-07-2011). 2011-07-22 INSTRUCCIONES de22 de julio de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el programa de gratuidad de los libros de texto para el curso escolar 2010/2011. 2011-07-21 ORDEN de 5 de julio de 2011 , conjunta de las Consejerías de Educación y Empleo, por la que se establecen las Bases Reguladoras de la Beca 6000 , dirigida a facilitar la permanencia en el Sistema Educativo del alumnado de Bachillerato y de Ciclos Formativos de Grado Medio de Formación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 21-07-2011). 2011-07-21 DECRETO 227/2011 de 5 de julio , por el que se regula el depósito, el registro y la supervisión de los libros de texto , así como el procedimiento de selección de los mismos por los centros docentes públicos de Andalucía (BOJA 21-07-2011). 2011-07-15 Aclaraciones en torno al Reglamento Orgánico delos institutos de Educación Secundaria , aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 20 de junio de 2011). 2011-07-14 DECRETO 219/2011, de 28 de junio , por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el Sistema Educativo Público de Andalucía (BOJA 14-07-2011). 2011-07-12 ORDEN de 29 de junio de 2011 , por la que se establece el procedimiento para la autorización de la enseñanza bilingüe en los centros docentes de titularidad privada (BOJA 12-07-2011). 2011-07-12 ORDEN de 28 de junio de 2011 , por la que se regula la enseñanza bilingüe en los centros docentes de la Comunidad Autónoma de Andalucía (BOJA12-07-2011). 2011-07-12 RESOLUCIÓN de 27 de junio de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 12-07-2011).";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"0000-00-00 00:00:00";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"0000-00-00 00:00:00";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:1;}i:1;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:21:{s:2:"id";s:3:"196";s:5:"alias";s:9:"normativa";s:7:"summary";s:65543:"<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td width="100%" valign="top">
<div id="contenido" class="contenido" style="padding-top: 10px; padding-right: 20px; padding-bottom: 10px; padding-left: 20px;">
<div id="tituloItem" class="blockTituloItem" style="display: block; margin-top: 15px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; border-bottom-width: 2px; border-bottom-style: dotted; border-bottom-color: #aaaaaa; float: left; width: 98%; padding: 0px;">
<div class="tituloIzq" style="float: left; text-align: left;">
<p><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; letter-spacing: 1pt; color: #71cd63;">ÚLTIMA NORMATIVA</span></p>
<table style="width: 100%px; padding: 0px;" id="tableBody" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333; background-color: #ffffff; padding-left: 10px; padding-right: 5px; vertical-align: top; width: 1054px;" id="columnCenter">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td style="font: normal normal normal 14px/normal georgia; color: #cc3300; font-weight: bold; letter-spacing: 0.1em; line-height: 26px; padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;" class="pageName">Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333; line-height: 24px;" class="bodyText">
<table cellspacing="3" border="1" width="98%">
<tbody>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" bgcolor="#9AB2CE" colspan="3"><span class="Estilo2">Últimas 30 disposiciones añadidas</span></td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" align="center" bgcolor="#9AB2CE"><span class="Estilo2">F. GRAB</span></td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" align="center" bgcolor="#9AB2CE"><span class="Estilo2">CATEGORÍA</span></td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" align="center" bgcolor="#9AB2CE"><span class="Estilo2">DISPOSICIÓN</span></td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-05-10</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Centros Docentes</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto140ModificaDecretosAutorizacionesCentros.pdf">DECRETO 140/2011, de 26 de abril</a>, por el que se modifican varios decretos relativos a la autorización de centros docentes para su adaptación a la Ley 17/2009, de 23 de noviembre, sobre el libre acceso a las actividades de servicios y su ejercicio (BOJA 10-05-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-05-09</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Inspección Educativa: Oposiciones</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden26abril2011ConvOposicionesInspeccion.pdf">ORDEN de 26 de abril de 2011</a>, ppor la que se realiza convocatoria pública para el acceso al Cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía (BOJA 09-05-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-05-04</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Funcionarios Docentes: Provisión de Puestos / Colocación Efectivos / Concursos</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden11abril2011RecolocacionDocentes.pdf">ORDEN de 11 de abril de 2011</a>, por la que se regulan los procedimientos de recolocación y de redistribución del personal funcionario de carrera de los cuerpos docentes contemplados en la disposición adicional séptima de la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOJA 04-05-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-05-03</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Convivencia Escolar</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden11abril2011RedEscuelaPaz.pdf">ORDEN de 11 de abril de 2011</a>, por la que se regula la participación de los centros docentes en la Red Andaluza «Escuela: Espacio de Paz» y el procedimiento para solicitar reconocimiento como Centros Promotores de Convivencia Positiva (Convivencia+) (BOJA 03-05-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-30</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Plan Apoyo Familias</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc6abril2011ComedoresActividadesExtraescolares.pdf">RESOLUCIÓN de 6 de abril de 2011</a>, de la Dirección General de Planificación y Centros, por la que se actualizan los centros docentes públicos que tienen autorizados los servicios de aula matinal, comedor escolar y actividades extraescolares (BOJA 30-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-29</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Arte Dramático</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc29abril2011AccesoEnsenanzasArtisticasSuperiores.pdf">INSTRUCCIONES de 29 de abril de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre admisión del alumnado y pruebas de acceso a las Enseñanzas Artísticas Superiores para el curso académico 2011/2012.&nbsp;<a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/AnexosInstruc29abril2011.pdf">Anexos I al VIII</a></td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-29</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Danza</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc29abril2011AccesoEnsenanzasArtisticasSuperiores.pdf">INSTRUCCIONES de 29 de abril de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre admisión del alumnado y pruebas de acceso a las Enseñanzas Artísticas Superiores para el curso académico 2011/2012.&nbsp;<a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/AnexosInstruc29abril2011.pdf">Anexos I al VIII</a></td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-29</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Escolarización y Matriculación</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc29abril2011AccesoEnsenanzasArtisticasSuperiores.pdf">INSTRUCCIONES de 29 de abril de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre admisión del alumnado y pruebas de acceso a las Enseñanzas Artísticas Superiores para el curso académico 2011/2012.&nbsp;<a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/AnexosInstruc29abril2011.pdf">Anexos I al VIII</a></td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-29</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Música</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc29abril2011AccesoEnsenanzasArtisticasSuperiores.pdf">INSTRUCCIONES de 29 de abril de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre admisión del alumnado y pruebas de acceso a las Enseñanzas Artísticas Superiores para el curso académico 2011/2012.&nbsp;<a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/AnexosInstruc29abril2011.pdf">Anexos I al VIII</a></td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-26</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Centros Docentes</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden22marzo2011AutorizacionPuestosInfantil.pdf">ORDEN de 22 de marzo de 2011</a>, por la que se autoriza la denominación específica, así como el número de puestos escolares de primer ciclo de educación infantil, a determinadas escuelas infantiles de titularidad municipal (BOJA 26-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-26</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Centros Docentes</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden21feb2011AutorizacionPuestosInfantil.pdf">ORDEN de 21 de febrero de 2011</a>, por la que se autoriza la denominación específica, así como el número de puestos escolares de primer ciclo de educación infantil, a determinadas escuelas infantiles de titularidad municipal (BOJA 26-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-20</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticas</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/CorreccionErroresOrden14marzo2011OposicionesMaestros.pdf">CORRECCIÓN de errores de la Orden de 14 de marzo de 2011</a>, por la que se efectúa convocatoria de procedimiento selectivo para el ingreso en el Cuerpo de Maestros (BOJA 20-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-14</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Educación Infantil</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc31marzo2001AspectosPrimerCicloInfantil.pdf">INSTRUCCIONES de 31 de marzo de 2011</a> de la Dirección General de Planificación y Centros sobre determinados aspectos del primer ciclo de la educación infantil.</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-14</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Formación Profesional Inicial</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc5abril2011OrganizacionPruebasAccesoFormacionProfesional.pdf">INSTRUCCIONES de 5 de abril de 2011</a> de la Dirección General de Formación Profesional y Educación Permanente sobre organización y realización de las pruebas de acceso a los ciclos formativos de formación profesional en las convocatorias de junio y septiembre de 2011.</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-13</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Organización y Funcionamiento</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc1abril2011AGAEVEindicadoresAutoevaluacion.pdf">RESOLUCIÓN de 1 de abril de 2011</a>, de la Dirección General de la Agencia Andaluza de Evaluación Educativa, por la que se establecen los indicadores homologados para la autoevaluación de los centros docentes públicos (BOJA 13-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-12</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Educación Especial</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc10marzo2011DictamenesAltasCapacidades.pdf">INSTRUCCIONES de 10 de marzo de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se concretan determinados aspectos sobre los dictámenes para el alumnado con necesidades específicas de apoyo educativo.</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-12</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Equipos de Orientación Educativa</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc10marzo2011DictamenesAltasCapacidades.pdf">INSTRUCCIONES de 10 de marzo de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se concretan determinados aspectos sobre los dictámenes para el alumnado con necesidades específicas de apoyo educativo.</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-12</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Escolarización y Matriculación</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/CorreccionErroresOrden24febrero2011Escolarizacion.pdf">CORRECCIÓN de errores de la Orden de 24 de febrero de 2011</a>, por la que se desarrolla el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato (BOJA 12-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-12</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Pruebas de Diagnóstico</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc7abril2011AGAEVEpruebasevaluacion.pdf">INSTRUCCIONES de 7 de abril de 2011</a> de la Agencia Andaluza de evaluación Educativa (AGAEVE), por la que se concretan determinados aspectos relacionados con la realización de las Pruebas de Evaluación de Diagnóstico para el curso 2010-2011.</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-08</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Educación Primaria</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden17marzo2011modificaOrdenesEvaluacion.pdf">ORDEN de 17 de marzo de 2011</a>, por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-08</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Escolarización y Matriculación</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc22marzo2011Escolarizacion.pdf">INSTRUCCIONES de la Viceconsejería de Educación de 22 de marzo de 2011</a> sobre planificación de la escolarización para el curso académico 2011/2012 en los centros docentes públicos y privados concertados.</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-04</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Bachillerato</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden17marzo2011modificaOrdenesEvaluacion.pdf">ORDEN de 17 de marzo de 2011</a>, por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-04</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Educación Infantil</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden17marzo2011modificaOrdenesEvaluacion.pdf">ORDEN de 17 de marzo de 2011</a>, por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-04</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Educación Secundaria</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden17marzo2011modificaOrdenesEvaluacion.pdf">ORDEN de 17 de marzo de 2011</a>, por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-04</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Evaluación de Enseñanzas</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden17marzo2011modificaOrdenesEvaluacion.pdf">ORDEN de 17 de marzo de 2011</a>, por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-04-01</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Consejería de Educación</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden22febrero2011DelegacionCompetencias.pdf">ORDEN de 22 de febrero de 2011</a>, por la que se modifica la Orden de 22 de septiembre de 2003, por la que se delegan competencias en diversos órganos de la Consejería (BOJA 01-04-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-03-31</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Educación de Personas Adultas</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/resoluc/CorreccionErratasResoluc9marzo2011PruebasGESO.pdf">CORRECCIÓN de erratas de la Resolución de 9 de marzo de 2011</a>, del la Dirección General de Formación Profesional y Educación Permanente, por la que se fijan los días y lugares de celebración, el desarrollo, la estructura y calificación de las pruebas para la obtención del título de Graduado en Educación Secundaria Obligatoria para personas mayores de dieciocho años y se nombran los Tribunales correspondientes (BOJA 31-03-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-03-31</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Escolarización y Matriculación</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/decretos/CorreccionErroresDecreto40Escolarizacion.pdf">CORRECCIÓN de errores al Decreto 40/2011</a>, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato (BOJA 31-03-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-03-24</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Educación de Personas Adultas</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc9marzo2011PruebasLibresGraduadoESO.pdf">RESOLUCIÓN de 9 de marzo de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se fijan los días y lugares de celebración, el desarrollo, la estructura y calificación de las pruebas para la obtención del título de Graduado en Educación Secundaria Obligatoria para personas mayores de dieciocho años y se nombran los Tribunales correspondientes. (BOJA 24-03-2011).</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" width="70">2011-03-24</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">Plan de Autoprotección</td>
<td style="font: normal normal normal 11px/normal arial; color: #333333;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc16marzo2011RofyPlanAutoproteccion.pdf">INSTRUCCIONES de 16 de marzo de 2011</a> de la Dirección General de Profesorado y Gestión de Recursos Humanos relativas a los aspectos relacionados con el plan de autoprotección y la prevención de riesgos laborales que deben incluir los reglamentos de organización y funcionamiento de los centros.</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" bgcolor="#FFFFFF"></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<p><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; letter-spacing: 1pt; color: #71cd63;"><br /></span></p>
</div>
</div>
<div id="contenido_sitio" style="width: 98%; float: left; display: block; text-align: left;">
<table border="1" cellspacing="3" cellpadding="0" width="517" style="text-align: left;">
<tbody>
<tr bgcolor="#ffefd6">
<td style="text-align: left;"><span style="font-size: 12pt;"><span style="font-family: &#39;comic sans ms&#39;, sans-serif;">Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía.</span></span></td>
</tr>
<tr>
<td style="text-align: left;">
<table border="1" cellspacing="3" width="515" style="width: 98%px; height: 2030px;">
<tbody>
<tr>
<td colspan="3" bgcolor="#9ab2ce"><span class="Estilo2">Últimas disposiciones añadidas (desde el 5 de septiembre de 2009 hasta el 6 de octubre de 2010)<br /></span></td>
</tr>
<tr>
<td align="center" bgcolor="#9ab2ce"><span class="Estilo2">F. GRAB</span></td>
<td align="center" bgcolor="#9ab2ce"><span class="Estilo2">CATEGORÍA</span></td>
<td align="center" bgcolor="#9ab2ce"><span class="Estilo2">DISPOSICIÓN</span></td>
</tr>
<tr>
<td width="70">2010-10-12</td>
<td>Sociedad del Conocimiento</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/resoluc/Resolucion5oct2010concesionselloescuela2_0.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">RESOLUCIÓN de 5 de octubre de 2010</a>, de la Secretaría de Estado de Educación y Formación Profesional, por la que se establece el procedimiento para la concesión del distintivo de calidad SELLO ESCUELA 2.0 (BOE 12-10-2010).</td>
</tr>
<tr>
<td width="70">2010-10-11</td>
<td>Equipos de Orientación Educativa</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/instruc/Instruc20sept2010atenciontemprana.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 20 de septiembre de 2010</a> de la Dirección General de Participación e Innovación Educaiva por la que se regulan determinados aspectos sobre la organización y el funcionamiento de las y los profesionales especialistas en atención temprana en la estructura de los Equipos de Orientación Educativa Especializados.</td>
</tr>
<tr>
<td width="70">2010-10-11</td>
<td>Equipos de Orientación Educativa</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/instruc/Instruc17sept2010educadorsocial.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 17 de septiembre de 2010</a> de la Dirección General de Participación e Innovación Educativa por la que se regula la intervención del educador y educadora social en el ámbito educativo.</td>
</tr>
<tr>
<td width="70">2010-10-09</td>
<td>Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticas</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/resoluc/ResolucionFasePracticasSecundaria07_10_2010.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">RESOLUCIÓN de 7 de octubre de 2010</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se regula la fase de prácticas del personal seleccionado en el procedimiento selectivo para el ingreso en los Cuerpos de Profesores de Enseñanza Secundaria, Profesores Técnicos de Formación Profesional, Profesores de Escuelas Oficiales de Idiomas, Profesores de Música y Artes Escénicas, Profesores de Artes Plásticas y Diseño y Maestros de Taller de Artes Plásticas y Diseño (próxima publicación en BOJA).</td>
</tr>
<tr>
<td width="70">2010-10-07</td>
<td>Bachillerato</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/instruc/Instruc4octubre2010pruebastitulobachillermayores20annos.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 4 de octubre de 2010</a>, de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de 2010.</td>
</tr>
<tr>
<td width="70">2010-10-06</td>
<td>Programa de Calidad y Mejora de los Rendimientos Escolares</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/instruc/Instruc6octubre2010programacalidad.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INTRUCCIONES de 6 de octubre de 2010</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre la participación de los Centros en el Programa de Calidad y Mejora de los rendimientos escolares.</td>
</tr>
<tr>
<td width="70">2010-10-05</td>
<td>Arte Dramático</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/instruc/Instruc30sept2010primercursoense%C3%B1anzasartisticasgrado.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INTRUCCIONES de 30 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado.</td>
</tr>
<tr>
<td width="70">2010-10-05</td>
<td>Danza</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/instruc/Instruc30sept2010primercursoense%C3%B1anzasartisticasgrado.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INTRUCCIONES de 30 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado.</td>
</tr>
<tr>
<td width="70">2010-10-05</td>
<td>Música</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/instruc/Instruc30sept2010primercursoense%C3%B1anzasartisticasgrado.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INTRUCCIONES de 30 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado.</td>
</tr>
<tr>
<td width="70">2010-10-01</td>
<td>Bachillerato</td>
<td><a href="../../centros-tic/18700441/helvia/sitio/normas/resoluc/Resolucion16sept2010convocatoriapruebaslibresBach.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">RESOLUCIÓN de 16 de septiembre de 2010</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 01-10-2010).</td>
</tr>
<tr>
<td width="70">2010-10-06</td>
<td>Programa de Calidad y Mejora de los Rendimientos Escolares</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc6octubre2010programacalidad.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INTRUCCIONES de 6 de octubre de 2010</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre la participación de los Centros en el Programa de Calidad y Mejora de los rendimientos escolares.</td>
</tr>
<tr>
<td width="70">2010-10-05</td>
<td>Arte Dramático</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc30sept2010primercursoense%C3%B1anzasartisticasgrado.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INTRUCCIONES de 30 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado.</td>
</tr>
<tr>
<td width="70">2010-10-05</td>
<td>Danza</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc30sept2010primercursoense%C3%B1anzasartisticasgrado.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INTRUCCIONES de 30 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado.</td>
</tr>
<tr>
<td width="70">2010-10-05</td>
<td>Música</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc30sept2010primercursoense%C3%B1anzasartisticasgrado.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INTRUCCIONES de 30 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado.</td>
</tr>
<tr>
<td width="70">2010-10-01</td>
<td>Bachillerato</td>
<td><a href="http://www.adideandalucia.es/normas/resoluc/Resolucion16sept2010convocatoriapruebaslibresBach.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">RESOLUCIÓN de 16 de septiembre de 2010</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 01-10-2010).</td>
</tr>
<tr>
<td width="70">2010-10-01</td>
<td>Bachillerato</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc22sept2010matriculacionalumnadoBachilleratoconsuspensos.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 22 de septiembre</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre las condiciones de matriculación del alumnado de bachillerato con evaluación negativa en algunas materias, para el curso académico 2010/2011.</td>
</tr>
<tr>
<td width="70">2010-10-01</td>
<td>Educación de Personas Adultas</td>
<td><a href="http://www.adideandalucia.es/normas/resoluc/Resolucion16sept2010convocatoriapruebaslibresBach.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">RESOLUCIÓN de 16 de septiembre de 2010</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 01-10-2010).</td>
</tr>
<tr>
<td width="70">2010-10-01</td>
<td>Plan de Plurilingüismo</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc21sept2010cursosinglesonline.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 21 de septiembre de 2010</a>, de las Direcciones Generales de Planificación y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2010/11, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento.</td>
</tr>
<tr>
<td width="70">2010-09-27</td>
<td>Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticas</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden10sept2010nombramientofuncionariosenpracticas.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 10 de septiembre de 2010</a>, por la que se publica la relación del personal seleccionado en los procedimientos selectivos para el ingreso en los Cuerpos de Profesores de Enseñanza Secundaria, Profesores Técnicos de Formación Profesional, Profesores de Escuelas Oficiales de Idiomas, Profesores de Música y Artes Escénicas, Profesores de Artes Plásticas y Diseño y Maestros de Taller de Artes Plásticas y Diseño y acceso al Cuerpo de Profesores de Enseñanza Secundaria y Profesores de Artes Plásticas y Diseño y se le nombra con carácter provisional funcionario en prácticas (BOJA 27-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-27</td>
<td>Jornada Personal Docente</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc6-2010horariopersonalsindical.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIÓN Nº 6/2010</a>, de 20 de septiembre, de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal con liberación sindical parcial.</td>
</tr>
<tr>
<td width="70">2010-09-27</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/AclaracionesROCprimaria27sept2010.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Aclaraciones en torno al Reglamento Orgánico de las Escuelas Infantiles de Segundo Ciclo</a>, de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centros públicos Específicos de Educación Especial, aprobado por el DECRETO 328/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de las Escuelas Infantiles de Segundo Ciclo, de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centros públicos Específicos de Educación Especial, así como el horario de los centros, del alumnado y del profesorado (actualización de 27 de septiembre de 2010).</td>
</tr>
<tr>
<td width="70">2010-09-27</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/AclaracionesROCsecundaria27sept2010.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria</a>, aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 27 de septiembre de 2010).</td>
</tr>
<tr>
<td width="70">2010-09-27</td>
<td>Plan para el Fomento de la Cultura Emprendedora</td>
<td><a href="http://www.adideandalucia.es/normas/acuerdos/Acuerdo14sept2010planculturaemprendedora.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ACUERDO de 14 de septiembre de 2010</a>, del Consejo de Gobierno, por el que se aprueba la formulación del Plan para el Fomento de la Cultura Emprendedora en el sistema educativo público de Andalucía (BOJA 27-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-24</td>
<td>Bachillerato</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2010premiosextraordinariosbachillerato2009-10.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 15 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa relativas a los Premios Extraordinarios de Bachillerato correspondientes al curso 2009/2010.</td>
</tr>
<tr>
<td width="70">2010-09-23</td>
<td>Bachillerato</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden8sept2010premiosextraordinariosbachillerato2009-10.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 8 de septiembre de 2010</a>, por la que se convocan los Premios Extraordinarios de Bachillerato correspondientes al curso 2009-2010 (BOJA 23-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-22</td>
<td>Equipos de Orientación Educativa</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc10sept2010personalatenciontempranaEOEE.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 10 de septiembre de 2010</a> de la Dirección General de Participación e Innovación Educativa por la que se regulan determinados aspectos sobre la organización y el funcionamiento de las y los profesionales especialistas en atención temprana en la estructura de los Equipos de Orientación Educativa Especializados.</td>
</tr>
<tr>
<td width="70">2010-09-22</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc22sept2010bibliotecasescolares2010-11.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 22 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa sobre la organización y funcionamiento, durante el curso 2010/11, de las bibliotecas escolares de los centros docentes públicos que imparten educación primaria o educación secundaria obligatoria.</td>
</tr>
<tr>
<td width="70">2010-09-22</td>
<td>Plan de Lectura y Bibliotecas</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc22sept2010bibliotecasescolares2010-11.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 22 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa sobre la organización y funcionamiento, durante el curso 2010/11, de las bibliotecas escolares de los centros docentes públicos que imparten educación primaria o educación secundaria obligatoria.</td>
</tr>
<tr>
<td width="70">2010-09-17</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden8sept2010procedimientosustituciones.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 8 de septiembre de 2010</a>, por la que se establece el procedimiento para la gestión de las sustituciones del profesorado de los centros docentes públicos dependientes de esta Consejería (BOJA 17-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-16</td>
<td>Deporte Escolar</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2010escuelasdeportiva2010-11.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 15-09-2010</a> de la Dirección General de Participación e Innovación Educativa sobre el programa "Escuelas Deportivas" para el curso escolar 2010/2011.</td>
</tr>
<tr>
<td width="70">2010-09-16</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden3sept2010reduccionescoordinadoresplanes.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 3-09-2010</a>, por la que se establece el horario de dedicación del profesorado responsable de la coordinación de los planes y programas estratégicos que desarrolla la Consejería competente en materia de educación (BOJA 16-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-16</td>
<td>Plan Apoyo Familias</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden3sept2010reduccionescoordinadoresplanes.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 3-09-2010</a>, por la que se establece el horario de dedicación del profesorado responsable de la coordinación de los planes y programas estratégicos que desarrolla la Consejería competente en materia de educación (BOJA 16-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-16</td>
<td>Plan de Plurilingüismo</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden3sept2010reduccionescoordinadoresplanes.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 3-09-201</a>0, por la que se establece el horario de dedicación del profesorado responsable de la coordinación de los planes y programas estratégicos que desarrolla la Consejería competente en materia de educación (BOJA 16-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-16</td>
<td>Programas de Cualificación Profesional Inicial</td>
<td><a href="http://www.adideandalucia.es/normas/resoluc/Resoluc22julio2010pcpiOperarioReprografia.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">RESOLUCIÓN de 22-07-201</a>0, de la Dirección General de Formación Profesional y Educación Permanente, por la que se establece el perfil profesional de Operario de Reprografía y el currículo de los módulos específicos del Programa de Cualificación Profesional Inicial correspondiente (BOJA 16-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-13</td>
<td>Plan de Plurilingüismo</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc9sept2010auxiliaresconversacionerasmus2010-11.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 9-09-2010</a> de la Dirección General de Participación e Innovación Educativa sobre Auxiliares de Conversación y otros Colaboradores Lingüísticos Erasmus para el curso escolar 2010/2011.</td>
</tr>
<tr>
<td width="70">2010-09-09</td>
<td>Bachillerato</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden26agosto2010pruebastituloBachiller.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 26 de agosto de 2010</a>, por la que se regulan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 09-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-09</td>
<td>Educación de Personas Adultas</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden26agosto2010pruebastituloBachiller.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 26 de agosto de 2010</a>, por la que se regulan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 09-09-2010).</td>
</tr>
<tr>
<td width="70">2010-09-09</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/AclaracionesROCsecundaria9sept2010.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria</a>, aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 9 de septiembre de 2010).</td>
</tr>
<tr>
<td width="70">2010-09-09</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/AclaracionesROCprimaria9sept2010.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Aclaraciones en torno al Reglamento Orgánico de las Escuelas Infantiles de Segundo Ciclo</a>, de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centros públicos Específicos de Educación Especial, aprobado por el DECRETO 328/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de las Escuelas Infantiles de Segundo Ciclo, de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centros públicos Específicos de Educación Especial, así como el horario de los centros, del alumnado y del profesorado (actualización de 9 de septiembre de 2010).</td>
</tr>
<tr>
<td width="70">2010-08-31</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc31agosto2010centrosbilingues.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 31-08-2010</a> conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente sobre la organización y funcionamiento de la modalidad de Enseñanza Bilingüe para el curso 2010-2011.</td>
</tr>
<tr>
<td width="70">2010-08-31</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc31agosto2010centrosbilingues.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 31-08-2010</a> conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente sobre la organización y funcionamiento de la modalidad de Enseñanza Bilingüe para el curso 2010-2011.</td>
</tr>
<tr>
<td width="70">2010-08-31</td>
<td>Plan de Plurilingüismo</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instruc31agosto2010centrosbilingues.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 31-08-2010</a> conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente sobre la organización y funcionamiento de la modalidad de Enseñanza Bilingüe para el curso 2010-2011.</td>
</tr>
<tr>
<td width="70">2010-08-30</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden20agosto2010organizacionfuncionamientoceips.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 20-08-2010</a>, por la que se regula la organización y el funcionamiento de las escuelas infantiles de segundo ciclo, de los colegios de educación primaria, de los colegios de educación infantil y primaria y de los centros públicos específicos de educación especial, así como el horario de los centros, del alumnado y del profesorado (BOJA 30-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-30</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden20agosto2010organizacionfuncionamientoies.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 20-08-2010</a>, por la que se regula la organización y el funcionamiento de los institutos de educación secundaria, así como el horario de los centros, del alumnado y del profesorado (BOJA 30-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-26</td>
<td>Centros Docentes</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden22julio2010modificacioncentros.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 22-07-2010</a>, por la que se modifican Escuelas Infantiles, Colegios de Educación Primaria y Centros Específicos de Educación Especia (BOJA 26-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-26</td>
<td>Conciertos Educativos</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden30julio2010conciertos2010_11.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 30-07-2010</a>, por la que se resuelve la convocatoria de la Orden que se indica para el acceso al régimen de conciertos educativos o la renovación o modificación de los mismos con centros docentes privados de la Comunidad Autónoma de Andalucía, a partir del curso académico 2010/11 (BOJA 26-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-12</td>
<td>Actividades Complementarias y Extraescolares</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden3agosto2010servicioscomplementarios.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 3-08-2010</a>, por la que se regulan los servicios complementarios de la enseñanza de aula matinal, comedor escolar y actividades extraescolares en los centros docentes públicos, así como la ampliación de horario (BOJA 12-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-12</td>
<td>Comedor Escolar</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden3agosto2010servicioscomplementarios.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 3-08-2010</a>, por la que se regulan los servicios complementarios de la enseñanza de aula matinal, comedor escolar y actividades extraescolares en los centros docentes públicos, así como la ampliación de horario (BOJA 12-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-12</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/Aclaraciones29julio2010DG-OEEroc_ies.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ACLARACIONES de 29 de julio de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa en torno al Reglamento Orgánico de los institutos de Educación Secundaria, aprobado por el DECRETO 327/2010, de 13 de julio.</td>
</tr>
<tr>
<td width="70">2010-08-12</td>
<td>Organización y Funcionamiento</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/Aclaraciones29julio2010DG-OEEroc_ceip.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ACLARACIONES de 29 de julio de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa en torno al Reglamento Orgánico de las Escuelas Infantiles de Segundo Ciclo, de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centros públicos Específicos de Educación Especial, aprobado por el DECRETO 328/2010, de 13 de julio.</td>
</tr>
<tr>
<td width="70">2010-08-12</td>
<td>Plan Apoyo Familias</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden3agosto2010servicioscomplementarios.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 3-08-2010</a>, por la que se regulan los servicios complementarios de la enseñanza de aula matinal, comedor escolar y actividades extraescolares en los centros docentes públicos, así como la ampliación de horario (BOJA 12-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-11</td>
<td>Centros Docentes</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/Decreto351-2010centrosinfantilmunicipales.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">DECRETO 351/2010</a>, de 27 de julio, por el que se crean escuelas infantiles de titularidad municipal en la Comunidad Autónoma de Andalucía (BOJA 11-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-10</td>
<td>Centros Docentes</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/Decreto339-2010creacioncentros.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">DECRETO 339/2010</a>, de 20 de julio, por el que se crean y suprimen centros docentes públicos en la Comunidad Autónoma de Andalucía (BOJA 10-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-10</td>
<td>Centros Docentes</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/Decreto338-2010creacionconservatoriojaen.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">DECRETO 338/2010</a>, de 20 de julio, por el que se crea el Conservatorio Superior de Música en la ciudad de Jaén (BOJA 10-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-07</td>
<td>Bachillerato</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden30julio2010curriculomixtobachilleratobaccalaureat.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN EDU/2157/2010, de 30 de julio</a>, por la que se regula el currículo mixto de las enseñanzas acogidas al Acuerdo entre el Gobierno de España y el Gobierno de Francia relativo a la doble titulación de Bachiller y de Baccalauréat en centros docentes españoles, así como los requisitos para su obtención (BOE 07-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-06</td>
<td>Plan de Plurilingüismo</td>
<td><a href="http://www.adideandalucia.es/normas/resoluc/CorreccionerroresResol26mayo2010centrosbilingues.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">CORRECCIÓN de errores de la Resolución de 26 de mayo de 2010</a>, de la Dirección General de Participación e Innovación Educativa, por la que se autorizan nuevos centros docentes públicos bilingües dependientes de la Consejería de Educación a partir del curso 2010-2011 (BOJA 06-08-2010).</td>
</tr>
<tr>
<td width="70">2010-08-05</td>
<td>Plan de Plurilingüismo</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden6julio2010convocatoriabolsaauxiliares2010-11.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 6-07-2010</a>, por la que se realiza convocatoria pública para constituir una bolsa de auxiliares de conversación en lenguas extranjeras en centros docentes públicos bilingües y Escuelas Oficiales de Idiomas, dependientes de la Consejería de Educación de la Junta de Andalucía, para el curso escolar 2010/2011, y se procede a la avocación y delegación de la competencia para su resolución (BOJA 05-08-2010).</td>
</tr>
<tr>
<td width="70">2010-07-30</td>
<td>Centros Docentes</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden5julio2010autorizacionpuestosinfantilmunicipal.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN de 5-07-2010</a>, por la que se autoriza el número de puestos escolares de primer ciclo de educación infantil, así como la denominación específica, a determinadas escuelas infantiles de titularidad municipal (BOJA 30-07-2010).</td>
</tr>
<tr>
<td width="70">2010-07-29</td>
<td>Bachillerato</td>
<td><a href="http://www.adideandalucia.es/normas/ordenes/Orden13julio2010premiosnacionalesbachillerato.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">ORDEN EDU/2058/2010, de 13 de julio</a>, por la que se regulan los Premios Nacionales de Bachillerato establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 29-07-2010).</td>
</tr>
<tr>
<td width="70">2010-07-28</td>
<td>Leyes Autonómicas</td>
<td><a href="http://www.adideandalucia.es/normas/decretos/DecretoLey5-2010reordenacionsectorpublico.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">DECRETO-LEY 5/2010, de 27 de julio</a>, por el que se aprueban medidas urgentes en materia de reordenación del sector público (BOJA 28-07-2010).</td>
</tr>
<tr>
<td width="70">2010-07-28</td>
<td>Libros de Texto y Material Curricular</td>
<td><a href="http://www.adideandalucia.es/normas/instruc/Instrucciones2junio2010gratuidadtextos2010-11.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INSTRUCCIONES de 2-06-2010</a>, de la Dirección General de Participación e Innovación Educativa, sobre el programa de gratuidad de los libros de texto para el curso escolar 2010/2011.</td>
</t";s:9:"extension";s:11:"com_content";s:10:"created_by";s:1:"0";s:8:"modified";s:19:"0000-00-00 00:00:00";s:11:"modified_by";s:1:"0";s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":0:{}}s:3:"lft";s:2:"91";s:9:"parent_id";s:1:"1";s:5:"level";s:1:"1";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":0:{}}s:4:"slug";s:13:"196:normativa";s:4:"mime";N;s:6:"layout";s:8:"category";s:10:"metaauthor";N;s:4:"path";s:49:"index.php?option=com_content&view=category&id=196";s:6:"weight";d:1.3067599999999999;s:7:"link_id";i:471;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:4:"link";i:4;s:7:"metakey";i:5;s:8:"metadesc";i:6;s:10:"metaauthor";i:7;s:6:"author";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:2:{s:4:"Type";a:1:{s:8:"Category";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:8:"Category";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:49:"index.php?option=com_content&view=category&id=196";s:5:"route";s:49:"index.php?option=com_content&view=category&id=196";s:5:"title";s:9:"Normativa";s:11:"description";s:29553:"ÚLTIMA NORMATIVA Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía Últimas 30 disposiciones añadidasnormal 11px/normal arial; color: #333333;" align="center" bgcolor="#9AB2CE"> F. GRAB CATEGORÍA DISPOSICIÓN 2011-05-10 Centros Docentes DECRETO 140/2011, de 26 de abril , por el que se modifican varios decretos relativos a la autorización de centros docentes para su adaptación a la Ley 17/2009, de 23 de noviembre, sobre el libre acceso a las actividades de servicios y su ejercicio (BOJA 10-05-2011). 2011-05-09 Inspección Educativa: Oposiciones ORDEN de 26 de abril de 2011 , ppor la que se realiza convocatoria pública para el acceso al Cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía (BOJA 09-05-2011). 2011-05-04#333333;"> Funcionarios Docentes: Provisión de Puestos / Colocación Efectivos / Concursos ORDEN de 11 de abril de 2011 , por la que se regulan los procedimientos de recolocación y de redistribución del personal funcionario de carrera de los cuerpos docentes contemplados en la disposición adicional séptima de la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOJA 04-05-2011). 2011-05-03 Convivencia Escolar ORDEN de 11 de abril de 2011 , por la que se regula la participación de los centros docentes en la Red Andaluza «Escuela: Espacio de Paz» y el procedimiento para solicitar reconocimiento como Centros Promotores de Convivencia Positiva (Convivencia+) (BOJA 03-05-2011). 2011-04-30 Plan Apoyo Familias RESOLUCIÓN de 6 de abril de 2011 , de la Dirección General de Planificación y Centros, por la que se actualizan los centros docentes públicos que tienen autorizados los servicios de aula matinal, comedor escolar yactividades extraescolares (BOJA 30-04-2011). 2011-04-29 Arte Dramático INSTRUCCIONES de 29 de abril de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, sobre admisión del alumnado y pruebas de acceso a las Enseñanzas Artísticas Superiores para el curso académico 2011/2012. Anexos I al VIII 2011-04-29 Danza INSTRUCCIONES de 29 de abril de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, sobre admisión del alumnado y pruebas de acceso a las Enseñanzas Artísticas Superiores para el curso académico 2011/2012. Anexos I al VIII 2011-04-29 Escolarización y Matriculaciónnormal 11px/normal arial; color: #333333;"> INSTRUCCIONES de 29 de abril de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, sobre admisión del alumnado y pruebas de acceso a las Enseñanzas Artísticas Superiores para el curso académico 2011/2012. Anexos I al VIII 2011-04-29 Música INSTRUCCIONES de 29 de abril de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, sobre admisión del alumnado y pruebas de acceso a las Enseñanzas Artísticas Superiores para el curso académico 2011/2012. Anexos I al VIII 2011-04-26 Centros Docentes ORDEN de 22 de marzo de 2011 , por la que se autoriza la denominación específica, así como elnúmero de puestos escolares de primer ciclo de educación infantil, a determinadas escuelas infantiles de titularidad municipal (BOJA 26-04-2011). 2011-04-26 Centros Docentes ORDEN de 21 de febrero de 2011 , por la que se autoriza la denominación específica, así como el número de puestos escolares de primer ciclo de educación infantil, a determinadas escuelas infantiles de titularidad municipal (BOJA 26-04-2011). 2011-04-20 Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticas CORRECCIÓN de errores de la Orden de 14 de marzo de 2011 , por la que se efectúa convocatoria de procedimiento selectivo para el ingreso en el Cuerpo de Maestros (BOJA 20-04-2011). 2011-04-14 Educación Infantil INSTRUCCIONES de 31 de marzode 2011 de la Dirección General de Planificación y Centros sobre determinados aspectos del primer ciclo de la educación infantil. 2011-04-14 Formación Profesional Inicial INSTRUCCIONES de 5 de abril de 2011 de la Dirección General de Formación Profesional y Educación Permanente sobre organización y realización de las pruebas de acceso a los ciclos formativos de formación profesional en las convocatorias de junio y septiembre de 2011. 2011-04-13 Organización y Funcionamiento RESOLUCIÓN de 1 de abril de 2011 , de la Dirección General de la Agencia Andaluza de Evaluación Educativa, por la que se establecen los indicadores homologados para la autoevaluación de los centros docentes públicos (BOJA 13-04-2011). 2011-04-12 Educación Especialhref="http://www.adideandalucia.es/normas/instruc/Instruc10marzo2011DictamenesAltasCapacidades.pdf"> INSTRUCCIONES de 10 de marzo de 2011 de la Dirección General de Participación e Innovación Educativa por las que se concretan determinados aspectos sobre los dictámenes para el alumnado con necesidades específicas de apoyo educativo. 2011-04-12 Equipos de Orientación Educativa INSTRUCCIONES de 10 de marzo de 2011 de la Dirección General de Participación e Innovación Educativa por las que se concretan determinados aspectos sobre los dictámenes para el alumnado con necesidades específicas de apoyo educativo. 2011-04-12 Escolarización y Matriculación CORRECCIÓN de errores de la Orden de 24 de febrero de 2011 , por la que se desarrolla el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato (BOJA 12-04-2011). 2011-04-12normal normal normal 11px/normal arial; color: #333333;"> Pruebas de Diagnóstico INSTRUCCIONES de 7 de abril de 2011 de la Agencia Andaluza de evaluación Educativa (AGAEVE), por la que se concretan determinados aspectos relacionados con la realización de las Pruebas de Evaluación de Diagnóstico para el curso 2010-2011. 2011-04-08 Educación Primaria ORDEN de 17 de marzo de 2011 , por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011). 2011-04-08 Escolarización y Matriculación INSTRUCCIONES de la Viceconsejería de Educación de 22 de marzo de 2011 sobre planificación de la escolarización para el curso académico 2011/2012 en los centros docentes públicos y privados concertados.#333333;" width="70"> 2011-04-04 Bachillerato ORDEN de 17 de marzo de 2011 , por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011). 2011-04-04 Educación Infantil ORDEN de 17 de marzo de 2011 , por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011). 2011-04-04 Educación Secundaria ORDEN de 17 de marzo de 2011 , por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía(BOJA 04-04-2011). 2011-04-04 Evaluación de Enseñanzas ORDEN de 17 de marzo de 2011 , por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011). 2011-04-01 Consejería de Educación ORDEN de 22 de febrero de 2011 , por la que se modifica la Orden de 22 de septiembre de 2003, por la que se delegan competencias en diversos órganos de la Consejería (BOJA 01-04-2011). 2011-03-31 Educación de Personas Adultas CORRECCIÓN de erratas de la Resolución de 9 de marzo de 2011 , del la Dirección General de Formación Profesional y Educación Permanente, por la que se fijan los días ylugares de celebración, el desarrollo, la estructura y calificación de las pruebas para la obtención del título de Graduado en Educación Secundaria Obligatoria para personas mayores de dieciocho años y se nombran los Tribunales correspondientes (BOJA 31-03-2011). 2011-03-31 Escolarización y Matriculación CORRECCIÓN de errores al Decreto 40/2011 , de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato (BOJA 31-03-2011). 2011-03-24 Educación de Personas Adultas RESOLUCIÓN de 9 de marzo de 2011 , de la Dirección General de Formación Profesional y Educación Permanente, por la que se fijan los días y lugares de celebración, el desarrollo, la estructura y calificación de las pruebas para la obtención del título de Graduado en Educación Secundaria Obligatoria para personas mayores de dieciocho años y se nombran los Tribunales correspondientes. (BOJA 24-03-2011).normal normal normal 11px/normal arial; color: #333333;" width="70"> 2011-03-24 Plan de Autoprotección INSTRUCCIONES de 16 de marzo de 2011 de la Dirección General de Profesorado y Gestión de Recursos Humanos relativas a los aspectos relacionados con el plan de autoprotección y la prevención de riesgos laborales que deben incluir los reglamentos de organización y funcionamiento de los centros. Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía. Últimas disposiciones añadidas (desde el5 de septiembre de 2009 hasta el 6 de octubre de 2010) F. GRAB CATEGORÍA DISPOSICIÓN 2010-10-12 Sociedad del Conocimiento RESOLUCIÓN de 5 de octubre de 2010 , de la Secretaría de Estado de Educación y Formación Profesional, por la que se establece el procedimiento para la concesión del distintivo de calidad SELLO ESCUELA 2.0 (BOE 12-10-2010). 2010-10-11 Equipos de Orientación Educativa INSTRUCCIONES de 20 de septiembre de 2010 de la Dirección General de Participación e Innovación Educaiva por la que se regulan determinados aspectos sobre la organización y el funcionamiento de las y los profesionales especialistas en atención temprana en la estructura de los Equipos de Orientación Educativa Especializados. 2010-10-11 Equipos de Orientación Educativa INSTRUCCIONES de 17 de septiembre de 2010 de la Dirección Generalde Participación e Innovación Educativa por la que se regula la intervención del educador y educadora social en el ámbito educativo. 2010-10-09 Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticas RESOLUCIÓN de 7 de octubre de 2010 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se regula la fase de prácticas del personal seleccionado en el procedimiento selectivo para el ingreso en los Cuerpos de Profesores de Enseñanza Secundaria, Profesores Técnicos de Formación Profesional, Profesores de Escuelas Oficiales de Idiomas, Profesores de Música y Artes Escénicas, Profesores de Artes Plásticas y Diseño y Maestros de Taller de Artes Plásticas y Diseño (próxima publicación en BOJA). 2010-10-07 Bachillerato INSTRUCCIONES de 4 de octubre de 2010 , de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de 2010. 2010-10-06 Programa de Calidad y Mejora de los Rendimientos Escolaresfont-weight: normal; text-decoration: underline;"> INTRUCCIONES de 6 de octubre de 2010 , de la Dirección General de Ordenación y Evaluación Educativa, sobre la participación de los Centros en el Programa de Calidad y Mejora de los rendimientos escolares. 2010-10-05 Arte Dramático INTRUCCIONES de 30 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado. 2010-10-05 Danza INTRUCCIONES de 30 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado. 2010-10-05 Música INTRUCCIONES de 30 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores deGrado. 2010-10-01 Bachillerato RESOLUCIÓN de 16 de septiembre de 2010 , de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 01-10-2010). 2010-10-06 Programa de Calidad y Mejora de los Rendimientos Escolares INTRUCCIONES de 6 de octubre de 2010 , de la Dirección General de Ordenación y Evaluación Educativa, sobre la participación de los Centros en el Programa de Calidad y Mejora de los rendimientos escolares. 2010-10-05 Arte Dramático INTRUCCIONES de 30 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado. 2010-10-05 DanzaArial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"> INTRUCCIONES de 30 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado. 2010-10-05 Música INTRUCCIONES de 30 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa para el curso académico 2010/2011 sobre la implantación del primer curso de las Enseñanzas Artísticas Superiores de Grado. 2010-10-01 Bachillerato RESOLUCIÓN de 16 de septiembre de 2010 , de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 01-10-2010). 2010-10-01 Bachillerato INSTRUCCIONES de 22 de septiembre , de la Dirección General de Ordenación y Evaluación Educativa, sobre las condiciones de matriculación del alumnado de bachilleratocon evaluación negativa en algunas materias, para el curso académico 2010/2011. 2010-10-01 Educación de Personas Adultas RESOLUCIÓN de 16 de septiembre de 2010 , de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 01-10-2010). 2010-10-01 Plan de Plurilingüismo INSTRUCCIONES de 21 de septiembre de 2010 , de las Direcciones Generales de Planificación y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2010/11, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento. 2010-09-27 Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticas ORDEN de 10 de septiembre de 2010 , por la que se publica la relación delpersonal seleccionado en los procedimientos selectivos para el ingreso en los Cuerpos de Profesores de Enseñanza Secundaria, Profesores Técnicos de Formación Profesional, Profesores de Escuelas Oficiales de Idiomas, Profesores de Música y Artes Escénicas, Profesores de Artes Plásticas y Diseño y Maestros de Taller de Artes Plásticas y Diseño y acceso al Cuerpo de Profesores de Enseñanza Secundaria y Profesores de Artes Plásticas y Diseño y se le nombra con carácter provisional funcionario en prácticas (BOJA 27-09-2010). 2010-09-27 Jornada Personal Docente INSTRUCCIÓN Nº 6/2010 , de 20 de septiembre, de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal con liberación sindical parcial. 2010-09-27 Organización y Funcionamiento Aclaraciones en torno al Reglamento Orgánico de las Escuelas Infantiles de Segundo Ciclo , de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centros públicos Específicos de Educación Especial, aprobado por el DECRETO 328/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de las Escuelas Infantiles de Segundo Ciclo, de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centros públicos Específicos de Educación Especial, asícomo el horario de los centros, del alumnado y del profesorado (actualización de 27 de septiembre de 2010). 2010-09-27 Organización y Funcionamiento Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria , aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 27 de septiembre de 2010). 2010-09-27 Plan para el Fomento de la Cultura Emprendedora ACUERDO de 14 de septiembre de 2010 , del Consejo de Gobierno, por el que se aprueba la formulación del Plan para el Fomento de la Cultura Emprendedora en el sistema educativo público de Andalucía (BOJA 27-09-2010). 2010-09-24 Bachillerato INSTRUCCIONES de 15 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa relativas a los Premios Extraordinarios de Bachillerato correspondientes al curso 2009/2010.width="70"> 2010-09-23 Bachillerato ORDEN de 8 de septiembre de 2010 , por la que se convocan los Premios Extraordinarios de Bachillerato correspondientes al curso 2009-2010 (BOJA 23-09-2010). 2010-09-22 Equipos de Orientación Educativa INSTRUCCIONES de 10 de septiembre de 2010 de la Dirección General de Participación e Innovación Educativa por la que se regulan determinados aspectos sobre la organización y el funcionamiento de las y los profesionales especialistas en atención temprana en la estructura de los Equipos de Orientación Educativa Especializados. 2010-09-22 Organización y Funcionamiento INSTRUCCIONES de 22 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa sobre la organización y funcionamiento, durante el curso 2010/11, de las bibliotecas escolares de los centros docentes públicos que imparten educación primaria o educación secundaria obligatoria. 2010-09-22 Plan de Lectura y Bibliotecashref="http://www.adideandalucia.es/normas/instruc/Instruc22sept2010bibliotecasescolares2010-11.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"> INSTRUCCIONES de 22 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa sobre la organización y funcionamiento, durante el curso 2010/11, de las bibliotecas escolares de los centros docentes públicos que imparten educación primaria o educación secundaria obligatoria. 2010-09-17 Organización y Funcionamiento ORDEN de 8 de septiembre de 2010 , por la que se establece el procedimiento para la gestión de las sustituciones del profesorado de los centros docentes públicos dependientes de esta Consejería (BOJA 17-09-2010). 2010-09-16 Deporte Escolar INSTRUCCIONES de 15-09-2010 de la Dirección General de Participación e Innovación Educativa sobre el programa "Escuelas Deportivas" para el curso escolar 2010/2011. 2010-09-16 Organización y Funcionamiento ORDEN de 3-09-2010 , por la que seestablece el horario de dedicación del profesorado responsable de la coordinación de los planes y programas estratégicos que desarrolla la Consejería competente en materia de educación (BOJA 16-09-2010). 2010-09-16 Plan Apoyo Familias ORDEN de 3-09-2010 , por la que se establece el horario de dedicación del profesorado responsable de la coordinación de los planes y programas estratégicos que desarrolla la Consejería competente en materia de educación (BOJA 16-09-2010). 2010-09-16 Plan de Plurilingüismo ORDEN de 3-09-201 0, por la que se establece el horario de dedicación del profesorado responsable de la coordinación de los planes y programas estratégicos que desarrolla la Consejería competente en materia de educación (BOJA 16-09-2010). 2010-09-16 Programas de Cualificación Profesional Inicial RESOLUCIÓN de 22-07-201 0, de la Dirección General de Formación Profesional y Educación Permanente, por la que se establece el perfil profesional de Operario de Reprografía y el currículo de los módulos específicos del Programa de Cualificación Profesional Inicialcorrespondiente (BOJA 16-09-2010). 2010-09-13 Plan de Plurilingüismo INSTRUCCIONES de 9-09-2010 de la Dirección General de Participación e Innovación Educativa sobre Auxiliares de Conversación y otros Colaboradores Lingüísticos Erasmus para el curso escolar 2010/2011. 2010-09-09 Bachillerato ORDEN de 26 de agosto de 2010 , por la que se regulan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 09-09-2010). 2010-09-09 Educación de Personas Adultas ORDEN de 26 de agosto de 2010 , por la que se regulan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 09-09-2010). 2010-09-09 Organización y Funcionamiento Aclaraciones en torno al Reglamento Orgánico de los institutos deEducación Secundaria , aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 9 de septiembre de 2010). 2010-09-09 Organización y Funcionamiento Aclaraciones en torno al Reglamento Orgánico de las Escuelas Infantiles de Segundo Ciclo , de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centros públicos Específicos de Educación Especial, aprobado por el DECRETO 328/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de las Escuelas Infantiles de Segundo Ciclo, de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centros públicos Específicos de Educación Especial, así como el horario de los centros, del alumnado y del profesorado (actualización de 9 de septiembre de 2010). 2010-08-31 Organización y Funcionamiento INSTRUCCIONES de 31-08-2010 conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente sobre la organización y funcionamiento de la modalidad de Enseñanza Bilingüe para el curso 2010-2011.width="70"> 2010-08-31 Organización y Funcionamiento INSTRUCCIONES de 31-08-2010 conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente sobre la organización y funcionamiento de la modalidad de Enseñanza Bilingüe para el curso 2010-2011. 2010-08-31 Plan de Plurilingüismo INSTRUCCIONES de 31-08-2010 conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente sobre la organización y funcionamiento de la modalidad de Enseñanza Bilingüe para el curso 2010-2011. 2010-08-30 Organización y Funcionamiento ORDEN de 20-08-2010 , por la que se regula la organización y el funcionamiento de las escuelas infantiles de segundo ciclo, de los colegios de educación primaria, de los colegios de educación infantil y primaria y de los centros públicos específicos de educación especial, así como el horario de los centros, del alumnado y del profesorado (BOJA 30-08-2010). 2010-08-30 Organizacióny Funcionamiento ORDEN de 20-08-2010 , por la que se regula la organización y el funcionamiento de los institutos de educación secundaria, así como el horario de los centros, del alumnado y del profesorado (BOJA 30-08-2010). 2010-08-26 Centros Docentes ORDEN de 22-07-2010 , por la que se modifican Escuelas Infantiles, Colegios de Educación Primaria y Centros Específicos de Educación Especia (BOJA 26-08-2010). 2010-08-26 Conciertos Educativos ORDEN de 30-07-2010 , por la que se resuelve la convocatoria de la Orden que se indica para el acceso al régimen de conciertos educativos o la renovación o modificación de los mismos con centros docentes privados de la Comunidad Autónoma de Andalucía, a partir del curso académico 2010/11 (BOJA 26-08-2010). 2010-08-12 Actividades Complementarias y Extraescolares ORDEN de3-08-2010 , por la que se regulan los servicios complementarios de la enseñanza de aula matinal, comedor escolar y actividades extraescolares en los centros docentes públicos, así como la ampliación de horario (BOJA 12-08-2010). 2010-08-12 Comedor Escolar ORDEN de 3-08-2010 , por la que se regulan los servicios complementarios de la enseñanza de aula matinal, comedor escolar y actividades extraescolares en los centros docentes públicos, así como la ampliación de horario (BOJA 12-08-2010). 2010-08-12 Organización y Funcionamiento ACLARACIONES de 29 de julio de 2010 de la Dirección General de Ordenación y Evaluación Educativa en torno al Reglamento Orgánico de los institutos de Educación Secundaria, aprobado por el DECRETO 327/2010, de 13 de julio. 2010-08-12 Organización y Funcionamiento ACLARACIONES de 29 de julio de 2010 de la Dirección General de Ordenación y Evaluación Educativa en torno al Reglamento Orgánico de las Escuelas Infantiles de Segundo Ciclo, de los colegios de Educación Primaria, de los colegios de Educación Infantil y Primaria y de los centrospúblicos Específicos de Educación Especial, aprobado por el DECRETO 328/2010, de 13 de julio. 2010-08-12 Plan Apoyo Familias ORDEN de 3-08-2010 , por la que se regulan los servicios complementarios de la enseñanza de aula matinal, comedor escolar y actividades extraescolares en los centros docentes públicos, así como la ampliación de horario (BOJA 12-08-2010). 2010-08-11 Centros Docentes DECRETO 351/2010 , de 27 de julio, por el que se crean escuelas infantiles de titularidad municipal en la Comunidad Autónoma de Andalucía (BOJA 11-08-2010). 2010-08-10 Centros Docentes DECRETO 339/2010 , de 20 de julio, por el que se crean y suprimen centros docentes públicos en la Comunidad Autónoma de Andalucía (BOJA 10-08-2010). 2010-08-10 Centros Docentes DECRETO 338/2010 , de 20 dejulio, por el que se crea el Conservatorio Superior de Música en la ciudad de Jaén (BOJA 10-08-2010). 2010-08-07 Bachillerato ORDEN EDU/2157/2010, de 30 de julio , por la que se regula el currículo mixto de las enseñanzas acogidas al Acuerdo entre el Gobierno de España y el Gobierno de Francia relativo a la doble titulación de Bachiller y de Baccalauréat en centros docentes españoles, así como los requisitos para su obtención (BOE 07-08-2010). 2010-08-06 Plan de Plurilingüismo CORRECCIÓN de errores de la Resolución de 26 de mayo de 2010 , de la Dirección General de Participación e Innovación Educativa, por la que se autorizan nuevos centros docentes públicos bilingües dependientes de la Consejería de Educación a partir del curso 2010-2011 (BOJA 06-08-2010). 2010-08-05 Plan de Plurilingüismo ORDEN de 6-07-2010 , por la que se realiza convocatoria pública para constituir una bolsa de auxiliares de conversación en lenguas extranjeras en centros docentes públicos bilingües y Escuelas Oficiales de Idiomas, dependientes de la Consejeríade Educación de la Junta de Andalucía, para el curso escolar 2010/2011, y se procede a la avocación y delegación de la competencia para su resolución (BOJA 05-08-2010). 2010-07-30 Centros Docentes ORDEN de 5-07-2010 , por la que se autoriza el número de puestos escolares de primer ciclo de educación infantil, así como la denominación específica, a determinadas escuelas infantiles de titularidad municipal (BOJA 30-07-2010). 2010-07-29 Bachillerato ORDEN EDU/2058/2010, de 13 de julio , por la que se regulan los Premios Nacionales de Bachillerato establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 29-07-2010). 2010-07-28 Leyes Autonómicas DECRETO-LEY 5/2010, de 27 de julio , por el que se aprueban medidas urgentes en materia de reordenación del sector público (BOJA 28-07-2010). 2010-07-28 Libros de Texto y Material CurricularVerdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"> INSTRUCCIONES de 2-06-2010 , de la Dirección General de Participación e Innovación Educativa, sobre el programa de gratuidad de los libros de texto para el curso escolar 2010/2011.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"0000-00-00 00:00:00";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2012-10-25 20:28:09";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:1;}}";
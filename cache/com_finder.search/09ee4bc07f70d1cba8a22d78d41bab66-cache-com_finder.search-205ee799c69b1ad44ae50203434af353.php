<?php die("Access Denied"); ?>#x#s:275232:"a:8:{i:0;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"286";s:5:"alias";s:17:"selectividad-2015";s:7:"summary";s:35261:"<h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE 2015<br /><br /></span></h1>
<ul style="text-align: justify;">
<li><strong style="color: #2a2a2a;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/15  (MIÉRCOLES) POR LA TARDE EN <a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a;"> </span><strong style="color: #2a2a2a;">EXÁMENES QUE HAN SALIDO EN JUNIO 2015 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<p style="text-align: justify;"><span style="text-decoration: underline;"><strong>JUNIO 2015 - EXÁMENES</strong></span></p>
<p style="text-align: justify;"><strong>16/06/15. </strong>Martes. primer día: sin incidencias, todos a su hora con los nervios habituales.</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II</li>
<li>HISTORIA DE ESPAÑA</li>
<li>HISTORIA DE LA FILOSOFÍA</li>
<li>IDIOMA EXTRANJERO</li>
</ul>
<p style="text-align: justify;"><strong>17/06/15.</strong> Miércoles. Una incidencia. Una alumna no se presenta al examen, nervios, llamada al instituto, había decidido no presentarse ¡pero se matriculó! hubiera necesitado saberlo con antelación. Nada más destacable.</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>HISTORIA DEL ARTE</li>
<li>MATEMÁTICAS II <span style="line-height: 16.6399993896484px;">Y COLISIÓN</span></li>
<li>TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS</li>
<li>QUÍMICA</li>
<li>ELECTROTÉCNIA</li>
<li>LITERATURA UNIVERSAL</li>
<li>LENGUAJE Y PRÁCTICA MUSICAL</li>
<li>TECNOLOGÍA INDUSTRIAL II</li>
<li>MATEMÁT. APLIC. A LAS CC. SOCIALES II Y COLISIÓN</li>
</ul>
<p style="text-align: justify;"><strong>18/06/15. </strong>Jueves. Dos alumnos que no se presentan, uno que llega tarde por la tarde, cosas del bus que lo ha perdido, ¡pero es que no hay taxis!</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>ANÁLISIS MUSICAL II (¡YA!)</li>
<li>DISEÑO <span style="line-height: 16.6399993896484px;">(¡YA!)</span></li>
<li>GEOGRAFÍA</li>
<li>BIOLOGÍA</li>
<li>DIBUJO TÉCNICO II</li>
<li>CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES</li>
<li>ECONOMÍA DE LA EMPRESA</li>
<li>GRIEGO II</li>
<li>HISTORIA DE LA MÚSICA Y LA DANZA</li>
<li>DIBUJO ARTÍSTICO II</li>
<li>FÍSICA</li>
<li>LATÍN II</li>
</ul>
<p style="text-align: justify;">Los criterios de corrección ya <a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_criterios_correccion.php" target="_blank" title="Criterios de corrección oficiales.">han sido publicados oficialmente</a> y añadidos en cada asignatura abajo.</p>
<p style="text-align: justify;">Las soluciones: más adelante. Ya están las de Matemáticas II.</p>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Biología</strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br />Ciencias de la Tierra y Medioamb.</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Comentario de Texto, Lengua Castellana y Literatura</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Artístico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Técnico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Economía <br />de la Empresa</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Diseño</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Electrotecnia</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Biologia_2014_15.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></span></strong></a><strong style="font-size: inherit;">Colisión</strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Biologiia.pdf" target="_blank">Crit. Correc.</a><br /><a href="archivos_ies/14_15/selectividad/criterios/Biologiia_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/CTM_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Ciencias%20De%20La%20Tierra.pdf" target="_blank">Crit. Correc.<br /></a><br /><a href="archivos_ies/14_15/selectividad/criterios/Ciencias%20De%20La%20Tierra_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong><br /> <a href="archivos_ies/14_15/selectividad/Comentario_lengua_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Comentario%20Texto%20Lengua%20Castellana%20Y%20Literatura.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> <a href="archivos_ies/14_15/selectividad/D_Artistico_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Colisión</span></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Dibujo%20Artiistico.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/D_Tecnico_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/DIBUJO_PAU_junio_2015_c.pdf" target="_blank">Soluciones</a><br />(Prof.  Manuel Martínez Vela<br />IES P. MANJÓN)<br /><a href="archivos_ies/14_15/selectividad/criterios/Dibujo%20Teecnico.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Economia_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Economiia%20De%20La%20Empresa.pdf" target="_blank">Crit. Correc.<br /></a><br /><a href="archivos_ies/14_15/selectividad/criterios/Economiia%20De%20La%20Empresa_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Dise%C3%B1o_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Diseno.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Disenno.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td align="center" bgcolor="#abFDe8">
<p style="text-align: center;"><a href="archivos_ies/14_15/selectividad/Electrotecnia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Colisión</strong></p>
<p style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Electrotecnia.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Crit. Correc.<br /><br /></a><a href="archivos_ies/14_15/selectividad/criterios/Electrotecnia_incom.pdf" target="_blank" style="font-size: inherit; text-decoration: none; color: #2f310d;">Crit. Incomp.</a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Dise%C3%B1o.pdf" target="_blank"><br /></a>Física</strong></td>
<td style="text-align: center;" align="center"><strong><br />Geografía</strong></td>
<td style="text-align: center;" align="center"><strong><br />Griego II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de España</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de la Filosofía</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia <br />del Arte</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong>Latín II </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong>Alemán</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Fisica_2014_15d.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-decoration: none; color: inherit; font-size: inherit; font-weight: normal; font-family: inherit; text-align: left;">Soluciones<a href="archivos_ies/13_14/selectividad/FISICA_2014_Junio.pdf" target="_blank"><br /></a></strong><a href="archivos_ies/14_15/selectividad/criterios/Fiisica.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Fiisica_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Geografia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="37" height="37" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Geografiia.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Geografiia_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> <a href="archivos_ies/14_15/selectividad/Griego_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Griego.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Griego_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong> <a href="archivos_ies/14_15/selectividad/Historia_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20Espanna.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><a href="archivos_ies/14_15/selectividad/Comentario_filosofia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20La%20Filosofiia.pdf" target="_blank">Crit. Correc.<strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong> </strong><a href="archivos_ies/14_15/selectividad/Historia_Arte_2014_15.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20Del%20Arte.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> </strong><a href="archivos_ies/14_15/selectividad/Latin_junio_e_incompatibilidades_2014_15.pdf"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Latiin.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Latiin_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td align="center">
<p><strong style="text-align: center;"><span style="color: #2f310d;"><span style="margin-right: auto; margin-left: auto;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; margin-right: auto; margin-left: auto; display: block;" /></span></span></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"></a><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Alemaan.pdf" target="_blank">Crit. Correc.</a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Francés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Inglés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Literatura Universal</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas Aplicadas a las Ciencias Sociales II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Matemáticas II</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong><br /> Química </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong> </strong><strong>Técnicas de Exp. Gráf. Plást. (no disponible)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Tecnología Industrial II</strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Frances_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Francees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Ingles.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Inglees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Lit_Universal_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Literatura%20Universal.pdf" target="_blank">Crit. Correc.<br /><br /></a><a href="archivos_ies/14_15/selectividad/criterios/Literatura%20Universal_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><a href="archivos_ies/14_15/selectividad/Mat_CCSSII_2014_15.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Soluciones_MCCSSII_Junio.pdf" target="_blank">Soluciones<br /></a>(emestrda.net)</strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Criterios_cor_Matemaaticas%20Aplicadas.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a><br /></strong></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">_________</strong></p>
<strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/Mat_CCSSII_COLISIION_2014_15.pdf" target="_blank">Colisión<br /></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Criterios_cor_Matemaaticas%20Aplicadas_incom.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Incomp.</a></strong><br />_________<br /><br /></strong></td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/selectividad/Mat_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a>NUEVO: <br /></span></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/15_mod_soljun.pdf" target="_blank">Soluciones<br /><strong style="color: #000000; font-family: Arial; font-size: medium; letter-spacing: normal; text-align: -webkit-center;"><span style="font-size: 8;">(D. Germán Jesús Rubio Luna</span></strong></a>)<br /><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Matemaaticas.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a></strong></strong><br />_________<br /><a href="archivos_ies/14_15/selectividad/Mat_II_COLISION_2014_15b.pdf" target="_blank">Colisión<br /></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Matemaaticas_incom.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Incomp.</a></strong><br />_________</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Quimica_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/QUIMICA_JUNIO_2015.pdf" target="_blank">Soluciones<br /></a></strong>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">(emestrda.net)<br /><br /></strong><a href="archivos_ies/14_15/selectividad/criterios/Quiimica.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a></strong></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong style="text-align: left;"><span style="color: #2f310d;"><br /><a href="archivos_ies/14_15/selectividad/Tecn_Expr_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></span></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Teecnicas%20De%20Expresioon.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><span style="color: #2f310d;"><span style="font-weight: normal;">Crit. Correc.</span></span></strong></strong><strong><br /></strong></a></p>
</td>
<td align="center" bgcolor="#abF856">
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Tec_Industrial_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; margin-right: auto; margin-left: auto; display: block;" /></a></span></p>
<p><span style="color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Tecnologiia%20Industrial.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /><br /></a></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Tecnologiia%20Industrial_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a></span><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong>HISTORIA DE LA<br /></strong>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">MÚSICA Y LA DANZA</strong><strong> </strong><strong style="color: inherit; font-family: inherit; font-size: inherit; background-color: #fafbf0;"> </strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br /></strong><strong>LENGUAJE Y PRÁCTICA<br />MUSICAL</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><br /><strong>ANÁLISIS<br />MUSICAL II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br /></strong><strong>Lengua Extranjera II (Italiano)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Portugués)</strong><strong> </strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"> </td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"> </td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><strong><a href="archivos_ies/14_15/selectividad/Historia_musica_danza_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20La%20Muusica%20Y%20Danza.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong style="color: inherit; font-family: inherit;"> <a href="archivos_ies/14_15/selectividad/Lenguaje_y_pr_musical_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #abF856;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Lenguaje%20Y%20Praactica%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p> <strong style="color: inherit; font-family: inherit; font-size: inherit; background-color: #abf856;"><a href="archivos_ies/14_15/selectividad/analisis_musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #abF856;" /></a></strong><span style="color: inherit; font-family: inherit; font-size: inherit;"> </span></p>
<p> <a href="archivos_ies/14_15/selectividad/criterios/Anaalisis%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"><br /><br /><br /><br /> <a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Italiano.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"><br /><br /><br /><br /> <a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Portuguees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"> </td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"> </td>
<td align="center" bgcolor="#ffffff"> </td>
</tr>
</tbody>
</table>
<p style="text-align: justify;"> (<a href="index.php?option=com_content&amp;view=article&amp;id=243:selectividad-2014&amp;catid=108:bachillerato&amp;Itemid=147&amp;highlight=WyJzZWxlY3RpdmlkYWQiLDIwMTQsInNlbGVjdGl2aWRhZCAyMDE0Il0=" target="_blank" title="Selectividad 2014.">Exámenes de Selectividad 2014 junio</a> y <a href="index.php?option=com_content&amp;view=article&amp;id=262kYWQiXQ==" target="_blank">septiembre</a>)</p>
<ul style="text-align: justify;">
<li>
<h3 style="margin: 1px;"><span style="text-align: center; color: #2a2a2a;">22 DE ABRIL A 4 DE JUNIO: REGISTRO PARA LAS PAU.</span></h3>
</li>
<li><span style="color: #7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;">LA MATRÍCULA SE REALIZARÁ DEL 2 AL 4 DE JUNIO Y DEL 3 AL 5 DE SEPTIEMBRE.</span></li>
</ul>
<ul style="text-align: justify;">
<li>
<h3 style="margin: 1px;">PARA CONSEGUIR EL PIN (REGISTRO PARA LAS PAU) ENTRAR EN LA SIGUIENTE WEB:</h3>
</li>
</ul>
<p style="text-align: center;"><strong><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank"><span style="color: #ffffff;">HTTPS://OFICINAVIRTUAL.UGR.ES/APLI/SOLICITUDPAU/SELECTIVIDAD</span>00-MENU.JSP</a></strong></p>
<p style="text-align: justify;"> <a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/documentacion/Parametros_AyB_UA_Bachillerato_2014_2015.pdf" target="_blank" style="font-size: 18px; font-weight: bold; text-align: left; text-transform: uppercase;">PARÁMETROS DE PONDERACIÓN, CONSULTA ESTÁTICA.</a></p>
<ul style="text-align: justify;">
<li>
<p style="margin: 1px;"><strong><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/g_b_parametros_top.php" target="_blank">CONSULTA DINÁMICA</a>.</strong></p>
</li>
<li>
<h4 style="margin: 1px;"><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/" target="_blank">DISTRITO ÚNICO ANDALUZ.</a></h4>
</li>
</ul>
<ul style="text-align: justify;">
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI ENCUENTRO DE LA UNIVERSIDAD CON LOS CENTROS.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">PRESENTACIÓN: REGISTRO PARA LA <span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">PRESENTACIÓN: MATRÍCULA DE <span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank"><span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>: ORIENTACIONES.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">WEB DE LA UNIVERSIDAD PARA REGISTRO Y MATRÍCULA.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">OTROS DATOS DE INTERÉS: </a></h4>
</li>
</ul>
<ol style="text-align: justify;"><ol>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">INSCRIPCIÓN</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PLAZOS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">MATRICULACIÓN</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PRECIOS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">CALENDARIO DE LAS PRUEBAS DÍA A DÍA</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">NOTA DE ACCESO</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">EXÁMENES DE OTROS AÑOS Y ORIENTACIONES</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">ORDEN DE PREFERENCIA DE CARRERAS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PROCEDIMIENTO DE MATRÍCULA EN LA UINIVERSIDAD</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PLAZOS</a></h4>
</li>
</ol></ol>
<h4> </h4>";s:4:"body";s:0:"";s:5:"catid";s:3:"104";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-06-30 16:15:46";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"57";s:8:"ordering";s:1:"0";s:8:"category";s:17:"ÚLTIMA NORMATIVA";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:21:"286:selectividad-2015";s:7:"catslug";s:20:"104:ultima-normativa";s:6:"author";s:14:"Miguel Anguita";s:6:"layout";s:7:"article";s:4:"path";s:93:"index.php?option=com_content&view=article&id=286:selectividad-2015&catid=104:ultima-normativa";s:10:"metaauthor";N;s:6:"weight";d:3.3599300000000003;s:7:"link_id";i:1924;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:14:"Miguel Anguita";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:14:"Miguel Anguita";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:17:"ÚLTIMA NORMATIVA";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:17:"ÚLTIMA NORMATIVA";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=286";s:5:"route";s:93:"index.php?option=com_content&view=article&id=286:selectividad-2015&catid=104:ultima-normativa";s:5:"title";s:17:"Selectividad 2015";s:11:"description";s:4674:"ZONA DEDICADA A LA SELECTIVIDAD DE 2015 LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/15 (MIÉRCOLES) POR LA TARDE EN ESTE ENLACE. EXÁMENES QUE HAN SALIDO EN JUNIO 2015 DE LAS DISTINTAS MATERIAS: JUNIO 2015 - EXÁMENES 16/06/15. Martes. primer día: sin incidencias, todos a su hora con los nervios habituales. COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II HISTORIA DE ESPAÑA HISTORIA DE LA FILOSOFÍA IDIOMA EXTRANJERO 17/06/15. Miércoles. Una incidencia. Una alumna no se presenta al examen, nervios, llamada al instituto, había decidido no presentarse ¡pero se matriculó! hubiera necesitado saberlo con antelación. Nada más destacable. HISTORIA DEL ARTE MATEMÁTICAS II Y COLISIÓN TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS QUÍMICA ELECTROTÉCNIA LITERATURA UNIVERSAL LENGUAJE Y PRÁCTICA MUSICAL TECNOLOGÍA INDUSTRIAL II MATEMÁT. APLIC. A LAS CC. SOCIALES II Y COLISIÓN 18/06/15. Jueves. Dos alumnos que no se presentan, unoque llega tarde por la tarde, cosas del bus que lo ha perdido, ¡pero es que no hay taxis! ANÁLISIS MUSICAL II (¡YA!) DISEÑO (¡YA!) GEOGRAFÍA BIOLOGÍA DIBUJO TÉCNICO II CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES ECONOMÍA DE LA EMPRESA GRIEGO II HISTORIA DE LA MÚSICA Y LA DANZA DIBUJO ARTÍSTICO II FÍSICA LATÍN II Los criterios de corrección ya han sido publicados oficialmente y añadidos en cada asignatura abajo. Las soluciones: más adelante. Ya están las de Matemáticas II. Biología Ciencias de la Tierra y Medioamb. Comentario de Texto, Lengua Castellana y Literatura Dibujo Artístico II Dibujo Técnico II Economía de la Empresabgcolor="#FEFDD6"> Diseño Electrotecnia Colisión Crit. Correc. Crit. Incomp. Crit. Correc. Crit. Incomp.href="archivos_ies/14_15/selectividad/criterios/Comentario%20Texto%20Lengua%20Castellana%20Y%20Literatura.pdf" target="_blank"> Crit. Correc. Colisión Crit. Correc. Soluciones (Prof. Manuel Martínez Vela IES P. MANJÓN) Crit. Correc. Crit. Correc. Crit.Incomp. Crit. Correc. Colisión Crit. Correc. Crit. Incomp. Física Geografía Griego II Historia deEspaña Historia de la Filosofía Historia del Arte Latín II Alemán Soluciones Crit. Correc. Crit. Incomp.#2f310d; font-weight: normal;"> Crit. Correc. Crit. Incomp. Crit. Correc. Crit. Incomp Crit. Correc. Crit. Correc.bgcolor="#abFDe8"> Crit. Correc. Crit. Correc. Crit. Incomp. Crit. Correc.align="center"> Lengua Extranjera II (Francés) Lengua Extranjera II (Inglés) Literatura Universal Matemáticas Aplicadas a las Ciencias Sociales II Matemáticas II Química Técnicas de Exp. Gráf. Plást. (no disponible) Tecnología Industrial II Crit. Correc.bgcolor="#abFDD6"> Crit. Correc. Crit. Correc. Crit. Incomp. Soluciones (emestrda.net)font-size: inherit; text-align: left;"> Crit. Correc. _________ Colisión Crit. Incomp. _________ NUEVO: Soluciones (D. Germán Jesús Rubio Luna ) Crit. Correc. _________href="archivos_ies/14_15/selectividad/Mat_II_COLISION_2014_15b.pdf" target="_blank"> Colisión Crit. Incomp. _________ Soluciones (emestrda.net) Crit. Correc.#2f310d;"> Crit. Correc. Crit. Correc. Crit. Incomp. HISTORIA DE LA MÚSICA Y LA DANZAinherit; font-family: inherit; font-size: inherit; background-color: #fafbf0;"> LENGUAJE Y PRÁCTICA MUSICAL ANÁLISIS MUSICAL II Lengua Extranjera II (Italiano) Lengua Extranjera II (Portugués) Crit. Correc.href="archivos_ies/14_15/selectividad/criterios/Lenguaje%20Y%20Praactica%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;"> Crit. Correc. Crit. Correc. Crit. Correc. Crit. Correc. ( Exámenes de Selectividad 2014 junio yhref="index.php?option=com_content&view=article&id=262kYWQiXQ==" target="_blank"> septiembre ) 22 DE ABRIL A 4 DE JUNIO: REGISTRO PARA LAS PAU. LA MATRÍCULA SE REALIZARÁ DEL 2 AL 4 DE JUNIO Y DEL 3 AL 5 DE SEPTIEMBRE. PARA CONSEGUIR EL PIN (REGISTRO PARA LAS PAU) ENTRAR EN LA SIGUIENTE WEB: HTTPS://OFICINAVIRTUAL.UGR.ES/APLI/SOLICITUDPAU/SELECTIVIDAD 00-MENU.JSP PARÁMETROS DE PONDERACIÓN, CONSULTA ESTÁTICA. CONSULTA DINÁMICA . DISTRITO ÚNICO ANDALUZ. VI ENCUENTRO DE LA UNIVERSIDAD CON LOS CENTROS. PRESENTACIÓN: REGISTRO PARA LAstyle="padding: 1px 4px;"> SELECTIVIDAD . PRESENTACIÓN: MATRÍCULA DE SELECTIVIDAD . SELECTIVIDAD : ORIENTACIONES. WEB DE LA UNIVERSIDAD PARA REGISTRO Y MATRÍCULA. OTROS DATOS DE INTERÉS: INSCRIPCIÓN PLAZOS MATRICULACIÓN PRECIOS CALENDARIO DE LAS PRUEBAS DÍA A DÍA NOTA DE ACCESOhref="index.php?option=com_content&view=article&id=139&Itemid=218" target="_blank"> EXÁMENES DE OTROS AÑOS Y ORIENTACIONES ORDEN DE PREFERENCIA DE CARRERAS PROCEDIMIENTO DE MATRÍCULA EN LA UINIVERSIDAD PLAZOS";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2015-06-16 17:30:58";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2015-06-16 17:30:58";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:1;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"251";s:5:"alias";s:23:"selectividad-junio-2014";s:7:"summary";s:17049:"<h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE 2014</span></h1>
<ul>
<li><strong style="color: #2a2a2a; font-size: 13px;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/14  (MARTES) POR LA TARDE EN <a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a; font-size: 13px;"> </span><strong style="color: #2a2a2a; font-size: 13px;">EXÁMENES QUE HAN SALIDO EN JUNIO 2014 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000; background-color: #e7eaad;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong>Biología</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Ciencias de la Tierra y Medioambientales</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Comentario de Texto, Lengua Castellana y Literatura</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Dibujo Artístico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Dibujo Técnico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Economía de la Empresa</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Diseño</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Electrotecnia</strong></td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><span style="text-decoration: underline;"><a href="archivos_ies/13_14/selectividad/Biologia_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></span></strong><a href="archivos_ies/13_14/selectividad/Biologia_Colision_Junio2014.pdf" target="_blank" style="font-size: inherit;"><strong><span>Colisión</span></strong></a></p>
</td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong> <a href="archivos_ies/13_14/selectividad/Lengua_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a><a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Colision_Junio2014.pdf" target="_blank"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Colisión</span></a></strong></p>
</td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Diseño_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a></strong></p>
<p style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Colision_Junio2014.pdf" target="_blank">Colisión</a><a href="archivos_ies/13_14/selectividad/Electrotecnia_Junio2014.pdf" target="_blank"><br /></a></strong></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;"><strong>Física</strong></td>
<td style="text-align: center;" align="center"><strong>Geografía</strong></td>
<td style="text-align: center;" align="center"><strong>Griego II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de España</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de la Filosofía</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia del Arte</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Latín II</strong><strong> </strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Alemán)</strong></td>
</tr>
<tr>
<td><strong><a href="archivos_ies/13_14/selectividad/Fisica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center"><strong><a href="archivos_ies/13_14/selectividad/Geografia_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a></strong></td>
<td align="center"><strong> <a href="archivos_ies/13_14/selectividad/Griego_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center"><strong> <a href="archivos_ies/13_14/selectividad/H_España_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center"><strong><a href="archivos_ies/13_14/selectividad/Filosofia_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a> </strong></td>
<td align="center"><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/H_Arte_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center"><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></td>
<td align="center"><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a></strong></td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Francés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Inglés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Literatura Universal</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas Aplicadas a las Ciencias Sociales II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong style="text-align: left;"> Química</strong><strong> </strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong> </strong><strong style="text-align: center;">Técnicas de Expresión Gráfico Plásticas (no disponible)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Tecnología Industrial II</strong></td>
</tr>
<tr>
<td style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluciones</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;">Soluciones</a></strong><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"> </span></p>
</td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></span></a></strong><strong> </strong></td>
<td style="text-align: center;" align="center"><strong style="text-align: left;"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></span></strong><strong> </strong></td>
<td align="center"><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a></span><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></td>
</tr>
</tbody>
</table>
<p> </p>
<ul style="color: #2a2a2a; font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13px; line-height: normal;">
<li>
<h3><span style="text-align: center; color: #2a2a2a;">22 de abril a 4 de junio: registro para las PAU.</span></h3>
</li>
<li><span style="color: #7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;">La matrícula se realizará del 2 al 4 de Junio y del 3 al 5 de septiembre.</span></li>
</ul>
<ul>
<li>
<h3>Para conseguir el PIN (registro para las PAU) entrar en la siguiente web:</h3>
</li>
</ul>
<h6 style="background-color: #1188ff;"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></h6>
<p> </p>
<ul>
<li><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI encuentro de la universidad con los centros.<br /><br /></a></li>
<li><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">Presentación: registro para la selectividad.<br /><br /></a></li>
<li><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">Presentación: matrícula de selectividad.<br /><br /></a></li>
<li><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank">Selectividad: orientaciones.<br /><br /></a></li>
<li><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Web de la Universidad para registro y matrícula.<br /><br /></a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Otros datos de interés: </a></li>
</ul>
<ol><ol>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Inscripción</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Matriculación</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Precios</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Calendario de las pruebas día a día</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Nota de acceso</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Exámenes de otros años y orientaciones</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Orden de preferencia de carreras</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Procedimiento de matrícula en la UIniversidad</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></li>
</ol></ol>
<p> </p>
<p><span style="color: #2a2a2a;"> </span></p>";s:4:"body";s:0:"";s:5:"catid";s:3:"108";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-06-15 11:09:26";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":68:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:1:"2";s:8:"ordering";s:1:"0";s:8:"category";s:12:"BACHILLERATO";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:27:"251:selectividad-junio-2014";s:7:"catslug";s:16:"108:bachillerato";s:6:"author";s:10:"Super User";s:6:"layout";s:7:"article";s:4:"path";s:106:"index.php?option=com_content&view=article&id=251:selectividad-junio-2014&catid=108:bachillerato&Itemid=147";s:10:"metaauthor";N;s:6:"weight";d:2.14662;s:7:"link_id";i:993;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"BACHILLERATO";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"BACHILLERATO";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=251";s:5:"route";s:106:"index.php?option=com_content&view=article&id=251:selectividad-junio-2014&catid=108:bachillerato&Itemid=147";s:5:"title";s:23:"Selectividad Junio 2014";s:11:"description";s:2010:"ZONA DEDICADA A LA SELECTIVIDAD DE 2014 LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/14 (MARTES) POR LA TARDE EN ESTE ENLACE. EXÁMENES QUE HAN SALIDO EN JUNIO 2014 DE LAS DISTINTAS MATERIAS: Biología Ciencias de la Tierra y Medioambientales Comentario de Texto, Lengua Castellana y Literatura Dibujo Artístico II Dibujo Técnico II Economía de la Empresa Diseño Electrotecniaborder-color: #bcbcbc; cursor: se-resize !important;" /> Colisión Colisiónse-resize !important;" /> Colisión Física Geografía Griego II Historia de España Historia de la Filosofía Historia del Arte Latín II Lengua Extranjera II(Alemán)href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"> Lengua Extranjera II (Francés) Lengua Extranjera II (Inglés) Literatura Universal Matemáticas Aplicadas a las Ciencias Sociales II Matemáticas II Química Técnicas de Expresión Gráfico Plásticas (no disponible)bgcolor="#FEFDD6"> Tecnología Industrial II Solucionessrc="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /> Soluciones 22 de abril a 4 de junio: registro para las PAU.#7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;"> La matrícula se realizará del 2 al 4 de Junio y del 3 al 5 de septiembre. Para conseguir el PIN (registro para las PAU) entrar en la siguiente web: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp VI encuentro de la universidad con los centros. Presentación: registro para la selectividad. Presentación: matrícula de selectividad. Selectividad: orientaciones. Web de la Universidad para registro y matrícula. Otros datos de interés: Inscripción Plazos Matriculación Precios Calendario de laspruebas día a día Nota de acceso Exámenes de otros años y orientaciones Orden de preferencia de carreras Procedimiento de matrícula en la UIniversidad Plazos";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2014-04-09 18:23:06";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2014-04-09 18:23:06";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:2;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"163";s:5:"alias";s:19:"dep-lengua-espanola";s:7:"summary";s:31980:"<div style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt; text-decoration: underline;"><strong>CURSO 2013/14.</strong></span></div>
<div style="text-align: left;"><ol>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><strong><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;">20/02/2013. 3º ESO. </strong> De_ratones y hombres (EPUB)</strong><span style="color: #222222; font-family: arial, sans-serif; font-size: 13px; font-weight: normal; background-color: #ffffff;"><br /></span></strong></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><strong><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;">20/02/2013. 3º ESO. </strong> De_ratones y hombres (MOBI)</strong></strong></strong></strong></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><strong><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;">20/02/2013. 3º ESO. </strong> De_ratones y hombres (PDF)</strong></strong></strong></strong></strong></strong></span></span></li>
<li><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px;"><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><strong><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;">20/02/2013. 3º ESO. </strong> </strong></strong></strong></strong></strong></strong></span></span>Drácula (EPUB)</strong></li>
<li><span style="color: #222222; font-family: arial, sans-serif;"><strong><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><strong><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;">20/02/2013. 3º ESO. </strong> </strong></strong></strong></strong></strong></strong></span></span>Drácula (MOBI)</strong></span></li>
<li><span style="color: #222222; font-family: arial, sans-serif;"><strong><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><strong><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: 13px; background-color: #ffffff;"><strong style="color: #423c33; font-family: 'arial black', 'avant garde'; font-size: 13.63636302947998px; background-color: #fefaf3;">20/02/2013. 3º ESO. </strong> </strong></strong></strong></strong></strong></strong></span></span><strong>Drácula (PDF)</strong></strong></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><strong>12/01/2013. 3º ESO. </strong>El guardián entre el centeno (EPUB)</span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><strong>12/01/2013. 3º ESO. </strong>El guardián entre el centeno (MOBI)</span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><strong>12/01/2013. 3º ESO. </strong>El guardián entre el centeno (PDF<a href="archivos_ies/lengua/El_guardian_entre_el_centeno_(PDF).pdf">)</a></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';">12/01/2013. 3º ESO. <span style="font-family: arial, helvetica, sans-serif;">El curioso incidente del perro a medianoche (EPUB).</span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';">12/01/2013. 3º ESO. El curioso incidente del perro a medianoche (MOBI).</span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';">12/01/2013. 3º ESO. El curioso incidente del perro a medianoche (PDF).</span></span></li>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;"><span style="font-family: arial black,avant garde;">03/01/2013. 3º ESO.</span> El sabueso de los Baskerville (EPUB).</span></li>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;"><span style="font-family: arial black,avant garde;">03</span><span style="font-size: 10pt;"><span style="font-family: arial black,avant garde;">/01/2013. 3º ESO.</span> El sabueso de los Baskerville MOBI).</span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: arial black,avant garde;">03</span><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;"><span style="font-family: arial black,avant garde;">/01/2013. 3º ESO.</span> </span><span style="font-size: 10pt;">El sabueso de los Baskerville (PDF).</span></span></li>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;">22/10/2012. 3º ESO.</span> <span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><span style="font-family: arial, helvetica, sans-serif;">Seleccion de relatos de Edgar Allan Poe (EPUB)</span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><span style="font-family: arial,helvetica,sans-serif;">22/10/2012. 3º ESO</span>. <span style="font-family: arial, helvetica, sans-serif;">Selección de relatos de Edgar Allan Poe (MOBI).</span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><span style="font-family: arial,helvetica,sans-serif;">22/10/2012. 3º ESO.</span> <span style="font-family: arial, helvetica, sans-serif;">Selección de relatos de Edgar Allan Poe (PDF).</span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><span style="font-family: arial,helvetica,sans-serif;">22/10/2012. 3º ESO.</span> <span style="font-family: arial, helvetica, sans-serif;">Mi planta de naranja lima (EPUB).</span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><span style="font-family: arial,helvetica,sans-serif;">22/10/2012. 3º ESO.</span> <span style="font-family: arial, helvetica, sans-serif;">Mi planta de naranja lima (MOBI).</span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><span style="font-family: arial,helvetica,sans-serif;">22/10/2012. 3º ESO.</span> <span style="font-family: arial, helvetica, sans-serif;">Mi planta de naranja lima(PDF)<a href="archivos_ies/lengua/Mi_planta_de_naranja_lima.pdf" target="_blank">.</a></span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><span style="font-family: arial,helvetica,sans-serif;">04/10/2012. 3º ESO.</span> <span style="font-family: arial, helvetica, sans-serif;">DIEZ NEGRITOS (EPUB).</span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><span style="font-family: arial,helvetica,sans-serif;">04/10/2012. 3º ESO.</span> <span style="font-family: arial, helvetica, sans-serif;">DIEZ NEGRITOS (MOBI).</span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><span style="font-family: 'arial black', 'avant garde';"><span style="font-family: arial,helvetica,sans-serif;">18/09/2012. 3º ESO. DIEZ NEGRITOS (PDF).</span></span></span></li>
<li><span style="font-size: 10pt; font-family: arial,helvetica,sans-serif;">17/09/2012. 3º ESO. Saca la lengua Cervantes.</span></li>
</ol></div>
<div><hr /></div>
<p> </p>
<p><strong><span style="text-decoration: underline;">CRITERIOS DE EVALUACIÓN. CURSO 2013/14.</span></strong></p>
<p> </p>
<ul>
<li><a href="archivos_ies/criterios_evaluacion/Criterios_de_Evaluacion_de_Lengua_1_ESO.pdf" target="_blank" title="Criterios de evaluación de Lengua 1º ESO." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Lengua 1º ESO 2012-13.</a></li>
<li><a href="archivos_ies/criterios_evaluacion/Criterios_de_Evaluacion_de_Lengua_2_ESO.pdf" target="_blank" title="Criterios de evaluación de Lengua 2º ESO." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Lengua 2º ESO 2012-13.</a></li>
<li><a href="archivos_ies/criterios_evaluacion/Criterios_de_Evaluacion_de_Lengua_3_ESO_2012-2013.pdf" target="_blank" title="Criterios de evaluación de Lengua 3º ESO." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Lengua 3º ESO 2012-13.</a></li>
<li><a href="archivos_ies/criterios_evaluacion/Criterios_de_Evaluacion_de_Lengua_4_ESO.pdf" target="_blank" title="Criterios de evaluación de Lengua 4º ESO." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Lengua 4º ESO 2012-13.</a></li>
<li><a href="archivos_ies/criterios_evaluacion/1_BACH LCL.pdf" target="_blank" title="Criterios de evaluación de Lengua 1º Bachillerato." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Lengua 1º Bachillerato 2012-13.</a></li>
<li><a href="archivos_ies/criterios_evaluacion/2_BACH_LCL.pdf" target="_blank" title="Criterios de evaluación de Lengua 2º Bachillerato." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Lengua 2º Bachillerato 2012-13.</a></li>
<li><a href="archivos_ies/lengua/LiteraturaUniversal.pdf" target="_blank" style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">RESUMEN PROGRAMACIÓN LITERATURA UNIVERSAL 2º BACHILLERATO 2012-13.</a></li>
<li><a href="archivos_ies/lengua/programacion_1_bach_Lengua.pdf" target="_blank" style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">PROGRAMACIÓN DIDÁCTICA DE PRIMERO DE BACHILLERATO LENGUA CASTELLANA Y LITERATURA 2012-13.</a></li>
<li><a href="archivos_ies/lengua/2_Bachiller_LCL.pdf" target="_blank" style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">RESUMEN PROGRAMACIÓN LENGUA CASTELLANA Y LITERATURA 2º BACHILLERATO 2012-13.</a></li>
<li><a href="archivos_ies/lengua/programacion_del_ambito_socio-lingiustico_de_3_ESO.docx" target="_blank">PROGRAMACIÓN DIDÁCTICA DEL ÁMBITO SOCIO-LINGÜÍSTICO 3º E.S.O. 2012-13.</a></li>
<li><a href="archivos_ies/criterios_evaluacion/Programacion_Dep_Lengua_y_Literatura_2012-2013.pdf" target="_blank">Programación Didáctica del Departamento de Lengua Castellana y Literatura</a> (2012-2013)</li>
</ul>
<hr />
<h2><span style="text-decoration: underline;"><strong>CUADERNO DE APUNTES DE LENGUA CASTELLANA Y LITERATURA.</strong></span></h2>
<ul>
<li><a href="archivos_ies/lengua/1bach/Esquema_Oracion_Compuesta.doc" target="_blank">Esquema Oración Compuesta</a></li>
<li><a href="archivos_ies/lengua/Todo_lo_que_debes_saber_del_VERBO.doc" target="_blank">Todo lo que debes saber del VERBO.</a></li>
<li><a href="archivos_ies/La_oracion_gramatical.pdf">La oración gramatical.</a> (03/01/12)</li>
<li><a href="archivos_ies/Sintaxis_de_la_oracion_simple.pdf">Sintaxis de la oración simple.</a> (03/01/12)</li>
<li><a href="archivos_ies/lengua/Sintaxis_de_la_oracion_compuesta.pdf" target="_blank">Sintaxis de la oración compuesta.</a> (09/01/12)</li>
</ul>
<div style="padding-left: 30px;"><hr /></div>
<ul>
<li><strong><span style="color: #977702; font-family: Arial, Helvetica, sans-serif;"><span style="text-decoration: underline;">Lecturas obligatorias</span></span> del DEPARTAMENTO DE LENGUA CASTELLANA Y LITERATURA PARA EL CURSO 2011-12.</strong></li>
<li><span style="text-decoration: underline;"><strong><span style="font-size: 10pt; font-family: Arial, sans-serif; color: black; background-image: initial; background-attachment: initial; background-color: white;"><a href="index.php?option=com_content&amp;view=article&amp;id=132:lectuaseletronicas&amp;catid=73&amp;Itemid=59" target="_blank">L</a>ecturas en formato electrónico de Segundo de Bachillerato.</span></strong></span><strong><span style="font-size: 10pt; font-family: Arial, sans-serif; color: black; background-image: initial; background-attachment: initial; background-color: white;"> (acceso identificado)</span></strong></li>
</ul>
<table style="width: 100%; margin-left: 0.75pt; border-collapse: collapse;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="padding: 1.5pt; border: 1pt solid #9a9542;" valign="top">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;">1º ESO </span></div>
</td>
<td style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-top-color: #9a9542; border-right-color: #9a9542; border-bottom-color: #9a9542; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; border-left-width: initial; border-left-color: initial; padding: 1.5pt;" valign="top">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;">2º ESO</span></div>
</td>
<td style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-top-color: #9a9542; border-right-color: #9a9542; border-bottom-color: #9a9542; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; border-left-width: initial; border-left-color: initial; padding: 1.5pt;" valign="top">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;"><a href="index.php?option=com_content&amp;view=article&amp;id=146:3o-eso&amp;catid=73&amp;Itemid=59" target="_blank">3º ESO </a></span></div>
</td>
<td style="border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-top-color: #9a9542; border-right-color: #9a9542; border-bottom-color: #9a9542; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; border-left-width: initial; border-left-color: initial; padding: 1.5pt;" valign="top">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;"><a href="index.php?option=com_content&amp;view=article&amp;id=147:4o-eso&amp;catid=73&amp;Itemid=59" target="_blank">4º ESO</a></span></div>
</td>
</tr>
<tr>
<td style="border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: #9a9542; border-bottom-color: #9a9542; border-left-color: #9a9542; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding: 1.5pt;">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;"><span style="color: #0d0d0d; text-decoration: none;"><a href="index.php?option=com_content&amp;view=article&amp;id=138:1o-bach&amp;catid=73&amp;Itemid=59">1º BACH</a></span></span></div>
</td>
<td style="border-top-style: none; border-top-width: initial; border-top-color: initial; border-left-style: none; border-left-width: initial; border-left-color: initial; border-bottom-style: solid; border-bottom-color: #9a9542; border-bottom-width: 1pt; border-right-style: solid; border-right-color: #9a9542; border-right-width: 1pt; padding: 1.5pt;">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;"><span style="color: #0d0d0d; text-decoration: none;"><a href="index.php?option=com_content&amp;view=article&amp;id=140:dep-lengua-espanola-2o-bach&amp;catid=73&amp;Itemid=59">2º BACH</a></span></span></div>
</td>
<td style="border-top-style: none; border-top-width: initial; border-top-color: initial; border-left-style: none; border-left-width: initial; border-left-color: initial; border-bottom-style: solid; border-bottom-color: #9a9542; border-bottom-width: 1pt; border-right-style: solid; border-right-color: #9a9542; border-right-width: 1pt; padding: 1.5pt;">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;"><a href="index.php?option=com_content&amp;view=article&amp;id=133:temas-de-selectividad-lengua-201112&amp;catid=73&amp;Itemid=59" target="_blank">TEMAS DE SELECTIVIDAD 2º BACHILLERATO</a></span></div>
</td>
<td style="border-top-style: none; border-top-width: initial; border-top-color: initial; border-left-style: none; border-left-width: initial; border-left-color: initial; border-bottom-style: solid; border-bottom-color: #9a9542; border-bottom-width: 1pt; border-right-style: solid; border-right-color: #9a9542; border-right-width: 1pt; padding: 1.5pt;">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;"><a href="index.php?option=com_content&amp;view=article&amp;id=152:pendientes-de-1o-bach-lengua&amp;catid=73&amp;Itemid=59">PENDIENTES DE 1º BACH</a></span></div>
</td>
</tr>
<tr>
<td style="border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: #9a9542; border-bottom-color: #9a9542; border-left-color: #9a9542; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding: 1.5pt;" valign="top">
<div style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: Arial, sans-serif; color: #0d0d0d;">PENDIENTES DE 1º ESO</span></div>
</td>
<td style="border-top-style: none; border-top-width: initial; border-top-color: initial; border-left-style: none; border-left-width: initial; border-left-color: initial; border-bottom-style: solid; border-bottom-color: #9a9542; border-bottom-width: 1pt; border-right-style: solid; border-right-color: #9a9542; border-right-width: 1pt; padding: 1.5pt;" valign="top">
<div style="text-align: center;"><span style="font-size: 9pt; line-height: 115%; font-family: Arial, sans-serif; color: #0d0d0d;">PENDIENTES DE 2º ESO</span></div>
</td>
<td style="border-top-style: none; border-top-width: initial; border-top-color: initial; border-left-style: none; border-left-width: initial; border-left-color: initial; border-bottom-style: solid; border-bottom-color: #9a9542; border-bottom-width: 1pt; border-right-style: solid; border-right-color: #9a9542; border-right-width: 1pt; padding: 1.5pt;" valign="top">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;">PENDIENTES DE 3º ESO</span></div>
</td>
<td style="border-top-style: none; border-top-width: initial; border-top-color: initial; border-left-style: none; border-left-width: initial; border-left-color: initial; border-bottom-style: solid; border-bottom-color: #9a9542; border-bottom-width: 1pt; border-right-style: solid; border-right-color: #9a9542; border-right-width: 1pt; padding: 1.5pt;" valign="top">
<div style="margin-top: 6pt; margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-align: center; line-height: normal;"><span style="font-size: 9pt; font-family: Arial, sans-serif; color: #0d0d0d;">PENDIENTES DE 4º ESO</span></div>
</td>
</tr>
</tbody>
</table>
<table style="border-collapse: collapse;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt; border: 1pt solid windowtext;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;"><span style="color: #ff0000; text-decoration: underline;"><strong>MICRORRELATOS.</strong></span></div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">Concepto_Microrrelato</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">El hogar (Orkény)</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">Vivir para volar</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">Microrrelato tranvía (Bocconi)</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">Microrrelato_pecera</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">Palpitando lento</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">La oveja negra (Monterroso)</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">Memorias de un ciudadano español- Raúl Quirós Hernández</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">Hacerse el muerto (Neuman)</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">Exposición microrrelatos</div>
</td>
</tr>
<tr>
<td style="width: 432.2pt; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576">
<div style="margin-bottom: 0.0001pt; line-height: normal;">Espejismo agónico</div>
</td>
</tr>
</tbody>
</table>
<ul>
<li>Andrés Neuman. <a href="archivos_ies/programa_folleto.pdf" target="_blank">Programa folleto.</a></li>
<li>Cartel del<a href="archivos_ies/cartel_dia_de_andalucia.pdf" target="_blank"> Día de Andalucía.</a></li>
<li>Cartel Exposición<a href="archivos_ies/lengua/Cartel_exposicion.pdf" target="_blank"> Darwin y la evolución de las especies.</a></li>
<li>Cartel <a href="archivos_ies/cartel_flamenco_y_programa.pdf" target="_blank">Flamenco y programa.</a></li>
</ul>
<hr />
<ul>
<li>Enlace a<a href="http://video.google.com/videoplay?docid=208262040160543262&amp;hl=es" target="_blank"> página 1 de vídeos de homenaje a Miguel Hernandez.</a></li>
<li>Enlace a <a href="http://video.google.es/videoplay?docid=-5237273883060838304&amp;hl=es" target="_blank">página 2 de vídeos de homenaje a Miguel Hernandez.</a></li>
</ul>
<p> </p>
<hr />";s:4:"body";s:0:"";s:5:"catid";s:3:"102";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-05-20 10:05:07";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":68:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"91";s:8:"ordering";s:1:"4";s:8:"category";s:28:"DEPARTAMENTOS DE HUMANIDADES";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:23:"163:dep-lengua-espanola";s:7:"catslug";s:32:"102:departamentos-de-humanidades";s:6:"author";N;s:6:"layout";s:7:"article";s:4:"path";s:118:"index.php?option=com_content&view=article&id=163:dep-lengua-espanola&catid=102:departamentos-de-humanidades&Itemid=112";s:10:"metaauthor";N;s:6:"weight";d:6.7066199999999991;s:7:"link_id";i:942;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:28:"DEPARTAMENTOS DE HUMANIDADES";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:28:"DEPARTAMENTOS DE HUMANIDADES";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=163";s:5:"route";s:118:"index.php?option=com_content&view=article&id=163:dep-lengua-espanola&catid=102:departamentos-de-humanidades&Itemid=112";s:5:"title";s:36:"Dep. Lengua Española. Curso 2013/14";s:11:"description";s:5097:"CURSO 2013/14. 20/02/2013. 3º ESO. De_ratones y hombres (EPUB) 20/02/2013. 3º ESO. De_ratones y hombres (MOBI)13px; background-color: #ffffff;"> 20/02/2013. 3º ESO. De_ratones y hombres (PDF) 20/02/2013. 3º ESO. Drácula (EPUB)13.63636302947998px; background-color: #fefaf3;"> 20/02/2013. 3º ESO. Drácula (MOBI) 20/02/2013. 3º ESO. Drácula (PDF) 12/01/2013. 3º ESO. El guardián entre el centeno (EPUB)arial, helvetica, sans-serif;"> 12/01/2013. 3º ESO. El guardián entre el centeno (MOBI) 12/01/2013. 3º ESO. El guardián entre el centeno (PDF ) 12/01/2013. 3º ESO. El curioso incidente del perro a medianoche (EPUB). 12/01/2013. 3º ESO. El curioso incidente del perro a medianoche (MOBI). 12/01/2013. 3º ESO. El curioso incidente del perro a medianoche (PDF). 03/01/2013. 3º ESO. El sabueso de los Baskerville (EPUB). 03 /01/2013. 3º ESO. El sabueso de los Baskerville MOBI). 03 /01/2013. 3º ESO.style="font-size: 10pt;"> El sabueso de los Baskerville (PDF). 22/10/2012. 3º ESO. Seleccion de relatos de Edgar Allan Poe (EPUB) 22/10/2012. 3º ESO . Selección de relatos de Edgar Allan Poe (MOBI). 22/10/2012. 3º ESO. Selección de relatos de Edgar Allan Poe (PDF). 22/10/2012. 3º ESO. Mi planta de naranja lima (EPUB). 22/10/2012. 3º ESO. Mi planta de naranja lima (MOBI). 22/10/2012. 3º ESO.style="font-family: arial, helvetica, sans-serif;"> Mi planta de naranja lima(PDF) . 04/10/2012. 3º ESO. DIEZ NEGRITOS (EPUB). 04/10/2012. 3º ESO. DIEZ NEGRITOS (MOBI). 18/09/2012. 3º ESO. DIEZ NEGRITOS (PDF). 17/09/2012. 3º ESO. Saca la lengua Cervantes. CRITERIOS DE EVALUACIÓN. CURSO 2013/14. Criterios de evaluación de Lengua 1º ESO 2012-13. Criterios de evaluación de Lengua 2º ESO 2012-13.href="archivos_ies/criterios_evaluacion/Criterios_de_Evaluacion_de_Lengua_3_ESO_2012-2013.pdf" target="_blank" title="Criterios de evaluación de Lengua 3º ESO." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;"> Criterios de evaluación de Lengua 3º ESO 2012-13. Criterios de evaluación de Lengua 4º ESO 2012-13. Criterios de evaluación de Lengua 1º Bachillerato 2012-13. Criterios de evaluación de Lengua 2º Bachillerato 2012-13. RESUMEN PROGRAMACIÓN LITERATURA UNIVERSAL 2º BACHILLERATO 2012-13. PROGRAMACIÓN DIDÁCTICA DE PRIMERO DE BACHILLERATO LENGUA CASTELLANA Y LITERATURA 2012-13. RESUMEN PROGRAMACIÓN LENGUA CASTELLANA Y LITERATURA 2º BACHILLERATO 2012-13.href="archivos_ies/lengua/programacion_del_ambito_socio-lingiustico_de_3_ESO.docx" target="_blank"> PROGRAMACIÓN DIDÁCTICA DEL ÁMBITO SOCIO-LINGÜÍSTICO 3º E.S.O. 2012-13. Programación Didáctica del Departamento de Lengua Castellana y Literatura (2012-2013) CUADERNO DE APUNTES DE LENGUA CASTELLANA Y LITERATURA. Esquema Oración Compuesta Todo lo que debes saber del VERBO. La oración gramatical. (03/01/12) Sintaxis de la oración simple. (03/01/12) Sintaxis de la oración compuesta. (09/01/12) Lecturas obligatorias del DEPARTAMENTO DE LENGUA CASTELLANA Y LITERATURA PARA EL CURSO 2011-12. L ecturas en formato electrónico de Segundo de Bachillerato.background-color: white;"> (acceso identificado) 1º ESO 2º ESO 3º ESOborder-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; border-left-width: initial; border-left-color: initial; padding: 1.5pt;" valign="top"> 4º ESO 1º BACH 2ºBACH TEMAS DE SELECTIVIDAD 2º BACHILLERATO PENDIENTES DE 1º BACH#0d0d0d;"> PENDIENTES DE 1º ESO PENDIENTES DE 2º ESO PENDIENTES DE 3º ESO PENDIENTES DE 4º ESOpadding-left: 5.4pt; border: 1pt solid windowtext;" valign="top" width="576"> MICRORRELATOS. Concepto_Microrrelato El hogar (Orkény) Vivir para volarborder-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576"> Microrrelato tranvía (Bocconi) Microrrelato_pecera Palpitando lento0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576"> La oveja negra (Monterroso) Memorias de un ciudadano español- Raúl Quirós Hernández Hacerse el muerto (Neuman) Exposición microrrelatosborder-bottom-style: solid; border-left-style: solid; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; border-top-width: initial; border-top-color: initial; padding-top: 0cm; padding-right: 5.4pt; padding-bottom: 0cm; padding-left: 5.4pt;" valign="top" width="576"> Espejismo agónico Andrés Neuman. Programa folleto. Cartel del Día de Andalucía. Cartel Exposición Darwin y la evolución de las especies. Cartel Flamenco y programa. Enlace a página 1 de vídeos de homenaje a Miguel Hernandez. Enlace a página 2 de vídeos de homenaje a Miguel Hernandez.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2011-01-14 12:16:55";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2011-01-09 12:16:55";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:3;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"243";s:5:"alias";s:17:"selectividad-2014";s:7:"summary";s:22559:"<h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE 2014</span></h1>
<ul>
<li><strong style="color: #2a2a2a; font-size: 13px;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/14  (MARTES) POR LA TARDE EN <a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a; font-size: 13px;"> </span><strong style="color: #2a2a2a; font-size: 13px;">EXÁMENES QUE HAN SALIDO EN JUNIO 2014 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #2a2a2a; font-size: 13px; text-decoration: underline;">JUNIO 2014 - EXÁMENES</strong></span></p>
<p><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_criterios_correccion.php" target="_blank"><span style="text-decoration: underline;"><strong style="color: #2a2a2a; font-size: 13px; text-decoration: underline;">Criterios de correción oficiales. (19/06/14)</strong></span></a></p>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000; background-color: #e7eaad;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Biología</strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br />Ciencias de la Tierra y Medioambientales</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Comentario de Texto, Lengua Castellana y Literatura</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Artístico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Técnico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Economía de la Empresa</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Diseño</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Electrotecnia</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><span style="text-decoration: underline;"><a href="archivos_ies/13_14/selectividad/Biologia_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" /></a></span></strong><a href="archivos_ies/13_14/selectividad/Biologia_Colision_Junio2014.pdf" target="_blank" style="font-size: inherit;"><strong><span>Colisión</span></strong></a></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Biologia_AB.pdf" target="_blank"><strong><span>Crit. Correc.</span></strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" /><br /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_CTM.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><br /> <a href="archivos_ies/13_14/selectividad/Lengua_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Lengua.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a><a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Colision_Junio2014.pdf" target="_blank"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Colisión</span></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_D_Artistico.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_D_Tecnico.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Economia.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Diseño_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank">Crit. Correc.</a><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank"></a></strong></p>
</td>
<td align="center">
<p style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></strong><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Colisión</strong></a></p>
<p style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Electrotecnia.pdf" target="_blank"><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Crit. Correc.</strong></a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank"><br /></a>Física</strong></td>
<td style="text-align: center;" align="center"><strong><br />Geografía</strong></td>
<td style="text-align: center;" align="center"><strong><br />Griego II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de España</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de la Filosofía</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia del Arte</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong>Latín II </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong>Alemán</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><a href="archivos_ies/13_14/selectividad/Fisica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/FISICA_2014_Junio.pdf" target="_blank" style="font-size: inherit; font-weight: normal; text-decoration: none; color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluciones<br /></strong></a><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Fisica.pdf" target="_blank">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Geografia_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Geografia.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/Griego_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Griego_II.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/H_España_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_H_España.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Filosofia_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Filosofia.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong><strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/H_Arte_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_H_Arte.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_LatinAB.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td align="center">
<p><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="display: block; margin-left: auto; margin-right: auto;" /></a></strong></p>
<p><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_AlemanAB.pdf" target="_blank"><strong>Crit. Correc.</strong></a><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /></a></strong></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Francés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Inglés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Literatura Universal</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas Aplicadas a las Ciencias Sociales II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Matemáticas II</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong><br /> Química </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong> </strong><strong style="text-align: center;">Técnicas de Exp. Gráf. Plást. (no disponible)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Tecnología Industrial II</strong></td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Frances.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Ingles.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Lit_Universal.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluciones</strong></a></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_MAT_CCSS_IIAB.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;">Crit. Correc.</strong></strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;">Soluciones</a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Matematicas_II2.pdf" target="_blank"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">Crit. Correc.</strong> </span></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></span></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Quimica.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong><strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong style="text-align: left;"><span style="color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></span></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Tec_Exp_Graf_Plastica.pdf" target="_blank"><strong style="text-align: left;"><span style="color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">Crit. Correc.</strong></span></strong><strong> </strong></a></p>
</td>
<td align="center">
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="display: block; margin-left: auto; margin-right: auto;" /></a></span></p>
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_T_IndustrialAB.pdf" target="_blank"><strong style="color: #000000; text-align: center;">Crit. Correc.</strong></a><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a></span><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<ul style="color: #2a2a2a; font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13px; line-height: normal;">
<li>
<h3><span style="text-align: center; color: #2a2a2a;">22 de abril a 4 de junio: registro para las PAU.</span></h3>
</li>
<li><span style="color: #7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;">La matrícula se realizará del 2 al 4 de Junio y del 3 al 5 de septiembre.</span></li>
</ul>
<ul>
<li>
<h3>Para conseguir el PIN (registro para las PAU) entrar en la siguiente web:</h3>
</li>
</ul>
<h3 style="text-align: center; background-color: #1188ff;"><strong><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank"><span style="color: #ffffff;">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</span></a></strong></h3>
<p> </p>
<ul style="font-size: 13px; text-align: justify; background-color: #e7eaad;">
<li>
<h4><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/documentacion/Parametros_AyB_UA_Bachillerato_2014_2015.pdf" target="_blank">PARÁMETROS DE PONDERACIÓN, Consulta estática.</a></h4>
<p><strong><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/g_b_parametros_top.php" target="_blank">CONSULTA DINÁMICA</a>.</strong></p>
</li>
<li>
<h4><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/" target="_blank">DISTRITO ÚNICO ANDALUZ.</a></h4>
</li>
</ul>
<ul>
<li>
<h4><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI encuentro de la universidad con los centros.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">Presentación: registro para la selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">Presentación: matrícula de selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank">Selectividad: orientaciones.<br /><br /></a></h4>
</li>
<li>
<h4><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Web de la Universidad para registro y matrícula.<br /><br /></a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Otros datos de interés: </a></h4>
</li>
</ul>
<ol><ol>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Inscripción</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Matriculación</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Precios</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Calendario de las pruebas día a día</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Nota de acceso</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Exámenes de otros años y orientaciones</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Orden de preferencia de carreras</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Procedimiento de matrícula en la UIniversidad</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
</ol></ol>
<h4> </h4>
<p><span style="color: #2a2a2a;"> </span></p>";s:4:"body";s:0:"";s:5:"catid";s:3:"108";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-06-20 11:06:25";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":68:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"47";s:8:"ordering";s:1:"1";s:8:"category";s:12:"BACHILLERATO";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:21:"243:selectividad-2014";s:7:"catslug";s:16:"108:bachillerato";s:6:"author";s:10:"Super User";s:6:"layout";s:7:"article";s:4:"path";s:100:"index.php?option=com_content&view=article&id=243:selectividad-2014&catid=108:bachillerato&Itemid=147";s:10:"metaauthor";N;s:6:"weight";d:1.8666199999999997;s:7:"link_id";i:1023;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"BACHILLERATO";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"BACHILLERATO";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=243";s:5:"route";s:100:"index.php?option=com_content&view=article&id=243:selectividad-2014&catid=108:bachillerato&Itemid=147";s:5:"title";s:17:"Selectividad 2014";s:11:"description";s:2405:"ZONA DEDICADA A LA SELECTIVIDAD DE 2014 LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/14 (MARTES) POR LA TARDE EN ESTE ENLACE. EXÁMENES QUE HAN SALIDO EN JUNIO 2014 DE LAS DISTINTAS MATERIAS: JUNIO 2014 - EXÁMENES Criterios de correción oficiales. (19/06/14) Biología Ciencias de la Tierra y Medioambientales Comentario de Texto, Lengua Castellana y Literatura Dibujo Artístico II Dibujo Técnico IIbgcolor="#FEFDD6"> Economía de la Empresa Diseño Electrotecnia Colisión Crit. Correc. Crit. Correc. Crit. Correc.target="_blank"> Colisión Crit. Correc. Crit. Correc. Crit. Correc. Crit. Correc.target="_blank"> Colisión Crit. Correc. Física Geografía Griego II Historia de España Historia de la Filosofía Historia del Arte Latín II Alemánhref="archivos_ies/13_14/selectividad/FISICA_2014_Junio.pdf" target="_blank" style="font-size: inherit; font-weight: normal; text-decoration: none; color: #2f310d;"> Soluciones Crit. Correc. Crit. Correc. Crit. Correc. Crit. Correc./> Crit. Correc. Crit. Correc. Crit. Correc. Crit. Correc.bgcolor="#ffffff"> Lengua Extranjera II (Francés) Lengua Extranjera II (Inglés) Literatura Universal Matemáticas Aplicadas a las Ciencias Sociales II Matemáticas II Química Técnicas de Exp. Gráf. Plást. (no disponible) Tecnología Industrial II Crit. Correc.none; color: #2f310d;"> Crit. Correc. Crit. Correc. Soluciones Crit. Correc.href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;"> Soluciones Crit. Correc. Crit. Correc. Crit. Correc. Crit.Correc. 22 de abril a 4 de junio: registro para las PAU. La matrícula se realizará del 2 al 4 de Junio y del 3 al 5 de septiembre. Para conseguir el PIN (registro para las PAU) entrar en la siguiente web: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp PARÁMETROS DE PONDERACIÓN, Consulta estática. CONSULTA DINÁMICA . DISTRITO ÚNICO ANDALUZ. VI encuentro de la universidad con los centros. Presentación: registro para laselectividad. Presentación: matrícula de selectividad. Selectividad: orientaciones. Web de la Universidad para registro y matrícula. Otros datos de interés: Inscripción Plazos Matriculación Precios Calendario de las pruebas día a día Nota de acceso Exámenes de otros años y orientaciones Orden de preferencia de carreras Procedimiento de matrícula en laUIniversidad Plazos";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2014-04-09 18:23:06";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2014-04-09 18:23:06";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:4;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"262";s:5:"alias";s:23:"selectividad-septi-2015";s:7:"summary";s:31560:"<h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE JUNIO Y SEPTIEMBRE 2014</span></h1>
<ul>
<li><strong style="color: #2a2a2a; font-size: 13px;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/09/14 &nbsp;(Miércoles) EN&nbsp;<a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a; font-size: 13px;">&nbsp;</span><strong style="color: #2a2a2a; font-size: 13px;">EXÁMENES QUE HAN SALIDO EN JUNIO Y SEPTIEMBRE 2014 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000; background-color: #e7eaad;" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Biología</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>CTM</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Comentario de Texto, Lengua&nbsp;</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Dibujo Art. II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Dibujo Téc. II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Economía de la Empresa</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Diseño</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Electrotec.</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td style="text-align: center;">
<p><span style="font-size: 10pt;"><strong>Junio<a href="archivos_ies/13_14/selectividad/Biologia_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" width="35" height="35" align="middle" border="0" /></a> </strong><a href="archivos_ies/13_14/selectividad/Biologia_Colision_Junio2014.pdf" target="_blank" style="font-size: inherit;"><strong>Colisión</strong></a> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" width="35" height="35" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/Lengua_Junio2014.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Junio2014.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a><a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Colision_Junio2014.pdf" target="_blank"><span style="color: inherit; font-family: inherit; text-align: left;">Colisión</span></a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Diseño_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
<p style="text-align: center;"><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Colision_Junio2014.pdf" target="_blank">Colisión</a></strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><span style="font-size: 10pt;">&nbsp;<a href="archivos_ies/13_14/selectividad/biologia_sept_2014.pdf" target="_blank" title="Biología."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/ctm_sept_2014.pdf" target="_blank" title="CTM."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/lengua_sept_2014.pdf" target="_blank" title="Lengua.">Sept.<strong style="color: #000000; text-align: center;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/dt_sept_2014.pdf" target="_blank" title="Dibujo Técnico II."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/economia_sept_2014.pdf" target="_blank" title="Economía."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/electrotecnia_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Electrotecnia.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;">
<p><span style="font-size: 12pt;"><strong>Física</strong></span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 12pt;"><strong>Geografía</strong></span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 12pt;"><strong>Griego II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>H. de España</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>H. Filosofía</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Historia del Arte</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Latín II</strong><strong>&nbsp;</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Lengua Extranjera II (Alemán)</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Fisica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Geografia_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/Griego_AyB.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/H_España_Junio2014.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Filosofia_AyB.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a>&nbsp;</strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;</strong><strong><a href="archivos_ies/13_14/selectividad/H_Arte_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a></strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;</strong><strong><a href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a></strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><span style="font-size: 10pt;"><strong>&nbsp;<a href="archivos_ies/13_14/selectividad/fisica_sept_2014.pdf" target="_blank" title="Física."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a></strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/geografia_sept_2014.pdf" target="_blank" title="Geografía."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong>&nbsp;<a href="archivos_ies/13_14/selectividad/griego_sept_2014.pdf" target="_blank" title="Griego."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></a></strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/historia_sept_2014.pdf" target="_blank" title="Historia de España."><strong style="color: #000000; text-align: -webkit-center; background-color: #ffffff;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/filosofia_sept_2014.pdf" target="_blank" style="text-align: -webkit-center; background-color: #ffffff;" title="Filosofía.">Sept<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong>&nbsp;<a href="archivos_ies/13_14/selectividad/h_arte_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Historia del Arte.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a></strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/latin_sept_2014.pdf" target="_blank" title="Latín."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong>&nbsp;</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td style="text-align: center;" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Lengua Extranjera II (Francés)</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Inglés</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Lit. Uni.</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Mat.Apl.a las CCSS II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Mat. II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong style="text-align: left;">&nbsp;Química</strong><strong>&nbsp;</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>&nbsp;</strong><strong style="text-align: center;">Téc. .Expr. Gráf. Plást.(no disponible)</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Tecnología Industrial II</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td style="text-align: center;">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluc.</strong></a> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;">Soluc.</a></strong><span style="color: inherit; font-family: inherit; text-align: left;">&nbsp;</span></span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong><strong>&nbsp;</strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong style="text-align: left;"><span style="color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </span> </strong><strong>&nbsp;</strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </span><strong style="color: inherit; font-family: inherit; font-size: inherit;">&nbsp;</strong> </span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><a href="archivos_ies/13_14/selectividad/frances_sept_2014.pdf" target="_blank" title="Francés."><span style="font-size: 10pt;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></span></a></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/ingles_sept_2014.pdf" target="_blank" style="text-align: -webkit-center; background-color: #ffffff;" title="Inglés.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/lit_uni_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Literatura Universal.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/mat_ccss_ii_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Matemáticas Aplicadas a las Ciencias Sociales II.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/mat_ii_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Matemáticas II.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/quimica_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Química.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/tec_indus_ii_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Tecnología Industrial II.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<ul>
<li>
<h3>Para ver las notas entrar en la siguiente web:</h3>
</li>
</ul>
<h3 style="background-color: #1188ff;"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></h3>
<p>&nbsp;</p>
<ul>
<li>
<h4><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI encuentro de la universidad con los centros.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">Presentación: registro para la selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">Presentación: matrícula de selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank">Selectividad: orientaciones.<br /><br /></a></h4>
</li>
<li>
<h4><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Web de la Universidad para registro y matrícula.<br /><br /></a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Otros datos de interés:&nbsp;</a></h4>
</li>
</ul>
<ol><ol>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Inscripción</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Matriculación</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Precios</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Calendario de las pruebas día a día</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Nota de acceso</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Exámenes de otros años y orientaciones</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Orden de preferencia de carreras</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Procedimiento de matrícula en la UIniversidad</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
</ol></ol>
<p>&nbsp;</p>
<p><span style="color: #2a2a2a;">&nbsp;</span></p>";s:4:"body";s:0:"";s:5:"catid";s:3:"140";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-09-18 20:34:30";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"37";s:8:"ordering";s:1:"0";s:8:"category";s:24:"Novedades de Secretaría";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:27:"262:selectividad-septi-2015";s:7:"catslug";s:27:"140:novedades-de-secretaria";s:6:"author";s:10:"Super User";s:6:"layout";s:7:"article";s:4:"path";s:106:"index.php?option=com_content&view=article&id=262:selectividad-septi-2015&catid=140:novedades-de-secretaria";s:10:"metaauthor";N;s:6:"weight";d:1.8666199999999997;s:7:"link_id";i:1297;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:24:"Novedades de Secretaría";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:24:"Novedades de Secretaría";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=262";s:5:"route";s:106:"index.php?option=com_content&view=article&id=262:selectividad-septi-2015&catid=140:novedades-de-secretaria";s:5:"title";s:39:"Selectividad Septiembre 2014: EXÁMENES";s:11:"description";s:2013:"ZONA DEDICADA A LA SELECTIVIDAD DE JUNIO Y SEPTIEMBRE 2014 LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/09/14 (Miércoles) EN ESTE ENLACE. EXÁMENES QUE HAN SALIDO EN JUNIO Y SEPTIEMBRE 2014 DE LAS DISTINTAS MATERIAS: Biología CTM Comentario de Texto, Lengua Dibujo Art. II Dibujo Téc. II Economía de la Empresa Diseño Electrotec.bgcolor="#ffffff"> Junio Colisión Junio Junio Juniocursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> Colisión Junio Junio Juniotarget="_blank"> Junio Colisión Sept. Sept. Sept.10pt;"> Sept. Sept. Sept. Física Geografía Griego IIbgcolor="#FEFDD6"> H. de España H. Filosofía Historia del Arte Latín II Lengua Extranjera II (Alemán) Junio Junio JunioJunio Junio Juniostyle="text-decoration: underline;"> Junio Junio Sept. Sept.align="center"> Sept. Sept. Sept Sept.title="Latín."> Sept. Lengua Extranjera II (Francés) Inglés Lit. Uni. Mat.Apl.a las CCSS II Mat. II Química Téc. .Expr. Gráf. Plást.(no disponible) Tecnología Industrial IIcolor: #2f310d;"> Junio Junio Junio JunioSoluc. Junio Soluc. Junio JunioJunio Sept. Sept. Sept.style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> Sept. Sept. Sept. Sept.align="middle" border="0" /> Para ver las notas entrar en la siguiente web: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp VI encuentro de la universidad con los centros. Presentación: registro para la selectividad. Presentación: matrícula de selectividad. Selectividad: orientaciones. Web de la Universidad para registro y matrícula. Otros datos de interés: Inscripción Plazos Matriculación Precioshref="index.php?option=com_content&view=article&id=139&Itemid=218" target="_blank"> Calendario de las pruebas día a día Nota de acceso Exámenes de otros años y orientaciones Orden de preferencia de carreras Procedimiento de matrícula en la UIniversidad Plazos";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2014-04-09 18:23:06";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2014-04-09 18:23:06";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:5;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"219";s:5:"alias";s:25:"ies-miguel-de-cervantes-2";s:7:"summary";s:34726:"<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Pendientes de Matemáticas.</span></h2>
<p style="padding-left: 30px;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;"><a href="archivos_ies/curso1314/HORARIO_INICIO_CURSO_2013-14.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></span></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Historia de Grecia, 1º de Bachillerato (23/09/13)</span></h2>
<p style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff; padding-left: 30px;"><a href="https://www.youtube.com/watch?v=cyvNgDMZEdw" target="_blank" style="color: #1155cc;">Ver vídeo</a></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Actualizado el temario de Matemáticas de 1º y 3º ESO bilingües para poder imprimirlo.</span></h2>
<p style="padding-left: 30px; color: #2e2a23; text-align: justify; background-color: #fbe9c5;"><a href="index.php?option=com_content&amp;view=article&amp;id=171&amp;Itemid=239" target="_blank" style="color: #1155cc;">Entrar en zona privada de bilingües.</a></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Horario de inicio de curso, día 16/09/13</span></h2>
<p style="margin: 0cm 0cm 6pt; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><a href="archivos_ies/curso1314/HORARIO_INICIO_CURSO_2013-14.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Decreto Formación Inicial y Permanente</span></h2>
<p style="margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff; line-height: 12pt;" align="left">Decreto 93/2013, de 27 de agosto, por el que se regula la formación inicial y permanente del profesorado en la Comunidad Autónoma de Andalucía, así como el Sistema Andaluz de Formación Permanente del Profesorado.</p>
<p style="margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff; line-height: 12pt;" align="left"><a href="archivos_ies/13_14/ver_decreto_39827.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></p>
<p style="margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff; line-height: 12pt;" align="left"> </p>
<p style="margin: 0cm 0cm 0.0001pt; text-align: justify; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><strong><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;">Orden de 5 de julio de 2013, que regula las ausencias del profesorado por enfermedad o accidente que no den lugar a incapacidad temporal</span></strong></p>
<p style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"> </p>
<p style="margin: 0cm 0cm 0.0001pt; padding-left: 30px; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><a href="archivos_ies/13_14/ver_orden_90644.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></p>
<p style="margin: 0cm 0cm 0.0001pt; padding-left: 30px; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><strong style="text-align: justify;"><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;"> </span></strong></p>
<p style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><strong style="text-align: justify;"><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;">Día lectivos y festivos en el Curso Escolar 2013/14</span></strong></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><a href="archivos_ies/13_14/Calendario_2013-2014.pdf" target="_blank" style="color: #1155cc;"><br />Ver enlace</a></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"> </p>
<p style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><strong style="text-align: justify;"><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;">Legislación actualizada a 05/09/13</span></strong></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><a href="archivos_ies/13_14/lex" target="_blank" style="color: #1155cc;"><br />Ver enlace</a></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"> </p>
<table style="border: 0px solid #efbc38; width: 100%; background-color: #ffffff;" border="0" cellspacing="5" cellpadding="5" align="center">
<tbody>
<tr style="border-color: #8799f9; border-bottom: 1px solid;" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="3" align="center"><span style="color: #ffffff;"><strong><span style="font-size: 12pt;">CURSO </span></strong><strong><span style="font-size: 12pt;"> 2012/13</span></strong></span></td>
</tr>
<tr>
<td class="textogris" style="text-align: center; border-color: #8799f9;" colspan="2">
<ul>
<li style="text-align: justify;"><span style="color: #484137;"><strong>20/02/13.  <a href="http://paleorama.wordpress.com/2013/02/19/entrevista-al-filologo-antoni-biosca-sobre-la-importancia-del-latin/" target="_blank">Entrevista al filólogo Antoni Biosca sobre la importancia del Latín.</a></strong></span></li>
<li style="text-align: justify;"><span style="color: #484137;"><strong>07/02/13.  <a href="index.php?option=com_content&amp;view=article&amp;id=193">Publicada nueva normativa en materia de permisos y licencias.</a></strong></span></li>
<li style="text-align: justify;"><strong>07/02/13:  Excelente web para <a href="index.php?option=com_content&amp;view=article&amp;id=194" target="_self">aprender jugando</a>, todas las asignaturas, todos los niveles.</strong></li>
<li style="text-align: justify;"><span style="color: #484137;"><strong>05/02/13.  </strong></span>Hoy, 5 de febrero, se celebra el <a href="http://www.osi.es/es/actualidad/blog/2013/02/05/conectate-y-respeta-netiquetate" target="_blank">Día Internacional de la Internet Segura. </a></li>
<li style="text-align: justify;"><span style="color: #484137;"><strong>04/02/13.   </strong></span><a href="archivos_ies/1_Ev_Analisiscuantitativo_2012_13.pdf" target="_blank"><strong>Datos, 1ª Evaluación 2012/13.</strong></a></li>
<li style="text-align: justify;"><span><strong>16/01/13. </strong></span><strong><a href="archivos_ies/PROGRAMA_ACE_POR_TRIMESTRES.xlsx" target="_blank" style="color: #655c4e;">Programa de las Actividades Complementarias y Extraescolares por trimestres.</a></strong></li>
<li style="text-align: justify;"><span style="text-decoration: underline;"><strong><span style="color: #0066cc; text-decoration: underline;">08/01/13.</span></strong></span><span style="color: #0066cc;">  </span><span style="text-decoration: underline;"><strong><span style="color: #0066cc; text-decoration: underline;">REANUDACIÓN</span><span style="color: #0066cc; text-decoration: underline;"> DE LAS CLASES EN SU HORARIO NORMAL, A PARTIR DE LAS 08.15.</span></strong></span></li>
<li style="text-align: justify;"></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">13/12/12.  </span></strong><strong><span style="color: #0066cc;"><a href="archivos_ies/TENSION_EN_LAS_AULAS.pdf">TENSION EN LAS AULAS.</a> Aportación del Departamento de Latín y Griego.</span></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">07/11/12. </span></strong><span style="color: #0066cc;"><strong><a href="archivos_ies/ENSENANZA_Y_CONOCIMIENTO.doc" target="_blank">ENSEÑANZA Y CONOCIMIENTO.</a> Aportación del Departamento de Latín y Griego.</strong></span></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">23/10/12. </span><a href="http://www.piiisa.es/" target="_blank">PROYECTO PIIISA 2012: Ciencia para jóvenes estudiantes  www.piiisa.es</a></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">16/09/12. <a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_examenes_anteriores.php" target="_blank">ORIENTACIONES OFICIALES DE LA UNIVERSIDAD SELECTIVIDAD 2012/13.</a></span></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">15/09/12</span></strong>. <a href="archivos_ies/lex/" target="_blank">LEGISLACIÓN.</a></li>
<li style="text-align: left;"><strong style="color: #339966; text-align: justify;">11/09/12.</strong> <a href="archivos_ies/Manual_funcionamiento_SGD.pdf" target="_blank"><strong style="color: #339966; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: justify;">Manual de funcionamiento de la unidad personal de SGD</strong>.</a> (Para el profesorado)</li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">29/06/12.  <a href="archivos_ies/libros_1_bach_2012-2013corregido.pdf" target="_blank" style="text-decoration: none; color: #0066cc;">Libros de 1º Bachillerato para el curso 2012/13.</a></span></strong></li>
<li style="text-align: justify;"><strong><strong><span style="color: #0066cc;">29/06/12.  <span style="color: #0066cc;"><a href="archivos_ies/libros_2_bach_2012-2013.pdf" style="text-decoration: none; color: #0066cc;">Libros de 2º Bachillerato para el curso 2012/13.</a></span></span></strong> </strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">25/06/12.</span> <a href="archivos_ies/Calendario_escolar_2012_13.pdf" target="_blank" style="text-decoration: none;"><span style="color: #0066cc;">Calendario Escolar 2012/13.</span></a></strong></li>
<li style="text-align: justify;"><strong><strong><span style="color: #0066cc;">25/06/12. </span> <a href="archivos_ies/Instrucciones_calendario_escolar_2012_13.pdf" target="_blank" style="text-decoration: none;"><span style="color: #0066cc;">Instrucciones del Calendario Escolar 2012/13.</span></a></strong> </strong></li>
<li style="text-align: justify;"><strong>22/06/12. </strong><a href="archivos_ies/Examenes_selectividad_junio_2012_oficiales.pdf" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Exámenes que han salido en Selectividad junio 2012.</a></li>
<li style="text-align: justify;"><a href="archivos_ies/PROYECTO_EDUCATIVO_IES_MIGUEL_DE_CERVANTES_definitivo_2011_12.pdf" target="_blank" title="PROYECTO EDUCATIVO IES MIGUEL DE CERVANTES 2011-12." style="text-decoration: none; color: #c6c37b; text-align: center;">ACTUALIZADO: PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES 2011-12.</a></li>
<li style="text-align: justify;"><span style="font-size: 10pt;"><strong>Consulta de los libros de la Biblioteca:   <a href="abiex/index.php" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Formato 1</a> -   <a href="tmp/index.php" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Formato 2</a> - </strong><a href="libros/index.php" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Formato 3</a></span></li>
<li style="text-align: justify;"><span style="font-size: 10pt;"><strong>Enlaces a <a href="index.php?option=com_content&amp;view=article&amp;id=153&amp;Itemid=231" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">bibliotecas y libros virtuales e interactivos.</a></strong></span></li>
<li style="text-align: justify;"><span style="font-size: 10pt;"><strong>Actualizados los criterios de evaluación, consultar los Departamentos.</strong></span></li>
<li style="text-align: justify;"><span style="font-size: 10pt;">Criterios que falten de 2º de Bachiller sobre Selectividad, consultar <a href="http://www.ujaen.es/" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">www.ujaen.es</a></span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<p> </p>
<table style="border: 0px solid #efbc38; width: 100%; background-color: #ffffff;" border="0" cellspacing="3" cellpadding="1" align="center">
<tbody>
<tr style="border-color: #8799f9; border-bottom: 1px solid;" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; text-align: center;" align="center"><span style="color: #ffffff;"><strong><span style="font-size: 12pt;"> Carteles sobre el invierno (Maestros y aprendices)</span></strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9;">
<td class="textogris" style="text-align: center; border-color: #8799f9;">
<div style="text-align: center;" align="center;">
<table border="0" align="center">
<tbody>
<tr>
<td>
<div style="text-align: center;" align="center;"><strong><span style="font-family: book antiqua,palatino; font-size: 12pt;">Cartel invierno</span></strong></div>
<div style="text-align: right;"><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Cartel_invierno.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Cartel_invierno.jpg" border="0" alt="Cartel invierno." width="179" height="117" /></a></span></div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Horacio (latín)</strong></span></div>
<div style="text-align: center;"><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Horacio_latin.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Horacio.jpg" border="0" alt="Horacio." width="164" height="127" /></a></span></div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong> Irene Centeno </strong></span></div>
<div style="text-align: center;"><span style="font-family: book antiqua,palatino; font-size: 12pt;"> <a href="archivos_ies/lengua/invierno/Irene_Centeno.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Irene_Centeno.jpg" border="0" alt="Irene Centeno" width="195" height="86" /></a></span></div>
<div style="text-align: center;"> </div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Paul Verlain 1</strong></span></div>
<div style="text-align: center;"><span style="text-align: center;"><a href="archivos_ies/lengua/invierno/Paul_Verlain_1.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/Paul_Verlain_1.jpg" border="0" alt="Paul Verlain 1." width="136" height="159" /></a></span></div>
<div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Paul Verlain 2</strong></span></div>
<div style="text-align: center;"><span style="text-align: center;"><a href="archivos_ies/lengua/invierno/Paul_Verlain_2.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Paul_Verlain_2.jpg" border="0" alt="Paul Verlain 2." width="197" height="79" /></a></span></div>
</div>
</td>
<td>
<div>
<div style="text-align: center;"><strong><span style="font-family: book antiqua,palatino; font-size: 12pt;">Jardín de invierno (P. Neruda)</span></strong></div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Jardin_de_invierno_P_Neruda.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Jardin_de_invierno.jpg" border="0" alt="Jardín de invierno." width="181" height="118" /></a></span></div>
</div>
<div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Maite Luzón</strong></span></div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Maite_Luzon.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/Maite_Luzon.jpg" border="0" alt="Maite Luzón." width="184" height="123" /></a></span></div>
</div>
<div><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>María Fernández</strong></span></div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Maria_Fernandez.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Maria_Fernandez.jpg" border="0" alt="Maria Fernández." width="196" height="114" /></a></span></div>
<div><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Proverbios (Francés)</strong></span></div>
<div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Proverbios _Frances.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Proverbios_Frances.jpg" border="0" alt="Proverbios (Francés)." width="183" height="111" /></a></span></div>
<div>
<div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Snow dreams (inglés)</strong></span></div>
<span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Snow_dreams_ingles.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Snow_dreams.jpg" border="0" alt="Snow dreams." width="176" height="106" /></a> </span></div>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
<div><br />
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="3" cellpadding="2" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid;" align="center" valign="middle" bgcolor="#c78d74">
<td style="text-align: center; border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">VÍDEO DEL VIAJE A BROOKLYN, NUEVA YORK, ABRIL 2011</span></strong></strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9 #8799f9 currentColor; border-bottom-width: 1px; border-bottom-style: solid;">
<td class="textogris" style="border-color: #8799f9; text-align: center;">
<div><span style="font-size: 12px;"><strong> <a href="http://www.youtube.com/watch?v=xiFxyNxAQR0" target="_blank"><img src="archivos_ies/CollageNY.jpg" border="0" alt="Vídeo del viaje a Brooklyn 2011." /></a> </strong></span><strong><img class="caption" src="images/stories/cervantes/qr_viaje_nueva_york.jpg" border="0" alt="Código qr del viaje a Nueva York. Leerlo con la cámara del móvil." title="Código qr del viaje a Nueva York. Leerlo con la cámara del móvil." style="border: 0;" /></strong></div>
</td>
</tr>
</tbody>
</table>
</div>
<div id="tituloItem" class="blockTituloItem" style="display: block;">
<div class="tituloIzq" style="float: left;">
<div><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; letter-spacing: 1pt; color: #71cd63;"> </span></div>
</div>
</div>
<div class="blockTituloItem" style="display: block;"> </div>
<div id="contenido_sitio" style="width: 100%; float: left; display: block;">
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="0" cellpadding="1" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;" align="center" valign="middle" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">DOCUMENTOS DEL PLAN DE CENTRO</span></strong> </strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9; border-bottom: 1px solid;">
<td class="textogris" style="text-align: right; border-color: #8799f9;">
<div style="text-align: left;">
<ul style="color: #151509; margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; list-style-type: none; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"><a href="archivos_ies/PROYECTO_EDUCATIVO_IES_MIGUEL_DE_CERVANTES_definitivo_2011_12.pdf" target="_blank" title="PROYECTO EDUCATIVO IES MIGUEL DE CERVANTES 2011-12." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES.</a></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PROYECTO_DE_GESTION_DEFINITIVO.pdf" target="_blank" title="PROYECTO DE GESTIÓN." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PROYECTO DE GESTIÓN.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/Plan_de_convivencia_definitivo.pdf" target="_blank" title="Plan de Convivencia." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PLAN DE CONVICENCIA.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/ROF_nuevo_2011_definitivo.pdf" target="_blank" title="ROF nuevo 2011." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">NUEVO ROF 2011.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PLAN_DE_ORIENTACION_Y_ACCION_TUTORIAL_definitivo.pdf" target="_blank" title="PLAN DE ORIENTACIÓN Y ACCIÓN." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PLAN DE ORIENTACIÓN Y ACCIÓN TUTORIAL.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PLAN_DE_FORMACION_MODELO_IES_CERVANTES_definitivo.pdf" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PLAN DE FORMACIÓN DEL IES MIGUEL DE CERVANTES.</a></span></li>
</ul>
</div>
<div style="text-align: left;"> </div>
</td>
</tr>
</tbody>
</table>
<br />
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="0" cellpadding="1" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;" align="center" valign="middle" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">ESCUELA TIC 2.0</span></strong> </strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9; border-bottom: 1px solid;">
<td class="textogris" style="text-align: right; border-color: #8799f9;">
<div style="text-align: left;">
<table style="margin: 1px; width: 100%; text-align: center; border-collapse: collapse;" border="0" cellspacing="2">
<tbody style="text-align: center;">
<tr style="text-align: center;">
<td style="vertical-align: top; text-align: center; padding: 2px; border: 0px solid #9a9542;">
<ul style="color: #129d0e; margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 2em; font-family: 'comic sans ms', sans-serif; font-size: 12px; line-height: 15px; list-style-type: none; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;">
<div style="text-align: left;"><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'Comic Sans MS'; color: #810081; font-size: 10pt;"><a href="http://blogsaverroes.juntadeandalucia.es/escuelatic20/" target="_blank" title="Escuela TIC 2.0" style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">ESCUELA TIC 2.0</a></span></strong><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><br /></strong></div>
</li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'Comic Sans MS'; color: #810081; font-size: 10pt;"><a href="http://www.juntadeandalucia.es/averroes/mochiladigitalESO/" target="_blank" title="Mochila digital." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">MOCHILA DIGITAL SECUNDARIA<br /></a></span></strong></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;">
<div style="text-align: left;"><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'Comic Sans MS'; color: #810081; font-size: 10pt;"><a href="http://www.cepgranada.org/~inicio/pf_login.php?4,60,,,,,,," target="_blank" title="Curso del CEP de Granada" style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">CURSOS CEP DE GRANADA</a></span></strong></div>
</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
<br />
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="0" cellpadding="1" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;" align="center" valign="middle" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">NOTICIAS</span></strong></strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9; border-bottom: 1px solid;">
<td class="textogris" style="text-align: right; border-color: #8799f9;">
<div style="text-align: left;"> </div>
<div style="text-align: left;">
<ul>
<li style="color: #151509;">Blog del <span style="font-size: 12px; color: #000000;"><a href="http://letsknoweachotherbetter.blogspot.com/" target="_blank">Viaje a Cumbria</a>, Inglaterra, 14 al 23 de junio de 2011.</span></li>
</ul>
<ul>
<li style="color: #151509;">Actividad extraescolar: "<a href="archivos_ies/Los_sitios_de_Mariana_de_Pineda.pdf" target="_blank">LOS SITIOS DE MARIANA PINEDA</a>", un recorrido por la ciudad de Granada: Departamento de Geografía e Historia.
<ul>
<li style="color: #151509;">26/05/2011: <a href="index.php/departamentos26/humanidesdes/dep-geografia/#mariana">añadidas 6 fotografías de la actividad.</a></li>
</ul>
</li>
<li><span style="font-family: arial, helvetica, sans-serif;">2º Bachillerato B. <a href="archivos_ies/Master2011.pdf" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Master de Secundaria y Bachillerato 2011</a>.</span></li>
<li><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><span style="text-decoration: underline;">31 de marzo de 2011</span>:  ENCUENTROS LITERARIOS EN EL INSTITUTO. <a href="index.php?option=com_content&amp;view=article&amp;id=132" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Andrés Neuman</a> estuvo con el alumnado de 2º de Bachillerato</span></li>
<li><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: 10pt;"><span style="text-decoration: underline;">9 de marzo:</span> <strong><span style="text-decoration: underline;"><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=132&amp;Itemid=188" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Día Internacional de la Mujer</a></span></strong>: se están realizando numerosas actividades en el centro durante dos semanas y se celebra la FERIA INTERCULTURAL DE LAS TAREAS DOMÉSTICAS.</span></li>
<li><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: 10pt;"><span style="text-decoration: underline;">8 de febrero:</span> <a href="archivos_ies/DIA_INTERNACIONAL_INTERNET_SEGURO.pdf" target="_blank" title="Día Internacioanl de Internet seguro." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">Día internacional de internet seguro.</a></span></li>
</ul>
</div>
</td>
</tr>
</tbody>
</table>
</div>
<p> </p>";s:4:"body";s:0:"";s:5:"catid";s:2:"37";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-09-04 07:07:51";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"0";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"0";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"0";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"0";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"0";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"0";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"0";s:15:"show_print_icon";s:1:"0";s:15:"show_email_icon";s:1:"0";s:9:"show_hits";s:1:"0";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:14:"Miguel Anguita";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:1:"4";s:8:"ordering";s:1:"7";s:8:"category";s:15:"Datos generales";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:29:"219:ies-miguel-de-cervantes-2";s:7:"catslug";s:18:"37:datos-generales";s:6:"author";N;s:6:"layout";s:7:"article";s:4:"path";s:99:"index.php?option=com_content&view=article&id=219:ies-miguel-de-cervantes-2&catid=37:datos-generales";s:10:"metaauthor";s:14:"Miguel Anguita";s:6:"weight";d:1.5866199999999999;s:7:"link_id";i:1123;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:15:"Datos generales";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:15:"Datos generales";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=219";s:5:"route";s:99:"index.php?option=com_content&view=article&id=219:ies-miguel-de-cervantes-2&catid=37:datos-generales";s:5:"title";s:37:"IES Miguel de Cervantes - Granada (2)";s:11:"description";s:4499:"Pendientes de Matemáticas. Ver enlace Historia de Grecia, 1º de Bachillerato (23/09/13) Ver vídeo Actualizado el temario de Matemáticas de 1º y 3º ESO bilingües para poder imprimirlo. Entrar en zona privada de bilingües.font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"> Horario de inicio de curso, día 16/09/13 Ver enlace Decreto Formación Inicial y Permanente Decreto 93/2013, de 27 de agosto, por el que se regula la formación inicial y permanente del profesorado en la Comunidad Autónoma de Andalucía, así como el Sistema Andaluz de Formación Permanente del Profesorado. Ver enlace#ffffff;"> Orden de 5 de julio de 2013, que regula las ausencias del profesorado por enfermedad o accidente que no den lugar a incapacidad temporal Ver enlace Día lectivos y festivos en el Curso Escolar 2013/14 Ver enlacestyle="text-align: justify;"> Legislación actualizada a 05/09/13 Ver enlace CURSO 2012/13 20/02/13. Entrevista al filólogo Antoni Biosca sobre la importancia del Latín. 07/02/13. Publicada nueva normativa en materia de permisos y licencias. 07/02/13: Excelente web parahref="index.php?option=com_content&view=article&id=194" target="_self"> aprender jugando , todas las asignaturas, todos los niveles. 05/02/13. Hoy, 5 de febrero, se celebra el Día Internacional de la Internet Segura. 04/02/13. Datos, 1ª Evaluación 2012/13. 16/01/13. Programa de las Actividades Complementarias y Extraescolares por trimestres. 08/01/13. REANUDACIÓN DE LAS CLASES EN SU HORARIO NORMAL, A PARTIR DE LAS 08.15. 13/12/12. TENSION EN LAS AULAS. Aportación del Departamento de Latín y Griego. 07/11/12. ENSEÑANZA Y CONOCIMIENTO. Aportación del Departamento de Latín yGriego. 23/10/12. PROYECTO PIIISA 2012: Ciencia para jóvenes estudiantes www.piiisa.es 16/09/12. ORIENTACIONES OFICIALES DE LA UNIVERSIDAD SELECTIVIDAD 2012/13. 15/09/12 . LEGISLACIÓN. 11/09/12. Manual de funcionamiento de la unidad personal de SGD . (Para el profesorado) 29/06/12. Libros de 1º Bachillerato para el curso 2012/13. 29/06/12. Libros de 2º Bachillerato para el curso 2012/13. 25/06/12. Calendario Escolar 2012/13. 25/06/12.Instrucciones del Calendario Escolar 2012/13. 22/06/12. Exámenes que han salido en Selectividad junio 2012. ACTUALIZADO: PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES 2011-12. Consulta de los libros de la Biblioteca: Formato 1 - Formato 2 - Formato 3 Enlaces a bibliotecas y libros virtuales e interactivos. Actualizados los criterios de evaluación, consultar los Departamentos. Criterios que falten de 2º de Bachiller sobre Selectividad, consultarstyle="text-decoration: none; color: #c6c37b; text-align: center;"> www.ujaen.es Carteles sobre el invierno (Maestros y aprendices) Cartel invierno Horacio (latín) Irene Centenostyle="text-align: center;"> Paul Verlain 1 Paul Verlain 2 Jardín de invierno (P. Neruda) Maite Luzónsrc="archivos_ies/lengua/invierno/Maite_Luzon.jpg" border="0" alt="Maite Luzón." width="184" height="123" /> María Fernández Proverbios (Francés) Snow dreams (inglés)border-bottom-style: solid;" colspan="4" align="center"> VÍDEO DEL VIAJE A BROOKLYN, NUEVA YORK, ABRIL 2011style="color: #ffffff;"> DOCUMENTOS DEL PLAN DE CENTRO PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES. PROYECTO DE GESTIÓN.url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"> PLAN DE CONVICENCIA. NUEVO ROF 2011. PLAN DE ORIENTACIÓN Y ACCIÓN TUTORIAL.no-repeat;"> PLAN DE FORMACIÓN DEL IES MIGUEL DE CERVANTES. ESCUELA TIC 2.0padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"> ESCUELA TIC 2.0 MOCHILA DIGITAL SECUNDARIAleft;"> CURSOS CEP DE GRANADA NOTICIAS Blog del Viaje a Cumbria , Inglaterra, 14 al 23 de junio de 2011. Actividad extraescolar: " LOS SITIOS DE MARIANA PINEDA ", un recorrido por la ciudadde Granada: Departamento de Geografía e Historia. 26/05/2011: añadidas 6 fotografías de la actividad. 2º Bachillerato B. Master de Secundaria y Bachillerato 2011 . 31 de marzo de 2011 : ENCUENTROS LITERARIOS EN EL INSTITUTO. Andrés Neuman estuvo con el alumnado de 2º de Bachillerato 9 de marzo: Día Internacional de la Mujer : se están realizando numerosas actividades en el centro durante dos semanas y se celebra la FERIA INTERCULTURAL DE LAS TAREAS DOMÉSTICAS. 8 de febrero: Día internacional de internetseguro.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2004-07-30 19:00:00";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2009-09-15 00:00:00";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:6;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:27:{s:2:"id";s:3:"144";s:5:"alias";s:10:"documentos";s:7:"summary";s:14393:"<div>
<h3 style="text-align: left;"><span style="text-decoration: underline;"><span style="color: #663300;"><strong>DOCUMENTOS RELACIONADOS CON LA JEFATURA DE ESTUDIOS</strong></span></span></h3>
<ul>
<li><strong><span style="color: #663300;">Alumnos y Padres de Alumnos</span></strong></li>
<ul>
<li><a href="archivos_ies/Faltasalumnos.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #663300; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px; background-color: #f8f8f8;">Justificación de faltas y autorización para ausentarse del Centro.</span></span></a></li>
<li><a href="archivos_ies/justificacion_de_faltas.docx" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #663300; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px; background-color: #f8f8f8;">Solicitud de Justificación de Ausencias de Alumnos</span></span></a></li>
<li><a href="archivos_ies/reclamaciones_de_las_calificaciones_finales.docx" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #663300; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px; background-color: #f8f8f8;">Procedimiento Reclamaciones Calificaciones Finales</span></span><br /></a></li>
<li><a href="archivos_ies/Plano_Centro_2012-2013.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #663300; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px; background-color: #f8f8f8;">Ubicación de las aulas</span></span></a></li>
</ul>
</ul>
<ul>
<li><strong><span style="color: #663300;"> Departamentos Didácticos</span></strong></li>
<ul>
<li><a href="archivos_ies/memoria_de_departamento_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><strong><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><span style="color: #663300;"><span style="font-weight: normal; background-color: #f8f8f8;">Memoria de Departamento rellenable</span></span><br /></strong></strong></a></li>
<li><strong><a href="archivos_ies/coordinacion_de_la_programacin_eso_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Seguimiento programación en la ESO</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/coordinacion_de_la_programacin_bachillerato_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Seguimiento programación en Bachillerato</a></strong></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/programacion_de_aula_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Programación de Aula</a></strong></strong></strong></li>
</ul>
</ul>
<ul>
<li><strong> </strong><strong><span style="color: #663300;">Personal del Centro</span></strong></li>
<ul>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/circular_permisos_y_licencias.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Circular de Licencias y Permisos</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/instruccion_2_2013.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Instrucción 2/2013, puesta en práctica medidas fiscales</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/solicitud_propuesta_de_abono.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Solicitud propuesta de abono incapacidad temporal</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/cuadro_resumen_permisos_y_licencias.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Cuadro resumen de permisos y licencias</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/grados_de_parentesco.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Grados de parentesco</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/solicitud_permisos_y_licencias_RELLENABLE.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Solicitud de Permisos y Licencias Personal (rellenable)</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/FaltasdelPersonal.docx" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Justificación de faltas del profesorado y PAS </a></strong></li>
<li></li>
</ul>
<li><strong><span style="color: #663300;">Guías de evaluación de las Competencias básicas</span></strong></li>
<ul>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_competencias_basicas_social.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Competencia básica Social y Ciudadana</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_competencias_basicas_lengua.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Comunicación lingüística (lengua española)</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_competencias_basicas_lengua_extranjera.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Comunicación lingüística (lenguas extranjeras)</a></span></strong></strong></span></span></li>
<li><a href="archivos_ies/guia_evaluacion_competencias_basicas_conocimiento.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><span style="font-weight: normal; background-color: #f8f8f8;">Conocimiento e interacción con el mundo físico y natural</span></span></strong></strong></span></span></a></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_competencias_basicas_matematicas.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Razonamiento matemático</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_destrezas_lectoras.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Guía de evaluación de destrezas lectoras</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_indicadores_homologados.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Guías de Indicadores Homologados: Autoevaluación de centros docentes de Andalucía</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/pisa_2009.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Informe PISA 2009</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/informe-espanol-panorama-educacion-ocde.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Indicadores de la OCDE 2009. Informe español</a></span></strong></strong></span></span></li>
<li></li>
</ul>
</ul>
</div>
<div>
<div>
<ul>
<li><strong><span style="color: #663300;">Tutorías</span></strong><span> </span></li>
<ul>
<li><span><a href="archivos_ies/memoria_de_tutora_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-size: 12px; letter-spacing: normal; line-height: 18px; text-align: start; background-color: #f8f8f8;">Memoria de Tutoría</a></span></li>
</ul>
</ul>
</div>
<div> </div>
<div style="text-align: -webkit-auto;"> </div>
</div>
<div class="fechaModif"> </div>";s:4:"body";s:0:"";s:5:"catid";s:3:"241";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2013-10-01 17:20:38";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":71:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"0";s:11:"link_titles";i:0;s:10:"show_intro";i:0;s:13:"show_category";i:0;s:13:"link_category";i:0;s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";i:0;s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";i:0;s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";i:0;s:15:"show_email_icon";i:0;s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";s:12:"show_section";i:0;s:12:"link_section";i:0;s:13:"show_pdf_icon";i:0;}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"23";s:8:"ordering";s:1:"1";s:8:"category";s:20:"Jefatura de Estudios";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:14:"144:documentos";s:7:"catslug";s:24:"241:jefatura-de-estudios";s:6:"author";N;s:4:"mime";N;s:6:"layout";s:7:"article";s:4:"path";s:101:"index.php?option=com_content&view=article&id=144:documentos&catid=241:jefatura-de-estudios&Itemid=609";s:10:"metaauthor";N;s:6:"weight";d:0.79330999999999996;s:7:"link_id";i:656;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:20:"Jefatura de Estudios";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:20:"Jefatura de Estudios";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=144";s:5:"route";s:101:"index.php?option=com_content&view=article&id=144:documentos&catid=241:jefatura-de-estudios&Itemid=609";s:5:"title";s:10:"Documentos";s:11:"description";s:1881:"DOCUMENTOS RELACIONADOS CON LA JEFATURA DE ESTUDIOS Alumnos y Padres de Alumnos Justificación de faltas y autorización para ausentarse del Centro. Solicitud de Justificación de Ausencias de Alumnos Procedimiento Reclamaciones Calificaciones Finalesletter-spacing: normal; text-align: start; background-color: #f8f8f8;"> Ubicación de las aulas Departamentos Didácticos Memoria de Departamento rellenable Seguimiento programación en la ESO Seguimiento programación en Bachilleratofont-size: 12px; line-height: 18px; background-color: #e5e5e5;"> Programación de Aula   Personal del Centro Circular de Licencias y Permisos Instrucción 2/2013, puesta en práctica medidas fiscales#663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"> Solicitud propuesta de abono incapacidad temporal Cuadro resumen de permisos y licencias Grados de parentesco Solicitud de Permisos y Licencias Personal (rellenable)background-color: #f8f8f8;"> Justificación de faltas del profesorado y PAS  Guías de evaluación de las Competencias básicas Competencia básica Social y Ciudadana Comunicación lingüística (lengua española) Comunicación lingüística (lenguas extranjeras)href="archivos_ies/guia_evaluacion_competencias_basicas_conocimiento.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"> Conocimiento e interacción con el mundo físico y natural Razonamiento matemático Guía de evaluación de destrezas lectoras#663300;"> Guías de Indicadores Homologados: Autoevaluación de centros docentes de Andalucía Informe PISA 2009 Indicadores de la OCDE 2009. Informe español Tutorías   Memoria deTutoría      ";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2011-01-08 20:48:01";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2011-01-08 20:48:01";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:7;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"195";s:5:"alias";s:14:"programaciones";s:7:"summary";s:23149:"<h2 style="text-align: center;"><span style="font-size: 16pt;"><strong><span style="text-decoration: underline;"><br />Programaciones ABREVIADAS, criterios de evaluación, recuperaciones. </span></strong></span></h2>
<h2 style="text-align: center;"><span style="font-size: 16pt;"><strong><span style="text-decoration: underline;">curso 2015/16</span></strong><br /></span></h2>
<h3><span style="text-decoration: underline;"><strong>BIOLOGÍA - GEOLOGÍA 15/16</strong></span></h3>
<ul style="text-align: justify;">
<li><a href="archivos_ies/15_16/programaciones/BG_Programaciones_2015-2016-resumida.pdf" target="_blank"><span style="color: #2a2a2a;">Resumen de las programaciones del Departamento. </span></a><span style="color: #2a2a2a;">(03/11/15)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;"><strong>CICLO DE ANIMACIÓN</strong></strong></span><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;"><strong><br /></strong></strong></p>
<ul>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Progr_OTL_14-15_EMT.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN OTL 14-15 EMT</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Prog_PROYECTO_14_15_EMT.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN PROYECTO 14-15 EMT</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Prog_Modulo_FCT_2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN MÓDULO FCT 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_SSA.doc" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN SSA 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_FCT.docx" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN FCT 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_DCO.docx" target="_blank" style="text-decoration: none; color: #2f310d;">ProgramacióN DCO 14-15</a></span><span style="color: #7b7b7b;"> </span><span style="color: #7b7b7b;"><span style="color: #414141;">(20/10/14)</span></span></li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_MIS.docx" target="_blank" style="text-decoration: none; color: #2f310d;">ProgramacióN MIS 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Prgrogramacion_Departamento_ASC.docx" target="_blank" style="text-decoration: none; color: #2f310d;">PROGRAMACIÓN DEP. ASC 14-15</a> (20/10/14)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">EDUCACIÓN FÍSICA 15/16<br /></strong></span></p>
<ul>
<li><a href="archivos_ies/15_16/ef/EF_1ESO.doc" target="_blank">Cuadernillo programación 1º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_2ESO.doc" target="_blank">Cuadernillo programación 2º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_3ESO.doc" target="_blank">Cuadernillo programación 3º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_4ESO.doc" target="_blank">Cuadernillo programación 4º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_1_Bachillerato.doc" target="_blank">Cuadernillo programación 1º BACH.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_2_Bachillerato.doc" target="_blank">Cuadernillo programación 2º BACH.</a> (06/10/15)</li>
</ul>
<h3><span style="text-decoration: underline;"><strong>EDUCACIÓN PLÁSTICA:</strong></span></h3>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/15_16/programaciones/">Programación reducida.</a> (13/10/14)<br /></span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/programaciones/ProgramacionDT2_Bach_2014-2015.pdf" target="_blank">Programación de Dibujo Técnico 2º Bachillerato.</a> (23/10/14)<br /><br /></span></li>
</ul>
<p><span style="line-height: 1.25em;"> </span><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">FILOSOFÍA</strong></span></p>
<ul>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/FIL_RESUMEN_CRITERIOS_EV_Y_CALIF.pdf" target="_blank">Programación</a> (03/11/15)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>FÍSICA QUÍMICA.</strong></span></h3>
<ul>
<li><a href="archivos_ies/15_16/programaciones/FQ_RESUMEN_3_ESO.pdf" target="_blank">Resumen de la programación de FQ 3º ESO.</a> <a href="archivos_ies/criterios_evaluacion/Cuadernillo_FQ_3_ESO.pdf" target="_blank" title="Criterios de evaluación de FQ 3º ESO"><span style="color: #414141; letter-spacing: normal; line-height: 16.25px; text-align: left;">(03/11/15)</span></a></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_RESUMEN_4_ESO.pdf" target="_blank">Resumen de la programación de FQ 4º ESO.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_RESUMEN_1_Bach.pdf" target="_blank">Resumen de la programación de FQ 1º Bach.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_FIS_RESUMEN_2_Bach.pdf" target="_blank">Resumen de la programación de Física 2º Bach.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_QU_RESUMEN_2_Bach.pdf" target="_blank">Resumen de la programación de Química 2º Bach.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase;"><span style="text-decoration: underline;">FRANCÉS</span></strong></p>
<ul>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_ESO.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º ESO</span><span><span style="letter-spacing: 1px; line-height: 16.25px;"><span>.</span></span></span></a><span style="line-height: 16.25px;"> (06/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen2_ESO.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 2º ESO</span><span><span style="letter-spacing: 1px; line-height: 16.25px;"><span>.</span></span></span></a><span style="line-height: 16.25px;"> (06/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen3_ESO_FR2.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 3º ESO FR2</span><span><span style="letter-spacing: 1px; line-height: 16.25px;"><span>.</span></span></span></a><span style="line-height: 16.25px;"> (06/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen3_ESOPrimerLenguaExtranj.pdf" target="_blank">Resumen programación 3º ESO Primera Lengua Extranjera.</a> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen4_ESO.pdf" target="_blank">Resumen programación 4º ESO.</a> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachFR2NAvance.pdf" target="_blank"><span style="line-height: 16.25px;">Resumen programación 1º Bach. </span><span style="color: #8e9326;"><span style="letter-spacing: 1px; line-height: 16.25px;"><span style="text-decoration: underline;">FR2 N. Avance.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachFR2NDebut.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º Bach. F</span><span style="color: #8e9326;"><span style="text-decoration: underline;"><span style="line-height: 16.25px;">R2 N. Debut.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachFR2NIntermed.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º Bach. </span><span><span><span style="line-height: 16.25px;">FR2 N. Intermed.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachLenguaExtranI.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º Bach. </span><span style="color: #8e9326;"><span style="letter-spacing: 1px; line-height: 16.25px;"><span style="text-decoration: underline;">Lengua Extranera 1.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen2_BachFR2.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="text-decoration: none; color: #2f310d; line-height: 16.25px;">Resumen programación 2º Bach. 2ª </span><span style="text-decoration: none; color: #8e9326;"><span style="line-height: 16.25px;"><span style="text-decoration: underline;">Lengua Extranera</span></span></span></a><span style="color: #8e9326;"><span style="text-decoration: underline;">.</span></span><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen2_BachLenguaExtranII.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="line-height: 16.25px;">Resumen programación 2º Bach. </span><span style="color: #8e9326;"><span style="line-height: 16.25px;"><span style="text-decoration: underline;">Lengua Extranera 2.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
</ul>
<div style="text-align: justify;">
<h3><strong><span style="text-decoration: underline;">GEOGRAFÍA E HISTORIA.<br /></span></strong></h3>
<ol>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/15_16/programaciones/Historia_PRIMERO_DE_ESO.docx" target="_blank">Programación 1º ESO</a>.</span><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/SEGUNDO_DE_ESO.pdf" target="_blank"><span style="color: #2a2a2a;">Programación 2º ESO.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Historia_TERCERO_DE_ESO.docx" target="_blank"><span style="color: #2a2a2a;">Programación 3º ESO.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Historia_CUARTO_DE_ESO.pdf" target="_blank"><span style="color: #2a2a2a;">Programación 4º ESO.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/HISTORIA_MUNDO_CONTEMPORANEO.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #2a2a2a;">Programación Hª del mundo Contemporáneo 1º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/GEOGRAFIA.pdf" target="_blank"><span style="color: #2a2a2a;">Programación Geografía 2º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/HISTORIA_DE_ESPANA.pdf" target="_blank"><span style="color: #2a2a2a;">Programación Hª de España 2º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Programacion_H_Arte_2Bach1516.pdf" target="_blank"><span style="color: #2a2a2a;">Programación Arte 2º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/Programacion_didactica_CEE.docx" target="_blank">Programación Cultura Emprendedora y Empresarial 1º Bach.</a> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/PROGRAMACION_ECONOMIA%20.docx" target="_blank" style="text-decoration: none; color: #2f310d;">Programación Economía 1º Bach.</a> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/PROGRAMACION_DIDACTICA_ECE%20.docx" target="_blank">Programación Economía de la Empresa 2º Bach.</a> (03/11/15)</span></li>
</ol></div>
<p><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase;"><span style="text-decoration: underline;">INGLÉS</span></strong></p>
<ul>
<li><a href="archivos_ies/14_15/programaciones/Programacion_ingles_2014-2015.pdf" target="_blank" title="Programación de inglés, 14/15." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none; color: #336600; font-size: 11px;">Programación de Inglés, curso 2014/15 </a><span style="line-height: 1.25em;">(02/10/14)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-transform: uppercase; text-decoration: underline;">LATÍN Y GRIEGO</strong> </span></p>
<ul>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/programacion_reducida_latin.pdf" target="_blank">Programación de Latín</a>. </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/programacion_reducida_griego.pdf" target="_blank">Programación de Griego.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-transform: uppercase; text-decoration: underline;">LENGUA ESPAÑOLA</strong></span></p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/programaciones/programacion_Lengua_curso_2014-2015.pdf/archivos_ies/15_16/programaciones/LE_ABREVIADA%202015-16.docx" target="_blank">Programación abreviada de Lengua</a> <span style="line-height: 16.25px;">(03/11/15)</span><br /><br /></li>
</ul>
</ul>
<p> <strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-transform: uppercase; text-decoration: underline;">M<strong>ATEMÁ</strong>TICAS.</strong></p>
<ul style="text-align: justify;">
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_ESO.doc" target="_blank">Resumen de  la programación 1º ESO</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_ESO.doc">Resumen de  la programación. 2<span style="color: #423c33;"><span style="color: #2f310d;">º ESO</span></span></a></span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_3_ESO.doc" target="_blank">Resumen de  la programación. 3<span style="color: #423c33;"><span style="color: #2f310d;">º ESO</span></span></a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_4_ESO.doc" target="_blank"> Resumen de  la programación. 4<span style="color: #423c33;"><span style="color: #2f310d;">º ESO</span></span></a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_padres_ESO_BILIN_15_16.docx" target="_blank">Resumen de  la programación. BILINGÜES</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_Y_T.doc" target="_blank">Resumen de  la programación. </a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_Y_T.doc" target="_blank">1º BACHILLERATO C. Y T.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_SOC.doc" target="_blank">Resumen de  la programación. </a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_SOC.doc" target="_blank">1º BACHILLERATO C. SOC.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_II.doc" target="_blank">Resumen de  la programación. 2</a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_II.doc" target="_blank">º BACHILLERATO MAT II</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_CC_SS_II..doc" target="_blank">Resumen de  la programación. 2</a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_CC_SS_II..doc" target="_blank">º BACHILLERATO C. SOC.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_padres_ESTAD_2BACH_15_16.docx" target="_blank">Resumen de  la programación. 2</a></span><span style="color: #423c33;"><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_padres_ESTAD_2BACH_15_16.docx" target="_blank">º BACHILLERATO ESTADÍSTICA</a> </span></span><strong style="color: #2b2721;"><span style="color: #423c33;"><span style="color: #414141; font-weight: normal; line-height: 16.25px;">(03/11/15)</span></span></strong></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">MÚSICA:</strong></span></p>
<ul style="text-align: justify;">
<li style="text-align: justify;"><span style="color: #8e9326;"><span style="letter-spacing: 1px;"><a href="archivos_ies/15_16/programaciones/MUS_Objetivos_Contenidos_Criterios_Ev1516.pdf" target="_blank">Objetivos, Contenidos y Criterios de Evaluación_2015/16</a> <strong><span style="color: #414141; letter-spacing: normal; text-align: left;"> </span></strong></span></span><span style="line-height: 16.25px; text-align: left;">(03/11/15)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>PLAN DE IGUALDAD</strong></span><span style="color: #2a2a2a; font-size: 13px; font-weight: normal;"> </span></h3>
<ul style="text-align: justify;">
<li><span style="line-height: 1.25em;"><a href="archivos_ies/14_15/programaciones/PROYECTO_PLAN_DE_IGUALDAD_14-15_Jennifer_Valero.pdf" target="_blank">Programación</a> (20/11/14)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>PROGRAMACIÓN BILINGÜE</strong></span><span style="color: #2a2a2a; font-size: 13px; font-weight: normal;"> </span></h3>
<ul style="text-align: justify;">
<li><span style="line-height: 1.25em;"><a href="archivos_ies/14_15/programaciones/Programacion_bilingue_2014-2015b.docx" target="_blank">Programación</a> (13/10/14)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>RELIGIÓN</strong></span></h3>
<ul style="text-align: justify;">
<li><span style="line-height: 1.25em;"><a href=":\Users\Manuel\Documents\0Cervantes15-16\Programaciones/Rel_PROGR_2015_2016.pdf" target="_blank">Programación</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">Tecnología</strong></span></p>
<ul>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologias_2_eso.pdf" target="_blank">Criterios de evaluación Tecnologías 2º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologias_3_eso.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Criterios de evaluación Tecnologías 3º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologia_4_eso.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Criterios de evaluación Tecnologías 4º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologia_1_bach.pdf" target="_blank">Criterios de evaluación Tecnologías 1º Bach.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologia_2_bach.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Criterios de evaluación Tecnologías 2º Bach.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Criterios_evaluacion_3_4_optativa_Informatica_15-16.pdf" target="_blank">Criterios de evaluación Informática 3º y 4º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/Criterios_evaluacion_3_4_optativa_Informatica_15-16.pdf" target="_blank">PROGRAMACIÓN DIDÁCTICA DE INFORMÁTICA 3º Y 4º ESO.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/Programacion_TICBachillerato_15-16.pdf" target="_blank">PROGRAMACIÓN DIDÁCTICA TIC BACHILLERATO.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p> </p>";s:4:"body";s:0:"";s:5:"catid";s:3:"100";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-11-06 18:43:53";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"1";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:3:"130";s:8:"ordering";s:1:"5";s:8:"category";s:13:"DEPARTAMENTOS";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:18:"195:programaciones";s:7:"catslug";s:17:"100:departamentos";s:6:"author";s:14:"Miguel Anguita";s:6:"layout";s:7:"article";s:4:"path";s:98:"index.php?option=com_content&view=article&id=195:programaciones&catid=100:departamentos&Itemid=666";s:10:"metaauthor";N;s:6:"weight";d:2.1933099999999999;s:7:"link_id";i:2060;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:14:"Miguel Anguita";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:14:"Miguel Anguita";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:13:"DEPARTAMENTOS";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:13:"DEPARTAMENTOS";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=195";s:5:"route";s:98:"index.php?option=com_content&view=article&id=195:programaciones&catid=100:departamentos&Itemid=666";s:5:"title";s:14:"Programaciones";s:11:"description";s:4183:"Programaciones ABREVIADAS, criterios de evaluación, recuperaciones. curso 2015/16 BIOLOGÍA - GEOLOGÍA 15/16 Resumen de las programaciones del Departamento. (03/11/15) CICLO DE ANIMACIÓN ProgramacióN OTL 14-15 EMT (20/10/14) ProgramacióN PROYECTO 14-15 EMT (20/10/14) ProgramacióN MÓDULO FCT 14-15 (20/10/14)style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;"> ProgramacióN SSA 14-15 (20/10/14) ProgramacióN FCT 14-15 (20/10/14) ProgramacióN DCO 14-15 (20/10/14) ProgramacióN MIS 14-15 (20/10/14) PROGRAMACIÓN DEP. ASC 14-15 (20/10/14) EDUCACIÓN FÍSICA 15/16 Cuadernillo programación 1º ESO. (06/10/15) Cuadernillo programación 2º ESO. (06/10/15) Cuadernillo programación 3º ESO. (06/10/15) Cuadernillo programación 4º ESO. (06/10/15) Cuadernillo programación 1º BACH. (06/10/15)href="archivos_ies/15_16/ef/EF_2_Bachillerato.doc" target="_blank"> Cuadernillo programación 2º BACH. (06/10/15) EDUCACIÓN PLÁSTICA: Programación reducida. (13/10/14) Programación de Dibujo Técnico 2º Bachillerato. (23/10/14) FILOSOFÍA Programación (03/11/15) FÍSICA QUÍMICA. Resumen de la programación de FQ 3º ESO. (03/11/15) Resumen de la programación de FQ 4º ESO. (03/11/15) Resumen de la programación de FQ 1º Bach. (03/11/15) Resumen de la programación de Física 2ºBach. (03/11/15) Resumen de la programación de Química 2º Bach. (03/11/15) FRANCÉS Resumen programación 1º ESO . (06/11/15) Resumen programación 2º ESO . (06/11/15) Resumen programación 3º ESO FR2 . (06/11/15) Resumen programación 3º ESO Primera Lengua Extranjera. (03/11/15) Resumen programación 4º ESO. (03/11/15)target="_blank"> Resumen programación 1º Bach. FR2 N. Avance. (03/11/15) Resumen programación 1º Bach. F R2 N. Debut. (03/11/15) Resumen programación 1º Bach. FR2 N. Intermed. (03/11/15) Resumen programación 1º Bach. Lengua Extranera 1. (03/11/15) Resumen programación 2º Bach. 2ª Lengua Extranerastyle="text-decoration: underline;"> . (03/11/15) Resumen programación 2º Bach. Lengua Extranera 2. (03/11/15) GEOGRAFÍA E HISTORIA. Programación 1º ESO . (03/11/15) Programación 2º ESO. (03/11/15) Programación 3º ESO. (03/11/15) Programación 4º ESO. (03/11/15) Programación Hª del mundo Contemporáneo 1º Bach. (03/11/15) Programación Geografía 2º Bach.16.25px;"> (03/11/15) Programación Hª de España 2º Bach. (03/11/15) Programación Arte 2º Bach. (03/11/15) Programación Cultura Emprendedora y Empresarial 1º Bach. (03/11/15) Programación Economía 1º Bach. (03/11/15) Programación Economía de la Empresa 2º Bach. (03/11/15) INGLÉS Programación de Inglés, curso 2014/15 (02/10/14) LATÍN Y GRIEGOtarget="_blank"> Programación de Latín . (03/11/15) Programación de Griego. (03/11/15) LENGUA ESPAÑOLA Programación abreviada de Lengua (03/11/15) M ATEMÁ TICAS. Resumen de la programación 1º ESO (03/11/15) Resumen de la programación. 2 º ESO (03/11/15) Resumen de la programación. 3 º ESO (03/11/15) Resumen de laprogramación. 4 º ESO (03/11/15) Resumen de la programación. BILINGÜES (03/11/15) Resumen de la programación. 1º BACHILLERATO C. Y T. (03/11/15) Resumen de la programación. 1º BACHILLERATO C. SOC. (03/11/15) Resumen de la programación. 2 º BACHILLERATO MAT II (03/11/15) Resumen de la programación. 2 º BACHILLERATO C. SOC. (03/11/15)style="color: #423c33;"> Resumen de la programación. 2 º BACHILLERATO ESTADÍSTICA (03/11/15) MÚSICA: Objetivos, Contenidos y Criterios de Evaluación_2015/16 (03/11/15) PLAN DE IGUALDAD Programación (20/11/14) PROGRAMACIÓN BILINGÜEtarget="_blank"> Programación (13/10/14) RELIGIÓN Programación (03/11/15) Tecnología Criterios de evaluación Tecnologías 2º ESO. (03/11/15) Criterios de evaluación Tecnologías 3º ESO. (03/11/15) Criterios de evaluación Tecnologías 4º ESO. (03/11/15) Criterios de evaluación Tecnologías 1º Bach. (03/11/15) Criterios de evaluación Tecnologías 2º Bach.16.25px;"> (03/11/15) Criterios de evaluación Informática 3º y 4º ESO. (03/11/15) PROGRAMACIÓN DIDÁCTICA DE INFORMÁTICA 3º Y 4º ESO. (03/11/15) PROGRAMACIÓN DIDÁCTICA TIC BACHILLERATO. (03/11/15)";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2013-02-10 17:21:43";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2013-02-10 17:21:43";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}}";
<?php die("Access Denied"); ?>#x#s:166040:"a:2:{i:0;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"236";s:5:"alias";s:39:"pendientes-de-matematicas-curso-2013-14";s:7:"summary";s:24051:"<p> </p>
<hr />
<div id="contenido_sitio" style="text-align: justify; background-color: #e7eaad; float: left; width: 793.796875px;">
<h2><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES:</span></strong></span></h2>
<table style="width: 90%;" border="0">
<tbody>
<tr>
<td style="font-family: arial,sans-serif; margin: 0px;">
<div id="contenido_sitio" style="font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13.3333339691162px; text-align: justify; float: left; width: 793.796875px; background-color: #e7eaad;">
<p style="font-size: 13.3333px; color: inherit; padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">CURSO 215/16:</span></strong></span></p>
<p style="font-size: 13.3333px; color: inherit;"> </p>
<ul style="font-size: 13.3333px; color: inherit;">
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_1_Cal.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Pendientes de 1º y 2º ESO.</span></a></strong></li>
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_2_Cal.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Pendientes de 3º ESO y 1º Bachillerato.</span></a></strong></li>
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_3_materia.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Distribución de materia de alumnos pendientes de cursos anteriores.</span></a></strong></li>
</ul>
</div>
</td>
</tr>
</tbody>
</table>
<table class="borde_outset" style="text-align: center; color: #2a2a2a;" border="1">
<tbody>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" title="Pulsar para descargar." style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a>,</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,11,12,13,19,21,28.</li>
<li><strong>Tema 2:</strong> 1,6,8,14,16,17,18,19,20,23.</li>
<li><strong>Tema 3:</strong> 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43.</li>
<li><strong>Tema 4:</strong> Todos.</li>
<li><strong>Tema 5:</strong> 1,2,5,8,9,10,11,12,13.</li>
<li><strong>Tema 6:</strong> 1,2,8,11,29,31,32.</li>
</ul>
</ul>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 18/01/2016</strong></span></p>
<p><strong>TEMA 1</strong>. Números naturales</p>
<p><strong>TEMA 2.</strong> Divisibilidad</p>
<p><strong>TEMA 3.</strong> Fracciones</p>
<p><strong>TEMA 4.</strong>  Números decimales</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p><span style="font-family: Arial, sans-serif;"> </span></p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR.  </a></strong></span><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">Solamente los siguientes números por tema:</a>
<ul>
<li><strong>Tema 7:</strong> 1,4,6,7,8,9,10,11,12,13.</li>
<li><strong>Tema 8:</strong> 2,3,6,7,9,11,12,13,14,15.</li>
<li><strong>Tema 9: TODOS.</strong></li>
<li><strong>Tema 10:</strong> 1,3,4,5,6,7,8,11,13,14.</li>
<li><strong>Tema 11:</strong> <strong>TODOS.</strong></li>
</ul>
</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 04/04/2016</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p><strong>TEMA 10.</strong> Polígonos</p>
<p><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p> </p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1</strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/15_16/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a><hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<a href="archivos_ies/15_16/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 2º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span><span style="font-size: 13px;"> Solamente los siguientes números por tema:</span></li>
<ul>
<li><strong>Tema 1:</strong> 1,6,9,10,12,16,17,18.</li>
<li><strong>Tema 2:</strong>  1,3,5,6,7, 11,13,15,16.</li>
<li><strong>Tema 3:</strong> 1,2,5,9,11,12,16,17,18,20.</li>
<li><strong>Tema 4:</strong>  1,5,8,11,12,13,16,19.</li>
<li><strong>Tema 5:</strong>  1,2,3,4,7,8,9, 10,11,12.</li>
<li></li>
</ul>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 21/01/2016</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números enteros</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>2.</strong> Fracciones</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>3.</strong> Números decimales</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>4.</strong> Sistema sexagesimal</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>5.</strong> Expresiones algebráicas</p>
<p> </p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/14_15/mate/pendientes_2_eso_parte_2c.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> (Actualizado 13/10/15)</strong></span></li>
<li><strong>Y los del Tema 6:</strong>  1,6,7,8,9,13,14,15.</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 07/04/2016</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>6.</strong><span style="font-weight: normal;"> Ecu</span><span style="color: inherit; font-family: inherit; font-size: inherit;">aciones de primer y segundo grado</span></strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>7.</strong> Sistemas de ecuaciones</span></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>8.</strong> Proporcionalidad numérica</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>9.</strong> Proporcionalidad geométrica</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>10.</strong> Figuras planas. Áreas</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank">UNIDADES 3 Y 4</a></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<br /><hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-decoration: underline;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank">UNIDAD 9 Y 10</a></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 3º ESO</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></span></span><span style="color: #2f310d;"> extraídos del siguiente DOCUMENTO COMPLETO.</span><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_1_a_5.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></span></strong></a></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 18/01/2016</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números racionales</p>
<p><span style="font-size: inherit;"><strong>UNIDAD 2.</strong> Números reales</span></p>
<p><strong>UNIDAD 3.</strong> Polinomios</p>
<p><strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado </p>
<p><strong>UNIDAD 5.</strong> Sistemas de ecuaciones</p>
<p><strong> </strong></p>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> </span></span></strong><strong><span style="color: #2f310d;">extraídos del siguiente DOCUMENTO COMPLETO.</span></strong><br /><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_6_a_10.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></span></strong></a></li>
<li></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 04/04/2016</strong></span></p>
<p><strong>UNIDAD 6.</strong> Proporcionalidad numérica</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 7.</strong> Progresiones</span></p>
<p><strong>UNIDAD 8.</strong> Lugares geométricos. Figuras planas</p>
<p><strong>UNIDAD 9.</strong> Cuerpos geométricos .</p>
<p><strong> </strong></p>
</td>
<td>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDADES 1 Y 2</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<hr />
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span> </a></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank">UNIDAD: 7</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank">UNIDAD: 8</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º BACH. MAT. CCNN.</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/1bach_cnat_ptes_examen1.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a><br /></span></span></strong></li>
</ul>
<p style="text-align: center;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;"><span style="text-decoration: underline;">PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM</span><br /></strong></p>
<p><strong>UNIDAD 1:</strong> Números Reales</p>
<p><strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas</p>
<p><strong>UNIDAD 3:</strong> Trigonometría</p>
<p><strong>UNIDAD 4:</strong> Números Complejos</p>
<p><strong>UNIDAD 5:</strong> Geometría analítica</p>
<p><strong>UNIDAD 6: </strong>Cónicas</p>
</td>
<td>
<ul>
<li><strong><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/1bach_cnat_ptes_examen2.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a><br /></span></strong></li>
</ul>
<p style="text-align: center;"><strong><span style="text-decoration: underline;">SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM</span><br /></strong></p>
<p><strong>UNIDAD 7:</strong> Funciones</p>
<p><strong>UNIDAD 8:</strong> Funciones elementales</p>
<p><strong>UNIDAD 9:</strong> Límite de una función.</p>
<p><strong>UNIDAD 10:</strong> Derivada de una función</p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 2</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 6<br /></span></a></p>
<hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank">UNIDADES: 7 Y 8</a></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank">UNIDAD 10</a></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º BACH. MAT. CCSS.</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></span></li>
</ul>
<p style="text-align: center;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;"><span style="text-decoration: underline;">PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM</span></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>1:</strong> Números Reales</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 2</strong><strong>:</strong> Aritmética Mercantil</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span></p>
<p><strong>UNIDAD 4:</strong> Ecuaciones, inecuaciones y sistemas</p>
<p class="MsoNormal" style="page-break-before: always;"> </p>
</td>
<td>
<ul>
<li><strong><a href="archivos_ies/15_16/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></li>
</ul>
<p style="text-align: center;"><strong><span style="text-decoration: underline;">SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM</span></strong></p>
<p style="text-align: center;"> </p>
<p><strong>UNIDAD 5:</strong> Funciones</p>
<p><strong>UNIDAD 6:</strong> <span style="color: inherit; font-family: inherit; font-size: inherit;">Funciones elementales</span></p>
<p><strong>UNIDAD 7:</strong> Límite de una función</p>
<p><strong>UNIDAD 8:</strong> Derivada de una función</p>
<p><strong>UNIDAD 9:</strong> Estadística unidimensional</p>
</td>
<td>
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Aritmetica_mercantil_resueltos.pdf" target="_blank"><span style="text-align: left;"><span style="text-align: left;">UNIDAD 2</span></span></a></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 3</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 4</a><br /></span></p>
<hr />
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank">UNIDAD 5</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank">UNIDAD 6</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/matematicas/Estadistica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 9</span></a></p>
</td>
</tr>
</tbody>
</table>
</div>
<div style="text-align: justify; background-color: #e7eaad; float: left; width: 793.796875px;"><strong style="color: #414141;"><span style="color: #2a2a2a; font-weight: normal;"> </span></strong></div>";s:4:"body";s:0:"";s:5:"catid";s:3:"129";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-12-17 18:54:28";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"35";s:8:"ordering";s:1:"2";s:8:"category";s:12:"Matemáticas";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:43:"236:pendientes-de-matematicas-curso-2013-14";s:7:"catslug";s:17:"129:matematicas11";s:6:"author";s:14:"Miguel Anguita";s:6:"layout";s:7:"article";s:4:"path";s:123:"index.php?option=com_content&view=article&id=236:pendientes-de-matematicas-curso-2013-14&catid=129:matematicas11&Itemid=671";s:10:"metaauthor";N;s:6:"weight";d:0.18668999999999999;s:7:"link_id";i:2066;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:14:"Miguel Anguita";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:14:"Miguel Anguita";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"Matemáticas";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"Matemáticas";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=236";s:5:"route";s:123:"index.php?option=com_content&view=article&id=236:pendientes-de-matematicas-curso-2013-14&catid=129:matematicas11&Itemid=671";s:5:"title";s:41:"Pendientes de Matemáticas. Curso 2015/16";s:11:"description";s:4503:"PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES: CURSO 215/16: Pendientes de 1º y 2º ESO. Pendientes de 3º ESO y 1º Bachillerato. Distribución de materia de alumnos pendientes de cursos anteriores. PENDIENTES DE 1º ESOstyle="text-decoration: underline;"> EJERCICIOS PARA RESOLVER Y ENTREGAR , Solamente los siguientes números por tema: Tema 1: 1,11,12,13,19,21,28. Tema 2: 1,6,8,14,16,17,18,19,20,23. Tema 3: 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43. Tema 4: Todos. Tema 5: 1,2,5,8,9,10,11,12,13. Tema 6: 1,2,8,11,29,31,32. PRIMER EXAMEN: 18/01/2016 TEMA 1 . Números naturales TEMA 2. Divisibilidad TEMA 3. Fracciones TEMA 4. Números decimales TEMA 5. Números enteros TEMA 6. Iniciación al álgebra EJERCICIOS PARA RESOLVER Y ENTREGAR. Solamente los siguientes números por tema: Tema 7: 1,4,6,7,8,9,10,11,12,13. Tema 8: 2,3,6,7,9,11,12,13,14,15. Tema 9:TODOS. Tema 10: 1,3,4,5,6,7,8,11,13,14. Tema 11: TODOS. SEGUNDO EXAMEN: 04/04/2016 TEMA 7. Sistema Métrico Decimal TEMA 8. Proporcionalidad numérica TEMA 9. Ángulos, circunferencias y círculos TEMA 10. Polígonos TEMA 11. Funciones y gráficas EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD 1 UNIDAD 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDAD 6 EXAMEN 2href="archivos_ies/15_16/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank"> UNIDAD 7 UNIDAD 8 UNIDAD 9 UNIDAD 10 UNIDAD 11 PENDIENTES DE 2º ESO EJERCICIOS PARA RESOLVER Y ENTREGAR Solamente los siguientes números por tema: Tema 1: 1,6,9,10,12,16,17,18. Tema 2: 1,3,5,6,7, 11,13,15,16. Tema 3: 1,2,5,9,11,12,16,17,18,20. Tema 4: 1,5,8,11,12,13,16,19. Tema 5: 1,2,3,4,7,8,9, 10,11,12. PRIMER EXAMEN: 21/01/2016 UNIDAD 1. Números enteros UNIDAD 2. Fraccionesinherit;"> UNIDAD 3. Números decimales UNIDAD 4. Sistema sexagesimal UNIDAD 5. Expresiones algebráicas EJERCICIOS PARA RESOLVER Y ENTREGAR (Actualizado 13/10/15) Y los del Tema 6: 1,6,7,8,9,13,14,15. SEGUNDO EXAMEN: 07/04/2016 UNIDAD 6. Ecu aciones de primer y segundo grado UNIDAD 7. Sistemas de ecuaciones UNIDAD 8. Proporcionalidad numérica UNIDAD 9. Proporcionalidad geométrica UNIDAD 10. Figuras planas. Áreasstyle="text-align: center;"> EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD 1 UNIDAD 2 UNIDADES 3 Y 4 UNIDAD 5 EXAMEN 2 UNIDAD 6 UNIDAD 7 UNIDAD 8 UNIDAD 9 Y 10 PENDIENTES DE 3º ESO EJERCICIOS PARA RESOLVER YENTREGAR extraídos del siguiente DOCUMENTO COMPLETO. DESCARGAR DOCUMENTO COMPLETO. PRIMER EXAMEN: 18/01/2016 UNIDAD 1. Números racionales UNIDAD 2. Números reales UNIDAD 3. Polinomios UNIDAD 4. Ecuaciones de primer y segundo grado UNIDAD 5. Sistemas de ecuaciones EJERCICIOS PARA RESOLVER Y ENTREGAR extraídos del siguiente DOCUMENTO COMPLETO. DESCARGAR DOCUMENTO COMPLETO. SEGUNDO EXAMEN: 04/04/2016 UNIDAD 6. Proporcionalidad numéricafont-size: inherit;"> UNIDAD 7. Progresiones UNIDAD 8. Lugares geométricos. Figuras planas UNIDAD 9. Cuerpos geométricos . EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDADES 1 Y 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 EXAMEN 2 UNIDAD 6 UNIDAD: 7 UNIDAD: 8href="archivos_ies/15_16/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank"> UNIDAD 9 PENDIENTES DE 1º BACH. MAT. CCNN. EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM UNIDAD 1: Números Reales UNIDAD 2: Ecuaciones, inecuaciones y sistemas UNIDAD 3: Trigonometría UNIDAD 4: Números Complejos UNIDAD 5: Geometría analítica UNIDAD 6: Cónicas EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM UNIDAD 7: Funciones UNIDAD 8: Funciones elementales UNIDAD 9: Límite de una función. UNIDAD 10: Derivada de una función EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1style="text-align: left;"> UNIDAD: 1 UNIDAD: 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDAD 6 EXAMEN 2 UNIDADES: 7 Y 8 UNIDAD 9 UNIDAD 10 PENDIENTES DE 1º BACH. MAT. CCSS. EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 20/01/2016 - 11.15 horas en elSUM UNIDAD 1: Números Reales UNIDAD 2 : Aritmética Mercantil UNIDAD 3: Polinomios y fracciones algebraicas UNIDAD 4: Ecuaciones, inecuaciones y sistemas EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM UNIDAD 5: Funciones UNIDAD 6: Funciones elementales UNIDAD 7: Límite de una función UNIDAD 8: Derivada de una función UNIDAD 9: Estadística unidimensional EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD 1 UNIDAD 2style="text-align: center;"> UNIDAD 3 UNIDAD 4 EXAMEN 2 UNIDAD 5 UNIDAD 6 UNIDAD 7 UNIDAD 8 UNIDAD 9";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2013-11-10 19:46:30";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2013-11-10 19:46:30";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:1;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:2:"51";s:5:"alias";s:15:"dep-matematicas";s:7:"summary";s:112182:"<h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; background-color: #ffffff; text-align: justify;"><strong style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><span style="text-decoration: underline;">ENLACES DIRECTOS A LAS SIGUIENTES SECCIONES:</span></strong></h3>
<h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; background-color: #ffffff; text-align: justify;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><a href="index.php?option=com_content&amp;view=article&amp;id=137&amp;Itemid=724" target="_blank">2º Eso</a><strong><span style="text-decoration: underline;"><br /></span></strong></span><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><a href="index.php?option=com_content&amp;view=article&amp;id=93&amp;Itemid=103" target="_blank" title="MAT. I">1º BACH. MAT.I</a><br /><span style="color: #5d5d5d; font-size: 20px; text-align: left; text-transform: uppercase;"><a href="http://matepaco.blogspot.com.es/p/bachillerato-ccss-i.html" target="_blank">1º BACH. MAT. CC.SS I</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=154&amp;Itemid=103" target="_blank" title="MAT. II">2º BACH. MAT. II</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=94&amp;Itemid=103" target="_blank" title="MAT. CC.SS. II">2º BACH. MAT. CC.SS II</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=103&amp;Itemid=103" target="_blank" title="MAT. CC.SS. II">2º BACH. ESTADÍSTICA<br /></a><a href="index.php?option=com_content&amp;view=article&amp;id=236&amp;Itemid=103" target="_blank" title="PENDIENTES.&quot;">PENDIENTES DE TODOS LOS CURSOS<br /><br /></a><a href="http://matepaco.blogspot.com.es/" target="_blank">UN BUEN bLOG DE MATEMÁTICAS</a></span></span></h3>
<p> </p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: justify; background-color: #ffffff;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><strong><span style="text-decoration: underline;"><span style="color: #ff0000; text-decoration: underline;">novedad:</span></span> <br /></strong>este curso 2014/15 contará con un grupo de estadística en 2º bachillerato cns.<span style="text-decoration: underline;"><br /><br />CURSO 2014/15<br /><br /></span><strong style="color: #2a2a2a; font-size: 13px; text-transform: none; background-color: #ffffff;"><span>IMPORTANCIA DE ELEGIR ESTADÍSTICA COMO OPTATIVA EN 2º BACHILLERATO CIENCIAS<br /><br /></span></strong><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">El alumnado de Ciencias puede subir la nota de Selectividad en la Fase Específica mediante la realización de pruebas de distintas materias. En particular las Matemáticas II suele puntuar con una ponderación de 0,2 en la mayoría de los Grados de Ciencias (subir hasta 2 puntos la nota de la Fase General). El problema es que muchos alumnos se examinan de Matemáticas II en la Fase General al no disponer de otra materia mejor, según sus intereses.<br /></span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Es por ello que el Departamento de Matemáticas informa que este inconveniente se puede subsanar si el alumnado se examinara de Matemáticas CC SS II en la Fase General y de </span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Matemáticas II en la Fase Específica.<br /></span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Como quiera que este alumnado de Ciencias sólo ha cursado la asignatura de Matemáticas II, existe una posibilidad de que completen los conocimientos que les faltan para poder dominar la materia de Matemáticas CC SS II mediante la asignatura de Estadística, que se puede elegir como optativa en 2º de Bachillerato Ciencias.<br /></span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Dada la importancia de obtener una buena nota final en Selectividad es por lo que el Departamento de Matemáticas anima al alumnado que actualmente está en fase de matriculación para que seleccione </span><strong style="color: #2a2a2a; font-size: 13px; text-transform: none;">Estadística</strong><span style="color: #2a2a2a; font-size: 13px; text-transform: none;"> como </span><strong style="color: #2a2a2a; font-size: 13px; text-transform: none;">optativa</strong><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">. </span></span><span style="text-align: left; background-color: #ffffff;">Si se necesita más información se puede consultar a cualquier miembro del Departamento, o escribiendo un mensaje a <strong>iescervantesgranada[arroba]gmail.com</strong> que se le haría llegar a los mismos.<br /><br /></span></p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: center; background-color: #ffffff;"><strong><span style="text-align: left; background-color: #ffffff;">A CONTINUACIÓN SE PUEDEN VER LOS DISTINTOS GRADOS QUE SE IMPARTEN EN LA UNIVERSIDAD DE GRANADA EN LOS QUE SE CURSA UNA ASIGNATURA DE ESTADÍSTICA:</span></strong><img src="http://www.stei.es/estadistica/images/stories/ugr3.png" border="0" alt="" width="602" height="102" style="text-align: center; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;" /></p>
<div class="floatbox" style="margin: 0px; padding: 0px; overflow: hidden; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #ffffff;">
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Enfermería</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/enfermeria.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Fisioterapia</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/fisioterapia.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Gestión y Administración Pública (Melilla)</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~eues/webgrupo/Docencia/MonteroAlonso/MonteroEstadistica2.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística II</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Relaciones Laborales</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~ramongs/relacioneslaborales.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~eao" style="text-decoration: none; color: #678925;">Estadística Asistida por Ordenador</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Biología</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioestad/ASIGNATURAS.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioestad/ASIGNATURAS.htm" target="_blank" style="text-decoration: none; color: #678925;">Fundamentos de Biología Aplicada I</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Ciencias de la Actividad Física  y el Deporte</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/inef.htm#1" target="_blank" style="text-decoration: none; color: #678925;">Estadística</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/inef.htm#2" target="_blank" style="text-decoration: none; color: #678925;">Estadística Aplicada a la Actividad Física</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Ciencias y Técnicas Estadísticas</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/ciencias.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Documentación</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~focana/stat.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Economía</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~focana/peef.htm" target="_blank" style="text-decoration: none; color: #678925;">Predicción Económica y Empresarial</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Farmacia</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~focana/mafarma.htm" target="_blank" style="text-decoration: none; color: #678925;">Matemática Aplicada</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Medicina</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/medicina02op.htm#be3" target="_blank" style="text-decoration: none; color: #678925;">Análisis Estadístico con Ordenador de Datos Médicos</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/medicina02tr.htm#be2" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Odontología</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/odonto.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística Aplicada</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Sociología</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~ramongs/sociologia.htm" target="_blank" style="text-decoration: none; color: #678925;">Análisis Multivariante</a></li>
</ul>
</div>
<hr />
<div id="contenido_sitio" style="float: CENTER; display: block; width: 98%;">
<table class="MsoNormalTable" style="padding-left: 30px; width: 650px;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 517.45pt; border-width: 1pt; padding: 1.5pt;" colspan="4" valign="top" width="690">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center; padding-left: 30px;" align="center"><strong><span style="text-decoration: underline;"><span style="font-size: 10pt; font-family: 'Century Gothic', sans-serif;">PENDIENTES DE MATEMÁTICAS PARA SEPTIEMBRE DE 2014</span></span></strong></p>
</td>
</tr>
<tr style="height: 112.8pt; padding-left: 10px;">
<td style="border-top-style: none; border-left-width: 1pt; border-bottom-style: none; border-right-width: 1pt; padding: 30pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 1º ESO</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-bottom-style: none; border-left-style: none; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">TEMA 1</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">. Números naturales<br /> <strong>TEMA 2.</strong> Divisibilidad<br /> <strong>TEMA 3.</strong> Fracciones<br /> <strong>TEMA 4.</strong>  Números decimales<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">TEMA 5.</span></strong><span style="font-size: 8pt; font-family: inherit, serif;"> Números enteros<br /> <strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-bottom-style: none; border-left-style: none; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 57pt; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">TEMA 7.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Sistema Métrico Decimal</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">TEMA 8.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Proporcionalidad numérica<br /> <strong>TEMA 9.</strong> Ángulos, circunferencias y círculos<br /> <strong>TEMA 10.</strong> Polígonos<br /> <strong>TEMA 11.</strong> Funciones y gráficas</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-bottom-style: none; border-left-style: none; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 1</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 2</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="font-family: 'inherit','serif'; color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 6</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 7</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 8</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 10</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="border-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 2º ESO</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 1.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números enteros<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">2.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Fracciones<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">3.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números decimales<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">4.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Sistema sexagesimal<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">5.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Expresiones algregráicas<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">6.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Ecu</span><span style="font-size: 8pt; font-family: inherit, serif;">acioens de primer y segundo grado</span></p>
</td>
<td style="width: 5cm; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_7_a_11.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt 9pt 12.65pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Sistemas de ecuaciones</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> <strong>UNIDAD </strong></span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">8.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Proporcionalidad numérica<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">9.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Proporcionalidad geométrica<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">10.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Figuras planas. Áreas<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">11.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Cuerpos geométricos</span></p>
</td>
<td style="width: 5cm; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 1</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 2</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDADES 3 Y 4</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><span style="color: #5d5d5d; letter-spacing: .75pt;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank">UNIDAD 5<br /></a></span></span><a href="archivos_ies/13_14/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank" style="letter-spacing: 0.75pt; font-size: 8pt;">UNIDAD 6</a></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 7</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 8</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9 Y 10</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Cuerpos_geometricos_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 3º ESO</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_1_a_6.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 1.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números racionales<br /> <strong>UNIDAD 2.</strong> Números reales<br /> <strong>UNIDAD 3.</strong> Polinomios<br /> <strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado <br /> <strong>UNIDAD 5.</strong> Sistemas de ecuaciones<br /> <strong>UNIDAD 6.</strong> Proporcionalidad numérica</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_7_a_12.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Progresiones</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 8.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Lugares geométricos.Figuras planas<br /> <strong>UNIDAD 9.</strong> Cuerpos geométricos<br /> <strong>UNIDAD 10.</strong> Movimientos y semejanza<br /> <strong>UNIDAD 11.</strong> Funciones<br /> <strong>UNIDAD 12.</strong> Funciones lineales y afines</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDADES 1 Y 2</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="font-family: 'inherit','serif'; color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 6</span><span style="color: #5d5d5d; letter-spacing: .75pt;"> </span></a></span></p>
<div class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><a href="archivos_ies/13_14/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank" style="font-size: 7pt;"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 7</span></a></div>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 8</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Movimientos_plano_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 10</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_y_graficas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_lineales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 12</span></a></span></p>
</td>
</tr>
<tr style="height: 132.75pt; padding-left: 30px;">
<td style="border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 1º BACH. MAT. CN.</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/examen1.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 1:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números Reales<br /> <strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas<br /> <strong>UNIDAD 3:</strong> Trigonometría<br /> <strong>UNIDAD 4:</strong> Números Complejos<br /> <strong>UNIDAD 5:</strong> Geometría analítica</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CNAT.pdf" target="_blank"><strong><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></strong></a></span></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Funciones<br /> <strong>UNIDAD 8:</strong> Funciones elementales<br /> <strong>UNIDAD 9:</strong> Límite de una función<br /> <strong>UNIDAD 10:</strong> Derivada de una función<br /> <strong>UNIDAD 11:</strong> Integrales</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 1</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 2</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a></span></p>
<div class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank" style="font-size: 7pt;"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDADES: 7 Y 8</span></a></div>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 10</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Integrales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 1º BACH. MAT. CCSS.</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">1:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números Reales<br /> <strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 4:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Ecuaciones, inecuaciones y sistemas</span></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; page-break-before: always; padding-left: 30px;"><span style="font-size: 10pt; font-family: 'Times New Roman', serif;"> </span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center; padding-left: 30px;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> </span></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 5:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Funciones<br /> <strong>UNIDAD 6:</strong> Funciones elementales</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Límite de una función<br /> <strong>UNIDAD 8:</strong> Derivada de una función<br /> <strong>UNIDAD 11:</strong> Probabilidad</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt;"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 1</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a></span></p>
<div class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank" style="font-size: 7pt;"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a></div>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 6</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 7</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 8</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Binomial_resueltos.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt; text-decoration: none; text-underline: none;">UNIDAD 11</span></a></span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><strong><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 107%;"> </span></strong></p>
<p class="MsoNormal" style="padding-left: 30px;"><strong><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 107%;">LIBROS DE TEXTO DEL DEPARTAMENTO DE MATEMÁTICAS, CURSO 2014/15:</span></strong><span style="color: #5d5d5d; font-size: 31px; font-weight: bold; text-transform: uppercase;"> </span></p>
<table class="MsoTableGrid" style="width: 642px; border: none; padding-left: 30px;" border="1" cellspacing="0" cellpadding="0">
<tbody style="padding-left: 30px;">
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>CURSO</strong></p>
</td>
<td style="width: 106.15pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Materia</strong></p>
</td>
<td style="width: 163.1pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Editorial</strong></p>
</td>
<td style="width: 106.3pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>EAN</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>1º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana  Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052570</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>2º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana  Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788429438215</p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>3º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana  Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052587</p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>4º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas A</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052112</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>4º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas B</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052242</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>1º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas I</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas I</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">La casa del saber (González y otros)</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>1º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas CCSS I</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas aplicadas a las CCSS I</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;">Santillana. </span>La casa del Saber (Escoredo y otros)</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>2º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas II</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas II</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;">Santillana. </span>Matemáticas II. La casa del saber (González y</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">otros)</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>2º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas CCSS II</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas aplicadas a las CCSS II</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;">Santillana. </span>Matemáticas aplicadas a las CCSS II. La casa</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">del saber (Escoredo y otros)</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
</tbody>
</table>
<h1 style="text-align: center; padding-left: 30px;"> <span style="text-align: left;"> </span></h1>
<hr style="padding-left: 30px;" />
<h1 style="text-align: center; padding-left: 30px;"><strong style="text-align: left;"><span style="color: #423c33;">CURSO 2013/14</span></strong></h1>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; padding-left: 30px;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Primera reunión de Coordinación para Selectividad de Matemáticas CC SS II </span></h2>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="color: #2a2a2a;"> </span><span style="text-indent: 0cm; text-transform: none; color: #2a2a2a;"> </span><a href="archivos_ies/13_14/2bach_matccssii/acta_1reunion_m_ccssii.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></li>
</ul>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; padding-left: 30px;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Primera reunión de Coordinación para Selectividad de Matemáticas II </span></h2>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="color: #2a2a2a;"> </span><span style="text-indent: 0cm; text-transform: none; color: #2a2a2a;"> </span><a href="archivos_ies/13_14/2bach_matii/informacionmatematicasii.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></li>
</ul>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"> </p>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="color: #423c33;">20/12/13:</span></strong></span><strong><span style="color: #423c33;"> </span></strong>Los desafíos matemáticos vuelven a esta casa por Navidad. Como ya sucedió el año pasado, con motivo del sorteo de la <a href="http://elpais.com/tag/loteria_navidad/a/">Lotería de Navidad</a> ofrecemos un nuevo problema. El encargado de presentar el <em>Desafío Extraordinario de Navidad</em> de 2013 es Javier Cilleruelo, profesor de la <a href="http://www.uam.es/">Universidad Autónoma de Madrid</a> y miembro del <a href="http://www.icmat.es/es">Instituto de Ciencias Matemáticas</a> (ICMAT).<br />Entre los acertantes se sorteará una biblioteca matemática como la que ofreció EL PAÍS en el quiosco durante 2011. El ganador recibirá, además, el libro <a href="http://estimulos-matematicos.aprenderapensar.net/">'Desafíos Matemáticos'</a> por cortesía de la <a href="http://www.rsme.es/">Real Sociedad Matemática Española</a>, una publicación de SM en la que se recogen los <a href="http://sociedad.elpais.com/sociedad/2011/07/12/actualidad/1310421608_850215.html">40 desafíos matemáticos</a> que ofrecimos en la web y con los que EL PAÍS y la RSME celebraron el centenario de esta institución hace dos años. Manda tu respuesta antes de las 00.00 horas del domingo 22 de diciembre (la medianoche del sábado al domingo, hora peninsular española) a <a href="mailto:problemamatematicas@gmail.com">problemamatematicas@gmail.com</a> y participa en el sorteo. <br />A continuación, para aclarar dudas y en atención a nuestros lectores sordos, añadimos el enunciado del problema por escrito: <br />El equipo que preparamos los desafíos matemáticos hemos decidido abonarnos durante todo el año a un número de la Lotería. Para elegir ese número, que debe estar comprendido entre el 0 y el 99.999, pusimos como condición que tuviese las cinco cifras distintas y que, además, cumpliese alguna otra propiedad interesante. Finalmente hemos conseguido un número que tiene la siguiente propiedad: si numeramos los meses del año del 1 al 12, en cualquier mes del año ocurre que al restar a nuestro número de lotería el número del mes anterior, el resultado es divisible por el número del mes en el que estemos. Y esto sucede para cada uno de los meses del año. <br />Es decir, si llamamos L a nuestro número, tenemos por ejemplo que en marzo L-2 es divisible entre 3 y en diciembre L-11 es divisible entre 12. <br /><br />El reto que os planteamos es que nos digáis a qué número de Lotería estamos abonados y que nos expliquéis cómo lo habéis encontrado.<br /><br /><strong>OBSERVACIONES IMPORTANTES.</strong> Insistimos en que es importante que hagáis llegar junto con el número el razonamiento de cómo lo habéis hallado. No se considerarán válidas las respuestas que den sólo el número o que lo hayan encontrado probando todos uno a uno (a mano o con un ordenador).</li>
</ul>
<p style="padding-left: 30px;"> </p>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="color: #423c33;"> </span></strong></span></p>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><a href="archivos_ies/13_14/programaciones/13_14_PROGRAMACION_Mate4.doc" target="_blank"><strong style="color: #2b2721;"><span style="color: #423c33;">PROGRAMACIÓN DE MATEMÁTICAS, CURSO 2013/14.</span></strong></a></li>
<li style="padding-left: 30px;"><strong style="color: #2b2721;"><span style="color: #423c33;"><a href="archivos_ies/13_14/pendientes/Pendientes13_14_b.pdf" target="_blank">FECHAS DE EXÁMENES: PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES.</a><strong style="color: #2b2721;"><span style="color: #423c33;"> <br /><br /></span></strong></span></strong></li>
<li style="padding-left: 30px;"><strong style="color: #2b2721;"><span style="color: #423c33;"><strong style="color: #2b2721;"><span style="color: #423c33;"><a href="archivos_ies/13_14/2bach_matccssii/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2013_2014.pdf" target="_blank">Directrices y orientaciones Selectividad Mat. CC.SS. II curso 2013/14.</a><br /><br /></span></strong></span></strong></li>
<li style="padding-left: 30px;">12/11/2012:  <a href="archivos_ies/13_14/2bach_matccssii/COORDINACION05-11-12_Y_DIRECTRICES_OFICIALES5.pdf" target="_blank">Coordinación de Selectividad de Mat. CC SS II del 05/11/12..</a></li>
<li style="padding-left: 30px;">09/11/2012: <a href="archivos_ies/13_14/2bach_matccssii/Sept_2012_selec_MatCCSSII_y_soluciones2.pdf" target="_blank">Examen y soluciones de Selectividad de MAT_CC_SS_II de sept. 2012.</a></li>
<li style="padding-left: 30px;">20/06/2012: <a href="archivos_ies/13_14/2bach_matccssii/Junio_2012_selec_MatCCSSII_y_soluciones.pdf" target="_blank">Exámen y soluciones de Selectividad de MAT_CC_SS_II de jun. 2012,</a></li>
</ul>
<hr style="padding-left: 30px;" />
<div style="padding-left: 30px;">
<ul style="color: #151509; margin: 1em 0px 1em 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; padding: 0px 0px 0px 30px;">
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS1_ESO.pdf" target="_blank" title="Criterios de evaluación de 1º ESO." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Matemáticas 1º ESO.</a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_2_ESO.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">2º ESO.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_3_ESO.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">3º ESO.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_4_ESO.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">4º ESO.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_CCSS I _1BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">1º Bachillerato CCSS I.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS I _1BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas I</span></span> 1</span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">º Bachillerato CNAT.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_CCSSII_2BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas </span></span>2</span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">º Bachillerato CCSS II.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_II_2BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas II</span></span> 2</span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">º Bachillerato CNAT</span></span>.</a></span></li>
</ul>
<p style="padding-left: 30px;"> </p>
<hr style="padding-left: 30px;" />
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=94" target="_blank"><span style="color: #2a2a2a;">ENLACE A ZONA DE 2º BACH. CC.SS. II.</span></a></strong></li>
<li style="padding-left: 30px;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=154" target="_blank"><span style="color: #2a2a2a;">ENLACE A ZONA DE 2º BACH. CC.NN. II.</span></a></strong></li>
</ul>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"> </p>
<h2 style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES:</span></strong></span></h2>
<p style="padding-left: 30px;"> </p>
<table class="borde_outset" style="text-align: center; color: #2a2a2a; padding-left: 30px; background-color: #e7eaad;" border="1">
<tbody style="padding-left: 30px;">
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 1º ESO</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" style="text-decoration: underline; color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"> <strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>TEMA 1</strong>. Números naturales</p>
<p style="padding-left: 30px;"><strong>TEMA 2.</strong> Divisibilidad</p>
<p style="padding-left: 30px;"><strong>TEMA 3.</strong> Fracciones</p>
<p style="padding-left: 30px;"><strong>TEMA 4.</strong>  Números decimales</p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p style="padding-left: 30px;"><span style="font-family: 'Arial','sans-serif';"> </span></p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="text-decoration: underline; color: #2f310d; background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="text-align: left; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p style="padding-left: 30px;"><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p style="padding-left: 30px;"><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p style="padding-left: 30px;"><strong>TEMA 10.</strong> Polígonos</p>
<p style="padding-left: 30px;"><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p style="padding-left: 30px;"> </p>
</td>
<td style="text-align: center; padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/13_14/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></span></a><hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p style="padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<a href="archivos_ies/13_14/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 2º ESO</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank" style="text-decoration: underline; color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"> <strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 1.</strong> Números enteros</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>2.</strong> Fracciones</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>3.</strong> Números decimales</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>4.</strong> Sistema sexagesimal</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>5.</strong> Expresiones algregráicas</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>6.</strong> Ecu<span style="color: inherit; font-family: inherit; font-size: inherit;">acioens de primer y segundo grado</span></p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_7_a_11.pdf" target="_blank" style="text-decoration: underline; color: #2f310d; background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="text-align: left; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>7.</strong> Sistemas de ecuaciones</span></p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>8.</strong> Proporcionalidad numérica</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>9.</strong> Proporcionalidad geométrica</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>10.</strong> Figuras planas. Áreas</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>11.</strong> Cuerpos geométricos</p>
</td>
<td style="text-align: center; padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank">UNIDADES 3 Y 4</a></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/13_14/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></span></a><hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank">UNIDAD 9 Y 10</a></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Cuerpos_geometricos_resueltos.pdf" target="_blank">UNIDAD 11</a><br /></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 3º ESO</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_1_a_6.pdf" target="_blank" style="text-decoration: underline; color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 1.</strong> Números racionales</p>
<p style="padding-left: 30px;"><span style="font-size: inherit;"><strong>UNIDAD 2.</strong> Números reales</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 3.</strong> Polinomios</p>
<p style="padding-left: 30px;"><strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado </p>
<p style="padding-left: 30px;"><strong>UNIDAD 5.</strong> Sistemas de ecuaciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 6.</strong> Proporcionalidad numérica</p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_7_a_12.pdf" target="_blank" style="text-decoration: underline; color: #2f310d; background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 7.</strong> Progresiones</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 8.</strong> Lugares geométricos. Figuras planas</p>
<p style="padding-left: 30px;"><strong>UNIDAD 9.</strong> Cuerpos geométricos .</p>
<p style="padding-left: 30px;"><strong>UNIDAD 10.</strong> Movimientos y semejanza</p>
<p style="padding-left: 30px;"><strong>UNIDAD 11.</strong> Funciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 12.</strong> Funciones lineales y afines</p>
</td>
<td style="padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDADES 1 Y 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span> </a></p>
<hr style="padding-left: 30px;" />
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /> </strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank">UNIDAD: 7</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank">UNIDAD: 8</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank">UNIDAD 9</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Movimientos_plano_resueltos.pdf" target="_blank">UNIDAD 10</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_y_graficas_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_lineales_resueltos.pdf" target="_blank">UNIDAD 12</a><br /></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 1º BACH. MAT. CN.</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><strong><span style="text-decoration: underline;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/examen1.pdf" target="_blank"><span style="color: #2f310d; text-decoration: underline;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></strong></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 1:</strong> Números Reales</p>
<p style="padding-left: 30px;"><strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas</p>
<p style="padding-left: 30px;"><strong>UNIDAD 3:</strong> Trigonometría</p>
<p style="padding-left: 30px;"><strong>UNIDAD 4:</strong> Números Complejos</p>
<p style="padding-left: 30px;"><strong>UNIDAD 5:</strong> Geometría analítica</p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CNAT.pdf" target="_blank"><span style="text-decoration: underline;"><strong><span style="color: #2f310d;"><span style="background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></span></strong></span></a></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 7:</strong> Funciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 8:</strong> Funciones elementales</p>
<p style="padding-left: 30px;"><strong>UNIDAD 9:</strong> Límite de una función.</p>
<p style="padding-left: 30px;"><strong>UNIDAD 10:</strong> Derivada de una función</p>
<p style="padding-left: 30px;"><strong>UNIDAD 11:</strong> Integrales</p>
</td>
<td style="text-align: center; padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /> </strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank">UNIDADES: 7 Y 8</a></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank">UNIDAD 9</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank">UNIDAD 10</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Integrales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 1º BACH. MAT. CCSS.</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d; text-decoration: underline;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>1:</strong> Números Reales</p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 4:</strong> Ecuaciones, inecuaciones y sistemas</p>
<p class="MsoNormal" style="page-break-before: always; padding-left: 30px;"> </p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><strong><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="text-decoration: underline;"><span style="color: #2f310d;"><span style="background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></span></span></a></strong></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="text-align: center; padding-left: 30px;"> </p>
<p style="padding-left: 30px;"><strong>UNIDAD 5:</strong> Funciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 6:</strong> <span style="color: inherit; font-family: inherit; font-size: inherit;">Funciones elementales</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 7:</strong> Límite de una función</p>
<p style="padding-left: 30px;"><strong>UNIDAD 8:</strong> Derivada de una función</p>
<p style="padding-left: 30px;"><strong>UNIDAD 11:</strong> Probabilidad</p>
</td>
<td style="padding-left: 30px;">
<p style="padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 3</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 4</a><br /></span></p>
<hr style="padding-left: 30px;" />
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /> </strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank">UNIDAD 5</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank">UNIDAD 6</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank">UNIDAD 7</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank">UNIDAD 8</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Binomial_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div style="float: left; display: block; width: 98%; padding-left: 30px;"><strong style="color: #414141;"><span style="color: #2a2a2a; font-weight: normal;"> </span></strong></div>
<div style="float: left; display: block; width: 98%; padding-left: 60px;"><span style="text-decoration: underline;"><strong style="color: #414141;"><span style="color: #2a2a2a; text-decoration: underline;">Página personal del profesor:</span></strong></span><br />
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="font-size: 12pt;">Gerardo Bustos: <a href="http://perso.orange.es/gerbustgut59/" target="_blank" title="http://perso.gratisweb.com/gerardobustos/">entrar.</a></span></li>
</ul>
<p style="padding-left: 30px;"><strong><span style="font-size: 12pt;"><img src="images/stories/cervantes/qr_dep_mat.png" border="0" alt="Código qr del Departamento de Matemáticas." width="196" height="196" /></span></strong></p>
<p style="padding-left: 30px;">Código qr del Dep. de Matemáticas.</p>
</div>
<div class="fechaModif" style="text-align: center; padding-left: 30px;">Ultima actualización: 10/11/2013</div>
<div class="fechaModif" style="text-align: center;"><hr /></div>";s:4:"body";s:0:"";s:5:"catid";s:3:"100";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-12-02 13:29:56";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:3:"177";s:8:"ordering";s:1:"7";s:8:"category";s:13:"DEPARTAMENTOS";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:18:"51:dep-matematicas";s:7:"catslug";s:17:"100:departamentos";s:6:"author";N;s:6:"layout";s:7:"article";s:4:"path";s:98:"index.php?option=com_content&view=article&id=51:dep-matematicas&catid=100:departamentos&Itemid=103";s:10:"metaauthor";N;s:6:"weight";d:0.18668999999999999;s:7:"link_id";i:2064;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:13:"DEPARTAMENTOS";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:13:"DEPARTAMENTOS";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:47:"index.php?option=com_content&view=article&id=51";s:5:"route";s:98:"index.php?option=com_content&view=article&id=51:dep-matematicas&catid=100:departamentos&Itemid=103";s:5:"title";s:29:"Departamento de Matemáticas.";s:11:"description";s:15806:"ENLACES DIRECTOS A LAS SIGUIENTES SECCIONES: 2º Eso 1º BACH. MAT.I 1º BACH. MAT. CC.SS I 2º BACH. MAT. II 2º BACH. MAT. CC.SS II 2º BACH. ESTADÍSTICAhref="index.php?option=com_content&view=article&id=236&Itemid=103" target="_blank" title="PENDIENTES.""> PENDIENTES DE TODOS LOS CURSOS UN BUEN bLOG DE MATEMÁTICAS novedad: este curso 2014/15 contará con un grupo de estadística en 2º bachillerato cns. CURSO 2014/15 IMPORTANCIA DE ELEGIR ESTADÍSTICA COMO OPTATIVA EN 2º BACHILLERATO CIENCIAS El alumnado de Ciencias puede subir la nota de Selectividad en la Fase Específica mediante la realización de pruebas de distintas materias. En particular las Matemáticas II suele puntuar con una ponderación de 0,2 en la mayoría de los Grados de Ciencias (subir hasta 2 puntos la nota de la Fase General). El problema es que muchos alumnos se examinan de Matemáticas II en la Fase General al no disponer de otra materia mejor, según sus intereses. Es por ello que el Departamento de Matemáticas informa que este inconveniente se puede subsanar si elalumnado se examinara de Matemáticas CC SS II en la Fase General y de Matemáticas II en la Fase Específica. Como quiera que este alumnado de Ciencias sólo ha cursado la asignatura de Matemáticas II, existe una posibilidad de que completen los conocimientos que les faltan para poder dominar la materia de Matemáticas CC SS II mediante la asignatura de Estadística, que se puede elegir como optativa en 2º de Bachillerato Ciencias. Dada la importancia de obtener una buena nota final en Selectividad es por lo que el Departamento de Matemáticas anima al alumnado que actualmente está en fase de matriculación para que seleccione Estadística como optativa . Si se necesita más información se puede consultar a cualquier miembro del Departamento, o escribiendo un mensaje a iescervantesgranada[arroba]gmail.com que se le haría llegar a los mismos. A CONTINUACIÓN SEPUEDEN VER LOS DISTINTOS GRADOS QUE SE IMPARTEN EN LA UNIVERSIDAD DE GRANADA EN LOS QUE SE CURSA UNA ASIGNATURA DE ESTADÍSTICA: Diplomado en Enfermería Bioestadística Diplomado en Fisioterapia Bioestadística Diplomado en Gestión y Administración Pública (Melilla) EstadísticaII Diplomado en Relaciones Laborales Estadística Estadística Asistida por Ordenador Licenciado en Biología Bioestadística Fundamentos de Biología Aplicada I Licenciado en Ciencias de la Actividad Física y el Deporte Estadística Estadística Aplicada a la Actividad Físicamargin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;"> Licenciado en Ciencias y Técnicas Estadísticas Bioestadística Licenciado en Documentación Estadística Licenciado en Economía Predicción Económica y Empresarial Licenciado en Farmacia Matemática Aplicada Licenciado enMedicina Análisis Estadístico con Ordenador de Datos Médicos Bioestadística Licenciado en Odontología Estadística Aplicada Licenciado en Sociología Análisis Multivariante PENDIENTES DE MATEMÁTICAS PARASEPTIEMBRE DE 2014 PENDIENTES DE 1º ESO · EJERCICIOS PARA RESOLVER Y ENTREGAR TEMA 1 . Números naturales TEMA 2. Divisibilidad TEMA 3. Fracciones TEMA 4. Números decimalesTEMA 5. Números enteros TEMA 6. Iniciación al álgebra · EJERCICIOS PARA RESOLVER Y ENTREGAR TEMA 7. Sistema Métrico Decimal TEMA 8.'Century Gothic', sans-serif;"> Proporcionalidad numérica TEMA 9. Ángulos, circunferencias y círculos TEMA 10. Polígonos TEMA 11. Funciones y gráficas EJERCICIOS RESUELTOS PARA PRACTICAR: UNIDAD 1 . UNIDAD 2 . UNIDAD 3 . UNIDAD 4 . UNIDAD 5 . UNIDAD 6'Century Gothic', sans-serif;"> UNIDAD 7 . UNIDAD 8 . UNIDAD 9 . UNIDAD 10 . UNIDAD 11 PENDIENTES DE 2º ESO ·mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"> EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 1. Números enteros UNIDAD 2. Fracciones UNIDAD 3. Números decimales UNIDAD 4. Sistema sexagesimal UNIDAD 5. Expresiones algregráicas UNIDAD 6.sans-serif;"> Ecu acioens de primer y segundo grado · EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 7. Sistemas de ecuaciones UNIDAD 8. Proporcionalidad numéricainherit, serif;"> UNIDAD 9. Proporcionalidad geométrica UNIDAD 10. Figuras planas. Áreas UNIDAD 11. Cuerpos geométricos EJERCICIOS RESUELTOS PARA PRACTICAR: UNIDAD 1 UNIDAD 2style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"> UNIDADES 3 Y 4 UNIDAD 5 UNIDAD 6 UNIDAD 7 UNIDAD 8 UNIDAD 9 Y 10sans-serif;"> UNIDAD 11 PENDIENTES DE 3º ESO · EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 1. Números racionales UNIDAD 2. Números reales UNIDAD3. Polinomios UNIDAD 4. Ecuaciones de primer y segundo grado UNIDAD 5. Sistemas de ecuaciones UNIDAD 6. Proporcionalidad numérica · EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 7. Progresiones UNIDAD 8. Lugares geométricos.Figurasplanas UNIDAD 9. Cuerpos geométricos UNIDAD 10. Movimientos y semejanza UNIDAD 11. Funciones UNIDAD 12. Funciones lineales y afines EJERCICIOS RESUELTOS PARA PRACTICAR: UNIDADES 1 Y 2 UNIDAD 3 UNIDAD 4.75pt;"> UNIDAD 5 UNIDAD 6 UNIDAD: 7 UNIDAD: 8 UNIDAD 9 UNIDAD 10href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_y_graficas_resueltos.pdf" target="_blank"> UNIDAD 11 UNIDAD 12 PENDIENTES DE 1º BACH. MAT. CN. · EJERCICIOS PARA RESOLVER YENTREGAR UNIDAD 1: Números Reales UNIDAD 2: Ecuaciones, inecuaciones y sistemas UNIDAD 3: Trigonometría UNIDAD 4: Números Complejos UNIDAD 5: Geometría analítica · EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 7:sans-serif;"> Funciones UNIDAD 8: Funciones elementales UNIDAD 9: Límite de una función UNIDAD 10: Derivada de una función UNIDAD 11: Integrales EJERCICIOS RESUELTOS PARA PRACTICAR: UNIDAD: 1 UNIDAD: 2 UNIDAD 3style="color: #5d5d5d; letter-spacing: .75pt;"> UNIDAD 4 UNIDAD 5 UNIDADES: 7 Y 8 UNIDAD 9 UNIDAD 10 UNIDAD 11text-align: center;" align="center"> PENDIENTES DE 1º BACH. MAT. CCSS. · EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 1: Números Reales UNIDAD 3: Polinomios y fracciones algebraicas UNIDAD 4: Ecuaciones, inecuaciones y sistemaspage-break-before: always; padding-left: 30px;"> · EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 5: Funciones UNIDAD 6: Funciones elementales UNIDAD7: Límite de una función UNIDAD 8: Derivada de una función UNIDAD 11: Probabilidad EJERCICIOS RESUELTOS PARA PRACTICAR: UNIDAD 1 UNIDAD 3 UNIDAD 4 UNIDAD 5text-align: center;" align="center"> UNIDAD 6 UNIDAD 7 UNIDAD 8 UNIDAD 11 LIBROS DE TEXTO DEL DEPARTAMENTO DE MATEMÁTICAS, CURSO 2014/15:30px;"> CURSO Materia Editorial EAN 1º ESOborder-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142"> Matemáticas Santillana Grazalema, Sl 9788483052570 2º ESO Matemáticas0cm 5.4pt 0cm 35px;" valign="top" width="217"> Santillana Grazalema, Sl 9788429438215 3º ESO Matemáticas Santillana Grazalema, Sl 9788483052587style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142"> 4º ESO Matemáticas A Santillana Grazalema, Sl 9788483052112 4º ESOborder-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142"> Matemáticas B Santillana Grazalema, Sl 9788483052242 1º BACHILLERATO: Matemáticas I Matemáticas Iborder-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217"> Santillana La casa del saber (González y otros) 1º BACHILLERATO: Matemáticas CCSS I Matemáticas aplicadas a las CCSS Ipadding-left: 30px;"> Santillana. La casa del Saber (Escoredo y otros) 2º BACHILLERATO: Matemáticas II Matemáticas II Santillana. Matemáticas II. La casa del saber (González y otros)border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142"> 2º BACHILLERATO: Matemáticas CCSS II Matemáticas aplicadas a las CCSS II Santillana. Matemáticas aplicadas a las CCSS II. La casa del saber (Escoredo y otros)1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142"> CURSO 2013/14 Primera reunión de Coordinación para Selectividad de Matemáticas CC SS II Ver enlace Primera reunión de Coordinación para Selectividad de Matemáticas II Ver enlacestyle="text-decoration: underline;"> 20/12/13: Los desafíos matemáticos vuelven a esta casa por Navidad. Como ya sucedió el año pasado, con motivo del sorteo de la Lotería de Navidad ofrecemos un nuevo problema. El encargado de presentar el Desafío Extraordinario de Navidad de 2013 es Javier Cilleruelo, profesor de la Universidad Autónoma de Madrid y miembro del Instituto de Ciencias Matemáticas (ICMAT). Entre los acertantes se sorteará una biblioteca matemática como la que ofreció EL PAÍS en el quiosco durante 2011. El ganador recibirá, además, el libro 'Desafíos Matemáticos' por cortesía de la Real Sociedad Matemática Española , una publicación de SM en la que se recogen los 40 desafíos matemáticos que ofrecimos en la web y con los que EL PAÍS y la RSME celebraron el centenario de esta institución hace dos años. Manda tu respuesta antes de las 00.00 horas del domingo 22 de diciembre (la medianoche del sábado al domingo, hora peninsular española) a problemamatematicas@gmail.com y participa en el sorteo. A continuación, para aclarar dudas y en atención a nuestros lectores sordos, añadimos el enunciado del problema por escrito: El equipo que preparamos los desafíos matemáticos hemos decidido abonarnos durante todo el año a un número de la Lotería. Para elegir ese número, que debe estar comprendido entre el 0 y el 99.999, pusimos como condición que tuviese las cinco cifras distintas y que, además, cumpliese alguna otra propiedad interesante. Finalmente hemos conseguido unnúmero que tiene la siguiente propiedad: si numeramos los meses del año del 1 al 12, en cualquier mes del año ocurre que al restar a nuestro número de lotería el número del mes anterior, el resultado es divisible por el número del mes en el que estemos. Y esto sucede para cada uno de los meses del año. Es decir, si llamamos L a nuestro número, tenemos por ejemplo que en marzo L-2 es divisible entre 3 y en diciembre L-11 es divisible entre 12. El reto que os planteamos es que nos digáis a qué número de Lotería estamos abonados y que nos expliquéis cómo lo habéis encontrado. OBSERVACIONES IMPORTANTES. Insistimos en que es importante que hagáis llegar junto con el número el razonamiento de cómo lo habéis hallado. No se considerarán válidas las respuestas que den sólo el número o que lo hayan encontrado probando todos uno a uno (a mano o con un ordenador). PROGRAMACIÓN DE MATEMÁTICAS, CURSO 2013/14. FECHAS DE EXÁMENES: PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES.href="archivos_ies/13_14/2bach_matccssii/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2013_2014.pdf" target="_blank"> Directrices y orientaciones Selectividad Mat. CC.SS. II curso 2013/14. 12/11/2012: Coordinación de Selectividad de Mat. CC SS II del 05/11/12.. 09/11/2012: Examen y soluciones de Selectividad de MAT_CC_SS_II de sept. 2012. 20/06/2012: Exámen y soluciones de Selectividad de MAT_CC_SS_II de jun. 2012, Criterios de evaluación de Matemáticas 1º ESO. Criterios de evaluación deMatemáticas 2º ESO. Criterios de evaluación de Matemáticas 3º ESO. Criterios de evaluación de Matemáticas 4º ESO.url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"> Criterios de evaluación de Matemáticas 1º Bachillerato CCSS I. Criterios de evaluación de Matemáticas I 1 º Bachillerato CNAT. Criterios de evaluación destyle="color: #336600;"> Matemáticas 2 º Bachillerato CCSS II. Criterios de evaluación de Matemáticas II 2 º Bachillerato CNAT . ENLACE A ZONA DE 2º BACH. CC.SS. II. ENLACE A ZONA DE 2º BACH. CC.NN. II. PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES:30px;"> Pendientes de 1º ESO EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 17/01/2014 TEMA 1 . Números naturales TEMA 2. Divisibilidad TEMA 3. Fracciones TEMA 4. Números decimales TEMA 5. Números enteros TEMA 6. Iniciación al álgebra EJERCICIOS PARA RESOLVER YENTREGAR SEGUNDO EXAMEN: 28/03/2014 TEMA 7. Sistema Métrico Decimal TEMA 8. Proporcionalidad numérica TEMA 9. Ángulos, circunferencias y círculos TEMA 10. Polígonos TEMA 11. Funciones y gráficas EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD 1 UNIDAD 2 UNIDAD 3 UNIDAD 4left;"> UNIDAD 5 UNIDAD 6 EXAMEN 2 UNIDAD 7 UNIDAD 8 UNIDAD 9 UNIDAD 10 UNIDAD 11 Pendientes de 2º ESO EJERCICIOS PARA RESOLVER Y ENTREGARpadding-left: 30px;"> PRIMER EXAMEN: 17/01/2014 UNIDAD 1. Números enteros UNIDAD 2. Fracciones UNIDAD 3. Números decimales UNIDAD 4. Sistema sexagesimal UNIDAD 5. Expresiones algregráicas UNIDAD 6. Ecu acioens de primer y segundo grado EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 28/03/2014 UNIDAD 7. Sistemas de ecuacionesinherit;"> UNIDAD 8. Proporcionalidad numérica UNIDAD 9. Proporcionalidad geométrica UNIDAD 10. Figuras planas. Áreas UNIDAD 11. Cuerpos geométricos EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD 1 UNIDAD 2 UNIDADES 3 Y 4 UNIDAD 5inherit;"> UNIDAD 6 EXAMEN 2 UNIDAD 7 UNIDAD 8 UNIDAD 9 Y 10 UNIDAD 11 Pendientes de 3º ESO EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 17/01/2014 UNIDAD 1. Números racionales UNIDAD 2. Números reales UNIDAD 3. Polinomiosstyle="padding-left: 30px;"> UNIDAD 4. Ecuaciones de primer y segundo grado UNIDAD 5. Sistemas de ecuaciones UNIDAD 6. Proporcionalidad numérica EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 28/03/2014 UNIDAD 7. Progresiones UNIDAD 8. Lugares geométricos. Figuras planas UNIDAD 9. Cuerpos geométricos . UNIDAD 10. Movimientos y semejanza UNIDAD 11. Funciones UNIDAD 12. Funciones lineales y afines EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDADES 1 Y 2target="_blank"> UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDAD 6 EXAMEN 2 UNIDAD: 7 UNIDAD: 8 UNIDAD 9 UNIDAD 10 UNIDAD 11left;"> UNIDAD 12 Pendientes de 1º BACH. MAT. CN. EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 17/01/2014 UNIDAD 1: Números Reales UNIDAD 2: Ecuaciones, inecuaciones y sistemas UNIDAD 3: Trigonometría UNIDAD 4: Números Complejos UNIDAD 5: Geometría analítica EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 28/03/2014 UNIDAD 7: Funciones UNIDAD 8: Funciones elementales UNIDAD 9: Límite de unafunción. UNIDAD 10: Derivada de una función UNIDAD 11: Integrales EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD: 1 UNIDAD: 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 EXAMEN 2 UNIDADES: 7 Y 8href="archivos_ies/13_14/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank"> UNIDAD 9 UNIDAD 10 UNIDAD 11 Pendientes de 1º BACH. MAT. CCSS. EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 17/01/2014 UNIDAD 1: Números Reales UNIDAD 3: Polinomios y fracciones algebraicas UNIDAD 4: Ecuaciones, inecuaciones y sistemastarget="_blank"> EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 28/03/2014 UNIDAD 5: Funciones UNIDAD 6: Funciones elementales UNIDAD 7: Límite de una función UNIDAD 8: Derivada de una función UNIDAD 11: Probabilidad EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD 1 UNIDAD 3 UNIDAD 4 EXAMEN 2left;"> UNIDAD 5 UNIDAD 6 UNIDAD 7 UNIDAD 8 UNIDAD 11 Página personal del profesor: Gerardo Bustos: entrar. Código qr del Dep. deMatemáticas. Ultima actualización: 10/11/2013";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2011-01-12 00:03:45";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2011-01-09 12:03:45";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}}";
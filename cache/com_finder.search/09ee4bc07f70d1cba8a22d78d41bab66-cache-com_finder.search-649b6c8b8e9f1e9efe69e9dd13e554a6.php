<?php die("Access Denied"); ?>#x#s:33408:"a:1:{i:0;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"236";s:5:"alias";s:39:"pendientes-de-matematicas-curso-2013-14";s:7:"summary";s:24032:"<p> </p>
<hr />
<div id="contenido_sitio" style="text-align: justify; background-color: #e7eaad; float: left; width: 793.796875px;">
<h2><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES:</span></strong></span></h2>
<table style="width: 90%;" border="0">
<tbody>
<tr>
<td style="font-family: arial,sans-serif; margin: 0px;">
<div id="contenido_sitio" style="font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13.3333339691162px; text-align: justify; float: left; width: 793.796875px; background-color: #e7eaad;">
<p style="font-size: 13.3333px; color: inherit; padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">CURSO 215/16:</span></strong></span></p>
<p style="font-size: 13.3333px; color: inherit;"> </p>
<ul style="font-size: 13.3333px; color: inherit;">
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_1_Cal.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Pendientes de 1º y 2º ESO.</span></a></strong></li>
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_2_Cal.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Pendientes de 3º ESO y 1º Bachillerato.</span></a></strong></li>
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_3_materia.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Distribución de materia de alumnos pendientes de cursos anteriores.</span></a></strong></li>
</ul>
</div>
</td>
</tr>
</tbody>
</table>
<table class="borde_outset" style="text-align: center; color: #2a2a2a;" border="1">
<tbody>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" title="Pulsar para descargar." style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a>,</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,11,12,13,19,21,28.</li>
<li><strong>Tema 2:</strong> 1,6,8,14,16,17,18,19,20,23.</li>
<li><strong>Tema 3:</strong> 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43.</li>
<li><strong>Tema 4:</strong> Todos.</li>
<li><strong>Tema 5:</strong> 1,2,5,8,9,10,11,12,13.</li>
<li><strong>Tema 6:</strong> 1,2,8,11,29,31,32.</li>
</ul>
</ul>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 18/01/2016</strong></span></p>
<p><strong>TEMA 1</strong>. Números naturales</p>
<p><strong>TEMA 2.</strong> Divisibilidad</p>
<p><strong>TEMA 3.</strong> Fracciones</p>
<p><strong>TEMA 4.</strong>  Números decimales</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p><span style="font-family: Arial, sans-serif;"> </span></p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR.  </a></strong></span><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">Solamente los siguientes números por tema:</a>
<ul>
<li><strong>Tema 7:</strong> 1,4,6,7,8,9,10,11,12,13.</li>
<li><strong>Tema 8:</strong> 2,3,6,7,9,11,12,13,14,15.</li>
<li><strong>Tema 9: TODOS.</strong></li>
<li><strong>Tema 10:</strong> 1,3,4,5,6,7,8,11,13,14.</li>
<li><strong>Tema 11:</strong> <strong>TODOS.</strong></li>
</ul>
</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 04/04/2016</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p><strong>TEMA 10.</strong> Polígonos</p>
<p><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p> </p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1</strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/15_16/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a><hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<a href="archivos_ies/15_16/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 2º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span><span style="font-size: 13px;"> Solamente los siguientes números por tema:</span></li>
<ul>
<li><strong>Tema 1:</strong> 1,6,9,10,12,16,17,18.</li>
<li><strong>Tema 2:</strong>  1,3,5,6,7, 11,13,15,16.</li>
<li><strong>Tema 3:</strong> 1,2,5,9,11,12,16,17,18,20.</li>
<li><strong>Tema 4:</strong>  1,5,8,11,12,13,16,19.</li>
<li><strong>Tema 5:</strong>  1,2,3,4,7,8,9, 10,11,12.</li>
<li></li>
</ul>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 21/01/2016</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números enteros</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>2.</strong> Fracciones</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>3.</strong> Números decimales</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>4.</strong> Sistema sexagesimal</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>5.</strong> Expresiones algebráicas</p>
<p> </p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/14_15/mate/pendientes_2_eso_parte_2c.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> (Actualizado 13/10/15)</strong></span></li>
<li><strong>Y los del Tema 6:</strong>  1,6,7,8,9,13,14,15.</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 07/04/2016</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>6.</strong><span style="font-weight: normal;"> Ecu</span><span style="color: inherit; font-family: inherit; font-size: inherit;">aciones de primer y segundo grado</span></strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>7.</strong> Sistemas de ecuaciones</span></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>8.</strong> Proporcionalidad numérica</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>9.</strong> Proporcionalidad geométrica</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>10.</strong> Figuras planas. Áreas</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank">UNIDADES 3 Y 4</a></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<br /><hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-decoration: underline;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank">UNIDAD 9 Y 10</a></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 3º ESO</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></span></span><span style="color: #2f310d;"> extraídos del siguiente DOCUMENTO COMPLETO.</span><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_1_a_5.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></span></strong></a></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 18/01/2016</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números racionales</p>
<p><span style="font-size: inherit;"><strong>UNIDAD 2.</strong> Números reales</span></p>
<p><strong>UNIDAD 3.</strong> Polinomios</p>
<p><strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado </p>
<p><strong>UNIDAD 5.</strong> Sistemas de ecuaciones</p>
<p><strong> </strong></p>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> </span></span></strong><strong><span style="color: #2f310d;">extraídos del siguiente DOCUMENTO COMPLETO.</span></strong><br /><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_6_a_10.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></span></strong></a></li>
<li></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 04/04/2016</strong></span></p>
<p><strong>UNIDAD 6.</strong> Proporcionalidad numérica</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 7.</strong> Progresiones</span></p>
<p><strong>UNIDAD 8.</strong> Lugares geométricos. Figuras planas</p>
<p><strong>UNIDAD 9.</strong> Cuerpos geométricos .</p>
<p><strong> </strong></p>
</td>
<td>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDADES 1 Y 2</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<hr />
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span> </a></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank">UNIDAD: 7</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank">UNIDAD: 8</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º BACH. MAT. CCNN.</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/1bach_cnat_ptes_examen1.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a><br /></span></span></strong></li>
</ul>
<p style="text-align: center;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;"><span style="text-decoration: underline;">PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM</span><br /></strong></p>
<p><strong>UNIDAD 1:</strong> Números Reales</p>
<p><strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas</p>
<p><strong>UNIDAD 3:</strong> Trigonometría</p>
<p><strong>UNIDAD 4:</strong> Números Complejos</p>
<p><strong>UNIDAD 5:</strong> Geometría analítica</p>
<p><strong>UNIDAD 6: </strong>Cónicas</p>
</td>
<td>
<ul>
<li><strong><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/1bach_cnat_ptes_examen2.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a><br /></span></strong></li>
</ul>
<p style="text-align: center;"><strong><span style="text-decoration: underline;">SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM</span><br /></strong></p>
<p><strong>UNIDAD 7:</strong> Funciones</p>
<p><strong>UNIDAD 8:</strong> Funciones elementales</p>
<p><strong>UNIDAD 9:</strong> Límite de una función.</p>
<p><strong>UNIDAD 10:</strong> Derivada de una función</p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 2</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 6<br /></span></a></p>
<hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank">UNIDADES: 7 Y 8</a></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank">UNIDAD 10</a></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º BACH. MAT. CCSS.</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></span></li>
</ul>
<p style="text-align: center;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;"><span style="text-decoration: underline;">PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM</span></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>1:</strong> Números Reales</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 2</strong><strong>:</strong> Aritmética Mercantil</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span></p>
<p><strong>UNIDAD 4:</strong> Ecuaciones, inecuaciones y sistemas</p>
<p class="MsoNormal" style="page-break-before: always;"> </p>
</td>
<td>
<ul>
<li><strong><a href="archivos_ies/15_16/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></li>
</ul>
<p style="text-align: center;"><strong><span style="text-decoration: underline;">SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM</span></strong></p>
<p style="text-align: center;"> </p>
<p><strong>UNIDAD 5:</strong> Funciones</p>
<p><strong>UNIDAD 6:</strong> <span style="color: inherit; font-family: inherit; font-size: inherit;">Funciones elementales</span></p>
<p><strong>UNIDAD 7:</strong> Límite de una función</p>
<p><strong>UNIDAD 8:</strong> Derivada de una función</p>
<p><strong>UNIDAD 9:</strong> Estadística unidimensional</p>
</td>
<td>
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/14_15/mate/Aritmetica_mercantil_resueltos.pdf" target="_blank"><span style="text-align: left;"><span style="text-align: left;">UNIDAD 2</span></span></a></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 3</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 4</a><br /></span></p>
<hr />
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank">UNIDAD 5</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank">UNIDAD 6</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/matematicas/Estadistica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 9</span></a></p>
</td>
</tr>
</tbody>
</table>
</div>
<div style="text-align: justify; background-color: #e7eaad; float: left; width: 793.796875px;"><strong style="color: #414141;"><span style="color: #2a2a2a; font-weight: normal;"> </span></strong></div>";s:4:"body";s:0:"";s:5:"catid";s:3:"129";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-10-26 18:13:27";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"34";s:8:"ordering";s:1:"2";s:8:"category";s:12:"Matemáticas";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:43:"236:pendientes-de-matematicas-curso-2013-14";s:7:"catslug";s:17:"129:matematicas11";s:6:"author";s:14:"Miguel Anguita";s:6:"layout";s:7:"article";s:4:"path";s:123:"index.php?option=com_content&view=article&id=236:pendientes-de-matematicas-curso-2013-14&catid=129:matematicas11&Itemid=671";s:10:"metaauthor";N;s:6:"weight";d:0.88668999999999998;s:7:"link_id";i:2034;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:14:"Miguel Anguita";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:14:"Miguel Anguita";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"Matemáticas";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"Matemáticas";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=236";s:5:"route";s:123:"index.php?option=com_content&view=article&id=236:pendientes-de-matematicas-curso-2013-14&catid=129:matematicas11&Itemid=671";s:5:"title";s:41:"Pendientes de Matemáticas. Curso 2015/16";s:11:"description";s:4484:"PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES: CURSO 215/16: Pendientes de 1º y 2º ESO. Pendientes de 3º ESO y 1º Bachillerato. Distribución de materia de alumnos pendientes de cursos anteriores. PENDIENTES DE 1º ESOstyle="text-decoration: underline;"> EJERCICIOS PARA RESOLVER Y ENTREGAR , Solamente los siguientes números por tema: Tema 1: 1,11,12,13,19,21,28. Tema 2: 1,6,8,14,16,17,18,19,20,23. Tema 3: 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43. Tema 4: Todos. Tema 5: 1,2,5,8,9,10,11,12,13. Tema 6: 1,2,8,11,29,31,32. PRIMER EXAMEN: 18/01/2016 TEMA 1 . Números naturales TEMA 2. Divisibilidad TEMA 3. Fracciones TEMA 4. Números decimales TEMA 5. Números enteros TEMA 6. Iniciación al álgebra EJERCICIOS PARA RESOLVER Y ENTREGAR. Solamente los siguientes números por tema: Tema 7: 1,4,6,7,8,9,10,11,12,13. Tema 8: 2,3,6,7,9,11,12,13,14,15. Tema 9:TODOS. Tema 10: 1,3,4,5,6,7,8,11,13,14. Tema 11: TODOS. SEGUNDO EXAMEN: 04/04/2016 TEMA 7. Sistema Métrico Decimal TEMA 8. Proporcionalidad numérica TEMA 9. Ángulos, circunferencias y círculos TEMA 10. Polígonos TEMA 11. Funciones y gráficas EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD 1 UNIDAD 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDAD 6 EXAMEN 2href="archivos_ies/15_16/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank"> UNIDAD 7 UNIDAD 8 UNIDAD 9 UNIDAD 10 UNIDAD 11 PENDIENTES DE 2º ESO EJERCICIOS PARA RESOLVER Y ENTREGAR Solamente los siguientes números por tema: Tema 1: 1,6,9,10,12,16,17,18. Tema 2: 1,3,5,6,7, 11,13,15,16. Tema 3: 1,2,5,9,11,12,16,17,18,20. Tema 4: 1,5,8,11,12,13,16,19. Tema 5: 1,2,3,4,7,8,9, 10,11,12. PRIMER EXAMEN: 21/01/2016 UNIDAD 1. Números enteros UNIDAD 2. Fraccionesinherit;"> UNIDAD 3. Números decimales UNIDAD 4. Sistema sexagesimal UNIDAD 5. Expresiones algebráicas EJERCICIOS PARA RESOLVER Y ENTREGAR (Actualizado 13/10/15) Y los del Tema 6: 1,6,7,8,9,13,14,15. SEGUNDO EXAMEN: 07/04/2016 UNIDAD 6. Ecu aciones de primer y segundo grado UNIDAD 7. Sistemas de ecuaciones UNIDAD 8. Proporcionalidad numérica UNIDAD 9. Proporcionalidad geométrica UNIDAD 10. Figuras planas. Áreasstyle="text-align: center;"> EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD 1 UNIDAD 2 UNIDADES 3 Y 4 UNIDAD 5 EXAMEN 2 UNIDAD 6 UNIDAD 7 UNIDAD 8 UNIDAD 9 Y 10 PENDIENTES DE 3º ESO EJERCICIOS PARA RESOLVER YENTREGAR extraídos del siguiente DOCUMENTO COMPLETO. DESCARGAR DOCUMENTO COMPLETO. PRIMER EXAMEN: 18/01/2016 UNIDAD 1. Números racionales UNIDAD 2. Números reales UNIDAD 3. Polinomios UNIDAD 4. Ecuaciones de primer y segundo grado UNIDAD 5. Sistemas de ecuaciones EJERCICIOS PARA RESOLVER Y ENTREGAR extraídos del siguiente DOCUMENTO COMPLETO. DESCARGAR DOCUMENTO COMPLETO. SEGUNDO EXAMEN: 04/04/2016 UNIDAD 6. Proporcionalidad numéricafont-size: inherit;"> UNIDAD 7. Progresiones UNIDAD 8. Lugares geométricos. Figuras planas UNIDAD 9. Cuerpos geométricos . EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDADES 1 Y 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 EXAMEN 2 UNIDAD 6 UNIDAD: 7 UNIDAD: 8href="archivos_ies/15_16/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank"> UNIDAD 9 PENDIENTES DE 1º BACH. MAT. CCNN. EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM UNIDAD 1: Números Reales UNIDAD 2: Ecuaciones, inecuaciones y sistemas UNIDAD 3: Trigonometría UNIDAD 4: Números Complejos UNIDAD 5: Geometría analítica UNIDAD 6: Cónicas EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM UNIDAD 7: Funciones UNIDAD 8: Funciones elementales UNIDAD 9: Límite de una función. UNIDAD 10: Derivada de una función EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1style="text-align: left;"> UNIDAD: 1 UNIDAD: 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDAD 6 EXAMEN 2 UNIDADES: 7 Y 8 UNIDAD 9 UNIDAD 10 PENDIENTES DE 1º BACH. MAT. CCSS. EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 20/01/2016 - 11.15 horas en elSUM UNIDAD 1: Números Reales UNIDAD 2 : Aritmética Mercantil UNIDAD 3: Polinomios y fracciones algebraicas UNIDAD 4: Ecuaciones, inecuaciones y sistemas EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM UNIDAD 5: Funciones UNIDAD 6: Funciones elementales UNIDAD 7: Límite de una función UNIDAD 8: Derivada de una función UNIDAD 9: Estadística unidimensional EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD 1 UNIDAD 2center;"> UNIDAD 3 UNIDAD 4 EXAMEN 2 UNIDAD 5 UNIDAD 6 UNIDAD 7 UNIDAD 8 UNIDAD 9";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2013-11-10 19:46:30";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2013-11-10 19:46:30";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}}";
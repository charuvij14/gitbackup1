<?php die("Access Denied"); ?>#x#s:905401:"a:18:{i:0;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"259";s:5:"alias";s:14:"selectividad-3";s:7:"summary";s:117866:"<p> </p>
<table class="tabla_redonda" style="border: 1px solid #ced5d7; border-top-left-radius: 6px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 6px; padding: 10px 15px; box-shadow: #b5c1c5 0px 5px 10px, #eef5f7 0px 0px 0px 10px inset; color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left; background-color: #ffffff;" width="98%" border="0"><caption class="titulo" style="font-weight: bold; color: #ff9900; font-size: 16px; border: 1px solid #aaaaaa; vertical-align: middle; height: 45px; margin: 10px auto; -webkit-box-shadow: black 0px 0px 4px; box-shadow: black 0px 0px 4px; text-shadow: black 1px 1px 1px; padding-top: 8px; width: 95%; background: #e8edff;">CALENDARIO DE LAS PRUEBAS DE ACCESO A LA UNIVERSIDAD CURSO 2014/15<br />LUGAR: FACULTAD DE MEDICINA, AVDA. DE MADRID<br /><br /></caption></table>
<h3 style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-transform: none;" align="center"><strong>CURSO 2014/2015</strong></h3>
<table style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;" width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td align="left"><strong>Convocatoria ordinaria:</strong> <br />16, 17, 18 de junio de 2015</td>
<td align="right"><strong>Convocatoria extraordinaria:</strong> <br />15, 16 y 17 de septiembre de 2015</td>
</tr>
</tbody>
</table>
<p> </p>
<table style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;" width="90%" border="0" cellspacing="2" cellpadding="3" align="center">
<tbody>
<tr>
<td align="center" bgcolor="#D8D8D8">HORARIO</td>
<td align="center" bgcolor="#F3F781" width="25%">PRIMER DÍA<br /><strong>SE RECOMIENDA LLEGAR DE 7 A 7.30 EL PRIMER DÍA</strong></td>
<td align="center" bgcolor="#F7BE81" width="25%">SEGUNDO DÍA</td>
<td align="center" bgcolor="#00FF80" width="25%">TERCER DÍA</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td align="center" bgcolor="#D8D8D8">HORARIO</td>
<td align="center" bgcolor="#CEE3F6" width="25%">TERCER DÍA (TARDE)<br />Exclusivo para incompatabilidades</td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">08:00-08:30</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td align="center" bgcolor="#D8D8D8">16:00-16:30</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">08:30-10:00</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>HISTORIA DEL ARTE</li>
<li>MATEMÁTICAS II</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>ANÁLISIS MUSICAL II</li>
<li>DISEÑO</li>
<li>GEOGRAFÍA</li>
<li>BIOLOGÍA</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">16:30-18:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">10:00-10:45</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">18:00-18:30</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">10:45-12:15</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>HISTORIA DE ESPAÑA</li>
<li>HISTORIA DE LA FILOSOFÍA</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS</li>
<li>QUÍMICA</li>
<li>ELECTROTÉCNIA</li>
<li>LITERATURA UNIVERSAL</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>DIBUJO TÉCNICO II</li>
<li>CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES</li>
<li>ECONOMÍA DE LA EMPRESA</li>
<li>GRIEGO II</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">18:30-20:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">12:15-13:00</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">20:00-20:30</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">13:00-14:30</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>IDIOMA EXTRANJERO</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>LENGUAJE Y PRÁCTICA MUSICAL</li>
<li>TECNOLOGÍA INDUSTRIAL II</li>
<li>MATEMÁT. APLIC. A LAS CC. SOCIALES II</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>HISTORIA DE LA MÚSICA Y LA DANZA</li>
<li>DIBUJO ARTÍSTICO II</li>
<li>FÍSICA</li>
<li>LATÍN II</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">20:30-22:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
</tbody>
</table>
<p> </p>
<p class="texto_azul" style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; line-height: 16.6399993896484px;"><strong>NOTAS IMPORTANTES:</strong></p>
<ol style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>Las franjas horarias de citación son en defecto de que la universidad no fije otras que en razón de las sedes de que se traten, considere más oportunas; asi mismo sí la última franja horaria del tercer día para incompatibilidades, quedase libre, el descanso entre la primera y segunda franja horaria será de 45 minutos.</li>
<li>El horario de las materias del tercer día por la tarde es <strong><span style="text-decoration: underline;">exclusivamente</span></strong> para quienes tienen más de un examen el mismo día y a la misma hora (en el 2º o 3º día por la mañana), debiendo realizar en dicho horario y en primer lugar el examen que aparece antes en el respectivo cuadro y realizará el examen de la otra materia en el tercer día por la tarde en el horario que se indicará oportunamente.</li>
</ol>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px; background-color: #ffffff; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; text-align: center;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><span style="text-decoration: underline;"><span style="color: #2a2a2a;"><span style="text-transform: none;"><span style="font-size: 14pt;"><strong>SELECTIVIDAD SEPTIEMBRE 2014<br /><br /><a href="index.php?option=com_content&amp;view=article&amp;id=262kYWQiXQ==" target="_blank">V E R   E X Á M E N E S: D Í A S 16 - 17 - 18 S E P T I E M B R E<br /></a><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Las notas están disponibles el día 24</a><br /></strong></span></span> </span> </span><a href="archivos_ies/14_15/mate/Normas_PAU.pdf" target="_blank"><span style="color: #2a2a2a; font-size: 12pt;"><span style="text-transform: none;">Normas de las PAU</span></span></a><span style="text-decoration: underline;"><br /> </span><span style="font-family: verdana,geneva;"><span style="color: #2a2a2a; font-size: 10pt;"><span style="text-transform: none;"><span style="color: #000000; background-color: #ffffff;">SEDE 5 (GRANADA) FACULTAD DE MEDICINA<br />AVDA. DE MADRID S/N</span></span> </span><span style="color: #2a2a2a; font-size: 12pt;"><span style="text-transform: none;"><br /><a href="index.php?option=com_content&amp;view=article&amp;id=254">Ver más información</a> </span> </span> </span> </span></p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px; background-color: #ffffff; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; text-align: center;"><span style="text-decoration: underline; font-size: 14pt; color: #000000;"><strong>SELECTIVIDAD JUNIO<a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><span style="color: #000000; text-decoration: underline;"> 2014:</span> </a> </strong> </span><br /><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO.">ENHORABUENA A NUESTRO ALUMNADO, HA SUPERADO LAS SELECTIVIDAD UN 98,57 % (69 DE 70 ALUMNOS PRESENTADOS DE BACHILLERATO Y LOS 3 DEL CICLO)</a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."> </a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO.">ESTUDIO ESTADÍSTICO DE LAS NOTAS OBTENIDAS POR EL ALUMNADO DEL</a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."> IES MIGUEL DE CERVANTES</a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/pau.png" border="0" alt="" width="407" height="305" /></a><strong style="background-color: transparent; text-align: center;"><strong style="background-color: transparent; text-align: center;"><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."> <img src="archivos_ies/13_14/selectividad/comunes1.png" border="0" alt="" style="border: 0pt none;" /></a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/gen-esp.png" border="0" alt="" width="549" height="546" /></a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/dosfases.png" border="0" alt="" width="549" height="260" style="border: 0pt none;" /></a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/aprobados1.png" border="0" alt="" style="border: 0pt none;" /></a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/medias3.png" border="0" alt="" width="551" height="375" style="border: 0pt none;" /></a><br />__________________________________________________________________________________<br /></strong></strong>(Análisis realizado por Miguel Anguita)</p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px; background-color: #ffffff; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: center;"><span style="color: #5d5d5d; font-size: 31px; font-weight: bold; text-align: left; text-transform: uppercase; background-color: #fafbf0;">INSCRIPCIÓN PRUEBAS DE ACCESO A LA UNIVERSIDAD</span></p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px; background-color: #ffffff; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: center;"><a id="enlace_pdf" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion?theme=pdf" title="Versión en PDF" style="font-size: 13px; background-color: #fafbf0;">Descargar versión en PDF<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_requisitos" style="font-size: 13px; background-color: #fafbf0;">Requisitos<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_plazos_y_solicitud" style="font-size: 13px; background-color: #fafbf0;">Plazos y solicitud<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_precios_de_inscripcion" style="font-size: 13px; background-color: #fafbf0;">Precios de inscripción<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_hoja_informativa" style="font-size: 13px; background-color: #fafbf0;">Hoja informativa<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_adaptacion_de_los_examenes_para_estudiantes_con_discapacidad" style="font-size: 13px; background-color: #fafbf0;">Adaptación de los exámenes para estudiantes con discapacidad</a></p>
<div id="pagina">
<div id="contenido" class="sec_interior">
<div class="content_doku">
<h1>REQUISITOS</h1>
<div class="level1">
<ul>
<li class="level1">
<div class="li">Estar en posesión del <strong>Título de Bachillerato</strong> para la realización de la prueba completa.</div>
</li>
<li class="level1">
<div class="li">Estar en posesión del <strong>Título de técnico superior de formación profesional, técnico superior de artes plásticas y diseño, o técnico deportivo superior o título equivalente</strong>, únicamente a efectos de la <em class="u">fase específica voluntaria</em> para mejorar la nota de admisión a las enseñanzas oficiales de grado.</div>
</li>
</ul>
</div>
<h1>PLAZOS Y SOLICITUD</h1>
<div class="level1">
<p>Todos los/as estudiantes que estén interesados en participar en la Prueba de Acceso a la Universidad tanto en su fase general como específica, ya sean alumnos/as que se presentan por primera vez o para mejorar nota, en cualquiera de sus convocatorias Ordinaria o Extraordinaria, deberán realizar los siguientes pasos en los plazos establecidos:</p>
</div>
<h2>PRIMER PASO: REGISTRARSE</h2>
<div class="level2">
<p>Hay que cumplimentar el trámite de <strong>REGISTRO</strong> a través del portal WEB de Selectividad:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a class="urlextern" href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" title="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></div>
</li>
</ul>
<p><strong>PLAZO DE REGISTRO</strong> SELECTIVIDAD CONVOCATORIA 2014:</p>
<table class="inline">
<tbody>
<tr><th>Convocatoria Ordinaria</th><th>Convocatoria Extraordinaria</th></tr>
<tr>
<td class="leftalign">del 22 de abril al 4 de junio 2014</td>
<td class="leftalign">del 1 de agosto al 5 de septiembre 2014</td>
</tr>
</tbody>
</table>
<p>Aquí puedes encontrar una presentación que te ayudará a realizar el registro:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad1" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad1">Alumnos que este curso aprobarán el 2º de Bachillerato o el 2º curso del CFGS en un centro de Granada, Ceuta, Melilla o centros adscritos a la Universidad de Granada en Marruecos</a></div>
</li>
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad2" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad2">Alumnos que se han presentado alguna vez a Selectividad en la Universidad de Granada</a></div>
</li>
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad3" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad3">Alumnos procedentes de otras universidades y que nunca se han presentado a Selectividad en la Universidad de Granada</a></div>
</li>
</ul>
</div>
<h2>SEGUNDO PASO: MATRICULARSE</h2>
<div class="level2">
<p>Una vez hecho el registro hay que realizar la inscripción (<strong>MATRÍCULA</strong>) en la prueba a través del portal WEB de Selectividad:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a class="urlextern" href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" title="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></div>
</li>
</ul>
<p><strong>PLAZO DE MATRÍCULA</strong> SELECTIVIDAD CONVOCATORIA 2014:</p>
<table class="inline">
<tbody>
<tr><th>Convocatoria Ordinaria</th><th>Convocatoria Extraordinaria</th></tr>
<tr>
<td class="leftalign">2 al 4 de junio 2014</td>
<td class="leftalign">3 al 5 de septiembre 2014</td>
</tr>
</tbody>
</table>
<ul class="departamento">
<li class="level1 level1_primero impar">
<div class="li"><strong>MUY IMPORTANTE:</strong></div>
<ul>
<li class="level4">
<div class="li"><strong>La matrícula se podrá realizar hasta las 24:00h del día en que finaliza el plazo y no se admitirán pagos posteriores a las 14:00h del día siguiente a dicha finalización.</strong></div>
</li>
</ul>
</li>
</ul>
<ul class="enlaces">
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectividadgranada" title="pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectividadgranada">Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Granada y provincia</a></div>
</li>
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectivdadceutamelillamarruecos" title="pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectivdadceutamelillamarruecos">Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Ceuta, Melilla y Marruecos</a></div>
</li>
</ul>
<p><strong>NOTA CONVOCATORIA SEPTIEMBRE: Solo el ingreso</strong> mediante carta de pago se podrá realizar hasta el día 8 de septiembre debido a que los días 6 y 7 de septiembre son sábado y domingo respectivamente; aunque la carta de pago se debe haber obtenido en el plazo del 3 al 5 de septiembre.</p>
</div>
<h1>PRECIOS DE INSCRIPCIÓN</h1>
<div class="level1">
<ul>
<li class="level1">
<div class="li">Los derechos de examen se podrán ingresar mediante <strong>Carta de Pago</strong> en cualquier sucursal de <em>Caja Granada o Caja Rural de Granada</em> o mediante <strong>pago con tarjeta</strong>. Ambas opciones estarán reflejadas en la solicitud de inscripción que deberá realizar en esta misma WEB.</div>
</li>
</ul>
<ul>
<li class="level1">
<div class="li">La cantidad a ingresar será:</div>
</li>
</ul>
<ul class="departamento">
<li class="level1 par">
<div class="li"><strong>Precios públicos ordinarios:</strong></div>
<ul>
<li class="level4">
<div class="li"><strong>Fase General</strong>: 58.70 € .</div>
</li>
<li class="level4">
<div class="li"><strong>Fase Específica</strong>: 14.70 € x número de materias de las que se examina.</div>
</li>
</ul>
</li>
</ul>
<ul class="departamento">
<li class="level1 impar">
<div class="li"><strong>Precios públicos para beneficiarios Familia Numerosa:</strong></div>
<ul>
<li class="level4">
<div class="li">De categoría <strong>general</strong>: ......................... Reducción del 50%.</div>
</li>
<li class="level4">
<div class="li">De categoría <strong>especial</strong>: ........................ Exentos de pago.</div>
</li>
</ul>
</li>
</ul>
<ul class="departamento">
<li class="level1 par">
<div class="li"><strong>Precios públicos para personas con discapacidad</strong>:</div>
<ul>
<li class="level4">
<div class="li">Exentos de pago.</div>
</li>
<li class="level4">
<div class="li">tendrán la consideración de personas con discapacidad aquellas a quienes se les haya reconocido un grado de minusvalía igual o superior al 33 % por la Consejería para la Igualdad y Bienestar Social de la Junta de Andalucía u órgano competente en la comunidad de procedencia del interesado.</div>
</li>
</ul>
</li>
<li class="level1 level1_ultimo impar">
<div class="li"><strong>IMPORTANTE:</strong></div>
<ul>
<li class="level4">
<div class="li">Los alumnos que tengan exención total de precios públicos bien por <em class="u"><strong>FAMILIA NUMEROSA DE CATEGORÍA ESPECIAL</strong></em>, o <em class="u"><strong>DISCAPACIDAD</strong></em> deberán acceder a la misma página web y seguir el mismo proceso de matriculación que finalizará con la impresión del RESGUARDO, quedando ya matriculados. El reguardo deberán llevarlo el día del examen.</div>
</li>
<li class="level4">
<div class="li">Los documentos justificativos de familia numerosa como de discapacidad deben estar actualizados en el momento de realizar la inscripción.</div>
</li>
</ul>
</li>
</ul>
</div>
<h1>HOJA INFORMATIVA</h1>
<div class="level1">
<p><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02" title="pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02">Hoja informativa inscripción/horarios Selectividad</a></p>
</div>
<h1>ADAPTACIÓN DE LOS EXÁMENES PARA ESTUDIANTES CON DISCAPACIDAD</h1>
<div class="level1">
<p>Todos los alumnos que padezcan alguna discapacidad y que necesiten condiciones especiales para la realización de las pruebas deberán:</p>
<ul>
<li class="level1">
<div class="li">Comunicarlo a este Servicio y justificarlo mediante informe del Centro en el que consten las adaptaciones que necesite, y certificado expedido por la Consejería para la igualdad y bienestar social de la Junta de Andalucía o la administración correspondiente en otra Comunidad, en el que conste el grado de minusvalía.</div>
</li>
</ul>
<p>Los documentos acreditativos deberán estar actualizados.</p>
<p> </p>
</div>
</div>
</div>
</div>
<div style="font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13px; text-align: justify;"> </div>
<div id="pagina">
<div id="contenido" class="sec_interior">
<div class="content_doku">
<div class="level1">
<p> </p>
<table class="tabla_redonda" style="border: 1px solid #ced5d7; border-top-left-radius: 6px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 6px; padding: 10px 15px; box-shadow: #b5c1c5 0px 5px 10px, #eef5f7 0px 0px 0px 10px inset; color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left; background-color: #ffffff;" width="98%" border="0"><caption class="titulo" style="font-weight: bold; color: #ff9900; font-size: 16px; border: 1px solid #aaaaaa; vertical-align: middle; height: 45px; margin: 10px auto; -webkit-box-shadow: black 0px 0px 4px; box-shadow: black 0px 0px 4px; text-shadow: black 1px 1px 1px; padding-top: 8px; width: 90%; background: #e8edff;">EXÁMENES Y ORIENTACIONES SOBRE SELECTIVIDAD</caption></table>
<p style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; line-height: 16.6399993896484px;" align="center">Si desea conocer el tamaño del archivo a descargar, deje el ratón sobre el icono del archivo correspondiente. </p>
<table style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td><strong>Asignatura</strong></td>
<td align="center"><strong>Orientaciones <br />2014/2015</strong></td>
<td align="center"><strong>2005</strong></td>
<td align="center"><strong>2006</strong></td>
<td align="center"><strong>2007</strong></td>
<td align="center"><strong>2008</strong></td>
<td align="center"><strong>2009</strong></td>
<td align="center"><strong>2010</strong></td>
<td align="center"><strong>2011</strong></td>
<td align="center"><strong>2012</strong></td>
<td align="center"><strong>2013</strong></td>
</tr>
<tr>
<td>Análisis Musical II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_analisis_musical_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="1.23 Mb" title="1.23 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="5.31 Mb" title="5.31 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="170.95 Mb" title="170.95 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="265.22 Mb" title="265.22 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Biología <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_biologia_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="1.58 Mb" title="1.58 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.09 Mb" title="2.09 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.29 Mb" title="1.29 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.26 Mb" title="1.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.07 Mb" title="2.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.54 Mb" title="2.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Ciencias de la Tierra y Medioambientales <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_ciencias_de_la_tierra_y_medioambientales_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.64 Mb" title="0.64 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.44 Mb" title="1.44 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="14.79 Mb" title="14.79 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.47 Mb" title="1.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.88 Mb" title="1.88 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.45 Mb" title="1.45 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Comentario de Texto,Lengua Castellana y Literatura <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_comentario_texto_lengua_castellana_y_literatura_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.36 Mb" title="0.36 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.4 Mb" title="0.4 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Dibujo Artístico II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_artistico_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="13.76 Mb" title="13.76 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.22 Mb" title="2.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.34 Mb" title="1.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="5.2 Mb" title="5.2 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="27.54 Mb" title="27.54 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="13.52 Mb" title="13.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="59.45 Mb" title="59.45 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.72 Mb" title="3.72 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Dibujo Técnico II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_tecnico_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.56 Mb" title="2.56 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.86 Mb" title="6.86 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="8.05 Mb" title="8.05 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="15.45 Mb" title="15.45 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.99 Mb" title="12.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.97 Mb" title="6.97 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Diseño <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_diseno_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.38 Mb" title="12.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.66 Mb" title="0.66 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.12 Mb" title="1.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.76 Mb" title="3.76 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Economía de la Empresa <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_economia_de_la_empresa_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.1 Mb" title="0.1 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.31 Mb" title="0.31 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.96 Mb" title="3.96 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.38 Mb" title="1.38 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Electrotecnia <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_electrotecnia_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.46 Mb" title="0.46 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Física <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_fisica_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.26 Mb" title="0.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.28 Mb" title="0.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.25 Mb" title="0.25 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.39 Mb" title="1.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.36 Mb" title="0.36 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Geografía <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_geografia_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.29 Mb" title="0.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.09 Mb" title="3.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.32 Mb" title="1.32 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.41 Mb" title="12.41 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.11 Mb" title="2.11 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.86 Mb" title="3.86 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.98 Mb" title="1.98 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Griego II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_griego_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.54 Mb" title="0.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.6 Mb" title="0.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.35 Mb" title="2.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="4.69 Mb" title="4.69 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.57 Mb" title="0.57 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.51 Mb" title="0.51 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Historia de España <em>(Común)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_espanna_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.74 Mb" title="0.74 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.73 Mb" title="1.73 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.77 Mb" title="1.77 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia de la Filosofía <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_filosofia_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.05 Mb" title="0.05 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.68 Mb" title="0.68 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Historia de la Música y de la Danza <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_musica_y_de_la_danza_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="3.3 Mb" title="3.3 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="27.28 Mb" title="27.28 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="22.08 Mb" title="22.08 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.51 Mb" title="1.51 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="13.4 Mb" title="13.4 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.38 Mb" title="2.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="318.43 Mb" title="318.43 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="10.61 Mb" title="10.61 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="93.24 Mb" title="93.24 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="101.86 Mb" title="101.86 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia del Arte <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_del_arte_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.49 Mb" title="0.49 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.67 Mb" title="0.67 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.16 Mb" title="3.16 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="8.71 Mb" title="8.71 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.91 Mb" title="6.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.5 Mb" title="3.5 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="18.28 Mb" title="18.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.06 Mb" title="1.06 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Latín II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_latin_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.94 Mb" title="0.94 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.94 Mb" title="1.94 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1 Mb" title="1 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.55 Mb" title="0.55 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Alemán) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(aleman)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.26 Mb" title="0.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.35 Mb" title="1.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.39 Mb" title="0.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.3 Mb" title="1.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Francés) <em>(Común)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(frances)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.1 Mb" title="0.1 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Inglés) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(ingles)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.75 Mb" title="0.75 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.68 Mb" title="1.68 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.06 Mb" title="2.06 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.18 Mb" title="1.18 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.19 Mb" title="12.19 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.74 Mb" title="1.74 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Italiano) <em>(Común)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(italiano)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.29 Mb" title="0.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.75 Mb" title="0.75 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Portugués) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(portugues)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.32 Mb" title="0.32 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.78 Mb" title="0.78 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.77 Mb" title="0.77 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lenguaje y Práctica Musical <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lenguaje_y_practica_musical_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="1.4 Mb" title="1.4 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="92.65 Mb" title="92.65 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.31 Mb" title="2.31 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="80.36 Mb" title="80.36 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="64.72 Mb" title="64.72 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Literatura Universal <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_literatura_universal_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.76 Mb" title="0.76 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Matemáticas Aplicadas a las Ciencias Sociales II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.62 Mb" title="0.62 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.51 Mb" title="6.51 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.42 Mb" title="1.42 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.53 Mb" title="1.53 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Matemáticas II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="4.39 Mb" title="4.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.54 Mb" title="1.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.13 Mb" title="1.13 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Química <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_quimica_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.34 Mb" title="0.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.34 Mb" title="0.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.19 Mb" title="1.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.5 Mb" title="0.5 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.66 Mb" title="1.66 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Técnicas de Expresión Gráfico Plásticas <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnicas_de_expresion_grafico-plastica_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.85 Mb" title="0.85 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="10.44 Mb" title="10.44 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.86 Mb" title="0.86 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="4.72 Mb" title="4.72 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="8.63 Mb" title="8.63 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.41 Mb" title="1.41 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Tecnología Industrial II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnologia_industrial_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.14 Mb" title="1.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.12 Mb" title="1.12 Mb" align="center" /></a></td>
</tr>
</tbody>
</table>
<p> </p>
<p style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; line-height: 16.6399993896484px;"> </p>
<p><span style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; background-color: #ffffff;">Si desea conocer el tamaño del archivo a descargar, deje el ratón sobre el icono del archivo correspondiente.</span></p>
</div>
</div>
</div>
</div>";s:4:"body";s:0:"";s:5:"catid";s:2:"59";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-09-10 19:50:30";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"0";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"0";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"12";s:8:"ordering";s:1:"0";s:8:"category";s:12:"Selectividad";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:18:"259:selectividad-3";s:7:"catslug";s:15:"59:selectividad";s:6:"author";N;s:6:"layout";s:7:"article";s:4:"path";s:96:"index.php?option=com_content&view=article&id=259:selectividad-3&catid=59:selectividad&Itemid=243";s:10:"metaauthor";N;s:6:"weight";d:3.5931700000000002;s:7:"link_id";i:1993;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"Selectividad";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"Selectividad";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=259";s:5:"route";s:96:"index.php?option=com_content&view=article&id=259:selectividad-3&catid=59:selectividad&Itemid=243";s:5:"title";s:12:"Selectividad";s:11:"description";s:14731:"CALENDARIO DE LAS PRUEBAS DE ACCESO A LA UNIVERSIDAD CURSO 2014/15 LUGAR: FACULTAD DE MEDICINA, AVDA. DE MADRID CURSO 2014/2015 Convocatoria ordinaria: 16, 17, 18 de junio de 2015 Convocatoria extraordinaria: 15, 16 y 17 de septiembre de 2015 HORARIO PRIMER DÍA SE RECOMIENDA LLEGAR DE 7 A 7.30 EL PRIMER DÍA SEGUNDO DÍA TERCER DÍAwidth="5%"> HORARIO TERCER DÍA (TARDE) Exclusivo para incompatabilidades 08:00-08:30 CITACIÓN CITACIÓN CITACIÓN 16:00-16:30 CITACIÓN 08:30-10:00 COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II HISTORIA DEL ARTE MATEMÁTICAS II ANÁLISIS MUSICAL II DISEÑO GEOGRAFÍA BIOLOGÍA 16:30-18:00 10:00-10:45 DESCANSO DESCANSO DESCANSO 18:00-18:30 DESCANSO 10:45-12:15 HISTORIA DE ESPAÑA HISTORIA DE LA FILOSOFÍA TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS QUÍMICA ELECTROTÉCNIA LITERATURA UNIVERSAL DIBUJO TÉCNICO II CIENCIAS DE LA TIERRA YMEDIOAMBIENTALES ECONOMÍA DE LA EMPRESA GRIEGO II 18:30-20:00 12:15-13:00 DESCANSO DESCANSO DESCANSO 20:00-20:30 DESCANSO 13:00-14:30 IDIOMA EXTRANJERO LENGUAJE Y PRÁCTICA MUSICAL TECNOLOGÍA INDUSTRIAL II MATEMÁT. APLIC. A LAS CC. SOCIALES II HISTORIA DE LA MÚSICA Y LA DANZA DIBUJO ARTÍSTICO II FÍSICA LATÍN II 20:30-22:00 NOTAS IMPORTANTES: Las franjas horarias de citación son en defecto de que la universidad no fije otras que en razón de las sedes de que se traten, considere más oportunas; asi mismo sí la última franja horaria del tercer día para incompatibilidades, quedase libre, el descanso entre la primera y segunda franja horaria será de 45 minutos. El horario de las materias del tercer día por la tarde es exclusivamente para quienes tienen más de unexamen el mismo día y a la misma hora (en el 2º o 3º día por la mañana), debiendo realizar en dicho horario y en primer lugar el examen que aparece antes en el respectivo cuadro y realizará el examen de la otra materia en el tercer día por la tarde en el horario que se indicará oportunamente. SELECTIVIDAD SEPTIEMBRE 2014 V E R E X Á M E N E S: D Í A S 16 - 17 - 18 S E P T I E M B R E Las notas están disponibles el día 24 Normas de las PAU SEDE 5 (GRANADA) FACULTAD DE MEDICINA AVDA. DE MADRID S/N Ver más informaciónmargin-bottom: 20px; padding: 5px; background-color: #ffffff; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; text-align: center;"> SELECTIVIDAD JUNIO 2014: ENHORABUENA A NUESTRO ALUMNADO, HA SUPERADO LAS SELECTIVIDAD UN 98,57 % (69 DE 70 ALUMNOS PRESENTADOS DE BACHILLERATO Y LOS 3 DEL CICLO) ESTUDIO ESTADÍSTICO DE LAS NOTAS OBTENIDAS POR EL ALUMNADO DEL IES MIGUEL DE CERVANTES0pt none;" /> __________________________________________________________________________________ (Análisis realizado por Miguel Anguita) INSCRIPCIÓN PRUEBAS DE ACCESO A LA UNIVERSIDADsolid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: center;"> Descargar versión en PDF Requisitos Plazos y solicitud Precios de inscripción Hoja informativa Adaptación de los exámenes para estudiantes con discapacidad REQUISITOS Estar en posesión del Título de Bachillerato para la realización de la prueba completa. Estar en posesióndel Título de técnico superior de formación profesional, técnico superior de artes plásticas y diseño, o técnico deportivo superior o título equivalente , únicamente a efectos de la fase específica voluntaria para mejorar la nota de admisión a las enseñanzas oficiales de grado. PLAZOS Y SOLICITUD Todos los/as estudiantes que estén interesados en participar en la Prueba de Acceso a la Universidad tanto en su fase general como específica, ya sean alumnos/as que se presentan por primera vez o para mejorar nota, en cualquiera de sus convocatorias Ordinaria o Extraordinaria, deberán realizar los siguientes pasos en los plazos establecidos: PRIMER PASO: REGISTRARSE Hay que cumplimentar el trámite de REGISTRO a través del portal WEB de Selectividad: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp PLAZO DE REGISTRO SELECTIVIDAD CONVOCATORIA 2014: Convocatoria Ordinaria Convocatoria Extraordinaria del 22 de abril al 4 de junio 2014 del 1 de agosto al 5 de septiembre 2014 Aquí puedes encontrar una presentación que te ayudará a realizar el registro: Alumnos que este curso aprobarán el 2º deBachillerato o el 2º curso del CFGS en un centro de Granada, Ceuta, Melilla o centros adscritos a la Universidad de Granada en Marruecos Alumnos que se han presentado alguna vez a Selectividad en la Universidad de Granada Alumnos procedentes de otras universidades y que nunca se han presentado a Selectividad en la Universidad de Granada SEGUNDO PASO: MATRICULARSE Una vez hecho el registro hay que realizar la inscripción ( MATRÍCULA ) en la prueba a través del portal WEB de Selectividad: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp PLAZO DE MATRÍCULA SELECTIVIDAD CONVOCATORIA 2014: Convocatoria Ordinaria Convocatoria Extraordinaria 2 al 4 de junio 2014 3 al 5 de septiembre 2014 MUY IMPORTANTE: La matrícula se podrá realizar hasta las 24:00h del día en que finaliza el plazo y nose admitirán pagos posteriores a las 14:00h del día siguiente a dicha finalización. Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Granada y provincia Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Ceuta, Melilla y Marruecos NOTA CONVOCATORIA SEPTIEMBRE: Solo el ingreso mediante carta de pago se podrá realizar hasta el día 8 de septiembre debido a que los días 6 y 7 de septiembre son sábado y domingo respectivamente; aunque la carta de pago se debe haber obtenido en el plazo del 3 al 5 de septiembre. PRECIOS DE INSCRIPCIÓN Los derechos de examen se podrán ingresar mediante Carta de Pago en cualquier sucursal de Caja Granada o Caja Rural de Granada o mediante pago con tarjeta . Ambas opciones estarán reflejadas en la solicitud de inscripción que deberá realizar en esta misma WEB. La cantidad a ingresar será: Precios públicos ordinarios: Fase General : 58.70 €. Fase Específica : 14.70 € x número de materias de las que se examina. Precios públicos para beneficiarios Familia Numerosa: De categoría general : ......................... Reducción del 50%. De categoría especial : ........................ Exentos de pago. Precios públicos para personas con discapacidad : Exentos de pago. tendrán la consideración de personas con discapacidad aquellas a quienes se les haya reconocido un grado de minusvalía igual o superior al 33 % por la Consejería para la Igualdad y Bienestar Social de la Junta de Andalucía u órgano competente en la comunidad de procedencia del interesado. IMPORTANTE: Los alumnos que tengan exención total de precios públicos bien por FAMILIA NUMEROSA DE CATEGORÍA ESPECIAL , o DISCAPACIDAD deberán acceder a la misma página web y seguir el mismo proceso de matriculación que finalizará con la impresión del RESGUARDO, quedando ya matriculados. El reguardo deberán llevarlo el día del examen. Los documentos justificativos de familia numerosa como de discapacidad deben estar actualizados en el momento de realizar la inscripción. HOJA INFORMATIVAclass="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02" title="pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02"> Hoja informativa inscripción/horarios Selectividad ADAPTACIÓN DE LOS EXÁMENES PARA ESTUDIANTES CON DISCAPACIDAD Todos los alumnos que padezcan alguna discapacidad y que necesiten condiciones especiales para la realización de las pruebas deberán: Comunicarlo a este Servicio y justificarlo mediante informe del Centro en el que consten las adaptaciones que necesite, y certificado expedido por la Consejería para la igualdad y bienestar social de la Junta de Andalucía o la administración correspondiente en otra Comunidad, en el que conste el grado de minusvalía. Los documentos acreditativos deberán estar actualizados. EXÁMENES YORIENTACIONES SOBRE SELECTIVIDAD Si desea conocer el tamaño del archivo a descargar, deje el ratón sobre el icono del archivo correspondiente. Asignatura Orientaciones 2014/2015 2005 2006 2007 2008 2009 2010 2011 2012 2013 Análisis Musical II (Modalidad)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_analisis_musical.zip"> Biología (Modalidad)src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.29 Mb" title="1.29 Mb" align="center" />href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_biologia.zip"> Ciencias de la Tierra y Medioambientales (Modalidad)src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="center" />src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.45 Mb" title="1.45 Mb" align="center" /> Comentario de Texto,Lengua Castellana y Literatura (Común)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_castellana.zip"> Dibujo Artístico II (Modalidad)alt="1.34 Mb" title="1.34 Mb" align="center" /> Dibujo Técnico II (Modalidad)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_tecnico_ii_2014_2015.pdf">border="0" alt="1.25 Mb" title="1.25 Mb" align="center" /> Diseño (Modalidad)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fundamentos_diseno.zip">href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fundamento_diseno.zip"> Economía de la Empresa (Modalidad)src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="center" />href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_economia.zip"> Electrotecnia (Modalidad)Mb" align="center" />/> Física (Modalidad)src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.39 Mb" title="1.39 Mb" align="center" /> Geografía (Modalidad)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_geografia.zip">href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_geografia.zip"> Griego II (Modalidad)border="0" alt="0.08 Mb" title="0.08 Mb" align="center" />src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.57 Mb" title="0.57 Mb" align="center" /> Historia de España (Común)title="1.77 Mb" align="center" /> Historia de la Filosofía (Común)src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="center" /> Historia de la Música y de la Danza (Modalidad)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_musica.zip">bgcolor="#FEFDD6"> Historia del Arte (Modalidad)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_arte.zip"> Latín II (Modalidad)src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" />border="0" alt="0.19 Mb" title="0.19 Mb" align="center" /> Lengua Extranjera II (Alemán) (Común)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_aleman.zip">bgcolor="#FEFDD6"> Lengua Extranjera II (Francés) (Común)alt="0.14 Mb" title="0.14 Mb" align="center" />bgcolor="#FEFDD6"> Lengua Extranjera II (Inglés) (Común)src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.68 Mb" title="1.68 Mb" align="center" /> Lengua Extranjera II (Italiano) (Común)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_italiano.zip">href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_italiano.zip"> Lengua Extranjera II (Portugués) (Común)align="center" bgcolor="#FEFDD6"> Lenguaje y Práctica Musical (Modalidad)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_practica_musical.zip"> Literatura Universal (Modalidad)Mb" align="center" /> Matemáticas Aplicadas a las Ciencias Sociales II (Modalidad)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicas_aplicadas.zip">align="center"> Matemáticas II (Modalidad)src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="center" />href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicasII.zip"> Química (Modalidad)align="center"> Técnicas de Expresión Gráfico Plásticas (Modalidad)src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" />href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnicas_expresion_grafica.zip"> Tecnología Industrial II (Modalidad)href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnologia_industrial.zip">align="center"> Si desea conocer el tamaño del archivo a descargar, deje el ratón sobre el icono del archivo correspondiente.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2011-06-11 09:17:57";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2011-06-08 09:17:57";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:1;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:27:{s:2:"id";s:3:"192";s:5:"alias";s:11:"universidad";s:7:"summary";s:1438:"<ul>
<li><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/" target="_blank">Distrito Único Andaluz.</a></li>
<li><a href="http://coga.ugr.es/pages/ponencias" target="_blank">Coordinación Prueba de Acceso.</a></li>
<li>Universidades Andaluzas.</li>
<ul>
<li class="first"><a href="http://www.ual.es/" target="_blank"> <span> Almería </span> </a></li>
<li><a href="http://www.uca.es/es/" target="_blank"> <span> Cádiz </span> </a></li>
<li><a href="http://www.uco.es/" target="_blank"> <span> Córdoba </span> </a></li>
<li><a href="http://www.ugr.es/" target="_blank"> <span> Granada </span> </a></li>
<li><a href="http://www.uhu.es/index.php" target="_blank"> <span> Huelva </span> </a></li>
<li><a href="http://www.unia.es/" target="_blank"> <span> Internacional </span> </a></li>
<li><a href="http://www.ujaen.es/" target="_blank"> <span> Jaén </span> </a></li>
<li><a href="http://www.uma.es/" target="_blank"> <span> Málaga </span> </a></li>
<li><a href="http://www.upo.es/portal/impe/web/portada" target="_blank"> <span> Pablo de Olavide </span> </a></li>
<li class="last"><span><a href="http://www.us.es/" target="_blank"> Sevilla</a></span></li>
</ul>
<li><a href="http://iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=243">P.A.U. 2012/13.</a></li>
<ul>
<li class="last"><a href="http://www.us.es/" target="_blank"> </a></li>
</ul>
</ul>";s:4:"body";s:0:"";s:5:"catid";s:3:"194";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2013-02-19 07:22:19";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":70:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";i:0;s:10:"show_intro";s:1:"1";s:13:"show_category";i:0;s:13:"link_category";i:0;s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";i:0;s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";s:12:"show_section";i:0;s:12:"link_section";i:0;}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:1:"4";s:8:"ordering";s:1:"5";s:8:"category";s:32:"IES Miguel de Cervantes- General";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:15:"192:universidad";s:7:"catslug";s:35:"194:ies-miguel-de-cervantes-general";s:6:"author";s:10:"Super User";s:4:"mime";N;s:6:"layout";s:7:"article";s:4:"path";s:113:"index.php?option=com_content&view=article&id=192:universidad&catid=194:ies-miguel-de-cervantes-general&Itemid=587";s:10:"metaauthor";N;s:6:"weight";d:2.7132100000000001;s:7:"link_id";i:693;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:32:"IES Miguel de Cervantes- General";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:32:"IES Miguel de Cervantes- General";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=192";s:5:"route";s:113:"index.php?option=com_content&view=article&id=192:universidad&catid=194:ies-miguel-de-cervantes-general&Itemid=587";s:5:"title";s:11:"UNIVERSIDAD";s:11:"description";s:190:"Distrito Único Andaluz. Coordinación Prueba de Acceso. Universidades Andaluzas. Almería Cádiz Córdoba Granada Huelva Internacional Jaén Málaga Pablo de Olavide Sevilla P.A.U. 2012/13.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2013-02-06 20:04:16";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2013-02-06 20:04:16";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:2;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"139";s:5:"alias";s:12:"selectividad";s:7:"summary";s:11516:"<p>&nbsp;</p>
<div id="pagina">
<h1 id="titulo_pagina"><span class="texto_titulo">Inscripción Pruebas de Acceso a la Universidad</span></h1>
<a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion?theme=pdf" id="enlace_pdf" title="Versión en PDF">Descargar versión en PDF</a>
<div id="contenido" class="sec_interior">
<div class="toc">
<div id="toc__inside">
<ul class="toc">
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_requisitos" class="toc">Requisitos</a></span></div>
</li>
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_plazos_y_solicitud" class="toc">Plazos y solicitud</a></span></div>
</li>
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_precios_de_inscripcion" class="toc">Precios de inscripción</a></span></div>
</li>
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_hoja_informativa" class="toc">Hoja informativa</a></span></div>
</li>
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_adaptacion_de_los_examenes_para_estudiantes_con_discapacidad" class="toc">Adaptación de los exámenes para estudiantes con discapacidad</a></span></div>
</li>
</ul>
</div>
</div>
<div class="content_doku">
<h1><a id="__doku_requisitos"></a>Requisitos</h1>
<div class="level1">
<ul>
<li class="level1">
<div class="li">Estar en posesión del <strong>Título de Bachillerato</strong> para la realización de la prueba completa.</div>
</li>
<li class="level1">
<div class="li">Estar en posesión del <strong>Título de técnico superior de formación profesional, técnico superior de artes plásticas y diseño, o técnico deportivo superior o título equivalente</strong>, únicamente a efectos de la <em class="u">fase específica voluntaria</em> para mejorar la nota de admisión a las enseñanzas oficiales de grado.</div>
</li>
</ul>
</div>
<h1><a id="__doku_plazos_y_solicitud"></a>Plazos y solicitud</h1>
<div class="level1">
<p>Todos los/as estudiantes que estén interesados en participar en la Prueba de Acceso a la Universidad tanto en su fase general como específica, ya sean alumnos/as que se presentan por primera vez o para mejorar nota, en cualquiera de sus convocatorias Ordinaria o Extraordinaria, deberán realizar los siguientes pasos en los plazos establecidos:</p>
</div>
<h2><a id="__doku_primer_pasoregistrarse"></a>PRIMER PASO: REGISTRARSE</h2>
<div class="level2">
<p>Hay que cumplimentar el trámite de <strong>REGISTRO</strong> a través del portal WEB de Selectividad:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" class="urlextern" title="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></div>
</li>
</ul>
<p><strong>PLAZO DE REGISTRO</strong> SELECTIVIDAD CONVOCATORIA 2014:</p>
<table class="inline">
<tbody>
<tr><th>Convocatoria Ordinaria</th><th>Convocatoria Extraordinaria</th></tr>
<tr>
<td class="leftalign">del 22 de abril al 4 de junio 2014</td>
<td class="leftalign">del 1 de agosto al 5 de septiembre 2014</td>
</tr>
</tbody>
</table>
<p>Aquí puedes encontrar una presentación que te ayudará a realizar el registro:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad1" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad1">Alumnos que este curso aprobarán el 2º de Bachillerato o el 2º curso del CFGS en un centro de Granada, Ceuta, Melilla o centros adscritos a la Universidad de Granada en Marruecos </a></div>
</li>
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad2" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad2">Alumnos que se han presentado alguna vez a Selectividad en la Universidad de Granada</a></div>
</li>
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad3" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad3">Alumnos procedentes de otras universidades y que nunca se han presentado a Selectividad en la Universidad de Granada</a></div>
</li>
</ul>
</div>
<h2><a id="__doku_segundo_pasomatricularse"></a>SEGUNDO PASO: MATRICULARSE</h2>
<div class="level2">
<p>Una vez hecho el registro hay que realizar la inscripción (<strong>MATRÍCULA</strong>) en la prueba a través del portal WEB de Selectividad:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" class="urlextern" title="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></div>
</li>
</ul>
<p><strong>PLAZO DE MATRÍCULA</strong> SELECTIVIDAD CONVOCATORIA 2014:</p>
<table class="inline">
<tbody>
<tr><th>Convocatoria Ordinaria</th><th>Convocatoria Extraordinaria</th></tr>
<tr>
<td class="leftalign">2 al 4 de junio 2014</td>
<td class="leftalign">3 al 5 de septiembre 2014</td>
</tr>
</tbody>
</table>
<ul class="departamento">
<li class="level1 level1_primero impar">
<div class="li"><strong>MUY IMPORTANTE:</strong></div>
<ul>
<li class="level4">
<div class="li"><strong>La matrícula se podrá realizar hasta las 24:00h del día en que finaliza el plazo y no se admitirán pagos posteriores a las 14:00h del día siguiente a dicha finalización.</strong></div>
</li>
</ul>
</li>
</ul>
<ul class="enlaces">
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectividadgranada" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectividadgranada"> Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Granada y provincia </a></div>
</li>
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectivdadceutamelillamarruecos" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectivdadceutamelillamarruecos"> Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Ceuta, Melilla y Marruecos </a></div>
</li>
</ul>
<p><strong>NOTA CONVOCATORIA SEPTIEMBRE: Solo el ingreso</strong> mediante carta de pago se podrá realizar hasta el día 8 de septiembre debido a que los días 6 y 7 de septiembre son sábado y domingo respectivamente; aunque la carta de pago se debe haber obtenido en el plazo del 3 al 5 de septiembre.</p>
</div>
<h1><a id="__doku_precios_de_inscripcion"></a>Precios de inscripción</h1>
<div class="level1">
<ul>
<li class="level1">
<div class="li">Los derechos de examen se podrán ingresar mediante <strong>Carta de Pago</strong> en cualquier sucursal de <em>Caja Granada o Caja Rural de Granada</em> o mediante <strong>pago con tarjeta</strong>. Ambas opciones estarán reflejadas en la solicitud de inscripción que deberá realizar en esta misma WEB.</div>
</li>
</ul>
<ul>
<li class="level1">
<div class="li">La cantidad a ingresar será:</div>
</li>
</ul>
<ul class="departamento">
<li class="level1 par">
<div class="li"><strong>Precios públicos ordinarios:</strong></div>
<ul>
<li class="level4">
<div class="li"><strong>Fase General</strong>: 58.70 € .</div>
</li>
<li class="level4">
<div class="li"><strong>Fase Específica</strong>: 14.70 € x número de materias de las que se examina.</div>
</li>
</ul>
</li>
</ul>
<ul class="departamento">
<li class="level1 impar">
<div class="li"><strong>Precios públicos para beneficiarios Familia Numerosa:</strong></div>
<ul>
<li class="level4">
<div class="li">De categoría <strong>general</strong>: ......................... Reducción del 50%.</div>
</li>
<li class="level4">
<div class="li">De categoría <strong>especial</strong>: ........................ Exentos de pago.</div>
</li>
</ul>
</li>
</ul>
<ul class="departamento">
<li class="level1 par">
<div class="li"><strong>Precios públicos para personas con discapacidad</strong>:</div>
<ul>
<li class="level4">
<div class="li">Exentos de pago.</div>
</li>
<li class="level4">
<div class="li">tendrán la consideración de personas con discapacidad aquellas a quienes se les haya reconocido un grado de minusvalía igual o superior al 33 % por la Consejería para la Igualdad y Bienestar Social de la Junta de Andalucía u órgano competente en la comunidad de procedencia del interesado.</div>
</li>
</ul>
</li>
<li class="level1 level1_ultimo impar">
<div class="li"><strong>IMPORTANTE:</strong></div>
<ul>
<li class="level4">
<div class="li">Los alumnos que tengan exención total de precios públicos bien por <em class="u"><strong>FAMILIA NUMEROSA DE CATEGORÍA ESPECIAL</strong></em>, o <em class="u"><strong>DISCAPACIDAD</strong></em> deberán acceder a la misma página web y seguir el mismo proceso de matriculación que finalizará con la impresión del RESGUARDO, quedando ya matriculados. El reguardo deberán llevarlo el día del examen.</div>
</li>
<li class="level4">
<div class="li">Los documentos justificativos de familia numerosa como de discapacidad deben estar actualizados en el momento de realizar la inscripción.</div>
</li>
</ul>
</li>
</ul>
</div>
<h1><a id="__doku_hoja_informativa"></a>Hoja informativa</h1>
<div class="level1">
<p><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02"> Hoja informativa inscripción/horarios Selectividad </a></p>
</div>
<h1><a id="__doku_adaptacion_de_los_examenes_para_estudiantes_con_discapacidad"></a>Adaptación de los exámenes para estudiantes con discapacidad</h1>
<div class="level1">
<p>Todos los alumnos que padezcan alguna discapacidad y que necesiten condiciones especiales para la realización de las pruebas deberán:</p>
<ul>
<li class="level1">
<div class="li">Comunicarlo a este Servicio y justificarlo mediante informe del Centro en el que consten las adaptaciones que necesite, y certificado expedido por la Consejería para la igualdad y bienestar social de la Junta de Andalucía o la administración correspondiente en otra Comunidad, en el que conste el grado de minusvalía.</div>
</li>
</ul>
<p>Los documentos acreditativos deberán estar actualizados.</p>
</div>
</div>
</div>
</div>";s:4:"body";s:0:"";s:5:"catid";s:2:"59";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-09-09 18:28:42";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"0";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"0";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"44";s:8:"ordering";s:1:"2";s:8:"category";s:12:"Selectividad";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:16:"139:selectividad";s:7:"catslug";s:15:"59:selectividad";s:6:"author";N;s:6:"layout";s:7:"article";s:4:"path";s:94:"index.php?option=com_content&view=article&id=139:selectividad&catid=59:selectividad&Itemid=243";s:10:"metaauthor";N;s:6:"weight";d:2.5665499999999999;s:7:"link_id";i:1233;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"Selectividad";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"Selectividad";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=139";s:5:"route";s:94:"index.php?option=com_content&view=article&id=139:selectividad&catid=59:selectividad&Itemid=243";s:5:"title";s:16:"Selectividad_OLD";s:11:"description";s:5135:"Inscripción Pruebas de Acceso a la Universidad Descargar versión en PDF Requisitos Plazos y solicitud Precios de inscripción Hoja informativa Adaptación de los exámenes para estudiantes con discapacidad Requisitos Estar en posesión del Título de Bachillerato para la realización de la prueba completa. Estar en posesióndel Título de técnico superior de formación profesional, técnico superior de artes plásticas y diseño, o técnico deportivo superior o título equivalente , únicamente a efectos de la fase específica voluntaria para mejorar la nota de admisión a las enseñanzas oficiales de grado. Plazos y solicitud Todos los/as estudiantes que estén interesados en participar en la Prueba de Acceso a la Universidad tanto en su fase general como específica, ya sean alumnos/as que se presentan por primera vez o para mejorar nota, en cualquiera de sus convocatorias Ordinaria o Extraordinaria, deberán realizar los siguientes pasos en los plazos establecidos: PRIMER PASO: REGISTRARSE Hay que cumplimentar el trámite de REGISTRO a través del portal WEB de Selectividad: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp PLAZO DE REGISTRO SELECTIVIDAD CONVOCATORIA 2014: Convocatoria Ordinaria Convocatoria Extraordinaria del 22 de abril al 4 de junio 2014 del 1 de agosto al 5 de septiembre 2014 Aquí puedes encontrar una presentación que te ayudará a realizar el registro:title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad1"> Alumnos que este curso aprobarán el 2º de Bachillerato o el 2º curso del CFGS en un centro de Granada, Ceuta, Melilla o centros adscritos a la Universidad de Granada en Marruecos Alumnos que se han presentado alguna vez a Selectividad en la Universidad de Granada Alumnos procedentes de otras universidades y que nunca se han presentado a Selectividad en la Universidad de Granada SEGUNDO PASO: MATRICULARSE Una vez hecho el registro hay que realizar la inscripción ( MATRÍCULA ) en la prueba a través del portal WEB de Selectividad: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp PLAZO DE MATRÍCULA SELECTIVIDAD CONVOCATORIA 2014: Convocatoria Ordinaria Convocatoria Extraordinaria 2 al 4 de junio 2014 3 al 5 de septiembre 2014 MUYIMPORTANTE: La matrícula se podrá realizar hasta las 24:00h del día en que finaliza el plazo y no se admitirán pagos posteriores a las 14:00h del día siguiente a dicha finalización. Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Granada y provincia Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Ceuta, Melilla y Marruecos NOTA CONVOCATORIA SEPTIEMBRE: Solo el ingreso mediante carta de pago se podrá realizar hasta el día 8 de septiembre debido a que los días 6 y 7 de septiembre son sábado y domingo respectivamente; aunque la carta de pago se debe haber obtenido en el plazo del 3 al 5 de septiembre. Precios de inscripción Los derechos de examen se podrán ingresar mediante Carta de Pago en cualquier sucursal de Caja Granada o Caja Rural de Granada o mediante pago con tarjeta . Ambas opciones estarán reflejadas en la solicitud de inscripción que deberá realizar en esta misma WEB. La cantidad a ingresar será:class="departamento"> Precios públicos ordinarios: Fase General : 58.70 € . Fase Específica : 14.70 € x número de materias de las que se examina. Precios públicos para beneficiarios Familia Numerosa: De categoría general : ......................... Reducción del 50%. De categoría especial : ........................ Exentos de pago. Precios públicos para personas con discapacidad : Exentos de pago. tendrán la consideración de personas con discapacidad aquellas a quienes se les haya reconocido un grado de minusvalía igual o superior al 33 % por la Consejería para la Igualdad y Bienestar Social de la Junta de Andalucía u órgano competente en la comunidad de procedencia del interesado. IMPORTANTE: Los alumnos que tengan exención total de precios públicos bien por FAMILIA NUMEROSA DE CATEGORÍA ESPECIAL , o DISCAPACIDAD deberán acceder a la misma página web y seguir el mismo proceso de matriculación que finalizará con la impresión del RESGUARDO, quedando ya matriculados. El reguardo deberán llevarlo el día del examen. Los documentos justificativos de familia numerosacomo de discapacidad deben estar actualizados en el momento de realizar la inscripción. Hoja informativa Hoja informativa inscripción/horarios Selectividad Adaptación de los exámenes para estudiantes con discapacidad Todos los alumnos que padezcan alguna discapacidad y que necesiten condiciones especiales para la realización de las pruebas deberán: Comunicarlo a este Servicio y justificarlo mediante informe del Centro en el que consten las adaptaciones que necesite, y certificado expedido por la Consejería para la igualdad y bienestar social de la Junta de Andalucía o la administración correspondiente en otra Comunidad, en el que conste el grado de minusvalía. Los documentos acreditativos deberán estar actualizados.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2011-06-11 09:17:57";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2011-06-08 09:17:57";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:3;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"286";s:5:"alias";s:17:"selectividad-2015";s:7:"summary";s:35261:"<h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE 2015<br /><br /></span></h1>
<ul style="text-align: justify;">
<li><strong style="color: #2a2a2a;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/15  (MIÉRCOLES) POR LA TARDE EN <a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a;"> </span><strong style="color: #2a2a2a;">EXÁMENES QUE HAN SALIDO EN JUNIO 2015 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<p style="text-align: justify;"><span style="text-decoration: underline;"><strong>JUNIO 2015 - EXÁMENES</strong></span></p>
<p style="text-align: justify;"><strong>16/06/15. </strong>Martes. primer día: sin incidencias, todos a su hora con los nervios habituales.</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II</li>
<li>HISTORIA DE ESPAÑA</li>
<li>HISTORIA DE LA FILOSOFÍA</li>
<li>IDIOMA EXTRANJERO</li>
</ul>
<p style="text-align: justify;"><strong>17/06/15.</strong> Miércoles. Una incidencia. Una alumna no se presenta al examen, nervios, llamada al instituto, había decidido no presentarse ¡pero se matriculó! hubiera necesitado saberlo con antelación. Nada más destacable.</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>HISTORIA DEL ARTE</li>
<li>MATEMÁTICAS II <span style="line-height: 16.6399993896484px;">Y COLISIÓN</span></li>
<li>TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS</li>
<li>QUÍMICA</li>
<li>ELECTROTÉCNIA</li>
<li>LITERATURA UNIVERSAL</li>
<li>LENGUAJE Y PRÁCTICA MUSICAL</li>
<li>TECNOLOGÍA INDUSTRIAL II</li>
<li>MATEMÁT. APLIC. A LAS CC. SOCIALES II Y COLISIÓN</li>
</ul>
<p style="text-align: justify;"><strong>18/06/15. </strong>Jueves. Dos alumnos que no se presentan, uno que llega tarde por la tarde, cosas del bus que lo ha perdido, ¡pero es que no hay taxis!</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>ANÁLISIS MUSICAL II (¡YA!)</li>
<li>DISEÑO <span style="line-height: 16.6399993896484px;">(¡YA!)</span></li>
<li>GEOGRAFÍA</li>
<li>BIOLOGÍA</li>
<li>DIBUJO TÉCNICO II</li>
<li>CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES</li>
<li>ECONOMÍA DE LA EMPRESA</li>
<li>GRIEGO II</li>
<li>HISTORIA DE LA MÚSICA Y LA DANZA</li>
<li>DIBUJO ARTÍSTICO II</li>
<li>FÍSICA</li>
<li>LATÍN II</li>
</ul>
<p style="text-align: justify;">Los criterios de corrección ya <a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_criterios_correccion.php" target="_blank" title="Criterios de corrección oficiales.">han sido publicados oficialmente</a> y añadidos en cada asignatura abajo.</p>
<p style="text-align: justify;">Las soluciones: más adelante. Ya están las de Matemáticas II.</p>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Biología</strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br />Ciencias de la Tierra y Medioamb.</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Comentario de Texto, Lengua Castellana y Literatura</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Artístico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Técnico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Economía <br />de la Empresa</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Diseño</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Electrotecnia</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Biologia_2014_15.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></span></strong></a><strong style="font-size: inherit;">Colisión</strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Biologiia.pdf" target="_blank">Crit. Correc.</a><br /><a href="archivos_ies/14_15/selectividad/criterios/Biologiia_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/CTM_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Ciencias%20De%20La%20Tierra.pdf" target="_blank">Crit. Correc.<br /></a><br /><a href="archivos_ies/14_15/selectividad/criterios/Ciencias%20De%20La%20Tierra_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong><br /> <a href="archivos_ies/14_15/selectividad/Comentario_lengua_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Comentario%20Texto%20Lengua%20Castellana%20Y%20Literatura.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> <a href="archivos_ies/14_15/selectividad/D_Artistico_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Colisión</span></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Dibujo%20Artiistico.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/D_Tecnico_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/DIBUJO_PAU_junio_2015_c.pdf" target="_blank">Soluciones</a><br />(Prof.  Manuel Martínez Vela<br />IES P. MANJÓN)<br /><a href="archivos_ies/14_15/selectividad/criterios/Dibujo%20Teecnico.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Economia_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Economiia%20De%20La%20Empresa.pdf" target="_blank">Crit. Correc.<br /></a><br /><a href="archivos_ies/14_15/selectividad/criterios/Economiia%20De%20La%20Empresa_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Dise%C3%B1o_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Diseno.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Disenno.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td align="center" bgcolor="#abFDe8">
<p style="text-align: center;"><a href="archivos_ies/14_15/selectividad/Electrotecnia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Colisión</strong></p>
<p style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Electrotecnia.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Crit. Correc.<br /><br /></a><a href="archivos_ies/14_15/selectividad/criterios/Electrotecnia_incom.pdf" target="_blank" style="font-size: inherit; text-decoration: none; color: #2f310d;">Crit. Incomp.</a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Dise%C3%B1o.pdf" target="_blank"><br /></a>Física</strong></td>
<td style="text-align: center;" align="center"><strong><br />Geografía</strong></td>
<td style="text-align: center;" align="center"><strong><br />Griego II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de España</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de la Filosofía</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia <br />del Arte</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong>Latín II </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong>Alemán</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Fisica_2014_15d.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-decoration: none; color: inherit; font-size: inherit; font-weight: normal; font-family: inherit; text-align: left;">Soluciones<a href="archivos_ies/13_14/selectividad/FISICA_2014_Junio.pdf" target="_blank"><br /></a></strong><a href="archivos_ies/14_15/selectividad/criterios/Fiisica.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Fiisica_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Geografia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="37" height="37" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Geografiia.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Geografiia_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> <a href="archivos_ies/14_15/selectividad/Griego_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Griego.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Griego_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong> <a href="archivos_ies/14_15/selectividad/Historia_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20Espanna.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><a href="archivos_ies/14_15/selectividad/Comentario_filosofia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20La%20Filosofiia.pdf" target="_blank">Crit. Correc.<strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong> </strong><a href="archivos_ies/14_15/selectividad/Historia_Arte_2014_15.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20Del%20Arte.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> </strong><a href="archivos_ies/14_15/selectividad/Latin_junio_e_incompatibilidades_2014_15.pdf"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Latiin.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Latiin_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td align="center">
<p><strong style="text-align: center;"><span style="color: #2f310d;"><span style="margin-right: auto; margin-left: auto;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; margin-right: auto; margin-left: auto; display: block;" /></span></span></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"></a><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Alemaan.pdf" target="_blank">Crit. Correc.</a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Francés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Inglés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Literatura Universal</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas Aplicadas a las Ciencias Sociales II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Matemáticas II</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong><br /> Química </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong> </strong><strong>Técnicas de Exp. Gráf. Plást. (no disponible)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Tecnología Industrial II</strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Frances_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Francees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Ingles.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Inglees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Lit_Universal_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Literatura%20Universal.pdf" target="_blank">Crit. Correc.<br /><br /></a><a href="archivos_ies/14_15/selectividad/criterios/Literatura%20Universal_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><a href="archivos_ies/14_15/selectividad/Mat_CCSSII_2014_15.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Soluciones_MCCSSII_Junio.pdf" target="_blank">Soluciones<br /></a>(emestrda.net)</strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Criterios_cor_Matemaaticas%20Aplicadas.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a><br /></strong></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">_________</strong></p>
<strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/Mat_CCSSII_COLISIION_2014_15.pdf" target="_blank">Colisión<br /></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Criterios_cor_Matemaaticas%20Aplicadas_incom.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Incomp.</a></strong><br />_________<br /><br /></strong></td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/selectividad/Mat_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a>NUEVO: <br /></span></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/15_mod_soljun.pdf" target="_blank">Soluciones<br /><strong style="color: #000000; font-family: Arial; font-size: medium; letter-spacing: normal; text-align: -webkit-center;"><span style="font-size: 8;">(D. Germán Jesús Rubio Luna</span></strong></a>)<br /><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Matemaaticas.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a></strong></strong><br />_________<br /><a href="archivos_ies/14_15/selectividad/Mat_II_COLISION_2014_15b.pdf" target="_blank">Colisión<br /></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Matemaaticas_incom.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Incomp.</a></strong><br />_________</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Quimica_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/QUIMICA_JUNIO_2015.pdf" target="_blank">Soluciones<br /></a></strong>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">(emestrda.net)<br /><br /></strong><a href="archivos_ies/14_15/selectividad/criterios/Quiimica.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a></strong></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong style="text-align: left;"><span style="color: #2f310d;"><br /><a href="archivos_ies/14_15/selectividad/Tecn_Expr_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></span></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Teecnicas%20De%20Expresioon.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><span style="color: #2f310d;"><span style="font-weight: normal;">Crit. Correc.</span></span></strong></strong><strong><br /></strong></a></p>
</td>
<td align="center" bgcolor="#abF856">
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Tec_Industrial_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; margin-right: auto; margin-left: auto; display: block;" /></a></span></p>
<p><span style="color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Tecnologiia%20Industrial.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /><br /></a></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Tecnologiia%20Industrial_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a></span><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong>HISTORIA DE LA<br /></strong>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">MÚSICA Y LA DANZA</strong><strong> </strong><strong style="color: inherit; font-family: inherit; font-size: inherit; background-color: #fafbf0;"> </strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br /></strong><strong>LENGUAJE Y PRÁCTICA<br />MUSICAL</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><br /><strong>ANÁLISIS<br />MUSICAL II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br /></strong><strong>Lengua Extranjera II (Italiano)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Portugués)</strong><strong> </strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"> </td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"> </td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><strong><a href="archivos_ies/14_15/selectividad/Historia_musica_danza_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20La%20Muusica%20Y%20Danza.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong style="color: inherit; font-family: inherit;"> <a href="archivos_ies/14_15/selectividad/Lenguaje_y_pr_musical_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #abF856;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Lenguaje%20Y%20Praactica%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p> <strong style="color: inherit; font-family: inherit; font-size: inherit; background-color: #abf856;"><a href="archivos_ies/14_15/selectividad/analisis_musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #abF856;" /></a></strong><span style="color: inherit; font-family: inherit; font-size: inherit;"> </span></p>
<p> <a href="archivos_ies/14_15/selectividad/criterios/Anaalisis%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"><br /><br /><br /><br /> <a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Italiano.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"><br /><br /><br /><br /> <a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Portuguees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"> </td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"> </td>
<td align="center" bgcolor="#ffffff"> </td>
</tr>
</tbody>
</table>
<p style="text-align: justify;"> (<a href="index.php?option=com_content&amp;view=article&amp;id=243:selectividad-2014&amp;catid=108:bachillerato&amp;Itemid=147&amp;highlight=WyJzZWxlY3RpdmlkYWQiLDIwMTQsInNlbGVjdGl2aWRhZCAyMDE0Il0=" target="_blank" title="Selectividad 2014.">Exámenes de Selectividad 2014 junio</a> y <a href="index.php?option=com_content&amp;view=article&amp;id=262kYWQiXQ==" target="_blank">septiembre</a>)</p>
<ul style="text-align: justify;">
<li>
<h3 style="margin: 1px;"><span style="text-align: center; color: #2a2a2a;">22 DE ABRIL A 4 DE JUNIO: REGISTRO PARA LAS PAU.</span></h3>
</li>
<li><span style="color: #7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;">LA MATRÍCULA SE REALIZARÁ DEL 2 AL 4 DE JUNIO Y DEL 3 AL 5 DE SEPTIEMBRE.</span></li>
</ul>
<ul style="text-align: justify;">
<li>
<h3 style="margin: 1px;">PARA CONSEGUIR EL PIN (REGISTRO PARA LAS PAU) ENTRAR EN LA SIGUIENTE WEB:</h3>
</li>
</ul>
<p style="text-align: center;"><strong><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank"><span style="color: #ffffff;">HTTPS://OFICINAVIRTUAL.UGR.ES/APLI/SOLICITUDPAU/SELECTIVIDAD</span>00-MENU.JSP</a></strong></p>
<p style="text-align: justify;"> <a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/documentacion/Parametros_AyB_UA_Bachillerato_2014_2015.pdf" target="_blank" style="font-size: 18px; font-weight: bold; text-align: left; text-transform: uppercase;">PARÁMETROS DE PONDERACIÓN, CONSULTA ESTÁTICA.</a></p>
<ul style="text-align: justify;">
<li>
<p style="margin: 1px;"><strong><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/g_b_parametros_top.php" target="_blank">CONSULTA DINÁMICA</a>.</strong></p>
</li>
<li>
<h4 style="margin: 1px;"><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/" target="_blank">DISTRITO ÚNICO ANDALUZ.</a></h4>
</li>
</ul>
<ul style="text-align: justify;">
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI ENCUENTRO DE LA UNIVERSIDAD CON LOS CENTROS.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">PRESENTACIÓN: REGISTRO PARA LA <span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">PRESENTACIÓN: MATRÍCULA DE <span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank"><span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>: ORIENTACIONES.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">WEB DE LA UNIVERSIDAD PARA REGISTRO Y MATRÍCULA.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">OTROS DATOS DE INTERÉS: </a></h4>
</li>
</ul>
<ol style="text-align: justify;"><ol>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">INSCRIPCIÓN</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PLAZOS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">MATRICULACIÓN</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PRECIOS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">CALENDARIO DE LAS PRUEBAS DÍA A DÍA</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">NOTA DE ACCESO</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">EXÁMENES DE OTROS AÑOS Y ORIENTACIONES</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">ORDEN DE PREFERENCIA DE CARRERAS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PROCEDIMIENTO DE MATRÍCULA EN LA UINIVERSIDAD</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PLAZOS</a></h4>
</li>
</ol></ol>
<h4> </h4>";s:4:"body";s:0:"";s:5:"catid";s:3:"104";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-06-30 16:15:46";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"57";s:8:"ordering";s:1:"0";s:8:"category";s:17:"ÚLTIMA NORMATIVA";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:21:"286:selectividad-2015";s:7:"catslug";s:20:"104:ultima-normativa";s:6:"author";s:14:"Miguel Anguita";s:6:"layout";s:7:"article";s:4:"path";s:93:"index.php?option=com_content&view=article&id=286:selectividad-2015&catid=104:ultima-normativa";s:10:"metaauthor";N;s:6:"weight";d:1.0266200000000001;s:7:"link_id";i:1924;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:14:"Miguel Anguita";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:14:"Miguel Anguita";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:17:"ÚLTIMA NORMATIVA";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:17:"ÚLTIMA NORMATIVA";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=286";s:5:"route";s:93:"index.php?option=com_content&view=article&id=286:selectividad-2015&catid=104:ultima-normativa";s:5:"title";s:17:"Selectividad 2015";s:11:"description";s:4674:"ZONA DEDICADA A LA SELECTIVIDAD DE 2015 LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/15 (MIÉRCOLES) POR LA TARDE EN ESTE ENLACE. EXÁMENES QUE HAN SALIDO EN JUNIO 2015 DE LAS DISTINTAS MATERIAS: JUNIO 2015 - EXÁMENES 16/06/15. Martes. primer día: sin incidencias, todos a su hora con los nervios habituales. COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II HISTORIA DE ESPAÑA HISTORIA DE LA FILOSOFÍA IDIOMA EXTRANJERO 17/06/15. Miércoles. Una incidencia. Una alumna no se presenta al examen, nervios, llamada al instituto, había decidido no presentarse ¡pero se matriculó! hubiera necesitado saberlo con antelación. Nada más destacable. HISTORIA DEL ARTE MATEMÁTICAS II Y COLISIÓN TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS QUÍMICA ELECTROTÉCNIA LITERATURA UNIVERSAL LENGUAJE Y PRÁCTICA MUSICAL TECNOLOGÍA INDUSTRIAL II MATEMÁT. APLIC. A LAS CC. SOCIALES II Y COLISIÓN 18/06/15. Jueves. Dos alumnos que no se presentan, unoque llega tarde por la tarde, cosas del bus que lo ha perdido, ¡pero es que no hay taxis! ANÁLISIS MUSICAL II (¡YA!) DISEÑO (¡YA!) GEOGRAFÍA BIOLOGÍA DIBUJO TÉCNICO II CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES ECONOMÍA DE LA EMPRESA GRIEGO II HISTORIA DE LA MÚSICA Y LA DANZA DIBUJO ARTÍSTICO II FÍSICA LATÍN II Los criterios de corrección ya han sido publicados oficialmente y añadidos en cada asignatura abajo. Las soluciones: más adelante. Ya están las de Matemáticas II. Biología Ciencias de la Tierra y Medioamb. Comentario de Texto, Lengua Castellana y Literatura Dibujo Artístico II Dibujo Técnico II Economía de la Empresabgcolor="#FEFDD6"> Diseño Electrotecnia Colisión Crit. Correc. Crit. Incomp. Crit. Correc. Crit. Incomp.href="archivos_ies/14_15/selectividad/criterios/Comentario%20Texto%20Lengua%20Castellana%20Y%20Literatura.pdf" target="_blank"> Crit. Correc. Colisión Crit. Correc. Soluciones (Prof. Manuel Martínez Vela IES P. MANJÓN) Crit. Correc. Crit. Correc. Crit.Incomp. Crit. Correc. Colisión Crit. Correc. Crit. Incomp. Física Geografía Griego II Historia deEspaña Historia de la Filosofía Historia del Arte Latín II Alemán Soluciones Crit. Correc. Crit. Incomp.#2f310d; font-weight: normal;"> Crit. Correc. Crit. Incomp. Crit. Correc. Crit. Incomp Crit. Correc. Crit. Correc.bgcolor="#abFDe8"> Crit. Correc. Crit. Correc. Crit. Incomp. Crit. Correc.align="center"> Lengua Extranjera II (Francés) Lengua Extranjera II (Inglés) Literatura Universal Matemáticas Aplicadas a las Ciencias Sociales II Matemáticas II Química Técnicas de Exp. Gráf. Plást. (no disponible) Tecnología Industrial II Crit. Correc.bgcolor="#abFDD6"> Crit. Correc. Crit. Correc. Crit. Incomp. Soluciones (emestrda.net)font-size: inherit; text-align: left;"> Crit. Correc. _________ Colisión Crit. Incomp. _________ NUEVO: Soluciones (D. Germán Jesús Rubio Luna ) Crit. Correc. _________href="archivos_ies/14_15/selectividad/Mat_II_COLISION_2014_15b.pdf" target="_blank"> Colisión Crit. Incomp. _________ Soluciones (emestrda.net) Crit. Correc.#2f310d;"> Crit. Correc. Crit. Correc. Crit. Incomp. HISTORIA DE LA MÚSICA Y LA DANZAinherit; font-family: inherit; font-size: inherit; background-color: #fafbf0;"> LENGUAJE Y PRÁCTICA MUSICAL ANÁLISIS MUSICAL II Lengua Extranjera II (Italiano) Lengua Extranjera II (Portugués) Crit. Correc.href="archivos_ies/14_15/selectividad/criterios/Lenguaje%20Y%20Praactica%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;"> Crit. Correc. Crit. Correc. Crit. Correc. Crit. Correc. ( Exámenes de Selectividad 2014 junio yhref="index.php?option=com_content&view=article&id=262kYWQiXQ==" target="_blank"> septiembre ) 22 DE ABRIL A 4 DE JUNIO: REGISTRO PARA LAS PAU. LA MATRÍCULA SE REALIZARÁ DEL 2 AL 4 DE JUNIO Y DEL 3 AL 5 DE SEPTIEMBRE. PARA CONSEGUIR EL PIN (REGISTRO PARA LAS PAU) ENTRAR EN LA SIGUIENTE WEB: HTTPS://OFICINAVIRTUAL.UGR.ES/APLI/SOLICITUDPAU/SELECTIVIDAD 00-MENU.JSP PARÁMETROS DE PONDERACIÓN, CONSULTA ESTÁTICA. CONSULTA DINÁMICA . DISTRITO ÚNICO ANDALUZ. VI ENCUENTRO DE LA UNIVERSIDAD CON LOS CENTROS. PRESENTACIÓN: REGISTRO PARA LAstyle="padding: 1px 4px;"> SELECTIVIDAD . PRESENTACIÓN: MATRÍCULA DE SELECTIVIDAD . SELECTIVIDAD : ORIENTACIONES. WEB DE LA UNIVERSIDAD PARA REGISTRO Y MATRÍCULA. OTROS DATOS DE INTERÉS: INSCRIPCIÓN PLAZOS MATRICULACIÓN PRECIOS CALENDARIO DE LAS PRUEBAS DÍA A DÍA NOTA DE ACCESOhref="index.php?option=com_content&view=article&id=139&Itemid=218" target="_blank"> EXÁMENES DE OTROS AÑOS Y ORIENTACIONES ORDEN DE PREFERENCIA DE CARRERAS PROCEDIMIENTO DE MATRÍCULA EN LA UINIVERSIDAD PLAZOS";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2015-06-16 17:30:58";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2015-06-16 17:30:58";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:4;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:2:"94";s:5:"alias";s:9:"2o-bach-d";s:7:"summary";s:174800:"<p><span style="text-decoration: underline; font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">MATEMÁTICAS APLICADAS CC SS II - 2º BACH. CURSO 2014/15.</span></span></p>
<p> </p>
<h5 style="text-align: left; padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">0.   <a href="archivos_ies/14_15/mate/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2014_2015.pdf" target="_blank">ORIENTACIONES UNIVERSIDAD CURSO 2014/15</a></span> (14/10/14)</span></strong></h5>
<p style="text-align: left; padding-left: 30px;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">1.   <a href="archivos_ies/13_14/2bach_matccssii/Matrices.pdf" target="_blank">MATRICES</a></span> (14/10/14)  - <a href="archivos_ies/14_15/mate/MATRICES_2Solucionario.pdf" target="_blank">SOLUCIONES</a> </p>
<ul>
<ul>
<li><a href="archivos_ies/13_14/2bach_matccssii/matrices_2001_2013_resueltos.pdf" target="_blank"><span style="font-size: 14pt;">Exámenes de Selectividad resueltos desde 2001 hasta 2013.</span></a></li>
</ul>
</ul>
<h5 style="text-align: left; padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">2.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><a href="archivos_ies/14_15/mate/programacion_lineal_tema.pdf" target="_blank" style="color: #2f310d;">programación lineal</a></span></span></strong><strong><span style="font-size: 14pt;"> (05/11/13) - </span></strong><a href="archivos_ies/14_15/mate/programacion_lineal_soluciones_tema.pdf" target="_blank" style="font-size: 13px; font-weight: normal; color: #2f310d;">SOLUCIONES</a><span style="font-size: 13px; font-weight: normal;"> </span></h5>
<ul>
<ul>
<li><span style="font-size: 14pt;">Exámenes de Selectividad resueltos desde 2001 hasta 2013: </span><a href="http://emestrada.files.wordpress.com/2010/02/201342.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201241.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201146.pdf">2011</a> <span style="color: #3366ff;">; </span><a href="http://emestrada.files.wordpress.com/2010/02/201020.pdf">2010</a><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/2009_t31.pdf"><strong><span style="color: #3366ff;">2009</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2008_t31.pdf"><strong><span style="color: #3366ff;">2008</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2007_t31.pdf"><strong><span style="color: #3366ff;">2007</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2006_t3.pdf"><strong><span style="color: #3366ff;">2006</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2005_t3.pdf"><strong><span style="color: #3366ff;">2005</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2004_t3.pdf"><strong><span style="color: #3366ff;">2004</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2003_t3.pdf"><strong><span style="color: #3366ff;">2003</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2002_t3.pdf"><strong><span style="color: #3366ff;">2002</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2001_t3.pdf"><strong><span style="color: #3366ff;">2001</span></strong></a></li>
</ul>
</ul>
<h5 style="text-align: left; padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">3.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><a href="archivos_ies/14_15/mate/Unidad3.Determinantes.pdf" target="_blank" style="color: #2f310d;">DETERMINANTES</a></span></span></strong><strong><span style="font-size: 14pt;"> (01/11/13) </span></strong><strong><span style="font-size: 14pt;"> - </span></strong><a href="archivos_ies/14_15/mate/determinantes_solucionario.pdf" target="_blank" style="color: #2f310d; font-weight: normal; font-size: 13px;">SOLUCIONES</a><span style="font-size: 13px; font-weight: normal;"> </span></h5>
<h5 style="text-align: left; padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">4.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><a href="archivos_ies/14_15/mate/Probabilidat_tema10.pdf" target="_blank" style="color: #2f310d;">PROBABILIDAD</a> (23/11/13) </span></span></strong><strong><span style="font-size: 14pt;"> - </span></strong><span style="color: #2f310d; font-size: small;"><span style="font-weight: normal;">SOLUCIONES</span></span><span style="font-size: 13px; font-weight: normal;"> </span></h5>
<ul>
<ul>
<li><a href="archivos_ies/14_15/mate/Probabilidad_teoria.pdf" target="_blank" style="font-size: 12pt;">Teoría.</a></li>
<li><a href="archivos_ies/14_15/mate/Unidad10.Calculodeprobabilidades.pdf" target="_blank"><span style="color: #8e9326; font-size: medium;"><span style="letter-spacing: 1px;"><span style="text-decoration: underline;">Más apuntes.</span></span></span></a></li>
<li><a href="archivos_ies/14_15/mate/Probabilidad_otros_Apuntes.pdf" target="_blank" style="font-size: 12pt;">Otros apuntes.</a></li>
<li><a href="archivos_ies/14_15/mate/leygrandesnum.pdf" target="_blank">Ley de los grandes números</a>: <a href="archivos_ies/14_15/mate/LEY_GRANDES_NUMEROS_PRUEBA.xlsx" target="_blank">ejercicio excel</a> - <a href="archivos_ies/14_15/mate/ley_gr_numeros.ods" target="_blank">calc.</a></li>
<li><span style="font-size: 14pt;">Exámenes de Selectividad resueltos desde 2001 hasta 2013:  </span><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/201344.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201243.pdf">2012</a>;</span> <a href="http://emestrada.files.wordpress.com/2010/02/201148.pdf">2011</a> ; <span style="color: #3366ff;"><a href="http://emestrada.files.wordpress.com/2010/02/201046.pdf"><span style="color: #3366ff;">2010</span></a></span></strong><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/2009_t51.pdf"><span style="color: #3366ff;"><strong>2009</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2008_t52.pdf"><span style="color: #3366ff;"><strong>2008</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2007_t51.pdf"><span style="color: #3366ff;"><strong>2007</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2006_t5.pdf"><span style="color: #3366ff;"><strong>2006</strong></span></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2005.pdf"><strong><span style="color: #3366ff;">2005</span></strong></a><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/20043.pdf"><strong><span style="color: #3366ff;">2004</span></strong></a><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/2003.pdf"><strong><span style="color: #3366ff;">2003</span></strong></a><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/2002.pdf"><strong><span style="color: #3366ff;">2002</span></strong></a><strong><span style="color: #3366ff;"> ;</span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2001_t5.pdf"><span style="color: #3366ff;"><strong>2001</strong></span></a></li>
<li><span style="font-size: 14pt;"><a href="archivos_ies/Probabilidad_resueltos.pdf" target="_blank">Ejercicios de Probabilidad resueltos.</a></span></li>
</ul>
</ul>
<p> </p>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">5.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/2bach_matccssii/muestreoteoria.pdf" target="_blank">MUESTREO. DISTRIBUCIONES MUESTRALES</a> - <a href="archivos_ies/13_14/2bach_matccssii/muestreoejercicios.pdf" target="_blank">EJERCICIOS</a> - <a href="archivos_ies/13_14/2bach_matccssii/MUESTREOSolucionario.pdf" target="_blank">SOLUCIONES</a></span></span></span></strong></p>
<ul>
<ul>
<li><a href="archivos_ies/13_14/2bach_matccssii/muestreo_resumen.pdf" target="_blank"><span style="color: #2f310d; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 19px; letter-spacing: 1.3333333730697632px;"><strong>Resumen de muestreo.</strong></span></span></a></li>
<li><strong><span style="color: #3366ff; text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201345.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201244.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201149.pdf">2011</a> ; </span><span style="text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201045.pdf">2010</a></span>; </strong><a href="http://emestrada.files.wordpress.com/2010/02/2009_t6.pdf"><span style="color: #3366ff;"><strong>2009</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2008_t6.pdf"><span style="color: #3366ff;"><strong>2008</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2007_t6.pdf"><span style="color: #3366ff;"><strong>2007</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2006_t6.pdf"><span style="color: #3366ff;"><strong>2006</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2005_t6.pdf"><span style="color: #3366ff;"><strong>2005</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/200414.pdf"><span style="color: #0000ff;">2004</span></a></span></strong><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong><span style="color: #3366ff;">;</span></strong></span> </a><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/200310.pdf"><span style="color: #0000ff;">2003</span></a></span></strong><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/2002_t6.pdf"><strong><span style="color: #3366ff;">2002</span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/2001_t6.pdf"><strong><span style="color: #3366ff;">2001</span></strong></a></li>
</ul>
</ul>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">6. </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/2bach_matccssii/inferencia_tema.pdf" target="_blank">INFERENCIA ESTADÍSTICA. ESTIMACIÓN. CONTRASTE DE HIPÓTESIS</a> - SOLUCIONES<br /></span></span></span></strong></p>
<ul>
<ul>
<li><strong><span style="color: #3366ff;"><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/201346.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201232.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201163.pdf"><span style="color: #0000ff;">2011</span></a></span>; <a href="http://emestrada.files.wordpress.com/2010/02/201054.pdf">2010</a></span></strong></li>
</ul>
</ul>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">7.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt;">LÍMITES Y CONTINUIDAD</span></span></strong></p>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">8.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;">DERIVADA DE UNA FUNCIÓN</span></span></span></strong></p>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">9.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;">APLICACIONES DE LA DERIVADA</span></span></span></strong></p>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">10. </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;">REPRESENTACIÓN DE FUNCIONES</span></span></span></strong></p>
<ul>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #0000ff; text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201343.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201259.pdf">2012</a>;</span>  <a href="http://emestrada.files.wordpress.com/2010/02/201147.pdf">2011</a> </span></strong><span style="color: #3366ff;"><strong>; </strong></span><strong><a href="http://emestrada.files.wordpress.com/2010/02/201037.pdf">2010</a></strong><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2009_t42.pdf"><span style="color: #3366ff;"><strong>2009</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2008_t41.pdf"><span style="color: #3366ff;"><strong>2008</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2007_t41.pdf"><span style="color: #3366ff;"><strong>2007</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2006_t4.pdf"><span style="color: #3366ff;"><strong>2006</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2005_t4.pdf"><span style="color: #3366ff;"><strong>2005</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2004_t4.pdf"><span style="color: #3366ff;"><strong>2004</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2003_t4.pdf"><span style="color: #3366ff;"><strong>2003</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2002_t4.pdf"><span style="color: #3366ff;"><strong>2002</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2001_t4.pdf"><span style="color: #3366ff;"><strong>2001</strong></span></a></li>
</ul>
</ul>
<p> </p>
<hr />
<p><span style="text-decoration: underline;"><strong><span style="font-size: 14pt;">Últimos exámenes de Selectividad:</span></strong></span></p>
<p><span><span style="font-size: 14pt;">Con nuestro agradecimiento a nuestro compañero del IES Ayala Germán-Jesús Rubio Luna por sus soluciones.</span></span></p>
<p style="padding-left: 60px;"><span> </span><strong style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: center; background-color: #ffffff;"><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center;"><span style="text-decoration: underline;">Junio 2014</span> </strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" alt="" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank" style="background-color: #ffffff;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluc.</strong></a></p>
<p style="padding-left: 60px;"><a href="archivos_ies/13_14/selectividad/mat_ccss_ii_sept_2014.pdf" target="_blank" title="Matemáticas Aplicadas a las Ciencias Sociales II." style="text-align: -webkit-center; background-color: #ffffff;">Sept. 2014<strong style="color: #000000;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" alt="" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; background-color: #ffffff;"> </span></p>
<center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_exjun.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio (Modelo ) 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_soljun.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_exjun_espe.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio Colisiones (Modelo ) 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_soljun_espe.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_exsep.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre (Modelo ) 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_solsep.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
<br /><br /></center>
<ul>
<li><span style="text-decoration: underline;"><strong><span style="font-size: 14pt; color: #2b2721; text-decoration: underline;">Junio 2013:</span></strong></span></li>
</ul>
<ol><ol>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/selectividad_2013_Segundo_dia_2_MAT_APL_CCSS_II_A.pdf" target="_blank">Opción A.</a> <a href="archivos_ies/13_14/2bach_matccssii/2013_soljun.pdf" target="_blank">Soluciones.</a></span></span></li>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/selectividad_2013_Segundo_dia_2_MAT_APL_CCSS_II_B.pdf" target="_blank">Opción B.</a>  <a href="archivos_ies/13_14/2bach_matccssii/2013_soljun.pdf" target="_blank">Soluciones.</a></span></span></li>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/2013_exjun_esp.pdf" target="_blank">Específico (4º día)</a>  <a href="archivos_ies/13_14/2bach_matccssii/2013_soljun_esp.pdf" target="_blank">Soluciones.</a></span></span></li>
</ol></ol>
<ul>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"> </span></span><strong><span style="font-size: 14pt; color: #2b2721; text-decoration: underline;">Sept. 2013:</span></strong></li>
</ul>
<ol><ol>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/MAT_CCSSII_A_Selec_sep2013.pdf" target="_blank">Opción A.</a>  </span></span><a href="archivos_ies/13_14/2bach_matccssii/Sept_2012_selec_MatCCSSII_y_soluciones2.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 19px;">Soluciones.</a></li>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/MAT_CCSSII_B_Selec_sep2013.pdf">Opción B.</a>   </span></span><a href="archivos_ies/13_14/2bach_matccssii/2013_solsep.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 19px;">Soluciones.</a></li>
</ol></ol>
<ul>
<li><a href="archivos_ies/13_14/2bach_matccssii/Junio_2012_selec_MatCCSSII_y_soluciones.pdf" target="_blank"><span style="text-decoration: underline;"><strong><span style="font-size: 14pt; color: #2b2721; text-decoration: underline;">Junio 2012 con soluciones.</span></strong></span><span style="font-size: 19px; color: #2b2721;"> </span></a></li>
</ul>
<ul>
<li><span style="text-decoration: underline;"><strong><span style="font-size: 14pt; color: #2b2721; text-decoration: underline;"><a href="archivos_ies/13_14/2bach_matccssii/Sept_2012_selec_MatCCSSII_y_soluciones2.pdf" target="_blank">Sept. 2012 con soluciones.<br /><br /></a></span></strong></span></li>
</ul>
<div style="text-align: center;"><hr /><a href="index.php?option=com_content&amp;view=article&amp;id=96" target="_blank">ENTRAR EN ZONA PRIVADA DEL CURSO.</a><hr />
<table style="color: #000000; font-family: verdana; font-size: 11px; line-height: 16.6399993896484px; text-align: left;" width="80%" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td><strong>Asignatura</strong></td>
<td align="center"><strong>Orientaciones<br />2014/2015</strong></td>
<td align="center"><strong>2005</strong></td>
<td align="center"><strong>2006</strong></td>
<td align="center"><strong>2007</strong></td>
<td align="center"><strong>2008</strong></td>
<td align="center"><strong>2009</strong></td>
<td align="center"><strong>2010</strong></td>
<td align="center"><strong>2011</strong></td>
<td align="center"><strong>2012</strong></td>
<td align="center"><strong>2013</strong></td>
<td align="center"><strong>2014</strong></td>
</tr>
<tr>
<td>Análisis Musical II <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_analisis_musical_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="1.66 Mb" title="1.66 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="5.31 Mb" title="5.31 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="170.95 Mb" title="170.95 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="265.22 Mb" title="265.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="213.99 Mb" title="213.99 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Biología <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_biologia_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="2.83 Mb" title="2.83 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.09 Mb" title="2.09 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.29 Mb" title="1.29 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.26 Mb" title="1.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.07 Mb" title="2.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.54 Mb" title="2.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.5 Mb" title="1.5 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Ciencias de la Tierra y Medioambientales <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_ciencias_de_la_tierra_y_medioambientales_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.64 Mb" title="0.64 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.44 Mb" title="1.44 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="14.79 Mb" title="14.79 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.47 Mb" title="1.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.88 Mb" title="1.88 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.45 Mb" title="1.45 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.58 Mb" title="0.58 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Comentario de Texto,Lengua Castellana y Literatura<em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_comentario_texto_lengua_castellana_y_literatura_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.4 Mb" title="0.4 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.95 Mb" title="1.95 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Dibujo Artístico II <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_artistico_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="13.76 Mb" title="13.76 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.22 Mb" title="2.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.34 Mb" title="1.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="5.2 Mb" title="5.2 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="27.54 Mb" title="27.54 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="13.52 Mb" title="13.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="59.45 Mb" title="59.45 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.72 Mb" title="3.72 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="7.72 Mb" title="7.72 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Dibujo Técnico II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_tecnico_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.56 Mb" title="2.56 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="6.86 Mb" title="6.86 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="8.05 Mb" title="8.05 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="15.45 Mb" title="15.45 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="12.99 Mb" title="12.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="6.97 Mb" title="6.97 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="7.72 Mb" title="7.72 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Diseño <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_diseno_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="12.38 Mb" title="12.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.66 Mb" title="0.66 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.12 Mb" title="1.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.76 Mb" title="3.76 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="23.21 Mb" title="23.21 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Economía de la Empresa <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_economia_de_la_empresa_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.1 Mb" title="0.1 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.31 Mb" title="0.31 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.96 Mb" title="3.96 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.38 Mb" title="1.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Electrotecnia <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_electrotecnia_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.46 Mb" title="0.46 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Física <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_fisica_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.26 Mb" title="0.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.28 Mb" title="0.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.25 Mb" title="0.25 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.39 Mb" title="1.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.36 Mb" title="0.36 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Geografía <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_geografia_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.29 Mb" title="0.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.09 Mb" title="3.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.32 Mb" title="1.32 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="12.41 Mb" title="12.41 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.11 Mb" title="2.11 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.86 Mb" title="3.86 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.98 Mb" title="1.98 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.38 Mb" title="1.38 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Griego II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_griego_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.54 Mb" title="0.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.6 Mb" title="0.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.35 Mb" title="2.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="4.69 Mb" title="4.69 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.57 Mb" title="0.57 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.51 Mb" title="0.51 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.36 Mb" title="0.36 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Historia de España <em>(Común)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_espanna_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.74 Mb" title="0.74 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.73 Mb" title="1.73 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.77 Mb" title="1.77 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.74 Mb" title="0.74 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia de la Filosofía <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_filosofia_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.05 Mb" title="0.05 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.68 Mb" title="0.68 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Historia de la Música y de la Danza <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_musica_y_de_la_danza_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="3.3 Mb" title="3.3 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="27.28 Mb" title="27.28 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="22.08 Mb" title="22.08 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.51 Mb" title="1.51 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="13.4 Mb" title="13.4 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.38 Mb" title="2.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="318.43 Mb" title="318.43 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="10.61 Mb" title="10.61 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="93.24 Mb" title="93.24 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="101.86 Mb" title="101.86 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="121.51 Mb" title="121.51 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia del Arte <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_del_arte_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.49 Mb" title="0.49 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.67 Mb" title="0.67 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.16 Mb" title="3.16 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="8.71 Mb" title="8.71 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="6.91 Mb" title="6.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.5 Mb" title="3.5 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="18.28 Mb" title="18.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.06 Mb" title="1.06 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Latín II <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_latin_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.94 Mb" title="0.94 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.94 Mb" title="1.94 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1 Mb" title="1 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.55 Mb" title="0.55 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Alemán) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(aleman)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.26 Mb" title="0.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.35 Mb" title="1.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.39 Mb" title="0.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.3 Mb" title="1.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Francés) <em>(Común)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(frances)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.1 Mb" title="0.1 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Inglés) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(ingles)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="2.16 Mb" title="2.16 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.68 Mb" title="1.68 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.06 Mb" title="2.06 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.18 Mb" title="1.18 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="12.19 Mb" title="12.19 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.74 Mb" title="1.74 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.16 Mb" title="1.16 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Italiano) <em>(Común)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(italiano)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.29 Mb" title="0.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.75 Mb" title="0.75 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.31 Mb" title="0.31 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Portugués) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(portugues)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.32 Mb" title="0.32 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.78 Mb" title="0.78 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.77 Mb" title="0.77 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lenguaje y Práctica Musical <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lenguaje_y_practica_musical_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="1.4 Mb" title="1.4 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="92.65 Mb" title="92.65 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.31 Mb" title="2.31 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="80.36 Mb" title="80.36 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="64.72 Mb" title="64.72 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="60.71 Mb" title="60.71 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Literatura Universal <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_literatura_universal_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.76 Mb" title="0.76 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Matemáticas Aplicadas a las Ciencias Sociales II<em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.62 Mb" title="0.62 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="6.51 Mb" title="6.51 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.42 Mb" title="1.42 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.53 Mb" title="1.53 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.53 Mb" title="1.53 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Matemáticas II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="4.39 Mb" title="4.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.54 Mb" title="1.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.13 Mb" title="1.13 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Química <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_quimica_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.34 Mb" title="0.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.34 Mb" title="0.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.19 Mb" title="1.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.5 Mb" title="0.5 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.66 Mb" title="1.66 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.61 Mb" title="1.61 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Técnicas de Expresión Gráfico Plásticas <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnicas_de_expresion_grafico-plastica_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.85 Mb" title="0.85 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="10.44 Mb" title="10.44 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.86 Mb" title="0.86 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="4.72 Mb" title="4.72 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="8.63 Mb" title="8.63 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.41 Mb" title="1.41 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.99 Mb" title="1.99 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Tecnología Industrial II <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnologia_industrial_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.14 Mb" title="1.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.12 Mb" title="1.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a><br /><br /></td>
</tr>
</tbody>
</table>
<hr />
<table class="tabla_redonda" style="border: 1px solid #ced5d7; border-radius: 6px; padding: 10px 15px; box-shadow: #b5c1c5 0px 5px 10px, #eef5f7 0px 0px 0px 10px inset; color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left; background-color: #ffffff;" width="98%" border="0"><caption class="titulo" style="font-weight: bold; color: #ff9900; font-size: 16px; border: 1px solid #aaaaaa; vertical-align: middle; height: 45px; margin: 10px auto; -webkit-box-shadow: black 0px 0px 4px; box-shadow: black 0px 0px 4px; text-shadow: black 1px 1px 1px; padding-top: 8px; width: 80%; background: #e8edff;">CALENDARIO DE LA PRUEBA DE BACHILLERATO</caption></table>
<h3 style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-transform: none;" align="center"><strong>CURSO 2014/2015</strong></h3>
<table style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;" width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td align="left"><strong>Convocatoria ordinaria:</strong> <br />16, 17, 18 de junio de 2015</td>
<td align="right"><strong>Convocatoria extraordinaria:</strong> <br />15, 16 y 17 de septiembre de 2015</td>
</tr>
</tbody>
</table>
<br style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;" />
<table style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;" width="90%" border="0" cellspacing="2" cellpadding="3" align="center">
<tbody>
<tr>
<td align="center" bgcolor="#D8D8D8">HORARIO</td>
<td align="center" bgcolor="#F3F781" width="25%">PRIMER DÍA</td>
<td align="center" bgcolor="#F7BE81" width="25%">SEGUNDO DÍA</td>
<td align="center" bgcolor="#00FF80" width="25%">TERCER DÍA</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td align="center" bgcolor="#D8D8D8">HORARIO</td>
<td align="center" bgcolor="#CEE3F6" width="25%">TERCER DÍA (TARDE)<br />Exclusivo para incompatabilidades</td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">08:00-08:30</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td align="center" bgcolor="#D8D8D8">16:00-16:30</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">08:30-10:00</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>HISTORIA DEL ARTE</li>
<li>MATEMÁTICAS II</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>ANÁLISIS MUSICAL II</li>
<li>DISEÑO</li>
<li>GEOGRAFÍA</li>
<li>BIOLOGÍA</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">16:30-18:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">10:00-10:45</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">18:00-18:30</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">10:45-12:15</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>HISTORIA DE ESPAÑA</li>
<li>HISTORIA DE LA FILOSOFÍA</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS</li>
<li>QUÍMICA</li>
<li>ELECTROTÉCNIA</li>
<li>LITERATURA UNIVERSAL</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>DIBUJO TÉCNICO II</li>
<li>CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES</li>
<li>ECONOMÍA DE LA EMPRESA</li>
<li>GRIEGO II</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">18:30-20:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">12:15-13:00</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">20:00-20:30</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">13:00-14:30</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>IDIOMA EXTRANJERO</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>LENGUAJE Y PRÁCTICA MUSICAL</li>
<li>TECNOLOGÍA INDUSTRIAL II</li>
<li>MATEMÁT. APLIC. A LAS CC. SOCIALES II</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>HISTORIA DE LA MÚSICA Y LA DANZA</li>
<li>DIBUJO ARTÍSTICO II</li>
<li>FÍSICA</li>
<li>LATÍN II</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">20:30-22:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
</tbody>
</table>
<br style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;" />
<p class="texto_azul" style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;"><strong>NOTAS IMPORTANTES:</strong></p>
<ol style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;">
<li>Las franjas horarias de citación son en defecto de que la universidad no fije otras que en razón de las sedes de que se traten, considere más oportunas; asi mismo sí la última franja horaria del tercer día para incompatibilidades, quedase libre, el descanso entre la primera y segunda franja horaria será de 45 minutos.</li>
<li>El horario de las materias del tercer día por la tarde es <strong><span style="text-decoration: underline;">exclusivamente</span></strong> para quienes tienen más de un examen el mismo día y a la misma hora (en el 2º o 3º día por la mañana), debiendo realizar en dicho horario y en primer lugar el examen que aparece antes en el respectivo cuadro y realizará el examen de la otra materia en el tercer día por la tarde en el horario que se indicará oportunamente.</li>
</ol>
<p style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;"> </p>
<div style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;"><br /><br /><br /><span style="font-size: xx-small;"><strong>Nota de exención de responsabilidad</strong></span></div>
<p style="padding-left: 30px; text-align: center;"><span style="font-size: x-small; color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;">Las informaciones ofrecidas por este medio tienen exclusivamente carácter ilustrativo, y no originarán derechos ni expectativas de derechos.</span><span style="font-size: x-small; color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;"> </span><a href="http://juntadeandalucia.es/boja/1995/136/1" target="_blank" style="font-size: x-small; line-height: 16.6399993896484px;">(Decreto 204/95, artículo 4; BOJA 136 de 26 de Octubre)</a></p>
<hr />
<p style="padding-left: 30px; text-align: center;"> </p>
<h2 style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: -webkit-center; text-transform: none;"><span style="font-size: x-large;">Exámenes resueltos de Matemáticas CCSS II de Andalucía</span></h2>
<p><span style="color: #000000; font-size: medium; text-align: start; font-family: Verdana;">Realizados por:</span></p>
<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: medium;" align="center"><span style="font-family: Arial;"><strong><span style="font-size: large;">D. Germán Jesús Rubio Luna, Catedrático de Matemáticas </span></strong><span style="font-size: large;"><br /></span><strong><span style="font-size: large;">del IES Francisco Ayala de Granada</span>.</strong></span></p>
<p style="padding-left: 30px;"><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif;">Desde el año 2004-2005 hasta el año 2012-2013 </span></p>
<table id="table20" width="55%" border="1">
<tbody>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2013" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2013</strong></span></a></p>
</td>
<td align="center">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2012" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2012</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2011" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2011</strong></span></a></p>
</td>
<td align="center">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2010" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2010</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#hipotesis2009" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Problemas Test Hipótesis. Comisión año 2009</strong></span></a></p>
</td>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#proporciones2007" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Intervalos Proporciones. Comisión año 2007</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2009" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2009</strong></span></a></p>
</td>
<td align="center">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2008" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2008</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2007" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2007</strong></span></a></p>
</td>
<td align="center">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2006" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2006</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2005" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2005</strong></span></a></p>
</td>
<td align="center">
<p><span style="font-family: 'Times New Roman'; font-size: xx-small;"><strong>Modelos del año 2004</strong></span></p>
</td>
</tr>
</tbody>
</table>
<center><br /><br /><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2013"></a></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Año 2012-2013 </span></strong></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2013"></a></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2013"></a></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio Reserva 2 (Modelo 1) 2013</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_exsep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre (Modelo 2) 2013</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_solsep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_exjun_esp.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio Colisión (Modelo 5)2013</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_soljun_esp.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_exjun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio (Modelo 6) 2013</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_soljun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2013"></a><span style="font-family: Arial;"><a name="2012"></a></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Año 2011-2012</span></strong><span style="font-size: medium;"> </span></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2012"></a></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2012"></a></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod1_12.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de 2012</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod1_12_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod2_12.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 de 2012</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod2_12_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/sep12gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 de 2012 (Septiembre General)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/solsep12gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/jun12gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de 2012 (Junio General)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/soljun12gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod5_12.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de 2012</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod5_12_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod6_12.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de 2012</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod6_12_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2012"></a></span></span></span><center><a name="2012"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a></center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2011"></a></span></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Año 2010-2011</span></strong> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2011"></a></span></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2011"></a></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod1_11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de 2011</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod1_11_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod2_11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 de 2011</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod2_11_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod3_11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 de 2011</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod3_11_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/jun11gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de 2011 (Junio General)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/soljun11gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/sept11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de 2011 (Septiembre)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/solsept11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/jun11espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de 2011 (Junio Específico)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/soljun11espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2011"></a></span></span></span></span><center><a name="2011"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a></center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2010"></a></span></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Año 2009-2010 </span></strong><span style="font-size: medium;"> </span></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2010"></a></span></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2010"></a></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2010</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod2.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 de sobrantes del 2010</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod2_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/sep10gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre de 2010 (General Modelo 3)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/solsep10gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/jun10espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio de 2010 (Específico Modelo 4)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/soljun10espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/jun10gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio de 2010 (General Modelo 5)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/soljun10gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2010</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2010"></a><a name="hipotesis2009"></a></span></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Problemas Test de Hipósis (Propuestos el año 2009) </span></strong><span style="font-size: medium;"> </span></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="hipotesis2009"></a></span></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="hipotesis2009"></a></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/hipotesis/09_test.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciados</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Problemas Test de Hipósis (Propuestos el año 2009)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/hipotesis/09_test_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Soluciones</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="hipotesis2009"></a></span></span></span></span><center><a name="hipotesis2009"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a></center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2009"></a><strong><span style="font-family: Arial; font-size: large;">Año 2008-2009</span></strong></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod2(sep).pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Septiembre) de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod2(sep)_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod3(junio).pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 (Junio) de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod3(junio)_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod5.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod5_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2009"></a></span></span></span></span><center><a name="2009"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a> <br /><br /></center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2008"></a><strong><span style="font-family: Arial; font-size: large;">Año 2007-2008</span></strong></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod2_Sep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Septiembre) de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod2_Sep_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod3_jun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 (Junio) de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod3_jun_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod5.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod5_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2008"></a><a name="proporciones2007"></a></span></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Intervalos de Confianza Proporciones (Propuestos el año 2007) </span></strong><span style="font-size: medium;"> </span></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="proporciones2007"></a></span></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="proporciones2007"></a></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/proporciones/07_propor.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciados</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Intervalos de Proporciones (Propuestos el año 2007)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/proporciones/07_propor_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Soluciones</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="proporciones2007"></a></span></span></span></span><center><a name="proporciones2007"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a> <br /><br /><a name="2007"></a><strong><span style="font-family: Arial; font-size: large;">Año 2006-2007</span></strong><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod2_Junio.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Junio) de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod2_Junio_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod3_sep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 (Septiembre) de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod3_sep_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod5.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod5_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><center><a name="2007"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a> <br /><a name="2006"></a><strong><span style="font-family: Arial; font-size: large;">Año 2005-2006</span></strong><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod2_sep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Septiembre) de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod2_sep_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod3_jun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 (Junio) de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod3_jun_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod5.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod5_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><center><a name="2006" style="color: #000000; font-family: Arial; letter-spacing: normal; text-align: -webkit-center;"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue; font-family: Arial; letter-spacing: normal; text-align: -webkit-center;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a><span style="color: #000000;"> </span><br style="color: #000000;" /><a name="2005" style="color: #000000; font-family: Arial; letter-spacing: normal; text-align: -webkit-center;"></a><strong><span style="font-family: Arial; font-size: large;">Año 2004-2005</span></strong><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod2_jun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Junio) de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod2_jun_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod3.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod3_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod5_sep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 (Septiembre) de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod5_sep_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center></center></center></center></center>
<p style="padding-left: 30px;"> </p>
<p style="padding-left: 30px;"><strong><span style="text-decoration: underline;"><span style="font-family: 'comic sans ms',sans-serif;"><span style="font-size: 14pt;"> </span></span></span></strong></p>
</div>";s:4:"body";s:0:"";s:5:"catid";s:2:"38";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-02-13 10:01:23";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"93";s:8:"ordering";s:1:"3";s:8:"category";s:12:"Matemáticas";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:12:"94:2o-bach-d";s:7:"catslug";s:15:"38:matematicas5";s:6:"author";N;s:6:"layout";s:7:"article";s:4:"path";s:90:"index.php?option=com_content&view=article&id=94:2o-bach-d&catid=38:matematicas5&Itemid=668";s:10:"metaauthor";N;s:6:"weight";d:1.0266200000000001;s:7:"link_id";i:1656;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"Matemáticas";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"Matemáticas";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:47:"index.php?option=com_content&view=article&id=94";s:5:"route";s:90:"index.php?option=com_content&view=article&id=94:2o-bach-d&catid=38:matematicas5&Itemid=668";s:5:"title";s:10:"2º BACH D";s:11:"description";s:15845:"MATEMÁTICAS APLICADAS CC SS II - 2º BACH. CURSO 2014/15. 0. ORIENTACIONES UNIVERSIDAD CURSO 2014/15 (14/10/14) 1. MATRICES (14/10/14) - SOLUCIONES Exámenes de Selectividad resueltos desde 2001 hasta 2013. 2. programación lineal (05/11/13) -#2f310d;"> SOLUCIONES Exámenes de Selectividad resueltos desde 2001 hasta 2013: 2013 ; 2012 ; 2011 ; 2010 ; 2009 ; 2008 ; 2007 ; 2006 ; 2005 ; 2004 ; 2003 ; 2002style="color: #3366ff;"> ; 2001 3. DETERMINANTES (01/11/13) - SOLUCIONES 4. PROBABILIDAD (23/11/13) - SOLUCIONES Teoría.target="_blank"> Más apuntes. Otros apuntes. Ley de los grandes números : ejercicio excel - calc. Exámenes de Selectividad resueltos desde 2001 hasta 2013: 2013 ; 2012 ; 2011 ; 2010 ; 2009 ; 2008 ; 2007 ; 2006 ;#3366ff;"> 2005 ; 2004 ; 2003 ; 2002 ; 2001 Ejercicios de Probabilidad resueltos. 5. MUESTREO. DISTRIBUCIONES MUESTRALES - EJERCICIOS - SOLUCIONES Resumen de muestreo.#3366ff; text-decoration: underline;"> 2013 ; 2012 ; 2011 ; 2010 ; 2009 ; 2008 ; 2007 ; 2006 ; 2005 ; 2004 ;href="http://emestrada.files.wordpress.com/2010/02/200310.pdf"> 2003 ; 2002 ; 2001 6. INFERENCIA ESTADÍSTICA. ESTIMACIÓN. CONTRASTE DE HIPÓTESIS - SOLUCIONES 2013 ; 2012 ; 2011 ; 2010 7.letter-spacing: 1pt;"> LÍMITES Y CONTINUIDAD 8. DERIVADA DE UNA FUNCIÓN 9. APLICACIONES DE LA DERIVADA 10. REPRESENTACIÓN DE FUNCIONES 2013 ; 2012 ; 2011 ; 2010 ;href="http://emestrada.files.wordpress.com/2010/02/2009_t42.pdf"> 2009 ; 2008 ; 2007 ; 2006 ; 2005 ; 2004 ; 2003 ; 2002 ; 2001 Últimos exámenes de Selectividad: Con nuestro agradecimiento a nuestro compañero del IES Ayala Germán-Jesús Rubio Luna por sus soluciones.font-family: Verdana, Arial, Helvetica, sans-serif; text-align: center; background-color: #ffffff;"> Junio 2014 Soluc. Sept. 2014 Enunciado Examen de Junio (Modelo ) 2014 Soluciónhref="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_exjun_espe.pdf" target="_blank" style="text-decoration: none; color: blue;"> Enunciado Examen de Junio Colisiones (Modelo ) 2014 Solución Enunciado Examen de Septiembre (Modelo ) 2014 Solución Junio 2013: Opción A. Soluciones. Opción B.target="_blank"> Soluciones. Específico (4º día) Soluciones. Sept. 2013: Opción A. Soluciones. Opción B. Soluciones. Junio 2012 con soluciones. Sept. 2012 con soluciones.href="index.php?option=com_content&view=article&id=96" target="_blank"> ENTRAR EN ZONA PRIVADA DEL CURSO. Asignatura Orientaciones 2014/2015 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 Análisis Musical II (Modalidad)Mb" align="center" /> Biología (Modalidad)align="center" />href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_biologia.zip"> Ciencias de la Tierra y Medioambientales (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ciencias_tierra.zip">src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.58 Mb" title="0.58 Mb" align="center" /> Comentario de Texto,Lengua Castellana y Literatura (Común)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_castellana.zip"> Dibujo Artístico II (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_artistico.zip">src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="7.72 Mb" title="7.72 Mb" align="center" /> Dibujo Técnico II (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_tecnico.zip"> Diseño (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_diseno_2014_2015.pdf">href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fundamento_diseno.zip"> Economía de la Empresa (Modalidad)bgcolor="#FEFDD6">href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_economia.zip"> Electrotecnia (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_electrotecnia.zip">src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /> Física (Modalidad)src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.28 Mb" title="0.28 Mb" align="center" />title="0.38 Mb" align="center" /> Geografía (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_geografia.zip"> Griego II (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_griego_ii_2014_2015.pdf">href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_griego.zip"> Historia de España (Común)align="center"> Historia de la Filosofía (Común)border="0" alt="0.05 Mb" title="0.05 Mb" align="center" /> Historia de la Música y de laDanza (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_musica.zip"> Historia del Arte (Modalidad)bgcolor="#FEFDD6">href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_arte.zip"> Latín II (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_latin.zip">border="0" alt="1 Mb" title="1 Mb" align="center" /> Lengua Extranjera II (Alemán) (Común)border="0" alt="0.26 Mb" title="0.26 Mb" align="center" />href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_aleman.zip"> Lengua Extranjera II (Francés) (Común)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_frances.zip"> Lengua Extranjera II (Inglés) (Común)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(ingles)_2014_2015.pdf">href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ingles.zip"> Lengua Extranjera II (Italiano) (Común)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_italiano.zip">src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="center" /> Lengua Extranjera II (Portugués) (Común)Mb" title="0.32 Mb" align="center" /> Lenguaje y Práctica Musical (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_practica_musical.zip"> Literatura Universal (Modalidad)/> Matemáticas Aplicadas a las Ciencias Sociales II (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2014_2015.pdf">href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicas_aplicadas.zip"> Matemáticas II (Modalidad)bgcolor="#FEFDD6">href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicasII.zip"> Química (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_quimica.zip">src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.66 Mb" title="1.66 Mb" align="center" /> Técnicas de Expresión Gráfico Plásticas (Modalidad)href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnicas_expresion_grafica.zip">bgcolor="#FEFDD6"> Tecnología Industrial II (Modalidad)/>href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_tecnologia_industrial.zip"> CALENDARIO DE LA PRUEBA DE BACHILLERATO CURSO 2014/2015 Convocatoria ordinaria: 16, 17, 18 de junio de 2015 Convocatoria extraordinaria: 15, 16 y 17 de septiembre de 2015align="center" bgcolor="#D8D8D8"> HORARIO PRIMER DÍA SEGUNDO DÍA TERCER DÍA HORARIO TERCER DÍA (TARDE) Exclusivo para incompatabilidades 08:00-08:30 CITACIÓN CITACIÓN CITACIÓN 16:00-16:30 CITACIÓN 08:30-10:00 COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II HISTORIA DEL ARTE MATEMÁTICAS II ANÁLISIS MUSICAL II DISEÑO GEOGRAFÍA BIOLOGÍA 16:30-18:00 10:00-10:45 DESCANSO DESCANSO DESCANSO 18:00-18:30 DESCANSO 10:45-12:15 HISTORIA DE ESPAÑA HISTORIA DE LA FILOSOFÍAclass="selectividad"> TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS QUÍMICA ELECTROTÉCNIA LITERATURA UNIVERSAL DIBUJO TÉCNICO II CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES ECONOMÍA DE LA EMPRESA GRIEGO II 18:30-20:00 12:15-13:00 DESCANSO DESCANSO DESCANSO 20:00-20:30 DESCANSO 13:00-14:30 IDIOMA EXTRANJERO LENGUAJE Y PRÁCTICA MUSICAL TECNOLOGÍA INDUSTRIAL II MATEMÁT. APLIC. A LAS CC. SOCIALES II HISTORIA DE LA MÚSICA Y LA DANZA DIBUJO ARTÍSTICO II FÍSICA LATÍN II 20:30-22:00 NOTAS IMPORTANTES: Las franjas horariasde citación son en defecto de que la universidad no fije otras que en razón de las sedes de que se traten, considere más oportunas; asi mismo sí la última franja horaria del tercer día para incompatibilidades, quedase libre, el descanso entre la primera y segunda franja horaria será de 45 minutos. El horario de las materias del tercer día por la tarde es exclusivamente para quienes tienen más de un examen el mismo día y a la misma hora (en el 2º o 3º día por la mañana), debiendo realizar en dicho horario y en primer lugar el examen que aparece antes en el respectivo cuadro y realizará el examen de la otra materia en el tercer día por la tarde en el horario que se indicará oportunamente. Nota de exención de responsabilidad Las informaciones ofrecidas por este medio tienen exclusivamente carácter ilustrativo, y no originarán derechos ni expectativas de derechos. (Decreto 204/95, artículo 4; BOJA 136 de 26 de Octubre)-webkit-center; text-transform: none;"> Exámenes resueltos de Matemáticas CCSS II de Andalucía Realizados por: D. Germán Jesús Rubio Luna, Catedrático de Matemáticas del IES Francisco Ayala de Granada . Desde el año 2004-2005 hasta el año 2012-2013 Modelos del año 2013 Modelos del año 2012 Modelos del año 2011 Modelos del año2010 Problemas Test Hipótesis. Comisión año 2009 Intervalos Proporciones. Comisión año 2007 Modelos del año 2009 Modelos del año 2008 Modelos del año 2007 Modelos del año 2006color: blue;"> Modelos del año 2005 Modelos del año 2004 Año 2012-2013 Enunciado Examen de Junio Reserva 2 (Modelo 1) 2013 Solución Enunciado Examen de Septiembre (Modelo 2) 2013 Soluciónhref="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_exjun_esp.pdf" style="text-decoration: none; color: blue;"> Enunciado Examen de Junio Colisión (Modelo 5)2013 Solución Enunciado Examen de Junio (Modelo 6) 2013 Solución Año 2011-2012xx-small;"> Enunciado Examen Modelo 1 de 2012 Solución Enunciado Examen Modelo 2 de 2012 Solución Enunciado Examen Modelo 3 de 2012 (Septiembre General) Solución Enunciado Examen Modelo 4 de 2012 (Junio General)color: blue;"> Solución Enunciado Examen Modelo 5 de 2012 Solución Enunciado Examen Modelo 6 de 2012 Solución Año 2010-2011Arial;"> Enunciado Examen Modelo 1 de 2011 Solución Enunciado Examen Modelo 2 de 2011 Solución Enunciado Examen Modelo 3 de 2011none; color: blue;"> Solución Enunciado Examen Modelo 4 de 2011 (Junio General) Solución Enunciado Examen Modelo 5 de 2011 (Septiembre) Solución Enunciado Examen Modelo 6 de 2011 (Junio Específico) Soluciónname="2011"> Año 2009-2010 Enunciado Examen Modelo 1 de sobrantes del 2010 Solución Enunciado Examen Modelo 2 desobrantes del 2010 Solución Enunciado Examen de Septiembre de 2010 (General Modelo 3) Solución Enunciado Examen de Junio de 2010 (Específico Modelo 4) Solución Enunciado Examen de Junio de 2010 (General Modelo 5)xx-small;"> Solución Enunciado Examen Modelo 6 de sobrantes del 2010 Solución Problemas Test de Hipósis (Propuestos el año 2009) Enunciados Problemas Test de Hipósis (Propuestos el año 2009)blue;"> Soluciones Año 2008-2009 Enunciado Examen Modelo 1 de sobrantes del 2009 Solución Enunciado Examen Modelo 2 (Septiembre) de sobrantes del 2009style="text-decoration: none; color: blue;"> Solución Enunciado Examen Modelo 3 (Junio) de sobrantes del 2009 Solución Enunciado Examen Modelo 4 de sobrantes del 2009 Solución Enunciado Examen Modelo 5 de sobrantes del 2009 Soluciónnone; color: blue;"> Enunciado Examen Modelo 6 de sobrantes del 2009 Solución Año 2007-2008 Enunciado Examen Modelo 1 de sobrantes del 2008 Soluciónstyle="text-decoration: none; color: blue;"> Enunciado Examen Modelo 2 (Septiembre) de sobrantes del 2008 Solución Enunciado Examen Modelo 3 (Junio) de sobrantes del 2008 Solución Enunciado Examen Modelo 4 de sobrantes del 2008 Solución Enunciado Examen Modelo 5 de sobrantes del 2008href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod5_sol.pdf" style="text-decoration: none; color: blue;"> Solución Enunciado Examen Modelo 6 de sobrantes del 2008 Solución Intervalos de Confianza Proporciones (Propuestos el año 2007) Enunciados Intervalos de Proporciones (Propuestos elaño 2007) Soluciones Año 2006-2007 Enunciado Examen Modelo 1 de sobrantes del 2007 Solución Enunciado Examen Modelo 2 (Junio) de sobrantes del 2007href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod2_Junio_sol.pdf" style="text-decoration: none; color: blue;"> Solución Enunciado Examen Modelo 3 (Septiembre) de sobrantes del 2007 Solución Enunciado Examen Modelo 4 de sobrantes del 2007 Solución Enunciado Examen Modelo 5 de sobrantes del 2007 Soluciónhref="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod6.pdf" style="text-decoration: none; color: blue;"> Enunciado Examen Modelo 6 de sobrantes del 2007 Solución Año 2005-2006 Enunciado Examen Modelo 1 de sobrantes del 2006 Solución Enunciado Examen Modelo 2 (Septiembre) de sobrantes del 2006align="center"> Solución Enunciado Examen Modelo 3 (Junio) de sobrantes del 2006 Solución Enunciado Examen Modelo 4 de sobrantes del 2006 Solución Enunciado Examen Modelo 5 de sobrantes del 2006 Soluciónhref="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod6.pdf" style="text-decoration: none; color: blue;"> Enunciado Examen Modelo 6 de sobrantes del 2006 Solución Año 2004-2005 Enunciado Examen Modelo 1 de sobrantes del 2005 Soluciónhref="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod2_jun.pdf" style="text-decoration: none; color: blue;"> Enunciado Examen Modelo 2 (Junio) de sobrantes del 2005 Solución Enunciado Examen Modelo 3 de sobrantes del 2005 Solución Enunciado Examen Modelo 4 de sobrantes del 2005 Solución Enunciado ExamenModelo 5 (Septiembre) de sobrantes del 2005 Solución Enunciado Examen Modelo 6 de sobrantes del 2005 Solución";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2012-09-18 09:59:06";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2012-09-18 09:59:06";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:5;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"262";s:5:"alias";s:23:"selectividad-septi-2015";s:7:"summary";s:31560:"<h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE JUNIO Y SEPTIEMBRE 2014</span></h1>
<ul>
<li><strong style="color: #2a2a2a; font-size: 13px;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/09/14 &nbsp;(Miércoles) EN&nbsp;<a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a; font-size: 13px;">&nbsp;</span><strong style="color: #2a2a2a; font-size: 13px;">EXÁMENES QUE HAN SALIDO EN JUNIO Y SEPTIEMBRE 2014 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000; background-color: #e7eaad;" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Biología</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>CTM</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Comentario de Texto, Lengua&nbsp;</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Dibujo Art. II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Dibujo Téc. II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Economía de la Empresa</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Diseño</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Electrotec.</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td style="text-align: center;">
<p><span style="font-size: 10pt;"><strong>Junio<a href="archivos_ies/13_14/selectividad/Biologia_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" width="35" height="35" align="middle" border="0" /></a> </strong><a href="archivos_ies/13_14/selectividad/Biologia_Colision_Junio2014.pdf" target="_blank" style="font-size: inherit;"><strong>Colisión</strong></a> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" width="35" height="35" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/Lengua_Junio2014.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Junio2014.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a><a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Colision_Junio2014.pdf" target="_blank"><span style="color: inherit; font-family: inherit; text-align: left;">Colisión</span></a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Diseño_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
<p style="text-align: center;"><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Colision_Junio2014.pdf" target="_blank">Colisión</a></strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><span style="font-size: 10pt;">&nbsp;<a href="archivos_ies/13_14/selectividad/biologia_sept_2014.pdf" target="_blank" title="Biología."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/ctm_sept_2014.pdf" target="_blank" title="CTM."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/lengua_sept_2014.pdf" target="_blank" title="Lengua.">Sept.<strong style="color: #000000; text-align: center;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/dt_sept_2014.pdf" target="_blank" title="Dibujo Técnico II."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/economia_sept_2014.pdf" target="_blank" title="Economía."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/electrotecnia_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Electrotecnia.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;">
<p><span style="font-size: 12pt;"><strong>Física</strong></span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 12pt;"><strong>Geografía</strong></span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 12pt;"><strong>Griego II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>H. de España</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>H. Filosofía</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Historia del Arte</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Latín II</strong><strong>&nbsp;</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Lengua Extranjera II (Alemán)</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Fisica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Geografia_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/Griego_AyB.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/H_España_Junio2014.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Filosofia_AyB.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a>&nbsp;</strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;</strong><strong><a href="archivos_ies/13_14/selectividad/H_Arte_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a></strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;</strong><strong><a href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a></strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><span style="font-size: 10pt;"><strong>&nbsp;<a href="archivos_ies/13_14/selectividad/fisica_sept_2014.pdf" target="_blank" title="Física."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a></strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/geografia_sept_2014.pdf" target="_blank" title="Geografía."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong>&nbsp;<a href="archivos_ies/13_14/selectividad/griego_sept_2014.pdf" target="_blank" title="Griego."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></a></strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/historia_sept_2014.pdf" target="_blank" title="Historia de España."><strong style="color: #000000; text-align: -webkit-center; background-color: #ffffff;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/filosofia_sept_2014.pdf" target="_blank" style="text-align: -webkit-center; background-color: #ffffff;" title="Filosofía.">Sept<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong>&nbsp;<a href="archivos_ies/13_14/selectividad/h_arte_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Historia del Arte.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a></strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/latin_sept_2014.pdf" target="_blank" title="Latín."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong>&nbsp;</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td style="text-align: center;" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Lengua Extranjera II (Francés)</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Inglés</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Lit. Uni.</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Mat.Apl.a las CCSS II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Mat. II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong style="text-align: left;">&nbsp;Química</strong><strong>&nbsp;</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>&nbsp;</strong><strong style="text-align: center;">Téc. .Expr. Gráf. Plást.(no disponible)</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Tecnología Industrial II</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td style="text-align: center;">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluc.</strong></a> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;">Soluc.</a></strong><span style="color: inherit; font-family: inherit; text-align: left;">&nbsp;</span></span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong><strong>&nbsp;</strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong style="text-align: left;"><span style="color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </span> </strong><strong>&nbsp;</strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </span><strong style="color: inherit; font-family: inherit; font-size: inherit;">&nbsp;</strong> </span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><a href="archivos_ies/13_14/selectividad/frances_sept_2014.pdf" target="_blank" title="Francés."><span style="font-size: 10pt;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></span></a></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/ingles_sept_2014.pdf" target="_blank" style="text-align: -webkit-center; background-color: #ffffff;" title="Inglés.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/lit_uni_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Literatura Universal.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/mat_ccss_ii_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Matemáticas Aplicadas a las Ciencias Sociales II.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/mat_ii_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Matemáticas II.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/quimica_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Química.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/tec_indus_ii_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Tecnología Industrial II.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<ul>
<li>
<h3>Para ver las notas entrar en la siguiente web:</h3>
</li>
</ul>
<h3 style="background-color: #1188ff;"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></h3>
<p>&nbsp;</p>
<ul>
<li>
<h4><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI encuentro de la universidad con los centros.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">Presentación: registro para la selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">Presentación: matrícula de selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank">Selectividad: orientaciones.<br /><br /></a></h4>
</li>
<li>
<h4><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Web de la Universidad para registro y matrícula.<br /><br /></a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Otros datos de interés:&nbsp;</a></h4>
</li>
</ul>
<ol><ol>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Inscripción</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Matriculación</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Precios</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Calendario de las pruebas día a día</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Nota de acceso</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Exámenes de otros años y orientaciones</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Orden de preferencia de carreras</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Procedimiento de matrícula en la UIniversidad</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
</ol></ol>
<p>&nbsp;</p>
<p><span style="color: #2a2a2a;">&nbsp;</span></p>";s:4:"body";s:0:"";s:5:"catid";s:3:"140";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-09-18 20:34:30";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"37";s:8:"ordering";s:1:"0";s:8:"category";s:24:"Novedades de Secretaría";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:27:"262:selectividad-septi-2015";s:7:"catslug";s:27:"140:novedades-de-secretaria";s:6:"author";s:10:"Super User";s:6:"layout";s:7:"article";s:4:"path";s:106:"index.php?option=com_content&view=article&id=262:selectividad-septi-2015&catid=140:novedades-de-secretaria";s:10:"metaauthor";N;s:6:"weight";d:1.0266200000000001;s:7:"link_id";i:1297;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:24:"Novedades de Secretaría";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:24:"Novedades de Secretaría";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=262";s:5:"route";s:106:"index.php?option=com_content&view=article&id=262:selectividad-septi-2015&catid=140:novedades-de-secretaria";s:5:"title";s:39:"Selectividad Septiembre 2014: EXÁMENES";s:11:"description";s:2013:"ZONA DEDICADA A LA SELECTIVIDAD DE JUNIO Y SEPTIEMBRE 2014 LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/09/14 (Miércoles) EN ESTE ENLACE. EXÁMENES QUE HAN SALIDO EN JUNIO Y SEPTIEMBRE 2014 DE LAS DISTINTAS MATERIAS: Biología CTM Comentario de Texto, Lengua Dibujo Art. II Dibujo Téc. II Economía de la Empresa Diseño Electrotec.bgcolor="#ffffff"> Junio Colisión Junio Junio Juniocursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> Colisión Junio Junio Juniotarget="_blank"> Junio Colisión Sept. Sept. Sept.10pt;"> Sept. Sept. Sept. Física Geografía Griego IIbgcolor="#FEFDD6"> H. de España H. Filosofía Historia del Arte Latín II Lengua Extranjera II (Alemán) Junio Junio JunioJunio Junio Juniostyle="text-decoration: underline;"> Junio Junio Sept. Sept.align="center"> Sept. Sept. Sept Sept.title="Latín."> Sept. Lengua Extranjera II (Francés) Inglés Lit. Uni. Mat.Apl.a las CCSS II Mat. II Química Téc. .Expr. Gráf. Plást.(no disponible) Tecnología Industrial IIcolor: #2f310d;"> Junio Junio Junio JunioSoluc. Junio Soluc. Junio JunioJunio Sept. Sept. Sept.style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> Sept. Sept. Sept. Sept.align="middle" border="0" /> Para ver las notas entrar en la siguiente web: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp VI encuentro de la universidad con los centros. Presentación: registro para la selectividad. Presentación: matrícula de selectividad. Selectividad: orientaciones. Web de la Universidad para registro y matrícula. Otros datos de interés: Inscripción Plazos Matriculación Precioshref="index.php?option=com_content&view=article&id=139&Itemid=218" target="_blank"> Calendario de las pruebas día a día Nota de acceso Exámenes de otros años y orientaciones Orden de preferencia de carreras Procedimiento de matrícula en la UIniversidad Plazos";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2014-04-09 18:23:06";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2014-04-09 18:23:06";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:6;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"243";s:5:"alias";s:17:"selectividad-2014";s:7:"summary";s:22559:"<h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE 2014</span></h1>
<ul>
<li><strong style="color: #2a2a2a; font-size: 13px;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/14  (MARTES) POR LA TARDE EN <a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a; font-size: 13px;"> </span><strong style="color: #2a2a2a; font-size: 13px;">EXÁMENES QUE HAN SALIDO EN JUNIO 2014 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #2a2a2a; font-size: 13px; text-decoration: underline;">JUNIO 2014 - EXÁMENES</strong></span></p>
<p><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_criterios_correccion.php" target="_blank"><span style="text-decoration: underline;"><strong style="color: #2a2a2a; font-size: 13px; text-decoration: underline;">Criterios de correción oficiales. (19/06/14)</strong></span></a></p>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000; background-color: #e7eaad;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Biología</strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br />Ciencias de la Tierra y Medioambientales</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Comentario de Texto, Lengua Castellana y Literatura</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Artístico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Técnico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Economía de la Empresa</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Diseño</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Electrotecnia</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><span style="text-decoration: underline;"><a href="archivos_ies/13_14/selectividad/Biologia_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" /></a></span></strong><a href="archivos_ies/13_14/selectividad/Biologia_Colision_Junio2014.pdf" target="_blank" style="font-size: inherit;"><strong><span>Colisión</span></strong></a></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Biologia_AB.pdf" target="_blank"><strong><span>Crit. Correc.</span></strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" /><br /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_CTM.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><br /> <a href="archivos_ies/13_14/selectividad/Lengua_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Lengua.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a><a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Colision_Junio2014.pdf" target="_blank"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Colisión</span></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_D_Artistico.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_D_Tecnico.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Economia.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Diseño_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank">Crit. Correc.</a><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank"></a></strong></p>
</td>
<td align="center">
<p style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></strong><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Colisión</strong></a></p>
<p style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Electrotecnia.pdf" target="_blank"><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Crit. Correc.</strong></a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank"><br /></a>Física</strong></td>
<td style="text-align: center;" align="center"><strong><br />Geografía</strong></td>
<td style="text-align: center;" align="center"><strong><br />Griego II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de España</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de la Filosofía</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia del Arte</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong>Latín II </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong>Alemán</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><a href="archivos_ies/13_14/selectividad/Fisica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/FISICA_2014_Junio.pdf" target="_blank" style="font-size: inherit; font-weight: normal; text-decoration: none; color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluciones<br /></strong></a><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Fisica.pdf" target="_blank">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Geografia_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Geografia.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/Griego_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Griego_II.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/H_España_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_H_España.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Filosofia_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Filosofia.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong><strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/H_Arte_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_H_Arte.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_LatinAB.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td align="center">
<p><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="display: block; margin-left: auto; margin-right: auto;" /></a></strong></p>
<p><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_AlemanAB.pdf" target="_blank"><strong>Crit. Correc.</strong></a><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /></a></strong></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Francés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Inglés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Literatura Universal</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas Aplicadas a las Ciencias Sociales II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Matemáticas II</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong><br /> Química </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong> </strong><strong style="text-align: center;">Técnicas de Exp. Gráf. Plást. (no disponible)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Tecnología Industrial II</strong></td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Frances.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Ingles.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Lit_Universal.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluciones</strong></a></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_MAT_CCSS_IIAB.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;">Crit. Correc.</strong></strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;">Soluciones</a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Matematicas_II2.pdf" target="_blank"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">Crit. Correc.</strong> </span></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></span></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Quimica.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong><strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong style="text-align: left;"><span style="color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></span></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Tec_Exp_Graf_Plastica.pdf" target="_blank"><strong style="text-align: left;"><span style="color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">Crit. Correc.</strong></span></strong><strong> </strong></a></p>
</td>
<td align="center">
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="display: block; margin-left: auto; margin-right: auto;" /></a></span></p>
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_T_IndustrialAB.pdf" target="_blank"><strong style="color: #000000; text-align: center;">Crit. Correc.</strong></a><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a></span><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<ul style="color: #2a2a2a; font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13px; line-height: normal;">
<li>
<h3><span style="text-align: center; color: #2a2a2a;">22 de abril a 4 de junio: registro para las PAU.</span></h3>
</li>
<li><span style="color: #7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;">La matrícula se realizará del 2 al 4 de Junio y del 3 al 5 de septiembre.</span></li>
</ul>
<ul>
<li>
<h3>Para conseguir el PIN (registro para las PAU) entrar en la siguiente web:</h3>
</li>
</ul>
<h3 style="text-align: center; background-color: #1188ff;"><strong><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank"><span style="color: #ffffff;">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</span></a></strong></h3>
<p> </p>
<ul style="font-size: 13px; text-align: justify; background-color: #e7eaad;">
<li>
<h4><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/documentacion/Parametros_AyB_UA_Bachillerato_2014_2015.pdf" target="_blank">PARÁMETROS DE PONDERACIÓN, Consulta estática.</a></h4>
<p><strong><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/g_b_parametros_top.php" target="_blank">CONSULTA DINÁMICA</a>.</strong></p>
</li>
<li>
<h4><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/" target="_blank">DISTRITO ÚNICO ANDALUZ.</a></h4>
</li>
</ul>
<ul>
<li>
<h4><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI encuentro de la universidad con los centros.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">Presentación: registro para la selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">Presentación: matrícula de selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank">Selectividad: orientaciones.<br /><br /></a></h4>
</li>
<li>
<h4><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Web de la Universidad para registro y matrícula.<br /><br /></a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Otros datos de interés: </a></h4>
</li>
</ul>
<ol><ol>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Inscripción</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Matriculación</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Precios</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Calendario de las pruebas día a día</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Nota de acceso</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Exámenes de otros años y orientaciones</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Orden de preferencia de carreras</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Procedimiento de matrícula en la UIniversidad</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
</ol></ol>
<h4> </h4>
<p><span style="color: #2a2a2a;"> </span></p>";s:4:"body";s:0:"";s:5:"catid";s:3:"108";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-06-20 11:06:25";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":68:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"47";s:8:"ordering";s:1:"1";s:8:"category";s:12:"BACHILLERATO";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:21:"243:selectividad-2014";s:7:"catslug";s:16:"108:bachillerato";s:6:"author";s:10:"Super User";s:6:"layout";s:7:"article";s:4:"path";s:100:"index.php?option=com_content&view=article&id=243:selectividad-2014&catid=108:bachillerato&Itemid=147";s:10:"metaauthor";N;s:6:"weight";d:1.0266200000000001;s:7:"link_id";i:1023;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"BACHILLERATO";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"BACHILLERATO";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=243";s:5:"route";s:100:"index.php?option=com_content&view=article&id=243:selectividad-2014&catid=108:bachillerato&Itemid=147";s:5:"title";s:17:"Selectividad 2014";s:11:"description";s:2405:"ZONA DEDICADA A LA SELECTIVIDAD DE 2014 LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/14 (MARTES) POR LA TARDE EN ESTE ENLACE. EXÁMENES QUE HAN SALIDO EN JUNIO 2014 DE LAS DISTINTAS MATERIAS: JUNIO 2014 - EXÁMENES Criterios de correción oficiales. (19/06/14) Biología Ciencias de la Tierra y Medioambientales Comentario de Texto, Lengua Castellana y Literatura Dibujo Artístico II Dibujo Técnico IIbgcolor="#FEFDD6"> Economía de la Empresa Diseño Electrotecnia Colisión Crit. Correc. Crit. Correc. Crit. Correc.target="_blank"> Colisión Crit. Correc. Crit. Correc. Crit. Correc. Crit. Correc.target="_blank"> Colisión Crit. Correc. Física Geografía Griego II Historia de España Historia de la Filosofía Historia del Arte Latín II Alemánhref="archivos_ies/13_14/selectividad/FISICA_2014_Junio.pdf" target="_blank" style="font-size: inherit; font-weight: normal; text-decoration: none; color: #2f310d;"> Soluciones Crit. Correc. Crit. Correc. Crit. Correc. Crit. Correc./> Crit. Correc. Crit. Correc. Crit. Correc. Crit. Correc.bgcolor="#ffffff"> Lengua Extranjera II (Francés) Lengua Extranjera II (Inglés) Literatura Universal Matemáticas Aplicadas a las Ciencias Sociales II Matemáticas II Química Técnicas de Exp. Gráf. Plást. (no disponible) Tecnología Industrial II Crit. Correc.none; color: #2f310d;"> Crit. Correc. Crit. Correc. Soluciones Crit. Correc.href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;"> Soluciones Crit. Correc. Crit. Correc. Crit. Correc. Crit.Correc. 22 de abril a 4 de junio: registro para las PAU. La matrícula se realizará del 2 al 4 de Junio y del 3 al 5 de septiembre. Para conseguir el PIN (registro para las PAU) entrar en la siguiente web: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp PARÁMETROS DE PONDERACIÓN, Consulta estática. CONSULTA DINÁMICA . DISTRITO ÚNICO ANDALUZ. VI encuentro de la universidad con los centros. Presentación: registro para laselectividad. Presentación: matrícula de selectividad. Selectividad: orientaciones. Web de la Universidad para registro y matrícula. Otros datos de interés: Inscripción Plazos Matriculación Precios Calendario de las pruebas día a día Nota de acceso Exámenes de otros años y orientaciones Orden de preferencia de carreras Procedimiento de matrícula en laUIniversidad Plazos";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2014-04-09 18:23:06";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2014-04-09 18:23:06";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:7;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"251";s:5:"alias";s:23:"selectividad-junio-2014";s:7:"summary";s:17049:"<h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE 2014</span></h1>
<ul>
<li><strong style="color: #2a2a2a; font-size: 13px;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/14  (MARTES) POR LA TARDE EN <a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a; font-size: 13px;"> </span><strong style="color: #2a2a2a; font-size: 13px;">EXÁMENES QUE HAN SALIDO EN JUNIO 2014 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000; background-color: #e7eaad;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong>Biología</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Ciencias de la Tierra y Medioambientales</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Comentario de Texto, Lengua Castellana y Literatura</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Dibujo Artístico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Dibujo Técnico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Economía de la Empresa</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Diseño</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Electrotecnia</strong></td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><span style="text-decoration: underline;"><a href="archivos_ies/13_14/selectividad/Biologia_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></span></strong><a href="archivos_ies/13_14/selectividad/Biologia_Colision_Junio2014.pdf" target="_blank" style="font-size: inherit;"><strong><span>Colisión</span></strong></a></p>
</td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong> <a href="archivos_ies/13_14/selectividad/Lengua_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a><a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Colision_Junio2014.pdf" target="_blank"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Colisión</span></a></strong></p>
</td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Diseño_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a></strong></p>
<p style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Colision_Junio2014.pdf" target="_blank">Colisión</a><a href="archivos_ies/13_14/selectividad/Electrotecnia_Junio2014.pdf" target="_blank"><br /></a></strong></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;"><strong>Física</strong></td>
<td style="text-align: center;" align="center"><strong>Geografía</strong></td>
<td style="text-align: center;" align="center"><strong>Griego II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de España</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de la Filosofía</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia del Arte</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Latín II</strong><strong> </strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Alemán)</strong></td>
</tr>
<tr>
<td><strong><a href="archivos_ies/13_14/selectividad/Fisica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center"><strong><a href="archivos_ies/13_14/selectividad/Geografia_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a></strong></td>
<td align="center"><strong> <a href="archivos_ies/13_14/selectividad/Griego_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center"><strong> <a href="archivos_ies/13_14/selectividad/H_España_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center"><strong><a href="archivos_ies/13_14/selectividad/Filosofia_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a> </strong></td>
<td align="center"><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/H_Arte_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td align="center"><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></td>
<td align="center"><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a></strong></td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Francés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Inglés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Literatura Universal</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas Aplicadas a las Ciencias Sociales II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong style="text-align: left;"> Química</strong><strong> </strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong> </strong><strong style="text-align: center;">Técnicas de Expresión Gráfico Plásticas (no disponible)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Tecnología Industrial II</strong></td>
</tr>
<tr>
<td style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong></td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluciones</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;">Soluciones</a></strong><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"> </span></p>
</td>
<td style="text-align: center;" align="center"><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></span></a></strong><strong> </strong></td>
<td style="text-align: center;" align="center"><strong style="text-align: left;"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></span></strong><strong> </strong></td>
<td align="center"><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" /></a></span><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></td>
</tr>
</tbody>
</table>
<p> </p>
<ul style="color: #2a2a2a; font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13px; line-height: normal;">
<li>
<h3><span style="text-align: center; color: #2a2a2a;">22 de abril a 4 de junio: registro para las PAU.</span></h3>
</li>
<li><span style="color: #7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;">La matrícula se realizará del 2 al 4 de Junio y del 3 al 5 de septiembre.</span></li>
</ul>
<ul>
<li>
<h3>Para conseguir el PIN (registro para las PAU) entrar en la siguiente web:</h3>
</li>
</ul>
<h6 style="background-color: #1188ff;"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></h6>
<p> </p>
<ul>
<li><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI encuentro de la universidad con los centros.<br /><br /></a></li>
<li><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">Presentación: registro para la selectividad.<br /><br /></a></li>
<li><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">Presentación: matrícula de selectividad.<br /><br /></a></li>
<li><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank">Selectividad: orientaciones.<br /><br /></a></li>
<li><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Web de la Universidad para registro y matrícula.<br /><br /></a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Otros datos de interés: </a></li>
</ul>
<ol><ol>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Inscripción</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Matriculación</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Precios</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Calendario de las pruebas día a día</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Nota de acceso</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Exámenes de otros años y orientaciones</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Orden de preferencia de carreras</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Procedimiento de matrícula en la UIniversidad</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></li>
</ol></ol>
<p> </p>
<p><span style="color: #2a2a2a;"> </span></p>";s:4:"body";s:0:"";s:5:"catid";s:3:"108";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-06-15 11:09:26";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":68:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:1:"2";s:8:"ordering";s:1:"0";s:8:"category";s:12:"BACHILLERATO";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:27:"251:selectividad-junio-2014";s:7:"catslug";s:16:"108:bachillerato";s:6:"author";s:10:"Super User";s:6:"layout";s:7:"article";s:4:"path";s:106:"index.php?option=com_content&view=article&id=251:selectividad-junio-2014&catid=108:bachillerato&Itemid=147";s:10:"metaauthor";N;s:6:"weight";d:1.0266200000000001;s:7:"link_id";i:993;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"BACHILLERATO";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"BACHILLERATO";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=251";s:5:"route";s:106:"index.php?option=com_content&view=article&id=251:selectividad-junio-2014&catid=108:bachillerato&Itemid=147";s:5:"title";s:23:"Selectividad Junio 2014";s:11:"description";s:2010:"ZONA DEDICADA A LA SELECTIVIDAD DE 2014 LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/14 (MARTES) POR LA TARDE EN ESTE ENLACE. EXÁMENES QUE HAN SALIDO EN JUNIO 2014 DE LAS DISTINTAS MATERIAS: Biología Ciencias de la Tierra y Medioambientales Comentario de Texto, Lengua Castellana y Literatura Dibujo Artístico II Dibujo Técnico II Economía de la Empresa Diseño Electrotecniaborder-color: #bcbcbc; cursor: se-resize !important;" /> Colisión Colisiónse-resize !important;" /> Colisión Física Geografía Griego II Historia de España Historia de la Filosofía Historia del Arte Latín II Lengua Extranjera II(Alemán)href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"> Lengua Extranjera II (Francés) Lengua Extranjera II (Inglés) Literatura Universal Matemáticas Aplicadas a las Ciencias Sociales II Matemáticas II Química Técnicas de Expresión Gráfico Plásticas (no disponible)bgcolor="#FEFDD6"> Tecnología Industrial II Solucionessrc="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /> Soluciones 22 de abril a 4 de junio: registro para las PAU.#7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;"> La matrícula se realizará del 2 al 4 de Junio y del 3 al 5 de septiembre. Para conseguir el PIN (registro para las PAU) entrar en la siguiente web: https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp VI encuentro de la universidad con los centros. Presentación: registro para la selectividad. Presentación: matrícula de selectividad. Selectividad: orientaciones. Web de la Universidad para registro y matrícula. Otros datos de interés: Inscripción Plazos Matriculación Precios Calendario de laspruebas día a día Nota de acceso Exámenes de otros años y orientaciones Orden de preferencia de carreras Procedimiento de matrícula en la UIniversidad Plazos";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2014-04-09 18:23:06";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2014-04-09 18:23:06";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:8;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:27:{s:2:"id";s:3:"211";s:5:"alias";s:16:"prueba-de-acceso";s:7:"summary";s:13593:"<h2 class="contentheading">Prueba de Acceso a Ciclos Formativos</h2>
<h3 class="plg_fa_karmany"><span style="color: #003366;"><strong>Información General</strong></span></h3>
<div id="articlepxfontsize1">
<div style="text-align: justify;"><strong>LUGAR Y FECHAS DE INSCRIPCIÓN</strong></div>
<div style="text-align: justify;">Cada año, y con antelación suficiente, la Consejería de Educación determinará los Centros en los que se realizarán dichas Pruebas. Las fechas de inscripción serán las siguientes:
<ul class="list-icon icon-calendar">
<li>Para la Convocatoria Ordinaria: La Primera Quincena de Mayo.</li>
<li>Para la Convocatoria Extraordinaria: La Segunda Quincena de Julio.</li>
</ul>
<strong>LUGAR Y FECHAS DE REALIZACIÓN</strong>
<div style="text-align: justify;">Las Pruebas se realizarán en el Centro en el que se presentó la solicitud. Las fechas de realización son las siguientes:
<ul class="list-icon icon-calendar">
<li>Convocatoria Ordinaria: el 5 de Junio de cada año (si fuera sábado o festivo, el día hábil siguiente).</li>
<li>Convocatoria Extraordinaria: el 7 de Septiembre de cada año (si este fuera domingo sería el 5 de Septiembre y si fuera sábado o festivo, el siguiente día hábil).</li>
</ul>
</div>
<div style="text-align: justify;"><strong>EFECTOS DE HABER SUPERADO LA PRUEBA </strong></div>
<div style="text-align: justify;"><ol>
<li>La superación de la Prueba de Acceso a la Formación Profesional Específica faculta a las personas que no poseen requisitos académicos para cursar las enseñanzas correspondientes a los Ciclos Formativos de F.P.</li>
<li>La superación de la citada Prueba no supone la posesión de ninguna titulación académica ni tampoco equivale a haber superado alguna área de la ESO o alguna materia de Bachillerato.</li>
<li>La superación de la Prueba no garantiza ni proporciona el derecho a una puesto escolar. Para ser admitidos/as en un Ciclo Formativo, es requisito indispensable presentar solicitud de admisión en el Centro donde se desee cursar estudios, del 1 al 25 de junio o del 1 al 10 de septiembre de cada año, y una vez superada la prueba.</li>
<li>Una vez superada la Prueba, sus efectos tendrán validez en todo el territorio español.</li>
</ol></div>
<div style="text-align: justify;"><strong>RECLAMACIONES </strong></div>
<ul class="list-arrow arrow-blue">
<li><strong>En Primera Instancia</strong><br />Los/as interesados/as o su padres, si son menores de edad, si no estuvieran conformes con la Calificación obtenida en la Prueba, podrán presentar ante la Comisión de valoración la correspondiente reclamación, en el plazo de 2 días hábiles desde la publicación de las actas. En el plazo máximo de 2 días hábiles, la Comisión deberá resolver todas las reclamaciones presentadas.</li>
<li><strong>En Segunda Instancia</strong><br />En caso de que los/as interesados/as o su padres, si son menores de edad, no estuvieran conformes con la resolución de la Comisión, podrán presentar recurso de alzada ante la persona titular de la Delegación Provincial de Educación en el plazo de un mes, de conformidad con lo establecido en los artículos 114 y 115 de la Ley 30/1992, de 26 de noviembre.</li>
</ul>
</div>
<h3 class="text-tip"><span style="color: #800000;"><strong>Pruebas de Acceso de Grado Medio</strong></span></h3>
<div style="text-align: justify;"><strong>NORMATIVA</strong></div>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/boja/2008/90/d2.pdf" target="_blank">Orden de 23 de Abril de 2008 (BOJA nº 90 de 07-05-2008)</a></li>
</ul>
<div style="text-align: justify;"><strong>DOCUMENTOS </strong></div>
<ul class="list-icon icon-download">
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1640-requisitos-y-estructura-grado-medio" target="_blank">Requisitos y estructura</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1644-contenidos-y-criterios-de-evaluacion-grado-medio" target="_blank">Temario de contenidos (Anexo III)</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1642-exenciones-grado-medio" target="_blank">Exenciones de materias ESO (Anexo IV)</a></li>
</ul>
<div style="text-align: justify;"><br /><strong>SOLICITANTES</strong></div>
<div style="text-align: justify;">Las personas que deseen realizar la Prueba de Acceso a un Ciclo Formativo de Grado Medio, deben tener <strong>cumplidos 17 años</strong>, o cumplirlos en el año natural de celebración de la Prueba. Para ello deberán presentar la Solicitud de Inscripción según el Modelo Anexo I, en el Centro en el que se pretenda realizar la Prueba.</div>
<div style="text-align: justify;"><br /><strong>ESTRUCTURA Y CONTENIDOS DE LA PRUEBA</strong></div>
<div style="text-align: justify;">Las Pruebas se estructurarán en tres partes:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-alpha;">
<li>Comunicación</li>
<li>Social</li>
<li>Científico-tecnológica</li>
</ol></div>
<div style="text-align: justify;">Los contenidos aparecen recogidos en el Anexo III.</div>
<div style="text-align: justify;">Para la homogeneizar criterios, la Dirección General facilitará a la Comisión de las Pruebas de Acceso la Prueba ya elaborada, así como los criterios de corrección.</div>
<div style="text-align: justify;"><br /><strong>EXENCIONES DE REALIZACIÓN</strong></div>
<div style="text-align: justify;">Estarán <strong>exentas de realizar la totalidad de la Prueba de Acceso </strong>aquellas personas que reúnan alguno de los siguientes requisitos:<ol style="list-style-type: lower-roman;">
<li>Tener superada la Prueba de Acceso a la Universidad para Mayores de 25 años.</li>
<li>Tener superada la Prueba de Acceso a un Grado Superior.</li>
</ol></div>
<h3 class="text-comment"><span style="color: #9b2626;"><strong>Pruebas de Acceso de Grado Superior</strong></span></h3>
<strong>NORMATIVA</strong>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/boja/2008/90/d2.pdf" target="_blank">Orden de 23 de Abril de 2008 (BOJA nº 90 de 07-05-2008)</a></li>
</ul>
<strong>DOCUMENTOS DESCARGABLES</strong>
<ul class="list-icon icon-download">
<li><a href="archivos_ies/anexo_ii_solicitud_gs_rellenable.pdf" target="_blank">Inscripción Prueba de Acceso (anexo II)</a></li>
<li><a href="archivos_ies/temario-parte-comun-anexo-v.pdf" target="_blank">Temario parte común (anexo V)</a></li>
<li><a href="archivos_ies/GS%20ANEXO%20VI.pdf" target="_blank">Opciones (anexo VI)</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1645-contenidos-y-criterios-de-evaluacion-grado-superior" target="_blank"><strong>CONTENIDOS Y CRITERIOS DE EVALUACIÓN</strong></a></li>
<li><a href="archivos_ies/EXENCIONES%20GS%20completo%20BUENO.pdf" target="_blank">Exención de materias de Bachillerato (anexo VIII)</a></li>
</ul>
<strong>SOLICITANTES</strong>
<div style="text-align: justify;">Las personas que deseen realizar la Prueba de Acceso a un Ciclo Formativo de Grado Superior, deberán cumplir alguno de los siguientes requisitos:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li>Tener<strong> cumplidos 19 años</strong>, o cumplirlos a lo largo del año natural de celebración de la Prueba.</li>
<li>Tener <strong>cumplidos 18 años</strong>, o cumplirlos a lo largo del año natural de celebración de la Prueba, y estar en posesión de un Título de Grado Medio relacionado con aquel de Grado Superior al que se desea acceder.</li>
</ol></div>
<div style="text-align: justify;">Para ello deberán presentar la Solicitud de Inscripción según el Modelo Anexo II, en el Centro en el que se pretenda realizar la Prueba.<br /><br /></div>
<div style="text-align: justify;"><strong>ESTRUCTURA Y CONTENIDOS DE LA PRUEBA</strong></div>
<div style="text-align: justify;">La Prueba contará de dos partes:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-alpha;">
<li>Parte Común.</li>
<li>Parte Específica.</li>
</ol></div>
<div style="text-align: justify;">a)<strong> La Parte Común</strong>, constará de 3 ejercicios:</div>
<div style="text-align: justify;"><ol style="list-style-type: upper-roman;">
<li>Lengua Española</li>
<li>Matemáticas</li>
<li>Lengua Extranjera</li>
</ol></div>
<div style="text-align: justify;">Esta parte se considerará superada cuando habiendo obtenido al menos 3 puntos en cada ejercicio, la media aritmética sea igual o superior a 5 puntos. Los contenidos de esta parte aparecen recogidos en el Anexo V.</div>
<div style="text-align: justify;"><br />b) En la <strong>Parte Específica</strong>, se ofertarán 3 materias distintas, según el Ciclo al que se pretende acceder. Estas materias vienen recogidas en el Anexo VI, debiendo el/la alumno/a elegir 2.</div>
<div style="text-align: justify;"><br />Por ejemplo, para Administración y Finanzas (Opción A), las materias ofertadas son:</div>
<div style="text-align: justify;"><ol style="list-style-type: upper-roman;">
<li>Economía de la Empresa</li>
<li>Geografía</li>
<li>Segunda Lengua Extranjera</li>
</ol></div>
<div style="text-align: justify;">Los contenidos de esta parte aparecen recogidos en el Anexo VII.</div>
<div style="text-align: justify;">Esta parte se considerará superada cuando habiendo obtenido al menos 3 puntos en cada ejercicio, la media aritmética sea igual o superior a 5 puntos.</div>
<div style="text-align: justify;">Para la homogeneizar criterios, la Dirección General facilitará a la Comisión de las Pruebas de Acceso la Prueba ya elaborada, así como los criterios de corrección.</div>
<div style="text-align: justify;"><br /><strong>EXENCIONES DE REALIZACIÓN</strong></div>
<div style="text-align: justify;"><strong>Exención Total</strong>: Estarán exentas de realizar la totalidad de la Prueba de Acceso aquellas personas que reúnan el siguiente requisito:</div>
<div style="text-align: justify;">
<ul style="list-style-type: disc;">
<li>Tener superada la Prueba de Acceso a la Universidad para Mayores de 25 años.</li>
</ul>
</div>
<div style="text-align: justify;"><strong>Exención Parcial</strong>: Estarán exentas de realizar alguna de las partes de la Prueba de Acceso aquellas personas que reúnan alguno de los siguientes requisitos:</div>
<div style="text-align: justify;"><br />DE LA PARTE ESPECÍFICA</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li>Estar en posesión del Título de Técnico relacionado con aquel Ciclo al que se pretende acceder.</li>
<li>Estar en posesión de un certificado de profesionalidad de alguna de las familias profesionales incluidas en la opción por la que se presenta, de un nivel competencial 2 ó superior.</li>
<li>Acreditar una experiencia laboral de al menos un año con jornada completa en el campo profesional correspondiente a la familia de la opción por la que se presente, que se deberá justificar de la siguiente forma:</li>
</ol></div>
<div style="text-align: justify; margin-left: 30px;">
<ul style="list-style-type: circle;">
<li>Trabajadores por cuenta ajena: Mediante Certificación de la Tesorería General de la Seguridad Social.</li>
<li>Trabajadores pro cuenta propia: Mediante Certificación del periodo de cotización en régimen de autónomos.</li>
<li>Tener aprobadas las materias de Bachillerato de las que conste la parte específica.</li>
</ul>
</div>
<div style="text-align: justify;">DE LA PARTE COMÚN</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li>Tener aprobadas las materias de Bachillerato que constituyen la parte Común de la Prueba.</li>
<li>Tener superada la Prueba de Acceso a otro Ciclo Formativo de Grado Superior.</li>
</ol></div>
<div style="text-align: justify;"><strong>Nota:</strong> Las exenciones de los apartados anteriores podrán ser acumulables.<br /><br /></div>
<h3 class="text-alert"><span style="color: #800000;"><strong>Evaluación y Calificación de las pruebas</strong></span></h3>
La calificación de cada una de las partes de la Prueba será numérica, entre 0 y 10. <br />
<ul style="list-style-type: circle;">
<li>La nota final de la Prueba se calculará siempre que se obtenga al menos una puntuación de 4 en cada una de las partes, y será la media aritmética de éstas, expresada con 2 decimales.</li>
<li>Para aquellos/as que hayan realizado y superado el Curso de Preparación de la Prueba de Acceso, a la nota final obtenida en la Prueba de Acceso se le sumará el resultado de multiplicar la nota obtenida en el Curso de Preparación multiplicada por el coeficiente 0,15. </li>
<li>Se considera superada la Prueba de Acceso cuando la nota final sea igual o superior a 5 puntos, siendo la calificación máxima de 10 puntos.</li>
<li>A efectos de cálculo de la nota final de la Prueba, no se tendrán en cuenta las partes de la Prueba exentas, haciéndolo constar así en las actas de evaluación mediante la expresión EX en la casilla correspondiente.</li>
<li>El acta de la convocatoria ordinaria se deberá exponer en el Tablón de Anuncios a partir del 2º día hábil, contado desde el de celebración de la Prueba.</li>
<li>El acta de la convocatoria extraordinaria se deberá exponer en el Tablón de Anuncios el día siguiente de celebración de la Prueba.</li>
</ul>
</div>";s:4:"body";s:0:"";s:5:"catid";s:3:"242";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2013-10-01 17:13:22";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":70:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"0";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"0";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";s:12:"show_section";i:0;s:12:"link_section";i:0;}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:1:"9";s:8:"ordering";s:1:"2";s:8:"category";s:11:"Secretaría";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:20:"211:prueba-de-acceso";s:7:"catslug";s:14:"242:secretaria";s:6:"author";s:10:"Super User";s:4:"mime";N;s:6:"layout";s:7:"article";s:4:"path";s:97:"index.php?option=com_content&view=article&id=211:prueba-de-acceso&catid=242:secretaria&Itemid=617";s:10:"metaauthor";N;s:6:"weight";d:1.0266200000000001;s:7:"link_id";i:713;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:11:"Secretaría";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:11:"Secretaría";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=211";s:5:"route";s:97:"index.php?option=com_content&view=article&id=211:prueba-de-acceso&catid=242:secretaria&Itemid=617";s:5:"title";s:16:"Prueba de acceso";s:11:"description";s:8585:"Prueba de Acceso a Ciclos Formativos Información General LUGAR Y FECHAS DE INSCRIPCIÓN Cada año, y con antelación suficiente, la Consejería de Educación determinará los Centros en los que se realizarán dichas Pruebas. Las fechas de inscripción serán las siguientes: Para la Convocatoria Ordinaria: La Primera Quincena de Mayo. Para la Convocatoria Extraordinaria: La Segunda Quincena de Julio. LUGAR Y FECHAS DE REALIZACIÓN Las Pruebas se realizarán en el Centro en el que se presentó la solicitud. Las fechas de realización son las siguientes: Convocatoria Ordinaria: el 5 de Junio de cada año (si fuera sábado o festivo, el día hábil siguiente). Convocatoria Extraordinaria: el 7 de Septiembre de cada año (si este fuera domingo sería el 5 de Septiembre y si fuera sábado o festivo, el siguiente día hábil). EFECTOS DE HABER SUPERADO LA PRUEBA La superación de la Prueba de Acceso a la Formación Profesional Específica faculta a las personas que no poseen requisitos académicos para cursar las enseñanzas correspondientes a los Ciclos Formativos de F.P. La superación de la citada Prueba no supone la posesión de ninguna titulación académica ni tampoco equivale a haber superado alguna área de la ESO o alguna materia de Bachillerato. La superación de la Prueba no garantiza ni proporciona el derecho a una puesto escolar. Para ser admitidos/as en un Ciclo Formativo, es requisito indispensable presentar solicitud de admisión en el Centro donde sedesee cursar estudios, del 1 al 25 de junio o del 1 al 10 de septiembre de cada año, y una vez superada la prueba. Una vez superada la Prueba, sus efectos tendrán validez en todo el territorio español. RECLAMACIONES En Primera Instancia Los/as interesados/as o su padres, si son menores de edad, si no estuvieran conformes con la Calificación obtenida en la Prueba, podrán presentar ante la Comisión de valoración la correspondiente reclamación, en el plazo de 2 días hábiles desde la publicación de las actas. En el plazo máximo de 2 días hábiles, la Comisión deberá resolver todas las reclamaciones presentadas. En Segunda Instancia En caso de que los/as interesados/as o su padres, si son menores de edad, no estuvieran conformes con la resolución de la Comisión, podrán presentar recurso de alzada ante la persona titular de la Delegación Provincial de Educación en el plazo de un mes, de conformidad con lo establecido en los artículos 114 y 115 de la Ley 30/1992, de 26 de noviembre. Pruebas de Acceso de Grado Medio NORMATIVA Orden de 23 de Abril de 2008 (BOJA nº 90 de 07-05-2008) DOCUMENTOS Requisitos y estructuratarget="_blank"> Temario de contenidos (Anexo III) Exenciones de materias ESO (Anexo IV) SOLICITANTES Las personas que deseen realizar la Prueba de Acceso a un Ciclo Formativo de Grado Medio, deben tener cumplidos 17 años , o cumplirlos en el año natural de celebración de la Prueba. Para ello deberán presentar la Solicitud de Inscripción según el Modelo Anexo I, en el Centro en el que se pretenda realizar la Prueba. ESTRUCTURA Y CONTENIDOS DE LA PRUEBA Las Pruebas se estructurarán en tres partes: Comunicación Social Científico-tecnológica Los contenidos aparecen recogidos en el Anexo III. Para la homogeneizar criterios, la Dirección General facilitará a la Comisión de las Pruebas de Acceso la Prueba ya elaborada, así como los criterios de corrección. EXENCIONES DE REALIZACIÓN Estarán exentas de realizar la totalidad de la Prueba de Acceso  aquellas personas que reúnan alguno de los siguientes requisitos: Tener superada la Prueba de Acceso a la Universidad para Mayores de 25 años. Tener superada la Prueba de Acceso a un Grado Superior. Pruebas de Acceso de Grado Superior NORMATIVAhref="http://www.juntadeandalucia.es/boja/2008/90/d2.pdf" target="_blank"> Orden de 23 de Abril de 2008 (BOJA nº 90 de 07-05-2008) DOCUMENTOS DESCARGABLES Inscripción Prueba de Acceso (anexo II) Temario parte común (anexo V) Opciones (anexo VI) CONTENIDOS Y CRITERIOS DE EVALUACIÓN Exención de materias de Bachillerato (anexo VIII) SOLICITANTES Las personas que deseen realizar la Prueba de Acceso a un Ciclo Formativo de Grado Superior, deberán cumplir alguno de los siguientes requisitos: Tener cumplidos 19 años , o cumplirlos a lo largo del año natural de celebración de la Prueba. Tener cumplidos 18 años , o cumplirlos a lo largo del año natural de celebración de la Prueba, y estar en posesión de un Título de Grado Medio relacionado con aquel de Grado Superior al que se desea acceder. Para ello deberán presentar la Solicitud de Inscripción según el Modelo Anexo II, en el Centro en el que se pretenda realizar la Prueba. ESTRUCTURA Y CONTENIDOS DE LA PRUEBA La Prueba contará de dos partes:lower-alpha;"> Parte Común. Parte Específica. a) La Parte Común , constará de 3 ejercicios: Lengua Española Matemáticas Lengua Extranjera Esta parte se considerará superada cuando habiendo obtenido al menos 3 puntos en cada ejercicio, la media aritmética sea igual o superior a 5 puntos. Los contenidos de esta parte aparecen recogidos en el Anexo V. b) En la Parte Específica , se ofertarán 3 materias distintas, según el Ciclo al que se pretende acceder. Estas materias vienen recogidas en el Anexo VI, debiendo el/la alumno/a elegir 2. Por ejemplo, para Administración y Finanzas (Opción A), las materias ofertadas son: Economía de la Empresa Geografía Segunda Lengua Extranjera Los contenidos de esta parte aparecen recogidos en el Anexo VII. Esta parte se considerará superada cuando habiendo obtenido al menos 3 puntos en cada ejercicio, la media aritmética sea igual o superior a 5 puntos. Para la homogeneizar criterios, la Dirección General facilitará a la Comisión de las Pruebas de Acceso la Prueba ya elaborada, así como los criterios de corrección. EXENCIONES DE REALIZACIÓN Exención Total : Estarán exentas de realizar la totalidad de la Prueba de Acceso aquellas personas que reúnan el siguiente requisito:disc;"> Tener superada la Prueba de Acceso a la Universidad para Mayores de 25 años. Exención Parcial : Estarán exentas de realizar alguna de las partes de la Prueba de Acceso aquellas personas que reúnan alguno de los siguientes requisitos: DE LA PARTE ESPECÍFICA Estar en posesión del Título de Técnico relacionado con aquel Ciclo al que se pretende acceder. Estar en posesión de un certificado de profesionalidad de alguna de las familias profesionales incluidas en la opción por la que se presenta, de un nivel competencial 2 ó superior. Acreditar una experiencia laboral de al menos un año con jornada completa en el campo profesional correspondiente a la familia de la opción por la que se presente, que se deberá justificar de la siguiente forma: Trabajadores por cuenta ajena: Mediante Certificación de la Tesorería General de la Seguridad Social. Trabajadores pro cuenta propia: Mediante Certificación del periodo de cotización en régimen de autónomos. Tener aprobadas las materias de Bachillerato de las que conste la parte específica. DE LA PARTE COMÚN Tener aprobadas las materias de Bachillerato que constituyen la parte Común de la Prueba. Tener superada la Prueba de Acceso a otro Ciclo Formativo de Grado Superior. Nota: Las exenciones de los apartados anteriores podrán ser acumulables. Evaluación y Calificación de laspruebas La calificación de cada una de las partes de la Prueba será numérica, entre 0 y 10. La nota final de la Prueba se calculará siempre que se obtenga al menos una puntuación de 4 en cada una de las partes, y será la media aritmética de éstas, expresada con 2 decimales. Para aquellos/as que hayan realizado y superado el Curso de Preparación de la Prueba de Acceso, a la nota final obtenida en la Prueba de Acceso se le sumará el resultado de multiplicar la nota obtenida en el Curso de Preparación multiplicada por el coeficiente 0,15.  Se considera superada la Prueba de Acceso cuando la nota final sea igual o superior a 5 puntos, siendo la calificación máxima de 10 puntos. A efectos de cálculo de la nota final de la Prueba, no se tendrán en cuenta las partes de la Prueba exentas, haciéndolo constar así en las actas de evaluación mediante la expresión EX en la casilla correspondiente. El acta de la convocatoria ordinaria se deberá exponer en el Tablón de Anuncios a partir del 2º día hábil, contado desde el de celebración de la Prueba. El acta de la convocatoria extraordinaria se deberá exponer en el Tablón de Anuncios el día siguiente de celebración de la Prueba.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2013-02-21 20:20:36";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2013-02-21 20:20:36";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:9;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:21:{s:2:"id";s:3:"104";s:5:"alias";s:16:"ultima-normativa";s:7:"summary";s:60028:"<table valign="center" align="center" cellpadding="1" cellspacing="0" border="0" style="border-collapse: collapse; width: auto; background-color: #ffffff; margin: 1px;">
<tbody>
<tr bgcolor="#efbc38" style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;">
<td align="center" style="border-top-color: #8799f9; border-right-color: #8799f9; border-bottom-color: initial; border-left-color: #8799f9; vertical-align: top; text-align: center; border-width: 1px; border-style: solid; padding: 2px;">
<p style="color: #333333;"><span style="color: #ff0000;"><strong><span style="font-size: 12pt;">ÚLTIMA NORMATIVA (11/10/2011)</span></strong></span></p>
</td>
</tr>
<tr class="textogris" style="border-color: #8799f9;">
<td class="textogris" style="vertical-align: top; text-align: center; padding: 2px; border: 1px solid #8799f9;">
<table cellspacing="3" border="1" style="border-collapse: collapse; width: 528px; height: 1px; margin: 1px;">
<tbody>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-11</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Atención a la Diversidad</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc5oct2011ProgramaProfundiza.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 5 de octubre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-11</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Organización y Funcionamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc5oct2011ProgramaProfundiza.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 5 de octubre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-06</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación Compensatoria</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/circulares/Circular29sept2011ProgramaAcompanamiento.pdf" style="text-decoration: none; color: #005fa9;">CIRCULAR de 29 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-06</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Acompañamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/circulares/Circular29sept2011ProgramaAcompanamiento.pdf" style="text-decoration: none; color: #005fa9;">CIRCULAR de 29 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-05</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación Compensatoria</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc30sept2011PlanAcompanamientoExtranjero.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 30 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-05</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Adscripción / Integración / Especialidades</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden23sept2011FormacionEquivalenteMaster.pdf" style="text-decoration: none; color: #005fa9;">ORDEN EDU/2645/2011, de 23 de septiembre</a>, por la que se establece la formación equivalente a la formación pedagógica y didáctica exigida para aquellas personas que estando en posesión de una titulación declarada equivalente a efectos de docencia no pueden realizar los estudios de máster (BOE 05-10-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-05</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Acompañamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc30sept2011PlanAcompanamientoExtranjero.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 30 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012.</td>
</tr>
</tbody>
</table>
<table cellspacing="3" border="1" style="border-collapse: collapse; width: 528px; margin: 1px;">
<tbody>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;">2011-09-30</p>
</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;"><span style="font-size: 11px;">Acceso a la universidad</span></p>
</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resolucion29abril2010AnexoIactualizacion12julio2011.pdf" style="text-decoration: none; color: #005fa9;">Anexo I de la Resolución de 29 de abril de 2010</a>, de la Secretaría de Estado de Educación y Formación Profesional, por la que se establecen las instrucciones para el cálculo de la nota media que debe figurar en las credenciales de convalidación y homologación de estudios y títulos extranjeros con el bachiller españolen el apartado Estudios y Títulos no Universitarios (actualización de 12-07-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación de Personas Adultas</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc23sept2011PruebasLibresBach2011.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 23 de septiembre de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de 2011.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Asistencia Jurídica / Responsabilidad / Abstención</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resolucion16sept2011AsistenciaJuridica.pdf" style="color: #005fa9;">RESOLUCIÓN de 16 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se dispone la publicación del Pacto de Mesa Sectorial en materia de prestación de asistencia jurídica gratuita al personal docente no universitario y al personal de Administración y Servicios de los centros docentes públicos y de los servicios educativos dependientes de la Consejería competente en materia de educación (BOJA 29-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden22sept2011SubvencionesAuxiliaresConversacion.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 22 de septiembre de 2011</a>, por la que se establecen las modalidades de provisión y las bases reguladoras para la concesión de subvenciones a auxiliares de conversación, y se efectúa convocatoria para el curso 2011/12 (BOJA 29-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Calidad y Mejora de los Rendimientos Escolares</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden26sept2011ProgramaCalidad.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 26 de septiembre de 2011</a>, por la que se regula el Programa de calidad y mejora de los rendimientos escolares en los centros docentes públicos (BOJA 29-09-2011)</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación Compensatoria</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2011ProgramaAcompEscolar.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 15 de septiembre de 2011</a>, de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapa de Educación Secundaria Obligatoria.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Órganos Unipersonales</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc26mayo2011FormInicialDirectores.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCION de 26 de mayo de 2011</a> de la Dirección General de Profesorado y Gestión de Recursos Humanos por la que se oferta la formación inicial para la dirección de centros docentes a otros directores y directoras que lo soliciten.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Acompañamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2011ProgramaAcompEscolar.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 15 de septiembre de 2011</a>, de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapa de Educación Secundaria Obligatoria.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-26</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Deporte Escolar</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc6sept2011ProgramaEscuelasDeportivas.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 6 de septiembre de 2011</a> de la Dirección General de Participación e Innovación Educativa sobre el programa Escuelas Deportivas para el curso escolar 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-24</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Jornada Personal Docente</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc16sept2011LiberadosSindicales.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 16 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-24</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Organización y Funcionamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc16sept2011LiberadosSindicales.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 16 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-21</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Arte Dramático</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc20sept2011EnsArtisticasSupGrado1112.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 20 de Septiembre de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-21</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Danza</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc20sept2011EnsArtisticasSupGrado1112.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 20 de Septiembre de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-21</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Música</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc20sept2011EnsArtisticasSupGrado1112.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 20 de Septiembre de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-16</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc12sept2011BachillerBaccalaureat.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 12 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el programa de doble titulación BACHILLER-BACCALAURÉAT en centros docentes de la Comunidad Autónoma de Andalucía.</td>
</tr>
</tbody>
</table>
<div style="text-align: justify;">
<table cellspacing="3" border="1" width="98%" style="border-collapse: collapse; width: auto; margin: 1px;">
<tbody>
<tr>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-16</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Formación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/NotaInformativa16septiembre2011FCT.pdf">NOTA INFORMATIVA de 16 de septiembre de 2011</a> de la Dirección General de Formación Profesional y Educación Permanente sobre la nueva Orden reguladora de los módulos de Formación en Centros de Trabajo y de Proyecto.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-15</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc7sept2011PruebasBachillerato.pdf">RESOLUCIÓN de 7 de septiembre de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-15</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Educación de Personas Adultas</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc7sept2011PruebasBachillerato.pdf">RESOLUCIÓN de 7 de septiembre de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-14</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Inspección Educativa: Oposiciones</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc2sept2011AdmitidosDefOposInsp.pdf">RESOLUCIÓN de 2 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas definitivas de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía convocados por Orden de 26 de abril de 2011 (BOJA 14-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-14</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Organización y Funcionamiento</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc30agosto2011CelebracionLorca.pdf">RESOLUCIÓN de 30 de agosto de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por la que se dictan instrucciones para la realización en los centros docentes de la Comunidad Autónoma Andaluza de actividades orientadas a conmemorar la figura de Federico García Lorca (BOJA 14-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-13</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Centros Docentes</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden21julio2011ModificacionCentrosPrimaria.pdf">ORDEN de 21 de julio de 2011</a>, por la que se modifican escuelas infantiles de segundo ciclo, colegios de educación primaria, colegios de educación infantil y primaria y un centro específico de educación especial, así como colegios públicos rurales (BOJA 13-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Calendario y Jornada Escolar</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Nota7sept2011CalenCentrosSupArtisticos1112.pdf">NOTA INFORMATIVA de 7 de septiembre de 2011</a> de la Dirección General de Ordenación y Evaluación Educativa sobre el calendario escolar de los centros superiores de enseñanzas artísticas para el curso 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Escuela Oficial de Idiomas</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc9sept2011CALonline1112.pdf">INSTRUCCIONES de 9 de septiembre de 2011</a>, de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc9sept2011CALonline1112.pdf">INSTRUCCIONES de 9 de septiembre de 2011</a>, de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc8sept2011IdiomaChino.pdf">INSTRUCCIONES de 8 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el programa de implantación de la Enseñanza de la Lengua China en centros docentes andaluces de Educación Primaria y Secundaria.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-07</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Escuela Oficial de Idiomas</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc28julio2011ProgramaThatsEnglish1112.pdf">INSTRUCCIONES de 28 de julio de 2011</a> de la Dirección General de Formación Profesional y Educación Permanente para el funcionamiento del programa “That´S English!” en el curso 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-07</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc2sept2011OrgFuncCentrosBilingues.pdf">INSTRUCCIONES de 2 de septiembre de 2011</a> conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente del Profesorado sobre la organización y funcionamiento de la enseñanza bilingüe para el curso 2011-2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-07</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Programas de Cualificación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden1agosto2011BasesSubvencionesPCPI.pdf">ORDEN de 1 de agosto de 2011</a>, por la que se aprueban las bases reguladoras para la concesión de subvenciones en régimen de concurrencia competitiva a Corporaciones Locales, asociaciones profesionales y organizaciones no gubernamentales para el desarrollo de los módulos obligatorios de los Programas de Cualificación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 07-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-05</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Atención a la Diversidad</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc1sept2011ProcAltasCapacidades.pdf">INSTRUCCIONES de 1 de septiembre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el procedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-05</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Educación Especial</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc1sept2011ProcAltasCapacidades.pdf">INSTRUCCIONES de 1 de septiembre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el procedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-02</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Artes Plásticas y Diseño</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden18agosto2011CurriculoCiclosArtesPlasticasCeramica.pdf">ORDEN de 18 de agosto de 2011</a>, por la que se desarrolla el currículo correspondiente a los títulos de Técnico de Artes Plásticas y Diseño en Alfarería y en Decoración Cerámica y los títulos de Técnico superior de Artes Plásticas y Diseño en Cerámica Artística, en Modelismo y Matricería Cerámica y en Recubrimientos Cerámicos, pertenecientes a la familia profesional artística de la Cerámica Artística (BOJA 02-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-31</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/ModifInstruc6julio2011PremiosBachillerato.pdf">Modificación a las INSTRUCCIONES de 6 de julio de 2011</a> de la Dirección General de Ordenación y Evaluación Educativa, sobre los premios extraordinarios de bachillerato correspondientes al curso 2010/2011.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-23</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Arte Dramático</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.juntadeandalucia.es/boja/boletines/2011/165/d/7.html">DECRETO 259/2011, de 26 de julio</a>, por el que se establecen las enseñanzas artísticas superiores de Grado en Arte Dramático en Andalucía (BOJA 23-08-2011).&nbsp;<a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto259-2011ArteDramatico.pdf">Descargar disposición en pdf (20,8 MB)</a></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-23</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Música</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.juntadeandalucia.es/boja/boletines/2011/165/d/8.html">DECRETO 260/2011, de 26 de julio</a>, por el que se establecen las enseñanzas artísticas superiores de Grado en Música en Andalucía (BOJA 23-08-2011).&nbsp;<a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto260-2011Musica.pdf">Descargar disposición en pdf (51,4 MB)</a></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-22</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Danza</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.juntadeandalucia.es/boja/boletines/2011/164/d/7.html">DECRETO 258/2011, de 26 de julio</a>, por el que se establecen las enseñanzas artísticas superiores de Grado en Danza en Andalucía (BOJA 22-08-2011).&nbsp;<a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto258-2011Danza.pdf">Descargar disposición en pdf (77 MB)</a></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-19</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Acceso a la universidad</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.iesmigueldecervantes.es/normas/acuerdos/CorreccionErroresAcuerdo11marzo2011GradoUniversidad.pdf">CORRECCIÓN de errores del Acuerdo de 11 de marzo de 2011</a>, de la Dirección General de Universidades, Comisión del Distrito Único Universitario de Andalucía, por el que se establece el procedimiento para el ingreso en los estudios universitarios de Grado (BOJA 19-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-19</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Procedimiento Disciplinario / Incompatibilidades</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden2agosto2011PotestadDisciplinariaDirectores.pdf">ORDEN de 2 de agosto de 2011</a>, por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de los centros públicos de educación no universitaria (BOJA 19-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-19</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Órganos Unipersonales</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden2agosto2011PotestadDisciplinariaDirectores.pdf">ORDEN de 2 de agosto de 2011</a>, por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de los centros públicos de educación no universitaria (BOJA 19-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-16</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Inspección Educativa: Oposiciones</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/CorreccionErrataResolucion27junio2011OposicionesInspeccion.pdf">Corrección de errata de la RESOLUCIÓN de 27 de junio de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas de ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 16-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-11</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Centros Docentes</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden20julio2011AutorizacionEnsenanzas1112.pdf">ORDEN de 20 de julio de 2011</a>, por la que se modifica la autorización de enseñanzas en centros docentes públicos a partir del curso escolar 2011/12 (BOJA 11-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Cooperación con Entidades Locales</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden6julio2011SubvencionesEscuelasMusicaDanza.pdf">ORDEN de 6 de julio de 2011</a>, por la que se establecen las bases reguladoras para la concesión de subvenciones a las Escuelas de Música y Danza dependientes de entidades locales y se efectúa su convocatoria para el año 2012 (BOJA 09-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Cultura Emprendedora</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/CorreccionErroresDecreto219CulturaEmprendedora.pdf">CORRECCIÓN de errores del Decreto 219/2011</a>, de 28 de junio, por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el sistema educativo público de Andalucía (BOJA 09-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-08</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Centros Docentes</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden15julio2011PuestosCentrosInfantil.pdf">ORDEN de 15 de julio de 2011</a>, por la que se autoriza la denominación específica, así como el número de puestos escolares de primer ciclo de educación infantil, a determinadas escuelas infantiles de titularidad municipal (BOJA 08-08-2011).</td>
</tr>
<tr>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;">2011-08-03</p>
</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/CorreccionErroresOrden15junio2011PremiosBachillerato.pdf" style="text-decoration: none; color: #005fa9;">CORRECCIÓN de errores de la Orden de 15 de junio de 2011</a>, por la que se convocan los premios extraordinarios de bachillerato correspondientes al curso académico 2010/2011 (BOJA 03-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-03</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Danza</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto253ModificaEnsenanzasProfesionalesDanza.pdf" style="text-decoration: none; color: #005fa9;">DECRETO 253/2011, de 19 de julio</a>, por el que se modifica el Decreto 240/2007, de 4 de septiembre, por el que se establece la ordenación y currículo de las enseñanzas profesionales de danza en Andalucía (BOJA 03-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-03</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticas</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden18julio2011NombramientoMaestrosPracticas.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 18 de julio de 2011</a>, por la que se hacen públicas las listas del personal seleccionado en el procedimiento selectivo para el ingreso en el Cuerpo de Maestros, y se le nombra con carácter provisional funcionario en prácticas (BOJA 03-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-02</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Leyes Ordinarias</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/Leyes/Ley27-2011ActualizacionSeguridadSocial.pdf" style="text-decoration: none; color: #005fa9;">LEY 27/2011, de 1 de agosto</a>, sobre actualización, adecuación y modernización del sistema de Seguridad Social (BOE 02-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-02</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Leyes Ordinarias</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/Leyes/Ley26-2011DerechosPersonasDiscapacidad.pdf" style="text-decoration: none; color: #005fa9;">LEY 26/2011, de 1 de agosto</a>, de adaptación normativa a la Convención Internacional sobre los Derechos de las Personas con Discapacidad (BOE 02-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Formación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/OrdenMEC18julio2011AyudasEstudiosExtranjero.pdf" style="text-decoration: none; color: #005fa9;">ORDEN EDU/2127/2011, de 18 de julio</a>, por la que se establecen las bases y se convocan ayudas para el alumnado que curse estudios en niveles no universitarios en el exterior (BOE 28-07-2011). (Plazo: hasta el 26-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Formación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/OrdenMEC15julio2011PremiosNacionalesFPGradoSuperior.pdf" style="text-decoration: none; color: #005fa9;">ORDEN EDU/2128/2011, de 15 de julio</a>, por la que se crean y regulan los Premios Nacionales de Formación Profesional de grado superior establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 28-07-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-27</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><strong>Organización y Funcionamiento</strong></td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><strong><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/AclaracionesROCsecundaria27julio2011.pdf" style="text-decoration: none; color: #005fa9;">Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria</a>, aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 27 de julio de 2011).</strong></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-26</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Conciertos Educativos</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden4julio2011ConciertosEductivos.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 4 de julio de 2011</a>, por la que se resuelve la convocatoria de la Orden que se indica para el acceso al régimen de conciertos educativos o la renovación o modificación de los mismos con centros docentes privados de la Comunidad Autónoma de Andalucía, a partir del curso académico 2011/12 (BOJA 26-07-2011).</td>
</tr>
</tbody>
</table>
</div>
<br />
<div style="text-align: center;"></div>
<div></div>
<table align="center" width="98%" cellspacing="4" border="0" class="datos" style="border-collapse: collapse; width: auto; margin: 1px;">
<tbody>
<tr>
<td colspan="2" class="cabecera" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"></td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;">2011-07-23</p>
</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/rdecre/RD879-2011TDSSalvamentoySocorrismo.pdf">REAL DECRETO 879/2011, de 24 de junio</a>, por el que se establece el título de Técnico Deportivo Superior en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BPE 23-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-22</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/rdecre/RD878-2011TDSalvamentoySocorrismo.pdf">REAL DECRETO 878/2011, de 24 de junio</a>, por el que se establece el título de Técnico Deportivo en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BOE 22-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-22</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instrucciones22julio2011GratuidadLibrosTexto.pdf">INSTRUCCIONES de 22 de julio de 2011</a>, de la Dirección General de Participación e Innovación Educativa,&nbsp;<strong>sobre el programa de gratuidad de los libros de texto para el curso escolar 2010/2011.</strong></td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-21</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden5julio2011Becas6000.pdf">ORDEN de 5 de julio de 2011</a>, conjunta de las Consejerías de Educación y Empleo, por la que se establecen las&nbsp;<strong>Bases Reguladoras de la Beca 6000</strong>, dirigida a facilitar la permanencia en el Sistema Educativo del alumnado de Bachillerato y de Ciclos Formativos de Grado Medio de Formación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 21-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-21</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto227-2011SupervisionLibrosTexto.pdf">DECRETO 227/2011 de 5 de julio</a>, por el que se regula el depósito, el registro y la supervisión de los&nbsp;<strong>libros de texto</strong>, así como el<strong> procedimiento de selección </strong>de los mismos por los centros docentes públicos de Andalucía (BOJA 21-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-15</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/AclaracionesROC-Secundaria20junio2011.pdf">Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria</a>, aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la&nbsp;<strong>Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 20 de junio de 2011).</strong></td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-14</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto219_2011PlanFomentoCulturaEmprendedora.pdf">DECRETO 219/2011, de 28 de junio</a>, por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el Sistema Educativo Público de Andalucía (BOJA 14-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-12</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden29junio2011AutorizacionCentrosBilingues.pdf">ORDEN de 29 de junio de 2011</a>, por la que se establece el procedimiento para la autorización de la enseñanza bilingüe en los centros docentes de titularidad privada (BOJA 12-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-12</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden28junio2011EnsenanzaBilingue.pdf">ORDEN de 28 de junio de 2011</a>, por la que&nbsp;<strong>se regula la enseñanza bilingüe en los centros docentes </strong>de la Comunidad Autónoma de Andalucía (BOJA 12-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-12</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc27junio2011ListasProvOposicionesInspeccion.pdf">RESOLUCIÓN de 27 de junio de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 12-07-2011).</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>";s:9:"extension";s:11:"com_content";s:10:"created_by";s:1:"0";s:8:"modified";s:19:"2012-10-25 20:28:09";s:11:"modified_by";s:1:"0";s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":0:{}}s:3:"lft";s:2:"92";s:9:"parent_id";s:3:"196";s:5:"level";s:1:"2";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":0:{}}s:4:"slug";s:20:"104:ultima-normativa";s:4:"mime";N;s:6:"layout";s:8:"category";s:10:"metaauthor";N;s:4:"path";s:49:"index.php?option=com_content&view=category&id=104";s:6:"weight";d:1.0266200000000001;s:7:"link_id";i:431;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:4:"link";i:4;s:7:"metakey";i:5;s:8:"metadesc";i:6;s:10:"metaauthor";i:7;s:6:"author";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:2:{s:4:"Type";a:1:{s:8:"Category";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:8:"Category";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:49:"index.php?option=com_content&view=category&id=104";s:5:"route";s:49:"index.php?option=com_content&view=category&id=104";s:5:"title";s:17:"ÚLTIMA NORMATIVA";s:11:"description";s:22348:"ÚLTIMA NORMATIVA (11/10/2011) 2011-10-11 Atención a la Diversidad INSTRUCCIONES de 5 de octubre de 2011 de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012.style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> 2011-10-11 Organización y Funcionamiento INSTRUCCIONES de 5 de octubre de 2011 de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012. 2011-10-06 Educación Compensatoria CIRCULAR de 29 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar.#9a9542;"> 2011-10-06 Programa de Acompañamiento CIRCULAR de 29 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar. 2011-10-05 Educación Compensatoria INSTRUCCIONES de 30 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012. 2011-10-05arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> Funcionarios Docentes: Adscripción / Integración / Especialidades ORDEN EDU/2645/2011, de 23 de septiembre , por la que se establece la formación equivalente a la formación pedagógica y didáctica exigida para aquellas personas que estando en posesión de una titulación declarada equivalente a efectos de docencia no pueden realizar los estudios de máster (BOE 05-10-2011). 2011-10-05 Programa de Acompañamiento INSTRUCCIONES de 30 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012.1px solid #9a9542;"> 2011-09-30 Acceso a la universidad Anexo I de la Resolución de 29 de abril de 2010 , de la Secretaría de Estado de Educación y Formación Profesional, por la que se establecen las instrucciones para el cálculo de la nota media que debe figurar en las credenciales de convalidación y homologación de estudios y títulos extranjeros con el bachiller españolen el apartado Estudios y Títulos no Universitarios (actualización de 12-07-2011). 2011-09-29 Educación de Personas Adultas INSTRUCCIONES de 23 de septiembre de 2011 , de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de2011. 2011-09-29 Funcionarios Docentes: Asistencia Jurídica / Responsabilidad / Abstención RESOLUCIÓN de 16 de septiembre de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se dispone la publicación del Pacto de Mesa Sectorial en materia de prestación de asistencia jurídica gratuita al personal docente no universitario y al personal de Administración y Servicios de los centros docentes públicos y de los servicios educativos dependientes de la Consejería competente en materia de educación (BOJA 29-09-2011). 2011-09-29 Plan de Plurilingüismo ORDEN de 22 de septiembre de 2011 , por la que se establecen las modalidades de provisión y las bases reguladoras para laconcesión de subvenciones a auxiliares de conversación, y se efectúa convocatoria para el curso 2011/12 (BOJA 29-09-2011). 2011-09-29 Programa de Calidad y Mejora de los Rendimientos Escolares ORDEN de 26 de septiembre de 2011 , por la que se regula el Programa de calidad y mejora de los rendimientos escolares en los centros docentes públicos (BOJA 29-09-2011) 2011-09-28 Educación Compensatoria INSTRUCCIONES de 15 de septiembre de 2011 , de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapade Educación Secundaria Obligatoria. 2011-09-28 Órganos Unipersonales INSTRUCCION de 26 de mayo de 2011 de la Dirección General de Profesorado y Gestión de Recursos Humanos por la que se oferta la formación inicial para la dirección de centros docentes a otros directores y directoras que lo soliciten. 2011-09-28 Programa de Acompañamiento INSTRUCCIONES de 15 de septiembre de 2011 , de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapa de Educación SecundariaObligatoria. 2011-09-26 Deporte Escolar INSTRUCCIONES de 6 de septiembre de 2011 de la Dirección General de Participación e Innovación Educativa sobre el programa Escuelas Deportivas para el curso escolar 2011/2012. 2011-09-24 Jornada Personal Docente INSTRUCCIONES de 16 de septiembre de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial. 2011-09-24text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> Organización y Funcionamiento INSTRUCCIONES de 16 de septiembre de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial. 2011-09-21 Arte Dramático INSTRUCCIONES de 20 de Septiembre de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012. 2011-09-21 Danzafont: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> INSTRUCCIONES de 20 de Septiembre de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012. 2011-09-21 Música INSTRUCCIONES de 20 de Septiembre de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012. 2011-09-16 Bachilleratohref="http://www.adideandalucia.es/normas/instruc/Instruc12sept2011BachillerBaccalaureat.pdf" style="text-decoration: none; color: #005fa9;"> INSTRUCCIONES de 12 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el programa de doble titulación BACHILLER-BACCALAURÉAT en centros docentes de la Comunidad Autónoma de Andalucía. 2011-09-16 Formación Profesional Inicial NOTA INFORMATIVA de 16 de septiembre de 2011 de la Dirección General de Formación Profesional y Educación Permanente sobre la nueva Orden reguladora de los módulos de Formación en Centros de Trabajo y de Proyecto. 2011-09-15 Bachillerato RESOLUCIÓN de 7 de septiembre de 2011 , de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011).padding: 2px; border: 1px solid #9a9542;"> 2011-09-15 Educación de Personas Adultas RESOLUCIÓN de 7 de septiembre de 2011 , de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011). 2011-09-14 Inspección Educativa: Oposiciones RESOLUCIÓN de 2 de septiembre de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas definitivas de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía convocados por Orden de 26 de abril de 2011 (BOJA 14-09-2011). 2011-09-14 Organización y Funcionamiento RESOLUCIÓN de 30 deagosto de 2011 , de la Dirección General de Ordenación y Evaluación Educativa, por la que se dictan instrucciones para la realización en los centros docentes de la Comunidad Autónoma Andaluza de actividades orientadas a conmemorar la figura de Federico García Lorca (BOJA 14-09-2011). 2011-09-13 Centros Docentes ORDEN de 21 de julio de 2011 , por la que se modifican escuelas infantiles de segundo ciclo, colegios de educación primaria, colegios de educación infantil y primaria y un centro específico de educación especial, así como colegios públicos rurales (BOJA 13-09-2011). 2011-09-09 Calendario y Jornada Escolar NOTA INFORMATIVA de 7 de septiembre de 2011 de la Dirección General de Ordenación y Evaluación Educativa sobre el calendario escolar de los centros superiores de enseñanzas artísticas para el curso 2011/2012. 2011-09-09 Escuela Oficial de Idiomaspadding: 2px; border: 1px solid #9a9542;"> INSTRUCCIONES de 9 de septiembre de 2011 , de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento. 2011-09-09 Plan de Plurilingüismo INSTRUCCIONES de 9 de septiembre de 2011 , de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento. 2011-09-09 Plan de Plurilingüismohref="http://www.adideandalucia.es/normas/instruc/Instruc8sept2011IdiomaChino.pdf"> INSTRUCCIONES de 8 de septiembre de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el programa de implantación de la Enseñanza de la Lengua China en centros docentes andaluces de Educación Primaria y Secundaria. 2011-09-07 Escuela Oficial de Idiomas INSTRUCCIONES de 28 de julio de 2011 de la Dirección General de Formación Profesional y Educación Permanente para el funcionamiento del programa “That´S English!” en el curso 2011/2012. 2011-09-07 Plan de Plurilingüismo INSTRUCCIONES de 2 de septiembre de 2011 conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente del Profesorado sobre la organización y funcionamiento de la enseñanza bilingüe para el curso 2011-2012. 2011-09-07 Programas de CualificaciónProfesional Inicial ORDEN de 1 de agosto de 2011 , por la que se aprueban las bases reguladoras para la concesión de subvenciones en régimen de concurrencia competitiva a Corporaciones Locales, asociaciones profesionales y organizaciones no gubernamentales para el desarrollo de los módulos obligatorios de los Programas de Cualificación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 07-08-2011). 2011-09-05 Atención a la Diversidad INSTRUCCIONES de 1 de septiembre de 2011 de la Dirección General de Participación e Innovación Educativa por las que se regula el procedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales. 2011-09-05 Educación Especial INSTRUCCIONES de 1 de septiembre de 2011 de la Dirección General de Participación e Innovación Educativa por las que se regula elprocedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales. 2011-09-02 Artes Plásticas y Diseño ORDEN de 18 de agosto de 2011 , por la que se desarrolla el currículo correspondiente a los títulos de Técnico de Artes Plásticas y Diseño en Alfarería y en Decoración Cerámica y los títulos de Técnico superior de Artes Plásticas y Diseño en Cerámica Artística, en Modelismo y Matricería Cerámica y en Recubrimientos Cerámicos, pertenecientes a la familia profesional artística de la Cerámica Artística (BOJA 02-09-2011). 2011-08-31 Bachillerato Modificación a las INSTRUCCIONES de 6 de julio de 2011 de la Dirección General de Ordenación y Evaluación Educativa, sobre los premios extraordinarios de bachillerato correspondientes al curso 2010/2011. 2011-08-23 Arte Dramáticostyle="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"> DECRETO 259/2011, de 26 de julio , por el que se establecen las enseñanzas artísticas superiores de Grado en Arte Dramático en Andalucía (BOJA 23-08-2011). Descargar disposición en pdf (20,8 MB) 2011-08-23 Música DECRETO 260/2011, de 26 de julio , por el que se establecen las enseñanzas artísticas superiores de Grado en Música en Andalucía (BOJA 23-08-2011). Descargar disposición en pdf (51,4 MB) 2011-08-22 Danza DECRETO 258/2011, de 26 de julio , por el que se establecen las enseñanzas artísticas superiores de Grado en Danza en Andalucía (BOJA 22-08-2011). Descargar disposición en pdf (77 MB)1px solid #9a9542;"> 2011-08-19 Acceso a la universidad CORRECCIÓN de errores del Acuerdo de 11 de marzo de 2011 , de la Dirección General de Universidades, Comisión del Distrito Único Universitario de Andalucía, por el que se establece el procedimiento para el ingreso en los estudios universitarios de Grado (BOJA 19-08-2011). 2011-08-19 Funcionarios Docentes: Procedimiento Disciplinario / Incompatibilidades ORDEN de 2 de agosto de 2011 , por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de los centros públicos de educación no universitaria (BOJA 19-08-2011). 2011-08-19 Órganos Unipersonales ORDEN de 2 de agosto de 2011 , por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de loscentros públicos de educación no universitaria (BOJA 19-08-2011). 2011-08-16 Inspección Educativa: Oposiciones Corrección de errata de la RESOLUCIÓN de 27 de junio de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas de ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 16-08-2011). 2011-08-11 Centros Docentes ORDEN de 20 de julio de 2011 , por la que se modifica la autorización de enseñanzas en centros docentes públicos a partir del curso escolar 2011/12 (BOJA 11-08-2011). 2011-08-09 Cooperación con Entidades Localeshref="http://www.adideandalucia.es/normas/ordenes/Orden6julio2011SubvencionesEscuelasMusicaDanza.pdf"> ORDEN de 6 de julio de 2011 , por la que se establecen las bases reguladoras para la concesión de subvenciones a las Escuelas de Música y Danza dependientes de entidades locales y se efectúa su convocatoria para el año 2012 (BOJA 09-08-2011). 2011-08-09 Cultura Emprendedora CORRECCIÓN de errores del Decreto 219/2011 , de 28 de junio, por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el sistema educativo público de Andalucía (BOJA 09-08-2011). 2011-08-08 Centros Docentes ORDEN de 15 de julio de 2011 , por la que se autoriza la denominación específica, así como el número de puestos escolares de primer ciclo de educación infantil, a determinadas escuelas infantiles de titularidad municipal (BOJA 08-08-2011). 2011-08-03normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> Bachillerato CORRECCIÓN de errores de la Orden de 15 de junio de 2011 , por la que se convocan los premios extraordinarios de bachillerato correspondientes al curso académico 2010/2011 (BOJA 03-08-2011). 2011-08-03 Danza DECRETO 253/2011, de 19 de julio , por el que se modifica el Decreto 240/2007, de 4 de septiembre, por el que se establece la ordenación y currículo de las enseñanzas profesionales de danza en Andalucía (BOJA 03-08-2011). 2011-08-03 Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticasnormal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> ORDEN de 18 de julio de 2011 , por la que se hacen públicas las listas del personal seleccionado en el procedimiento selectivo para el ingreso en el Cuerpo de Maestros, y se le nombra con carácter provisional funcionario en prácticas (BOJA 03-08-2011). 2011-08-02 Leyes Ordinarias LEY 27/2011, de 1 de agosto , sobre actualización, adecuación y modernización del sistema de Seguridad Social (BOE 02-08-2011). 2011-08-02 Leyes Ordinarias LEY 26/2011, de1 de agosto , de adaptación normativa a la Convención Internacional sobre los Derechos de las Personas con Discapacidad (BOE 02-08-2011). 2011-07-28 Formación Profesional Inicial ORDEN EDU/2127/2011, de 18 de julio , por la que se establecen las bases y se convocan ayudas para el alumnado que curse estudios en niveles no universitarios en el exterior (BOE 28-07-2011). (Plazo: hasta el 26-09-2011). 2011-07-28 Formación Profesional Inicial ORDEN EDU/2128/2011, de 15 de julio , por la que se crean y regulan los Premios Nacionales de Formación Profesional de grado superior establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 28-07-2011).width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"> 2011-07-27 Organización y Funcionamiento Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria , aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 27 de julio de 2011). 2011-07-26 Conciertos Educativos ORDEN de 4 de julio de 2011 , por la que se resuelve la convocatoria de la Orden que se indica para el acceso al régimen de conciertos educativos o la renovación o modificación de los mismos con centros docentes privados de la ComunidadAutónoma de Andalucía, a partir del curso académico 2011/12 (BOJA 26-07-2011). 2011-07-23 REAL DECRETO 879/2011, de 24 de junio , por el que se establece el título de Técnico Deportivo Superior en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BPE 23-07-2011). 2011-07-22 REAL DECRETO 878/2011, de 24 de junio , por el que se establece el título de Técnico Deportivo en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BOE 22-07-2011). 2011-07-22 INSTRUCCIONES de22 de julio de 2011 , de la Dirección General de Participación e Innovación Educativa, sobre el programa de gratuidad de los libros de texto para el curso escolar 2010/2011. 2011-07-21 ORDEN de 5 de julio de 2011 , conjunta de las Consejerías de Educación y Empleo, por la que se establecen las Bases Reguladoras de la Beca 6000 , dirigida a facilitar la permanencia en el Sistema Educativo del alumnado de Bachillerato y de Ciclos Formativos de Grado Medio de Formación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 21-07-2011). 2011-07-21 DECRETO 227/2011 de 5 de julio , por el que se regula el depósito, el registro y la supervisión de los libros de texto , así como el procedimiento de selección de los mismos por los centros docentes públicos de Andalucía (BOJA 21-07-2011). 2011-07-15 Aclaraciones en torno al Reglamento Orgánico delos institutos de Educación Secundaria , aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 20 de junio de 2011). 2011-07-14 DECRETO 219/2011, de 28 de junio , por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el Sistema Educativo Público de Andalucía (BOJA 14-07-2011). 2011-07-12 ORDEN de 29 de junio de 2011 , por la que se establece el procedimiento para la autorización de la enseñanza bilingüe en los centros docentes de titularidad privada (BOJA 12-07-2011). 2011-07-12 ORDEN de 28 de junio de 2011 , por la que se regula la enseñanza bilingüe en los centros docentes de la Comunidad Autónoma de Andalucía (BOJA12-07-2011). 2011-07-12 RESOLUCIÓN de 27 de junio de 2011 , de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 12-07-2011).";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"0000-00-00 00:00:00";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"0000-00-00 00:00:00";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:1;}i:10;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:2:"51";s:5:"alias";s:15:"dep-matematicas";s:7:"summary";s:113169:"<h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; background-color: #ffffff; text-align: justify;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><strong><span style="text-decoration: underline;">CURSO 2014/15<br /><br /></span></strong><strong style="font-size: 13px; text-transform: none; color: #2b2721; background-color: #ffffff;"><span style="color: #423c33;"><a href="archivos_ies/14_15/programaciones/PROGRAMAC2014-15.pdf" target="_blank">PROGRAMACIÓN DE MATEMÁTICAS, CURSO 2014/15.</a>  12/10/14<br /><br />25/04/2015:<a href="archivos_ies/14_15/mate/calendario_pendientes_mat_prueba_final_eso.pdf" target="_blank" style="text-align: justify; color: #2f310d; font-size: 13px; font-weight: normal; text-transform: none;"> ACTUALIZADAS LAS FECHAS DE LAS PRUEBAS FINALES de pendientes de cursos anteriores DE ESO. VER ENLACE.</a></span></strong></span></h3>
<p><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><strong style="font-size: 13px; text-transform: none; color: #2b2721; background-color: #ffffff;"><span style="color: #423c33;"> </span></strong></span></p>
<h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; background-color: #ffffff; text-align: justify;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><strong><span style="text-decoration: underline;">ENLACES DIRECTOS A LAS SIGUIENTES SECCIONES:<br /><br /></span></strong><a href="index.php?option=com_content&amp;view=article&amp;id=137&amp;Itemid=724" target="_blank">2º Eso</a><strong><span style="text-decoration: underline;"><br /></span></strong></span><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><a href="index.php?option=com_content&amp;view=article&amp;id=93&amp;Itemid=103" target="_blank" title="MAT. I">1º BACH. MAT.I</a><br /><span style="color: #5d5d5d; font-size: 20px; text-align: left; text-transform: uppercase;"><a href="http://matepaco.blogspot.com.es/p/bachillerato-ccss-i.html" target="_blank">1º BACH. MAT. CC.SS I</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=154&amp;Itemid=103" target="_blank" title="MAT. II">2º BACH. MAT. II</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=94&amp;Itemid=103" target="_blank" title="MAT. CC.SS. II">2º BACH. MAT. CC.SS II</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=103&amp;Itemid=103" target="_blank" title="MAT. CC.SS. II">2º BACH. ESTADÍSTICA<br /></a><a href="index.php?option=com_content&amp;view=article&amp;id=236&amp;Itemid=103" target="_blank" title="PENDIENTES.&quot;">PENDIENTES DE TODOS LOS CURSOS<br /><br /></a><a href="http://matepaco.blogspot.com.es/" target="_blank">UN BUEN bLOG DE MATEMÁTICAS</a></span></span></h3>
<p> </p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: justify; background-color: #ffffff;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><strong><span style="text-decoration: underline;"><span style="color: #ff0000; text-decoration: underline;">novedad:</span></span> <br /></strong>este curso 2014/15 contará con un grupo de estadística en 2º bachillerato cns.<span style="text-decoration: underline;"><br /><br />CURSO 2014/15<br /><br /></span><strong style="color: #2a2a2a; font-size: 13px; text-transform: none; background-color: #ffffff;"><span>IMPORTANCIA DE ELEGIR ESTADÍSTICA COMO OPTATIVA EN 2º BACHILLERATO CIENCIAS<br /><br /></span></strong><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">El alumnado de Ciencias puede subir la nota de Selectividad en la Fase Específica mediante la realización de pruebas de distintas materias. En particular las Matemáticas II suele puntuar con una ponderación de 0,2 en la mayoría de los Grados de Ciencias (subir hasta 2 puntos la nota de la Fase General). El problema es que muchos alumnos se examinan de Matemáticas II en la Fase General al no disponer de otra materia mejor, según sus intereses.<br /></span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Es por ello que el Departamento de Matemáticas informa que este inconveniente se puede subsanar si el alumnado se examinara de Matemáticas CC SS II en la Fase General y de </span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Matemáticas II en la Fase Específica.<br /></span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Como quiera que este alumnado de Ciencias sólo ha cursado la asignatura de Matemáticas II, existe una posibilidad de que completen los conocimientos que les faltan para poder dominar la materia de Matemáticas CC SS II mediante la asignatura de Estadística, que se puede elegir como optativa en 2º de Bachillerato Ciencias.<br /></span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Dada la importancia de obtener una buena nota final en Selectividad es por lo que el Departamento de Matemáticas anima al alumnado que actualmente está en fase de matriculación para que seleccione </span><strong style="color: #2a2a2a; font-size: 13px; text-transform: none;">Estadística</strong><span style="color: #2a2a2a; font-size: 13px; text-transform: none;"> como </span><strong style="color: #2a2a2a; font-size: 13px; text-transform: none;">optativa</strong><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">. </span></span><span style="text-align: left; background-color: #ffffff;">Si se necesita más información se puede consultar a cualquier miembro del Departamento, o escribiendo un mensaje a <strong>iescervantesgranada[arroba]gmail.com</strong> que se le haría llegar a los mismos.<br /><br /></span></p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: center; background-color: #ffffff;"><strong><span style="text-align: left; background-color: #ffffff;">A CONTINUACIÓN SE PUEDEN VER LOS DISTINTOS GRADOS QUE SE IMPARTEN EN LA UNIVERSIDAD DE GRANADA EN LOS QUE SE CURSA UNA ASIGNATURA DE ESTADÍSTICA:</span></strong><img src="http://www.stei.es/estadistica/images/stories/ugr3.png" border="0" alt="" width="602" height="102" style="text-align: center; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;" /></p>
<div class="floatbox" style="margin: 0px; padding: 0px; overflow: hidden; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #ffffff;">
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Enfermería</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/enfermeria.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Fisioterapia</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/fisioterapia.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Gestión y Administración Pública (Melilla)</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~eues/webgrupo/Docencia/MonteroAlonso/MonteroEstadistica2.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística II</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Relaciones Laborales</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~ramongs/relacioneslaborales.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~eao" style="text-decoration: none; color: #678925;">Estadística Asistida por Ordenador</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Biología</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioestad/ASIGNATURAS.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioestad/ASIGNATURAS.htm" target="_blank" style="text-decoration: none; color: #678925;">Fundamentos de Biología Aplicada I</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Ciencias de la Actividad Física  y el Deporte</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/inef.htm#1" target="_blank" style="text-decoration: none; color: #678925;">Estadística</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/inef.htm#2" target="_blank" style="text-decoration: none; color: #678925;">Estadística Aplicada a la Actividad Física</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Ciencias y Técnicas Estadísticas</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/ciencias.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Documentación</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~focana/stat.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Economía</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~focana/peef.htm" target="_blank" style="text-decoration: none; color: #678925;">Predicción Económica y Empresarial</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Farmacia</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~focana/mafarma.htm" target="_blank" style="text-decoration: none; color: #678925;">Matemática Aplicada</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Medicina</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/medicina02op.htm#be3" target="_blank" style="text-decoration: none; color: #678925;">Análisis Estadístico con Ordenador de Datos Médicos</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/medicina02tr.htm#be2" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Odontología</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/odonto.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística Aplicada</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Sociología</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~ramongs/sociologia.htm" target="_blank" style="text-decoration: none; color: #678925;">Análisis Multivariante</a></li>
</ul>
</div>
<hr />
<div id="contenido_sitio" style="float: CENTER; display: block; width: 98%;">
<table class="MsoNormalTable" style="padding-left: 30px; width: 650px;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 517.45pt; border-width: 1pt; padding: 1.5pt;" colspan="4" valign="top" width="690">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center; padding-left: 30px;" align="center"><strong><span style="text-decoration: underline;"><span style="font-size: 10pt; font-family: 'Century Gothic', sans-serif;">PENDIENTES DE MATEMÁTICAS PARA SEPTIEMBRE DE 2014</span></span></strong></p>
</td>
</tr>
<tr style="height: 112.8pt; padding-left: 10px;">
<td style="border-top-style: none; border-left-width: 1pt; border-bottom-style: none; border-right-width: 1pt; padding: 30pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 1º ESO</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-bottom-style: none; border-left-style: none; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">TEMA 1</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">. Números naturales<br /> <strong>TEMA 2.</strong> Divisibilidad<br /> <strong>TEMA 3.</strong> Fracciones<br /> <strong>TEMA 4.</strong>  Números decimales<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">TEMA 5.</span></strong><span style="font-size: 8pt; font-family: inherit, serif;"> Números enteros<br /> <strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-bottom-style: none; border-left-style: none; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 57pt; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">TEMA 7.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Sistema Métrico Decimal</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">TEMA 8.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Proporcionalidad numérica<br /> <strong>TEMA 9.</strong> Ángulos, circunferencias y círculos<br /> <strong>TEMA 10.</strong> Polígonos<br /> <strong>TEMA 11.</strong> Funciones y gráficas</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-bottom-style: none; border-left-style: none; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 1</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 2</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="font-family: 'inherit','serif'; color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 6</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 7</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 8</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 10</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="border-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 2º ESO</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 1.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números enteros<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">2.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Fracciones<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">3.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números decimales<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">4.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Sistema sexagesimal<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">5.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Expresiones algregráicas<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">6.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Ecu</span><span style="font-size: 8pt; font-family: inherit, serif;">acioens de primer y segundo grado</span></p>
</td>
<td style="width: 5cm; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_7_a_11.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt 9pt 12.65pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Sistemas de ecuaciones</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> <strong>UNIDAD </strong></span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">8.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Proporcionalidad numérica<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">9.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Proporcionalidad geométrica<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">10.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Figuras planas. Áreas<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">11.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Cuerpos geométricos</span></p>
</td>
<td style="width: 5cm; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 1</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 2</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDADES 3 Y 4</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><span style="color: #5d5d5d; letter-spacing: .75pt;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank">UNIDAD 5<br /></a></span></span><a href="archivos_ies/13_14/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank" style="letter-spacing: 0.75pt; font-size: 8pt;">UNIDAD 6</a></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 7</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 8</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9 Y 10</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Cuerpos_geometricos_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 3º ESO</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_1_a_6.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 1.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números racionales<br /> <strong>UNIDAD 2.</strong> Números reales<br /> <strong>UNIDAD 3.</strong> Polinomios<br /> <strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado <br /> <strong>UNIDAD 5.</strong> Sistemas de ecuaciones<br /> <strong>UNIDAD 6.</strong> Proporcionalidad numérica</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_7_a_12.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Progresiones</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 8.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Lugares geométricos.Figuras planas<br /> <strong>UNIDAD 9.</strong> Cuerpos geométricos<br /> <strong>UNIDAD 10.</strong> Movimientos y semejanza<br /> <strong>UNIDAD 11.</strong> Funciones<br /> <strong>UNIDAD 12.</strong> Funciones lineales y afines</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDADES 1 Y 2</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="font-family: 'inherit','serif'; color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 6</span><span style="color: #5d5d5d; letter-spacing: .75pt;"> </span></a></span></p>
<div class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><a href="archivos_ies/13_14/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank" style="font-size: 7pt;"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 7</span></a></div>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 8</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Movimientos_plano_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 10</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_y_graficas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_lineales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 12</span></a></span></p>
</td>
</tr>
<tr style="height: 132.75pt; padding-left: 30px;">
<td style="border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 1º BACH. MAT. CN.</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/examen1.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 1:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números Reales<br /> <strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas<br /> <strong>UNIDAD 3:</strong> Trigonometría<br /> <strong>UNIDAD 4:</strong> Números Complejos<br /> <strong>UNIDAD 5:</strong> Geometría analítica</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CNAT.pdf" target="_blank"><strong><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></strong></a></span></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Funciones<br /> <strong>UNIDAD 8:</strong> Funciones elementales<br /> <strong>UNIDAD 9:</strong> Límite de una función<br /> <strong>UNIDAD 10:</strong> Derivada de una función<br /> <strong>UNIDAD 11:</strong> Integrales</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 1</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 2</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a></span></p>
<div class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank" style="font-size: 7pt;"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDADES: 7 Y 8</span></a></div>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 10</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Integrales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 1º BACH. MAT. CCSS.</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">1:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números Reales<br /> <strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 4:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Ecuaciones, inecuaciones y sistemas</span></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; page-break-before: always; padding-left: 30px;"><span style="font-size: 10pt; font-family: 'Times New Roman', serif;"> </span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center; padding-left: 30px;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> </span></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 5:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Funciones<br /> <strong>UNIDAD 6:</strong> Funciones elementales</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Límite de una función<br /> <strong>UNIDAD 8:</strong> Derivada de una función<br /> <strong>UNIDAD 11:</strong> Probabilidad</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt;"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 1</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a></span></p>
<div class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank" style="font-size: 7pt;"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a></div>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 6</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 7</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 8</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Binomial_resueltos.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt; text-decoration: none; text-underline: none;">UNIDAD 11</span></a></span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><strong><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 107%;"> </span></strong></p>
<p class="MsoNormal" style="padding-left: 30px;"><strong><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 107%;">LIBROS DE TEXTO DEL DEPARTAMENTO DE MATEMÁTICAS, CURSO 2014/15:</span></strong><span style="color: #5d5d5d; font-size: 31px; font-weight: bold; text-transform: uppercase;"> </span></p>
<table class="MsoTableGrid" style="width: 642px; border: none; padding-left: 30px;" border="1" cellspacing="0" cellpadding="0">
<tbody style="padding-left: 30px;">
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>CURSO</strong></p>
</td>
<td style="width: 106.15pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Materia</strong></p>
</td>
<td style="width: 163.1pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Editorial</strong></p>
</td>
<td style="width: 106.3pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>EAN</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>1º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana  Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052570</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>2º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana  Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788429438215</p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>3º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana  Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052587</p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>4º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas A</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052112</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>4º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas B</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052242</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>1º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas I</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas I</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">La casa del saber (González y otros)</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>1º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas CCSS I</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas aplicadas a las CCSS I</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;">Santillana. </span>La casa del Saber (Escoredo y otros)</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>2º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas II</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas II</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;">Santillana. </span>Matemáticas II. La casa del saber (González y</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">otros)</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>2º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas CCSS II</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas aplicadas a las CCSS II</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;">Santillana. </span>Matemáticas aplicadas a las CCSS II. La casa</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">del saber (Escoredo y otros)</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
</tbody>
</table>
<h1 style="text-align: center; padding-left: 30px;"> <span style="text-align: left;"> </span></h1>
<hr style="padding-left: 30px;" />
<h1 style="text-align: center; padding-left: 30px;"><strong style="text-align: left;"><span style="color: #423c33;">CURSO 2013/14</span></strong></h1>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; padding-left: 30px;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Primera reunión de Coordinación para Selectividad de Matemáticas CC SS II </span></h2>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="color: #2a2a2a;"> </span><span style="text-indent: 0cm; text-transform: none; color: #2a2a2a;"> </span><a href="archivos_ies/13_14/2bach_matccssii/acta_1reunion_m_ccssii.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></li>
</ul>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; padding-left: 30px;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Primera reunión de Coordinación para Selectividad de Matemáticas II </span></h2>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="color: #2a2a2a;"> </span><span style="text-indent: 0cm; text-transform: none; color: #2a2a2a;"> </span><a href="archivos_ies/13_14/2bach_matii/informacionmatematicasii.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></li>
</ul>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"> </p>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="color: #423c33;">20/12/13:</span></strong></span><strong><span style="color: #423c33;"> </span></strong>Los desafíos matemáticos vuelven a esta casa por Navidad. Como ya sucedió el año pasado, con motivo del sorteo de la <a href="http://elpais.com/tag/loteria_navidad/a/">Lotería de Navidad</a> ofrecemos un nuevo problema. El encargado de presentar el <em>Desafío Extraordinario de Navidad</em> de 2013 es Javier Cilleruelo, profesor de la <a href="http://www.uam.es/">Universidad Autónoma de Madrid</a> y miembro del <a href="http://www.icmat.es/es">Instituto de Ciencias Matemáticas</a> (ICMAT).<br />Entre los acertantes se sorteará una biblioteca matemática como la que ofreció EL PAÍS en el quiosco durante 2011. El ganador recibirá, además, el libro <a href="http://estimulos-matematicos.aprenderapensar.net/">'Desafíos Matemáticos'</a> por cortesía de la <a href="http://www.rsme.es/">Real Sociedad Matemática Española</a>, una publicación de SM en la que se recogen los <a href="http://sociedad.elpais.com/sociedad/2011/07/12/actualidad/1310421608_850215.html">40 desafíos matemáticos</a> que ofrecimos en la web y con los que EL PAÍS y la RSME celebraron el centenario de esta institución hace dos años. Manda tu respuesta antes de las 00.00 horas del domingo 22 de diciembre (la medianoche del sábado al domingo, hora peninsular española) a <a href="mailto:problemamatematicas@gmail.com">problemamatematicas@gmail.com</a> y participa en el sorteo. <br />A continuación, para aclarar dudas y en atención a nuestros lectores sordos, añadimos el enunciado del problema por escrito: <br />El equipo que preparamos los desafíos matemáticos hemos decidido abonarnos durante todo el año a un número de la Lotería. Para elegir ese número, que debe estar comprendido entre el 0 y el 99.999, pusimos como condición que tuviese las cinco cifras distintas y que, además, cumpliese alguna otra propiedad interesante. Finalmente hemos conseguido un número que tiene la siguiente propiedad: si numeramos los meses del año del 1 al 12, en cualquier mes del año ocurre que al restar a nuestro número de lotería el número del mes anterior, el resultado es divisible por el número del mes en el que estemos. Y esto sucede para cada uno de los meses del año. <br />Es decir, si llamamos L a nuestro número, tenemos por ejemplo que en marzo L-2 es divisible entre 3 y en diciembre L-11 es divisible entre 12. <br /><br />El reto que os planteamos es que nos digáis a qué número de Lotería estamos abonados y que nos expliquéis cómo lo habéis encontrado.<br /><br /><strong>OBSERVACIONES IMPORTANTES.</strong> Insistimos en que es importante que hagáis llegar junto con el número el razonamiento de cómo lo habéis hallado. No se considerarán válidas las respuestas que den sólo el número o que lo hayan encontrado probando todos uno a uno (a mano o con un ordenador).</li>
</ul>
<p style="padding-left: 30px;"> </p>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="color: #423c33;"> </span></strong></span></p>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><a href="archivos_ies/13_14/programaciones/13_14_PROGRAMACION_Mate4.doc" target="_blank"><strong style="color: #2b2721;"><span style="color: #423c33;">PROGRAMACIÓN DE MATEMÁTICAS, CURSO 2013/14.</span></strong></a></li>
<li style="padding-left: 30px;"><strong style="color: #2b2721;"><span style="color: #423c33;"><a href="archivos_ies/13_14/pendientes/Pendientes13_14_b.pdf" target="_blank">FECHAS DE EXÁMENES: PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES.</a><strong style="color: #2b2721;"><span style="color: #423c33;"> <br /><br /></span></strong></span></strong></li>
<li style="padding-left: 30px;"><strong style="color: #2b2721;"><span style="color: #423c33;"><strong style="color: #2b2721;"><span style="color: #423c33;"><a href="archivos_ies/13_14/2bach_matccssii/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2013_2014.pdf" target="_blank">Directrices y orientaciones Selectividad Mat. CC.SS. II curso 2013/14.</a><br /><br /></span></strong></span></strong></li>
<li style="padding-left: 30px;">12/11/2012:  <a href="archivos_ies/13_14/2bach_matccssii/COORDINACION05-11-12_Y_DIRECTRICES_OFICIALES5.pdf" target="_blank">Coordinación de Selectividad de Mat. CC SS II del 05/11/12..</a></li>
<li style="padding-left: 30px;">09/11/2012: <a href="archivos_ies/13_14/2bach_matccssii/Sept_2012_selec_MatCCSSII_y_soluciones2.pdf" target="_blank">Examen y soluciones de Selectividad de MAT_CC_SS_II de sept. 2012.</a></li>
<li style="padding-left: 30px;">20/06/2012: <a href="archivos_ies/13_14/2bach_matccssii/Junio_2012_selec_MatCCSSII_y_soluciones.pdf" target="_blank">Exámen y soluciones de Selectividad de MAT_CC_SS_II de jun. 2012,</a></li>
</ul>
<hr style="padding-left: 30px;" />
<div style="padding-left: 30px;">
<ul style="color: #151509; margin: 1em 0px 1em 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; padding: 0px 0px 0px 30px;">
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS1_ESO.pdf" target="_blank" title="Criterios de evaluación de 1º ESO." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Matemáticas 1º ESO.</a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_2_ESO.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">2º ESO.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_3_ESO.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">3º ESO.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_4_ESO.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">4º ESO.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_CCSS I _1BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">1º Bachillerato CCSS I.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS I _1BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas I</span></span> 1</span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">º Bachillerato CNAT.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_CCSSII_2BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas </span></span>2</span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">º Bachillerato CCSS II.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_II_2BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas II</span></span> 2</span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">º Bachillerato CNAT</span></span>.</a></span></li>
</ul>
<p style="padding-left: 30px;"> </p>
<hr style="padding-left: 30px;" />
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=94" target="_blank"><span style="color: #2a2a2a;">ENLACE A ZONA DE 2º BACH. CC.SS. II.</span></a></strong></li>
<li style="padding-left: 30px;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=154" target="_blank"><span style="color: #2a2a2a;">ENLACE A ZONA DE 2º BACH. CC.NN. II.</span></a></strong></li>
</ul>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"> </p>
<h2 style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES:</span></strong></span></h2>
<p style="padding-left: 30px;"> </p>
<table class="borde_outset" style="text-align: center; color: #2a2a2a; padding-left: 30px; background-color: #e7eaad;" border="1">
<tbody style="padding-left: 30px;">
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 1º ESO</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" style="text-decoration: underline; color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"> <strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>TEMA 1</strong>. Números naturales</p>
<p style="padding-left: 30px;"><strong>TEMA 2.</strong> Divisibilidad</p>
<p style="padding-left: 30px;"><strong>TEMA 3.</strong> Fracciones</p>
<p style="padding-left: 30px;"><strong>TEMA 4.</strong>  Números decimales</p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p style="padding-left: 30px;"><span style="font-family: 'Arial','sans-serif';"> </span></p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="text-decoration: underline; color: #2f310d; background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="text-align: left; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p style="padding-left: 30px;"><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p style="padding-left: 30px;"><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p style="padding-left: 30px;"><strong>TEMA 10.</strong> Polígonos</p>
<p style="padding-left: 30px;"><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p style="padding-left: 30px;"> </p>
</td>
<td style="text-align: center; padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/13_14/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></span></a><hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p style="padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<a href="archivos_ies/13_14/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 2º ESO</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank" style="text-decoration: underline; color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"> <strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 1.</strong> Números enteros</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>2.</strong> Fracciones</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>3.</strong> Números decimales</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>4.</strong> Sistema sexagesimal</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>5.</strong> Expresiones algregráicas</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>6.</strong> Ecu<span style="color: inherit; font-family: inherit; font-size: inherit;">acioens de primer y segundo grado</span></p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_7_a_11.pdf" target="_blank" style="text-decoration: underline; color: #2f310d; background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="text-align: left; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>7.</strong> Sistemas de ecuaciones</span></p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>8.</strong> Proporcionalidad numérica</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>9.</strong> Proporcionalidad geométrica</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>10.</strong> Figuras planas. Áreas</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>11.</strong> Cuerpos geométricos</p>
</td>
<td style="text-align: center; padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank">UNIDADES 3 Y 4</a></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/13_14/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></span></a><hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank">UNIDAD 9 Y 10</a></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Cuerpos_geometricos_resueltos.pdf" target="_blank">UNIDAD 11</a><br /></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 3º ESO</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_1_a_6.pdf" target="_blank" style="text-decoration: underline; color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 1.</strong> Números racionales</p>
<p style="padding-left: 30px;"><span style="font-size: inherit;"><strong>UNIDAD 2.</strong> Números reales</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 3.</strong> Polinomios</p>
<p style="padding-left: 30px;"><strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado </p>
<p style="padding-left: 30px;"><strong>UNIDAD 5.</strong> Sistemas de ecuaciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 6.</strong> Proporcionalidad numérica</p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_7_a_12.pdf" target="_blank" style="text-decoration: underline; color: #2f310d; background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 7.</strong> Progresiones</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 8.</strong> Lugares geométricos. Figuras planas</p>
<p style="padding-left: 30px;"><strong>UNIDAD 9.</strong> Cuerpos geométricos .</p>
<p style="padding-left: 30px;"><strong>UNIDAD 10.</strong> Movimientos y semejanza</p>
<p style="padding-left: 30px;"><strong>UNIDAD 11.</strong> Funciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 12.</strong> Funciones lineales y afines</p>
</td>
<td style="padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDADES 1 Y 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span> </a></p>
<hr style="padding-left: 30px;" />
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /> </strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank">UNIDAD: 7</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank">UNIDAD: 8</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank">UNIDAD 9</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Movimientos_plano_resueltos.pdf" target="_blank">UNIDAD 10</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_y_graficas_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_lineales_resueltos.pdf" target="_blank">UNIDAD 12</a><br /></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 1º BACH. MAT. CN.</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><strong><span style="text-decoration: underline;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/examen1.pdf" target="_blank"><span style="color: #2f310d; text-decoration: underline;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></strong></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 1:</strong> Números Reales</p>
<p style="padding-left: 30px;"><strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas</p>
<p style="padding-left: 30px;"><strong>UNIDAD 3:</strong> Trigonometría</p>
<p style="padding-left: 30px;"><strong>UNIDAD 4:</strong> Números Complejos</p>
<p style="padding-left: 30px;"><strong>UNIDAD 5:</strong> Geometría analítica</p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CNAT.pdf" target="_blank"><span style="text-decoration: underline;"><strong><span style="color: #2f310d;"><span style="background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></span></strong></span></a></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 7:</strong> Funciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 8:</strong> Funciones elementales</p>
<p style="padding-left: 30px;"><strong>UNIDAD 9:</strong> Límite de una función.</p>
<p style="padding-left: 30px;"><strong>UNIDAD 10:</strong> Derivada de una función</p>
<p style="padding-left: 30px;"><strong>UNIDAD 11:</strong> Integrales</p>
</td>
<td style="text-align: center; padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /> </strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank">UNIDADES: 7 Y 8</a></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank">UNIDAD 9</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank">UNIDAD 10</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Integrales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 1º BACH. MAT. CCSS.</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d; text-decoration: underline;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>1:</strong> Números Reales</p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 4:</strong> Ecuaciones, inecuaciones y sistemas</p>
<p class="MsoNormal" style="page-break-before: always; padding-left: 30px;"> </p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><strong><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="text-decoration: underline;"><span style="color: #2f310d;"><span style="background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></span></span></a></strong></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="text-align: center; padding-left: 30px;"> </p>
<p style="padding-left: 30px;"><strong>UNIDAD 5:</strong> Funciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 6:</strong> <span style="color: inherit; font-family: inherit; font-size: inherit;">Funciones elementales</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 7:</strong> Límite de una función</p>
<p style="padding-left: 30px;"><strong>UNIDAD 8:</strong> Derivada de una función</p>
<p style="padding-left: 30px;"><strong>UNIDAD 11:</strong> Probabilidad</p>
</td>
<td style="padding-left: 30px;">
<p style="padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 3</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 4</a><br /></span></p>
<hr style="padding-left: 30px;" />
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /> </strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank">UNIDAD 5</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank">UNIDAD 6</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank">UNIDAD 7</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank">UNIDAD 8</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Binomial_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div style="float: left; display: block; width: 98%; padding-left: 30px;"><strong style="color: #414141;"><span style="color: #2a2a2a; font-weight: normal;"> </span></strong></div>
<div style="float: left; display: block; width: 98%; padding-left: 60px;"><span style="text-decoration: underline;"><strong style="color: #414141;"><span style="color: #2a2a2a; text-decoration: underline;">Página personal del profesor:</span></strong></span><br />
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="font-size: 12pt;">Gerardo Bustos: <a href="http://perso.orange.es/gerbustgut59/" target="_blank" title="http://perso.gratisweb.com/gerardobustos/">entrar.</a></span></li>
</ul>
<p style="padding-left: 30px;"><strong><span style="font-size: 12pt;"><img src="images/stories/cervantes/qr_dep_mat.png" border="0" alt="Código qr del Departamento de Matemáticas." width="196" height="196" /></span></strong></p>
<p style="padding-left: 30px;">Código qr del Dep. de Matemáticas.</p>
</div>
<div class="fechaModif" style="text-align: center; padding-left: 30px;">Ultima actualización: 10/11/2013</div>
<div class="fechaModif" style="text-align: center;"><hr /></div>";s:4:"body";s:0:"";s:5:"catid";s:3:"100";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-11-23 17:47:20";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:3:"176";s:8:"ordering";s:1:"6";s:8:"category";s:13:"DEPARTAMENTOS";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:18:"51:dep-matematicas";s:7:"catslug";s:17:"100:departamentos";s:6:"author";N;s:6:"layout";s:7:"article";s:4:"path";s:98:"index.php?option=com_content&view=article&id=51:dep-matematicas&catid=100:departamentos&Itemid=103";s:10:"metaauthor";N;s:6:"weight";d:1.0266200000000001;s:7:"link_id";i:2061;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:13:"DEPARTAMENTOS";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:13:"DEPARTAMENTOS";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:47:"index.php?option=com_content&view=article&id=51";s:5:"route";s:98:"index.php?option=com_content&view=article&id=51:dep-matematicas&catid=100:departamentos&Itemid=103";s:5:"title";s:29:"Departamento de Matemáticas.";s:11:"description";s:16178:"CURSO 2014/15 PROGRAMACIÓN DE MATEMÁTICAS, CURSO 2014/15. 12/10/14 25/04/2015: ACTUALIZADAS LAS FECHAS DE LAS PRUEBAS FINALES de pendientes de cursos anteriores DE ESO. VER ENLACE. ENLACES DIRECTOS A LAS SIGUIENTES SECCIONES: 2ºEso 1º BACH. MAT.I 1º BACH. MAT. CC.SS I 2º BACH. MAT. II 2º BACH. MAT. CC.SS II 2º BACH. ESTADÍSTICA PENDIENTES DE TODOS LOS CURSOS UN BUEN bLOG DE MATEMÁTICAS novedad: este curso 2014/15 contará con un grupo de estadística en 2º bachillerato cns.underline;"> CURSO 2014/15 IMPORTANCIA DE ELEGIR ESTADÍSTICA COMO OPTATIVA EN 2º BACHILLERATO CIENCIAS El alumnado de Ciencias puede subir la nota de Selectividad en la Fase Específica mediante la realización de pruebas de distintas materias. En particular las Matemáticas II suele puntuar con una ponderación de 0,2 en la mayoría de los Grados de Ciencias (subir hasta 2 puntos la nota de la Fase General). El problema es que muchos alumnos se examinan de Matemáticas II en la Fase General al no disponer de otra materia mejor, según sus intereses. Es por ello que el Departamento de Matemáticas informa que este inconveniente se puede subsanar si el alumnado se examinara de Matemáticas CC SS II en la Fase General y de Matemáticas II en la Fase Específica. Como quiera que este alumnado de Ciencias sólo ha cursado la asignatura de Matemáticas II, existe una posibilidad de que completen los conocimientos que les faltan para poder dominar la materia de Matemáticas CC SS II mediante la asignatura de Estadística, que se puede elegir como optativa en 2º de Bachillerato Ciencias. Dada la importancia de obtener una buena nota final en Selectividad es por lo que el Departamento de Matemáticas anima al alumnado que actualmente está en fase de matriculación para que seleccione Estadística comostyle="color: #2a2a2a; font-size: 13px; text-transform: none;"> optativa . Si se necesita más información se puede consultar a cualquier miembro del Departamento, o escribiendo un mensaje a iescervantesgranada[arroba]gmail.com que se le haría llegar a los mismos. A CONTINUACIÓN SE PUEDEN VER LOS DISTINTOS GRADOS QUE SE IMPARTEN EN LA UNIVERSIDAD DE GRANADA EN LOS QUE SE CURSA UNA ASIGNATURA DE ESTADÍSTICA: Diplomado en Enfermería Bioestadística15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;"> Diplomado en Fisioterapia Bioestadística Diplomado en Gestión y Administración Pública (Melilla) Estadística II Diplomado en Relaciones Laborales Estadística Estadística Asistida por Ordenador Licenciado en Biologíacolor: #678925;"> Bioestadística Fundamentos de Biología Aplicada I Licenciado en Ciencias de la Actividad Física y el Deporte Estadística Estadística Aplicada a la Actividad Física Licenciado en Ciencias y Técnicas Estadísticas Bioestadística Licenciado en Documentación Estadística30px;"> Licenciado en Economía Predicción Económica y Empresarial Licenciado en Farmacia Matemática Aplicada Licenciado en Medicina Análisis Estadístico con Ordenador de Datos Médicos Bioestadística Licenciado en Odontología Estadística AplicadaArial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;"> Licenciado en Sociología Análisis Multivariante PENDIENTES DE MATEMÁTICAS PARA SEPTIEMBRE DE 2014 PENDIENTES DE 1º ESO ·Roman';"> EJERCICIOS PARA RESOLVER Y ENTREGAR TEMA 1 . Números naturales TEMA 2. Divisibilidad TEMA 3. Fracciones TEMA 4. Números decimales TEMA 5. Números enteros TEMA 6. Iniciación al álgebra ·Roman'; color: #414141; mso-fareast-language: ES;"> EJERCICIOS PARA RESOLVER Y ENTREGAR TEMA 7. Sistema Métrico Decimal TEMA 8. Proporcionalidad numérica TEMA 9. Ángulos, circunferencias y círculos TEMA 10. Polígonos TEMA 11. Funciones y gráficas EJERCICIOS RESUELTOS PARA PRACTICAR: UNIDAD 1 .href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"> UNIDAD 2 . UNIDAD 3 . UNIDAD 4 . UNIDAD 5 . UNIDAD 6 UNIDAD 7 . UNIDAD 8 . UNIDAD 9 . UNIDAD 10 . UNIDAD 11class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"> PENDIENTES DE 2º ESO · EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 1. Números enteros UNIDAD 2. Fracciones UNIDADsans-serif;"> 3. Números decimales UNIDAD 4. Sistema sexagesimal UNIDAD 5. Expresiones algregráicas UNIDAD 6. Ecu acioens de primer y segundo grado ·target="_blank"> EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 7. Sistemas de ecuaciones UNIDAD 8. Proporcionalidad numérica UNIDAD 9. Proporcionalidad geométrica UNIDAD 10. Figuras planas. Áreas UNIDAD 11. Cuerpos geométricosclass="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"> EJERCICIOS RESUELTOS PARA PRACTICAR: UNIDAD 1 UNIDAD 2 UNIDADES 3 Y 4 UNIDAD 5 UNIDAD 6href="archivos_ies/13_14/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank"> UNIDAD 7 UNIDAD 8 UNIDAD 9 Y 10 UNIDAD 11 PENDIENTES DE 3º ESOmso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;"> · EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 1. Números racionales UNIDAD 2. Números reales UNIDAD 3. Polinomios UNIDAD 4. Ecuaciones de primer y segundo grado UNIDAD 5. Sistemas de ecuaciones UNIDAD 6. Proporcionalidad numérica ·Roman'; color: #414141; mso-fareast-language: ES;"> EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 7. Progresiones UNIDAD 8. Lugares geométricos.Figuras planas UNIDAD 9. Cuerpos geométricos UNIDAD 10. Movimientos y semejanza UNIDAD 11. Funciones UNIDAD 12. Funciones lineales y afines EJERCICIOS RESUELTOS PARA PRACTICAR:.75pt;"> UNIDADES 1 Y 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDAD 6 UNIDAD: 7target="_blank"> UNIDAD: 8 UNIDAD 9 UNIDAD 10 UNIDAD 11 UNIDAD 12 PENDIENTES DE 1º BACH. MAT. CN.border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="197"> · EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 1: Números Reales UNIDAD 2: Ecuaciones, inecuaciones y sistemas UNIDAD 3: Trigonometría UNIDAD 4: Números Complejos UNIDAD 5: Geometría analítica ·'Times New Roman';"> EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 7: Funciones UNIDAD 8: Funciones elementales UNIDAD 9: Límite de una función UNIDAD 10: Derivada de una función UNIDAD 11: Integrales EJERCICIOS RESUELTOS PARA PRACTICAR: UNIDAD: 1class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"> UNIDAD: 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDADES: 7 Y 8 UNIDAD9 UNIDAD 10 UNIDAD 11 PENDIENTES DE 1º BACH. MAT. CCSS. ·href="archivos_ies/13_14/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"> EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 1: Números Reales UNIDAD 3: Polinomios y fracciones algebraicas UNIDAD 4: Ecuaciones, inecuaciones y sistemas ·style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;"> EJERCICIOS PARA RESOLVER Y ENTREGAR UNIDAD 5: Funciones UNIDAD 6: Funciones elementales UNIDAD 7: Límite de una función UNIDAD 8: Derivada de una función UNIDAD 11: Probabilidad EJERCICIOS RESUELTOS PARA PRACTICAR: UNIDAD 10.0001pt; text-align: center;" align="center"> UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDAD 6 UNIDAD 7 UNIDAD 80.0001pt; text-align: center;" align="center"> UNIDAD 11 LIBROS DE TEXTO DEL DEPARTAMENTO DE MATEMÁTICAS, CURSO 2014/15: CURSO Materia30px;"> Editorial EAN 1º ESO Matemáticas Santillana Grazalema, Sl 97884830525700.0001pt; padding-left: 30px;"> 2º ESO Matemáticas Santillana Grazalema, Sl 9788429438215 3º ESOborder-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142"> Matemáticas Santillana Grazalema, Sl 9788483052587 4º ESO Matemáticas Astyle="margin-bottom: 0.0001pt; padding-left: 30px;"> Santillana Grazalema, Sl 9788483052112 4º ESO Matemáticas B Santillana Grazalema, Sl 9788483052242style="margin-bottom: 0.0001pt; padding-left: 30px;"> 1º BACHILLERATO: Matemáticas I Matemáticas I Santillana La casa del saber (González y otros)border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142"> 1º BACHILLERATO: Matemáticas CCSS I Matemáticas aplicadas a las CCSS I Santillana. La casa del Saber (Escoredo y otros) 2º BACHILLERATO:30px;"> Matemáticas II Matemáticas II Santillana. Matemáticas II. La casa del saber (González y otros) 2º BACHILLERATO: Matemáticas CCSS IIborder-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142"> Matemáticas aplicadas a las CCSS II Santillana. Matemáticas aplicadas a las CCSS II. La casa del saber (Escoredo y otros) CURSO 2013/14 Primera reunión de Coordinación para Selectividad de Matemáticas CC SS IItext-transform: none; color: #2a2a2a;"> Ver enlace Primera reunión de Coordinación para Selectividad de Matemáticas II Ver enlace 20/12/13: Los desafíos matemáticos vuelven a esta casa por Navidad. Como ya sucedió el año pasado, con motivo del sorteo de la Lotería de Navidad ofrecemos un nuevo problema. El encargado de presentar el Desafío Extraordinario de Navidad de 2013 es Javier Cilleruelo, profesor de la Universidad Autónoma de Madrid y miembro del Instituto de Ciencias Matemáticas (ICMAT). Entre los acertantes se sorteará una biblioteca matemática como la que ofreció EL PAÍS en el quiosco durante 2011. El ganador recibirá, además, el libro 'Desafíos Matemáticos' por cortesía de la Real Sociedad Matemática Española ,una publicación de SM en la que se recogen los 40 desafíos matemáticos que ofrecimos en la web y con los que EL PAÍS y la RSME celebraron el centenario de esta institución hace dos años. Manda tu respuesta antes de las 00.00 horas del domingo 22 de diciembre (la medianoche del sábado al domingo, hora peninsular española) a problemamatematicas@gmail.com y participa en el sorteo. A continuación, para aclarar dudas y en atención a nuestros lectores sordos, añadimos el enunciado del problema por escrito: El equipo que preparamos los desafíos matemáticos hemos decidido abonarnos durante todo el año a un número de la Lotería. Para elegir ese número, que debe estar comprendido entre el 0 y el 99.999, pusimos como condición que tuviese las cinco cifras distintas y que, además, cumpliese alguna otra propiedad interesante. Finalmente hemos conseguido un número que tiene la siguiente propiedad: si numeramos los meses del año del 1 al 12, en cualquier mes del año ocurre que al restar a nuestro número de lotería el número del mes anterior, el resultado es divisible por el número del mes en el que estemos. Y esto sucede para cada uno de los meses del año. Es decir, si llamamos L a nuestro número, tenemos por ejemplo que en marzo L-2 es divisible entre 3 y en diciembre L-11 es divisible entre 12. El reto que os planteamos es que nos digáis a qué número de Lotería estamos abonados y que nos expliquéis cómo lo habéis encontrado. OBSERVACIONES IMPORTANTES. Insistimos en que es importante que hagáis llegar junto con el número el razonamiento de cómo lo habéis hallado. No se considerarán válidas las respuestas que den sólo el número o que lo hayan encontrado probando todos uno a uno (a mano o con un ordenador).30px;" /> PROGRAMACIÓN DE MATEMÁTICAS, CURSO 2013/14. FECHAS DE EXÁMENES: PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES. Directrices y orientaciones Selectividad Mat. CC.SS. II curso 2013/14. 12/11/2012: Coordinación de Selectividad de Mat. CC SS II del 05/11/12.. 09/11/2012: Examen y soluciones de Selectividad de MAT_CC_SS_II de sept. 2012. 20/06/2012: Exámen y soluciones de Selectividad de MAT_CC_SS_II de jun. 2012,Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; padding: 0px 0px 0px 30px;"> Criterios de evaluación de Matemáticas 1º ESO. Criterios de evaluación de Matemáticas 2º ESO. Criterios de evaluación de Matemáticasstyle="font-size: 11px;"> 3º ESO. Criterios de evaluación de Matemáticas 4º ESO. Criterios de evaluación de Matemáticas 1º Bachillerato CCSS I. Criterios de evaluación deMatemáticas I 1 º Bachillerato CNAT. Criterios de evaluación de Matemáticas 2 º Bachillerato CCSS II. Criterios de evaluación de Matemáticas II 2 ºBachillerato CNAT . ENLACE A ZONA DE 2º BACH. CC.SS. II. ENLACE A ZONA DE 2º BACH. CC.NN. II. PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES: Pendientes de 1º ESO EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 17/01/2014 TEMA 1 . Números naturales TEMA 2. Divisibilidad TEMA 3. Fraccionesstyle="padding-left: 30px;"> TEMA 4. Números decimales TEMA 5. Números enteros TEMA 6. Iniciación al álgebra EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 28/03/2014 TEMA 7. Sistema Métrico Decimal TEMA 8. Proporcionalidad numérica TEMA 9. Ángulos, circunferencias y círculos TEMA 10. Polígonos TEMA 11. Funciones y gráficas EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"> UNIDAD 1 UNIDAD 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDAD 6 EXAMEN 2 UNIDAD 7 UNIDAD 8 UNIDAD 9style="padding-left: 30px;"> UNIDAD 10 UNIDAD 11 Pendientes de 2º ESO EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 17/01/2014 UNIDAD 1. Números enteros UNIDAD 2. Fracciones UNIDAD 3. Números decimales UNIDAD 4. Sistema sexagesimal UNIDAD 5. Expresiones algregráicas UNIDAD 6. Ecu acioensde primer y segundo grado EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 28/03/2014 UNIDAD 7. Sistemas de ecuaciones UNIDAD 8. Proporcionalidad numérica UNIDAD 9. Proporcionalidad geométrica UNIDAD 10. Figuras planas. Áreas UNIDAD 11. Cuerpos geométricos EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD1 UNIDAD 2 UNIDADES 3 Y 4 UNIDAD 5 UNIDAD 6 EXAMEN 2 UNIDAD 7 UNIDAD 8 UNIDAD 9 Y 10 UNIDAD 11padding-left: 30px;"> Pendientes de 3º ESO EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 17/01/2014 UNIDAD 1. Números racionales UNIDAD 2. Números reales UNIDAD 3. Polinomios UNIDAD 4. Ecuaciones de primer y segundo grado UNIDAD 5. Sistemas de ecuaciones UNIDAD 6. Proporcionalidad numérica EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 28/03/2014 UNIDAD 7. Progresiones UNIDAD 8. Lugares geométricos. Figuras planas UNIDAD 9. Cuerpos geométricos .30px;"> UNIDAD 10. Movimientos y semejanza UNIDAD 11. Funciones UNIDAD 12. Funciones lineales y afines EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDADES 1 Y 2 UNIDAD 3 UNIDAD 4 UNIDAD 5 UNIDAD 6 EXAMEN 2 UNIDAD: 7padding-left: 30px;"> UNIDAD: 8 UNIDAD 9 UNIDAD 10 UNIDAD 11 UNIDAD 12 Pendientes de 1º BACH. MAT. CN. EJERCICIOS PARA RESOLVER Y ENTREGAR PRIMER EXAMEN: 17/01/2014 UNIDAD 1: Números Reales UNIDAD 2: Ecuaciones, inecuaciones y sistemas UNIDAD 3: Trigonometríastyle="padding-left: 30px;"> UNIDAD 4: Números Complejos UNIDAD 5: Geometría analítica EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 28/03/2014 UNIDAD 7: Funciones UNIDAD 8: Funciones elementales UNIDAD 9: Límite de una función. UNIDAD 10: Derivada de una función UNIDAD 11: Integrales EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1 UNIDAD: 1 UNIDAD: 2 UNIDAD3 UNIDAD 4 UNIDAD 5 EXAMEN 2 UNIDADES: 7 Y 8 UNIDAD 9 UNIDAD 10 UNIDAD 11 Pendientes de 1º BACH. MAT. CCSS. EJERCICIOS PARA RESOLVER Y ENTREGAR30px;"> PRIMER EXAMEN: 17/01/2014 UNIDAD 1: Números Reales UNIDAD 3: Polinomios y fracciones algebraicas UNIDAD 4: Ecuaciones, inecuaciones y sistemas EJERCICIOS PARA RESOLVER Y ENTREGAR SEGUNDO EXAMEN: 28/03/2014 UNIDAD 5: Funciones UNIDAD 6: Funciones elementales UNIDAD 7: Límite de una función UNIDAD 8: Derivada de una función UNIDAD 11: Probabilidad EJERCICIOS RESUELTOS PARA PRACTICAR: EXAMEN 1/> UNIDAD 1 UNIDAD 3 UNIDAD 4 EXAMEN 2 UNIDAD 5 UNIDAD 6 UNIDAD 7 UNIDAD 8 UNIDAD 11style="color: #2a2a2a; font-weight: normal;"> Página personal del profesor: Gerardo Bustos: entrar. Código qr del Dep. de Matemáticas. Ultima actualización: 10/11/2013";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2011-01-12 00:03:45";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2011-01-09 12:03:45";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:11;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"274";s:5:"alias";s:5:"lomce";s:7:"summary";s:8273:"<p><span style="color: #222222; font-family: arial, sans-serif;"><span style="color: #222222; font-family: arial, sans-serif; font-weight: normal; text-align: start;"><span style="text-decoration: underline;"><strong>ÚLTIMA HORA: NORMATIVA ESTATAL.</strong></span><br /><a href="index.php?option=com_content&amp;view=article&amp;id=274" target="_blank">BOE: RD 1105/2014, establece currÍculum básico de ESO y Bachillerato, adaptado a la LOMCE.</a><br /><a href="archivos_ies/14_15/BOE-A-2015-738.pdf" target="_blank">BOE: <span style="color: #000000; font-family: Helvetica, Arial, sans-serif, Verdana; font-size: 13.2800006866455px; line-height: 16.6000003814697px;">Orden ECD/65/2015, se describen las relaciones entre las competencias, los contenidos y los criterios de evaluación de EP, ESO y Bachillerato.</span></a><br />Falta que la </span><span style="color: #222222; font-family: arial, sans-serif; font-weight: normal; text-align: left;">legislación andaluza se adapte a este RD. VER -&gt;  </span><a href="http://www.adideandalucia.es/normas/proyectos/BorradorInstruccionesSecundaria_feb2015.pdf" target="_blank" style="font-size: 13px; color: #005fa9;">Proyecto de INSTRUCCIONES</a><span style="font-size: 13px; text-align: left; color: #414141;"> </span><span style="font-size: 13px; text-align: left; color: #414141;">de la Secretaría General de Educación de la Consejería de Educación, Cultura y Deporte, sobre la ordenación educativa de la Educación Secundaria Obligatoria y Bachillerato y otras consideraciones para el curso escolar 2015-2016 (Borrador de 16/02/2015).</span></span></p>
<p><span style="color: #222222; font-family: arial, sans-serif;"><span style="font-size: 13px; text-align: left; color: #414141;">_______________________________________________________</span></span></p>
<p><span style="color: #222222; font-family: arial, sans-serif;"> </span></p>
<p><span style="color: #222222; font-family: arial, sans-serif;">El sábado 3 de enero se publicó en el BOE el Real Decreto 1105/2014, que establece el curriculum básico de la ESO y del Bachillerato, adaptado a la LOMCE. Es de suponer que la legislación andaluza se adapte a este RD, aunque no se sabe cuándo se publicará esta legislación autonómica.</span></p>
<div style="color: #222222; font-family: arial, sans-serif;"><span style="text-decoration: underline;"><strong>Algunos comentarios de carácter general:</strong></span></div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- Se redefinen las competencias básicas en el artículo 2. Las nuevas competencias son:</div>
<div style="color: #222222; font-family: arial, sans-serif;"><ol>
<li style="margin-left: 15px;">Comunicación lingüística.</li>
<li style="margin-left: 15px;">Competencia matemática y competencias básicas en ciencia y tecnología.</li>
<li style="margin-left: 15px;">Competencia digital.</li>
<li style="margin-left: 15px;">Aprender a aprender.</li>
<li style="margin-left: 15px;">Competencias sociales y cívicas.</li>
<li style="margin-left: 15px;">Sentido de iniciativa y espíritu emprendedor.</li>
<li style="margin-left: 15px;">Conciencia y expresiones culturales.</li>
</ol></div>
<div style="color: #222222; font-family: arial, sans-serif;">- La estructura de las materias por nivel es la que ya establecía la LOMCE, así por ejemplo se habla de asignaturas troncales, específicas y de libre configuración. En el anexo I del RD se recoge el curriculum básico de las materias troncales, en el anexo II el de las materias específicas.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- Se definen como elementos transversales, que se trabajarán en todas las materias la compresión lectora, la expresión oral, la expresión escrita, la comunicación audiovisual y las TIC.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- El capítulo II trata de la ESO, con la organización de materias de la LOMCE, especifado en los artículos 13 y 14 de este RD. Son importantes los artículos 21 y 22, que tratan de la famosa reválida de la ESO y de los mecanismos de promoción respectivamente. Hay una novedad interesante: Los alumnos repetirán curso cuando suspendan más de dos materias o si suspenden Lengua y Matemáticas simultáneamente. Como ya sucedía los alumnos sólo pueden repetir el mismo curso una vez, y pueden repetir dos veces en la ESO. La edad límite de permanencia en ESO es de 19 años cumplidos dentro del curso académico.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- El capítulo III trata sobre el Bachillerato, cuya estructura desgrana en los artículos 26,27,28. El artículo 31 define la reválida para la obtención del título de Bachillerato. En cuanto a la promoción, teniendo en cuenta que hay un plazo de cuatro años para hacer los dos cursos, sólo puede repetirse una vez cada nivel salvo casos excepcionales que determinará el equipo educativo. En cuanto a la continuidad entre materias, para cursar determinadas materias de 2º será necesario haber impartido la correspondiente de 1º (p.e. para hacer Matemáticas II tiene que haber cursado Matemáticas I), si bien el profesorado que imparte la materia de 2º podrá permitir saltarse esta norma si el alumno tiene posibilidades de hacer la asignatura con aprovechamiento.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- No hay novedades significativas en lo tocante a documentos oficiales de evaluación (actas, expedientes académicos etc.).</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- Quedan derogados los Reales Decretos 1631/2006 (de la ESO) y 1467/2007 (del Bachillerato).</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;"><span style="color: #ff0000;">CALENDARIO DE IMPLANTACIÓN:</span></div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">CURSO 2015/2016:     1º Y 3º DE ESO. 1º DE BACHILLERATO.</div>
<div style="color: #222222; font-family: arial, sans-serif;"><span style="color: #ff0000;">_____________________________________________________________________________________________</span></div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">CURSO 2016/2017:     2º Y 4º DE ESO. 2º DE BACHILLERATO.</div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 </div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 PRIMERA REVÁLIDA DE ESO. SIN EFECTOS ACADÉMICOS.</div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 </div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 PRIMERA REVÁLIDA DE BACHILLERATO. CON EFECTOS PARA EL INGRESO A LA UNIVERSIDAD,</div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 PERO SIN EFECTOS PARA LA OBTENCIÓN DEL TÍTULO DE BACHILLER.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">ENTRADA EN VIGOR DEL REAL DECRETO: A PARTIR DEL 3 DE ENERO DE 2015.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;"><a href="archivos_ies/14_15/RD1105-2014CurriculoSecundaria.pdf" target="_blank">VER EL REAL DECRETO ÍNTEGRO --&gt;<br /><br /><br /></a>___________________________________________________</div>";s:4:"body";s:0:"";s:5:"catid";s:3:"143";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2015-03-13 16:52:43";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:2:"10";s:8:"ordering";s:1:"0";s:8:"category";s:9:"NOVEDADES";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:9:"274:lomce";s:7:"catslug";s:13:"143:novedades";s:6:"author";s:10:"Super User";s:6:"layout";s:7:"article";s:4:"path";s:74:"index.php?option=com_content&view=article&id=274:lomce&catid=143:novedades";s:10:"metaauthor";N;s:6:"weight";d:0.51331000000000004;s:7:"link_id";i:1681;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:9:"NOVEDADES";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:9:"NOVEDADES";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=274";s:5:"route";s:74:"index.php?option=com_content&view=article&id=274:lomce&catid=143:novedades";s:5:"title";s:5:"LOMCE";s:11:"description";s:4069:"ÚLTIMA HORA: NORMATIVA ESTATAL. BOE: RD 1105/2014, establece currÍculum básico de ESO y Bachillerato, adaptado a la LOMCE. BOE: Orden ECD/65/2015, se describen las relaciones entre las competencias, los contenidos y los criterios de evaluación de EP, ESO y Bachillerato. Falta que la legislación andaluza se adapte a este RD. VER -> Proyecto de INSTRUCCIONES de la Secretaría General de Educación de la Consejería de Educación, Cultura y Deporte, sobre la ordenación educativa de la Educación Secundaria Obligatoria y Bachillerato y otras consideraciones para el curso escolar 2015-2016 (Borrador de 16/02/2015). _______________________________________________________ El sábado 3 de enero se publicó en el BOE el Real Decreto 1105/2014, queestablece el curriculum básico de la ESO y del Bachillerato, adaptado a la LOMCE. Es de suponer que la legislación andaluza se adapte a este RD, aunque no se sabe cuándo se publicará esta legislación autonómica. Algunos comentarios de carácter general: - Se redefinen las competencias básicas en el artículo 2. Las nuevas competencias son: Comunicación lingüística. Competencia matemática y competencias básicas en ciencia y tecnología. Competencia digital. Aprender a aprender. Competencias sociales y cívicas. Sentido de iniciativa y espíritu emprendedor. Conciencia y expresiones culturales. - La estructura de las materias por nivel es la que ya establecía la LOMCE, así por ejemplo se habla de asignaturas troncales, específicas y de libre configuración. En el anexo I del RD se recoge el curriculum básico de las materias troncales, en el anexo II el de las materias específicas. - Se definen como elementos transversales, que se trabajarán en todas las materias la compresión lectora, la expresión oral, la expresión escrita, la comunicación audiovisual y las TIC.arial, sans-serif;"> - El capítulo II trata de la ESO, con la organización de materias de la LOMCE, especifado en los artículos 13 y 14 de este RD. Son importantes los artículos 21 y 22, que tratan de la famosa reválida de la ESO y de los mecanismos de promoción respectivamente. Hay una novedad interesante: Los alumnos repetirán curso cuando suspendan más de dos materias o si suspenden Lengua y Matemáticas simultáneamente. Como ya sucedía los alumnos sólo pueden repetir el mismo curso una vez, y pueden repetir dos veces en la ESO. La edad límite de permanencia en ESO es de 19 años cumplidos dentro del curso académico. - El capítulo III trata sobre el Bachillerato, cuya estructura desgrana en los artículos 26,27,28. El artículo 31 define la reválida para la obtención del título de Bachillerato. En cuanto a la promoción, teniendo en cuenta que hay un plazo de cuatro años para hacer los dos cursos, sólo puede repetirse una vez cada nivel salvo casos excepcionales que determinará el equipo educativo. En cuanto a la continuidad entre materias, para cursar determinadas materias de 2º será necesario haber impartido la correspondiente de 1º (p.e. para hacer Matemáticas II tiene que haber cursado Matemáticas I), si bien el profesorado que imparte la materia de 2º podrá permitir saltarse esta norma si el alumno tiene posibilidades de hacer la asignatura con aprovechamiento. - No hay novedades significativas en lo tocante a documentos oficiales de evaluación (actas, expedientes académicos etc.). - Quedan derogados los Reales Decretos 1631/2006 (de la ESO) y 1467/2007 (delBachillerato). CALENDARIO DE IMPLANTACIÓN: CURSO 2015/2016: 1º Y 3º DE ESO. 1º DE BACHILLERATO. _____________________________________________________________________________________________ CURSO 2016/2017: 2º Y 4º DE ESO. 2º DE BACHILLERATO. PRIMERA REVÁLIDA DE ESO. SIN EFECTOS ACADÉMICOS. PRIMERA REVÁLIDA DE BACHILLERATO. CON EFECTOS PARA EL INGRESO A LA UNIVERSIDAD, PERO SIN EFECTOS PARA LA OBTENCIÓN DEL TÍTULO DE BACHILLER. ENTRADA EN VIGOR DEL REAL DECRETO: A PARTIR DEL 3 DE ENERO DE 2015. VER EL REALDECRETO ÍNTEGRO --> ___________________________________________________";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2015-01-06 19:38:44";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2015-01-06 19:38:44";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:12;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:26:{s:2:"id";s:3:"219";s:5:"alias";s:25:"ies-miguel-de-cervantes-2";s:7:"summary";s:34726:"<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Pendientes de Matemáticas.</span></h2>
<p style="padding-left: 30px;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;"><a href="archivos_ies/curso1314/HORARIO_INICIO_CURSO_2013-14.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></span></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Historia de Grecia, 1º de Bachillerato (23/09/13)</span></h2>
<p style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff; padding-left: 30px;"><a href="https://www.youtube.com/watch?v=cyvNgDMZEdw" target="_blank" style="color: #1155cc;">Ver vídeo</a></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Actualizado el temario de Matemáticas de 1º y 3º ESO bilingües para poder imprimirlo.</span></h2>
<p style="padding-left: 30px; color: #2e2a23; text-align: justify; background-color: #fbe9c5;"><a href="index.php?option=com_content&amp;view=article&amp;id=171&amp;Itemid=239" target="_blank" style="color: #1155cc;">Entrar en zona privada de bilingües.</a></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Horario de inicio de curso, día 16/09/13</span></h2>
<p style="margin: 0cm 0cm 6pt; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><a href="archivos_ies/curso1314/HORARIO_INICIO_CURSO_2013-14.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Decreto Formación Inicial y Permanente</span></h2>
<p style="margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff; line-height: 12pt;" align="left">Decreto 93/2013, de 27 de agosto, por el que se regula la formación inicial y permanente del profesorado en la Comunidad Autónoma de Andalucía, así como el Sistema Andaluz de Formación Permanente del Profesorado.</p>
<p style="margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff; line-height: 12pt;" align="left"><a href="archivos_ies/13_14/ver_decreto_39827.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></p>
<p style="margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff; line-height: 12pt;" align="left"> </p>
<p style="margin: 0cm 0cm 0.0001pt; text-align: justify; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><strong><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;">Orden de 5 de julio de 2013, que regula las ausencias del profesorado por enfermedad o accidente que no den lugar a incapacidad temporal</span></strong></p>
<p style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"> </p>
<p style="margin: 0cm 0cm 0.0001pt; padding-left: 30px; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><a href="archivos_ies/13_14/ver_orden_90644.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></p>
<p style="margin: 0cm 0cm 0.0001pt; padding-left: 30px; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><strong style="text-align: justify;"><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;"> </span></strong></p>
<p style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><strong style="text-align: justify;"><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;">Día lectivos y festivos en el Curso Escolar 2013/14</span></strong></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><a href="archivos_ies/13_14/Calendario_2013-2014.pdf" target="_blank" style="color: #1155cc;"><br />Ver enlace</a></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"> </p>
<p style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><strong style="text-align: justify;"><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;">Legislación actualizada a 05/09/13</span></strong></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><a href="archivos_ies/13_14/lex" target="_blank" style="color: #1155cc;"><br />Ver enlace</a></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"> </p>
<table style="border: 0px solid #efbc38; width: 100%; background-color: #ffffff;" border="0" cellspacing="5" cellpadding="5" align="center">
<tbody>
<tr style="border-color: #8799f9; border-bottom: 1px solid;" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="3" align="center"><span style="color: #ffffff;"><strong><span style="font-size: 12pt;">CURSO </span></strong><strong><span style="font-size: 12pt;"> 2012/13</span></strong></span></td>
</tr>
<tr>
<td class="textogris" style="text-align: center; border-color: #8799f9;" colspan="2">
<ul>
<li style="text-align: justify;"><span style="color: #484137;"><strong>20/02/13.  <a href="http://paleorama.wordpress.com/2013/02/19/entrevista-al-filologo-antoni-biosca-sobre-la-importancia-del-latin/" target="_blank">Entrevista al filólogo Antoni Biosca sobre la importancia del Latín.</a></strong></span></li>
<li style="text-align: justify;"><span style="color: #484137;"><strong>07/02/13.  <a href="index.php?option=com_content&amp;view=article&amp;id=193">Publicada nueva normativa en materia de permisos y licencias.</a></strong></span></li>
<li style="text-align: justify;"><strong>07/02/13:  Excelente web para <a href="index.php?option=com_content&amp;view=article&amp;id=194" target="_self">aprender jugando</a>, todas las asignaturas, todos los niveles.</strong></li>
<li style="text-align: justify;"><span style="color: #484137;"><strong>05/02/13.  </strong></span>Hoy, 5 de febrero, se celebra el <a href="http://www.osi.es/es/actualidad/blog/2013/02/05/conectate-y-respeta-netiquetate" target="_blank">Día Internacional de la Internet Segura. </a></li>
<li style="text-align: justify;"><span style="color: #484137;"><strong>04/02/13.   </strong></span><a href="archivos_ies/1_Ev_Analisiscuantitativo_2012_13.pdf" target="_blank"><strong>Datos, 1ª Evaluación 2012/13.</strong></a></li>
<li style="text-align: justify;"><span><strong>16/01/13. </strong></span><strong><a href="archivos_ies/PROGRAMA_ACE_POR_TRIMESTRES.xlsx" target="_blank" style="color: #655c4e;">Programa de las Actividades Complementarias y Extraescolares por trimestres.</a></strong></li>
<li style="text-align: justify;"><span style="text-decoration: underline;"><strong><span style="color: #0066cc; text-decoration: underline;">08/01/13.</span></strong></span><span style="color: #0066cc;">  </span><span style="text-decoration: underline;"><strong><span style="color: #0066cc; text-decoration: underline;">REANUDACIÓN</span><span style="color: #0066cc; text-decoration: underline;"> DE LAS CLASES EN SU HORARIO NORMAL, A PARTIR DE LAS 08.15.</span></strong></span></li>
<li style="text-align: justify;"></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">13/12/12.  </span></strong><strong><span style="color: #0066cc;"><a href="archivos_ies/TENSION_EN_LAS_AULAS.pdf">TENSION EN LAS AULAS.</a> Aportación del Departamento de Latín y Griego.</span></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">07/11/12. </span></strong><span style="color: #0066cc;"><strong><a href="archivos_ies/ENSENANZA_Y_CONOCIMIENTO.doc" target="_blank">ENSEÑANZA Y CONOCIMIENTO.</a> Aportación del Departamento de Latín y Griego.</strong></span></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">23/10/12. </span><a href="http://www.piiisa.es/" target="_blank">PROYECTO PIIISA 2012: Ciencia para jóvenes estudiantes  www.piiisa.es</a></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">16/09/12. <a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_examenes_anteriores.php" target="_blank">ORIENTACIONES OFICIALES DE LA UNIVERSIDAD SELECTIVIDAD 2012/13.</a></span></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">15/09/12</span></strong>. <a href="archivos_ies/lex/" target="_blank">LEGISLACIÓN.</a></li>
<li style="text-align: left;"><strong style="color: #339966; text-align: justify;">11/09/12.</strong> <a href="archivos_ies/Manual_funcionamiento_SGD.pdf" target="_blank"><strong style="color: #339966; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: justify;">Manual de funcionamiento de la unidad personal de SGD</strong>.</a> (Para el profesorado)</li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">29/06/12.  <a href="archivos_ies/libros_1_bach_2012-2013corregido.pdf" target="_blank" style="text-decoration: none; color: #0066cc;">Libros de 1º Bachillerato para el curso 2012/13.</a></span></strong></li>
<li style="text-align: justify;"><strong><strong><span style="color: #0066cc;">29/06/12.  <span style="color: #0066cc;"><a href="archivos_ies/libros_2_bach_2012-2013.pdf" style="text-decoration: none; color: #0066cc;">Libros de 2º Bachillerato para el curso 2012/13.</a></span></span></strong> </strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">25/06/12.</span> <a href="archivos_ies/Calendario_escolar_2012_13.pdf" target="_blank" style="text-decoration: none;"><span style="color: #0066cc;">Calendario Escolar 2012/13.</span></a></strong></li>
<li style="text-align: justify;"><strong><strong><span style="color: #0066cc;">25/06/12. </span> <a href="archivos_ies/Instrucciones_calendario_escolar_2012_13.pdf" target="_blank" style="text-decoration: none;"><span style="color: #0066cc;">Instrucciones del Calendario Escolar 2012/13.</span></a></strong> </strong></li>
<li style="text-align: justify;"><strong>22/06/12. </strong><a href="archivos_ies/Examenes_selectividad_junio_2012_oficiales.pdf" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Exámenes que han salido en Selectividad junio 2012.</a></li>
<li style="text-align: justify;"><a href="archivos_ies/PROYECTO_EDUCATIVO_IES_MIGUEL_DE_CERVANTES_definitivo_2011_12.pdf" target="_blank" title="PROYECTO EDUCATIVO IES MIGUEL DE CERVANTES 2011-12." style="text-decoration: none; color: #c6c37b; text-align: center;">ACTUALIZADO: PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES 2011-12.</a></li>
<li style="text-align: justify;"><span style="font-size: 10pt;"><strong>Consulta de los libros de la Biblioteca:   <a href="abiex/index.php" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Formato 1</a> -   <a href="tmp/index.php" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Formato 2</a> - </strong><a href="libros/index.php" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Formato 3</a></span></li>
<li style="text-align: justify;"><span style="font-size: 10pt;"><strong>Enlaces a <a href="index.php?option=com_content&amp;view=article&amp;id=153&amp;Itemid=231" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">bibliotecas y libros virtuales e interactivos.</a></strong></span></li>
<li style="text-align: justify;"><span style="font-size: 10pt;"><strong>Actualizados los criterios de evaluación, consultar los Departamentos.</strong></span></li>
<li style="text-align: justify;"><span style="font-size: 10pt;">Criterios que falten de 2º de Bachiller sobre Selectividad, consultar <a href="http://www.ujaen.es/" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">www.ujaen.es</a></span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<p> </p>
<table style="border: 0px solid #efbc38; width: 100%; background-color: #ffffff;" border="0" cellspacing="3" cellpadding="1" align="center">
<tbody>
<tr style="border-color: #8799f9; border-bottom: 1px solid;" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; text-align: center;" align="center"><span style="color: #ffffff;"><strong><span style="font-size: 12pt;"> Carteles sobre el invierno (Maestros y aprendices)</span></strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9;">
<td class="textogris" style="text-align: center; border-color: #8799f9;">
<div style="text-align: center;" align="center;">
<table border="0" align="center">
<tbody>
<tr>
<td>
<div style="text-align: center;" align="center;"><strong><span style="font-family: book antiqua,palatino; font-size: 12pt;">Cartel invierno</span></strong></div>
<div style="text-align: right;"><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Cartel_invierno.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Cartel_invierno.jpg" border="0" alt="Cartel invierno." width="179" height="117" /></a></span></div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Horacio (latín)</strong></span></div>
<div style="text-align: center;"><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Horacio_latin.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Horacio.jpg" border="0" alt="Horacio." width="164" height="127" /></a></span></div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong> Irene Centeno </strong></span></div>
<div style="text-align: center;"><span style="font-family: book antiqua,palatino; font-size: 12pt;"> <a href="archivos_ies/lengua/invierno/Irene_Centeno.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Irene_Centeno.jpg" border="0" alt="Irene Centeno" width="195" height="86" /></a></span></div>
<div style="text-align: center;"> </div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Paul Verlain 1</strong></span></div>
<div style="text-align: center;"><span style="text-align: center;"><a href="archivos_ies/lengua/invierno/Paul_Verlain_1.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/Paul_Verlain_1.jpg" border="0" alt="Paul Verlain 1." width="136" height="159" /></a></span></div>
<div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Paul Verlain 2</strong></span></div>
<div style="text-align: center;"><span style="text-align: center;"><a href="archivos_ies/lengua/invierno/Paul_Verlain_2.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Paul_Verlain_2.jpg" border="0" alt="Paul Verlain 2." width="197" height="79" /></a></span></div>
</div>
</td>
<td>
<div>
<div style="text-align: center;"><strong><span style="font-family: book antiqua,palatino; font-size: 12pt;">Jardín de invierno (P. Neruda)</span></strong></div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Jardin_de_invierno_P_Neruda.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Jardin_de_invierno.jpg" border="0" alt="Jardín de invierno." width="181" height="118" /></a></span></div>
</div>
<div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Maite Luzón</strong></span></div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Maite_Luzon.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/Maite_Luzon.jpg" border="0" alt="Maite Luzón." width="184" height="123" /></a></span></div>
</div>
<div><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>María Fernández</strong></span></div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Maria_Fernandez.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Maria_Fernandez.jpg" border="0" alt="Maria Fernández." width="196" height="114" /></a></span></div>
<div><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Proverbios (Francés)</strong></span></div>
<div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Proverbios _Frances.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Proverbios_Frances.jpg" border="0" alt="Proverbios (Francés)." width="183" height="111" /></a></span></div>
<div>
<div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Snow dreams (inglés)</strong></span></div>
<span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Snow_dreams_ingles.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Snow_dreams.jpg" border="0" alt="Snow dreams." width="176" height="106" /></a> </span></div>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
<div><br />
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="3" cellpadding="2" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid;" align="center" valign="middle" bgcolor="#c78d74">
<td style="text-align: center; border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">VÍDEO DEL VIAJE A BROOKLYN, NUEVA YORK, ABRIL 2011</span></strong></strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9 #8799f9 currentColor; border-bottom-width: 1px; border-bottom-style: solid;">
<td class="textogris" style="border-color: #8799f9; text-align: center;">
<div><span style="font-size: 12px;"><strong> <a href="http://www.youtube.com/watch?v=xiFxyNxAQR0" target="_blank"><img src="archivos_ies/CollageNY.jpg" border="0" alt="Vídeo del viaje a Brooklyn 2011." /></a> </strong></span><strong><img class="caption" src="images/stories/cervantes/qr_viaje_nueva_york.jpg" border="0" alt="Código qr del viaje a Nueva York. Leerlo con la cámara del móvil." title="Código qr del viaje a Nueva York. Leerlo con la cámara del móvil." style="border: 0;" /></strong></div>
</td>
</tr>
</tbody>
</table>
</div>
<div id="tituloItem" class="blockTituloItem" style="display: block;">
<div class="tituloIzq" style="float: left;">
<div><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; letter-spacing: 1pt; color: #71cd63;"> </span></div>
</div>
</div>
<div class="blockTituloItem" style="display: block;"> </div>
<div id="contenido_sitio" style="width: 100%; float: left; display: block;">
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="0" cellpadding="1" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;" align="center" valign="middle" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">DOCUMENTOS DEL PLAN DE CENTRO</span></strong> </strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9; border-bottom: 1px solid;">
<td class="textogris" style="text-align: right; border-color: #8799f9;">
<div style="text-align: left;">
<ul style="color: #151509; margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; list-style-type: none; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"><a href="archivos_ies/PROYECTO_EDUCATIVO_IES_MIGUEL_DE_CERVANTES_definitivo_2011_12.pdf" target="_blank" title="PROYECTO EDUCATIVO IES MIGUEL DE CERVANTES 2011-12." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES.</a></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PROYECTO_DE_GESTION_DEFINITIVO.pdf" target="_blank" title="PROYECTO DE GESTIÓN." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PROYECTO DE GESTIÓN.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/Plan_de_convivencia_definitivo.pdf" target="_blank" title="Plan de Convivencia." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PLAN DE CONVICENCIA.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/ROF_nuevo_2011_definitivo.pdf" target="_blank" title="ROF nuevo 2011." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">NUEVO ROF 2011.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PLAN_DE_ORIENTACION_Y_ACCION_TUTORIAL_definitivo.pdf" target="_blank" title="PLAN DE ORIENTACIÓN Y ACCIÓN." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PLAN DE ORIENTACIÓN Y ACCIÓN TUTORIAL.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PLAN_DE_FORMACION_MODELO_IES_CERVANTES_definitivo.pdf" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PLAN DE FORMACIÓN DEL IES MIGUEL DE CERVANTES.</a></span></li>
</ul>
</div>
<div style="text-align: left;"> </div>
</td>
</tr>
</tbody>
</table>
<br />
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="0" cellpadding="1" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;" align="center" valign="middle" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">ESCUELA TIC 2.0</span></strong> </strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9; border-bottom: 1px solid;">
<td class="textogris" style="text-align: right; border-color: #8799f9;">
<div style="text-align: left;">
<table style="margin: 1px; width: 100%; text-align: center; border-collapse: collapse;" border="0" cellspacing="2">
<tbody style="text-align: center;">
<tr style="text-align: center;">
<td style="vertical-align: top; text-align: center; padding: 2px; border: 0px solid #9a9542;">
<ul style="color: #129d0e; margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 2em; font-family: 'comic sans ms', sans-serif; font-size: 12px; line-height: 15px; list-style-type: none; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;">
<div style="text-align: left;"><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'Comic Sans MS'; color: #810081; font-size: 10pt;"><a href="http://blogsaverroes.juntadeandalucia.es/escuelatic20/" target="_blank" title="Escuela TIC 2.0" style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">ESCUELA TIC 2.0</a></span></strong><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><br /></strong></div>
</li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'Comic Sans MS'; color: #810081; font-size: 10pt;"><a href="http://www.juntadeandalucia.es/averroes/mochiladigitalESO/" target="_blank" title="Mochila digital." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">MOCHILA DIGITAL SECUNDARIA<br /></a></span></strong></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;">
<div style="text-align: left;"><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'Comic Sans MS'; color: #810081; font-size: 10pt;"><a href="http://www.cepgranada.org/~inicio/pf_login.php?4,60,,,,,,," target="_blank" title="Curso del CEP de Granada" style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">CURSOS CEP DE GRANADA</a></span></strong></div>
</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
<br />
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="0" cellpadding="1" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;" align="center" valign="middle" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">NOTICIAS</span></strong></strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9; border-bottom: 1px solid;">
<td class="textogris" style="text-align: right; border-color: #8799f9;">
<div style="text-align: left;"> </div>
<div style="text-align: left;">
<ul>
<li style="color: #151509;">Blog del <span style="font-size: 12px; color: #000000;"><a href="http://letsknoweachotherbetter.blogspot.com/" target="_blank">Viaje a Cumbria</a>, Inglaterra, 14 al 23 de junio de 2011.</span></li>
</ul>
<ul>
<li style="color: #151509;">Actividad extraescolar: "<a href="archivos_ies/Los_sitios_de_Mariana_de_Pineda.pdf" target="_blank">LOS SITIOS DE MARIANA PINEDA</a>", un recorrido por la ciudad de Granada: Departamento de Geografía e Historia.
<ul>
<li style="color: #151509;">26/05/2011: <a href="index.php/departamentos26/humanidesdes/dep-geografia/#mariana">añadidas 6 fotografías de la actividad.</a></li>
</ul>
</li>
<li><span style="font-family: arial, helvetica, sans-serif;">2º Bachillerato B. <a href="archivos_ies/Master2011.pdf" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Master de Secundaria y Bachillerato 2011</a>.</span></li>
<li><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><span style="text-decoration: underline;">31 de marzo de 2011</span>:  ENCUENTROS LITERARIOS EN EL INSTITUTO. <a href="index.php?option=com_content&amp;view=article&amp;id=132" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Andrés Neuman</a> estuvo con el alumnado de 2º de Bachillerato</span></li>
<li><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: 10pt;"><span style="text-decoration: underline;">9 de marzo:</span> <strong><span style="text-decoration: underline;"><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=132&amp;Itemid=188" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Día Internacional de la Mujer</a></span></strong>: se están realizando numerosas actividades en el centro durante dos semanas y se celebra la FERIA INTERCULTURAL DE LAS TAREAS DOMÉSTICAS.</span></li>
<li><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: 10pt;"><span style="text-decoration: underline;">8 de febrero:</span> <a href="archivos_ies/DIA_INTERNACIONAL_INTERNET_SEGURO.pdf" target="_blank" title="Día Internacioanl de Internet seguro." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">Día internacional de internet seguro.</a></span></li>
</ul>
</div>
</td>
</tr>
</tbody>
</table>
</div>
<p> </p>";s:4:"body";s:0:"";s:5:"catid";s:2:"37";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2014-09-04 07:07:51";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":69:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"0";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"0";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"0";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"0";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"0";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"0";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"0";s:15:"show_print_icon";s:1:"0";s:15:"show_email_icon";s:1:"0";s:9:"show_hits";s:1:"0";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:32:"show_category_heading_title_text";s:1:"1";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:14:"Miguel Anguita";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:1:"4";s:8:"ordering";s:1:"7";s:8:"category";s:15:"Datos generales";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:29:"219:ies-miguel-de-cervantes-2";s:7:"catslug";s:18:"37:datos-generales";s:6:"author";N;s:6:"layout";s:7:"article";s:4:"path";s:99:"index.php?option=com_content&view=article&id=219:ies-miguel-de-cervantes-2&catid=37:datos-generales";s:10:"metaauthor";s:14:"Miguel Anguita";s:6:"weight";d:0.51331000000000004;s:7:"link_id";i:1123;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:15:"Datos generales";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:15:"Datos generales";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=219";s:5:"route";s:99:"index.php?option=com_content&view=article&id=219:ies-miguel-de-cervantes-2&catid=37:datos-generales";s:5:"title";s:37:"IES Miguel de Cervantes - Granada (2)";s:11:"description";s:4499:"Pendientes de Matemáticas. Ver enlace Historia de Grecia, 1º de Bachillerato (23/09/13) Ver vídeo Actualizado el temario de Matemáticas de 1º y 3º ESO bilingües para poder imprimirlo. Entrar en zona privada de bilingües.font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"> Horario de inicio de curso, día 16/09/13 Ver enlace Decreto Formación Inicial y Permanente Decreto 93/2013, de 27 de agosto, por el que se regula la formación inicial y permanente del profesorado en la Comunidad Autónoma de Andalucía, así como el Sistema Andaluz de Formación Permanente del Profesorado. Ver enlace#ffffff;"> Orden de 5 de julio de 2013, que regula las ausencias del profesorado por enfermedad o accidente que no den lugar a incapacidad temporal Ver enlace Día lectivos y festivos en el Curso Escolar 2013/14 Ver enlacestyle="text-align: justify;"> Legislación actualizada a 05/09/13 Ver enlace CURSO 2012/13 20/02/13. Entrevista al filólogo Antoni Biosca sobre la importancia del Latín. 07/02/13. Publicada nueva normativa en materia de permisos y licencias. 07/02/13: Excelente web parahref="index.php?option=com_content&view=article&id=194" target="_self"> aprender jugando , todas las asignaturas, todos los niveles. 05/02/13. Hoy, 5 de febrero, se celebra el Día Internacional de la Internet Segura. 04/02/13. Datos, 1ª Evaluación 2012/13. 16/01/13. Programa de las Actividades Complementarias y Extraescolares por trimestres. 08/01/13. REANUDACIÓN DE LAS CLASES EN SU HORARIO NORMAL, A PARTIR DE LAS 08.15. 13/12/12. TENSION EN LAS AULAS. Aportación del Departamento de Latín y Griego. 07/11/12. ENSEÑANZA Y CONOCIMIENTO. Aportación del Departamento de Latín yGriego. 23/10/12. PROYECTO PIIISA 2012: Ciencia para jóvenes estudiantes www.piiisa.es 16/09/12. ORIENTACIONES OFICIALES DE LA UNIVERSIDAD SELECTIVIDAD 2012/13. 15/09/12 . LEGISLACIÓN. 11/09/12. Manual de funcionamiento de la unidad personal de SGD . (Para el profesorado) 29/06/12. Libros de 1º Bachillerato para el curso 2012/13. 29/06/12. Libros de 2º Bachillerato para el curso 2012/13. 25/06/12. Calendario Escolar 2012/13. 25/06/12.Instrucciones del Calendario Escolar 2012/13. 22/06/12. Exámenes que han salido en Selectividad junio 2012. ACTUALIZADO: PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES 2011-12. Consulta de los libros de la Biblioteca: Formato 1 - Formato 2 - Formato 3 Enlaces a bibliotecas y libros virtuales e interactivos. Actualizados los criterios de evaluación, consultar los Departamentos. Criterios que falten de 2º de Bachiller sobre Selectividad, consultarstyle="text-decoration: none; color: #c6c37b; text-align: center;"> www.ujaen.es Carteles sobre el invierno (Maestros y aprendices) Cartel invierno Horacio (latín) Irene Centenostyle="text-align: center;"> Paul Verlain 1 Paul Verlain 2 Jardín de invierno (P. Neruda) Maite Luzónsrc="archivos_ies/lengua/invierno/Maite_Luzon.jpg" border="0" alt="Maite Luzón." width="184" height="123" /> María Fernández Proverbios (Francés) Snow dreams (inglés)border-bottom-style: solid;" colspan="4" align="center"> VÍDEO DEL VIAJE A BROOKLYN, NUEVA YORK, ABRIL 2011style="color: #ffffff;"> DOCUMENTOS DEL PLAN DE CENTRO PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES. PROYECTO DE GESTIÓN.url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"> PLAN DE CONVICENCIA. NUEVO ROF 2011. PLAN DE ORIENTACIÓN Y ACCIÓN TUTORIAL.no-repeat;"> PLAN DE FORMACIÓN DEL IES MIGUEL DE CERVANTES. ESCUELA TIC 2.0padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"> ESCUELA TIC 2.0 MOCHILA DIGITAL SECUNDARIAleft;"> CURSOS CEP DE GRANADA NOTICIAS Blog del Viaje a Cumbria , Inglaterra, 14 al 23 de junio de 2011. Actividad extraescolar: " LOS SITIOS DE MARIANA PINEDA ", un recorrido por la ciudadde Granada: Departamento de Geografía e Historia. 26/05/2011: añadidas 6 fotografías de la actividad. 2º Bachillerato B. Master de Secundaria y Bachillerato 2011 . 31 de marzo de 2011 : ENCUENTROS LITERARIOS EN EL INSTITUTO. Andrés Neuman estuvo con el alumnado de 2º de Bachillerato 9 de marzo: Día Internacional de la Mujer : se están realizando numerosas actividades en el centro durante dos semanas y se celebra la FERIA INTERCULTURAL DE LAS TAREAS DOMÉSTICAS. 8 de febrero: Día internacional de internetseguro.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2004-07-30 19:00:00";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2009-09-15 00:00:00";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:13;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:27:{s:2:"id";s:2:"50";s:5:"alias";s:25:"normativa-de-bachillerato";s:7:"summary";s:14553:"<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="pageName" style="font: normal normal normal 14px/normal georgia; color: #cc3300; font-weight: bold; letter-spacing: 0.1em; line-height: 26px; padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;">Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía</td>
</tr>
<tr>
<td class="bodyText" style="font: normal normal normal 11px/normal arial; color: #333333; line-height: 24px;">
<table width="98%" border="0" cellspacing="1">
<tbody>
<tr>
<td bgcolor="#9AB2CE" style="font: normal normal normal 11px/normal arial; color: #333333;"><span class="Estilo2">Bachillerato</span></td>
</tr>
<tr>
<td class="listado" style="font: normal normal normal 11px/normal arial; color: #333333;"><ol>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden17marzo2011modificaOrdenesEvaluacion.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 17 de marzo de 2011</a>, por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/resoluc/Resoluc22oct2010bachillerbaccalaureat.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">RESOLUCIÓN de 22 de octubre de 2010</a>, de la Secretaría de Estado de Educación y Formación Profesional, por la que se dictan instrucciones relativas al programa de doble titulación Bachiller-Baccalauréat correspondientes al curso 2010- 2011 (BOE 04-11-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc4octubre2010pruebastitulobachillermayores20annos.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 4 de octubre de 2010</a>, de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de 2010.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/resoluc/Resolucion16sept2010convocatoriapruebaslibresBach.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">RESOLUCIÓN de 16 de septiembre de 2010</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 01-10-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc22sept2010matriculacionalumnadoBachilleratoconsuspensos.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 22 de septiembre</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre las condiciones de matriculación del alumnado de bachillerato con evaluación negativa en algunas materias, para el curso académico 2010/2011.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2010premiosextraordinariosbachillerato2009-10.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 15 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa relativas a los Premios Extraordinarios de Bachillerato correspondientes al curso 2009/2010.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden8sept2010premiosextraordinariosbachillerato2009-10.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 8 de septiembre de 2010</a>, por la que se convocan los Premios Extraordinarios de Bachillerato correspondientes al curso 2009-2010 (BOJA 23-09-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden26agosto2010pruebastituloBachiller.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 26 de agosto de 2010</a>, por la que se regulan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 09-09-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden30julio2010curriculomixtobachilleratobaccalaureat.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN EDU/2157/2010, de 30 de julio</a>, por la que se regula el currículo mixto de las enseñanzas acogidas al Acuerdo entre el Gobierno de España y el Gobierno de Francia relativo a la doble titulación de Bachiller y de Baccalauréat en centros docentes españoles, así como los requisitos para su obtención (BOE 07-08-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden13julio2010premiosnacionalesbachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN EDU/2058/2010, de 13 de julio</a>, por la que se regulan los Premios Nacionales de Bachillerato establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 29-07-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden6mayo2010modificacionOrdenbeca6000.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 6-05-2010</a>, por la que se modifica la de 24 de junio de 2009, conjunta de las Consejerías de Educación y de Empleo, por la que se establecen las bases reguladoras de la BECA 6000 dirigida a facilitar la permanencia en el sistema educativo del alumnado de bachillerato o de ciclos formativos de grado medio de formación profesional inicial y se efectúa su convocatoria para el curso escolar 2009-2010 (BOJA 07-06-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD-102-2010-bachillerato-espana-francia.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">REAL DECRETO 102/2010</a>, de 5 de febrero, por el que se regula la ordenación de las enseñanzas acogidas al acuerdo entre el Gobierno de España y el Gobierno de Francia relativo a la doble titulación de Bachiller y de Baccalauréat en centros docentes españoles (BOE 12-03-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD1953-2009modificandoRDcalculonotamediabachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">REAL DECRETO 1953/2009</a>, de 18 de diciembre, por el que se modifican el Real Decreto 1577/2006, de 22 de diciembre, el Real Decreto 85/2007, de 26 de enero, y el Real Decreto 1467/2007, de 2 de noviembre, en lo relativo al cálculo de la nota media de los alumnos de las enseñanzas profesionales de música y danza (BOE 18-01-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc1oct2009promocioncursoincompletobach.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 1-10-2009</a> de la Dirección General de Ordenación y Evaluación Educativa, sobre la promoción del alumnado del curso incompleto de segundo de bachillerato del sistema educativo regulado en la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo al previsto en la Ley Orgánica 2/2006, de 3 de mayo, de Educación.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/OrdenEDU9sept2009promocioncursoLOGSE-LOE.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN EDU/2395/2009</a>, de 9 de septiembre, por la que se regula la promoción de un curso incompleto del sistema educativo definido por la Ley Orgánica 1/1990, de 3 de octubre, de ordenación general del sistema educativo, a otro de la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 12-09-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc22junio2009optativas2bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 22-6-2009</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre la oferta por los centros de materias optativas de segundo curso de Bachillerato.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/INSTRUC_PRIMBACH_3_4_MAT_NOSUP_2009.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 16-6-2009</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre la permanencia en el primer curso de bachillerato del alumnado con tres ó cuatro materias no superadas.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden24junio2009becas6000.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 24-6-2009</a>, conjunta de las Consejerías de Educación y de Empleo, por las que se establecen las bases reguladoras de la BECA 6000, dirigida a facilitar la permanencia en el sistema educativo del alumnado de bachillerato o de ciclos formativos de grado medio de formación profesional inicial y se efectúa su convocatoria para el curso escolar 2009-2010 (BOJA 29-06-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/resoluc/Resoluc17junio2009acuerdorepeticionbachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">RESOLUCIÓN de 17-6-2009</a>, de la Dirección General de Evaluación y Cooperación Territorial, por la que se publica el Acuerdo de la Conferencia Sectorial de Educación sobre las condiciones de repetición en el primer curso de Bachillerato (BOE 19-06-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden%20de%2015%20_12_2008%20Evaluacion%20Bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 15-12-2008</a>, por la que se establece la ordenación de la evaluación del proceso de aprendizaje del alumnado de bachillerato en la Comunidad Autónoma de Andalucía.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/bachillerato%20de%20adultos.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 29-9-2008</a>, por la que se regulan las enseñanzas de Bachillerato para personas adultas.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden%205-8-2008%20Curriculo%20Bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 5-8-2008,</a> por la que se desarrolla el currículo correspondiente al Bachillerato en Andalucía. (BOJA 26-8-2008)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/decretos/Decreto%20416-2008%20Bachillerato%20Andalucia.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">DECRETO 416/2008, de 22 de julio,</a> por el que se establece la ordenación y las enseñanzas correspondientes al Bachillerato en Andalucía. (BOJA 28-7-2008)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/resoluc/Resoluc%2011-4-2008%20ConverCalif%20Bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">RESOLUCIÓN de 11-4-2008,</a> de la Secretaría General de Educación, por la que se establecen las normas para la conversión de las calificaciones cualitativas en calificaciones numéricas del expediente académico del alumnado de bachillerato y cursos de acceso a la universidad de planes anteriores a la Ley Orgánica 1/1990, de 3 de mayo, de Ordenación General del Sistema Educativo. (BOE 24-4-2008)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD%201467-2007%20%20Correccion%20errores.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">Corrección de errores del Real Decreto 1467/2007, de 2 de noviembre,</a> por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas. (BOE 7-11-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD%201467-2007%20Estructura%20Bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">REAL DECRETO 1467/2007, de 2 de noviembre,</a> por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas. (BOE 6-11-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/orden%2013-12-1994%20Religion%20Bachillerato.htm" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 13-12-1994</a> por la que se establece el curriculum de Religión Católica en el Bachillerato en Andalucía. (BOJA de 10-01-95).</li>
</ol></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>";s:4:"body";s:0:"";s:5:"catid";s:3:"104";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2012-12-05 10:24:50";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":70:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"0";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";i:0;s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"0";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"0";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"0";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"0";s:9:"show_vote";i:0;s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"0";s:15:"show_print_icon";s:1:"0";s:15:"show_email_icon";i:0;s:9:"show_hits";s:1:"0";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";s:12:"show_section";i:0;s:12:"link_section";i:0;}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":2:{s:6:"robots";s:0:"";s:6:"author";s:0:"";}}s:7:"version";s:1:"3";s:8:"ordering";s:1:"3";s:8:"category";s:17:"ÚLTIMA NORMATIVA";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:28:"50:normativa-de-bachillerato";s:7:"catslug";s:20:"104:ultima-normativa";s:6:"author";N;s:4:"mime";N;s:6:"layout";s:7:"article";s:4:"path";s:100:"index.php?option=com_content&view=article&id=50:normativa-de-bachillerato&catid=104:ultima-normativa";s:10:"metaauthor";N;s:6:"weight";d:0.51331000000000004;s:7:"link_id";i:573;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:17:"ÚLTIMA NORMATIVA";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:17:"ÚLTIMA NORMATIVA";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:47:"index.php?option=com_content&view=article&id=50";s:5:"route";s:100:"index.php?option=com_content&view=article&id=50:normativa-de-bachillerato&catid=104:ultima-normativa";s:5:"title";s:12:"Bachillerato";s:11:"description";s:7055:"Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía Bachillerato ORDEN de 17 de marzo de 2011 , por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011). RESOLUCIÓN de 22 de octubre de 2010 , de la Secretaría de Estado de Educación y Formación Profesional, por la que se dictan instrucciones relativas al programa de doble titulación Bachiller-Baccalauréat correspondientes al curso 2010- 2011 (BOE 04-11-2010).href="http://www.adideandalucia.es/normas/instruc/Instruc4octubre2010pruebastitulobachillermayores20annos.pdf" target="_blank" style="color: #005fa9; text-decoration: none;"> INSTRUCCIONES de 4 de octubre de 2010 , de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de 2010. RESOLUCIÓN de 16 de septiembre de 2010 , de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 01-10-2010). INSTRUCCIONES de 22 de septiembre , de la Dirección General de Ordenación y Evaluación Educativa, sobre las condiciones de matriculación del alumnado de bachillerato con evaluación negativa en algunas materias, para el curso académico 2010/2011. INSTRUCCIONES de 15 de septiembre de 2010 de la Dirección General de Ordenación y Evaluación Educativa relativas a los Premios Extraordinarios de Bachillerato correspondientes al curso 2009/2010.10px; padding-left: 0px;"> ORDEN de 8 de septiembre de 2010 , por la que se convocan los Premios Extraordinarios de Bachillerato correspondientes al curso 2009-2010 (BOJA 23-09-2010). ORDEN de 26 de agosto de 2010 , por la que se regulan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 09-09-2010). ORDEN EDU/2157/2010, de 30 de julio , por la que se regula el currículo mixto de las enseñanzas acogidas al Acuerdo entre el Gobierno de España y el Gobierno de Francia relativo a la doble titulación de Bachiller y de Baccalauréat en centros docentes españoles, así como los requisitos para su obtención (BOE 07-08-2010). ORDEN EDU/2058/2010, de 13 de julio , por la que se regulan los Premios Nacionales de Bachillerato establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 29-07-2010).target="_blank" style="color: #005fa9; text-decoration: none;"> ORDEN de 6-05-2010 , por la que se modifica la de 24 de junio de 2009, conjunta de las Consejerías de Educación y de Empleo, por la que se establecen las bases reguladoras de la BECA 6000 dirigida a facilitar la permanencia en el sistema educativo del alumnado de bachillerato o de ciclos formativos de grado medio de formación profesional inicial y se efectúa su convocatoria para el curso escolar 2009-2010 (BOJA 07-06-2010). REAL DECRETO 102/2010 , de 5 de febrero, por el que se regula la ordenación de las enseñanzas acogidas al acuerdo entre el Gobierno de España y el Gobierno de Francia relativo a la doble titulación de Bachiller y de Baccalauréat en centros docentes españoles (BOE 12-03-2010). REAL DECRETO 1953/2009 , de 18 de diciembre, por el que se modifican el Real Decreto 1577/2006, de 22 de diciembre, el Real Decreto 85/2007, de 26 de enero, y el Real Decreto 1467/2007, de 2 de noviembre, en lo relativo al cálculo de la nota media de los alumnos de las enseñanzas profesionales de música y danza (BOE 18-01-2010). INSTRUCCIONES de 1-10-2009 de la Dirección General de Ordenación y Evaluación Educativa, sobre la promoción del alumnado del cursoincompleto de segundo de bachillerato del sistema educativo regulado en la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo al previsto en la Ley Orgánica 2/2006, de 3 de mayo, de Educación. ORDEN EDU/2395/2009 , de 9 de septiembre, por la que se regula la promoción de un curso incompleto del sistema educativo definido por la Ley Orgánica 1/1990, de 3 de octubre, de ordenación general del sistema educativo, a otro de la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 12-09-2009). INSTRUCCIONES de 22-6-2009 , de la Dirección General de Ordenación y Evaluación Educativa, sobre la oferta por los centros de materias optativas de segundo curso de Bachillerato. INSTRUCCIONES de 16-6-2009 , de la Dirección General de Ordenación y Evaluación Educativa, sobre la permanencia en el primer curso de bachillerato del alumnado con tres ó cuatro materias no superadas. ORDEN de 24-6-2009 , conjunta de las Consejerías de Educación y de Empleo, por las que se establecenlas bases reguladoras de la BECA 6000, dirigida a facilitar la permanencia en el sistema educativo del alumnado de bachillerato o de ciclos formativos de grado medio de formación profesional inicial y se efectúa su convocatoria para el curso escolar 2009-2010 (BOJA 29-06-2009). RESOLUCIÓN de 17-6-2009 , de la Dirección General de Evaluación y Cooperación Territorial, por la que se publica el Acuerdo de la Conferencia Sectorial de Educación sobre las condiciones de repetición en el primer curso de Bachillerato (BOE 19-06-2009). ORDEN de 15-12-2008 , por la que se establece la ordenación de la evaluación del proceso de aprendizaje del alumnado de bachillerato en la Comunidad Autónoma de Andalucía. ORDEN de 29-9-2008 , por la que se regulan las enseñanzas de Bachillerato para personas adultas. ORDEN de 5-8-2008, por la que se desarrolla el currículo correspondiente al Bachillerato en Andalucía. (BOJA 26-8-2008)padding-bottom: 10px; padding-left: 0px;"> DECRETO 416/2008, de 22 de julio, por el que se establece la ordenación y las enseñanzas correspondientes al Bachillerato en Andalucía. (BOJA 28-7-2008) RESOLUCIÓN de 11-4-2008, de la Secretaría General de Educación, por la que se establecen las normas para la conversión de las calificaciones cualitativas en calificaciones numéricas del expediente académico del alumnado de bachillerato y cursos de acceso a la universidad de planes anteriores a la Ley Orgánica 1/1990, de 3 de mayo, de Ordenación General del Sistema Educativo. (BOE 24-4-2008) Corrección de errores del Real Decreto 1467/2007, de 2 de noviembre, por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas. (BOE 7-11-2007) REAL DECRETO 1467/2007, de 2 de noviembre, por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas. (BOE 6-11-2007)href="http://www.adideandalucia.es/normas/ordenes/orden%2013-12-1994%20Religion%20Bachillerato.htm" target="_blank" style="color: #005fa9; text-decoration: none;"> ORDEN de 13-12-1994 por la que se establece el curriculum de Religión Católica en el Bachillerato en Andalucía. (BOJA de 10-01-95).";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2011-05-11 17:40:00";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2011-05-11 17:40:00";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:14;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:27:{s:2:"id";s:2:"34";s:5:"alias";s:6:"2bachc";s:7:"summary";s:23613:"<div class="iconosTituloSeccion" style="float: right;">&nbsp;&nbsp;</div>
<div style="clear: both;"></div>
<div id="tituloItem" class="blockTituloItem" style="display: block; margin-top: 15px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; border-bottom-width: 2px; border-bottom-style: dotted; border-bottom-color: #aaaaaa; float: left; width: 546px; padding: 0px;">
<div class="tituloIzq" style="float: left;"><img alt="vineta" src="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/skins/helvia/img/vineta.gif" border="0" /> <span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; letter-spacing: 1pt; color: #71cd63;">Matemáticas CC SS 2º BACH.</span></div>
</div>
<div id="contenido_sitio" style="width: 546px; float: left; display: block;">
<ul>
<li>Ejercicios y soluciones de&nbsp;<a title="Solucionario 2º BACH. MAT CC SS II. Matrices." href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/upload/2_pdfsam_2BachilleratoSolucionarioMatematicasAplicadasCCSSLaCasadelSa.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Matrices 2010/11.</a></li>
<li>Ejercicios y soluciones de&nbsp;<a title="Solucionario 2º BACH. MAT CC SS II. Programación lineal." href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/upload/129_pdfsam_2BachilleratoSolucionarioMatematicasAplicadasCCSSLaCasadelSa.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Programación lineal 2010/11.</a> 
<hr />
</li>
<li><span style="font-family: arial, helvetica, sans-serif;"><span style="font-size: 12pt; color: #0000ff; font-family: Arial;">Exámemes de Selectividad resueltos de&nbsp;<a title="http://www.ies-acci.com/antonioroldan/Selectividad%20-%20directo/Selectividad.htm" href="http://www.ies-acci.com/antonioroldan/Selectividad%20-%20directo/Selectividad.htm" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Matemáticas II y CC SS II</a>.</span></span></li>
<li><span style="font-family: arial, helvetica, sans-serif;"><strong>COMPLETADAS LAS SOLUCIONES DE MATEMÁTICAS CCSS II. Entrar en </strong><a title="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/aula/archivos.cgi?wAccion=vergrupo&amp;wIdGrupo=916" href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/aula/archivos.cgi?wAccion=vergrupo&amp;wIdGrupo=916" style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><strong>este enlace</strong></a><strong>, poner las claves, después 2ºBACHSOLUCIONARIO Y </strong><a class="noRightClick Contenido-Item-LinkNombre" href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/aula/location.cgi?wseccion=03&amp;esMicrositio=no&amp;wid_archivo=9" target="_blank" style="color: #003000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><strong>SolucionesIntegralesProbInferencia.pdf</strong></a></span></li>
</ul>
<p><strong><span style="text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 14pt;">SELECTIVIDAD 2009/10</span></span></span></strong></p>
DIRECTRICES Y ORIENTACIONES GENERALES<br />PARA LAS PRUEBAS DE ACCESO A LA UNIVERSIDAD&nbsp;<a title="upload/ORINTACIONESmatematicas_CCSSII200910.pdf" href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/upload/ORINTACIONESmatematicas_CCSSII200910.pdf" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">MAT. CC SS II</a>.
<p><strong><span style="text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 14pt;"><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/upload/REUNION_DE_LA_PONENCIA_DE_SELECTIVIDAD_DE_MATEMATICAS_CC_SS_II.pdf" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: mceinline;">INFORMACIÓN DE LA PONENCIA DE MAT. CC SS II DEL DÍA 15/10/09</span></a></span></span></span></span></strong></p>
<p><strong><span style="text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 14pt;"><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 14pt;"><span style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; font-weight: normal;"><span style="font-family: mceinline;"><span style="font-size: 10pt; color: #810081;"><a title="upload/Segunda_coordinacionTODO.pdf" href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/upload/Segunda_coordinacionTODO.pdf" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">INFORMACIÓN DE LA PONENCIA DE MAT. CC SS II DEL DÍA 08/04/10</a></span></span></span></span></span></span></span></span></span></strong></p>
<p><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;"><a title="upload/relacion_contrastes_ponencia.pdf" href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/upload/relacion_contrastes_ponencia.pdf" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Relación de ejecrcios de Contraste de Hipóteis resueltos</a>, proporcionados por la Ponencia de Selectividad el día 14/10/09.</span></span><strong><span style="text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><br /></span></span></strong><a href="http://www.iesalpujarra.edu.es/files/criterios_sel_matematicas_aplicadas200910.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Indicaciones para Matemáticas CC SS II.</span></span></a><br /><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Indicaciones para todas las asignaturas de 2º de Bachillerato.</span></span></a></p>
<p><strong> </strong></p>
<p><strong><span style="text-decoration: underline;"><span style="font-size: 12pt;"><span style="font-family: 'comic sans ms', sans-serif;">CONTENIDOS , RECURSOS, PRUEBAS, LEGISLACIÓN: CURSO 09/10.</span></span></span></strong></p>
<ul>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Documento de las Universidades sobre la </span></span><a href="http://www.iesalpujarra.edu.es//files/matematicas/MCSII0809criteriosuniversidades.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">asignatura curso 2008/09.</span></span></a></li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Documento de las Universidades sobre la </span></span><a href="http://www.iesalpujarra.edu.es//files/matematicas/matematicas_aplicadas.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">asignatura curso 2007/08.</span></span></a><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;"> </span></span> 
<ul>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Calendario de Selectividad 2007/08: </span></span><a href="http://www.iesalpujarra.edu.es//files/matematicas/calendarioselec08.mht" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">junio y sept. 08.</span></span></a></li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Calendario de Selectividad 2008/09: junio y sept. 09. Próximamente.</span></span></li>
</ul>
</li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Objetivos, contenidos, procedimientos, actitudes. </span></span><a href="http://www.iesalpujarra.edu.es//files/matematicas/obj_cont_proc_act.doc" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">MCS II.</span></span></a></li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Curriculo del Bachillerato en Andalucía. </span></span><a href="http://www.iesalpujarra.edu.es//files/matematicas/curricul_bach_boja26ago08.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Boja 26/08/08.</span></span></a></li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Decreto 208/2002 del Bachillerato en Andalucía. </span></span><a href="http://www.iesalpujarra.edu.es//files/matematicas/decreto208_2002bach_andalucia.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Decreto 208/2002.</span></span></a></li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Decreto 416/2008 de Ordenación del Bachillerato en Andalucía. </span></span><a href="http://www.iesalpujarra.edu.es//files/matematicas/decreto416_2008_ordenacion_bach.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Decreto 416.</span></span></a></li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Materiales curriculares del Bachillerato en Andalucía. </span></span><a href="http://www.iesalpujarra.edu.es//files/matematicas/materiales_curriculares_bach.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Materiales curriculares.</span></span></a></li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Consejería de Educación: Averroes. Bachillerato. </span></span><a href="http://www.juntadeandalucia.es/averroes/impe/web/contenido?pag=/contenidos/B/ApoyoAlCurriculo/CurriculoDeAndalucia/Seccion/Bachillerato/Bachillerato" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Averroes.</span></span></a></li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Selectividad 2007/08. Exámenes y criterios de corrección de Selectividad 07/08.</span></span></li>
</ul>
<table border="0" style="padding-left: 30px; text-align: center;">
<thead> 
<tr>
<th><span style="font-family: 'comic sans ms', sans-serif;">Examen</span></th><th><span style="font-family: 'comic sans ms', sans-serif;">Criterios</span></th>
</tr>
</thead> 
<tbody>
<tr class="odd">
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/examen%201.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Examen 1.</span></span></a></td>
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/criterios%201.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Criterios 1.</span></span></a></td>
</tr>
<tr class="even">
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/examen%202.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Examen 2.</span></span></a></td>
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/criterios%202.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Criterios 2.</span></span></a></td>
</tr>
<tr class="odd">
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/examen%203.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Examen 3.</span></span></a></td>
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/criterios%203.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Criterios 3.</span></span></a></td>
</tr>
<tr class="even">
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/examen%204.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Examen 4.</span></span></a></td>
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/criterios%204.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Criterios 4.</span></span></a></td>
</tr>
<tr class="odd">
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/examen%205.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Examen 5.</span></span></a></td>
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/criterios%205.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Criterios5.</span></span></a></td>
</tr>
<tr class="even">
<td align="center"><a href="http://www.iesalpujarra.edu.es//files/matematicas/selectividad0708/examen%206.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Examen 6.</span></span></a></td>
<td align="center"><a href="http://www.iesalpujarra.edu.es/files/matematicas/selectividad0708/criterios%206.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Criterios 6.</span></span></a></td>
</tr>
</tbody>
</table>
<ul>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Exámenes de Selectividad de junio y septiembre de 2008 </span></span><a href="http://www.mundofree.com/gjrubio/" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">resueltos.</span></span></a></li>
<li><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Exámenes de Selectividad de junio y septiembre de 2009. Próximamente. </span></span></li>
</ul>
<p align="left"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;"> Ejercicios resueltos de: </span></span></p>
<ul>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_sist_ecu.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Sistemas de ecuaciones.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_matrices.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Matrices.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_resol_sist_det.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Determinantes.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_progr_linmeal.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Programación lineal.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_lim_cont.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Límites y continuidad.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_derivadas.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Derivadas.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_repr_fun.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Representación de funciones.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_integrales.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Integrales.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_probabilidad1.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Probabilidad.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_muestras.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Muestras.</span></span></a></li>
<li><a href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_inferencia.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Inferencia.</span></span></a></li>
</ul>
<p><span style="font-size: 12pt;"><span style="font-family: 'comic sans ms', sans-serif;"><strong><span style="text-decoration: underline;"><span style="color: #ff0000;"><span style="background-color: #ffcc99;">PLAN BOLONIA:</span></span></span></strong> </span></span><a href="http://www.iesalpujarra.edu.es/files/PlanBolonia.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'comic sans ms', sans-serif;"><span style="font-size: 10pt;">Ver aquí.</span></span></a></p>
</div>
<div class="fechaModif" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; text-align: left; color: #666666; text-decoration: none;"></div>";s:4:"body";s:0:"";s:5:"catid";s:3:"238";s:10:"created_by";s:2:"62";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2012-12-05 10:24:50";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":70:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";i:0;s:11:"link_titles";i:0;s:10:"show_intro";i:0;s:13:"show_category";i:0;s:13:"link_category";i:0;s:20:"show_parent_category";s:1:"0";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"0";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"0";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"0";s:9:"show_vote";i:0;s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"0";s:15:"show_print_icon";s:1:"0";s:15:"show_email_icon";i:0;s:9:"show_hits";s:1:"0";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";s:12:"show_section";i:0;s:12:"link_section";i:0;}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":2:{s:6:"robots";s:0:"";s:6:"author";s:0:"";}}s:7:"version";s:1:"1";s:8:"ordering";s:2:"13";s:8:"category";s:12:"Matemáticas";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:9:"34:2bachc";s:7:"catslug";s:15:"238:matematicas";s:6:"author";N;s:4:"mime";N;s:6:"layout";s:7:"article";s:4:"path";s:76:"index.php?option=com_content&view=article&id=34:2bachc&catid=238:matematicas";s:10:"metaauthor";N;s:6:"weight";d:0.51331000000000004;s:7:"link_id";i:557;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:3:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:12:"Matemáticas";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:12:"Matemáticas";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:47:"index.php?option=com_content&view=article&id=34";s:5:"route";s:76:"index.php?option=com_content&view=article&id=34:2bachc&catid=238:matematicas";s:5:"title";s:7:"2BACH_C";s:11:"description";s:3349:"Matemáticas CC SS 2º BACH. Ejercicios y soluciones de Matrices 2010/11. Ejercicios y soluciones de Programación lineal 2010/11. Exámemes de Selectividad resueltos dehref="http://www.ies-acci.com/antonioroldan/Selectividad%20-%20directo/Selectividad.htm" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"> Matemáticas II y CC SS II . COMPLETADAS LAS SOLUCIONES DE MATEMÁTICAS CCSS II. Entrar en este enlace , poner las claves, después 2ºBACHSOLUCIONARIO Y SolucionesIntegralesProbInferencia.pdf SELECTIVIDAD 2009/10 DIRECTRICES Y ORIENTACIONES GENERALES PARA LAS PRUEBAS DE ACCESO A LA UNIVERSIDAD MAT. CC SS II .underline;"> INFORMACIÓN DE LA PONENCIA DE MAT. CC SS II DEL DÍA 15/10/09 INFORMACIÓN DE LA PONENCIA DE MAT. CC SS II DEL DÍA 08/04/10 Relación deejecrcios de Contraste de Hipóteis resueltos , proporcionados por la Ponencia de Selectividad el día 14/10/09. Indicaciones para Matemáticas CC SS II. Indicaciones para todas las asignaturas de 2º de Bachillerato. CONTENIDOS , RECURSOS, PRUEBAS, LEGISLACIÓN: CURSO 09/10. Documento de las Universidades sobre la asignatura curso 2008/09. Documento de las Universidades sobre lahref="http://www.iesalpujarra.edu.es//files/matematicas/matematicas_aplicadas.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"> asignatura curso 2007/08. Calendario de Selectividad 2007/08: junio y sept. 08. Calendario de Selectividad 2008/09: junio y sept. 09. Próximamente. Objetivos, contenidos, procedimientos, actitudes. MCS II. Curriculo del Bachillerato en Andalucía.font-weight: normal; text-decoration: underline;"> Boja 26/08/08. Decreto 208/2002 del Bachillerato en Andalucía. Decreto 208/2002. Decreto 416/2008 de Ordenación del Bachillerato en Andalucía. Decreto 416. Materiales curriculares del Bachillerato en Andalucía. Materiales curriculares. Consejería de Educación: Averroes. Bachillerato.href="http://www.juntadeandalucia.es/averroes/impe/web/contenido?pag=/contenidos/B/ApoyoAlCurriculo/CurriculoDeAndalucia/Seccion/Bachillerato/Bachillerato" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"> Averroes. Selectividad 2007/08. Exámenes y criterios de corrección de Selectividad 07/08. Examen Criterios Examen 1. Criterios 1.sans-serif;"> Examen 2. Criterios 2. Examen 3. Criterios 3. Examen 4.10pt;"> Criterios 4. Examen 5. Criterios5. Examen 6. Criterios 6. Exámenes de Selectividad de junio y septiembre de 2008underline;"> resueltos. Exámenes de Selectividad de junio y septiembre de 2009. Próximamente. Ejercicios resueltos de: Sistemas de ecuaciones. Matrices. Determinantes. Programación lineal.href="http://www.iesalpujarra.edu.es/files/Ejercicios_resueltos_lim_cont.pdf" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"> Límites y continuidad. Derivadas. Representación de funciones. Integrales. Probabilidad.11px; font-weight: normal; text-decoration: underline;"> Muestras. Inferencia. PLAN BOLONIA: Ver aquí.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2011-02-01 08:43:43";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2011-02-01 08:43:43";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:15;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:27:{s:2:"id";s:3:"173";s:5:"alias";s:6:"milena";s:7:"summary";s:7386:"<p style="text-align: center;"><strong><span style="font-size: 14pt;">CONFERENCIA INAUGURAL DE LA FERIA DEL LIBRO DEL I.E.S. MIGUEL DE CERVANTES A CARGO DE D. ANTONIO CARVAJAL MILENA</span>&nbsp;</strong></p>
<p style="text-align: center;"><strong> <img style="float: right;" src="archivos_ies/actividades/imagenes/actualidad/a_carvajal.jpg" width="129" height="161" /><br /></strong></p>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25" align="justify"><span style="font-family: 'Times New Roman',Times,serif; color: #000033;" class="Estilo27"><img border="0" hspace="5" align="left" src="archivos_ies/actividades/imagenes/actualidad/a_carvajal1.jpg" width="221" height="166" />Como ya viene siendo habitual en los últimos años y en el ámbito del vigente Plan de Lectura que desarrolla el I.E.S. Miguel de Cervantes, hemos vuelto a contar con un autor literario de relevancia para fomentar la lectura y el estudio entre nuestros alumnos. El día 10 de Noviembre y como acto inaugural de la feria del libro, tuvimos la suerte de contar con un escritor excepcional, el poeta D. Antonio Carvajal Milena. Sus principales características poéticas, notas biográficas y bibliográficas ya conocían los alumnos de 2º Bachillerato, a quienes iba dirigida su conferencia, por su trabajo previo en clase.&nbsp; Se trata de un poeta de resonancias clásicas y eruditas; gran maestro del lenguaje; todo un lujo para el centro y para los privilegiados oyentes que se encontraban en el S.U.M. a las 9’15 horas.<br /></span><br /><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Biografía </span><br /><span style="color: #000033; font-size: medium;" class="Estilo18"><img border="0" hspace="5" align="right" src="archivos_ies/actividades/imagenes/actualidad/a_carvajal2.jpg" width="221" height="166" />Antonio Carvajal es doctor en Filología Románica por la Universidad de Granada y titular de Métrica. En 1990 recibió el&nbsp;<a title="Premio Nacional de la Crítica" href="http://enciclopedia.us.es/index.php/Premio_Nacional_de_la_Cr%C3%ADtica">Premio Nacional de la Crítica</a>.&nbsp;<br />Está considerado como uno de los poetas mayores de la actual poesía española. Sus libros han sido destacados como los más intensos y personales que han aparecido en las últimas décadas.&nbsp;<br />Notable renovador de la tradición poética andaluza, artífice de una versificación depurada e innovadora, ha seguido fielmente, desde su primer poemario, la línea de la poesía barroca junto con algunas aportaciones de las vanguardias. Así lo acreditan el uso de complicadas combinaciones estróficas, figuras retóricas de corte quevediano y gongorino y los planteamientos filosóficos de sus contenidos. Desde mediados de los años 80, junto a las estructuras cultas ya mencionadas, se pueden observar módulos de poesía popular.</span></p>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25"><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Obra </span><br /><span style="color: #000033; font-size: medium;" class="Estilo18">De entre su obras, destacan:</span><span style="color: #000033; font-size: medium;" class="Estilo18"><img border="0" hspace="100" align="right" src="archivos_ies/actividades/imagenes/actualidad/a_carvajal3.jpg" width="221" height="166" /></span></p>
<ol style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25">
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Tigres en el jardín (1968)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Serenata y navaja (1973)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Siesta en el mirador (1979)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Servidumbre de paso (1982)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Del viento en los jazmines (1984)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Noticia de setiembre (1984)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>De un capricho celeste (1988)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Testimonio de invierno (1990)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Miradas sobre agua (1993)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Alma región luciente (1998)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Los pasos evocados (2004)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Una canción más clara (2008)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Cartas a los amigos (2009)</em></li>
</ol>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25"><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Selección<br /></span><br /><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">Siesta en el mirador&nbsp;<em>1968</em></span> <br />Sólo para tus labios mi sangre está madura,<br />con obsesión de estío preparada a tus besos,<br />siempre fiel a mis brazos y llena de hermosura,<br />exangües cada noche, y cada aurora ilesos.</p>
<p>Si crepitan los bosques de caza y aventura<br />y los pájaros altos burlan de vernos presos,<br />no dejes que tus ojos dibujen la amargura<br />de los que no han llevado el amor en los huesos.</p>
<p>Quédate entre mis brazos, que sólo a mí me tienes,<br />que los demás te odian, que el corazón te acecha<br />en los latidos cálidos del vientre y de las sienes.</p>
<p>Mira que no hay jardines más allá de este muro,<br />que es todo un largo olvido y si mi amor te estrecha<br />verás un cielo abierto detrás del llanto oscuro.</p>
<p><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">A veces el amor tiene caricias... </span><br />A veces el amor tiene caricias<br />frías, como navajas de barbero.<br />Cierras los ojos. Das tu cuello entero<br />a un peligroso filo de delicias.</p>
<p>Otras veces se clava como aguja<br />irisada de sedas en el raso<br />del bastidor: raso del lento ocaso<br />donde un cisne precoz se somorguja.</p>
<p>En general, adopta una manera<br />belicosa, de horcas y cuchillos,<br />de lanza en ristre o de falcón en mano.</p>
<p>Pero es lo más frecuente que te hiera<br />con ojos tan serenos y sencillos<br />como un arroyo fresco en el verano.</p>
<p><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">Aldaba de noviembre </span><br />Una tristeza dulce y anterior<br />al suspiro y las lágrimas,<br />anterior al idilio de la tarde<br />azul y el jacaranda,<br />invade la memoria con su música,<br />su brisa, su nostalgia:<br />Es la tristeza de mirar el cielo<br />cautivo entre las ramas.</p>";s:4:"body";s:0:"";s:5:"catid";s:3:"199";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"2012-12-16 09:19:12";s:11:"modified_by";s:3:"664";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":68:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"0";s:10:"show_intro";s:1:"0";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"0";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"0";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:1:"2";s:8:"ordering";s:1:"7";s:8:"category";s:11:"Actividades";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:10:"173:milena";s:7:"catslug";s:15:"199:actividades";s:6:"author";s:10:"Super User";s:4:"mime";N;s:6:"layout";s:7:"article";s:4:"path";s:87:"index.php?option=com_content&view=article&id=173:milena&catid=199:actividades&Itemid=77";s:10:"metaauthor";N;s:6:"weight";d:0.51331000000000004;s:7:"link_id";i:533;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:11:"Actividades";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:11:"Actividades";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=173";s:5:"route";s:87:"index.php?option=com_content&view=article&id=173:milena&catid=199:actividades&Itemid=77";s:5:"title";s:23:"Antonio Carvajal Milena";s:11:"description";s:3823:"CONFERENCIA INAUGURAL DE LA FERIA DEL LIBRO DEL I.E.S. MIGUEL DE CERVANTES A CARGO DE D. ANTONIO CARVAJAL MILENA Como ya viene siendo habitual en los últimos años y en el ámbito del vigente Plan de Lectura que desarrolla el I.E.S. Miguel de Cervantes, hemos vuelto a contar con un autor literario de relevancia para fomentar la lectura y el estudio entre nuestros alumnos. El día 10 de Noviembre y como acto inaugural de la feria del libro, tuvimos la suerte de contar con un escritor excepcional, el poeta D. Antonio Carvajal Milena. Sus principales características poéticas, notas biográficas y bibliográficas ya conocían los alumnos de 2º Bachillerato, a quienes iba dirigida su conferencia, por su trabajo previo en clase. Se trata de un poeta de resonancias clásicas y eruditas; gran maestro del lenguaje; todo un lujo para el centro y para los privilegiados oyentes que se encontraban en el S.U.M. a las 9’15 horas. Biografía Antonio Carvajal es doctor en Filología Románica por la Universidad de Granada y titular de Métrica. En 1990 recibió eltitle="Premio Nacional de la Crítica" href="http://enciclopedia.us.es/index.php/Premio_Nacional_de_la_Cr%C3%ADtica"> Premio Nacional de la Crítica . Está considerado como uno de los poetas mayores de la actual poesía española. Sus libros han sido destacados como los más intensos y personales que han aparecido en las últimas décadas. Notable renovador de la tradición poética andaluza, artífice de una versificación depurada e innovadora, ha seguido fielmente, desde su primer poemario, la línea de la poesía barroca junto con algunas aportaciones de las vanguardias. Así lo acreditan el uso de complicadas combinaciones estróficas, figuras retóricas de corte quevediano y gongorino y los planteamientos filosóficos de sus contenidos. Desde mediados de los años 80, junto a las estructuras cultas ya mencionadas, se pueden observar módulos de poesía popular. Obra De entre su obras, destacan: Tigres en el jardín (1968) Serenata y navaja (1973) Siesta en el mirador (1979) Servidumbre de paso (1982) Del viento enlos jazmines (1984) Noticia de setiembre (1984) De un capricho celeste (1988) Testimonio de invierno (1990) Miradas sobre agua (1993) Alma región luciente (1998) Los pasos evocados (2004) Una canción más clara (2008) Cartas a los amigos (2009) Selección Siesta en el mirador 1968 Sólo para tus labios mi sangre está madura, con obsesión de estío preparada a tus besos, siempre fiel a mis brazos y llena de hermosura, exangües cada noche, y cada aurora ilesos. Si crepitan los bosques de caza y aventura y los pájaros altos burlan de vernos presos, no dejes que tus ojos dibujen la amargura de los que no han llevado el amor en los huesos. Quédate entre mis brazos, que sólo a mí me tienes, que los demás te odian, que el corazón te acecha en los latidos cálidos del vientre y de las sienes. Mira que no hay jardines más allá de este muro, que es todo un largo olvido y si mi amor te estrecha verás uncielo abierto detrás del llanto oscuro. A veces el amor tiene caricias... A veces el amor tiene caricias frías, como navajas de barbero. Cierras los ojos. Das tu cuello entero a un peligroso filo de delicias. Otras veces se clava como aguja irisada de sedas en el raso del bastidor: raso del lento ocaso donde un cisne precoz se somorguja. En general, adopta una manera belicosa, de horcas y cuchillos, de lanza en ristre o de falcón en mano. Pero es lo más frecuente que te hiera con ojos tan serenos y sencillos como un arroyo fresco en el verano. Aldaba de noviembre Una tristeza dulce y anterior al suspiro y las lágrimas, anterior al idilio de la tarde azul y el jacaranda, invade la memoria con su música, su brisa, su nostalgia: Es la tristeza de mirar el cielo cautivo entre las ramas.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2012-12-16 09:05:49";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2012-12-16 09:05:49";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:16;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:27:{s:2:"id";s:3:"172";s:5:"alias";s:23:"antonio-carvajal-milena";s:7:"summary";s:7334:"<p style="text-align: center;"><strong><span style="font-size: 14pt;">CONFERENCIA INAUGURAL DE LA FERIA DEL LIBRO DEL I.E.S. MIGUEL DE CERVANTES A CARGO DE D. ANTONIO CARVAJAL MILENA</span>&nbsp;</strong></p>
<p style="text-align: center;"><strong> <img style="float: right;" src="actividades/imagenes/actualidad/a_carvajal.jpg" width="129" height="161" /><br /></strong></p>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25" align="justify"><span style="font-family: 'Times New Roman',Times,serif; color: #000033;" class="Estilo27"><img border="0" hspace="5" align="left" src="actividades/imagenes/actualidad/a_carvajal1.jpg" width="221" height="166" />Como ya viene siendo habitual en los últimos años y en el ámbito del vigente Plan de Lectura que desarrolla el I.E.S. Miguel de Cervantes, hemos vuelto a contar con un autor literario de relevancia para fomentar la lectura y el estudio entre nuestros alumnos. El día 10 de Noviembre y como acto inaugural de la feria del libro, tuvimos la suerte de contar con un escritor excepcional, el poeta D. Antonio Carvajal Milena. Sus principales características poéticas, notas biográficas y bibliográficas ya conocían los alumnos de 2º Bachillerato, a quienes iba dirigida su conferencia, por su trabajo previo en clase.&nbsp; Se trata de un poeta de resonancias clásicas y eruditas; gran maestro del lenguaje; todo un lujo para el centro y para los privilegiados oyentes que se encontraban en el S.U.M. a las 9’15 horas.<br /></span><br /><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Biografía </span><br /><span style="color: #000033; font-size: medium;" class="Estilo18"><img border="0" hspace="5" align="right" src="actividades/imagenes/actualidad/a_carvajal2.jpg" width="221" height="166" />Antonio Carvajal es doctor en Filología Románica por la Universidad de Granada y titular de Métrica. En 1990 recibió el&nbsp;<a title="Premio Nacional de la Crítica" href="http://enciclopedia.us.es/index.php/Premio_Nacional_de_la_Cr%C3%ADtica">Premio Nacional de la Crítica</a>.&nbsp;<br />Está considerado como uno de los poetas mayores de la actual poesía española. Sus libros han sido destacados como los más intensos y personales que han aparecido en las últimas décadas.&nbsp;<br />Notable renovador de la tradición poética andaluza, artífice de una versificación depurada e innovadora, ha seguido fielmente, desde su primer poemario, la línea de la poesía barroca junto con algunas aportaciones de las vanguardias. Así lo acreditan el uso de complicadas combinaciones estróficas, figuras retóricas de corte quevediano y gongorino y los planteamientos filosóficos de sus contenidos. Desde mediados de los años 80, junto a las estructuras cultas ya mencionadas, se pueden observar módulos de poesía popular.</span></p>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25"><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Obra </span><br /><span style="color: #000033; font-size: medium;" class="Estilo18">De entre su obras, destacan:</span><span style="color: #000033; font-size: medium;" class="Estilo18"><img border="0" hspace="100" align="right" src="actividades/imagenes/actualidad/a_carvajal3.jpg" width="221" height="166" /></span></p>
<ol style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25">
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Tigres en el jardín (1968)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Serenata y navaja (1973)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Siesta en el mirador (1979)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Servidumbre de paso (1982)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Del viento en los jazmines (1984)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Noticia de setiembre (1984)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>De un capricho celeste (1988)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Testimonio de invierno (1990)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Miradas sobre agua (1993)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Alma región luciente (1998)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Los pasos evocados (2004)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Una canción más clara (2008)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Cartas a los amigos (2009)</em></li>
</ol>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25"><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Selección<br /></span><br /><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">Siesta en el mirador&nbsp;<em>1968</em></span> <br />Sólo para tus labios mi sangre está madura,<br />con obsesión de estío preparada a tus besos,<br />siempre fiel a mis brazos y llena de hermosura,<br />exangües cada noche, y cada aurora ilesos.</p>
<p>Si crepitan los bosques de caza y aventura<br />y los pájaros altos burlan de vernos presos,<br />no dejes que tus ojos dibujen la amargura<br />de los que no han llevado el amor en los huesos.</p>
<p>Quédate entre mis brazos, que sólo a mí me tienes,<br />que los demás te odian, que el corazón te acecha<br />en los latidos cálidos del vientre y de las sienes.</p>
<p>Mira que no hay jardines más allá de este muro,<br />que es todo un largo olvido y si mi amor te estrecha<br />verás un cielo abierto detrás del llanto oscuro.</p>
<p><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">A veces el amor tiene caricias... </span><br />A veces el amor tiene caricias<br />frías, como navajas de barbero.<br />Cierras los ojos. Das tu cuello entero<br />a un peligroso filo de delicias.</p>
<p>Otras veces se clava como aguja<br />irisada de sedas en el raso<br />del bastidor: raso del lento ocaso<br />donde un cisne precoz se somorguja.</p>
<p>En general, adopta una manera<br />belicosa, de horcas y cuchillos,<br />de lanza en ristre o de falcón en mano.</p>
<p>Pero es lo más frecuente que te hiera<br />con ojos tan serenos y sencillos<br />como un arroyo fresco en el verano.</p>
<p><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">Aldaba de noviembre </span><br />Una tristeza dulce y anterior<br />al suspiro y las lágrimas,<br />anterior al idilio de la tarde<br />azul y el jacaranda,<br />invade la memoria con su música,<br />su brisa, su nostalgia:<br />Es la tristeza de mirar el cielo<br />cautivo entre las ramas.</p>";s:4:"body";s:0:"";s:5:"catid";s:3:"199";s:10:"created_by";s:3:"664";s:16:"created_by_alias";s:0:"";s:8:"modified";s:19:"0000-00-00 00:00:00";s:11:"modified_by";s:1:"0";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":71:{s:14:"article_layout";s:9:"_:default";s:10:"show_title";s:1:"1";s:11:"link_titles";s:1:"1";s:10:"show_intro";s:1:"1";s:13:"show_category";s:1:"1";s:13:"link_category";s:1:"1";s:20:"show_parent_category";s:1:"1";s:20:"link_parent_category";s:1:"1";s:11:"show_author";s:1:"1";s:11:"link_author";s:1:"0";s:16:"show_create_date";s:1:"0";s:16:"show_modify_date";s:1:"1";s:17:"show_publish_date";s:1:"0";s:20:"show_item_navigation";s:1:"1";s:9:"show_vote";s:1:"0";s:13:"show_readmore";s:1:"1";s:19:"show_readmore_title";s:1:"1";s:14:"readmore_limit";s:3:"100";s:10:"show_icons";s:1:"1";s:15:"show_print_icon";s:1:"1";s:15:"show_email_icon";s:1:"1";s:9:"show_hits";s:1:"1";s:11:"show_noauth";s:1:"0";s:13:"urls_position";s:1:"0";s:23:"show_publishing_options";s:1:"1";s:20:"show_article_options";s:1:"1";s:25:"show_urls_images_frontend";s:1:"0";s:24:"show_urls_images_backend";s:1:"1";s:7:"targeta";i:0;s:7:"targetb";i:0;s:7:"targetc";i:0;s:11:"float_intro";s:4:"left";s:14:"float_fulltext";s:4:"left";s:15:"category_layout";s:6:"_:blog";s:19:"show_category_title";s:1:"0";s:16:"show_description";s:1:"0";s:22:"show_description_image";s:1:"0";s:8:"maxLevel";s:1:"1";s:21:"show_empty_categories";s:1:"0";s:16:"show_no_articles";s:1:"1";s:16:"show_subcat_desc";s:1:"1";s:21:"show_cat_num_articles";s:1:"1";s:21:"show_base_description";s:1:"1";s:11:"maxLevelcat";s:2:"-1";s:25:"show_empty_categories_cat";s:1:"0";s:20:"show_subcat_desc_cat";s:1:"1";s:25:"show_cat_num_articles_cat";s:1:"1";s:20:"num_leading_articles";s:1:"3";s:18:"num_intro_articles";s:1:"4";s:11:"num_columns";s:1:"2";s:9:"num_links";s:1:"4";s:18:"multi_column_order";s:1:"0";s:24:"show_subcategory_content";s:1:"0";s:21:"show_pagination_limit";s:1:"1";s:12:"filter_field";s:4:"hide";s:13:"show_headings";s:1:"1";s:14:"list_show_date";s:1:"0";s:11:"date_format";s:0:"";s:14:"list_show_hits";s:1:"1";s:16:"list_show_author";s:1:"1";s:11:"orderby_pri";s:5:"order";s:11:"orderby_sec";s:5:"rdate";s:10:"order_date";s:9:"published";s:15:"show_pagination";s:1:"2";s:23:"show_pagination_results";s:1:"1";s:14:"show_feed_link";s:1:"1";s:12:"feed_summary";s:1:"0";s:18:"feed_show_readmore";s:1:"0";s:12:"show_section";i:0;s:12:"link_section";i:0;s:8:"language";s:5:"es-ES";}}s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":4:{s:6:"robots";s:0:"";s:6:"author";s:0:"";s:6:"rights";s:0:"";s:10:"xreference";s:0:"";}}s:7:"version";s:1:"1";s:8:"ordering";s:1:"8";s:8:"category";s:11:"Actividades";s:9:"cat_state";s:1:"1";s:10:"cat_access";s:1:"1";s:4:"slug";s:27:"172:antonio-carvajal-milena";s:7:"catslug";s:15:"199:actividades";s:6:"author";s:10:"Super User";s:4:"mime";N;s:6:"layout";s:7:"article";s:4:"path";s:104:"index.php?option=com_content&view=article&id=172:antonio-carvajal-milena&catid=199:actividades&Itemid=76";s:10:"metaauthor";N;s:6:"weight";d:0.51331000000000004;s:7:"link_id";i:532;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:7:"metakey";i:4;s:8:"metadesc";i:5;s:10:"metaauthor";i:6;s:6:"author";i:7;s:16:"created_by_alias";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:4:{s:4:"Type";a:1:{s:7:"Article";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:7:"Article";s:5:"state";i:1;s:6:"access";i:1;}}s:6:"Author";a:1:{s:10:"Super User";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:10:"Super User";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Category";a:1:{s:11:"Actividades";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:11:"Actividades";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=article&id=172";s:5:"route";s:104:"index.php?option=com_content&view=article&id=172:antonio-carvajal-milena&catid=199:actividades&Itemid=76";s:5:"title";s:23:"Antonio Carvajal Milena";s:11:"description";s:3831:"CONFERENCIA INAUGURAL DE LA FERIA DEL LIBRO DEL I.E.S. MIGUEL DE CERVANTES A CARGO DE D. ANTONIO CARVAJAL MILENA Como ya viene siendo habitual en los últimos años y en el ámbito del vigente Plan de Lectura que desarrolla el I.E.S. Miguel de Cervantes, hemos vuelto a contar con un autor literario de relevancia para fomentar la lectura y el estudio entre nuestros alumnos. El día 10 de Noviembre y como acto inaugural de la feria del libro, tuvimos la suerte de contar con un escritor excepcional, el poeta D. Antonio Carvajal Milena. Sus principales características poéticas, notas biográficas y bibliográficas ya conocían los alumnos de 2º Bachillerato, a quienes iba dirigida su conferencia, por su trabajo previo en clase. Se trata de un poeta de resonancias clásicas y eruditas; gran maestro del lenguaje; todo un lujo para el centro y para los privilegiados oyentes que se encontraban en el S.U.M. a las 9’15 horas. Biografía Antonio Carvajal es doctor en Filología Románica por la Universidad de Granada y titular de Métrica. En 1990 recibió elhref="http://enciclopedia.us.es/index.php/Premio_Nacional_de_la_Cr%C3%ADtica"> Premio Nacional de la Crítica . Está considerado como uno de los poetas mayores de la actual poesía española. Sus libros han sido destacados como los más intensos y personales que han aparecido en las últimas décadas. Notable renovador de la tradición poética andaluza, artífice de una versificación depurada e innovadora, ha seguido fielmente, desde su primer poemario, la línea de la poesía barroca junto con algunas aportaciones de las vanguardias. Así lo acreditan el uso de complicadas combinaciones estróficas, figuras retóricas de corte quevediano y gongorino y los planteamientos filosóficos de sus contenidos. Desde mediados de los años 80, junto a las estructuras cultas ya mencionadas, se pueden observar módulos de poesía popular. Obra De entre su obras, destacan: Tigres en el jardín (1968) Serenata y navaja (1973) Siesta en el mirador (1979) Servidumbre de paso (1982) Del viento en los jazmines (1984)#000033; font-size: medium;" class="Estilo18"> Noticia de setiembre (1984) De un capricho celeste (1988) Testimonio de invierno (1990) Miradas sobre agua (1993) Alma región luciente (1998) Los pasos evocados (2004) Una canción más clara (2008) Cartas a los amigos (2009) Selección Siesta en el mirador 1968 Sólo para tus labios mi sangre está madura, con obsesión de estío preparada a tus besos, siempre fiel a mis brazos y llena de hermosura, exangües cada noche, y cada aurora ilesos. Si crepitan los bosques de caza y aventura y los pájaros altos burlan de vernos presos, no dejes que tus ojos dibujen la amargura de los que no han llevado el amor en los huesos. Quédate entre mis brazos, que sólo a mí me tienes, que los demás te odian, que el corazón te acecha en los latidos cálidos del vientre y de las sienes. Mira que no hay jardines más allá de este muro, que es todo un largo olvido y si mi amor te estrecha verás un cielo abierto detrás del llantooscuro. A veces el amor tiene caricias... A veces el amor tiene caricias frías, como navajas de barbero. Cierras los ojos. Das tu cuello entero a un peligroso filo de delicias. Otras veces se clava como aguja irisada de sedas en el raso del bastidor: raso del lento ocaso donde un cisne precoz se somorguja. En general, adopta una manera belicosa, de horcas y cuchillos, de lanza en ristre o de falcón en mano. Pero es lo más frecuente que te hiera con ojos tan serenos y sencillos como un arroyo fresco en el verano. Aldaba de noviembre Una tristeza dulce y anterior al suspiro y las lágrimas, anterior al idilio de la tarde azul y el jacaranda, invade la memoria con su música, su brisa, su nostalgia: Es la tristeza de mirar el cielo cautivo entre las ramas.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"2012-12-16 09:05:25";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"2012-12-16 09:05:25";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:3;}i:17;O:19:"FinderIndexerResult":18:{s:11:" * elements";a:21:{s:2:"id";s:2:"89";s:5:"alias";s:23:"antonio-carvajal-milena";s:7:"summary";s:7334:"<p style="text-align: center;"><strong><span style="font-size: 14pt;">CONFERENCIA INAUGURAL DE LA FERIA DEL LIBRO DEL I.E.S. MIGUEL DE CERVANTES A CARGO DE D. ANTONIO CARVAJAL MILENA</span>&nbsp;</strong></p>
<p style="text-align: center;"><strong> <img style="float: right;" src="actividades/imagenes/actualidad/a_carvajal.jpg" width="129" height="161" /><br /></strong></p>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25" align="justify"><span style="font-family: 'Times New Roman',Times,serif; color: #000033;" class="Estilo27"><img border="0" hspace="5" align="left" src="actividades/imagenes/actualidad/a_carvajal1.jpg" width="221" height="166" />Como ya viene siendo habitual en los últimos años y en el ámbito del vigente Plan de Lectura que desarrolla el I.E.S. Miguel de Cervantes, hemos vuelto a contar con un autor literario de relevancia para fomentar la lectura y el estudio entre nuestros alumnos. El día 10 de Noviembre y como acto inaugural de la feria del libro, tuvimos la suerte de contar con un escritor excepcional, el poeta D. Antonio Carvajal Milena. Sus principales características poéticas, notas biográficas y bibliográficas ya conocían los alumnos de 2º Bachillerato, a quienes iba dirigida su conferencia, por su trabajo previo en clase.&nbsp; Se trata de un poeta de resonancias clásicas y eruditas; gran maestro del lenguaje; todo un lujo para el centro y para los privilegiados oyentes que se encontraban en el S.U.M. a las 9’15 horas.<br /></span><br /><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Biografía </span><br /><span style="color: #000033; font-size: medium;" class="Estilo18"><img border="0" hspace="5" align="right" src="actividades/imagenes/actualidad/a_carvajal2.jpg" width="221" height="166" />Antonio Carvajal es doctor en Filología Románica por la Universidad de Granada y titular de Métrica. En 1990 recibió el&nbsp;<a title="Premio Nacional de la Crítica" href="http://enciclopedia.us.es/index.php/Premio_Nacional_de_la_Cr%C3%ADtica">Premio Nacional de la Crítica</a>.&nbsp;<br />Está considerado como uno de los poetas mayores de la actual poesía española. Sus libros han sido destacados como los más intensos y personales que han aparecido en las últimas décadas.&nbsp;<br />Notable renovador de la tradición poética andaluza, artífice de una versificación depurada e innovadora, ha seguido fielmente, desde su primer poemario, la línea de la poesía barroca junto con algunas aportaciones de las vanguardias. Así lo acreditan el uso de complicadas combinaciones estróficas, figuras retóricas de corte quevediano y gongorino y los planteamientos filosóficos de sus contenidos. Desde mediados de los años 80, junto a las estructuras cultas ya mencionadas, se pueden observar módulos de poesía popular.</span></p>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25"><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Obra </span><br /><span style="color: #000033; font-size: medium;" class="Estilo18">De entre su obras, destacan:</span><span style="color: #000033; font-size: medium;" class="Estilo18"><img border="0" hspace="100" align="right" src="actividades/imagenes/actualidad/a_carvajal3.jpg" width="221" height="166" /></span></p>
<ol style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25">
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Tigres en el jardín (1968)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Serenata y navaja (1973)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Siesta en el mirador (1979)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Servidumbre de paso (1982)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Del viento en los jazmines (1984)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Noticia de setiembre (1984)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>De un capricho celeste (1988)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Testimonio de invierno (1990)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Miradas sobre agua (1993)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Alma región luciente (1998)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Los pasos evocados (2004)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Una canción más clara (2008)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Cartas a los amigos (2009)</em></li>
</ol>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25"><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Selección<br /></span><br /><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">Siesta en el mirador&nbsp;<em>1968</em></span> <br />Sólo para tus labios mi sangre está madura,<br />con obsesión de estío preparada a tus besos,<br />siempre fiel a mis brazos y llena de hermosura,<br />exangües cada noche, y cada aurora ilesos.</p>
<p>Si crepitan los bosques de caza y aventura<br />y los pájaros altos burlan de vernos presos,<br />no dejes que tus ojos dibujen la amargura<br />de los que no han llevado el amor en los huesos.</p>
<p>Quédate entre mis brazos, que sólo a mí me tienes,<br />que los demás te odian, que el corazón te acecha<br />en los latidos cálidos del vientre y de las sienes.</p>
<p>Mira que no hay jardines más allá de este muro,<br />que es todo un largo olvido y si mi amor te estrecha<br />verás un cielo abierto detrás del llanto oscuro.</p>
<p><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">A veces el amor tiene caricias... </span><br />A veces el amor tiene caricias<br />frías, como navajas de barbero.<br />Cierras los ojos. Das tu cuello entero<br />a un peligroso filo de delicias.</p>
<p>Otras veces se clava como aguja<br />irisada de sedas en el raso<br />del bastidor: raso del lento ocaso<br />donde un cisne precoz se somorguja.</p>
<p>En general, adopta una manera<br />belicosa, de horcas y cuchillos,<br />de lanza en ristre o de falcón en mano.</p>
<p>Pero es lo más frecuente que te hiera<br />con ojos tan serenos y sencillos<br />como un arroyo fresco en el verano.</p>
<p><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">Aldaba de noviembre </span><br />Una tristeza dulce y anterior<br />al suspiro y las lágrimas,<br />anterior al idilio de la tarde<br />azul y el jacaranda,<br />invade la memoria con su música,<br />su brisa, su nostalgia:<br />Es la tristeza de mirar el cielo<br />cautivo entre las ramas.</p>";s:9:"extension";s:11:"com_content";s:10:"created_by";s:1:"0";s:8:"modified";s:19:"2012-12-16 08:52:20";s:11:"modified_by";s:3:"664";s:7:"metakey";s:0:"";s:8:"metadesc";s:0:"";s:8:"metadata";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":2:{s:6:"author";s:0:"";s:6:"robots";s:0:"";}}s:3:"lft";s:3:"138";s:9:"parent_id";s:3:"199";s:5:"level";s:1:"2";s:6:"params";O:9:"JRegistry":1:{s:7:" * data";O:8:"stdClass":2:{s:15:"category_layout";s:0:"";s:5:"image";s:0:"";}}s:4:"slug";s:26:"89:antonio-carvajal-milena";s:4:"mime";N;s:6:"layout";s:8:"category";s:10:"metaauthor";N;s:4:"path";s:58:"index.php?option=com_content&view=category&id=89&Itemid=76";s:6:"weight";d:0.51331000000000004;s:7:"link_id";i:416;}s:15:" * instructions";a:5:{i:1;a:3:{i:0;s:5:"title";i:1;s:8:"subtitle";i:2;s:2:"id";}i:2;a:2:{i:0;s:7:"summary";i:1;s:4:"body";}i:3;a:8:{i:0;s:4:"meta";i:1;s:10:"list_price";i:2;s:10:"sale_price";i:3;s:4:"link";i:4;s:7:"metakey";i:5;s:8:"metadesc";i:6;s:10:"metaauthor";i:7;s:6:"author";}i:4;a:2:{i:0;s:4:"path";i:1;s:5:"alias";}i:5;a:1:{i:0;s:8:"comments";}}s:11:" * taxonomy";a:2:{s:4:"Type";a:1:{s:8:"Category";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:8:"Category";s:5:"state";i:1;s:6:"access";i:1;}}s:8:"Language";a:1:{s:1:"*";O:7:"JObject":4:{s:10:" * _errors";a:0:{}s:5:"title";s:1:"*";s:5:"state";i:1;s:6:"access";i:1;}}}s:3:"url";s:48:"index.php?option=com_content&view=category&id=89";s:5:"route";s:58:"index.php?option=com_content&view=category&id=89&Itemid=76";s:5:"title";s:23:"Antonio Carvajal Milena";s:11:"description";s:3831:"CONFERENCIA INAUGURAL DE LA FERIA DEL LIBRO DEL I.E.S. MIGUEL DE CERVANTES A CARGO DE D. ANTONIO CARVAJAL MILENA Como ya viene siendo habitual en los últimos años y en el ámbito del vigente Plan de Lectura que desarrolla el I.E.S. Miguel de Cervantes, hemos vuelto a contar con un autor literario de relevancia para fomentar la lectura y el estudio entre nuestros alumnos. El día 10 de Noviembre y como acto inaugural de la feria del libro, tuvimos la suerte de contar con un escritor excepcional, el poeta D. Antonio Carvajal Milena. Sus principales características poéticas, notas biográficas y bibliográficas ya conocían los alumnos de 2º Bachillerato, a quienes iba dirigida su conferencia, por su trabajo previo en clase. Se trata de un poeta de resonancias clásicas y eruditas; gran maestro del lenguaje; todo un lujo para el centro y para los privilegiados oyentes que se encontraban en el S.U.M. a las 9’15 horas. Biografía Antonio Carvajal es doctor en Filología Románica por la Universidad de Granada y titular de Métrica. En 1990 recibió elhref="http://enciclopedia.us.es/index.php/Premio_Nacional_de_la_Cr%C3%ADtica"> Premio Nacional de la Crítica . Está considerado como uno de los poetas mayores de la actual poesía española. Sus libros han sido destacados como los más intensos y personales que han aparecido en las últimas décadas. Notable renovador de la tradición poética andaluza, artífice de una versificación depurada e innovadora, ha seguido fielmente, desde su primer poemario, la línea de la poesía barroca junto con algunas aportaciones de las vanguardias. Así lo acreditan el uso de complicadas combinaciones estróficas, figuras retóricas de corte quevediano y gongorino y los planteamientos filosóficos de sus contenidos. Desde mediados de los años 80, junto a las estructuras cultas ya mencionadas, se pueden observar módulos de poesía popular. Obra De entre su obras, destacan: Tigres en el jardín (1968) Serenata y navaja (1973) Siesta en el mirador (1979) Servidumbre de paso (1982) Del viento en los jazmines (1984)#000033; font-size: medium;" class="Estilo18"> Noticia de setiembre (1984) De un capricho celeste (1988) Testimonio de invierno (1990) Miradas sobre agua (1993) Alma región luciente (1998) Los pasos evocados (2004) Una canción más clara (2008) Cartas a los amigos (2009) Selección Siesta en el mirador 1968 Sólo para tus labios mi sangre está madura, con obsesión de estío preparada a tus besos, siempre fiel a mis brazos y llena de hermosura, exangües cada noche, y cada aurora ilesos. Si crepitan los bosques de caza y aventura y los pájaros altos burlan de vernos presos, no dejes que tus ojos dibujen la amargura de los que no han llevado el amor en los huesos. Quédate entre mis brazos, que sólo a mí me tienes, que los demás te odian, que el corazón te acecha en los latidos cálidos del vientre y de las sienes. Mira que no hay jardines más allá de este muro, que es todo un largo olvido y si mi amor te estrecha verás un cielo abierto detrás del llantooscuro. A veces el amor tiene caricias... A veces el amor tiene caricias frías, como navajas de barbero. Cierras los ojos. Das tu cuello entero a un peligroso filo de delicias. Otras veces se clava como aguja irisada de sedas en el raso del bastidor: raso del lento ocaso donde un cisne precoz se somorguja. En general, adopta una manera belicosa, de horcas y cuchillos, de lanza en ristre o de falcón en mano. Pero es lo más frecuente que te hiera con ojos tan serenos y sencillos como un arroyo fresco en el verano. Aldaba de noviembre Una tristeza dulce y anterior al suspiro y las lágrimas, anterior al idilio de la tarde azul y el jacaranda, invade la memoria con su música, su brisa, su nostalgia: Es la tristeza de mirar el cielo cautivo entre las ramas.";s:9:"published";N;s:5:"state";i:1;s:6:"access";s:1:"1";s:8:"language";s:1:"*";s:18:"publish_start_date";s:19:"0000-00-00 00:00:00";s:16:"publish_end_date";s:19:"0000-00-00 00:00:00";s:10:"start_date";s:19:"0000-00-00 00:00:00";s:8:"end_date";s:19:"0000-00-00 00:00:00";s:10:"list_price";N;s:10:"sale_price";N;s:7:"type_id";i:1;}}";
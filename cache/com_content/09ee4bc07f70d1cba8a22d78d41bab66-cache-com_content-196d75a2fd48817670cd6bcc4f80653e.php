<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9463:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Antonio Carvajal</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Antonio Carvajal Milena</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 16 Diciembre 2012 09:19</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=173:milena&amp;catid=199&amp;Itemid=77&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=f391c8324cb3e26c90c9c07a00c8bf1fa96f825d" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2649
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p style="text-align: center;"><strong><span style="font-size: 14pt;">CONFERENCIA INAUGURAL DE LA FERIA DEL LIBRO DEL I.E.S. MIGUEL DE CERVANTES A CARGO DE D. ANTONIO CARVAJAL MILENA</span>&nbsp;</strong></p>
<p style="text-align: center;"><strong> <img style="float: right;" src="archivos_ies/actividades/imagenes/actualidad/a_carvajal.jpg" width="129" height="161" /><br /></strong></p>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25" align="justify"><span style="font-family: 'Times New Roman',Times,serif; color: #000033;" class="Estilo27"><img border="0" hspace="5" align="left" src="archivos_ies/actividades/imagenes/actualidad/a_carvajal1.jpg" width="221" height="166" />Como ya viene siendo habitual en los últimos años y en el ámbito del vigente Plan de Lectura que desarrolla el I.E.S. Miguel de Cervantes, hemos vuelto a contar con un autor literario de relevancia para fomentar la lectura y el estudio entre nuestros alumnos. El día 10 de Noviembre y como acto inaugural de la feria del libro, tuvimos la suerte de contar con un escritor excepcional, el poeta D. Antonio Carvajal Milena. Sus principales características poéticas, notas biográficas y bibliográficas ya conocían los alumnos de 2º Bachillerato, a quienes iba dirigida su conferencia, por su trabajo previo en clase.&nbsp; Se trata de un poeta de resonancias clásicas y eruditas; gran maestro del lenguaje; todo un lujo para el centro y para los privilegiados oyentes que se encontraban en el S.U.M. a las 9’15 horas.<br /></span><br /><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Biografía </span><br /><span style="color: #000033; font-size: medium;" class="Estilo18"><img border="0" hspace="5" align="right" src="archivos_ies/actividades/imagenes/actualidad/a_carvajal2.jpg" width="221" height="166" />Antonio Carvajal es doctor en Filología Románica por la Universidad de Granada y titular de Métrica. En 1990 recibió el&nbsp;<a title="Premio Nacional de la Crítica" href="http://enciclopedia.us.es/index.php/Premio_Nacional_de_la_Cr%C3%ADtica">Premio Nacional de la Crítica</a>.&nbsp;<br />Está considerado como uno de los poetas mayores de la actual poesía española. Sus libros han sido destacados como los más intensos y personales que han aparecido en las últimas décadas.&nbsp;<br />Notable renovador de la tradición poética andaluza, artífice de una versificación depurada e innovadora, ha seguido fielmente, desde su primer poemario, la línea de la poesía barroca junto con algunas aportaciones de las vanguardias. Así lo acreditan el uso de complicadas combinaciones estróficas, figuras retóricas de corte quevediano y gongorino y los planteamientos filosóficos de sus contenidos. Desde mediados de los años 80, junto a las estructuras cultas ya mencionadas, se pueden observar módulos de poesía popular.</span></p>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25"><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Obra </span><br /><span style="color: #000033; font-size: medium;" class="Estilo18">De entre su obras, destacan:</span><span style="color: #000033; font-size: medium;" class="Estilo18"><img border="0" hspace="100" align="right" src="archivos_ies/actividades/imagenes/actualidad/a_carvajal3.jpg" width="221" height="166" /></span></p>
<ol style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25">
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Tigres en el jardín (1968)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Serenata y navaja (1973)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Siesta en el mirador (1979)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Servidumbre de paso (1982)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Del viento en los jazmines (1984)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Noticia de setiembre (1984)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>De un capricho celeste (1988)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Testimonio de invierno (1990)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Miradas sobre agua (1993)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Alma región luciente (1998)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Los pasos evocados (2004)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Una canción más clara (2008)</em></li>
<li style="color: #000033; font-size: medium;" class="Estilo18"><em>Cartas a los amigos (2009)</em></li>
</ol>
<p style="font-size: medium; font-family: 'Times New Roman';" class="Estilo25"><span style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: large; font-weight: bold;" class="Estilo29">Selección<br /></span><br /><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">Siesta en el mirador&nbsp;<em>1968</em></span> <br />Sólo para tus labios mi sangre está madura,<br />con obsesión de estío preparada a tus besos,<br />siempre fiel a mis brazos y llena de hermosura,<br />exangües cada noche, y cada aurora ilesos.</p>
<p>Si crepitan los bosques de caza y aventura<br />y los pájaros altos burlan de vernos presos,<br />no dejes que tus ojos dibujen la amargura<br />de los que no han llevado el amor en los huesos.</p>
<p>Quédate entre mis brazos, que sólo a mí me tienes,<br />que los demás te odian, que el corazón te acecha<br />en los latidos cálidos del vientre y de las sienes.</p>
<p>Mira que no hay jardines más allá de este muro,<br />que es todo un largo olvido y si mi amor te estrecha<br />verás un cielo abierto detrás del llanto oscuro.</p>
<p><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">A veces el amor tiene caricias... </span><br />A veces el amor tiene caricias<br />frías, como navajas de barbero.<br />Cierras los ojos. Das tu cuello entero<br />a un peligroso filo de delicias.</p>
<p>Otras veces se clava como aguja<br />irisada de sedas en el raso<br />del bastidor: raso del lento ocaso<br />donde un cisne precoz se somorguja.</p>
<p>En general, adopta una manera<br />belicosa, de horcas y cuchillos,<br />de lanza en ristre o de falcón en mano.</p>
<p>Pero es lo más frecuente que te hiera<br />con ojos tan serenos y sencillos<br />como un arroyo fresco en el verano.</p>
<p><span style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24">Aldaba de noviembre </span><br />Una tristeza dulce y anterior<br />al suspiro y las lágrimas,<br />anterior al idilio de la tarde<br />azul y el jacaranda,<br />invade la memoria con su música,<br />su brisa, su nostalgia:<br />Es la tristeza de mirar el cielo<br />cautivo entre las ramas.</p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Actividades</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:67:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Antonio Carvajal";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:16:"Antonio Carvajal";s:4:"link";s:58:"index.php?option=com_content&view=article&id=173&Itemid=77";}}s:6:"module";a:0:{}}
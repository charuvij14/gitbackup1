<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:35025:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postcontent">
<div class="art-article"><h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Pendientes de Matemáticas.</span></h2>
<p style="padding-left: 30px;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;"><a href="archivos_ies/curso1314/HORARIO_INICIO_CURSO_2013-14.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></span></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Historia de Grecia, 1º de Bachillerato (23/09/13)</span></h2>
<p style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff; padding-left: 30px;"><a href="https://www.youtube.com/watch?v=cyvNgDMZEdw" target="_blank" style="color: #1155cc;">Ver vídeo</a></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Actualizado el temario de Matemáticas de 1º y 3º ESO bilingües para poder imprimirlo.</span></h2>
<p style="padding-left: 30px; color: #2e2a23; text-align: justify; background-color: #fbe9c5;"><a href="index.php?option=com_content&amp;view=article&amp;id=171&amp;Itemid=239" target="_blank" style="color: #1155cc;">Entrar en zona privada de bilingües.</a></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Horario de inicio de curso, día 16/09/13</span></h2>
<p style="margin: 0cm 0cm 6pt; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><a href="archivos_ies/curso1314/HORARIO_INICIO_CURSO_2013-14.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></p>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Decreto Formación Inicial y Permanente</span></h2>
<p style="margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff; line-height: 12pt;" align="left">Decreto 93/2013, de 27 de agosto, por el que se regula la formación inicial y permanente del profesorado en la Comunidad Autónoma de Andalucía, así como el Sistema Andaluz de Formación Permanente del Profesorado.</p>
<p style="margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff; line-height: 12pt;" align="left"><a href="archivos_ies/13_14/ver_decreto_39827.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></p>
<p style="margin-right: 0cm; margin-bottom: 6pt; margin-left: 0cm; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff; line-height: 12pt;" align="left"> </p>
<p style="margin: 0cm 0cm 0.0001pt; text-align: justify; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><strong><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;">Orden de 5 de julio de 2013, que regula las ausencias del profesorado por enfermedad o accidente que no den lugar a incapacidad temporal</span></strong></p>
<p style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"> </p>
<p style="margin: 0cm 0cm 0.0001pt; padding-left: 30px; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><a href="archivos_ies/13_14/ver_orden_90644.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></p>
<p style="margin: 0cm 0cm 0.0001pt; padding-left: 30px; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><strong style="text-align: justify;"><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;"> </span></strong></p>
<p style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><strong style="text-align: justify;"><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;">Día lectivos y festivos en el Curso Escolar 2013/14</span></strong></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><a href="archivos_ies/13_14/Calendario_2013-2014.pdf" target="_blank" style="color: #1155cc;"><br />Ver enlace</a></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"> </p>
<p style="margin: 0cm 0cm 0.0001pt; text-indent: 0cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;" align="left"><strong style="text-align: justify;"><span style="font-size: 14pt; font-family: Tahoma, sans-serif; color: green;">Legislación actualizada a 05/09/13</span></strong></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"><a href="archivos_ies/13_14/lex" target="_blank" style="color: #1155cc;"><br />Ver enlace</a></p>
<p style="margin: 0cm 0cm 6pt; padding-left: 30px; text-align: justify; text-indent: 1cm; font-size: 11pt; font-family: Arial, sans-serif; color: #222222; background-color: #ffffff;"> </p>
<table style="border: 0px solid #efbc38; width: 100%; background-color: #ffffff;" border="0" cellspacing="5" cellpadding="5" align="center">
<tbody>
<tr style="border-color: #8799f9; border-bottom: 1px solid;" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="3" align="center"><span style="color: #ffffff;"><strong><span style="font-size: 12pt;">CURSO </span></strong><strong><span style="font-size: 12pt;"> 2012/13</span></strong></span></td>
</tr>
<tr>
<td class="textogris" style="text-align: center; border-color: #8799f9;" colspan="2">
<ul>
<li style="text-align: justify;"><span style="color: #484137;"><strong>20/02/13.  <a href="http://paleorama.wordpress.com/2013/02/19/entrevista-al-filologo-antoni-biosca-sobre-la-importancia-del-latin/" target="_blank">Entrevista al filólogo Antoni Biosca sobre la importancia del Latín.</a></strong></span></li>
<li style="text-align: justify;"><span style="color: #484137;"><strong>07/02/13.  <a href="index.php?option=com_content&amp;view=article&amp;id=193">Publicada nueva normativa en materia de permisos y licencias.</a></strong></span></li>
<li style="text-align: justify;"><strong>07/02/13:  Excelente web para <a href="index.php?option=com_content&amp;view=article&amp;id=194" target="_self">aprender jugando</a>, todas las asignaturas, todos los niveles.</strong></li>
<li style="text-align: justify;"><span style="color: #484137;"><strong>05/02/13.  </strong></span>Hoy, 5 de febrero, se celebra el <a href="http://www.osi.es/es/actualidad/blog/2013/02/05/conectate-y-respeta-netiquetate" target="_blank">Día Internacional de la Internet Segura. </a></li>
<li style="text-align: justify;"><span style="color: #484137;"><strong>04/02/13.   </strong></span><a href="archivos_ies/1_Ev_Analisiscuantitativo_2012_13.pdf" target="_blank"><strong>Datos, 1ª Evaluación 2012/13.</strong></a></li>
<li style="text-align: justify;"><span><strong>16/01/13. </strong></span><strong><a href="archivos_ies/PROGRAMA_ACE_POR_TRIMESTRES.xlsx" target="_blank" style="color: #655c4e;">Programa de las Actividades Complementarias y Extraescolares por trimestres.</a></strong></li>
<li style="text-align: justify;"><span style="text-decoration: underline;"><strong><span style="color: #0066cc; text-decoration: underline;">08/01/13.</span></strong></span><span style="color: #0066cc;">  </span><span style="text-decoration: underline;"><strong><span style="color: #0066cc; text-decoration: underline;">REANUDACIÓN</span><span style="color: #0066cc; text-decoration: underline;"> DE LAS CLASES EN SU HORARIO NORMAL, A PARTIR DE LAS 08.15.</span></strong></span></li>
<li style="text-align: justify;"></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">13/12/12.  </span></strong><strong><span style="color: #0066cc;"><a href="archivos_ies/TENSION_EN_LAS_AULAS.pdf">TENSION EN LAS AULAS.</a> Aportación del Departamento de Latín y Griego.</span></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">07/11/12. </span></strong><span style="color: #0066cc;"><strong><a href="archivos_ies/ENSENANZA_Y_CONOCIMIENTO.doc" target="_blank">ENSEÑANZA Y CONOCIMIENTO.</a> Aportación del Departamento de Latín y Griego.</strong></span></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">23/10/12. </span><a href="http://www.piiisa.es/" target="_blank">PROYECTO PIIISA 2012: Ciencia para jóvenes estudiantes  www.piiisa.es</a></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">16/09/12. <a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_examenes_anteriores.php" target="_blank">ORIENTACIONES OFICIALES DE LA UNIVERSIDAD SELECTIVIDAD 2012/13.</a></span></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">15/09/12</span></strong>. <a href="archivos_ies/lex/" target="_blank">LEGISLACIÓN.</a></li>
<li style="text-align: left;"><strong style="color: #339966; text-align: justify;">11/09/12.</strong> <a href="archivos_ies/Manual_funcionamiento_SGD.pdf" target="_blank"><strong style="color: #339966; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: justify;">Manual de funcionamiento de la unidad personal de SGD</strong>.</a> (Para el profesorado)</li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">29/06/12.  <a href="archivos_ies/libros_1_bach_2012-2013corregido.pdf" target="_blank" style="text-decoration: none; color: #0066cc;">Libros de 1º Bachillerato para el curso 2012/13.</a></span></strong></li>
<li style="text-align: justify;"><strong><strong><span style="color: #0066cc;">29/06/12.  <span style="color: #0066cc;"><a href="archivos_ies/libros_2_bach_2012-2013.pdf" style="text-decoration: none; color: #0066cc;">Libros de 2º Bachillerato para el curso 2012/13.</a></span></span></strong> </strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">25/06/12.</span> <a href="archivos_ies/Calendario_escolar_2012_13.pdf" target="_blank" style="text-decoration: none;"><span style="color: #0066cc;">Calendario Escolar 2012/13.</span></a></strong></li>
<li style="text-align: justify;"><strong><strong><span style="color: #0066cc;">25/06/12. </span> <a href="archivos_ies/Instrucciones_calendario_escolar_2012_13.pdf" target="_blank" style="text-decoration: none;"><span style="color: #0066cc;">Instrucciones del Calendario Escolar 2012/13.</span></a></strong> </strong></li>
<li style="text-align: justify;"><strong>22/06/12. </strong><a href="archivos_ies/Examenes_selectividad_junio_2012_oficiales.pdf" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Exámenes que han salido en Selectividad junio 2012.</a></li>
<li style="text-align: justify;"><a href="archivos_ies/PROYECTO_EDUCATIVO_IES_MIGUEL_DE_CERVANTES_definitivo_2011_12.pdf" target="_blank" title="PROYECTO EDUCATIVO IES MIGUEL DE CERVANTES 2011-12." style="text-decoration: none; color: #c6c37b; text-align: center;">ACTUALIZADO: PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES 2011-12.</a></li>
<li style="text-align: justify;"><span style="font-size: 10pt;"><strong>Consulta de los libros de la Biblioteca:   <a href="abiex/index.php" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Formato 1</a> -   <a href="tmp/index.php" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Formato 2</a> - </strong><a href="libros/index.php" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">Formato 3</a></span></li>
<li style="text-align: justify;"><span style="font-size: 10pt;"><strong>Enlaces a <a href="index.php?option=com_content&amp;view=article&amp;id=153&amp;Itemid=231" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">bibliotecas y libros virtuales e interactivos.</a></strong></span></li>
<li style="text-align: justify;"><span style="font-size: 10pt;"><strong>Actualizados los criterios de evaluación, consultar los Departamentos.</strong></span></li>
<li style="text-align: justify;"><span style="font-size: 10pt;">Criterios que falten de 2º de Bachiller sobre Selectividad, consultar <a href="http://www.ujaen.es/" target="_blank" style="text-decoration: none; color: #c6c37b; text-align: center;">www.ujaen.es</a></span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<p> </p>
<table style="border: 0px solid #efbc38; width: 100%; background-color: #ffffff;" border="0" cellspacing="3" cellpadding="1" align="center">
<tbody>
<tr style="border-color: #8799f9; border-bottom: 1px solid;" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; text-align: center;" align="center"><span style="color: #ffffff;"><strong><span style="font-size: 12pt;"> Carteles sobre el invierno (Maestros y aprendices)</span></strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9;">
<td class="textogris" style="text-align: center; border-color: #8799f9;">
<div style="text-align: center;" align="center;">
<table border="0" align="center">
<tbody>
<tr>
<td>
<div style="text-align: center;" align="center;"><strong><span style="font-family: book antiqua,palatino; font-size: 12pt;">Cartel invierno</span></strong></div>
<div style="text-align: right;"><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Cartel_invierno.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Cartel_invierno.jpg" border="0" alt="Cartel invierno." width="179" height="117" /></a></span></div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Horacio (latín)</strong></span></div>
<div style="text-align: center;"><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Horacio_latin.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Horacio.jpg" border="0" alt="Horacio." width="164" height="127" /></a></span></div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong> Irene Centeno </strong></span></div>
<div style="text-align: center;"><span style="font-family: book antiqua,palatino; font-size: 12pt;"> <a href="archivos_ies/lengua/invierno/Irene_Centeno.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Irene_Centeno.jpg" border="0" alt="Irene Centeno" width="195" height="86" /></a></span></div>
<div style="text-align: center;"> </div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Paul Verlain 1</strong></span></div>
<div style="text-align: center;"><span style="text-align: center;"><a href="archivos_ies/lengua/invierno/Paul_Verlain_1.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/Paul_Verlain_1.jpg" border="0" alt="Paul Verlain 1." width="136" height="159" /></a></span></div>
<div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Paul Verlain 2</strong></span></div>
<div style="text-align: center;"><span style="text-align: center;"><a href="archivos_ies/lengua/invierno/Paul_Verlain_2.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Paul_Verlain_2.jpg" border="0" alt="Paul Verlain 2." width="197" height="79" /></a></span></div>
</div>
</td>
<td>
<div>
<div style="text-align: center;"><strong><span style="font-family: book antiqua,palatino; font-size: 12pt;">Jardín de invierno (P. Neruda)</span></strong></div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Jardin_de_invierno_P_Neruda.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Jardin_de_invierno.jpg" border="0" alt="Jardín de invierno." width="181" height="118" /></a></span></div>
</div>
<div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Maite Luzón</strong></span></div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Maite_Luzon.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/Maite_Luzon.jpg" border="0" alt="Maite Luzón." width="184" height="123" /></a></span></div>
</div>
<div><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>María Fernández</strong></span></div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Maria_Fernandez.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Maria_Fernandez.jpg" border="0" alt="Maria Fernández." width="196" height="114" /></a></span></div>
<div><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Proverbios (Francés)</strong></span></div>
<div>
<div><span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Proverbios _Frances.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Proverbios_Frances.jpg" border="0" alt="Proverbios (Francés)." width="183" height="111" /></a></span></div>
<div>
<div>
<div style="text-align: center;"><hr /><span style="font-family: book antiqua,palatino; font-size: 12pt;"><strong>Snow dreams (inglés)</strong></span></div>
<span style="font-family: book antiqua,palatino; font-size: 12pt;"><a href="archivos_ies/lengua/invierno/Snow_dreams_ingles.pdf" target="_blank"><img src="archivos_ies/lengua/invierno/1Snow_dreams.jpg" border="0" alt="Snow dreams." width="176" height="106" /></a> </span></div>
</div>
</div>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
<div><br />
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="3" cellpadding="2" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid;" align="center" valign="middle" bgcolor="#c78d74">
<td style="text-align: center; border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">VÍDEO DEL VIAJE A BROOKLYN, NUEVA YORK, ABRIL 2011</span></strong></strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9 #8799f9 currentColor; border-bottom-width: 1px; border-bottom-style: solid;">
<td class="textogris" style="border-color: #8799f9; text-align: center;">
<div><span style="font-size: 12px;"><strong> <a href="http://www.youtube.com/watch?v=xiFxyNxAQR0" target="_blank"><img src="archivos_ies/CollageNY.jpg" border="0" alt="Vídeo del viaje a Brooklyn 2011." /></a> </strong></span><strong><img class="caption" src="images/stories/cervantes/qr_viaje_nueva_york.jpg" border="0" alt="Código qr del viaje a Nueva York. Leerlo con la cámara del móvil." title="Código qr del viaje a Nueva York. Leerlo con la cámara del móvil." style="border: 0;" /></strong></div>
</td>
</tr>
</tbody>
</table>
</div>
<div id="tituloItem" class="blockTituloItem" style="display: block;">
<div class="tituloIzq" style="float: left;">
<div><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; letter-spacing: 1pt; color: #71cd63;"> </span></div>
</div>
</div>
<div class="blockTituloItem" style="display: block;"> </div>
<div id="contenido_sitio" style="width: 100%; float: left; display: block;">
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="0" cellpadding="1" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;" align="center" valign="middle" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">DOCUMENTOS DEL PLAN DE CENTRO</span></strong> </strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9; border-bottom: 1px solid;">
<td class="textogris" style="text-align: right; border-color: #8799f9;">
<div style="text-align: left;">
<ul style="color: #151509; margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; list-style-type: none; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"><a href="archivos_ies/PROYECTO_EDUCATIVO_IES_MIGUEL_DE_CERVANTES_definitivo_2011_12.pdf" target="_blank" title="PROYECTO EDUCATIVO IES MIGUEL DE CERVANTES 2011-12." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES.</a></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PROYECTO_DE_GESTION_DEFINITIVO.pdf" target="_blank" title="PROYECTO DE GESTIÓN." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PROYECTO DE GESTIÓN.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/Plan_de_convivencia_definitivo.pdf" target="_blank" title="Plan de Convivencia." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PLAN DE CONVICENCIA.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/ROF_nuevo_2011_definitivo.pdf" target="_blank" title="ROF nuevo 2011." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">NUEVO ROF 2011.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PLAN_DE_ORIENTACION_Y_ACCION_TUTORIAL_definitivo.pdf" target="_blank" title="PLAN DE ORIENTACIÓN Y ACCIÓN." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PLAN DE ORIENTACIÓN Y ACCIÓN TUTORIAL.</a></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PLAN_DE_FORMACION_MODELO_IES_CERVANTES_definitivo.pdf" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">PLAN DE FORMACIÓN DEL IES MIGUEL DE CERVANTES.</a></span></li>
</ul>
</div>
<div style="text-align: left;"> </div>
</td>
</tr>
</tbody>
</table>
<br />
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="0" cellpadding="1" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;" align="center" valign="middle" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">ESCUELA TIC 2.0</span></strong> </strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9; border-bottom: 1px solid;">
<td class="textogris" style="text-align: right; border-color: #8799f9;">
<div style="text-align: left;">
<table style="margin: 1px; width: 100%; text-align: center; border-collapse: collapse;" border="0" cellspacing="2">
<tbody style="text-align: center;">
<tr style="text-align: center;">
<td style="vertical-align: top; text-align: center; padding: 2px; border: 0px solid #9a9542;">
<ul style="color: #129d0e; margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 2em; font-family: 'comic sans ms', sans-serif; font-size: 12px; line-height: 15px; list-style-type: none; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;">
<div style="text-align: left;"><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'Comic Sans MS'; color: #810081; font-size: 10pt;"><a href="http://blogsaverroes.juntadeandalucia.es/escuelatic20/" target="_blank" title="Escuela TIC 2.0" style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">ESCUELA TIC 2.0</a></span></strong><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><br /></strong></div>
</li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); text-align: left; background-repeat: no-repeat no-repeat;"><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'Comic Sans MS'; color: #810081; font-size: 10pt;"><a href="http://www.juntadeandalucia.es/averroes/mochiladigitalESO/" target="_blank" title="Mochila digital." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">MOCHILA DIGITAL SECUNDARIA<br /></a></span></strong></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;">
<div style="text-align: left;"><strong style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><span style="font-family: 'Comic Sans MS'; color: #810081; font-size: 10pt;"><a href="http://www.cepgranada.org/~inicio/pf_login.php?4,60,,,,,,," target="_blank" title="Curso del CEP de Granada" style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">CURSOS CEP DE GRANADA</a></span></strong></div>
</li>
</ul>
</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
<br />
<table style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;" border="0" cellspacing="0" cellpadding="1" align="center">
<tbody>
<tr style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;" align="center" valign="middle" bgcolor="#c78d74">
<td style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial; text-align: center;" colspan="4" align="center"><span style="color: #ffffff;"><strong><strong><span style="font-size: 12pt;">NOTICIAS</span></strong></strong></span></td>
</tr>
<tr class="textogris" style="border-color: #8799f9; border-bottom: 1px solid;">
<td class="textogris" style="text-align: right; border-color: #8799f9;">
<div style="text-align: left;"> </div>
<div style="text-align: left;">
<ul>
<li style="color: #151509;">Blog del <span style="font-size: 12px; color: #000000;"><a href="http://letsknoweachotherbetter.blogspot.com/" target="_blank">Viaje a Cumbria</a>, Inglaterra, 14 al 23 de junio de 2011.</span></li>
</ul>
<ul>
<li style="color: #151509;">Actividad extraescolar: "<a href="archivos_ies/Los_sitios_de_Mariana_de_Pineda.pdf" target="_blank">LOS SITIOS DE MARIANA PINEDA</a>", un recorrido por la ciudad de Granada: Departamento de Geografía e Historia.
<ul>
<li style="color: #151509;">26/05/2011: <a href="index.php/departamentos26/humanidesdes/dep-geografia/#mariana">añadidas 6 fotografías de la actividad.</a></li>
</ul>
</li>
<li><span style="font-family: arial, helvetica, sans-serif;">2º Bachillerato B. <a href="archivos_ies/Master2011.pdf" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Master de Secundaria y Bachillerato 2011</a>.</span></li>
<li><span style="font-family: arial, helvetica, sans-serif; font-size: 10pt;"><span style="text-decoration: underline;">31 de marzo de 2011</span>:  ENCUENTROS LITERARIOS EN EL INSTITUTO. <a href="index.php?option=com_content&amp;view=article&amp;id=132" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Andrés Neuman</a> estuvo con el alumnado de 2º de Bachillerato</span></li>
<li><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: 10pt;"><span style="text-decoration: underline;">9 de marzo:</span> <strong><span style="text-decoration: underline;"><a href="index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=132&amp;Itemid=188" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Día Internacional de la Mujer</a></span></strong>: se están realizando numerosas actividades en el centro durante dos semanas y se celebra la FERIA INTERCULTURAL DE LAS TAREAS DOMÉSTICAS.</span></li>
<li><span style="color: #000000; font-family: arial, helvetica, sans-serif; font-size: 10pt;"><span style="text-decoration: underline;">8 de febrero:</span> <a href="archivos_ies/DIA_INTERNACIONAL_INTERNET_SEGURO.pdf" target="_blank" title="Día Internacioanl de Internet seguro." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #129d0e; font-size: 11px; font-weight: normal;">Día internacional de internet seguro.</a></span></li>
</ul>
</div>
</td>
</tr>
</tbody>
</table>
</div>
<p> </p></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:88:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - IES Miguel de Cervantes - Granada (2)";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:5:{i:0;O:8:"stdClass":2:{s:4:"name";s:12:"BILINGÜISMO";s:4:"link";s:59:"index.php?option=com_content&view=article&id=171&Itemid=239";}i:1;O:8:"stdClass":2:{s:4:"name";s:17:"1º ESO BILINGÜE";s:4:"link";s:58:"index.php?option=com_content&view=article&id=95&Itemid=500";}i:2;O:8:"stdClass":2:{s:4:"name";s:32:"IES Miguel de Cervantes- General";s:4:"link";s:60:"index.php?option=com_content&view=category&id=194&Itemid=500";}i:3;O:8:"stdClass":2:{s:4:"name";s:15:"Datos generales";s:4:"link";s:59:"index.php?option=com_content&view=category&id=37&Itemid=500";}i:4;O:8:"stdClass":2:{s:4:"name";s:37:"IES Miguel de Cervantes - Granada (2)";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:39821:"<div class="blog"><div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Especial</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 09 Octubre 2013 19:59</span> | Visitas: 44302
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 style="text-align: center;"><span style="text-decoration: underline;"><strong>CURSOS BILINGÜES.</strong></span></h2>
<p style="text-align: center;"><a href="index.php?option=com_content&amp;view=article&amp;id=95&amp;Itemid=500">1º  ESO BILINGÜES</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=134&amp;Itemid=241">2º  ESO BILINGÜES</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=30&amp;Itemid=240">3º  ESO BILINGÜES</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=135&amp;Itemid=249">4º  ESO BILINGÜES</a></p>
<hr style="padding-left: 30px;" />
<h3 style="padding-left: 30px; text-align: center;"><a href="index.php?option=com_content&amp;view=article&amp;id=231" target="_blank"><span style="text-decoration: underline;"><strong style="color: #2a2a2a; font-size: 13px; text-align: justify; text-transform: none; background-color: #e7eaad;"><span style="color: #71cd63; font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; font-size: 14pt;">ZONA PRIVADA PARA EL ALUMNADO DE FQ DE 3º ESO A 2º BACH.</span></strong></span></a></h3>
<h3 style="padding-left: 30px; text-align: center;"><span style="text-decoration: underline;"><span><a href="index.php?option=com_content&amp;view=article&amp;id=96" target="_blank">2º bach. c - d. zona privada.</a></span></span></h3>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px; text-align: center;"> </p>
<h5 style="padding-left: 30px; text-align: center;"><span style="text-decoration: underline;"><span>MATERIAL BILINGÜE AICLE DE LA JUNTA DE ANDALUCIA</span></span> </h5>
<ul style="padding-left: 30px; text-align: center;">
<li>
<div style="text-align: left;"><!-- ws:start:WikiTextUrlRule:135:http://portal.ced.junta-andalucia.es/educacion/webportal/web/aicle/contenidos --><a class="wiki_link_ext" href="http://portal.ced.junta-andalucia.es/educacion/webportal/web/aicle/contenidos" rel="nofollow"><span style="color: #0066cc;">http://portal.ced.junta-andalucia.es/educacion/webportal/web/aicle/contenidos</span></a><!-- ws:end:WikiTextUrlRule:135 --></div>
</li>
</ul>
<h6> <span style="text-decoration: underline;">Actividades para trabajar el portfolio europeo de las lenguas</span></h6>
<ul>
<li><span style="text-decoration: underline;"><a class="wiki_link_ext" href="http://www.juntadeandalucia.es/educacion/webportal/web/pel" rel="nofollow"><span style="color: #0066cc;">Actividades PEL</span></a></span></li>
</ul>
<p> </p>
<p> </p>
<h5><span style="text-decoration: underline;"><span class="PageTitle"><span style="color: #0066cc; text-decoration: underline;">Documentación y normativa</span></span> </span>        </h5>
<ul>
<li>LinguaRed para ver legislación y normativa sobre bilingüismo:<br /><br /><a class="wiki_link_ext" href="http://ulises.cepgranada.org/moodle/mod/resource/view.php?id=3687" rel="nofollow"> <span style="color: #0066cc;">http://ulises.cepgranada.org/moodle/mod/resource/view.php?id=3687</span></a></li>
<li> O bien:<br /><br />- Legislación educativa andaluza y española de ámbito estatal sobre Plurilingüismo en vigor en Andalucía:<br /><br /><a class="wiki_link_ext" href="http://www.adideandalucia.es/disposicion.php?cat=75" rel="nofollow"><span style="color: #0066cc;">http://www.adideandalucia.es/disposicion.php?cat=75</span></a></li>
</ul>
<hr />
<p style="font-family: arial,helvetica,sans-serif;" align="center"><strong><span style="font-size: large;">LEGISLACIÓN Y ORGANIZACIÓN DE CENTROS BILINGÜES</span></strong></p>
<p style="font-family: arial,helvetica,sans-serif;"><strong>1- Objetivos:</strong></p>
<p style="font-family: arial,helvetica,sans-serif;">Disponer de toda la normativa relativa al Plan de Fomento del Plurilingüismo en Andalucía de una manera lógica y organizada.</p>
<p style="font-family: arial,helvetica,sans-serif;">Señalar aquellos puntos más significativos de la misma.</p>
<p style="font-family: arial,helvetica,sans-serif;"> </p>
<p style="font-family: arial,helvetica,sans-serif;"><strong>2- Contenidos: <br /></strong></p>
<p style="font-family: arial,helvetica,sans-serif;"><strong><span style="font-weight: normal;"><span style="text-decoration: underline;">Organización y funcionamiento de los centros bilingües</span>.</span></strong></p>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Organizacion/Orden_sobre_organizacion_y_funcionamiento_de_centros_bilinguees.pdf" target="_blank" title="Orden marco"><span style="color: #0066cc;">Orden marco sobre organización y funcionamiento</span></a>.</span></strong></li>
</ul>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Organizacion/Instrucciones_centros_bilingues_2010-11.pdf" target="_blank" title="Curso 2010-2011"><span style="color: #0066cc; font-family: Arial;">Instrucciones para el curso 2010-2011</span></a>.</p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Organizacion/Orden_dedicacion_horas_de_coordinacion.pdf" target="_blank" title="Horas de coordinación"><span style="color: #0066cc; font-family: Arial;">Orden de horario de coordinación</span></a>.</p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Organizacion/Instrucciones_maestro_adicional_8-9-2009_Bilingue_Infantil.pdf" target="_blank" title="Instrucciones maestro infantil"><span style="color: #0066cc; font-family: Arial;">Instrucciones maestro adicional en infantil</span></a>.</span></strong></p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Organizacion/Instrucciones_centros_bilinguees_2009-10.pdf" target="_blank" title="Instrucciones 09-10"><span style="color: #0066cc; font-family: Arial;">Instrucciones para el curso 2009/2010</span></a>.</span></strong></p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Organizacion/Instrucciones_centros_bilinguees_2008-09.pdf" target="_blank" title="Instrucciones 08-09"><span style="color: #0066cc; font-family: Arial;">Instrucciones para el curso 2008/2009</span></a>.</span></strong></p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Organizacion/Instrucciones_centros_bilinguees_2007-08.pdf" target="_blank" title="Instrucciones 07-08"><span style="color: #0066cc; font-family: Arial;">Instrucciones para el curso 2007/2008</span></a>.<br /></span></strong></p>
<p style="font-family: arial,helvetica,sans-serif;"><strong><span style="font-weight: normal;"><span style="text-decoration: underline;">Normativa de rango superior</span>.</span></strong></p>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/LOE.pdf" target="_blank" title="LOE"><strong><span style="font-weight: normal;"><span style="color: #0066cc;">LOE</span></span></strong></a></li>
</ul>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/LEA.pdf" target="_blank" title="LEA"><strong><span style="font-weight: normal;"><span style="color: #0066cc;">LEA</span></span></strong></a></li>
</ul>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;">Reales Decretos de Enseñanzas Mínimas: <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Real_Decreto_1631_Secundaria.pdf" target="_blank" title="RD 1631"><span style="color: #0066cc; font-family: Arial;">Secundaria 1631</span></a>, <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Real_Decreto_1513_Primaria.pdf" target="_blank" title="RD 1513"><span style="color: #0066cc; font-family: Arial;">Primaria 1513</span></a>, <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Real_Decreto_1630_Infantil_2_ciclo.pdf" target="_blank" title="RD 1630"><span style="color: #0066cc; font-family: Arial;">Infantil 1630</span></a>.</span></strong></p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 120px;"><strong><span style="font-weight: normal;">Secciones de los Reales Decretos relativas a Lengua Extranjera: </span></strong><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Legislacion_superior/Real_Decreto_1631_Lengua_Extranjera.pdf" target="_blank" title="RD 1631 Lengua Extranjera"><span style="color: #0066cc; font-family: Arial;">Secundaria</span></a><strong><span style="font-weight: normal;">, <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Real_Decreto_Primaria_1513_lengua_extranjera.pdf" target="_blank" title="RD 1513 Lengua Extranjera"><span style="color: #0066cc; font-family: Arial;">Primaria</span></a>.<br /></span></strong></p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;">Decretos de Enseñanzas en Andalucía: <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Decreto_231_de_Secundaria_Andalucia.pdf" target="_blank" title="Decreto 231"><span style="color: #0066cc; font-family: Arial;">Secundaria 231</span></a>, <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Decreto_230_de_Primaria_Andalucia.pdf" target="_blank" title="Decreto 230"><span style="color: #0066cc; font-family: Arial;">Primaria 230</span></a>, <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Decreto_428_de_Infantil_Andalucia.pdf" target="_blank" title="Decreto 248"><span style="color: #0066cc; font-family: Arial;">Infantil 248</span></a>.</span></strong></p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;">Órdenes de currículo: <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Orden_Curriculo_Secundaria.pdf" target="_blank" title="Currículo secundaria"><span style="color: #0066cc; font-family: Arial;">Secundaria</span></a>, <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Orden_Curriculo_Primaria.pdf" target="_blank" title="Currículo primaria"><span style="color: #0066cc; font-family: Arial;">Primaria</span></a>, <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Orden_Curriculo_Infantil.pdf" target="_blank" title="Currículo infantil"><span style="color: #0066cc; font-family: Arial;">Infantil</span></a>.</span></strong></p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;">Órdenes de evaluación: <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Orden_de_Evaluacion_en_Secundaria_Andalucia.pdf" target="_blank" title="Evaluación secundaria"><span style="color: #0066cc; font-family: Arial;">Secundaria</span></a>, <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Orden_de_Evaluacion_en_Primaria_Andalucia.pdf" target="_blank" title="Evaluación primaria"><span style="color: #0066cc; font-family: Arial;">Primaria</span></a>, <a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Legislacion_superior/Orden_de_Evaluacion_en_Infantil_Andalucia.pdf" target="_blank" title="Evaluación infantil"><span style="color: #0066cc; font-family: Arial;">Infantil</span></a>.<br /></span></strong></p>
<p style="font-family: arial,helvetica,sans-serif;"><strong><span style="font-weight: normal;"><span style="text-decoration: underline;">Séneca y gestión administrativa</span>.</span></strong></p>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Seneca/Decreto_285_de_Seneca.pdf" target="_blank" title="Decreto de Séneca"><span style="color: #0066cc;">Decreto que regula Séneca</span></a>.</li>
<li><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Seneca/Instrucciones_para_grabar_en_Seneca.pdf" target="_blank" title="Grabar en Séneca"><span style="color: #0066cc;">Grabar en Séneca</span></a>.</span></strong></li>
</ul>
<div style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Seneca/seneca_grabar_asuxiliares.pdf" target="_blank" title="Pagar Auxiliares"><span style="color: #0066cc; font-family: Arial;">Grabar a los auxiliares de conversación</span></a>.<br /><br /><a href="http://pluri.wikispaces.com/seneca"><span style="color: #0066cc; font-family: Arial;">Grabar en Séneca Mayo 2010</span></a><br /><br /><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Seneca/Grabacion_datos_Seneca.PDF" target="_blank" title="Grabar datos Séneca"><span style="color: #0066cc; font-family: Arial;">Escrito relativo a la grabación de datos en Séneca</span></a>.<br /><br /><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Seneca/Tutorial_Seneca_09-10.pdf" target="_blank" title="Tutorial Séneca"><span style="color: #0066cc; font-family: Arial;">Tutorial de Séneca</span></a>.<br /><br /><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Seneca/Instrucciones_para_grabar_en_Seneca.pdf" target="_blank" title="Instrucciones para grabar en Séneca"><span style="color: #0066cc; font-family: Arial;">Grabación en Séneca de participantes</span></a>.<br /><br /><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Seneca/Seneca_Instruccionespara_todos_los_centros_mayo_2010.pdf" target="_blank" title="Seneca todos los centros"><span style="color: #0066cc; font-family: Arial;">Instrucciones de mayo de 2010 para todos los centros bilingües</span></a>.<br /><br /><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Seneca/Seneca_Instrucciones_para_los_centros_de_Infantil_primaria_mayo_2010.pdf" target="_blank" title="Primaria Séneca"><span style="color: #0066cc; font-family: Arial;">Instrucciones de mayo de 2010 para centros de Infantil y Primaria</span></a>.</div>
<p style="font-family: arial,helvetica,sans-serif;"><strong><span style="font-weight: normal;"><span style="text-decoration: underline;">Auxiliares de conversación</span>.</span></strong></p>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Auxiliares/Orden_reguladora_auxiliares_2006.pdf" target="_blank" title="Orden auxiliares"><span style="color: #0066cc;">Orden reguladora de auxiliares</span></a>.</span></strong></li>
</ul>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Auxiliares/Orden_14-9-2007_regula_mas_Auxiliares_Conversacion.pdf" target="_blank" title="Orden auxiliares 2"><span style="color: #0066cc; font-family: Arial;">Orden que modifica la reguladora de auxiliares</span></a>.<br /></span></strong></p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Auxiliares/Instruc9sept2010auxiliaresconversacionerasmus2010-11.pdf" target="_blank" title="Auxiliares 2010-11"><span style="color: #0066cc; font-family: Arial;">Instrucciones sobre auxiliares de conversación para el curso escolar 2010/2011</span></a>.</p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Auxiliares/Instrucciones_Aux_de_Conversacion_2009-2010.pdf" target="_blank" title="Instrucciones auxiliares 09-10"><span style="color: #0066cc; font-family: Arial;">Instrucciones auxiliares 2009/2010</span></a>.<br /></span></strong></p>
<p style="font-family: arial,helvetica,sans-serif;"><strong><span style="font-weight: normal;"><span style="text-decoration: underline;">Materiales curriculares</span>.</span></strong></p>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Materiales/Orden_7-3-2008_Regula_Premios_Plurilinguismo.pdf" target="_blank" title="Orden reguladora premios"><span style="color: #0066cc;">Orden reguladora de premios a materiales bilingües</span></a>.</span></strong></li>
</ul>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Materiales/Premios_materiales_bilinguees_2008.pdf" target="_blank" title="Resolución premios 2008"><span style="color: #0066cc; font-family: Arial;">Resolución premios 2008</span></a>.<br /></span></strong></p>
<p style="font-family: arial,helvetica,sans-serif;"><strong><span style="font-weight: normal;"><span style="text-decoration: underline;">Licencias, permisos y estancias</span>.</span></strong></p>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Licencias_y_estancias/Orden_reguladora_sobre_estancias_de_inmersion.pdf" target="_blank" title="Orden estancias"><span style="color: #0066cc;">Orden reguladora de estancias</span></a>.</span></strong></li>
</ul>
<div style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Licencias_y_estancias/Resolucion_convocatoria_estancias_2009.pdf" target="_blank" title="Convocatoria estancias 2009"><span style="color: #0066cc; font-family: Arial;">Convocatoria estancias 2010</span></a>.<br /><br /><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Licencias_y_estancias/Resolucion_provisional_estancias_2010.pdf" target="_blank" title="Resolución provisional estancias 2010"><span style="color: #0066cc; font-family: Arial;">Resolución estancias 2010</span></a>.</div>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Licencias_y_estancias/Orden_reguladora_sobre_licencias_por_estudio.pdf" target="_blank" title="Orden licencias"><span style="color: #0066cc;">Orden reguladora de licencias</span></a>.<br /></span></strong></li>
</ul>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Licencias_y_estancias/Convocatoria_licencias_curso_10-11.pdf" target="_blank" title="Convocatoria licencias 2010-11"><span style="color: #0066cc; font-family: Arial;">Convocatoria licencias curso 2010/2011</span></a>.</p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/Licencias_y_estancias/Convocatoria_de_licencias_curso_2009-10.pdf" target="_blank" title="Licencias 09-10"><span style="color: #0066cc; font-family: Arial;">Convocatoria de licencias curso 2009/2010</span></a>.</span></strong></p>
<p style="font-family: arial,helvetica,sans-serif; margin-left: 80px;"><strong><span style="font-weight: normal;"><a href="http://www.juntadeandalucia.es/educacion/nav/contenido.jsp?pag=/Contenidos/IEFP/FORMACION/LICENCIAS_09_10_DEF_RESOLU" target="_blank" title="Resolución licencias 09-10"><span style="color: #0066cc; font-family: Arial;">Resolución de licencias 2009/2010</span></a>.<br /></span></strong></p>
<p><span style="font-family: arial,helvetica,sans-serif; text-decoration: underline;">Convocatoria de plazas en centros bilingües</span><span style="font-family: arial,helvetica,sans-serif;">.</span><br style="font-family: arial,helvetica,sans-serif;" /><br style="font-family: arial,helvetica,sans-serif;" /></p>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Convocatoria_plazas/090527_Bases_adjudicacion_provisinal_09-10.pdf" target="_blank" title="Bases adjudicación provisional 09-10"><span style="color: #0066cc;">Resolución para la adjudicación de destinos provisionales para el curso académico 2009/10</span></a>.</li>
</ul>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Convocatoria_plazas/090508_Convocatoria_Secundaria_09-10.pdf" target="_blank" title="Secundaria 09-10"><span style="color: #0066cc;">Resolución para la cobertura provisional de puestos vacantes en centros docentes bilingües para el curso escolar 2009/10 de Secundaria y Formación Profesional</span></a>.</li>
</ul>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/Convocatoria_plazas/090508_Convocatoria_Secundaria_09-10_Correccion_errores.pdf" target="_blank" title="Corrección errores secundaria 09-10"><span style="color: #0066cc;">Corrección de la anterior</span></a>.</li>
</ul>
<p style="font-family: arial,helvetica,sans-serif;"><strong><span style="font-weight: normal;"><span style="text-decoration: underline;">Curso de actualización lingüística</span>.</span></strong></p>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><strong><span style="font-weight: normal;"><a href="http://ulises.cepgranada.org/moodle/pluginfile.php/1807/mod_page/content/1/Legislacion/CAL/Instruc15sept2009CALonline2009-10.pdf" target="_blank" title="Instrucciones CAL"><span style="color: #0066cc;">Instrucciones CAL "on line" 2009/2010</span></a>.</span></strong></li>
</ul>
<ul style="font-family: arial,helvetica,sans-serif;">
<li><a href="http://ulises.cepgranada.org/moodle/mod/file.php/937/Legislacion/CAL/instrucciones_pruebas_online_100519.pdf" target="_blank" title="Pruebas CAL on line 2009-10"><span style="color: #0066cc;">Instrucciones pruebas finales CAL "on line" 2009/2010</span></a>.</li>
</ul>
<p> </p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">ALUMNADO</span> / <span class="art-post-metadata-category-name">Grupos del curso 2012/13</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">2º ESO BILINGÜE2012-13</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 29 Septiembre 2013 19:16</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=214:2o-eso-bilinguee-2&amp;catid=144&amp;Itemid=105&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=62d5493f96960a2be11cd86540b72eeae7977565" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 8106
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p style="font-size: 11.818181991577148px;"><span style="text-decoration: underline;"><strong style="color: #000000; font-size: 19.09090805053711px;">APUNTES DE EDUCACIÓN FÍSICA 2º ESO BILINGÜE.</strong></span></p>
<p style="font-size: 11.818181991577148px;"><span style="text-decoration: underline;"><strong>PROFESORA:</strong></span><strong> Dª Nuria Manrique Morá.</strong></p>
<p style="font-size: 11.818181991577148px;"> </p>
<ul>
<li><a href="archivos_ies/bilingues/2eso/What_equipment_is_necessary.Vocabulary.pdf" target="_blank">1.- What equipment is necessary. Vocabulary</a></li>
<li><a href="archivos_ies/bilingues/2eso/Warm-up.pdf" target="_blank">2.- Warm-up</a></li>
<li><a href="archivos_ies/bilingues/2eso/PREVENTING_INJURIES.pdf" target="_blank">3.- Preventing injuries.</a> (27/09/12)</li>
<li><a href="archivos_ies/bilingues/2eso/HEALTH_AND_HYGIENE.pdf" target="_blank">4.- HEALTH AND HYGIENE.</a> (14/10/12)</li>
</ul>
<p> </p>
<hr />
<p><span style="text-decoration: underline;"><strong style="color: #000000; font-size: 19.09090805053711px;">APUNTES DE MATEMÁTICAS 2º ESO BILINGÜE.</strong></span></p>
<p><span style="text-decoration: underline;"><strong>PROFESORA:</strong></span><strong> Dª Clotilde García Sánchez.</strong></p>
<ul>
<li><a href="archivos_ies/bilingues/2eso/mat2extraactchap1.pdf" target="_blank">1.- CHAPTER 1: INTEGERS.</a> (08/10/12)</li>
<li><a href="archivos_ies/bilingues/2eso/integers_and_science.pdf" target="_blank">2.- Integers and science.</a> (08/10/12)</li>
<li><a href="archivos_ies/bilingues/2eso/PROBLEMS_WITH_FRACTIONS.pdf" target="_blank">3.- PROBLEMS WITH FRACTIONS.</a> (07/11/12)</li>
<li><a href="archivos_ies/bilingues/2eso/Decimals_fractions_and_percentages.pdf" target="_blank">4.- Decimals, fractions and percentages.</a> (07/11/12)</li>
<li><a href="archivos_ies/bilingues/2eso/Exercises_Decimals_fractions_and_percentages.pdf" target="_blank">5.- Exercises, Decimals, fractions and percentages.</a> (07/11/12)</li>
<li><a href="archivos_ies/bilingues/2eso/Actividad_Mixed_fraction(soluciones).pdf" target="_blank">6.- Actividad: Mixed fraction (soluciones).</a> (08/11/12)</li>
<li><strong><a href="archivos_ies/bilingues/2eso/-Conversiones_Decimals_fractions_percentages.pdf" target="_blank">7.- Conversiones Decimals, fractions, percentages.</a> (23/11/12)<br /></strong></li>
<li><strong><a href="archivos_ies/bilingues/2eso/Actividad_Fractions_using_reciprocal_soluciones.pdf" target="_blank">8.- Actividad: Fractions using reciprocal (soluciones)</a> (26/11/12)</strong></li>
<li><a href="archivos_ies/bilingues/2eso/-Solutions_Convert_Decimals_fractions_percentages.pdf" target="_blank"><span style="color: #0066cc;">9.- Solutions. Conver _Decimals fractions, percentages.</span></a> (04/12/12)</li>
<li><strong><a href="archivos_ies/Sexagesimal_system.pdf" target="_blank"><strong><span style="color: #0000ff;">10.- Sexagesimal system.</span></strong></a> (19/12/12)</strong></li>
<li><strong><a href="archivos_ies/bilingues/2eso/Vocabulary_Algebra.pdf" target="_blank"><strong><span style="color: #0000ff;">11.- Vocabulary Algebra.</span></strong></a> (19/01/13)</strong></li>
<li><a href="archivos_ies/bilingues/2eso/OLIMPIADA_MATEMATICA1.pdf" target="_blank"><strong>12.- OLIMPIADA MATEMATICA. (28/01/13)</strong></a></li>
<li><a href="archivos_ies/bilingues/2eso/soluciones_libroblanco_polinomios.pdf" target="_blank">13.- Soluciones libro blanco polinomios.</a> (28/01/13)</li>
<li><strong><a href="archivos_ies/bilingues/2eso/worsheet_Origins_of_Algebra_Solution.pdf" target="_blank">14.- Worsheet Origins of Algebra Solution.</a> (08/02/13)</strong></li>
<li><strong><a href="archivos_ies/bilingues/2eso/Vocabulary_eqation.pdf" target="_blank">15.- Vocabulary equation.</a> (08/02/13)<br /></strong></li>
<li><strong><a href="archivos_ies/bilingues/2eso/soluciones_libro_blanco2.docx" target="_blank">16.- Soluciones libro_blanco.</a> (08/02/13)<br /></strong></li>
</ul>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<hr /></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Cursos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 16 Diciembre 2012 08:04</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=136:cursos&amp;catid=144&amp;Itemid=105&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=5fff2c4102a1b3e969bc09e58c0d7a6decdb3572" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3171
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><table style="border-collapse: collapse; margin: 1px; width: auto; color: #3b484e; font-family: 'Times New Roman'; font-size: 11.818181991577148px; text-align: center;" class="borde_outset" border="1">
<tbody>
<tr>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=88:matematicas-1o-eso&amp;catid=38&amp;Itemid=69">1º ESO A</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=129%3A2o-eso&amp;catid=141&amp;Itemid=103">2º ESO A</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=89&amp;Itemid=103">3º ESO A</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;">4º ESO A</p>
</td>
</tr>
<tr>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=88:matematicas-1o-eso&amp;catid=38&amp;Itemid=69">1º ESO B</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=129%3A2o-eso&amp;catid=141&amp;Itemid=103">2º ESO B</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=89&amp;Itemid=103">3º ESO B</a>: Bilingüe</p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;">4º ESO B</p>
</td>
</tr>
<tr>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=88:matematicas-1o-eso&amp;catid=38&amp;Itemid=69">1º ESO C</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=129%3A2o-eso&amp;catid=141&amp;Itemid=103">2º ESO C</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=89&amp;Itemid=103">3º ESO C</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;">4º ESO C</p>
</td>
</tr>
<tr>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=88:matematicas-1o-eso&amp;catid=38&amp;Itemid=69">1º ESO D</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=129%3A2o-eso&amp;catid=141&amp;Itemid=103">2º ESO D</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=89&amp;Itemid=103">3º ESO D</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;">4º ESO D</p>
</td>
</tr>
<tr>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=90&amp;Itemid=103">1º BACH A</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=90&amp;Itemid=103">1º BACH B</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;">1º BACH C</p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;" rowspan="2">
<p style="text-align: center;">CICLO SUPERIOR</p>
</td>
</tr>
<tr>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a title="Página web de 2º BACH A MAT II." href="http://www.juntadeandalucia.es/averroes/centros-tic/18700441/helvia/sitio/index.cgi?wid_seccion=1&amp;wid_item=8" target="_blank">2º BACH A</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=130%3A2o-bachillerato-b&amp;catid=141&amp;Itemid=103">2º BACH B</a></p>
</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top; text-align: left;">
<p style="text-align: center;"><a title="Página web de 2º BACH. II CC SS. Profesor Miguel Anguita." href="http://www.iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=94:2bachilleratoc&amp;catid=38&amp;Itemid=69" target="_blank">2º BACH C</a></p>
</td>
</tr>
<tr>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top;"><a href="http://www.iesmigueldecervantes.es/archivos_ies/pendientes_mat/1eso_mat_pendientes_2011-2012.pdf" target="_blank">PENDIENTES DE 1º ESO</a></td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top;">PENDIENTES DE 2º ESO</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top;">PENDIENTES DE 3º ESO</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top;">PENDIENTES DE 1º BACH:<a href="http://www.iesmigueldecervantes.es/archivos_ies/pendientes_mat/mcs1_pendientes_1_20112012.pdf" target="_blank"> Parte 1</a> -&nbsp;<a href="http://www.iesmigueldecervantes.es/archivos_ies/pendientes_mat/mcs1_pendientes_2_2011-2012.pdf" target="_blank">Parte 2</a></td>
</tr>
<tr>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top;">PROYECTO INTEGRADO 2º BACH</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top;">LECTURAS MATEMÁTICAS</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top;">PASATIEMPOS MATEMÁTICOS</td>
<td style="padding: 2px; border: 1px solid #9a9542; vertical-align: top;">SELECTIVIDAD 2º&nbsp;BACH</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">ALUMNADO</span> / <span class="art-post-metadata-category-name">Grupos del curso 2012/13</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
</div>";s:4:"head";a:10:{s:5:"title";s:81:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Dpto. de Biología y Geología";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:102:"/index.php?option=com_content&amp;view=category&amp;id=144&amp;Itemid=105&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:103:"/index.php?option=com_content&amp;view=category&amp;id=144&amp;Itemid=105&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:5:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:30:"Área Científica-Tecnológica";s:4:"link";s:59:"index.php?option=com_content&view=article&id=185&Itemid=571";}i:2;O:8:"stdClass":2:{s:4:"name";s:30:"Dpto. de Biología y Geología";s:4:"link";s:59:"index.php?option=com_content&view=article&id=161&Itemid=105";}i:3;O:8:"stdClass":2:{s:4:"name";s:8:"ALUMNADO";s:4:"link";s:60:"index.php?option=com_content&view=category&id=222&Itemid=105";}i:4;O:8:"stdClass":2:{s:4:"name";s:24:"Grupos del curso 2012/13";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:26528:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">ERASMUS PLUS</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 21 Mayo 2015 19:59</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=258:erasmun-plus&amp;catid=40&amp;Itemid=710&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=88feb97403970e6d3dd29894c0c64db43258c82f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3272
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p class="MsoNormal" style="margin-left: -14.2pt; padding-left: 30px;"><!--[if gte vml 1]><v:shapetype
 id="_x0000_t75" coordsize="21600,21600" o:spt="75" o:preferrelative="t"
 path="m@4@5l@4@11@9@11@9@5xe" filled="f" stroked="f">
 <v:stroke joinstyle="miter"/>
 <v:formulas>
  <v:f eqn="if lineDrawn pixelLineWidth 0"/>
  <v:f eqn="sum @0 1 0"/>
  <v:f eqn="sum 0 0 @1"/>
  <v:f eqn="prod @2 1 2"/>
  <v:f eqn="prod @3 21600 pixelWidth"/>
  <v:f eqn="prod @3 21600 pixelHeight"/>
  <v:f eqn="sum @0 0 1"/>
  <v:f eqn="prod @6 1 2"/>
  <v:f eqn="prod @7 21600 pixelWidth"/>
  <v:f eqn="sum @8 21600 0"/>
  <v:f eqn="prod @7 21600 pixelHeight"/>
  <v:f eqn="sum @10 21600 0"/>
 </v:formulas>
 <v:path o:extrusionok="f" gradientshapeok="t" o:connecttype="rect"/>
 <o:lock v:ext="edit" aspectratio="t"/>
</v:shapetype><v:shape id="_x0000_s1026" type="#_x0000_t75" style='position:absolute;
 left:0;text-align:left;margin-left:-10.95pt;margin-top:1.5pt;width:162pt;
 height:13.9pt;z-index:-251659776'>
 <v:imagedata src="file:///C:\Users\Manuel\AppData\Local\Temp\msohtmlclip1\01\clip_image001.png"
  o:title=""/>
</v:shape><![endif]--><!--[if !vml]--><span style="mso-ignore: vglayout; position: absolute; z-index: -1895826944; left: 0px; margin-left: -15px; margin-top: 2px; width: 216px; height: 19px;"><img src="file:///C:/Users/Manuel/AppData/Local/Temp/msohtmlclip1/01/clip_image002.jpg" border="0" width="216" height="19" /></span><!--[endif]--><span style="font-family: 'Eras Demi ITC',sans-serif; mso-bidi-font-family: Shruti; color: #246e24; mso-ansi-language: ES; mso-no-proof: yes;">                                                                                 </span></p>
<p class="MsoNormal" style="margin-left: -14.2pt; padding-left: 30px;"><span style="font-family: 'Eras Demi ITC',sans-serif; mso-bidi-font-family: Shruti; color: #246e24; mso-ansi-language: ES; mso-no-proof: yes;"> CONSEJERÍA DE EDUCACIÓN</span></p>
<p class="MsoNormal" style="text-align: justify; padding-left: 30px;"><span style="color: #246e24; font-family: Shruti, sans-serif; text-align: left;"> </span><span style="color: #246e24; font-family: Shruti, sans-serif; text-align: left;" lang="PT-BR">I.E.S. Miguel de Cervantes<br /><img src="http://www.oapee.es/oapee/inicio/ErasmusPlus/contentParagraphCentralAux/00/fichero/EU%20flag-Erasmus+_vect_POS.png" border="0" alt="Erasmus +" title="Erasmus +" style="color: #666666; font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; line-height: 18px; cursor: se-resize !important;" /></span></p>
<p class="MsoNormal" style="text-align: justify; padding-left: 30px;"><strong><span style="font-size: 14.0pt; mso-bidi-font-size: 12.0pt; mso-ansi-language: ES;">EL CENTRO SE ENCUENTRA INMERSO EN LA REALIZACIÓN DE UN PROYECTO DE FORMACIÓN EN METODOLOGÍA CLIL Y ELABORACIÓN DE UNIDADES DIDÁCTICAS SUBVENCIONADO POR EL PROGRAMA ERASMUS + DE LA UNIÓN EUROPEA.</span></strong></p>
<p class="MsoNormal" style="text-align: center; padding-left: 30px;" align="center"><strong>METODOLOGÍA CLIL. APRENDIZAJE DE LA L2 A TRAVÉS DE LA ENSEÑANZA DE LA BIOLOGÍA, LA GEOLOGÍA, LA GEOGRAFÍA Y LA HISTORIA.</strong></p>
<p class="MsoNormal" style="padding-left: 30px;"> </p>
<p class="MsoNormal" style="text-align: justify; padding-left: 30px;"><span style="font-size: 14.0pt; mso-bidi-font-size: 12.0pt; mso-ansi-language: ES;">En este <strong><span style="color: #4f81bd;"><a href="https://drive.google.com/file/d/0B6ESuKJ9BtVQRW1pWUlVdnYyNFU/view?usp=sharing" target="_blank"><span style="color: #4f81bd;">enlace</span></a></span></strong> está el proyecto concreto que se está llevando a cabo en el que los objetivos fundamentales son:</span></p>
<p class="MsoListParagraphCxSpFirst" style="text-align: justify; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; mso-bidi-font-family: Calibri;">1.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">     </span></span><!--[endif]--><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%;">Formación del profesorado de las Áreas No Lingüísticas del Proyecto de Centro Bilingüe. </span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 72pt; text-align: justify; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; mso-bidi-font-family: Calibri;">a. </span><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%;">Cuatro cursos de formación en metodología CLIL para profesorado de Geografía e Historia</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 72pt; text-align: justify; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; mso-bidi-font-family: Calibri;">b. </span><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%;">Dos cursos de formación en metodología CLIL para profesorado de Ciencias Naturales</span></p>
<p class="MsoListParagraphCxSpMiddle" style="text-align: justify; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; mso-bidi-font-family: Calibri;">2.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';"> </span></span><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%;">Elaboración de un Libro electrónico con técnicas, experiencias y métodos de enseñanza con la L2 como lengua vehicular</span></p>
<p class="MsoListParagraphCxSpMiddle" style="text-align: justify; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; mso-bidi-font-family: Calibri;">3.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">   </span></span><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%;">Elaboración de unidades CLIL de cada una de las áreas no lingüísticas</span></p>
<p class="MsoListParagraphCxSpMiddle" style="text-align: justify; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; mso-bidi-font-family: Calibri;">4.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">     </span></span><!--[endif]--><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%;">Elaboración de una unidad CLIL integrada titulada </span><strong><span style="font-size: 16.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%;">"Mediterranean Landscape: Malta and Andalucía. Nature, Geography and a Melting pot of Cultures"</span></strong></p>
<p class="MsoListParagraphCxSpMiddle" style="text-align: justify; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; mso-bidi-font-family: Calibri;">5.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%;">Puesta en práctica de las unidades didácticas elaboradas. </span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 72pt; text-align: justify; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><strong><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; mso-bidi-font-family: Calibri; color: #4f81bd;">a.<span style="font-weight: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">      </span></span></strong><!--[endif]--><strong><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; color: #4f81bd;"><a href="https://drive.google.com/file/d/0B6ESuKJ9BtVQLWlFVVVfcVpPWDA/view?usp=sharing" target="_blank"><span style="color: #4f81bd;">Unidad Didáctica CLIL Jardín del Centro</span></a></span></strong></p>
<p class="MsoListParagraphCxSpLast" style="text-align: justify; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%; mso-bidi-font-family: Calibri;">6.<span style="font-stretch: normal; font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">     </span></span><span style="font-size: 14.0pt; mso-bidi-font-size: 11.0pt; line-height: 115%;">Publicación en la página web del centro, como ya se está haciendo de las unidades y de los resultados de las movilidades. Concretamente se publicarán, de hecho ya se hace, en el blog de la sección bilingüe del centro y en la propia del proyecto. </span></p>
<p class="MsoNormal" style="text-align: justify; padding-left: 30px;"><span style="color: #333333; mso-ansi-language: ES;"> </span></p>
<p class="MsoNormal" style="text-align: justify; padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="color: #333333; text-decoration: underline;">ENLACES:</span></strong></span></p>
<p class="MsoNormal" style="text-align: justify; padding-left: 30px;"> </p>
<ol style="padding-left: 30px;">
<li style="padding-left: 30px;"><a href="archivos_ies/14_15/Pagina_Web.doc" target="_blank"><span style="color: #333333; text-align: justify;">Documento Word.</span></a></li>
<li style="padding-left: 30px;"><a href="archivos_ies/14_15/KA1_KA101_A_1.06_ES_SOURCE.pdf" target="_blank"><span style="color: #333333; text-align: justify;">Formulario de solicitud</span></a></li>
<li style="padding-left: 30px;"><a href="archivos_ies/14_15/1.5_Study_ecosystem.pdf" target="_blank"><span style="color: #333333; text-align: justify;">ECOSYSTEM: SCHOOL GROUND</span></a></li>
</ol>
<p style="padding-left: 30px;"> </p>
<p class="MsoNormal" style="text-align: justify;"><span style="color: #333333; mso-ansi-language: ES;"> </span></p>
<p class="MsoNormal" style="text-align: justify;"><span style="color: #333333;"><br /> </span></p>
<!--[endif]-->
<p class="MsoNormal" style="text-align: justify;"><span style="color: #333333; mso-ansi-language: ES;"> </span></p>
<p class="MsoNormal" style="text-align: justify;"><span style="color: #333333; mso-ansi-language: ES;"> </span></p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"> </p>
<hr />
<h2 style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;"><span style="text-decoration: underline;"><strong>2013/14</strong></span><br /><br /><img src="http://www.oapee.es/oapee/inicio/ErasmusPlus/contentParagraphCentralAux/00/fichero/EU%20flag-Erasmus+_vect_POS.png" border="0" alt="Erasmus +" title="Erasmus +" /></h2>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;">El Parlamento Europeo ha aprobado el programa Erasmus + para el período 2014-2020, que entró en vigor el 1 de enero de 2014.</p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;">El nuevo programa Erasmus + se enmarca en la estrategia Europa 2020, en la estrategia Educación y Formación 2020 y en la estrategia Rethinking Education y engloba todas las iniciativas de educación, formación, juventud y deporte.</p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;">En materia educativa abarca todos los niveles: escolar, formación profesional, enseñanza superior y formación de personas adultas.</p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;">Erasmus + integrará los programas existentes en el Programa de Aprendizaje Permanente y también los programas de educación superior internacional: Mundus, Tempus, ALFA, Edulink y programas bilaterales, además del Programa Juventud en Acción.</p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;">Este nuevo programa se centra en el aprendizaje formal e informal más allá de las fronteras de la UE, con una clara vocación de internacionalización abriéndose a terceros países con el objetivo de mejorar las capacidades educativas y formativas de las personas para la empleabilidad de estudiantes, profesorado y trabajadores y trabajadoras.</p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;">      <a href="http://www.oapee.es/oapee/inicio/ErasmusPlus/Convocatoria.html" style="color: #003463;"><img src="http://modeproject.eu/eventosoapee/Comun/convocatoriap.jpg" border="0" alt="" /></a>              <a href="http://www.oapee.es/oapee/inicio/ErasmusPlus/Guias-y-Manuales.html" style="color: #003463;"><img src="http://modeproject.eu/eventosoapee/Comun/guiasmanuales.jpg" border="0" alt="" /></a><br /> </p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;">      <a href="http://www.oapee.es/oapee/inicio/ErasmusPlus/KA1.html" style="color: #003463;"><img src="http://modeproject.eu/eventosoapee/Comun/ka1p.jpg" border="0" alt="" /></a>              <a href="http://www.oapee.es/oapee/inicio/ErasmusPlus/KA2.html" style="color: #003463;"><img src="http://modeproject.eu/eventosoapee/Comun/ka2p.jpg" border="0" alt="" /></a></p>
<p style="font-family: Geneva, Arial, Verdana, Tahoma, sans-serif; font-size: 12px; color: #666666; margin: 0.6em 0.7em; line-height: 1.5em; display: inline-block; width: auto;">Si necesita alguna información añadida a la que puede encontrar en los enlaces de más arriba, no dude en ponerse en contacto con nosotros a través de las siguientes direcciones de correo electrónico:</p>
<p>- Proyectos de Movilidad de Educación superior (KA1): <br />  <span id="cloak26274">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak26274').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy26274 = 'm&#111;v&#105;l&#105;d&#97;d.&#101;s' + '&#64;';
 addy26274 = addy26274 + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 var addy_text26274 = 'm&#111;v&#105;l&#105;d&#97;d.&#101;s' + '&#64;' + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 document.getElementById('cloak26274').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy26274 + '\' style="color: #003463;">'+addy_text26274+'<\/a>';
 //-->
 </script> <br />- Proyectos de Asociaciones estratégicas orientadas al campo de la Educación superior (KA2): <br />  <span id="cloak47876">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak47876').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy47876 = '&#97;s&#111;c&#105;&#97;c&#105;&#111;n&#101;s.&#101;s' + '&#64;';
 addy47876 = addy47876 + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 var addy_text47876 = '&#97;s&#111;c&#105;&#97;c&#105;&#111;n&#101;s.&#101;s' + '&#64;' + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 document.getElementById('cloak47876').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy47876 + '\' style="color: #003463;">'+addy_text47876+'<\/a>';
 //-->
 </script> </p>
<p>- Proyectos de Movilidad de Formación profesional (KA1): <br />  <span id="cloak76319">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak76319').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy76319 = 'm&#111;v&#105;l&#105;d&#97;d.fp' + '&#64;';
 addy76319 = addy76319 + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 var addy_text76319 = 'm&#111;v&#105;l&#105;d&#97;d.fp@&#111;&#97;p&#101;&#101;.&#101;s&nbsp;';
 document.getElementById('cloak76319').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy76319 + '\' style="color: #003463;">'+addy_text76319+'<\/a>';
 //-->
 </script><br />- Proyectos de Asociaciones estratégicas orientadas al campo de la Formación profesional (KA2): <br />  <span id="cloak70544">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak70544').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy70544 = '&#97;s&#111;c&#105;&#97;c&#105;&#111;n&#101;s.fp' + '&#64;';
 addy70544 = addy70544 + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 var addy_text70544 = '&#97;s&#111;c&#105;&#97;c&#105;&#111;n&#101;s.fp' + '&#64;' + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 document.getElementById('cloak70544').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy70544 + '\' style="color: #003463;">'+addy_text70544+'<\/a>';
 //-->
 </script> </p>
<p>- Proyectos de Movilidad de Educación escolar (KA1): <br />  <span id="cloak68149">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak68149').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy68149 = 'm&#111;v&#105;l&#105;d&#97;d.&#101;sc&#111;l&#97;r' + '&#64;';
 addy68149 = addy68149 + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 var addy_text68149 = 'm&#111;v&#105;l&#105;d&#97;d.&#101;sc&#111;l&#97;r@&#111;&#97;p&#101;&#101;.&#101;s&nbsp;';
 document.getElementById('cloak68149').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy68149 + '\' style="color: #003463;">'+addy_text68149+'<\/a>';
 //-->
 </script><br />- Proyectos de Asociaciones estratégicas orientadas al campo de la Educación escolar (KA2): <br />  <span id="cloak74453">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak74453').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy74453 = '&#97;s&#111;c&#105;&#97;c&#105;&#111;n&#101;s.&#101;sc&#111;l&#97;r' + '&#64;';
 addy74453 = addy74453 + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 var addy_text74453 = '&#97;s&#111;c&#105;&#97;c&#105;&#111;n&#101;s.&#101;sc&#111;l&#97;r' + '&#64;' + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 document.getElementById('cloak74453').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy74453 + '\' style="color: #003463;">'+addy_text74453+'<\/a>';
 //-->
 </script> </p>
<p>- Proyectos de Movilidad de Educación de personas adultas (KA1): <br />  <span id="cloak41402">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak41402').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy41402 = 'm&#111;v&#105;l&#105;d&#97;d.&#97;d&#117;lt&#111;s' + '&#64;';
 addy41402 = addy41402 + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 var addy_text41402 = 'm&#111;v&#105;l&#105;d&#97;d.&#97;d&#117;lt&#111;s@&#111;&#97;p&#101;&#101;.&#101;s&nbsp;';
 document.getElementById('cloak41402').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy41402 + '\' style="color: #003463;">'+addy_text41402+'<\/a>';
 //-->
 </script><br />- Proyectos de Asociaciones estratégicas orientadas al campo de la Educación de personas adultas: <br />  <span id="cloak74892">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak74892').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy74892 = '&#97;s&#111;c&#105;&#97;c&#105;&#111;n&#101;s.&#97;d&#117;lt&#111;s' + '&#64;';
 addy74892 = addy74892 + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 var addy_text74892 = '&#97;s&#111;c&#105;&#97;c&#105;&#111;n&#101;s.&#97;d&#117;lt&#111;s' + '&#64;' + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 document.getElementById('cloak74892').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy74892 + '\' style="color: #003463;">'+addy_text74892+'<\/a>';
 //-->
 </script> </p>
<p>- Otra información general: <br />  <span id="cloak35749">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak35749').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy35749 = '&#111;&#97;p&#101;&#101;' + '&#64;';
 addy35749 = addy35749 + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 var addy_text35749 = '&#111;&#97;p&#101;&#101;' + '&#64;' + '&#111;&#97;p&#101;&#101;' + '&#46;' + '&#101;s';
 document.getElementById('cloak35749').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy35749 + '\' style="color: #003463;">'+addy_text35749+'<\/a>';
 //-->
 </script></p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Proyectos</span> / <span class="art-post-metadata-category-name">PROYECTOS</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:59:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - ERASMUS+";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:8:"ERASMUS+";s:4:"link";s:59:"index.php?option=com_content&view=article&id=258&Itemid=721";}}s:6:"module";a:0:{}}
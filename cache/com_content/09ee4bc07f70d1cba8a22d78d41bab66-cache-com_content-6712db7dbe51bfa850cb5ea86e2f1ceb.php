<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:14022:"<div class="blog"><div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Visitas</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Sábado, 15 Diciembre 2012 15:56</span> | Visitas: 3511
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div>
<ul>
<li>VISITAS A LAS DISTINTAS <a href="archivos_ies/paginas_vista_2012.pdf" target="_blank">PÁGINAS DURANTE 2012.</a></li>
</ul>
</div>
<ul>
<li>Comparación de visitas <a href="archivos_ies/iesmigueldecervantes.es_comparacion_sept_oct_2011.pdf" target="_blank">septiembre-octubre 2011.</a></li>
<li>Estadísticas de visitas durante <a href="archivos_ies/visitas_septiembre_2011.pdf" target="_blank">septiembre de 2011.</a></li>
<li>Estadísticas de visitas: <a href="archivos_ies/Analytics_iesmigueldecervantes.es_20110630-20110822.pdf" target="_blank">comparación de los periodos 30/06/2011-22/08/2011 con el anterior 07/05/2011-29/06/2011.</a></li>
<li>Estadísticas de visitas: <a href="archivos_ies/Visitas_15abril_15mayo2011.pdf" target="_blank">hasta el 15/05/11.</a></li>
<li>Visitas por páginas desde el <a href="archivos_ies/Analytic_22_al_30_ junio_2011.pdf" target="_blank">22 al 30 de junio de 2011</a>. (Desde el 22 de junio la web está en el nuevo servidor Nosolored)</li>
</ul></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">LOS SITIOS DE MARIANA PINEDA</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 26 Mayo 2011 13:36</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=59:los-sitios-de-mariana-pineda&amp;catid=135&amp;Itemid=96&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=95bbc0695f47d5f2df7b9c0d6ad2d0d194cabdb6" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2358
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p style="padding-left: 30px;"><b> </b></p>
<ul style="color: #151509; margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; list-style-type: none; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url(templates/otonio6/images/postbullets.png); text-align: left; background-repeat: no-repeat no-repeat;"><b><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;">ACTIVIDAD EXTRAESCOLAR&nbsp;“<a style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: underline; color: #336600; font-size: 11px; font-weight: normal;" target="_blank" href="archivos_ies/Los_sitios_de_Mariana_de_Pineda.pdf" title="LOS SITIOS DE MARIANA PINEDA"><strong>LOS SITIOS DE MARIANA PINEDA</strong></a>”&nbsp;(UN RECORRIDO POR LA CIUDAD DE GRANADA)&nbsp;INSTITUTO MIGUEL DE CERVANTES&nbsp;(Departamento de Geografía e Historia).</span></b></li>
</ul>
<table style="border-collapse: collapse; width: 539px; text-align: center; margin: 1px;" border="0" cellspacing="2">
<tbody style="text-align: center;">
<tr style="text-align: center;">
<td style="vertical-align: top; text-align: center; padding: 2px; border: 1px solid #9a9542;"><center><span style="color: #129d0e; font-family: 'comic sans ms', sans-serif;" color="#129D0E" face="'comic sans ms', sans-serif"><span style="font-size: 12px;"><b> 
<ul style="color: #151509; margin-top: 0.5em; margin-right: 0px; margin-bottom: 0.5em; margin-left: 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; list-style-type: none; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url(templates/otonio6/images/postbullets.png); text-align: left; background-repeat: no-repeat no-repeat;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;">26/05/2011:&nbsp;<a style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;" href="index.php?option=com_content&amp;view=article&amp;id=70&amp;Itemid=110" target="_blank">Añadidas 6 fotografías de la actividad.</a></span></li>
</ul>
</b></span></span></center></td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Novedades</span> / <span class="art-post-metadata-category-name">Novedades mayo 2011</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Proyectos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 16 Mayo 2011 05:46</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=58:proyectos&amp;catid=135&amp;Itemid=96&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=dff7b443f70dffa25bf428a4edc8e5b1cd727973" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3616
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p style="text-align: center;"><br /><strong><span style="text-decoration: underline;"><span style="font-size: 12pt;">DOCUMENTOS DEL PLAN DE CENTRO.</span></span></strong></p>
<ul>
<li><a target="_blank" href="archivos_ies/PROYECTO_EDUCATIVO_IES_MIGUEL_DE_CERVANTES_definitivo.pdf" title="PROYECTO EDUCATIVO IES MIGUEL DE CERVANTES.">PROYECTO EDUCATIVO DEL IES MIGUEL DE CERVANTES.</a><strong><img style="vertical-align: bottom;" height="2" width="5" src="images/stories/cervantes/blank.png" /><img style="vertical-align: bottom;" src="images/stories/cervantes/nuevo.gif" width="40" height="12" alt="nuevo" /></strong> </li>
<li style="text-align: left;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a target="_blank" href="archivos_ies/PROYECTO_DE_GESTION_DEFINITIVO.pdf" title="PROYECTO DE GESTIÓN.">PROYECTO DE GESTIÓN.</a><img style="vertical-align: bottom;" height="2" width="5" src="images/stories/cervantes/blank.png" /><img style="vertical-align: bottom;" src="images/stories/cervantes/nuevo.gif" width="40" height="12" alt="nuevo" /></span></li>
<li style="text-align: left;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a target="_blank" href="archivos_ies/Plan_de_convivencia_definitivo.pdf" title="Plan de Convivencia.">PLAN DE CONVICENCIA DEFINITIVO.</a><img style="vertical-align: bottom;" height="2" width="5" src="images/stories/cervantes/blank.png" /><img style="vertical-align: bottom;" src="images/stories/cervantes/nuevo.gif" width="40" height="12" alt="nuevo" /><br /></span></li>
<li style="text-align: left;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a target="_blank" href="archivos_ies/ROF_nuevo_2011_definitivo.pdf" title="ROF nuevo 2011.">NUEVO ROF 2011.</a><img style="vertical-align: bottom;" height="2" width="5" src="images/stories/cervantes/blank.png" /><img style="vertical-align: bottom;" src="images/stories/cervantes/nuevo.gif" width="40" height="12" alt="nuevo" /><br /></span></li>
<li style="text-align: left;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a target="_blank" href="archivos_ies/PLAN_DE_ORIENTACION_Y_ACCION_TUTORIAL_definitivo.pdf" title="PLAN DE ORIENTACIÓN Y ACCIÓN.">PLAN DE ORIENTACIÓN Y ACCIÓN TUTORIAL.</a><img style="vertical-align: bottom;" height="2" width="5" src="images/stories/cervantes/blank.png" /><img style="vertical-align: bottom;" src="images/stories/cervantes/nuevo.gif" width="40" height="12" alt="nuevo" /><br /></span></li>
<li style="text-align: left;"><span style="font-family: 'book antiqua', palatino; color: #000080; font-size: 10pt;"><a href="archivos_ies/PLAN_DE_FORMACION_MODELO_IES_CERVANTES_definitivo.pdf" target="_blank">PLAN DE FORMACIÓN DEL IES MIGUEL DE CERVANTES.</a><strong><img src="images/stories/cervantes/blank.png" width="5" height="2" style="vertical-align: bottom;" /><img alt="nuevo" height="12" width="40" src="images/stories/cervantes/nuevo.gif" style="vertical-align: bottom;" /></strong></span></li>
<strong> </strong> 
</ul>
<br /></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Novedades</span> / <span class="art-post-metadata-category-name">Novedades mayo 2011</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Estadísticas de las evaluaciones.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 10 Mayo 2011 15:58</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=60:estadisticas-evaluaciones&amp;catid=135&amp;Itemid=96&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=eaac47f5aa9dd4ee8812bd2242b1d549e0bbcf0d" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2325
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div style="text-align: left;"><span style="color: #244061; font-family: 'Book Antiqua', serif; font-size: small;" size="3" face="'Book Antiqua', serif" color="#244061"><br /></span></div>
<ul>
<li style="text-align: left;"><span style="color: #244061; font-family: 'Book Antiqua', serif; font-size: 12pt;" href="index.php?option=com_content&amp;view=article&amp;id=91&amp;Itemid=186" target="_blank">Estadísticas de resultados de la <a target="_blank" href="archivos_ies/1ªEvaluación.pdf">1ª Evaluación curso 2010/11</a>.</span></li>
<li style="text-align: left;"><span style="color: #244061; font-family: 'Book Antiqua', serif; font-size: 12pt;" href="index.php?option=com_content&amp;view=article&amp;id=91&amp;Itemid=186" target="_blank">Estadísticas de resultados de la <a target="_blank" href="archivos_ies/2ªEvaluación.pdf">2ª Evaluación curso 2010/11</a>.</span></li>
<li style="text-align: left;"><span style="color: #244061; font-family: 'Book Antiqua', serif; font-size: 12pt;" href="index.php?option=com_content&amp;view=article&amp;id=91&amp;Itemid=186" target="_blank">2º Bachillerato B. <a target="_blank" href="archivos_ies/Máster2011.pdf">Master de Secundaria y Bachillerato 2011</a>.</span></li>
</ul></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Novedades</span> / <span class="art-post-metadata-category-name">Novedades mayo 2011</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
            </div>";s:4:"head";a:10:{s:5:"title";s:78:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Dpto. de Educación Física";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:101:"/index.php?option=com_content&amp;view=category&amp;id=135&amp;Itemid=96&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:102:"/index.php?option=com_content&amp;view=category&amp;id=135&amp;Itemid=96&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:5:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:16:"Área Artística";s:4:"link";s:59:"index.php?option=com_content&view=article&id=184&Itemid=570";}i:2;O:8:"stdClass":2:{s:4:"name";s:27:"Dpto. de Educación Física";s:4:"link";s:57:"index.php?option=com_content&view=article&id=73&Itemid=96";}i:3;O:8:"stdClass":2:{s:4:"name";s:9:"Novedades";s:4:"link";s:59:"index.php?option=com_content&view=category&id=230&Itemid=96";}i:4;O:8:"stdClass":2:{s:4:"name";s:19:"Novedades mayo 2011";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
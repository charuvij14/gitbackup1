<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:4128:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">LYCÉE AMIRAL RONARC’H</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Asociaciones</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 05 Diciembre 2012 10:24</span> | <a href="#" onclick="window.print();return false;"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 2088
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="pageName" style="font: normal normal normal 14px/normal georgia; color: #cc3300; font-weight: bold; letter-spacing: 0.1em; line-height: 26px; padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;">Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía</td>
</tr>
<tr>
<td class="bodyText" style="font: normal normal normal 11px/normal arial; color: #333333; line-height: 24px;">
<table width="98%" border="0" cellspacing="1">
<tbody>
<tr>
<td bgcolor="#9AB2CE" style="font: normal normal normal 11px/normal arial; color: #333333;"><span class="Estilo2">Asociaciones de Madres y Padres del Alumnado y Asociaciones de Alumnos/as</span></td>
</tr>
<tr>
<td class="listado" style="font: normal normal normal 11px/normal arial; color: #333333;"><ol>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc17julio2009inscripcioncensoentidades.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 17-7-2009</a>&nbsp;de la Dirección General de Participación e Innovación Educativa para la solicitud de inscripción en el Censo de Entidades Colaboradoras de la Enseñanza.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/decretos/Decreto71-2009censoentidadescolaboradoras.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">DECRETO 71/2009, de 31 de marzo</a>, por el que se regula el Censo de Entidades Colaboradoras de la Enseñanza (BOJA 17-04-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD%201740-2003%20Asociaciones%20publicas.htm" target="_blank" style="color: #005fa9; text-decoration: none;">REAL DECRETO 1740/2003, de 19 de diciembre,</a> sobre procedimientos relativos a asociaciones de utilidad pública. (BOE 13-1-2004)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/decretos/Decreto%2028-1988%20Asociaciones%20alumnos.htm" target="_blank" style="color: #005fa9; text-decoration: none;">DECRETO 28/1988</a>, de 10 de febrero, por el que se regulan las Asociaciones de Alumnos de los centros docentes no universitarios en el ámbito de la C.A. de Andalucía (BOJA 1-3-88)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/decretos/Decreto%2027-1988%20Asociaciones%20padres.htm" target="_blank" style="color: #005fa9; text-decoration: none;">DECRETO 27/1988</a>, de 10 de febrero, por el que se regulan las APAs de centros docentes no universitarios en el ámbito de la Comunidad Autónoma de Andalucía (BOJA 1-3-88).</li>
</ol></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:63:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Asociaciones";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:4:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;s:6:"robots";s:17:"noindex, nofollow";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:6:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:13:"PROYECTOS ==>";s:4:"link";s:59:"index.php?option=com_content&view=categories&id=0&Itemid=28";}i:2;O:8:"stdClass":2:{s:4:"name";s:24:"LYCÉE AMIRAL RONARC’H";s:4:"link";s:57:"index.php?option=com_content&view=article&id=67&Itemid=75";}i:3;O:8:"stdClass":2:{s:4:"name";s:9:"Normativa";s:4:"link";s:59:"index.php?option=com_content&view=category&id=196&Itemid=75";}i:4;O:8:"stdClass":2:{s:4:"name";s:17:"ÚLTIMA NORMATIVA";s:4:"link";s:59:"index.php?option=com_content&view=category&id=104&Itemid=75";}i:5;O:8:"stdClass":2:{s:4:"name";s:12:"Asociaciones";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7384:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">3º ESO</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 23 Junio 2015 08:02</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=289:3-eso&amp;catid=99&amp;Itemid=726&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=248188c1ceb28ca8a70b09427ede2922f3749637" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1535
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 5px; padding: 5px; font-size: 13.3333px; text-align: center; text-transform: none; color: #2a2a2a; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 25px; background-color: #ffffff;"><a href="archivos_ies/14_15/programaciones/Cuadernillo_padres_3ESO_14_15e.pdf" target="_blank">PROGRAMACIÓN DE 3º ESO DE MATEMÁTICAS<br />CURSO 2014/15</a></h3>
<p style="font-size: 11.8181819915771px; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;"><strong style="font-size: 14pt;"><span style="color: #000000;">MATEMÁTICAS DE 3º ESO. CURSO 2014/15</span></strong></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR PARA SEPTIEMBRE 2015</strong></span></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDADES 1 Y 2</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<hr style="background-color: #e7eaad;" />
<p style="text-align: center; background-color: #e7eaad;"><span style="text-decoration: underline;"><a href="archivos_ies/13_14/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span> </a></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank">UNIDAD: 7</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank">UNIDAD: 8</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank">UNIDAD 9</a></span></p>
<table class="borde_outset" style="text-align: center; color: #2a2a2a;" border="1">
<tbody>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 3º ESO</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></span></span><span style="color: #2f310d;"> extraídos del siguiente DOCUMENTO COMPLETO.</span><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_1_a_5.pdf" target="_blank"><strong><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></strong></a></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 21/01/2015</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números racionales</p>
<p><span style="font-size: inherit;"><strong>UNIDAD 2.</strong> Números reales</span></p>
<p><strong>UNIDAD 3.</strong> Polinomios</p>
<p><strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado </p>
<p><strong>UNIDAD 5.</strong> Sistemas de ecuaciones</p>
<p><strong> </strong></p>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> </span></span></strong><strong><span style="color: #2f310d;">extraídos del siguiente DOCUMENTO COMPLETO.</span></strong><br /><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_6_a_10.pdf" target="_blank"><strong><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></strong></a></li>
<li></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 15/04/2015</strong></span></p>
<p><strong>UNIDAD 6.</strong> Proporcionalidad numérica</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 7.</strong> Progresiones</span></p>
<p><strong>UNIDAD 8.</strong> Lugares geométricos. Figuras planas</p>
<p><strong>UNIDAD 9.</strong> Cuerpos geométricos .</p>
<p><strong>UNIDAD 10.</strong></p>
</td>
</tr>
</tbody>
</table>
<p style="font-size: 11.8181819915771px; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;"><strong style="font-size: 14pt;"><span style="color: #000000;"> </span></strong></span></p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">MATEMÁTICAS</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:58:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - 3º ESO";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:30:"Área Científica-Tecnológica";s:4:"link";s:59:"index.php?option=com_content&view=article&id=185&Itemid=571";}i:2;O:8:"stdClass":2:{s:4:"name";s:21:"Dpto. de Matemáticas";s:4:"link";s:58:"index.php?option=com_content&view=article&id=51&Itemid=103";}i:3;O:8:"stdClass":2:{s:4:"name";s:7:"3º ESO";s:4:"link";s:59:"index.php?option=com_content&view=article&id=289&Itemid=726";}}s:6:"module";a:0:{}}
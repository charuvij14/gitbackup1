<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9165:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">BECAS</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 29 Enero 2015 12:35</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=275:becas&amp;catid=143&amp;Itemid=716&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=7cebb0cefb7790461dac0109669f68ceb955a83e" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1685
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="titulo_detalle" style="margin-top: 0px; margin-bottom: 0px; font-size: 15.6000003814697px; font-weight: bold; text-align: justify; color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; text-transform: none; background-color: #ffffff;"><span style="text-decoration: underline;"><strong> </strong></span></h2>
<h2 class="titulo_detalle" style="margin-top: 0px; margin-bottom: 0px; font-size: 15.6000003814697px; font-weight: bold; text-align: justify; color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; text-transform: none; background-color: #ffffff;"><span style="text-decoration: underline;"><strong>ESTADO ACTUAL DE LAS CONVOCATORIAS DE BECAS</strong></span></h2>
<ul>
<li><a href="archivos_ies/14_15/becas_del_27_1_al_9_2_2015.pdf" target="_blank"><strong>Becas y ayudas: </strong><span style="color: #2a2a2a;">Quincena del 27.01.2015 al 09.02.2015.</span></a></li>
</ul>
<div class="separacionCapas" style="margin: 0px; padding: 0px; color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px; line-height: 15px; height: 15px !important; background-color: #ffffff;"> </div>
<div class="contenido_detalle" style="margin: 0px; padding: 0px; text-align: justify; overflow: hidden; float: left; width: 590.421875px; color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px; line-height: 15px; background-color: #ffffff;">
<div class="detalle_html" style="margin: 0px; padding: 0px; overflow: hidden;">
<div style="margin: 0px; padding: 0px;"><strong>CURSO 2014-2015:</strong></div>
<ul style="margin-right: 1em; margin-left: 20px; list-style: disc outside; font-size: 11.8181819915771px;">
<li>Convocatoria de becas para enseñanzas postobligatorias no universitarias de carácter general: <span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes para estudiantes no universitarios </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">30 de septiembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación.</span></li>
<li>Convocatoria de Ayudas para el alumnado con Necesidad Específica de Apoyo Educativo:<span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">30 de septiembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación.</span></li>
<li>BECA 6000: <span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">30 de septiembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación.</span></li>
</ul>
<ul style="margin-right: 1em; margin-left: 20px; list-style: disc outside; font-size: 11.8181819915771px;">
<li>Beca Andalucía Segunda Oportunidad:<span style="font-size: 11.8181819915771px;"> </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">30 de septiembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación</span><span style="font-size: 11.8181819915771px;">.</span></li>
<li><strong>Beca Adriano</strong>: <span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">8 de noviembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación</span>.</li>
</ul>
<div style="margin: 0px; padding: 0px;"> </div>
<div style="margin: 0px; padding: 0px;"><strong>CURSO 2013-2014:</strong></div>
<ul style="margin-right: 1em; margin-left: 20px; list-style: disc outside;">
<li>Ayudas Individualizadas para el Transportes Escolar: <span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">8 de noviembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación</span>.</li>
</ul>
</div>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">ÚLTIMA NORMATIVA</span> / <span class="art-post-metadata-category-name">NOVEDADES</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:56:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - BECAS";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:5:"BECAS";s:4:"link";s:59:"index.php?option=com_content&view=article&id=275&Itemid=716";}}s:6:"module";a:0:{}}
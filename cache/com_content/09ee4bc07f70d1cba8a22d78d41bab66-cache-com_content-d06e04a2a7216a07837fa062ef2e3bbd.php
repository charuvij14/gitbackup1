<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6390:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Andres Neuman</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Andres Neuman</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 16 Diciembre 2012 08:04</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=132:andres-neuman&amp;catid=131&amp;Itemid=186&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=51a8e17f1c9c62d35bb40b9bfd1bfdd5099216d9" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 10155
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p style="margin-bottom: 0cm; line-height: 16px;" align="justify"><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 12pt;"><span style="text-decoration: underline;"><strong>ENCUENTROS LITERARIOS EN EL INSTITUTO</strong></span></span></span></span></p>
<p style="margin-bottom: 0cm; line-height: 16px;" align="justify"><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 10pt;">Jueves 31 de marzo de 2011</span></span></span></p>
<p style="margin-bottom: 0cm; line-height: 16px;" align="justify"><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 12pt;"><strong>Andrés Neuman</strong></span></span></span><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 12pt;"> estuvo aquí, con el alumnado de 2º de Bachillerato</span></span></span></p>
<p style="margin-bottom: 0cm; line-height: 16px;" align="justify"><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 10pt;">Lo mejor de estos encuentros literarios es que el rato que pasamos en la biblioteca con el autor y el tiempo que han dedicado los profesores en clase a hablarnos de Andrés Neuman y a leer fragmentos de su obra, lo dedicamos a la literatura por puro placer. No forma parte del temario de Selectividad, no es materia evaluable, es como cuando vamos al cine o escuchamos la música que nos gusta. Quizá es el modo en que debiéramos acercarnos a la literatura siempre.</span></span></span></p>
<p style="margin-bottom: 0cm; line-height: 16px;" align="justify"><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 10pt;"><br /></span></span></span></p>
<p style="margin-bottom: 0cm; line-height: 16px;" align="justify"><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 10pt;">Nos contó un cuento y nos divertimos interpretando sus múltiples sentidos posibles, nos habló de su obra, de su vida y de la nuestra, nos leyó un fragmento de </span></span></span><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 10pt;"><em>El viajero del siglo</em></span></span></span><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 10pt;"> cómo si lo reescribiera ¡Y no dejó de hacernos preguntas!</span></span></span></p>
<p style="margin-bottom: 0cm; line-height: 16px;" align="justify"><span style="color: #244061;"><span style="font-family: 'Book Antiqua', serif;"><span style="font-size: 10pt;">Si queréis saber más sobre este autor, aquí os dejamos los enlaces a su página web y a su blog</span></span></span></p>
<p style="margin-bottom: 0cm; line-height: 16px;" align="justify"><span style="color: #0000ff;"><span style="text-decoration: underline;"><a style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;" href="http://www.andresneuman.com/">http://www.andresneuman.com/</a></span></span></p>
<p style="margin-bottom: 0cm; line-height: 16px;" align="justify"><span style="color: #0000ff;"><span style="text-decoration: underline;"><a style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;" href="http://andresneuman.blogspot.com/">http://andresneuman.blogspot.com/</a></span></span></p>
<p style="margin-bottom: 0cm;"> </p>
<p style="margin-bottom: 0cm;"><img title="Neuman." alt="Neuman." src="images/NeumanIMG_4118.jpg" width="100%" /></p>
<p style="margin-bottom: 0cm;"><img title="Neuman con el alumnado 1." alt="Neuman con el alumnado 1." src="images/NeumanIMG_4110.jpg" width="100%" /></p>
<p style="margin-bottom: 0cm;"><img title="Neuman con el alumnado y profesorado." alt="Neuman con el alumnado 2." src="images/NeumanIMG_4109.jpg" width="100%" /></p>
<p style="margin-bottom: 0cm;"><img title="Neuman con profesorado y directivos." alt="Neuman con el alumnado 3." src="images/Neuman9.jpg" width="100%" /></p>
<p style="margin-bottom: 0cm;"><img title="Neuman con el alumnado 4." alt="Neuman con el alumnado 4." src="images/Neuman31.jpg" width="100%" /></p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Actividades</span> / <span class="art-post-metadata-category-name">Andres Neuman</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:64:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Andres Neuman";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:13:"Andres Neuman";s:4:"link";s:59:"index.php?option=com_content&view=article&id=132&Itemid=186";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10097:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">LOMCE</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 13 Marzo 2015 16:52</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=274:lomce&amp;catid=143&amp;Itemid=103&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=9709240e8bdb990ec375f5537208568ca0245ede" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 19693
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><span style="color: #222222; font-family: arial, sans-serif;"><span style="color: #222222; font-family: arial, sans-serif; font-weight: normal; text-align: start;"><span style="text-decoration: underline;"><strong>ÚLTIMA HORA: NORMATIVA ESTATAL.</strong></span><br /><a href="index.php?option=com_content&amp;view=article&amp;id=274" target="_blank">BOE: RD 1105/2014, establece currÍculum básico de ESO y Bachillerato, adaptado a la LOMCE.</a><br /><a href="archivos_ies/14_15/BOE-A-2015-738.pdf" target="_blank">BOE: <span style="color: #000000; font-family: Helvetica, Arial, sans-serif, Verdana; font-size: 13.2800006866455px; line-height: 16.6000003814697px;">Orden ECD/65/2015, se describen las relaciones entre las competencias, los contenidos y los criterios de evaluación de EP, ESO y Bachillerato.</span></a><br />Falta que la </span><span style="color: #222222; font-family: arial, sans-serif; font-weight: normal; text-align: left;">legislación andaluza se adapte a este RD. VER -&gt;  </span><a href="http://www.adideandalucia.es/normas/proyectos/BorradorInstruccionesSecundaria_feb2015.pdf" target="_blank" style="font-size: 13px; color: #005fa9;">Proyecto de INSTRUCCIONES</a><span style="font-size: 13px; text-align: left; color: #414141;"> </span><span style="font-size: 13px; text-align: left; color: #414141;">de la Secretaría General de Educación de la Consejería de Educación, Cultura y Deporte, sobre la ordenación educativa de la Educación Secundaria Obligatoria y Bachillerato y otras consideraciones para el curso escolar 2015-2016 (Borrador de 16/02/2015).</span></span></p>
<p><span style="color: #222222; font-family: arial, sans-serif;"><span style="font-size: 13px; text-align: left; color: #414141;">_______________________________________________________</span></span></p>
<p><span style="color: #222222; font-family: arial, sans-serif;"> </span></p>
<p><span style="color: #222222; font-family: arial, sans-serif;">El sábado 3 de enero se publicó en el BOE el Real Decreto 1105/2014, que establece el curriculum básico de la ESO y del Bachillerato, adaptado a la LOMCE. Es de suponer que la legislación andaluza se adapte a este RD, aunque no se sabe cuándo se publicará esta legislación autonómica.</span></p>
<div style="color: #222222; font-family: arial, sans-serif;"><span style="text-decoration: underline;"><strong>Algunos comentarios de carácter general:</strong></span></div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- Se redefinen las competencias básicas en el artículo 2. Las nuevas competencias son:</div>
<div style="color: #222222; font-family: arial, sans-serif;"><ol>
<li style="margin-left: 15px;">Comunicación lingüística.</li>
<li style="margin-left: 15px;">Competencia matemática y competencias básicas en ciencia y tecnología.</li>
<li style="margin-left: 15px;">Competencia digital.</li>
<li style="margin-left: 15px;">Aprender a aprender.</li>
<li style="margin-left: 15px;">Competencias sociales y cívicas.</li>
<li style="margin-left: 15px;">Sentido de iniciativa y espíritu emprendedor.</li>
<li style="margin-left: 15px;">Conciencia y expresiones culturales.</li>
</ol></div>
<div style="color: #222222; font-family: arial, sans-serif;">- La estructura de las materias por nivel es la que ya establecía la LOMCE, así por ejemplo se habla de asignaturas troncales, específicas y de libre configuración. En el anexo I del RD se recoge el curriculum básico de las materias troncales, en el anexo II el de las materias específicas.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- Se definen como elementos transversales, que se trabajarán en todas las materias la compresión lectora, la expresión oral, la expresión escrita, la comunicación audiovisual y las TIC.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- El capítulo II trata de la ESO, con la organización de materias de la LOMCE, especifado en los artículos 13 y 14 de este RD. Son importantes los artículos 21 y 22, que tratan de la famosa reválida de la ESO y de los mecanismos de promoción respectivamente. Hay una novedad interesante: Los alumnos repetirán curso cuando suspendan más de dos materias o si suspenden Lengua y Matemáticas simultáneamente. Como ya sucedía los alumnos sólo pueden repetir el mismo curso una vez, y pueden repetir dos veces en la ESO. La edad límite de permanencia en ESO es de 19 años cumplidos dentro del curso académico.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- El capítulo III trata sobre el Bachillerato, cuya estructura desgrana en los artículos 26,27,28. El artículo 31 define la reválida para la obtención del título de Bachillerato. En cuanto a la promoción, teniendo en cuenta que hay un plazo de cuatro años para hacer los dos cursos, sólo puede repetirse una vez cada nivel salvo casos excepcionales que determinará el equipo educativo. En cuanto a la continuidad entre materias, para cursar determinadas materias de 2º será necesario haber impartido la correspondiente de 1º (p.e. para hacer Matemáticas II tiene que haber cursado Matemáticas I), si bien el profesorado que imparte la materia de 2º podrá permitir saltarse esta norma si el alumno tiene posibilidades de hacer la asignatura con aprovechamiento.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- No hay novedades significativas en lo tocante a documentos oficiales de evaluación (actas, expedientes académicos etc.).</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- Quedan derogados los Reales Decretos 1631/2006 (de la ESO) y 1467/2007 (del Bachillerato).</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;"><span style="color: #ff0000;">CALENDARIO DE IMPLANTACIÓN:</span></div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">CURSO 2015/2016:     1º Y 3º DE ESO. 1º DE BACHILLERATO.</div>
<div style="color: #222222; font-family: arial, sans-serif;"><span style="color: #ff0000;">_____________________________________________________________________________________________</span></div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">CURSO 2016/2017:     2º Y 4º DE ESO. 2º DE BACHILLERATO.</div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 </div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 PRIMERA REVÁLIDA DE ESO. SIN EFECTOS ACADÉMICOS.</div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 </div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 PRIMERA REVÁLIDA DE BACHILLERATO. CON EFECTOS PARA EL INGRESO A LA UNIVERSIDAD,</div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 PERO SIN EFECTOS PARA LA OBTENCIÓN DEL TÍTULO DE BACHILLER.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">ENTRADA EN VIGOR DEL REAL DECRETO: A PARTIR DEL 3 DE ENERO DE 2015.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;"><a href="archivos_ies/14_15/RD1105-2014CurriculoSecundaria.pdf" target="_blank">VER EL REAL DECRETO ÍNTEGRO --&gt;<br /><br /><br /></a>___________________________________________________</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">ÚLTIMA NORMATIVA</span> / <span class="art-post-metadata-category-name">NOVEDADES</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:56:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - LOMCE";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:7:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:30:"Área Científica-Tecnológica";s:4:"link";s:59:"index.php?option=com_content&view=article&id=185&Itemid=571";}i:2;O:8:"stdClass":2:{s:4:"name";s:21:"Dpto. de Matemáticas";s:4:"link";s:58:"index.php?option=com_content&view=article&id=51&Itemid=103";}i:3;O:8:"stdClass":2:{s:4:"name";s:9:"Normativa";s:4:"link";s:60:"index.php?option=com_content&view=category&id=196&Itemid=103";}i:4;O:8:"stdClass":2:{s:4:"name";s:17:"ÚLTIMA NORMATIVA";s:4:"link";s:60:"index.php?option=com_content&view=category&id=104&Itemid=103";}i:5;O:8:"stdClass":2:{s:4:"name";s:9:"NOVEDADES";s:4:"link";s:60:"index.php?option=com_content&view=category&id=143&Itemid=103";}i:6;O:8:"stdClass":2:{s:4:"name";s:5:"LOMCE";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
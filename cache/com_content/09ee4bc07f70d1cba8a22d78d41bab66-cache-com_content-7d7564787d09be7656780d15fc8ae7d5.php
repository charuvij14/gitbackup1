<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5393:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Pruebas extraordinarias</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:14</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=212:pruebas-extraordinarias&amp;catid=242&amp;Itemid=618&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=2b647198e2357e2e1ab5674856add48058084b9d" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2435
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Pruebas extraordinarias en Ciclos Formativos </h2>
<div id="articlepxfontsize1"><strong>NORMATIVA</strong>
<ul class="list-icon icon-article">
<li><a href="http://www.juntadeandalucia.es/boja/2010/202/d2.pdf" target="_blank">ORDEN de 29 de septiembre de 2010 (BOJA nº 202 de 15-10-2010).</a></li>
</ul>
<strong>DOCUMENTOS DESCARGABLES</strong>
<ul class="list-icon icon-download">
<li><a href="archivos_ies/solicitud_convo_gracia.pdf" target="_blank">Impreso de Solicitud</a></li>
</ul>
<strong>PRUEBA EXTRAORDINARIA DE EVALUACIÓN (LOGSE)</strong>
<div style="text-align: justify;">El alumnado que haya<strong> agotado el número máximo de veces que puede tener calificación final en un módulo profesional determinado (4 convocatorias)</strong>, podrá solicitar convocatoria extraordinaria siempre que concurran algunas de las circunstancias que se establecen a continuación:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li><strong>Enfermedad prolongada</strong> o accidente del alumno o alumna.</li>
<li><strong>Incorporación</strong> o desempeño de un <strong>puesto de trabajo</strong> en un horario incompatible con las enseñanzas del ciclo formativo.</li>
<li>Por <strong>cuidado de hijo o hija</strong> menor de 16 meses o por accidente grave, enfermedad grave y hospitalización del cónyuge o análogo familiar hasta el segundo grado de parentesco por consaguinidad o afinidad. </li>
</ol><strong>PLAZO Y PRESENTACIÓN DE SOLICITUDES</strong></div>
<div style="text-align: justify;">La solicitud a la que se refiere el apartado anterior estará dirigida al Director o Directora del Centro donde la persona solicitante cursó por última vez el módulo profesional correspondiente.<strong> A dicha solicitud se adjuntarán los documentos acreditativos o explicación motivada de la circunstancia que concurra</strong>.</div>
<ul class="list-icon icon-calendar">
<li>Plazo de presentación de las solicitudes: <strong>Del 1 al 30 de Octubre de cada año.</strong></li>
</ul>
<div style="text-align: justify;">En caso de presentación incompleta se requerirá a la persona solicitante para que en el plazo de <strong>diez días</strong> acompañe los documentos preceptivos, con indicación de que, si así no lo hiciera, se le tendrá por desistido de su petición, archivándose sin más trámite.<br /><br /></div>
<div style="text-align: justify;"><strong>DESARROLLO DE LAS PRUEBAS</strong></div>
<div style="text-align: justify;">Cuando se trate de una resolución favorable, la Dirección del Centro educativo comunicará al Departamento o Departamentos correspondientes que procedan a la organización de las pruebas extraordinarias de evaluación de los módulos profesionales correspondientes, que <strong>serán antes de la finalización del Primer Trimestre del curso académico.</strong> <br />En ningún caso la autorización de la prueba extraordinaria conllevará una nueva matriculación, excepto si se trata del módulo profesional de Formación en Centros de Trabajo.<br /><br /></div>
<div style="text-align: justify;"><strong>CONTENIDO DE LAS PRUEBAS</strong></div>
<div style="text-align: justify;">Las pruebas extraordinarias de evaluación se adecuarán al currículo oficial establecido en la normativa vigente que regula cada uno de los títulos de Formación Profesional Específica, teniendo en cuenta los objetivos generales, las capacidades terminales, los criterios de evaluación y los contenidos correspondientes al módulo profesional del que se trate.</div>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:74:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Pruebas extraordinarias";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:17:"Ciclos formativos";s:4:"link";s:59:"index.php?option=com_content&view=article&id=208&Itemid=615";}i:1;O:8:"stdClass":2:{s:4:"name";s:23:"Pruebas extraordinarias";s:4:"link";s:59:"index.php?option=com_content&view=article&id=212&Itemid=618";}}s:6:"module";a:0:{}}
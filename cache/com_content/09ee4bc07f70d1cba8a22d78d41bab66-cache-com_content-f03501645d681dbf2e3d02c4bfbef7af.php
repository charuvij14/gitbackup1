<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:119564:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Selectividad</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 10 Septiembre 2015 19:50</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=259:selectividad-3&amp;catid=59&amp;Itemid=218&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=6eacb0cd4f7523400953a91be6694f0cec23e06b" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 10432
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p> </p>
<table class="tabla_redonda" style="border: 1px solid #ced5d7; border-top-left-radius: 6px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 6px; padding: 10px 15px; box-shadow: #b5c1c5 0px 5px 10px, #eef5f7 0px 0px 0px 10px inset; color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left; background-color: #ffffff;" width="98%" border="0"><caption class="titulo" style="font-weight: bold; color: #ff9900; font-size: 16px; border: 1px solid #aaaaaa; vertical-align: middle; height: 45px; margin: 10px auto; -webkit-box-shadow: black 0px 0px 4px; box-shadow: black 0px 0px 4px; text-shadow: black 1px 1px 1px; padding-top: 8px; width: 95%; background: #e8edff;">CALENDARIO DE LAS PRUEBAS DE ACCESO A LA UNIVERSIDAD CURSO 2014/15<br />LUGAR: FACULTAD DE MEDICINA, AVDA. DE MADRID<br /><br /></caption></table>
<h3 style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-transform: none;" align="center"><strong>CURSO 2014/2015</strong></h3>
<table style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;" width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td align="left"><strong>Convocatoria ordinaria:</strong> <br />16, 17, 18 de junio de 2015</td>
<td align="right"><strong>Convocatoria extraordinaria:</strong> <br />15, 16 y 17 de septiembre de 2015</td>
</tr>
</tbody>
</table>
<p> </p>
<table style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;" width="90%" border="0" cellspacing="2" cellpadding="3" align="center">
<tbody>
<tr>
<td align="center" bgcolor="#D8D8D8">HORARIO</td>
<td align="center" bgcolor="#F3F781" width="25%">PRIMER DÍA<br /><strong>SE RECOMIENDA LLEGAR DE 7 A 7.30 EL PRIMER DÍA</strong></td>
<td align="center" bgcolor="#F7BE81" width="25%">SEGUNDO DÍA</td>
<td align="center" bgcolor="#00FF80" width="25%">TERCER DÍA</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td align="center" bgcolor="#D8D8D8">HORARIO</td>
<td align="center" bgcolor="#CEE3F6" width="25%">TERCER DÍA (TARDE)<br />Exclusivo para incompatabilidades</td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">08:00-08:30</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td align="center" bgcolor="#D8D8D8">16:00-16:30</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">08:30-10:00</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>HISTORIA DEL ARTE</li>
<li>MATEMÁTICAS II</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>ANÁLISIS MUSICAL II</li>
<li>DISEÑO</li>
<li>GEOGRAFÍA</li>
<li>BIOLOGÍA</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">16:30-18:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">10:00-10:45</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">18:00-18:30</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">10:45-12:15</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>HISTORIA DE ESPAÑA</li>
<li>HISTORIA DE LA FILOSOFÍA</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS</li>
<li>QUÍMICA</li>
<li>ELECTROTÉCNIA</li>
<li>LITERATURA UNIVERSAL</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>DIBUJO TÉCNICO II</li>
<li>CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES</li>
<li>ECONOMÍA DE LA EMPRESA</li>
<li>GRIEGO II</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">18:30-20:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">12:15-13:00</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">20:00-20:30</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">13:00-14:30</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>IDIOMA EXTRANJERO</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>LENGUAJE Y PRÁCTICA MUSICAL</li>
<li>TECNOLOGÍA INDUSTRIAL II</li>
<li>MATEMÁT. APLIC. A LAS CC. SOCIALES II</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>HISTORIA DE LA MÚSICA Y LA DANZA</li>
<li>DIBUJO ARTÍSTICO II</li>
<li>FÍSICA</li>
<li>LATÍN II</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">20:30-22:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
</tbody>
</table>
<p> </p>
<p class="texto_azul" style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; line-height: 16.6399993896484px;"><strong>NOTAS IMPORTANTES:</strong></p>
<ol style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>Las franjas horarias de citación son en defecto de que la universidad no fije otras que en razón de las sedes de que se traten, considere más oportunas; asi mismo sí la última franja horaria del tercer día para incompatibilidades, quedase libre, el descanso entre la primera y segunda franja horaria será de 45 minutos.</li>
<li>El horario de las materias del tercer día por la tarde es <strong><span style="text-decoration: underline;">exclusivamente</span></strong> para quienes tienen más de un examen el mismo día y a la misma hora (en el 2º o 3º día por la mañana), debiendo realizar en dicho horario y en primer lugar el examen que aparece antes en el respectivo cuadro y realizará el examen de la otra materia en el tercer día por la tarde en el horario que se indicará oportunamente.</li>
</ol>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px; background-color: #ffffff; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; text-align: center;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><span style="text-decoration: underline;"><span style="color: #2a2a2a;"><span style="text-transform: none;"><span style="font-size: 14pt;"><strong>SELECTIVIDAD SEPTIEMBRE 2014<br /><br /><a href="index.php?option=com_content&amp;view=article&amp;id=262kYWQiXQ==" target="_blank">V E R   E X Á M E N E S: D Í A S 16 - 17 - 18 S E P T I E M B R E<br /></a><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Las notas están disponibles el día 24</a><br /></strong></span></span> </span> </span><a href="archivos_ies/14_15/mate/Normas_PAU.pdf" target="_blank"><span style="color: #2a2a2a; font-size: 12pt;"><span style="text-transform: none;">Normas de las PAU</span></span></a><span style="text-decoration: underline;"><br /> </span><span style="font-family: verdana,geneva;"><span style="color: #2a2a2a; font-size: 10pt;"><span style="text-transform: none;"><span style="color: #000000; background-color: #ffffff;">SEDE 5 (GRANADA) FACULTAD DE MEDICINA<br />AVDA. DE MADRID S/N</span></span> </span><span style="color: #2a2a2a; font-size: 12pt;"><span style="text-transform: none;"><br /><a href="index.php?option=com_content&amp;view=article&amp;id=254">Ver más información</a> </span> </span> </span> </span></p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px; background-color: #ffffff; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; text-align: center;"><span style="text-decoration: underline; font-size: 14pt; color: #000000;"><strong>SELECTIVIDAD JUNIO<a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><span style="color: #000000; text-decoration: underline;"> 2014:</span> </a> </strong> </span><br /><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO.">ENHORABUENA A NUESTRO ALUMNADO, HA SUPERADO LAS SELECTIVIDAD UN 98,57 % (69 DE 70 ALUMNOS PRESENTADOS DE BACHILLERATO Y LOS 3 DEL CICLO)</a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."> </a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO.">ESTUDIO ESTADÍSTICO DE LAS NOTAS OBTENIDAS POR EL ALUMNADO DEL</a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."> IES MIGUEL DE CERVANTES</a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/pau.png" border="0" alt="" width="407" height="305" /></a><strong style="background-color: transparent; text-align: center;"><strong style="background-color: transparent; text-align: center;"><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."> <img src="archivos_ies/13_14/selectividad/comunes1.png" border="0" alt="" style="border: 0pt none;" /></a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/gen-esp.png" border="0" alt="" width="549" height="546" /></a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/dosfases.png" border="0" alt="" width="549" height="260" style="border: 0pt none;" /></a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/aprobados1.png" border="0" alt="" style="border: 0pt none;" /></a><br /><a href="archivos_ies/13_14/selectividad/NOTAS_DE_SELECTIVIDAD7.pdf" target="_blank" title="ESTUDIO ESTADÍSTICO. PULSAR PARA VER AMPLIADO."><img src="archivos_ies/13_14/selectividad/medias3.png" border="0" alt="" width="551" height="375" style="border: 0pt none;" /></a><br />__________________________________________________________________________________<br /></strong></strong>(Análisis realizado por Miguel Anguita)</p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px; background-color: #ffffff; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: center;"><span style="color: #5d5d5d; font-size: 31px; font-weight: bold; text-align: left; text-transform: uppercase; background-color: #fafbf0;">INSCRIPCIÓN PRUEBAS DE ACCESO A LA UNIVERSIDAD</span></p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px; background-color: #ffffff; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: center;"><a id="enlace_pdf" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion?theme=pdf" title="Versión en PDF" style="font-size: 13px; background-color: #fafbf0;">Descargar versión en PDF<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_requisitos" style="font-size: 13px; background-color: #fafbf0;">Requisitos<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_plazos_y_solicitud" style="font-size: 13px; background-color: #fafbf0;">Plazos y solicitud<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_precios_de_inscripcion" style="font-size: 13px; background-color: #fafbf0;">Precios de inscripción<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_hoja_informativa" style="font-size: 13px; background-color: #fafbf0;">Hoja informativa<br /><br /></a><a class="toc" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_adaptacion_de_los_examenes_para_estudiantes_con_discapacidad" style="font-size: 13px; background-color: #fafbf0;">Adaptación de los exámenes para estudiantes con discapacidad</a></p>
<div id="pagina">
<div id="contenido" class="sec_interior">
<div class="content_doku">
<h1>REQUISITOS</h1>
<div class="level1">
<ul>
<li class="level1">
<div class="li">Estar en posesión del <strong>Título de Bachillerato</strong> para la realización de la prueba completa.</div>
</li>
<li class="level1">
<div class="li">Estar en posesión del <strong>Título de técnico superior de formación profesional, técnico superior de artes plásticas y diseño, o técnico deportivo superior o título equivalente</strong>, únicamente a efectos de la <em class="u">fase específica voluntaria</em> para mejorar la nota de admisión a las enseñanzas oficiales de grado.</div>
</li>
</ul>
</div>
<h1>PLAZOS Y SOLICITUD</h1>
<div class="level1">
<p>Todos los/as estudiantes que estén interesados en participar en la Prueba de Acceso a la Universidad tanto en su fase general como específica, ya sean alumnos/as que se presentan por primera vez o para mejorar nota, en cualquiera de sus convocatorias Ordinaria o Extraordinaria, deberán realizar los siguientes pasos en los plazos establecidos:</p>
</div>
<h2>PRIMER PASO: REGISTRARSE</h2>
<div class="level2">
<p>Hay que cumplimentar el trámite de <strong>REGISTRO</strong> a través del portal WEB de Selectividad:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a class="urlextern" href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" title="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></div>
</li>
</ul>
<p><strong>PLAZO DE REGISTRO</strong> SELECTIVIDAD CONVOCATORIA 2014:</p>
<table class="inline">
<tbody>
<tr><th>Convocatoria Ordinaria</th><th>Convocatoria Extraordinaria</th></tr>
<tr>
<td class="leftalign">del 22 de abril al 4 de junio 2014</td>
<td class="leftalign">del 1 de agosto al 5 de septiembre 2014</td>
</tr>
</tbody>
</table>
<p>Aquí puedes encontrar una presentación que te ayudará a realizar el registro:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad1" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad1">Alumnos que este curso aprobarán el 2º de Bachillerato o el 2º curso del CFGS en un centro de Granada, Ceuta, Melilla o centros adscritos a la Universidad de Granada en Marruecos</a></div>
</li>
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad2" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad2">Alumnos que se han presentado alguna vez a Selectividad en la Universidad de Granada</a></div>
</li>
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad3" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad3">Alumnos procedentes de otras universidades y que nunca se han presentado a Selectividad en la Universidad de Granada</a></div>
</li>
</ul>
</div>
<h2>SEGUNDO PASO: MATRICULARSE</h2>
<div class="level2">
<p>Una vez hecho el registro hay que realizar la inscripción (<strong>MATRÍCULA</strong>) en la prueba a través del portal WEB de Selectividad:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a class="urlextern" href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" title="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></div>
</li>
</ul>
<p><strong>PLAZO DE MATRÍCULA</strong> SELECTIVIDAD CONVOCATORIA 2014:</p>
<table class="inline">
<tbody>
<tr><th>Convocatoria Ordinaria</th><th>Convocatoria Extraordinaria</th></tr>
<tr>
<td class="leftalign">2 al 4 de junio 2014</td>
<td class="leftalign">3 al 5 de septiembre 2014</td>
</tr>
</tbody>
</table>
<ul class="departamento">
<li class="level1 level1_primero impar">
<div class="li"><strong>MUY IMPORTANTE:</strong></div>
<ul>
<li class="level4">
<div class="li"><strong>La matrícula se podrá realizar hasta las 24:00h del día en que finaliza el plazo y no se admitirán pagos posteriores a las 14:00h del día siguiente a dicha finalización.</strong></div>
</li>
</ul>
</li>
</ul>
<ul class="enlaces">
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectividadgranada" title="pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectividadgranada">Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Granada y provincia</a></div>
</li>
<li class="level1">
<div class="li"><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectivdadceutamelillamarruecos" title="pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectivdadceutamelillamarruecos">Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Ceuta, Melilla y Marruecos</a></div>
</li>
</ul>
<p><strong>NOTA CONVOCATORIA SEPTIEMBRE: Solo el ingreso</strong> mediante carta de pago se podrá realizar hasta el día 8 de septiembre debido a que los días 6 y 7 de septiembre son sábado y domingo respectivamente; aunque la carta de pago se debe haber obtenido en el plazo del 3 al 5 de septiembre.</p>
</div>
<h1>PRECIOS DE INSCRIPCIÓN</h1>
<div class="level1">
<ul>
<li class="level1">
<div class="li">Los derechos de examen se podrán ingresar mediante <strong>Carta de Pago</strong> en cualquier sucursal de <em>Caja Granada o Caja Rural de Granada</em> o mediante <strong>pago con tarjeta</strong>. Ambas opciones estarán reflejadas en la solicitud de inscripción que deberá realizar en esta misma WEB.</div>
</li>
</ul>
<ul>
<li class="level1">
<div class="li">La cantidad a ingresar será:</div>
</li>
</ul>
<ul class="departamento">
<li class="level1 par">
<div class="li"><strong>Precios públicos ordinarios:</strong></div>
<ul>
<li class="level4">
<div class="li"><strong>Fase General</strong>: 58.70 € .</div>
</li>
<li class="level4">
<div class="li"><strong>Fase Específica</strong>: 14.70 € x número de materias de las que se examina.</div>
</li>
</ul>
</li>
</ul>
<ul class="departamento">
<li class="level1 impar">
<div class="li"><strong>Precios públicos para beneficiarios Familia Numerosa:</strong></div>
<ul>
<li class="level4">
<div class="li">De categoría <strong>general</strong>: ......................... Reducción del 50%.</div>
</li>
<li class="level4">
<div class="li">De categoría <strong>especial</strong>: ........................ Exentos de pago.</div>
</li>
</ul>
</li>
</ul>
<ul class="departamento">
<li class="level1 par">
<div class="li"><strong>Precios públicos para personas con discapacidad</strong>:</div>
<ul>
<li class="level4">
<div class="li">Exentos de pago.</div>
</li>
<li class="level4">
<div class="li">tendrán la consideración de personas con discapacidad aquellas a quienes se les haya reconocido un grado de minusvalía igual o superior al 33 % por la Consejería para la Igualdad y Bienestar Social de la Junta de Andalucía u órgano competente en la comunidad de procedencia del interesado.</div>
</li>
</ul>
</li>
<li class="level1 level1_ultimo impar">
<div class="li"><strong>IMPORTANTE:</strong></div>
<ul>
<li class="level4">
<div class="li">Los alumnos que tengan exención total de precios públicos bien por <em class="u"><strong>FAMILIA NUMEROSA DE CATEGORÍA ESPECIAL</strong></em>, o <em class="u"><strong>DISCAPACIDAD</strong></em> deberán acceder a la misma página web y seguir el mismo proceso de matriculación que finalizará con la impresión del RESGUARDO, quedando ya matriculados. El reguardo deberán llevarlo el día del examen.</div>
</li>
<li class="level4">
<div class="li">Los documentos justificativos de familia numerosa como de discapacidad deben estar actualizados en el momento de realizar la inscripción.</div>
</li>
</ul>
</li>
</ul>
</div>
<h1>HOJA INFORMATIVA</h1>
<div class="level1">
<p><a class="wikilink2" href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02" title="pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02">Hoja informativa inscripción/horarios Selectividad</a></p>
</div>
<h1>ADAPTACIÓN DE LOS EXÁMENES PARA ESTUDIANTES CON DISCAPACIDAD</h1>
<div class="level1">
<p>Todos los alumnos que padezcan alguna discapacidad y que necesiten condiciones especiales para la realización de las pruebas deberán:</p>
<ul>
<li class="level1">
<div class="li">Comunicarlo a este Servicio y justificarlo mediante informe del Centro en el que consten las adaptaciones que necesite, y certificado expedido por la Consejería para la igualdad y bienestar social de la Junta de Andalucía o la administración correspondiente en otra Comunidad, en el que conste el grado de minusvalía.</div>
</li>
</ul>
<p>Los documentos acreditativos deberán estar actualizados.</p>
<p> </p>
</div>
</div>
</div>
</div>
<div style="font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13px; text-align: justify;"> </div>
<div id="pagina">
<div id="contenido" class="sec_interior">
<div class="content_doku">
<div class="level1">
<p> </p>
<table class="tabla_redonda" style="border: 1px solid #ced5d7; border-top-left-radius: 6px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 6px; padding: 10px 15px; box-shadow: #b5c1c5 0px 5px 10px, #eef5f7 0px 0px 0px 10px inset; color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left; background-color: #ffffff;" width="98%" border="0"><caption class="titulo" style="font-weight: bold; color: #ff9900; font-size: 16px; border: 1px solid #aaaaaa; vertical-align: middle; height: 45px; margin: 10px auto; -webkit-box-shadow: black 0px 0px 4px; box-shadow: black 0px 0px 4px; text-shadow: black 1px 1px 1px; padding-top: 8px; width: 90%; background: #e8edff;">EXÁMENES Y ORIENTACIONES SOBRE SELECTIVIDAD</caption></table>
<p style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; line-height: 16.6399993896484px;" align="center">Si desea conocer el tamaño del archivo a descargar, deje el ratón sobre el icono del archivo correspondiente. </p>
<table style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td><strong>Asignatura</strong></td>
<td align="center"><strong>Orientaciones <br />2014/2015</strong></td>
<td align="center"><strong>2005</strong></td>
<td align="center"><strong>2006</strong></td>
<td align="center"><strong>2007</strong></td>
<td align="center"><strong>2008</strong></td>
<td align="center"><strong>2009</strong></td>
<td align="center"><strong>2010</strong></td>
<td align="center"><strong>2011</strong></td>
<td align="center"><strong>2012</strong></td>
<td align="center"><strong>2013</strong></td>
</tr>
<tr>
<td>Análisis Musical II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_analisis_musical_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="1.23 Mb" title="1.23 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="5.31 Mb" title="5.31 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="170.95 Mb" title="170.95 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="265.22 Mb" title="265.22 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Biología <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_biologia_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="1.58 Mb" title="1.58 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.09 Mb" title="2.09 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.29 Mb" title="1.29 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.26 Mb" title="1.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.07 Mb" title="2.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.54 Mb" title="2.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Ciencias de la Tierra y Medioambientales <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_ciencias_de_la_tierra_y_medioambientales_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.64 Mb" title="0.64 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.44 Mb" title="1.44 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="14.79 Mb" title="14.79 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.47 Mb" title="1.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.88 Mb" title="1.88 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.45 Mb" title="1.45 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Comentario de Texto,Lengua Castellana y Literatura <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_comentario_texto_lengua_castellana_y_literatura_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.36 Mb" title="0.36 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.4 Mb" title="0.4 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Dibujo Artístico II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_artistico_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="13.76 Mb" title="13.76 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.22 Mb" title="2.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.34 Mb" title="1.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="5.2 Mb" title="5.2 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="27.54 Mb" title="27.54 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="13.52 Mb" title="13.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="59.45 Mb" title="59.45 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.72 Mb" title="3.72 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Dibujo Técnico II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_tecnico_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.56 Mb" title="2.56 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.86 Mb" title="6.86 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="8.05 Mb" title="8.05 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="15.45 Mb" title="15.45 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.99 Mb" title="12.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.97 Mb" title="6.97 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Diseño <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_diseno_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.38 Mb" title="12.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.66 Mb" title="0.66 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.12 Mb" title="1.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.76 Mb" title="3.76 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Economía de la Empresa <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_economia_de_la_empresa_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.1 Mb" title="0.1 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.31 Mb" title="0.31 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.96 Mb" title="3.96 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.38 Mb" title="1.38 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Electrotecnia <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_electrotecnia_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.46 Mb" title="0.46 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Física <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_fisica_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.26 Mb" title="0.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.28 Mb" title="0.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.25 Mb" title="0.25 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.39 Mb" title="1.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.36 Mb" title="0.36 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Geografía <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_geografia_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.29 Mb" title="0.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.09 Mb" title="3.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.32 Mb" title="1.32 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.41 Mb" title="12.41 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.11 Mb" title="2.11 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.86 Mb" title="3.86 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.98 Mb" title="1.98 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Griego II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_griego_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.54 Mb" title="0.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.6 Mb" title="0.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.35 Mb" title="2.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="4.69 Mb" title="4.69 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.57 Mb" title="0.57 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.51 Mb" title="0.51 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Historia de España <em>(Común)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_espanna_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.74 Mb" title="0.74 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.73 Mb" title="1.73 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.77 Mb" title="1.77 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia de la Filosofía <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_filosofia_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.05 Mb" title="0.05 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.68 Mb" title="0.68 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Historia de la Música y de la Danza <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_musica_y_de_la_danza_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="3.3 Mb" title="3.3 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="27.28 Mb" title="27.28 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="22.08 Mb" title="22.08 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.51 Mb" title="1.51 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="13.4 Mb" title="13.4 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.38 Mb" title="2.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="318.43 Mb" title="318.43 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="10.61 Mb" title="10.61 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="93.24 Mb" title="93.24 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="101.86 Mb" title="101.86 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia del Arte <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_del_arte_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.49 Mb" title="0.49 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.67 Mb" title="0.67 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.16 Mb" title="3.16 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="8.71 Mb" title="8.71 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.91 Mb" title="6.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.5 Mb" title="3.5 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="18.28 Mb" title="18.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.06 Mb" title="1.06 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Latín II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_latin_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.94 Mb" title="0.94 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.94 Mb" title="1.94 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1 Mb" title="1 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.55 Mb" title="0.55 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Alemán) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(aleman)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.26 Mb" title="0.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.35 Mb" title="1.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.39 Mb" title="0.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.3 Mb" title="1.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Francés) <em>(Común)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(frances)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.1 Mb" title="0.1 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Inglés) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(ingles)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.75 Mb" title="0.75 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.68 Mb" title="1.68 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.06 Mb" title="2.06 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.18 Mb" title="1.18 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.19 Mb" title="12.19 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.74 Mb" title="1.74 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Italiano) <em>(Común)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(italiano)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.29 Mb" title="0.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.75 Mb" title="0.75 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Portugués) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(portugues)_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.32 Mb" title="0.32 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.78 Mb" title="0.78 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.77 Mb" title="0.77 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lenguaje y Práctica Musical <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lenguaje_y_practica_musical_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="1.4 Mb" title="1.4 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="92.65 Mb" title="92.65 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.31 Mb" title="2.31 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="80.36 Mb" title="80.36 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="64.72 Mb" title="64.72 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Literatura Universal <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_literatura_universal_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.76 Mb" title="0.76 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Matemáticas Aplicadas a las Ciencias Sociales II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.62 Mb" title="0.62 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.51 Mb" title="6.51 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.42 Mb" title="1.42 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.53 Mb" title="1.53 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Matemáticas II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="4.39 Mb" title="4.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.54 Mb" title="1.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.13 Mb" title="1.13 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Química <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_quimica_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.34 Mb" title="0.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.34 Mb" title="0.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.19 Mb" title="1.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.5 Mb" title="0.5 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.66 Mb" title="1.66 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Técnicas de Expresión Gráfico Plásticas <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnicas_de_expresion_grafico-plastica_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.85 Mb" title="0.85 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="10.44 Mb" title="10.44 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.86 Mb" title="0.86 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="4.72 Mb" title="4.72 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="8.63 Mb" title="8.63 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.41 Mb" title="1.41 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Tecnología Industrial II <em>(Modalidad)</em></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnologia_industrial_ii_2014_2015.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.14 Mb" title="1.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="center" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.12 Mb" title="1.12 Mb" align="center" /></a></td>
</tr>
</tbody>
</table>
<p> </p>
<p style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; line-height: 16.6399993896484px;"> </p>
<p><span style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; background-color: #ffffff;">Si desea conocer el tamaño del archivo a descargar, deje el ratón sobre el icono del archivo correspondiente.</span></p>
</div>
</div>
</div>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">ALUMNADO</span> / <span class="art-post-metadata-category-name">Selectividad</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:63:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - SELECTIVIDAD";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:12:"SELECTIVIDAD";s:4:"link";s:59:"index.php?option=com_content&view=article&id=259&Itemid=218";}}s:6:"module";a:0:{}}
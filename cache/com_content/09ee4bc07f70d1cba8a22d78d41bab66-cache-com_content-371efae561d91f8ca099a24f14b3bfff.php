<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12138:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">CURSO 2013/14</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Oferta educativa</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 24 Octubre 2013 10:32</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=71:oferta-educativa-del-instituto-miguel-de-cevantes&amp;catid=37&amp;Itemid=193&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=ab7c7c400b0f3596c0f00a336f254b204b99a923" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 6060
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h4 class="contentheading"><span style="font-family: 'arial black', 'avant garde';"><span style="text-decoration: underline;">Oferta educativa del Instituto Miguel de Cevantes de </span></span><span style="font-family: 'arial black', 'avant garde';"><span style="text-decoration: underline;">Granada<br /></span></span></h4>
<h2><strong><span style="font-size: 12pt;"><span style="font-family: 'comic sans ms',sans-serif;"><span style="color: #993300;"> </span></span></span><span style="text-decoration: underline; font-size: 12pt;"><span style="font-family: 'comic sans ms',sans-serif;"><span style="color: #993300;">Eso:</span></span></span></strong></h2>
<ul>
<li>Primer y Segundo cursos de Educación Secundaria Obligatoria.</li>
<li>Tercer y Cuarto cursos de Educación Secundaria Obligatoria. Diversificación Curricular: 3º y 4º.</li>
</ul>
<p> </p>
<p><span style="text-decoration: underline; font-size: 12pt;"><strong><span style="font-family: 'comic sans ms', sans-serif; color: #800000;">Bachillerato:</span></strong></span></p>
<ul>
<li>Ciencias de la Naturaleza y de la Salud y Tecnología 1º y 2ºº: dos itrinerarios.</li>
<li>Humanidades y Ciencias Sociales 1º y 2º: dos itrinerarios.</li>
</ul>
<p> </p>
<p><span style="text-decoration: underline; font-size: 12pt;"><span style="font-family: 'comic sans ms', sans-serif;"><strong><span style="color: #800000;">Formación Profesional de Grado Superior:</span></strong></span></span></p>
<ul>
<li>Ciclo Superior de Animación Sociocultural: 1º y 2º</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Grupos (curso 2012/13)</span></strong></span></p>
<ul>
<li>1º ESO: 4 grupos.</li>
<li>2º ESO: 4 grupos.</li>
<li>3º ESO: 4 grupos.</li>
<li>4º ESO: 3 grupos.</li>
<li>Bachiller de Ciencias: 1,5 grupos en primero y 1,5 grupos en segundo.</li>
<li>Bachiller de Humanidades/Sociales: 2,5 grupos en primero y 2,5 grupos en segundo.</li>
<li>Varios grupos de Apoyo (Integración)</li>
</ul>
<p><br /><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Alumnado:</span></strong></span></p>
<ul>
<li>En el último curso 780 alumnos/as.</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Profesorado:</span></strong></span></p>
<ul>
<li>54 profesores/as.</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Planes y Programas educativos:</span></strong></span></p>
<ul>
<li>Plan de Bilngüismo: 2 secciones. Dotación de instalaciones TIC correspondientes. Centro bilingüe Español - Inglés</li>
<li>Proyecto de Biblioteca. Fomento de la lectura.</li>
<li>Plan de Igualdad hombres y mujeres.</li>
<li>Plan "A no fumar me apunto".</li>
<li>Plan de Salud Laboral.</li>
<li>Plan de Autoprotección.</li>
<li>Proyecto "Kyoto Educa".</li>
<li>Programa Comenius. Auxiliares de conversación e intercambios.</li>
<li>Proyectos intercambios escolares.</li>
</ul>
<p> </p>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Otros planes/programas:</span></strong></span></p>
<ul>
<li>That's English.</li>
<li>Centro Examinador del Trinity College.</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Intercambios escolares:</span></strong></span></p>
<ul>
<li>Instituto Kongsberg (Noruega).</li>
<li>Instituto de Osimo. Ancona (Italia), año 2011/12.</li>
<li>Instituto de Aix en Provence (Francia), año 2011/12.</li>
<li>Instituto Berqueley Carroll de Brooklyn, Nueva York (USA).</li>
<li>Instituto del Reino Unido. Visita preparatoria.</li>
<li>Intercambios de un día con varios institutos de alumnado de distintas nacionalidades (ingleses, suizos, nigerianos, franceses, canadienses...).</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Instalaciones:</span></strong></span></p>
<ul>
<li>Edificio propio en una parcela de 9300 m<sup>2 </sup>con ámplos jardines. </li>
<li>Pista polideportiva de futbito y baloncesto.</li>
<li>Aparcamiento profesorado.</li>
<li>gimnasio cubierto.</li>
<li>4 aulas de Informática fijas.</li>
<li>5 aulas móviles, dotadas con un total de 76 ordenadores portátiles.</li>
<li>Red WIFI en todo el Centro.</li>
<li>Laboratorios de Física y Química.</li>
<li>Laboratorios de Biología y Geología.</li>
<li>Taller de Tecnología.</li>
<li>Aulas específicas de: Dibujo, Música, Ciclo de animación Sociocultural.</li>
<li>Cafetería.</li>
<li>Departamentos de: Orientación, Animación Sociocultural, Dibujo/Tecnología, Lengua y Literatura, Biología y Geología, Inglés, Francés, Matemáticas, Geografía e Historia, Filosofía/Religión, Física y Química y Educaciñón Física.</li>
<li>Aula específica de Apoyo a la Integración.</li>
<li>Salón de Usos Múltiples.</li>
<li>área administrativa.</li>
<li>Secretaría.</li>
<li>Conserjería, Jefatura de Estudios.</li>
<li>Dirección.</li>
<li>Recepción familias y AMPA.</li>
<li>Accesos minusválidos: rampas y ascensor.</li>
<li>Almacenes.</li>
<li>Biblioteca on una ámplia dotación de bibliografía y acceso a Internet.</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Servicios:</span></strong></span></p>
<ul>
<li>Plan de apoyo a las familias: actividades extraescolares en horario de tarrde: deportivas y formativas.</li>
<li>Transporte Escolar (contratado por el ISE Andalucía).</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Control de Asitencia/Calificaciones:</span></strong></span></p>
<ul>
<li>Control diario hora por hora y consulta a partir de las 15,00 horas vía WEB (<a href="http://www.sgdweb.com" target="_blank">www.sgdweb.com</a>) con acceso identificado para las familias del alumnado.</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Actividades extraescolares y complementarias:</span></strong></span></p>
<ul>
<li>Numerosas y variadas. Programadas por los Departamentos o por el Centro en general (Viajes de estudios, convivencia, acampadas, final de ciclo, titotía, visitas al entorno cultural,...)</li>
<li>Relacionadas con el estudio de Idiomas (actividades de los Departamentos de Inglés y Francés).</li>
<li>Actividades deportivas.</li>
<li>Relacionadas con el Plan de Lectura y Bilioteca: Teatro, jornadas de lectura, jornadas con escritores ilustres,...</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Departamento de Orientación:</span></strong></span></p>
<ul>
<li>Asistencia pedagógica, orientación a familias y profesional.</li>
<li>Consulta Forma Joven (Persoanl Sanitario del Centro de ASalud Mirasierra).</li>
<li>Actividades del Departamento: trastornos alimentarios, prevención de drogodependencias, educación vial, educación afectivo-sexual, autoconocimento, habilidades sociales,...</li>
<li>ATAL: Atención al alumando extrajero con dificultas del idioma.</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Currúculo:</span></strong></span></p>
<ul>
<li>Incremento de una hora lectiva de Matemáticas y una hora lectiva de Lengua Castellana en 1º ESO.</li>
<li>Incremento de una hora lectiva de Lengua Castellana en 2º ESO.</li>
<li>Desdoble horario optativo para talleres de lectura en 1º y 2º ESO.</li>
<li>Refuerzo de Matemáticas en 3º ESO.</li>
<li>Itinerarios formativos adaptados a los Bachilleratos en 4º ESO:</li>
<ol>
<li>Itinerario científico/tecnológico.</li>
<li>Itinerario humanístico/socilaes.</li>
<li>Itienrario tecnológico/profesional.</li>
</ol>
<li>Primer idioma Inglés y Francés en todos los niveles.</li>
<li>Segundo idioma Inglés y Francés en todos los niveles.</li>
</ul>
<p><span style="color: #800000; font-family: 'comic sans ms', sans-serif; font-size: 12pt;"><strong><span style="text-decoration: underline;">Nota final:</span></strong></span></p>
<p>Pueden amplar la información de nuestro Centro a través de esta página Web, en los partados en los que se encuentran los documentos de interés como:</p>
<ul>
<li>El Plan de Centro que contiene:</li>
<ol>
<li><a href="http://iesmigueldecervantes.es/archivos_ies/PROYECTO_EDUCATIVO_IES_MIGUEL_DE_CERVANTES_definitivo.pdf" target="_blank">El Proyecto Eduativo</a>.</li>
<li><a href="http://iesmigueldecervantes.es/archivos_ies/ROF_nuevo_2011_definitivo.pdf" target="_blank">El Reglamento de Organización y funcionamiento.</a></li>
<li><a href="http://iesmigueldecervantes.es/archivos_ies/PROYECTO_DE_GESTION_DEFINITIVO.pdf" target="_blank">El Proyecto de Gestión.</a></li>
<li><a href="http://iesmigueldecervantes.es/archivos_ies/Plan_de_convivencia_definitivo.pdf" target="_blank">El Plan de Convivencia.</a></li>
<li><a href="http://iesmigueldecervantes.es/archivos_ies/PLAN_DE_ORIENTACION_Y_ACCION_TUTORIAL_definitivo.pdf" target="_blank">El Plan de Orientación y Acción Tutorial.</a></li>
<li><a href="http://iesmigueldecervantes.es/archivos_ies/PLAN_DE_FORMACION_MODELO_IES_CERVANTES_definitivo.pdf" target="_blank">El Plan de Formación.</a></li>
</ol>
<li><a href="http://iesmigueldecervantes.es/index.php?option=com_content&amp;view=article&amp;id=37&amp;Itemid=59" target="_blank">La distribución de Departamentos.</a></li>
<li><a href="http://iesmigueldecervantes.es/archivos_ies/PROGRAMACION_DEL_DEPARTAMENTO_ACE.docx" target="_blank">La reseña de actividades extraescolares</a> e intercambios realizados</li>
</ul></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:81:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - OFERTA EDUCATIVA CURSO 2013/14";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";s:58:"Uncategorized, Uncategorized, Example Pages and Menu Links";s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:6:"INICIO";s:4:"link";s:53:"index.php?option=com_content&view=featured&Itemid=250";}i:1;O:8:"stdClass":2:{s:4:"name";s:16:"Oferta educativa";s:4:"link";s:58:"index.php?option=com_content&view=article&id=71&Itemid=193";}}s:6:"module";a:0:{}}
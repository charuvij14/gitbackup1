<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7143:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">CURSO 2013/14</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Historia del Instituto</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 05 Diciembre 2012 10:24</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=17:historia-del-instituto&amp;catid=42&amp;Itemid=194&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=fe4fe8a7302d7d89d03c903a68239385cdde80df" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 555
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div id="tituloItem" class="blockTituloItem" style="margin-top: 15px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; border-bottom-width: 2px; border-bottom-style: dotted; border-bottom-color: #aaaaaa; float: center; width: 100%; padding: 0px;">
<div id="contenido_sitio" style="width: 100%; float: left; display: block;">
<p style="text-align: justify;"><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;"><span class="Estilo7">Nacido a fines de la década de los ochenta del siglo pasado, surge como extensión del Instituto Fray Luis de Granada, debido a la falta de espacio que provocaba la fuerte demanda del alumnado para cursar estudios en este centro. En un principio, sin sede propia, acabaría dotándose de las instalaciones actuales en el Camino de los Neveros. Una vez creado, como centro independiente, recibiría el nombre de IES Nº 3 de Granada, empezando a funcionar como tal en el curso 1991-1992. Aunque popularmente, fue llamado Instituto Bola de Oro, por el barrio donde se levantaba, como todavía es frecuente oír. Algo después, el Claustro y Consejo Escolar aprobaron solicitar la denominación de IES Miguel de Cervantes, acomodándola tanto al ilustre escritor como a la Avenida Cervantes, aledaña al edificio.</span></span></p>
<p class="Estilo7"><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;"><span class="Estilo4"><img style="float: right;" alt="_fachada2" class="Estiloimg" src="images/stories/fotos_ies/_fachada2.jpg" width="404" height="264" hspace="10" vspace="15" /></span>Desde un punto de vista meramente administrativo y de gestión,el Centro ha dispuesto a lo largo desu historia de cinco equipos directivos diferentes, coordinados por los siguientes directores:<em>desde su inicio y hasta el curso 1993-94</em>,&nbsp;<strong>Dª María Lorente Navío</strong>;<em>durante el curso 1994-95</em>,&nbsp;<strong>D. Fernando Tortosa Muñoz</strong>;&nbsp;<em>en el curso 1995-1996</em>,&nbsp;<strong>D. Joaquín López-Sidro Jiménez</strong>;&nbsp;<em>desde el curso 1996-97 hasta el curso 2000-2001</em>,&nbsp;<strong>D. Miguel Jiménez Jiménez</strong> y&nbsp;<em>desde el curso 2001-2002</em>,&nbsp;<strong>D. Alfonso Centeno Gómez</strong>. Todos ellos siguen impartiendo sus clases en nuestro instituto.</span></p>
<p><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">En un principio, el IES Miguel de Cervantes funcionó como centro exclusivo de Bachillerato, impartiendo clases de BUP y COU. Posteriormente,&nbsp;<strong>en 1997 se acomodó a la LOGSE</strong>, incorporando alumnado de&nbsp;<strong><em>3º de ESO</em></strong>, para progresivamente ir sustituyendo el viejo plan de estudios con las nuevas enseñanzas.</span></p>
<p class="Estilo7"><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">Más tarde, en el curso 2000-2001 inició los estudios de&nbsp;<em><strong>primer ciclo de la ESO</strong></em>, con el primer curso y, al año siguiente, el segundo año, completando así el espectro completo que diseñaba la todavía vigente ley educativa para el currículo de las enseñanzas secundarias.</span></p>
<p style="text-align: justify;"><span style="font-size: 10pt;"><span style="font-family: 'comic sans ms', sans-serif;">Al mismo tiempo, desde el mismo curso 2000-2001 inicia la docencia de un&nbsp;<strong><em>Ciclo Formativo de grado superior</em></strong> (Animación Sociocultural), de dos años de duración, para alumnos que han cubierto las enseñanz<img style="float: right;" alt="_historia2" class="Estiloimg" src="images/stories/fotos_ies/_historia2.jpg" width="382" height="287" hspace="10" vspace="15" /></span><span style="font-family: 'comic sans ms', sans-serif;">as de Bachillerato, o que superan una prueba específica de ingreso.</span></span></p>
<p class="Estilo7"><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">Esta nueva situación generó el desarrollo del Proyecto Educativo de Centro, la redacción del Reglamento de Organización y Funcionamiento (ROF) y la puesta en marcha del Programa de Diversificación Curricular, con los que el Centro se adaptaba plenamente a la nueva normativa legal, acomodando los currículos a las nuevas necesidades educativas y a las demandas de la sociedad.</span></p>
<p class="Estilo8"><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">Actualmente, con la implantación de un nuevo sistema educativo, la LOCE, se vislumbran nuevos cambios que todavía no se han desarrolldo plenamente, dentro de las competencias propias de la Consejería. Por lo que nos encontramos en un momento de incertidumbre que no tardará en materializarse en los sucesivos cursos académicos.</span></p>
<p class="Estilo8"><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">El Instituto imparte clases a alumnos desde los once años, por lo que las necesidades de organización han tenido que adaptarse a otras responsabilidades que han exigido el cierre de las puertas exteriores, sin olvidar el principio básico que nos motiva en la educación responsable para la libertad.</span></p>
<div style="text-align: -webkit-auto;"><span face="arial" size="3" style="font-family: arial; font-size: small;"><br /></span></div>
</div>
</div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
</div>";s:4:"head";a:10:{s:5:"title";s:73:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Historia del Instituto";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:117:"/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=42&amp;Itemid=194&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:118:"/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=42&amp;Itemid=194&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:6:"INICIO";s:4:"link";s:53:"index.php?option=com_content&view=featured&Itemid=250";}i:1;O:8:"stdClass":2:{s:4:"name";s:22:"Historia del Instituto";s:4:"link";s:71:"index.php?option=com_content&view=category&layout=blog&id=42&Itemid=194";}}s:6:"module";a:0:{}}
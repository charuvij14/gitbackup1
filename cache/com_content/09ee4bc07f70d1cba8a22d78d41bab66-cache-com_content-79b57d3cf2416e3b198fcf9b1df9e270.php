<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:57267:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Técnicas de estudio</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 12 Abril 2015 18:41</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=280:ed-plastica-1-eso&amp;catid=97&amp;Itemid=718&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=782d1d9e93495c73cda88f2c82af2f0fc0565a5c" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1329
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 style="background-color: #e7eaad;"><span style="text-decoration: underline;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt;">EDUCACIÓN PLÁSTICA Y VISUAL.</span></span></h1>
<h3 style="background-color: #e7eaad;"><span style="text-decoration: underline;"><strong><span style="color: #2a2a2a;">1º ESO (12/04/15)</span></strong></span></h3>
<p style="padding-left: 30px;"><a href="archivos_ies/14_15/dibujo/1eso/Portada-1esoNN.pdf" target="_blank"><span style="text-decoration: underline;"><strong><span style="color: #2a2a2a;">PORTADA.</span></strong></span></a></p>
<ol style="text-align: justify; background-color: #e7eaad;">
<li><a href="archivos_ies/14_15/dibujo/1eso/L1_Horizontales_y_verticales-1eso.pdf" target="_blank" title="Lámina 1.">Lámina 1.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L2_Lineas%20oblicuas_y_color-1eso.pdf" target="_blank">Lámina 2.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L3_Tramas_puntos-1eso.pdf" target="_blank">Lámina 3.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L4_Claroscuro1eso.pdf" target="_blank">Lámina 4.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L5_Colores_complementarios-1eso.pdf" target="_blank">Lámina 5.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L6_frios_y_calidos-1eso.pdf" target="_blank">Lámina 6.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L7_Anamorfosis-1eso.pdf" target="_blank">Lámina 7.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L8_La_forma_II-1eso.pdf" target="_blank">Lámina 8.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L9_Escalas-1eso.pdf" target="_blank">Lámina 9.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L10_Doriforo-1eso.pdf" target="_blank">Lámina 10.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L11_Formas_geome_1eso.pdf" target="_blank">Lámina 11.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L12_Poligonillos-1eso.pdf" target="_blank">Lámina 12.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L13_segun_lado-1eso.pdf" target="_blank">Lámina 13.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L14-1eso.pdf" target="_blank">Lámina 14.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L15_Proyeccion_conica_II-1eso.pdf" target="_blank">Lámina 15.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L16_Conica_III-1eso.pdf" target="_blank">Lámina 16.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/Planta_alzado_y_perfil-1eso.pdf" target="_blank">Lámina 17.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L18-1eso.pdf" target="_blank">Lámina 18.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L19-1eso.pdf" target="_blank">Lámina 19.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L20-1eso.pdf" target="_blank">Lámina 20.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L21-1eso.pdf" target="_blank">Lámina 21.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L22-1eso.pdf" target="_blank">Lámina 22.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L23-1eso.pdf" target="_blank">Lámina 23.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L24-1eso.pdf" target="_blank">Lámina 24.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L25-1eso.pdf" target="_blank">Lámina 25.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L26-1eso.pdf" target="_blank">Lámina 26.</a></li>
</ol></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 06 Diciembre 2012 10:46</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=74:zona-del-dibujo-tecnico-1o-bachillerato&amp;catid=97&amp;Itemid=134&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=49d5c40fa6e4a86b1f509237123fe05002978567" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2482
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div><strong> </strong></div>
<ul>
<li><strong><a href="archivos_ies/dep_dibujo/Papel_normalizado_1_Bach..pdf" target="_blank">Papel normalizado 1º Bachillerato.</a></strong></li>
</ul>
<p> </p>
<p> </p>
<p> </p>
<hr /></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
Visitas: 9760
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div style="text-align: center;">
<h3 style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt; text-decoration: underline;">LAMINAS DEL CURSO 2014 - 2015</span></h3>
<h3 style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt; text-decoration: underline;">DIBUJO 4º DE ESO.</span></h3>
</div>
<div style="text-align: center;"> </div>
<ol>
<li><a href="archivos_ies/14_15/dibujo/Unidad_1_Lenguaje_imagen.pdf" target="_blank" title="Tema 1.">TEMA 1.</a></li>
<ul>
<li><a href="archivos_ies/14_15/dibujo/T1_lam1%20.pdf" target="_blank">Lámina 1.</a></li>
<li><a href="archivos_ies/14_15/dibujo/T1_lam2%20.pdf" target="_blank">Lámina 2.</a></li>
<li><a href="archivos_ies/14_15/dibujo/T1_lam3%20.pdf" target="_blank">Lámina 3.</a></li>
<li><a href="archivos_ies/14_15/dibujo/T1_lam4.pdf" target="_blank">Lámina 4.</a></li>
<li><a href="archivos_ies/14_15/dibujo/T1_lam5%20.pdf" target="_blank">Lámina 5.</a></li>
</ul>
<li><a href="archivos_ies/14_15/dibujo/Unidad_2_4_eso.pdf" target="_blank" title="Tema 2.">TEMA 2.</a></li>
<ul>
<li><a href="archivos_ies/14_15/dibujo/lamina1_tema2_4eso.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a></li>
<li style="padding-left: 11px; overflow-y: hidden; background-image: url('templates/verde10/images/postbullets.png'); background-position: 0% 0%; background-repeat: no-repeat;"><a href="archivos_ies/14_15/dibujo/lamina2_tema2_4eso.jpg" target="_blank">Lámina 2.</a></li>
<li style="padding-left: 11px; overflow-y: hidden; background-image: url('templates/verde10/images/postbullets.png'); background-position: 0% 0%; background-repeat: no-repeat;"><a href="archivos_ies/14_15/dibujo/lamina3_tema2_4eso.jpg" target="_blank">Lámina 3.</a></li>
<li style="padding-left: 11px; overflow-y: hidden; background-image: url('templates/verde10/images/postbullets.png'); background-position: 0% 0%; background-repeat: no-repeat;"><a href="archivos_ies/14_15/dibujo/lamina4_tema2_4eso.jpg" target="_blank">Lámina 4.</a></li>
<li style="padding-left: 11px; overflow-y: hidden; background-image: url('templates/verde10/images/postbullets.png'); background-position: 0% 0%; background-repeat: no-repeat;"><a href="archivos_ies/14_15/dibujo/lamina5_tema2_4eso.jpg" target="_blank">Lámina 5.</a></li>
</ul>
<li><a href="archivos_ies/14_15/dibujo/Unidad_3_4eso.pdf" target="_blank">TEMA 3.</a><span style="color: #2a2a2a;"> </span></li>
</ol>
<ul>
<ul>
<li><a href="archivos_ies/14_15/dibujo/lamina1_tema3_4eso.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina2_tema3_4eso.jpg" target="_blank">Lámina 2.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina3_tema3_4eso.jpg" target="_blank">Lámina 3.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina4_tema3_4eso.jpg" target="_blank">Lámina 4.</a></li>
<li style="padding-left: 0px; overflow-y: visible; background: none;"></li>
<li style="padding-left: 0px; overflow-y: visible; background: none;"></li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="color: #414141; background-color: initial;">4. </span><a href="archivos_ies/14_15/dibujo/Tema_4_4eso.pdf" target="_blank" style="background-color: initial;">TEMA 4.</a><span style="color: #2a2a2a;"> </span></p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/dibujo/lamina1_tema4.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina2_tema4.jpg" target="_blank">Lámina 2.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina3_tema4.jpg" target="_blank">Lámina 3.</a></li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="color: #414141; background-color: initial;">5. <a href="archivos_ies/14_15/dibujo/Tema5_4_eso.pdf" target="_blank">TEMA 5.</a> (07/01/15)</span> </p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/dibujo/lam1_Tema5.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a> (27/01/15)</li>
<li><a href="archivos_ies/14_15/dibujo/lam2_Tema5.jpg" target="_blank">Lámina 2.</a> (27/01/15)</li>
<li><a href="archivos_ies/14_15/dibujo/lam3_Tema5.jpg" target="_blank">Lámina 3.</a> (27/01/15)</li>
<li><a href="archivos_ies/14_15/dibujo/lam4_Tema5.jpg" target="_blank" style="text-decoration: none; color: #2f310d;">Lámina 4.</a> (27/01/15)</li>
<li><a href="archivos_ies/14_15/dibujo/lam5_Tema5.jpg" target="_blank">Lámina 5.</a> (27/01/15)</li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="color: #414141; background-color: initial;"> 6. <a href="archivos_ies/14_15/dibujo/Tema_6_EP.pdf" target="_blank">TEMA 6.</a> (03/03/15)</span> </p>
<ul>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/dibujo/T6-lam-einstein.jpg" target="_blank">Lámina 1.</a> (06/04/15)</span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/dibujo/T6-lam2-meninas.JPG" target="_blank">Lámina 2.</a> (06/04/15)</span></li>
</ul>
</ul>
<p style="padding-left: 30px;"> 7. <a href="archivos_ies/14_15/dibujo/Tema7.pdf" target="_blank">TEMA 7. </a>(06(04/15)</p>
<ul>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/dibujo/T7lam1.jpg" target="_blank">Lámina 1.</a> (15/05/15)</span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/dibujo/T7lam2.jpg" target="_blank">Lámina 2.</a> (15/05/15)</span></li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="background-color: initial;"> 8. </span><a href="archivos_ies/14_15/dibujo/Tema8_FH11.pdf" target="_blank" style="background-color: initial;">TEMA 8.</a> <span style="background-color: initial;">(15(05/15)</span> </p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/dibujo/T8_lam1-4eso.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a> <span style="color: #2a2a2a;">(19/05/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T8_lam2-4eso.jpg" target="_blank">Lámina 2.</a> <span style="color: #2a2a2a;">(19/05/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T8_lam3-4eso.jpg" target="_blank">Lámina 3.</a> <span style="color: #2a2a2a;">(19/05/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T8_lam4-4eso.jpg" target="_blank">Lámina 4.</a> <span style="color: #2a2a2a;">(19/05/15)</span></li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="background-color: initial;"> 9. <a href="archivos_ies/14_15/dibujo/Tema_9.pdf" target="_blank">TEMA 9.</a> (19/05/15)</span></p>
<div style="text-align: center;"><hr /></div>
<div style="text-align: center;"> </div>
<div style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt; text-decoration: underline;">LAMINAS DEL CURSO 2012 - 2013</span></div>
<div style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt; text-decoration: underline;">DIBUJO 4º DE ESO.</span></div>
<div style="text-align: center;"> </div>
<div style="border: 0pt; text-align: center;">
<table align="center">
<tbody>
<tr>
<td style="text-align: center;" align="CENTER" valign="MIDDLE">
<ul>
<li style="text-align: justify;"><span style="font-family: arial, helvetica, sans-serif; font-size: small;"><a href="archivos_ies/dep_dibujo/Lamina_1_4_ESO.pdf" target="_blank">Lámina 1.</a>   Construcciones fundamentales. Operaciones con segmentos.</span></li>
<li style="text-align: justify;"><span style="font-family: arial, helvetica, sans-serif; font-size: small;"><a href="archivos_ies/dep_dibujo/Lamina_2_4_ESO.pdf" target="_blank">Lámina 2.</a>   Construcciones fundamentales. Paralelismo.</span></li>
<li style="text-align: justify;"><span style="font-family: arial, helvetica, sans-serif; font-size: small;"><a href="archivos_ies/dep_dibujo/Lamina_3_4_ESO.pdf" target="_blank">Lámina 3.</a>   Construcciones fundamentales. Perpendicularidad.</span></li>
<li style="text-align: justify;"><span style="font-family: arial, helvetica, sans-serif; font-size: small;"><a href="archivos_ies/dep_dibujo/Lamina_4_4_ESO.pdf" target="_blank">Lámina 4.</a>   Construcciones fundamentales. Estudio de la circunferencia.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_5_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 5.</a><span style="font-size: small;">   Construcciones fundamentales. Ángulos.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_6_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 6.</a><span style="font-size: small;">   Construcciones fundamentales. Construcción de ángulos.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_7_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 7.</a><span style="font-size: small;">   </span><span style="font-size: small;">Construcciones fundamentales. Aplicación. Segmentos y ángulos 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_8_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 8.</a><span style="font-size: small;">   </span><span style="font-size: small;">Construcciones fundamentales. Aplicación. Segmentos y ángulos 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_9_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 9.</a><span style="font-size: small;">   Triángulos 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_10_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 10.</a><span style="font-size: small;"> Triángulos 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_11_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 11.</a><span style="font-size: small;"> Triángulos 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_12_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 12.</a><span style="font-size: small;"> Triángulos 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_13_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 13.</a><span style="font-size: small;"> Triángulos 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_14_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 14.</a><span style="font-size: small;"> Triángulos 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_15_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 15.</a><span style="font-size: small;"> Aplicación de triángulos 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_16_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 16.</a><span style="font-size: small;"> Aplicación de triángulos 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_17_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 17.</a><span style="font-size: small;"> Aplicación de triángulos 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_18_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 18.</a><span style="font-size: small;"> Aplicación de triángulos 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_19_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 19.</a><span style="font-size: small;"> Cuadriláteros 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_20_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 20.</a><span style="font-size: small;"> Cuadriláteros 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_21_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 21.</a><span style="font-size: small;"> Aplicación de cuadriláteros 1</span><a href="archivos_ies/dep_dibujo/Lamina_21_4_ESO.pdf" target="_blank" style="font-size: small;">.</a></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_22_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 22.</a><span style="font-size: small;"> Aplicación de cuadriláteros 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_23_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 23.</a><span style="font-size: small;"> Polígonos Regulares.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_24_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 24.</a><span style="font-size: small;"> Polígonos Regulares 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_25_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 25.</a><span style="font-size: small;"> Polígonos Regulares 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_26_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 26.</a><span style="font-size: small;"> Polígonos Regulares 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_27_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 27.</a><span style="font-size: small;"> Aplicación de polígonos regulares 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_28_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 28.</a><span style="font-size: small;"> Aplicación de polígonos regulares 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_29_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 29.</a><span style="font-size: small;"> Tangencias 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_30_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 30.</a><span style="font-size: small;"> Tangencias 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_31_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 31.</a><span style="font-size: small;"> Tangencias 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_32_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 32.</a><span style="font-size: small;"> Tangencias 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_33_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 33.</a><span style="font-size: small;"> Tangencias 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_34_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 34.</a><span style="font-size: small;"> Tangencias 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_35_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 35.</a><span style="font-size: small;"> Tangencias 7.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_36_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 36.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_37_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 37.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_38_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 38.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_39_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 39.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_40_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 40.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_41_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 41.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_42_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 42.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 7.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_43_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 43.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 8.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_44_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 44.</a><span style="color: #655c4e; font-size: small;"> Curvas cónicas. Elipse.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_45_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 45.</a><span style="color: #655c4e; font-size: small;"> Curvas cónicas. Parábola.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_46_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 46.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Curvas cónicas. Hipérbola.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_47_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 47.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">  Colores fríos. Aplicación 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_48_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 48.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_49_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 49.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_50_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 50.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_51_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 51.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 4</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_52_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 52.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_53b_4_ESO.pdf" style="font-family: inherit; font-size: inherit;">Lámina 53.</a><span style="font-family: inherit; font-size: inherit;">   Vistas normalizadas 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_54_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 54.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 7.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_55_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 55.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 8.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_56_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 56</a><span style="font-family: inherit; font-size: small; color: #655c4e;">.   Vistas normalizadas 9.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_57_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 57.</a><span style="font-size: small;">   </span><span style="font-size: small;">Vistas normalizadas 10.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_58_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 58.</a><span style="font-size: small;">   </span><span style="font-size: small;">Vistas normalizadas 11.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_59_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 59.</a><span style="font-size: small;">   Isométrica 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_60_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 60.</a>  <span style="font-size: small;"> Isométrica 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_61_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 61.</a>  <span style="font-size: small;"> Isométrica 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_62_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 62.</a>  <span style="font-size: small;"> Isométrica 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_63_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 63.</a>  <span style="font-size: small;"> Isométrica 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_64_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 64.</a>  <span style="font-size: small;"> Isométrica 5b.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_65_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 65.</a>  <span style="font-size: small;"> Isométrica 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_66_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 66.</a>  <span style="font-size: small;"> Isométrica 7.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_67_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 67.</a><span style="font-size: small;">   Normalización I.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_68_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 68.</a>  <span style="font-size: small;"> Normalización II.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_69_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 69.</a>  <span style="font-size: small;"> Normalización III.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_70_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 70.</a>  <span style="font-size: small;"> Normalización IV.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_71_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 71.</a><span style="font-size: small;">   Normalización V</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_72_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 72.</a><span style="font-size: small;">   Normalización VI.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_73_4_ESOerrores.pdf" target="_blank" style="font-size: small;">Lámina 73.</a><span style="font-size: small;">   Normalización VII.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_74_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 74.</a><span style="font-size: small;">   Normalización VIII.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_75_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 75.</a><span style="font-size: small;">   Normalización IX.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_76_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 76.</a><span style="font-size: small;">   Normalización X.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_77_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 77.</a><span style="font-size: small;">   Croquización I.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_78_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 78.</a><span style="font-size: small;">   Croquización II.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_79_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 79.</a><span style="font-size: small;">   Gradaciones I.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_80_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 80.</a><span style="font-size: small;">   Gradaciones II.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_81_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 81.</a><span style="font-size: small;">   Gradaciones III</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_82_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 82.</a><span style="font-size: small;">   Gradaciones IV</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_83_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 83.</a><span style="font-size: small;">   Dibujo científico I.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_84_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 84.</a><span style="font-size: small;">   Dibujo científico II.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_85_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 85.</a><span style="font-size: small;">   Dibujo científico III.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_86_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 86.</a><span style="font-size: small;">   Dibujo científico IV.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_87_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 87.</a><span style="font-size: small;">   Dibujo científico V.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_88_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 88.</a><span style="font-size: small;">   Dibujo científico VI.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_89_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 89.</a><span style="font-size: small;">   Perspectiva cónica 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_90_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 90.</a><span style="font-size: small;">   Perspectiva cónica 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_91_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 91.</a><span style="font-size: small;">   Perspectiva cónica 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_92_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 92.</a><span style="font-size: small;">   Perspectiva cónica 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_93_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 93.</a><span style="font-size: small;">   Perspectiva cónica 5.</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<p style="text-align: left;"> </p>
<div><span style="font-size: x-small;"> </span></div>
</div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 22 Septiembre 2015 18:22</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=77:do-emilia-vilchez-campillos&amp;catid=97&amp;Itemid=563&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=ff07f6e9aac00cc8e91b425e56e416a548ad4a3e" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 7985
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1><span style="text-decoration: underline;"><span style="font-family: arial black,avant garde; font-size: 14pt;"><strong>EDUCACIÓN PLÁSTICA Y VISUAL.</strong></span></span></h1>
<h3><span style="text-decoration: underline;"><strong><span style="color: #2a2a2a; text-decoration: underline;">2º ESO</span></strong> </span></h3>
<ol>
<li><a href="archivos_ies/14_15/dibujo/UD1OIE2.pdf" target="_blank" title="Tema 1.">Tema 1.<br /></a></li>
<li><a href="archivos_ies/14_15/dibujo/UD2OIE2.pdf" target="_blank" title="Tema 2.">Tema 2.</a><span style="color: #2a2a2a;"> </span></li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T2_lam1_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T2_lam2_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;"><span style="color: #414141;">Lámina 2.</span></span></a></li>
</ol>
<li><a href="archivos_ies/14_15/dibujo/Uni_3.pdf" title="Tema 3."><span style="color: #2f310d;">Tema </span>3.</a></li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T3_lam1_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T3_lam2_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;"><span style="color: #414141;">Lámina 2.</span></span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T3_lam3_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 3.</span></a></li>
</ol>
<li><a href="archivos_ies/14_15/dibujo/Unidad_4_2_eso.pdf" target="_blank">Tema 4.</a><span style="color: #414141;"><br /></span></li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T4_lam1_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T4_lam2_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 2.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T4_lam3_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 3.</span></a></li>
</ol>
<li><a href="archivos_ies/14_15/dibujo/Tema5_2eso.pdf" target="_blank">Tema 5.</a><span><br /></span></li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T5_lam1_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T5_lam2_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 2.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T5_lam3_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 3.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T5_lam4_2eso.jpg" target="_blank"><span style="color: #414141;"><span style="color: #414141;"><span style="color: #2f310d;">Lámina </span>4.</span></span></a></li>
</ol>
<li><a href="archivos_ies/14_15/dibujo/tema-6-2-eso.pdf" target="_blank">Tema 6.</a> (07/01/15)</li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T6-lm1.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T6lam2-2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 2.</span></a></li>
</ol>
<li><a href="archivos_ies/15_16/dibujo/Tema7nuevo.pdf" target="_blank">Tema 7.</a> (22/09/15)</li>
</ol><ol><ol>
<li><span style="color: #414141;"><a href="archivos_ies/14_15/dibujo/T7-lm1.jpg" target="_blank" style="text-decoration: none; color: #2f310d;">Lámina 1.</a> (23/02/15)</span></li>
<li><span style="color: #414141;"><a href="archivos_ies/14_15/dibujo/T7lam2-2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;">Lámina 2.</a></span> (23/02/15)</li>
<li><span style="color: #414141;"><a href="archivos_ies/14_15/dibujo/T7Lam3-3eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;">Lámina 3.</a></span> (23/02/15)</li>
</ol></ol>
<p style="padding-left: 30px;"><span style="color: #414141;">8. <a href="archivos_ies/14_15/dibujo/T8.pdf" target="_blank">Tema 8.</a> (06/04/15)</span></p>
<ol><ol>
<li><span style="background-color: initial;"><a href="archivos_ies/14_15/dibujo/T8_lam1_2eso.jpg" target="_blank">Lámina 1.</a>  (14/04/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T8_lam2_2eso.jpg" target="_blank">Lámina 2.</a> (14/04/15)</li>
</ol></ol>
<p style="padding-left: 30px;"><span style="color: #414141;">9. <a href="archivos_ies/14_15/dibujo/T9_EP_2_ESO.pdf" target="_blank">Tema 9.</a> (03/05/15)</span></p>
<ol>
<li><span style="background-color: initial;"><a href="archivos_ies/14_15/dibujo/T9_lam1_2eso.jpg" target="_blank">Lámina 1.</a>  (03/05/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T9-lam2-2eso.jpg" target="_blank">Lámina 2.</a> (03/05/15)</li>
<li><a href="archivos_ies/14_15/dibujo/T9_lam3_2eso.jpg" target="_blank">Lámina 3.</a> (03/05/15)</li>
</ol><hr />
<ul>
<li><span style="color: #2a2a2a;">3º ESO</span></li>
<li><span style="color: #2a2a2a;">RECUPERACIÓN DE PENDIENTES</span></li>
</ul></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                            <div class="item column-2">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 28 Octubre 2013 10:57</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=78:educacion-plastica&amp;catid=97&amp;Itemid=143&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=a6ef8601b1d78c9accdffab646b6245e103d88d3" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 4902
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h3 style="text-align: center;"><span style="text-decoration: underline;"><strong>DIBUJO TÉCNICO 2013-14.</strong></span></h3>
<ul style="text-align: justify;">
<li><span style="color: #2a2a2a;">Resumen de las programaciones del Departamento</span><span style="color: #2a2a2a;">.</span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/13_14/programaciones/Programacion_2Bach_13-14.pdf" target="_blank">Programación de Dibujo Técnico 2º Bachillerato.</a> (28/10/13)</span></li>
</ul>
<ul>
<li><a href="archivos_ies/13_14/programaciones/Directrices_orientaciones_%20DTII_13_14.pdf" target="_blank">Directrices y orientaciones de Dibujo Técnico II para 2º de Bachillerato.</a> (19/10/13)</li>
</ul>
<h6><span style="text-decoration: underline;"><span style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff; text-decoration: underline;">Sistema Diédrico Aplicación de perpendicularidad recta-plano. (19/10/13)</span></span></h6>
<ul>
<li><a href="archivos_ies/13_14/dibujo/Aplicacion_de_perepndicularidad_1.pdf" target="_blank"><strong style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;">Aplicación de perepndicularidad 1</strong></a></li>
<li><a href="archivos_ies/13_14/dibujo/Aplicacion_de_perepndicularidad_2.pdf" target="_blank"><strong style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;">Aplicación de perepndicularidad 2</strong></a></li>
<li><a href="archivos_ies/13_14/dibujo/Aplicacion_de_perepndicularidad_3.pdf" target="_blank"><strong style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;">Aplicación de perepndicularidad 3</strong></a></li>
</ul>
<p><strong style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"> </strong></p>
<p><span style="text-decoration: underline; font-family: 'arial black', 'avant garde'; font-size: 12pt;"><strong>Equivalencia de formas planas 2º Bachillerato:</strong></span></p>
<ul>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><strong><a href="archivos_ies/Equivalencias.pdf" target="_blank">Equivalencias</a><br /> </strong></span></li>
<li><a class="at_url" href="archivos_ies/062_Equivalencia%20I%20C.pdf" target="_blank" title="Download this file (Equivalencia I C.pdf)" style="color: #c6c37b; font-weight: bold;">Equivalencia I C.pdf</a></li>
<li><a class="at_url" href="archivos_ies/062_Equivalencia%20II%20C.pdf" target="_blank" title="Download this file (Equivalencia II C.pdf)" style="text-decoration: none; font-weight: bold;">Equivalencia II C.pdf</a></li>
<li><a class="at_url" href="archivos_ies/062_Equivalencia%20III%20C.pdf" target="_blank" title="Download this file (Equivalencia III C.pdf)" style="text-decoration: none; font-weight: bold;">Equivalencia III C.pdf</a></li>
<li><a class="at_url" href="archivos_ies/062_Equivalencia%20III%20Sol.pdf" target="_blank" title="Download this file (Equivalencia III Sol.pdf)" style="text-decoration: none; font-weight: bold;">Equivalencia III Sol.pdf</a></li>
</ul>
<p><span style="text-decoration: underline; font-family: 'arial black', 'avant garde'; font-size: 12pt;"><strong>Ejercicios resueltos:</strong></span></p>
<ul>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><a href="archivos_ies/dep_dibujo/ejercicios_resueltos_de_triangulos.pdf" target="_blank">De triángulos.</a></span></li>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><a href="archivos_ies/dep_dibujo/ejercicios_resueltos_de_cuadrilateros.pdf" target="_blank">De cuadriláteros.</a></span></li>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><a href="archivos_ies/dep_dibujo/PAU_2010_Ejercicio_resuelto_Enunciado.pdf" target="_blank">Ejercicio de Selectividad 2010</a>, <a href="archivos_ies/dep_dibujo/PAU_2010_Ejercicio_resuelto_Solucion.pdf" target="_blank">resuelto y comentado.</a><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"> (añadido el 24/04/12)</span><a href="archivos_ies/dep_dibujo/ejercicios_resueltos_de_cuadrilateros.pdf" target="_blank"><br /></a></span></li>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><a href="archivos_ies/dep_dibujo/examen_sistema_diedrico_opIyII_enun_y_soluciones_2011_12.zip" target="_blank">EXAMEN SISTEMA DIÉDRICO. CURSO 2011/12. Opción I y II. Enunciado y soluciones.2º Bachillerato.</a> </span></span><span style="font-family: arial, helvetica, sans-serif; font-size: 13px;">(añadido el 24/04/12) (Botón derecho &gt; Guardar destino como... ) ES UN ZIP.</span></li>
</ul></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
                            <div class="items-row cols-2 row-1">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Departamento de Educación Plástica.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 02 Octubre 2014 18:20</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=76:departamento-de-dibujo&amp;catid=97&amp;Itemid=98&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=3a0819fc42dfb424f6fcb548bb109ebd8854e7b1" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 7088
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><ul style="color: #151509; margin: 1em 0px 1em 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"><span style="font-family: 'andale mono', times;"><span style="font-family: 'andale mono', times;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=77:do-emilia-vilchez-campillos&amp;catid=97:educacion-plastica&amp;Itemid=563">ENLACE A EDUCACIÓN PLÁSTICA Y VISUAL DE 2º ESO.<br /><br /></a><strong style="color: #414141; font-family: 'andale mono', times; letter-spacing: normal; text-align: left;"><a href="index.php?option=com_content&amp;view=article&amp;id=75:do-emilia-vilchez-campillos&amp;catid=75:educacion-plastica&amp;Itemid=563" style="text-decoration: none; color: #2f310d;">ENLACE A EDUCACIÓN PLÁSTICA Y VISUAL DE 4º ESO.</a></strong></strong></span></span></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"><hr /></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"></li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;">
<h3 style="line-height: normal; background-color: #e7eaad;"><span style="text-decoration: underline;"><strong>EDUCACIÓN PLÁSTICA CURSO 2013/14:</strong></span></h3>
<ul style="color: #2a2a2a; line-height: normal; text-align: justify; background-color: #e7eaad;">
<li><a href="archivos_ies/13_14/programaciones/Programacion_reducida_E_Plastica_2013-14.pdf" target="_blank" title="Criterios de evaluación de Educación Plásitica.">Criterios de evaluación de Educación Plástica curso 2013/14.</a><span style="line-height: 1.25em;"> (02/10/13)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/13_14/programaciones/EPV_Progamacion13-14.pdf" target="_blank">Programación</a> (29/10/13)</span></li>
<li><span style="color: #2a2a2a;">Resumen de las programaciones del Departamento</span><span style="color: #2a2a2a;">.</span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/13_14/programaciones/DT_Programacion_1Bach_13-14.pdf" target="_blank">Programación de Dibujo Técnico 1º Bachillerato.</a> (31/10/13)</span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/13_14/programaciones/Programacion_2Bach_13-14.pdf" target="_blank">Programación de Dibujo Técnico 2º Bachillerato.</a> (28/10/13)</span></li>
<li><a href="archivos_ies/13_14/programaciones/Directrices_orientaciones_%20DTII_13_14.pdf" target="_blank">Directrices y orientaciones de Dibujo Técnico II para 2º de Bachillerato.</a> (19/10/13)</li>
</ul>
</li>
</ul>
<p> </p>
<hr />
<ul style="color: #151509; margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; list-style-type: none; padding: 0px;">
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"><a href="archivos_ies/13_14/programaciones/Programacion_reducida_E_Plastica_2013-14.pdf" target="_blank" title="Criterios de evaluación de Educación Plásitica." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;"><br />Criterios de evaluación de Educación Plástica curso 2013/14.</a> (02/10/13)</li>
<li style="margin-top: 0.2em; margin-right: 0px; margin-bottom: 0.2em; margin-left: 0px; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 18px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat no-repeat;"><a href="archivos_ies/criterios_evaluacion/DibujoT_1.pdf" target="_blank" title="Criterios de evaluación de Dibujo Técnico 1º Bachillerato." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Dibujo Técnico de 1º Bachillerato.</a></li>
</ul>
<hr />
<ul>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=74:zona-del-dibujo-tecnico-1o-bachillerato&amp;catid=97:educacion-plastica">Entrada en la zona de Dibujo Técnico 1º Bachillerato.</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=78">Entrada en la zona de Dibujo Técnico 2º Bachillerato.</a></li>
<li></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=75:4o-eso-dibujo-tecnico&amp;catid=97:educacion-plastica">ZONA DE 4º ESO. DIBUJO TÉCNICO.</a></li>
</ul>
<p> </p>
<hr />
<p>Enlace con páginas del profesorado del Departamento de Dibujo</p>
<ul>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=77:do-emilia-vilchez-campillos&amp;catid=97:educacion-plastica&amp;Itemid=134">Emilia Vílchez Campillos.</a></li>
<li><a href="http://nuriaplastica.blogspot.com/" target="_blank">Blog de Nuria Manrique sobre Educación Plástica Bilingüe</a></li>
</ul></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">Educación Plástica</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
            </div>";s:4:"head";a:10:{s:5:"title";s:71:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Técnicas de estudio";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:101:"/index.php?option=com_content&amp;view=category&amp;id=97&amp;Itemid=134&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:102:"/index.php?option=com_content&amp;view=category&amp;id=97&amp;Itemid=134&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:5:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:21:"Dpto. de Orientación";s:4:"link";s:59:"index.php?option=com_content&view=article&id=104&Itemid=574";}i:2;O:8:"stdClass":2:{s:4:"name";s:20:"Técnicas de estudio";s:4:"link";s:59:"index.php?option=com_content&view=article&id=140&Itemid=134";}i:3;O:8:"stdClass":2:{s:4:"name";s:13:"Departamentos";s:4:"link";s:60:"index.php?option=com_content&view=category&id=195&Itemid=134";}i:4;O:8:"stdClass":2:{s:4:"name";s:20:"Educación Plástica";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
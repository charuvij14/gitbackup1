<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:3802:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Matrícula y anulación</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:10</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=206:matricula-y-anulacion&amp;catid=242&amp;Itemid=613&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=11907c66a33154a2a8fe60a9ae0b020c6783d693" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2798
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Matrícula y anulación de matrícula</h2>
<p class="text-info"><strong><span style="color: #003366;">Plazo de Matrícula</span></strong></p>
<div id="articlepxfontsize1">
<ul>
<li>Del 1 al 10 de Julio</li>
<li>Del 1 al 8 de Septiembre</li>
</ul>
<p class="text-alert"><span style="color: #800000;"><strong>Anulación de Matrícula</strong></span></p>
<ul class="list-arrow arrow-blue">
<li><span style="text-decoration: underline;"><strong>En E.S.O. (con 16 años cumplidos) y BACHILLER:</strong></span></li>
</ul>
<div style="text-align: justify;">La dirección de los centros docentes, a petición razonada del alumnado o, si es menor de edad, de su padre, madre o tutor legal, antes del:</div>
<ul>
<li>30 de abril de cada curso</li>
</ul>
<div style="text-align: justify;">y cuando las causas alegadas imposibiliten la asistencia del alumno o alumna a clase, podrá dejar sin efectos su matrícula en E.S.O., Bachillerato o Educación Secundaria para Adultos. En este caso, la matrícula no será computada a los efectos del número máximo de años de permanencia en la enseñanza.<br /><br /></div>
<ul class="list-arrow arrow-blue">
<li><span style="text-decoration: underline;"><strong>En CICLOS FORMATIVOS:</strong></span></li>
</ul>
<div style="text-align: justify;">La anulación de la matrícula será del curso completo y se podrá llevar a cabo hasta dos meses antes de la celebración de la Evaluación Ordinaria del curso correspondiente, o lo que es lo mismo:</div>
<ul class="list-icon icon-calendar">
<li>Primer Curso: hasta el 31 de Marzo</li>
<li>Segundo Curso (Grado Superior): hasta el 31 de Enero</li>
</ul>
<div style="text-align: justify;">Para anular la matrícula es necesario cumplimentar el impreso de solicitud y presentarlo en la Secretaría del Centro.</div>
<ul class="list-icon icon-article">
<li>Descargar <span style="text-decoration: underline;"><strong>impreso</strong></span> de Anulación de Matrícula</li>
</ul>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:74:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Matrícula y anulación";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:15:"Escolarización";s:4:"link";s:59:"index.php?option=com_content&view=article&id=210&Itemid=611";}i:1;O:8:"stdClass":2:{s:4:"name";s:23:"Matrícula y anulación";s:4:"link";s:59:"index.php?option=com_content&view=article&id=206&Itemid=613";}}s:6:"module";a:0:{}}
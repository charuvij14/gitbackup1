<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:68258:"<div class="blog"><div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Selectividad 2015</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 30 Junio 2015 16:15</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=286:selectividad-2015&amp;catid=104&amp;Itemid=709&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=b16157f0e1e7acac578267e1337874a21232ee8c" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 11310
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE 2015<br /><br /></span></h1>
<ul style="text-align: justify;">
<li><strong style="color: #2a2a2a;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/15  (MIÉRCOLES) POR LA TARDE EN <a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a;"> </span><strong style="color: #2a2a2a;">EXÁMENES QUE HAN SALIDO EN JUNIO 2015 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<p style="text-align: justify;"><span style="text-decoration: underline;"><strong>JUNIO 2015 - EXÁMENES</strong></span></p>
<p style="text-align: justify;"><strong>16/06/15. </strong>Martes. primer día: sin incidencias, todos a su hora con los nervios habituales.</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II</li>
<li>HISTORIA DE ESPAÑA</li>
<li>HISTORIA DE LA FILOSOFÍA</li>
<li>IDIOMA EXTRANJERO</li>
</ul>
<p style="text-align: justify;"><strong>17/06/15.</strong> Miércoles. Una incidencia. Una alumna no se presenta al examen, nervios, llamada al instituto, había decidido no presentarse ¡pero se matriculó! hubiera necesitado saberlo con antelación. Nada más destacable.</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>HISTORIA DEL ARTE</li>
<li>MATEMÁTICAS II <span style="line-height: 16.6399993896484px;">Y COLISIÓN</span></li>
<li>TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS</li>
<li>QUÍMICA</li>
<li>ELECTROTÉCNIA</li>
<li>LITERATURA UNIVERSAL</li>
<li>LENGUAJE Y PRÁCTICA MUSICAL</li>
<li>TECNOLOGÍA INDUSTRIAL II</li>
<li>MATEMÁT. APLIC. A LAS CC. SOCIALES II Y COLISIÓN</li>
</ul>
<p style="text-align: justify;"><strong>18/06/15. </strong>Jueves. Dos alumnos que no se presentan, uno que llega tarde por la tarde, cosas del bus que lo ha perdido, ¡pero es que no hay taxis!</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>ANÁLISIS MUSICAL II (¡YA!)</li>
<li>DISEÑO <span style="line-height: 16.6399993896484px;">(¡YA!)</span></li>
<li>GEOGRAFÍA</li>
<li>BIOLOGÍA</li>
<li>DIBUJO TÉCNICO II</li>
<li>CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES</li>
<li>ECONOMÍA DE LA EMPRESA</li>
<li>GRIEGO II</li>
<li>HISTORIA DE LA MÚSICA Y LA DANZA</li>
<li>DIBUJO ARTÍSTICO II</li>
<li>FÍSICA</li>
<li>LATÍN II</li>
</ul>
<p style="text-align: justify;">Los criterios de corrección ya <a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_criterios_correccion.php" target="_blank" title="Criterios de corrección oficiales.">han sido publicados oficialmente</a> y añadidos en cada asignatura abajo.</p>
<p style="text-align: justify;">Las soluciones: más adelante. Ya están las de Matemáticas II.</p>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Biología</strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br />Ciencias de la Tierra y Medioamb.</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Comentario de Texto, Lengua Castellana y Literatura</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Artístico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Técnico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Economía <br />de la Empresa</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Diseño</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Electrotecnia</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Biologia_2014_15.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></span></strong></a><strong style="font-size: inherit;">Colisión</strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Biologiia.pdf" target="_blank">Crit. Correc.</a><br /><a href="archivos_ies/14_15/selectividad/criterios/Biologiia_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/CTM_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Ciencias%20De%20La%20Tierra.pdf" target="_blank">Crit. Correc.<br /></a><br /><a href="archivos_ies/14_15/selectividad/criterios/Ciencias%20De%20La%20Tierra_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong><br /> <a href="archivos_ies/14_15/selectividad/Comentario_lengua_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Comentario%20Texto%20Lengua%20Castellana%20Y%20Literatura.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> <a href="archivos_ies/14_15/selectividad/D_Artistico_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Colisión</span></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Dibujo%20Artiistico.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/D_Tecnico_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/DIBUJO_PAU_junio_2015_c.pdf" target="_blank">Soluciones</a><br />(Prof.  Manuel Martínez Vela<br />IES P. MANJÓN)<br /><a href="archivos_ies/14_15/selectividad/criterios/Dibujo%20Teecnico.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Economia_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Economiia%20De%20La%20Empresa.pdf" target="_blank">Crit. Correc.<br /></a><br /><a href="archivos_ies/14_15/selectividad/criterios/Economiia%20De%20La%20Empresa_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Dise%C3%B1o_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Diseno.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Disenno.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td align="center" bgcolor="#abFDe8">
<p style="text-align: center;"><a href="archivos_ies/14_15/selectividad/Electrotecnia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Colisión</strong></p>
<p style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Electrotecnia.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Crit. Correc.<br /><br /></a><a href="archivos_ies/14_15/selectividad/criterios/Electrotecnia_incom.pdf" target="_blank" style="font-size: inherit; text-decoration: none; color: #2f310d;">Crit. Incomp.</a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Dise%C3%B1o.pdf" target="_blank"><br /></a>Física</strong></td>
<td style="text-align: center;" align="center"><strong><br />Geografía</strong></td>
<td style="text-align: center;" align="center"><strong><br />Griego II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de España</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de la Filosofía</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia <br />del Arte</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong>Latín II </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong>Alemán</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Fisica_2014_15d.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-decoration: none; color: inherit; font-size: inherit; font-weight: normal; font-family: inherit; text-align: left;">Soluciones<a href="archivos_ies/13_14/selectividad/FISICA_2014_Junio.pdf" target="_blank"><br /></a></strong><a href="archivos_ies/14_15/selectividad/criterios/Fiisica.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Fiisica_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Geografia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="37" height="37" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Geografiia.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Geografiia_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> <a href="archivos_ies/14_15/selectividad/Griego_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Griego.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Griego_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong> <a href="archivos_ies/14_15/selectividad/Historia_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20Espanna.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><a href="archivos_ies/14_15/selectividad/Comentario_filosofia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20La%20Filosofiia.pdf" target="_blank">Crit. Correc.<strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong> </strong><a href="archivos_ies/14_15/selectividad/Historia_Arte_2014_15.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20Del%20Arte.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> </strong><a href="archivos_ies/14_15/selectividad/Latin_junio_e_incompatibilidades_2014_15.pdf"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Latiin.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Latiin_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td align="center">
<p><strong style="text-align: center;"><span style="color: #2f310d;"><span style="margin-right: auto; margin-left: auto;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; margin-right: auto; margin-left: auto; display: block;" /></span></span></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"></a><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Alemaan.pdf" target="_blank">Crit. Correc.</a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Francés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Inglés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Literatura Universal</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas Aplicadas a las Ciencias Sociales II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Matemáticas II</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong><br /> Química </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong> </strong><strong>Técnicas de Exp. Gráf. Plást. (no disponible)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Tecnología Industrial II</strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Frances_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Francees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Ingles.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Inglees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Lit_Universal_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Literatura%20Universal.pdf" target="_blank">Crit. Correc.<br /><br /></a><a href="archivos_ies/14_15/selectividad/criterios/Literatura%20Universal_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><a href="archivos_ies/14_15/selectividad/Mat_CCSSII_2014_15.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Soluciones_MCCSSII_Junio.pdf" target="_blank">Soluciones<br /></a>(emestrda.net)</strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Criterios_cor_Matemaaticas%20Aplicadas.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a><br /></strong></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">_________</strong></p>
<strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/Mat_CCSSII_COLISIION_2014_15.pdf" target="_blank">Colisión<br /></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Criterios_cor_Matemaaticas%20Aplicadas_incom.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Incomp.</a></strong><br />_________<br /><br /></strong></td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/selectividad/Mat_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a>NUEVO: <br /></span></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/15_mod_soljun.pdf" target="_blank">Soluciones<br /><strong style="color: #000000; font-family: Arial; font-size: medium; letter-spacing: normal; text-align: -webkit-center;"><span style="font-size: 8;">(D. Germán Jesús Rubio Luna</span></strong></a>)<br /><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Matemaaticas.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a></strong></strong><br />_________<br /><a href="archivos_ies/14_15/selectividad/Mat_II_COLISION_2014_15b.pdf" target="_blank">Colisión<br /></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Matemaaticas_incom.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Incomp.</a></strong><br />_________</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Quimica_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/QUIMICA_JUNIO_2015.pdf" target="_blank">Soluciones<br /></a></strong>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">(emestrda.net)<br /><br /></strong><a href="archivos_ies/14_15/selectividad/criterios/Quiimica.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a></strong></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong style="text-align: left;"><span style="color: #2f310d;"><br /><a href="archivos_ies/14_15/selectividad/Tecn_Expr_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></span></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Teecnicas%20De%20Expresioon.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><span style="color: #2f310d;"><span style="font-weight: normal;">Crit. Correc.</span></span></strong></strong><strong><br /></strong></a></p>
</td>
<td align="center" bgcolor="#abF856">
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Tec_Industrial_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; margin-right: auto; margin-left: auto; display: block;" /></a></span></p>
<p><span style="color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Tecnologiia%20Industrial.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /><br /></a></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Tecnologiia%20Industrial_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a></span><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong>HISTORIA DE LA<br /></strong>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">MÚSICA Y LA DANZA</strong><strong> </strong><strong style="color: inherit; font-family: inherit; font-size: inherit; background-color: #fafbf0;"> </strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br /></strong><strong>LENGUAJE Y PRÁCTICA<br />MUSICAL</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><br /><strong>ANÁLISIS<br />MUSICAL II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br /></strong><strong>Lengua Extranjera II (Italiano)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Portugués)</strong><strong> </strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"> </td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"> </td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><strong><a href="archivos_ies/14_15/selectividad/Historia_musica_danza_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20La%20Muusica%20Y%20Danza.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong style="color: inherit; font-family: inherit;"> <a href="archivos_ies/14_15/selectividad/Lenguaje_y_pr_musical_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #abF856;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Lenguaje%20Y%20Praactica%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p> <strong style="color: inherit; font-family: inherit; font-size: inherit; background-color: #abf856;"><a href="archivos_ies/14_15/selectividad/analisis_musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #abF856;" /></a></strong><span style="color: inherit; font-family: inherit; font-size: inherit;"> </span></p>
<p> <a href="archivos_ies/14_15/selectividad/criterios/Anaalisis%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"><br /><br /><br /><br /> <a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Italiano.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"><br /><br /><br /><br /> <a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Portuguees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"> </td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"> </td>
<td align="center" bgcolor="#ffffff"> </td>
</tr>
</tbody>
</table>
<p style="text-align: justify;"> (<a href="index.php?option=com_content&amp;view=article&amp;id=243:selectividad-2014&amp;catid=108:bachillerato&amp;Itemid=147&amp;highlight=WyJzZWxlY3RpdmlkYWQiLDIwMTQsInNlbGVjdGl2aWRhZCAyMDE0Il0=" target="_blank" title="Selectividad 2014.">Exámenes de Selectividad 2014 junio</a> y <a href="index.php?option=com_content&amp;view=article&amp;id=262kYWQiXQ==" target="_blank">septiembre</a>)</p>
<ul style="text-align: justify;">
<li>
<h3 style="margin: 1px;"><span style="text-align: center; color: #2a2a2a;">22 DE ABRIL A 4 DE JUNIO: REGISTRO PARA LAS PAU.</span></h3>
</li>
<li><span style="color: #7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;">LA MATRÍCULA SE REALIZARÁ DEL 2 AL 4 DE JUNIO Y DEL 3 AL 5 DE SEPTIEMBRE.</span></li>
</ul>
<ul style="text-align: justify;">
<li>
<h3 style="margin: 1px;">PARA CONSEGUIR EL PIN (REGISTRO PARA LAS PAU) ENTRAR EN LA SIGUIENTE WEB:</h3>
</li>
</ul>
<p style="text-align: center;"><strong><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank"><span style="color: #ffffff;">HTTPS://OFICINAVIRTUAL.UGR.ES/APLI/SOLICITUDPAU/SELECTIVIDAD</span>00-MENU.JSP</a></strong></p>
<p style="text-align: justify;"> <a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/documentacion/Parametros_AyB_UA_Bachillerato_2014_2015.pdf" target="_blank" style="font-size: 18px; font-weight: bold; text-align: left; text-transform: uppercase;">PARÁMETROS DE PONDERACIÓN, CONSULTA ESTÁTICA.</a></p>
<ul style="text-align: justify;">
<li>
<p style="margin: 1px;"><strong><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/g_b_parametros_top.php" target="_blank">CONSULTA DINÁMICA</a>.</strong></p>
</li>
<li>
<h4 style="margin: 1px;"><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/" target="_blank">DISTRITO ÚNICO ANDALUZ.</a></h4>
</li>
</ul>
<ul style="text-align: justify;">
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI ENCUENTRO DE LA UNIVERSIDAD CON LOS CENTROS.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">PRESENTACIÓN: REGISTRO PARA LA <span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">PRESENTACIÓN: MATRÍCULA DE <span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank"><span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>: ORIENTACIONES.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">WEB DE LA UNIVERSIDAD PARA REGISTRO Y MATRÍCULA.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">OTROS DATOS DE INTERÉS: </a></h4>
</li>
</ul>
<ol style="text-align: justify;"><ol>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">INSCRIPCIÓN</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PLAZOS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">MATRICULACIÓN</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PRECIOS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">CALENDARIO DE LAS PRUEBAS DÍA A DÍA</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">NOTA DE ACCESO</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">EXÁMENES DE OTROS AÑOS Y ORIENTACIONES</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">ORDEN DE PREFERENCIA DE CARRERAS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PROCEDIMIENTO DE MATRÍCULA EN LA UINIVERSIDAD</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PLAZOS</a></h4>
</li>
</ol></ol>
<h4> </h4></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Normativa</span> / <span class="art-post-metadata-category-name">ÚLTIMA NORMATIVA</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Asociaciones</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 05 Diciembre 2012 10:24</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=48:asociaciones&amp;catid=104&amp;Itemid=709&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=891f60aed5c123dc436ca46ddb96a8c45ff4f46f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2089
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="pageName" style="font: normal normal normal 14px/normal georgia; color: #cc3300; font-weight: bold; letter-spacing: 0.1em; line-height: 26px; padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;">Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía</td>
</tr>
<tr>
<td class="bodyText" style="font: normal normal normal 11px/normal arial; color: #333333; line-height: 24px;">
<table width="98%" border="0" cellspacing="1">
<tbody>
<tr>
<td bgcolor="#9AB2CE" style="font: normal normal normal 11px/normal arial; color: #333333;"><span class="Estilo2">Asociaciones de Madres y Padres del Alumnado y Asociaciones de Alumnos/as</span></td>
</tr>
<tr>
<td class="listado" style="font: normal normal normal 11px/normal arial; color: #333333;"><ol>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc17julio2009inscripcioncensoentidades.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 17-7-2009</a>&nbsp;de la Dirección General de Participación e Innovación Educativa para la solicitud de inscripción en el Censo de Entidades Colaboradoras de la Enseñanza.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/decretos/Decreto71-2009censoentidadescolaboradoras.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">DECRETO 71/2009, de 31 de marzo</a>, por el que se regula el Censo de Entidades Colaboradoras de la Enseñanza (BOJA 17-04-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD%201740-2003%20Asociaciones%20publicas.htm" target="_blank" style="color: #005fa9; text-decoration: none;">REAL DECRETO 1740/2003, de 19 de diciembre,</a> sobre procedimientos relativos a asociaciones de utilidad pública. (BOE 13-1-2004)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/decretos/Decreto%2028-1988%20Asociaciones%20alumnos.htm" target="_blank" style="color: #005fa9; text-decoration: none;">DECRETO 28/1988</a>, de 10 de febrero, por el que se regulan las Asociaciones de Alumnos de los centros docentes no universitarios en el ámbito de la C.A. de Andalucía (BOJA 1-3-88)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/decretos/Decreto%2027-1988%20Asociaciones%20padres.htm" target="_blank" style="color: #005fa9; text-decoration: none;">DECRETO 27/1988</a>, de 10 de febrero, por el que se regulan las APAs de centros docentes no universitarios en el ámbito de la Comunidad Autónoma de Andalucía (BOJA 1-3-88).</li>
</ol></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">ESO</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 05 Diciembre 2012 10:24</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=49:normativa-de-eso&amp;catid=104&amp;Itemid=709&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=1d8e5af7e77ccd2a4beedd3e55ebb52a3017db08" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2411
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><table cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td style="font: normal normal normal 14px/normal georgia; color: #cc3300; font-weight: bold; letter-spacing: 0.1em; line-height: 26px; padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;" class="pageName">Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333; line-height: 24px;" class="bodyText">
<table cellspacing="1" border="0" width="98%">
<tbody>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" bgcolor="#9AB2CE"><span class="Estilo2">Educación Secundaria</span></td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" class="listado"><ol>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden17marzo2011modificaOrdenesEvaluacion.pdf">ORDEN de 17 de marzo de 2011</a>, por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/OrdenEDU9sept2009promocioncursoLOGSE-LOE.pdf">ORDEN EDU/2395/2009</a>, de 9 de septiembre, por la que se regula la promoción de un curso incompleto del sistema educativo definido por la Ley Orgánica 1/1990, de 3 de octubre, de ordenación general del sistema educativo, a otro de la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 12-09-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/20090122pruebasesomayores18.pdf">ORDEN de 8-1-2009</a>, por la que se regulan las pruebas para la obtención del título de Graduado en Educación Secundaria Obligatoria, para personas mayores de dieciocho años.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc%2017-12-2007%20Evaluacion%20ESO.pdf">INSTRUCCIONES de 17-12-2007,</a> de la Dirección General de Ordenación y Evaluación Educativa, por la que se complementa la normativa sobre evaluación del proceso de aprendizaje del alumnado de Educación Secundaria Obligatoria.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Aclaracion%204-10-2007%20Acceso%20ESA2.pdf">Aclaraciones de 4-10-2007,</a> de la Directora General de Formación Profesional y Educación Permanente, sobre el acceso del alumnado al Nivel II de las enseñanzas de Educación Secundaria Obligatoria para las personas adultas.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2010-8-2007%20Regula%20ESA.pdf">ORDEN de 10-8-2007,</a> por la que se regula la Educación Secundaria Obligatoria para Personas Adultas. (BOJA 31-8-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2010-8-2007%20Curriculo%20Secundaria.pdf">ORDEN de 10-8-2007,</a> por la que se desarrolla el currículo correspondiente a la Educación Secundaria Obligatoria en Andalucía. (BOJA 30-8-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2010-8-2007%20Evaluacion%20Secundaria.pdf">ORDEN de 10-8-2007,</a> por la que se establece la ordenación de la evaluación del proceso de aprendizaje del alumnado de educación secundaria obligatoria en la Comunidad Autónoma de Andalucía. (BOJA 23-8-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto%20231-2007%20Ensenanzas%20Secundaria.pdf">DECRETO 231/2007, de 31 de julio,</a> por el que se establece la ordenación y las enseñanzas correspondientes a la educación secundaria obligatoria en Andalucía. (BOJA 8-8-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%206-6-2007%20Curriculum%20Religion.pdf">ORDEN ECI/1957/2007, de 6 de junio,</a> por la que se establecen los currículos de las enseñanzas de religión católica correspondientes a la educación infantil, a la educación primaria y a la educación secundaria obligatoria. (BOE 3-7-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2019-6-2007%20Documentos%20Evaluacion.pdf">ORDEN ECI/1845/2007, de 19 de junio,</a> por la que se establecen los elementos de los documentos básicos de evaluación de la educación básica regulada por la Ley Orgánica 2/2006, de 3 de mayo, de Educación, así como los requisitos formales derivados del proceso de evaluación que son precisos para garantizar la movilidad del alumnado. (BOE 22-6-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2015-1-2007%20Inmigrantes%20ATAL.pdf">ORDEN de 15-1-2007,</a> por la que se regulan las medidas y actuaciones a desarrollar para la atención del alumnado inmigrante y, especialmente, las Aulas Temporales de Adaptación Lingüística. (BOJA 14-2-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/rdecre/RD%201631-2006%20Ensenanzas%20ESO.pdf">REAL DECRETO 1631/2006, de 29 de diciembre,</a> por el que se establecen las enseñanzas mínimas correspondientes a la Educación Secundaria Obligatoria. (BOE 5-1-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%205-9-2006%20Funcion%20Directiva.pdf">ORDEN de 5-9-2006,</a> por la que se amplían las horas de función directiva a los Directores y Directoras de los Institutos de Educación Secundaria y Centros de Enseñanza de Régimen Especial, a excepción de los Conservatorios Elementales de Música. (BOJA 22-9-2006)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/orden%2011-5-2006%20gastos%20funcionamiento.pdf">ORDEN de 11-5-2006,</a> conjunta de las Consejerías de Economía y Hacienda y de Educación, por la que se regula la gestión económica de los fondos con destino a inversiones que perciban con cargo al presupuesto de la Consejeríade Educación los centros docentes públicos de educación secundaria, de enseñanzas de régimen especial a excepción de los Conservatorios Elementales de Música, y las Residencias Escolares, dependientes de la Consejería de Educación. (BOJA 25-5-2006)</li>
</ol></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Bachillerato</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 05 Diciembre 2012 10:24</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=50:normativa-de-bachillerato&amp;catid=104&amp;Itemid=709&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=043d1a6c20c4fbbda7afeec3f14285f38cf4a2a1" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3339
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="pageName" style="font: normal normal normal 14px/normal georgia; color: #cc3300; font-weight: bold; letter-spacing: 0.1em; line-height: 26px; padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;">Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía</td>
</tr>
<tr>
<td class="bodyText" style="font: normal normal normal 11px/normal arial; color: #333333; line-height: 24px;">
<table width="98%" border="0" cellspacing="1">
<tbody>
<tr>
<td bgcolor="#9AB2CE" style="font: normal normal normal 11px/normal arial; color: #333333;"><span class="Estilo2">Bachillerato</span></td>
</tr>
<tr>
<td class="listado" style="font: normal normal normal 11px/normal arial; color: #333333;"><ol>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden17marzo2011modificaOrdenesEvaluacion.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 17 de marzo de 2011</a>, por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/resoluc/Resoluc22oct2010bachillerbaccalaureat.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">RESOLUCIÓN de 22 de octubre de 2010</a>, de la Secretaría de Estado de Educación y Formación Profesional, por la que se dictan instrucciones relativas al programa de doble titulación Bachiller-Baccalauréat correspondientes al curso 2010- 2011 (BOE 04-11-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc4octubre2010pruebastitulobachillermayores20annos.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 4 de octubre de 2010</a>, de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de 2010.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/resoluc/Resolucion16sept2010convocatoriapruebaslibresBach.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">RESOLUCIÓN de 16 de septiembre de 2010</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 01-10-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc22sept2010matriculacionalumnadoBachilleratoconsuspensos.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 22 de septiembre</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre las condiciones de matriculación del alumnado de bachillerato con evaluación negativa en algunas materias, para el curso académico 2010/2011.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2010premiosextraordinariosbachillerato2009-10.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 15 de septiembre de 2010</a> de la Dirección General de Ordenación y Evaluación Educativa relativas a los Premios Extraordinarios de Bachillerato correspondientes al curso 2009/2010.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden8sept2010premiosextraordinariosbachillerato2009-10.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 8 de septiembre de 2010</a>, por la que se convocan los Premios Extraordinarios de Bachillerato correspondientes al curso 2009-2010 (BOJA 23-09-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden26agosto2010pruebastituloBachiller.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 26 de agosto de 2010</a>, por la que se regulan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 09-09-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden30julio2010curriculomixtobachilleratobaccalaureat.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN EDU/2157/2010, de 30 de julio</a>, por la que se regula el currículo mixto de las enseñanzas acogidas al Acuerdo entre el Gobierno de España y el Gobierno de Francia relativo a la doble titulación de Bachiller y de Baccalauréat en centros docentes españoles, así como los requisitos para su obtención (BOE 07-08-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden13julio2010premiosnacionalesbachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN EDU/2058/2010, de 13 de julio</a>, por la que se regulan los Premios Nacionales de Bachillerato establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 29-07-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden6mayo2010modificacionOrdenbeca6000.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 6-05-2010</a>, por la que se modifica la de 24 de junio de 2009, conjunta de las Consejerías de Educación y de Empleo, por la que se establecen las bases reguladoras de la BECA 6000 dirigida a facilitar la permanencia en el sistema educativo del alumnado de bachillerato o de ciclos formativos de grado medio de formación profesional inicial y se efectúa su convocatoria para el curso escolar 2009-2010 (BOJA 07-06-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD-102-2010-bachillerato-espana-francia.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">REAL DECRETO 102/2010</a>, de 5 de febrero, por el que se regula la ordenación de las enseñanzas acogidas al acuerdo entre el Gobierno de España y el Gobierno de Francia relativo a la doble titulación de Bachiller y de Baccalauréat en centros docentes españoles (BOE 12-03-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD1953-2009modificandoRDcalculonotamediabachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">REAL DECRETO 1953/2009</a>, de 18 de diciembre, por el que se modifican el Real Decreto 1577/2006, de 22 de diciembre, el Real Decreto 85/2007, de 26 de enero, y el Real Decreto 1467/2007, de 2 de noviembre, en lo relativo al cálculo de la nota media de los alumnos de las enseñanzas profesionales de música y danza (BOE 18-01-2010).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc1oct2009promocioncursoincompletobach.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 1-10-2009</a> de la Dirección General de Ordenación y Evaluación Educativa, sobre la promoción del alumnado del curso incompleto de segundo de bachillerato del sistema educativo regulado en la Ley Orgánica 1/1990, de 3 de octubre, de Ordenación General del Sistema Educativo al previsto en la Ley Orgánica 2/2006, de 3 de mayo, de Educación.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/OrdenEDU9sept2009promocioncursoLOGSE-LOE.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN EDU/2395/2009</a>, de 9 de septiembre, por la que se regula la promoción de un curso incompleto del sistema educativo definido por la Ley Orgánica 1/1990, de 3 de octubre, de ordenación general del sistema educativo, a otro de la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 12-09-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/Instruc22junio2009optativas2bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 22-6-2009</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre la oferta por los centros de materias optativas de segundo curso de Bachillerato.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/instruc/INSTRUC_PRIMBACH_3_4_MAT_NOSUP_2009.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">INSTRUCCIONES de 16-6-2009</a>, de la Dirección General de Ordenación y Evaluación Educativa, sobre la permanencia en el primer curso de bachillerato del alumnado con tres ó cuatro materias no superadas.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden24junio2009becas6000.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 24-6-2009</a>, conjunta de las Consejerías de Educación y de Empleo, por las que se establecen las bases reguladoras de la BECA 6000, dirigida a facilitar la permanencia en el sistema educativo del alumnado de bachillerato o de ciclos formativos de grado medio de formación profesional inicial y se efectúa su convocatoria para el curso escolar 2009-2010 (BOJA 29-06-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/resoluc/Resoluc17junio2009acuerdorepeticionbachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">RESOLUCIÓN de 17-6-2009</a>, de la Dirección General de Evaluación y Cooperación Territorial, por la que se publica el Acuerdo de la Conferencia Sectorial de Educación sobre las condiciones de repetición en el primer curso de Bachillerato (BOE 19-06-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden%20de%2015%20_12_2008%20Evaluacion%20Bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 15-12-2008</a>, por la que se establece la ordenación de la evaluación del proceso de aprendizaje del alumnado de bachillerato en la Comunidad Autónoma de Andalucía.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/bachillerato%20de%20adultos.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 29-9-2008</a>, por la que se regulan las enseñanzas de Bachillerato para personas adultas.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/Orden%205-8-2008%20Curriculo%20Bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 5-8-2008,</a> por la que se desarrolla el currículo correspondiente al Bachillerato en Andalucía. (BOJA 26-8-2008)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/decretos/Decreto%20416-2008%20Bachillerato%20Andalucia.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">DECRETO 416/2008, de 22 de julio,</a> por el que se establece la ordenación y las enseñanzas correspondientes al Bachillerato en Andalucía. (BOJA 28-7-2008)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/resoluc/Resoluc%2011-4-2008%20ConverCalif%20Bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">RESOLUCIÓN de 11-4-2008,</a> de la Secretaría General de Educación, por la que se establecen las normas para la conversión de las calificaciones cualitativas en calificaciones numéricas del expediente académico del alumnado de bachillerato y cursos de acceso a la universidad de planes anteriores a la Ley Orgánica 1/1990, de 3 de mayo, de Ordenación General del Sistema Educativo. (BOE 24-4-2008)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD%201467-2007%20%20Correccion%20errores.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">Corrección de errores del Real Decreto 1467/2007, de 2 de noviembre,</a> por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas. (BOE 7-11-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/rdecre/RD%201467-2007%20Estructura%20Bachillerato.pdf" target="_blank" style="color: #005fa9; text-decoration: none;">REAL DECRETO 1467/2007, de 2 de noviembre,</a> por el que se establece la estructura del bachillerato y se fijan sus enseñanzas mínimas. (BOE 6-11-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a href="http://www.adideandalucia.es/normas/ordenes/orden%2013-12-1994%20Religion%20Bachillerato.htm" target="_blank" style="color: #005fa9; text-decoration: none;">ORDEN de 13-12-1994</a> por la que se establece el curriculum de Religión Católica en el Bachillerato en Andalucía. (BOJA de 10-01-95).</li>
</ol></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
                <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postcontent">
<div class="cat-children">
        <h3>Subcategorías</h3>
        
	<ul>
					<li class="last">
						<span class="item-title"><a href="/index.php?option=com_content&amp;view=category&amp;id=143&amp;Itemid=709">
				NOVEDADES</a>
			</span>

						            
						<dl>
				<dt>
					Número de Artículos:				</dt>
				<dd>
					5				</dd>
			</dl>
			
					</li>
				</ul>
    </div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:63:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - INTERCAMBIOS";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:102:"/index.php?option=com_content&amp;view=category&amp;id=104&amp;Itemid=709&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:103:"/index.php?option=com_content&amp;view=category&amp;id=104&amp;Itemid=709&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:12:"INTERCAMBIOS";s:4:"link";s:59:"index.php?option=com_content&view=article&id=257&Itemid=709";}i:1;O:8:"stdClass":2:{s:4:"name";s:9:"Normativa";s:4:"link";s:60:"index.php?option=com_content&view=category&id=196&Itemid=709";}i:2;O:8:"stdClass":2:{s:4:"name";s:17:"ÚLTIMA NORMATIVA";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:176908:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Proyecto de Centro</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">2º BACH D</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 13 Febrero 2015 10:01</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=94:2o-bach-d&amp;catid=38&amp;Itemid=668&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=9d44917c6d1d898b230790c7e02ce7049d2f1424" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 68059
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><span style="text-decoration: underline; font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">MATEMÁTICAS APLICADAS CC SS II - 2º BACH. CURSO 2014/15.</span></span></p>
<p> </p>
<h5 style="text-align: left; padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">0.   <a href="archivos_ies/14_15/mate/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2014_2015.pdf" target="_blank">ORIENTACIONES UNIVERSIDAD CURSO 2014/15</a></span> (14/10/14)</span></strong></h5>
<p style="text-align: left; padding-left: 30px;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">1.   <a href="archivos_ies/13_14/2bach_matccssii/Matrices.pdf" target="_blank">MATRICES</a></span> (14/10/14)  - <a href="archivos_ies/14_15/mate/MATRICES_2Solucionario.pdf" target="_blank">SOLUCIONES</a> </p>
<ul>
<ul>
<li><a href="archivos_ies/13_14/2bach_matccssii/matrices_2001_2013_resueltos.pdf" target="_blank"><span style="font-size: 14pt;">Exámenes de Selectividad resueltos desde 2001 hasta 2013.</span></a></li>
</ul>
</ul>
<h5 style="text-align: left; padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">2.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><a href="archivos_ies/14_15/mate/programacion_lineal_tema.pdf" target="_blank" style="color: #2f310d;">programación lineal</a></span></span></strong><strong><span style="font-size: 14pt;"> (05/11/13) - </span></strong><a href="archivos_ies/14_15/mate/programacion_lineal_soluciones_tema.pdf" target="_blank" style="font-size: 13px; font-weight: normal; color: #2f310d;">SOLUCIONES</a><span style="font-size: 13px; font-weight: normal;"> </span></h5>
<ul>
<ul>
<li><span style="font-size: 14pt;">Exámenes de Selectividad resueltos desde 2001 hasta 2013: </span><a href="http://emestrada.files.wordpress.com/2010/02/201342.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201241.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201146.pdf">2011</a> <span style="color: #3366ff;">; </span><a href="http://emestrada.files.wordpress.com/2010/02/201020.pdf">2010</a><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/2009_t31.pdf"><strong><span style="color: #3366ff;">2009</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2008_t31.pdf"><strong><span style="color: #3366ff;">2008</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2007_t31.pdf"><strong><span style="color: #3366ff;">2007</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2006_t3.pdf"><strong><span style="color: #3366ff;">2006</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2005_t3.pdf"><strong><span style="color: #3366ff;">2005</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2004_t3.pdf"><strong><span style="color: #3366ff;">2004</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2003_t3.pdf"><strong><span style="color: #3366ff;">2003</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2002_t3.pdf"><strong><span style="color: #3366ff;">2002</span></strong></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2001_t3.pdf"><strong><span style="color: #3366ff;">2001</span></strong></a></li>
</ul>
</ul>
<h5 style="text-align: left; padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">3.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><a href="archivos_ies/14_15/mate/Unidad3.Determinantes.pdf" target="_blank" style="color: #2f310d;">DETERMINANTES</a></span></span></strong><strong><span style="font-size: 14pt;"> (01/11/13) </span></strong><strong><span style="font-size: 14pt;"> - </span></strong><a href="archivos_ies/14_15/mate/determinantes_solucionario.pdf" target="_blank" style="color: #2f310d; font-weight: normal; font-size: 13px;">SOLUCIONES</a><span style="font-size: 13px; font-weight: normal;"> </span></h5>
<h5 style="text-align: left; padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">4.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><a href="archivos_ies/14_15/mate/Probabilidat_tema10.pdf" target="_blank" style="color: #2f310d;">PROBABILIDAD</a> (23/11/13) </span></span></strong><strong><span style="font-size: 14pt;"> - </span></strong><span style="color: #2f310d; font-size: small;"><span style="font-weight: normal;">SOLUCIONES</span></span><span style="font-size: 13px; font-weight: normal;"> </span></h5>
<ul>
<ul>
<li><a href="archivos_ies/14_15/mate/Probabilidad_teoria.pdf" target="_blank" style="font-size: 12pt;">Teoría.</a></li>
<li><a href="archivos_ies/14_15/mate/Unidad10.Calculodeprobabilidades.pdf" target="_blank"><span style="color: #8e9326; font-size: medium;"><span style="letter-spacing: 1px;"><span style="text-decoration: underline;">Más apuntes.</span></span></span></a></li>
<li><a href="archivos_ies/14_15/mate/Probabilidad_otros_Apuntes.pdf" target="_blank" style="font-size: 12pt;">Otros apuntes.</a></li>
<li><a href="archivos_ies/14_15/mate/leygrandesnum.pdf" target="_blank">Ley de los grandes números</a>: <a href="archivos_ies/14_15/mate/LEY_GRANDES_NUMEROS_PRUEBA.xlsx" target="_blank">ejercicio excel</a> - <a href="archivos_ies/14_15/mate/ley_gr_numeros.ods" target="_blank">calc.</a></li>
<li><span style="font-size: 14pt;">Exámenes de Selectividad resueltos desde 2001 hasta 2013:  </span><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/201344.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201243.pdf">2012</a>;</span> <a href="http://emestrada.files.wordpress.com/2010/02/201148.pdf">2011</a> ; <span style="color: #3366ff;"><a href="http://emestrada.files.wordpress.com/2010/02/201046.pdf"><span style="color: #3366ff;">2010</span></a></span></strong><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/2009_t51.pdf"><span style="color: #3366ff;"><strong>2009</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2008_t52.pdf"><span style="color: #3366ff;"><strong>2008</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2007_t51.pdf"><span style="color: #3366ff;"><strong>2007</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2006_t5.pdf"><span style="color: #3366ff;"><strong>2006</strong></span></a><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2005.pdf"><strong><span style="color: #3366ff;">2005</span></strong></a><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/20043.pdf"><strong><span style="color: #3366ff;">2004</span></strong></a><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/2003.pdf"><strong><span style="color: #3366ff;">2003</span></strong></a><strong><span style="color: #3366ff;">;</span></strong> <a href="http://emestrada.files.wordpress.com/2010/02/2002.pdf"><strong><span style="color: #3366ff;">2002</span></strong></a><strong><span style="color: #3366ff;"> ;</span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2001_t5.pdf"><span style="color: #3366ff;"><strong>2001</strong></span></a></li>
<li><span style="font-size: 14pt;"><a href="archivos_ies/Probabilidad_resueltos.pdf" target="_blank">Ejercicios de Probabilidad resueltos.</a></span></li>
</ul>
</ul>
<p> </p>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">5.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/2bach_matccssii/muestreoteoria.pdf" target="_blank">MUESTREO. DISTRIBUCIONES MUESTRALES</a> - <a href="archivos_ies/13_14/2bach_matccssii/muestreoejercicios.pdf" target="_blank">EJERCICIOS</a> - <a href="archivos_ies/13_14/2bach_matccssii/MUESTREOSolucionario.pdf" target="_blank">SOLUCIONES</a></span></span></span></strong></p>
<ul>
<ul>
<li><a href="archivos_ies/13_14/2bach_matccssii/muestreo_resumen.pdf" target="_blank"><span style="color: #2f310d; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 19px; letter-spacing: 1.3333333730697632px;"><strong>Resumen de muestreo.</strong></span></span></a></li>
<li><strong><span style="color: #3366ff; text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201345.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201244.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201149.pdf">2011</a> ; </span><span style="text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201045.pdf">2010</a></span>; </strong><a href="http://emestrada.files.wordpress.com/2010/02/2009_t6.pdf"><span style="color: #3366ff;"><strong>2009</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2008_t6.pdf"><span style="color: #3366ff;"><strong>2008</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2007_t6.pdf"><span style="color: #3366ff;"><strong>2007</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2006_t6.pdf"><span style="color: #3366ff;"><strong>2006</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2005_t6.pdf"><span style="color: #3366ff;"><strong>2005</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/200414.pdf"><span style="color: #0000ff;">2004</span></a></span></strong><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong><span style="color: #3366ff;">;</span></strong></span> </a><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/200310.pdf"><span style="color: #0000ff;">2003</span></a></span></strong><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/2002_t6.pdf"><strong><span style="color: #3366ff;">2002</span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/2001_t6.pdf"><strong><span style="color: #3366ff;">2001</span></strong></a></li>
</ul>
</ul>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">6. </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/2bach_matccssii/inferencia_tema.pdf" target="_blank">INFERENCIA ESTADÍSTICA. ESTIMACIÓN. CONTRASTE DE HIPÓTESIS</a> - SOLUCIONES<br /></span></span></span></strong></p>
<ul>
<ul>
<li><strong><span style="color: #3366ff;"><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/201346.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201232.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201163.pdf"><span style="color: #0000ff;">2011</span></a></span>; <a href="http://emestrada.files.wordpress.com/2010/02/201054.pdf">2010</a></span></strong></li>
</ul>
</ul>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">7.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt;">LÍMITES Y CONTINUIDAD</span></span></strong></p>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">8.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;">DERIVADA DE UNA FUNCIÓN</span></span></span></strong></p>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">9.   </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;">APLICACIONES DE LA DERIVADA</span></span></span></strong></p>
<p style="padding-left: 30px;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;">10. </span></span></strong><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;">REPRESENTACIÓN DE FUNCIONES</span></span></span></strong></p>
<ul>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #0000ff; text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201343.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201259.pdf">2012</a>;</span>  <a href="http://emestrada.files.wordpress.com/2010/02/201147.pdf">2011</a> </span></strong><span style="color: #3366ff;"><strong>; </strong></span><strong><a href="http://emestrada.files.wordpress.com/2010/02/201037.pdf">2010</a></strong><strong><span style="color: #3366ff;">; </span></strong><a href="http://emestrada.files.wordpress.com/2010/02/2009_t42.pdf"><span style="color: #3366ff;"><strong>2009</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2008_t41.pdf"><span style="color: #3366ff;"><strong>2008</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2007_t41.pdf"><span style="color: #3366ff;"><strong>2007</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2006_t4.pdf"><span style="color: #3366ff;"><strong>2006</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2005_t4.pdf"><span style="color: #3366ff;"><strong>2005</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2004_t4.pdf"><span style="color: #3366ff;"><strong>2004</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2003_t4.pdf"><span style="color: #3366ff;"><strong>2003</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2002_t4.pdf"><span style="color: #3366ff;"><strong>2002</strong></span></a><span style="color: #3366ff;"><strong>; </strong></span><a href="http://emestrada.files.wordpress.com/2010/02/2001_t4.pdf"><span style="color: #3366ff;"><strong>2001</strong></span></a></li>
</ul>
</ul>
<p> </p>
<hr />
<p><span style="text-decoration: underline;"><strong><span style="font-size: 14pt;">Últimos exámenes de Selectividad:</span></strong></span></p>
<p><span><span style="font-size: 14pt;">Con nuestro agradecimiento a nuestro compañero del IES Ayala Germán-Jesús Rubio Luna por sus soluciones.</span></span></p>
<p style="padding-left: 60px;"><span> </span><strong style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: center; background-color: #ffffff;"><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center;"><span style="text-decoration: underline;">Junio 2014</span> </strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" alt="" align="middle" style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" /></a></strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank" style="background-color: #ffffff;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluc.</strong></a></p>
<p style="padding-left: 60px;"><a href="archivos_ies/13_14/selectividad/mat_ccss_ii_sept_2014.pdf" target="_blank" title="Matemáticas Aplicadas a las Ciencias Sociales II." style="text-align: -webkit-center; background-color: #ffffff;">Sept. 2014<strong style="color: #000000;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" alt="" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; background-color: #ffffff;"> </span></p>
<center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_exjun.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio (Modelo ) 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_soljun.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_exjun_espe.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio Colisiones (Modelo ) 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_soljun_espe.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_exsep.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre (Modelo ) 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2013_14/14_solsep.pdf" target="_blank" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
<br /><br /></center>
<ul>
<li><span style="text-decoration: underline;"><strong><span style="font-size: 14pt; color: #2b2721; text-decoration: underline;">Junio 2013:</span></strong></span></li>
</ul>
<ol><ol>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/selectividad_2013_Segundo_dia_2_MAT_APL_CCSS_II_A.pdf" target="_blank">Opción A.</a> <a href="archivos_ies/13_14/2bach_matccssii/2013_soljun.pdf" target="_blank">Soluciones.</a></span></span></li>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/selectividad_2013_Segundo_dia_2_MAT_APL_CCSS_II_B.pdf" target="_blank">Opción B.</a>  <a href="archivos_ies/13_14/2bach_matccssii/2013_soljun.pdf" target="_blank">Soluciones.</a></span></span></li>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/2013_exjun_esp.pdf" target="_blank">Específico (4º día)</a>  <a href="archivos_ies/13_14/2bach_matccssii/2013_soljun_esp.pdf" target="_blank">Soluciones.</a></span></span></li>
</ol></ol>
<ul>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"> </span></span><strong><span style="font-size: 14pt; color: #2b2721; text-decoration: underline;">Sept. 2013:</span></strong></li>
</ul>
<ol><ol>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/MAT_CCSSII_A_Selec_sep2013.pdf" target="_blank">Opción A.</a>  </span></span><a href="archivos_ies/13_14/2bach_matccssii/Sept_2012_selec_MatCCSSII_y_soluciones2.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 19px;">Soluciones.</a></li>
<li><span style="color: #2b2721;"><span style="font-size: 19px;"><a href="archivos_ies/13_14/2bach_matccssii/MAT_CCSSII_B_Selec_sep2013.pdf">Opción B.</a>   </span></span><a href="archivos_ies/13_14/2bach_matccssii/2013_solsep.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 19px;">Soluciones.</a></li>
</ol></ol>
<ul>
<li><a href="archivos_ies/13_14/2bach_matccssii/Junio_2012_selec_MatCCSSII_y_soluciones.pdf" target="_blank"><span style="text-decoration: underline;"><strong><span style="font-size: 14pt; color: #2b2721; text-decoration: underline;">Junio 2012 con soluciones.</span></strong></span><span style="font-size: 19px; color: #2b2721;"> </span></a></li>
</ul>
<ul>
<li><span style="text-decoration: underline;"><strong><span style="font-size: 14pt; color: #2b2721; text-decoration: underline;"><a href="archivos_ies/13_14/2bach_matccssii/Sept_2012_selec_MatCCSSII_y_soluciones2.pdf" target="_blank">Sept. 2012 con soluciones.<br /><br /></a></span></strong></span></li>
</ul>
<div style="text-align: center;"><hr /><a href="index.php?option=com_content&amp;view=article&amp;id=96" target="_blank">ENTRAR EN ZONA PRIVADA DEL CURSO.</a><hr />
<table style="color: #000000; font-family: verdana; font-size: 11px; line-height: 16.6399993896484px; text-align: left;" width="80%" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td><strong>Asignatura</strong></td>
<td align="center"><strong>Orientaciones<br />2014/2015</strong></td>
<td align="center"><strong>2005</strong></td>
<td align="center"><strong>2006</strong></td>
<td align="center"><strong>2007</strong></td>
<td align="center"><strong>2008</strong></td>
<td align="center"><strong>2009</strong></td>
<td align="center"><strong>2010</strong></td>
<td align="center"><strong>2011</strong></td>
<td align="center"><strong>2012</strong></td>
<td align="center"><strong>2013</strong></td>
<td align="center"><strong>2014</strong></td>
</tr>
<tr>
<td>Análisis Musical II <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_analisis_musical_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="1.66 Mb" title="1.66 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="5.31 Mb" title="5.31 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="170.95 Mb" title="170.95 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="265.22 Mb" title="265.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_analisis_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="213.99 Mb" title="213.99 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Biología <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_biologia_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="2.83 Mb" title="2.83 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.09 Mb" title="2.09 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.29 Mb" title="1.29 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.26 Mb" title="1.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.07 Mb" title="2.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.54 Mb" title="2.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_biologia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.5 Mb" title="1.5 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Ciencias de la Tierra y Medioambientales <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_ciencias_de_la_tierra_y_medioambientales_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.64 Mb" title="0.64 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.44 Mb" title="1.44 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="14.79 Mb" title="14.79 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.47 Mb" title="1.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.88 Mb" title="1.88 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.45 Mb" title="1.45 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_ciencias_tierra.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.58 Mb" title="0.58 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Comentario de Texto,Lengua Castellana y Literatura<em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_comentario_texto_lengua_castellana_y_literatura_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.4 Mb" title="0.4 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_lengua_castellana.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.95 Mb" title="1.95 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Dibujo Artístico II <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_artistico_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="13.76 Mb" title="13.76 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.22 Mb" title="2.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.34 Mb" title="1.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="5.2 Mb" title="5.2 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="27.54 Mb" title="27.54 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="13.52 Mb" title="13.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="59.45 Mb" title="59.45 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.72 Mb" title="3.72 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_dibujo_artistico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="7.72 Mb" title="7.72 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Dibujo Técnico II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_tecnico_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.56 Mb" title="2.56 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="6.86 Mb" title="6.86 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="8.05 Mb" title="8.05 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="15.45 Mb" title="15.45 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="12.99 Mb" title="12.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="6.97 Mb" title="6.97 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_dibujo_tecnico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="7.72 Mb" title="7.72 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Diseño <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_diseno_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fundamentos_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="12.38 Mb" title="12.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.66 Mb" title="0.66 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.12 Mb" title="1.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.76 Mb" title="3.76 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_fundamento_diseno.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="23.21 Mb" title="23.21 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Economía de la Empresa <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_economia_de_la_empresa_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.1 Mb" title="0.1 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.31 Mb" title="0.31 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.96 Mb" title="3.96 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.38 Mb" title="1.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_economia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Electrotecnia <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_electrotecnia_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.46 Mb" title="0.46 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_electrotecnia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Física <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_fisica_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.26 Mb" title="0.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.28 Mb" title="0.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.25 Mb" title="0.25 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.39 Mb" title="1.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.36 Mb" title="0.36 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_fisica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Geografía <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_geografia_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.29 Mb" title="0.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.09 Mb" title="3.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.32 Mb" title="1.32 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="12.41 Mb" title="12.41 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.11 Mb" title="2.11 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.86 Mb" title="3.86 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.98 Mb" title="1.98 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_geografia.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.38 Mb" title="1.38 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Griego II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_griego_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.54 Mb" title="0.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.6 Mb" title="0.6 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.35 Mb" title="2.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="4.69 Mb" title="4.69 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.57 Mb" title="0.57 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.51 Mb" title="0.51 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_griego.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.36 Mb" title="0.36 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Historia de España <em>(Común)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_espanna_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.74 Mb" title="0.74 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.73 Mb" title="1.73 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.77 Mb" title="1.77 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_texto_historico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.74 Mb" title="0.74 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia de la Filosofía <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_filosofia_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.05 Mb" title="0.05 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.68 Mb" title="0.68 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_texto_filosofico.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Historia de la Música y de la Danza <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_musica_y_de_la_danza_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="3.3 Mb" title="3.3 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="27.28 Mb" title="27.28 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="22.08 Mb" title="22.08 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.51 Mb" title="1.51 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="13.4 Mb" title="13.4 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.38 Mb" title="2.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="318.43 Mb" title="318.43 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="10.61 Mb" title="10.61 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="93.24 Mb" title="93.24 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="101.86 Mb" title="101.86 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_historia_musica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="121.51 Mb" title="121.51 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia del Arte <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_del_arte_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.49 Mb" title="0.49 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.67 Mb" title="0.67 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.16 Mb" title="3.16 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="8.71 Mb" title="8.71 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="6.91 Mb" title="6.91 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="3.5 Mb" title="3.5 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="18.28 Mb" title="18.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.06 Mb" title="1.06 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_historia_arte.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Latín II <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_latin_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_latin.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.94 Mb" title="0.94 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.94 Mb" title="1.94 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1 Mb" title="1 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.55 Mb" title="0.55 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_latinII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Alemán) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(aleman)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.26 Mb" title="0.26 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.35 Mb" title="1.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.39 Mb" title="0.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.3 Mb" title="1.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_aleman.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Francés) <em>(Común)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(frances)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.1 Mb" title="0.1 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_frances.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Inglés) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(ingles)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="2.16 Mb" title="2.16 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.68 Mb" title="1.68 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.06 Mb" title="2.06 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.18 Mb" title="1.18 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="12.19 Mb" title="12.19 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.74 Mb" title="1.74 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_ingles.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.16 Mb" title="1.16 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Italiano) <em>(Común)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(italiano)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.29 Mb" title="0.29 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.75 Mb" title="0.75 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_italiano.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.31 Mb" title="0.31 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Portugués) <em>(Común)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(portugues)_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.32 Mb" title="0.32 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.78 Mb" title="0.78 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.77 Mb" title="0.77 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_portugues.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Lenguaje y Práctica Musical <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lenguaje_y_practica_musical_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="1.4 Mb" title="1.4 Mb" align="center" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="92.65 Mb" title="92.65 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="2.31 Mb" title="2.31 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="80.36 Mb" title="80.36 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="64.72 Mb" title="64.72 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_lengua_practica_musical.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="60.71 Mb" title="60.71 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Literatura Universal <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_literatura_universal_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.76 Mb" title="0.76 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_literatura_universal.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Matemáticas Aplicadas a las Ciencias Sociales II<em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.62 Mb" title="0.62 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="6.51 Mb" title="6.51 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.42 Mb" title="1.42 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.53 Mb" title="1.53 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_matematicas_aplicadas.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.53 Mb" title="1.53 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Matemáticas II <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="4.39 Mb" title="4.39 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.54 Mb" title="1.54 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.13 Mb" title="1.13 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_matematicasII.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Química <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_quimica_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.34 Mb" title="0.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.34 Mb" title="0.34 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.19 Mb" title="1.19 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.5 Mb" title="0.5 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.66 Mb" title="1.66 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_quimica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.61 Mb" title="1.61 Mb" align="center" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Técnicas de Expresión Gráfico Plásticas <em>(Modalidad)</em></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnicas_de_expresion_grafico-plastica_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.23 Mb" title="0.23 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.85 Mb" title="0.85 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="10.44 Mb" title="10.44 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.86 Mb" title="0.86 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="4.72 Mb" title="4.72 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="8.63 Mb" title="8.63 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.41 Mb" title="1.41 Mb" align="center" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_tecnicas_expresion_grafica.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.99 Mb" title="1.99 Mb" align="center" /></a></td>
</tr>
<tr>
<td>Tecnología Industrial II <em>(Modalidad)</em></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnologia_industrial_ii_2014_2015.pdf"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/pdf.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.14 Mb" title="1.14 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="1.12 Mb" title="1.12 Mb" align="center" /></a></td>
<td align="center"><a href="http://distritounicoandaluz.ceic.junta-andalucia.es/paginas/distrito/examenes_sel_m25/selectividad_2014/sel_2014_tecnologia_industrial.zip"><img src="http://distritounicoandaluz.ceic.junta-andalucia.es/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="center" /></a><br /><br /></td>
</tr>
</tbody>
</table>
<hr />
<table class="tabla_redonda" style="border: 1px solid #ced5d7; border-radius: 6px; padding: 10px 15px; box-shadow: #b5c1c5 0px 5px 10px, #eef5f7 0px 0px 0px 10px inset; color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left; background-color: #ffffff;" width="98%" border="0"><caption class="titulo" style="font-weight: bold; color: #ff9900; font-size: 16px; border: 1px solid #aaaaaa; vertical-align: middle; height: 45px; margin: 10px auto; -webkit-box-shadow: black 0px 0px 4px; box-shadow: black 0px 0px 4px; text-shadow: black 1px 1px 1px; padding-top: 8px; width: 80%; background: #e8edff;">CALENDARIO DE LA PRUEBA DE BACHILLERATO</caption></table>
<h3 style="color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-transform: none;" align="center"><strong>CURSO 2014/2015</strong></h3>
<table style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;" width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td align="left"><strong>Convocatoria ordinaria:</strong> <br />16, 17, 18 de junio de 2015</td>
<td align="right"><strong>Convocatoria extraordinaria:</strong> <br />15, 16 y 17 de septiembre de 2015</td>
</tr>
</tbody>
</table>
<br style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;" />
<table style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;" width="90%" border="0" cellspacing="2" cellpadding="3" align="center">
<tbody>
<tr>
<td align="center" bgcolor="#D8D8D8">HORARIO</td>
<td align="center" bgcolor="#F3F781" width="25%">PRIMER DÍA</td>
<td align="center" bgcolor="#F7BE81" width="25%">SEGUNDO DÍA</td>
<td align="center" bgcolor="#00FF80" width="25%">TERCER DÍA</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td align="center" bgcolor="#D8D8D8">HORARIO</td>
<td align="center" bgcolor="#CEE3F6" width="25%">TERCER DÍA (TARDE)<br />Exclusivo para incompatabilidades</td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">08:00-08:30</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td align="center" bgcolor="#D8D8D8">16:00-16:30</td>
<td align="center" bgcolor="#D8D8D8">CITACIÓN</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">08:30-10:00</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>HISTORIA DEL ARTE</li>
<li>MATEMÁTICAS II</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>ANÁLISIS MUSICAL II</li>
<li>DISEÑO</li>
<li>GEOGRAFÍA</li>
<li>BIOLOGÍA</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">16:30-18:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">10:00-10:45</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">18:00-18:30</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">10:45-12:15</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>HISTORIA DE ESPAÑA</li>
<li>HISTORIA DE LA FILOSOFÍA</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS</li>
<li>QUÍMICA</li>
<li>ELECTROTÉCNIA</li>
<li>LITERATURA UNIVERSAL</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>DIBUJO TÉCNICO II</li>
<li>CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES</li>
<li>ECONOMÍA DE LA EMPRESA</li>
<li>GRIEGO II</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">18:30-20:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
<tr>
<td align="center" bgcolor="#D8D8D8">12:15-13:00</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">20:00-20:30</td>
<td align="center" bgcolor="#D8D8D8">DESCANSO</td>
</tr>
<tr>
<td bgcolor="#D8D8D8">13:00-14:30</td>
<td bgcolor="#F3F781">
<ul class="selectividad">
<li>IDIOMA EXTRANJERO</li>
</ul>
</td>
<td bgcolor="#F7BE81">
<ul class="selectividad">
<li>LENGUAJE Y PRÁCTICA MUSICAL</li>
<li>TECNOLOGÍA INDUSTRIAL II</li>
<li>MATEMÁT. APLIC. A LAS CC. SOCIALES II</li>
</ul>
</td>
<td bgcolor="#00FF80">
<ul class="selectividad">
<li>HISTORIA DE LA MÚSICA Y LA DANZA</li>
<li>DIBUJO ARTÍSTICO II</li>
<li>FÍSICA</li>
<li>LATÍN II</li>
</ul>
</td>
<td align="center" bgcolor="white" width="5%"> </td>
<td bgcolor="#D8D8D8">20:30-22:00</td>
<td bgcolor="#CEE3F6"> </td>
</tr>
</tbody>
</table>
<br style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;" />
<p class="texto_azul" style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;"><strong>NOTAS IMPORTANTES:</strong></p>
<ol style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;">
<li>Las franjas horarias de citación son en defecto de que la universidad no fije otras que en razón de las sedes de que se traten, considere más oportunas; asi mismo sí la última franja horaria del tercer día para incompatibilidades, quedase libre, el descanso entre la primera y segunda franja horaria será de 45 minutos.</li>
<li>El horario de las materias del tercer día por la tarde es <strong><span style="text-decoration: underline;">exclusivamente</span></strong> para quienes tienen más de un examen el mismo día y a la misma hora (en el 2º o 3º día por la mañana), debiendo realizar en dicho horario y en primer lugar el examen que aparece antes en el respectivo cuadro y realizará el examen de la otra materia en el tercer día por la tarde en el horario que se indicará oportunamente.</li>
</ol>
<p style="margin-top: 5px; margin-bottom: 10px; color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;"> </p>
<div style="color: #000000; font-family: verdana; font-size: 12.8000001907349px; line-height: 16.6399993896484px; text-align: left;"><br /><br /><br /><span style="font-size: xx-small;"><strong>Nota de exención de responsabilidad</strong></span></div>
<p style="padding-left: 30px; text-align: center;"><span style="font-size: x-small; color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;">Las informaciones ofrecidas por este medio tienen exclusivamente carácter ilustrativo, y no originarán derechos ni expectativas de derechos.</span><span style="font-size: x-small; color: #000000; font-family: verdana; line-height: 16.6399993896484px; text-align: left;"> </span><a href="http://juntadeandalucia.es/boja/1995/136/1" target="_blank" style="font-size: x-small; line-height: 16.6399993896484px;">(Decreto 204/95, artículo 4; BOJA 136 de 26 de Octubre)</a></p>
<hr />
<p style="padding-left: 30px; text-align: center;"> </p>
<h2 style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: -webkit-center; text-transform: none;"><span style="font-size: x-large;">Exámenes resueltos de Matemáticas CCSS II de Andalucía</span></h2>
<p><span style="color: #000000; font-size: medium; text-align: start; font-family: Verdana;">Realizados por:</span></p>
<p style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: medium;" align="center"><span style="font-family: Arial;"><strong><span style="font-size: large;">D. Germán Jesús Rubio Luna, Catedrático de Matemáticas </span></strong><span style="font-size: large;"><br /></span><strong><span style="font-size: large;">del IES Francisco Ayala de Granada</span>.</strong></span></p>
<p style="padding-left: 30px;"><span style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif;">Desde el año 2004-2005 hasta el año 2012-2013 </span></p>
<table id="table20" width="55%" border="1">
<tbody>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2013" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2013</strong></span></a></p>
</td>
<td align="center">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2012" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2012</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2011" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2011</strong></span></a></p>
</td>
<td align="center">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2010" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2010</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#hipotesis2009" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Problemas Test Hipótesis. Comisión año 2009</strong></span></a></p>
</td>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#proporciones2007" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Intervalos Proporciones. Comisión año 2007</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2009" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2009</strong></span></a></p>
</td>
<td align="center">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2008" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2008</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2007" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2007</strong></span></a></p>
</td>
<td align="center">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2006" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2006</strong></span></a></p>
</td>
</tr>
<tr>
<td align="center" width="251">
<p><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#2005" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;"><strong>Modelos del año 2005</strong></span></a></p>
</td>
<td align="center">
<p><span style="font-family: 'Times New Roman'; font-size: xx-small;"><strong>Modelos del año 2004</strong></span></p>
</td>
</tr>
</tbody>
</table>
<center><br /><br /><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2013"></a></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Año 2012-2013 </span></strong></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2013"></a></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2013"></a></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio Reserva 2 (Modelo 1) 2013</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_exsep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre (Modelo 2) 2013</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_solsep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_exjun_esp.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio Colisión (Modelo 5)2013</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_soljun_esp.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_exjun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio (Modelo 6) 2013</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2012_13/13_soljun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2013"></a><span style="font-family: Arial;"><a name="2012"></a></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Año 2011-2012</span></strong><span style="font-size: medium;"> </span></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2012"></a></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2012"></a></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod1_12.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de 2012</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod1_12_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod2_12.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 de 2012</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod2_12_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/sep12gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 de 2012 (Septiembre General)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/solsep12gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/jun12gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de 2012 (Junio General)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/soljun12gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod5_12.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de 2012</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod5_12_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod6_12.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de 2012</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2011_12/mod6_12_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2012"></a></span></span></span><center><a name="2012"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a></center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2011"></a></span></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Año 2010-2011</span></strong> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2011"></a></span></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2011"></a></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod1_11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de 2011</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod1_11_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod2_11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 de 2011</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod2_11_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod3_11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 de 2011</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/mod3_11_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/jun11gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de 2011 (Junio General)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/soljun11gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/sept11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de 2011 (Septiembre)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/solsept11.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/jun11espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de 2011 (Junio Específico)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2010_11/soljun11espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2011"></a></span></span></span></span><center><a name="2011"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a></center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2010"></a></span></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Año 2009-2010 </span></strong><span style="font-size: medium;"> </span></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2010"></a></span></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2010"></a></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2010</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod2.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 de sobrantes del 2010</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod2_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/sep10gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre de 2010 (General Modelo 3)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/solsep10gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/jun10espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio de 2010 (Específico Modelo 4)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/soljun10espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/jun10gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio de 2010 (General Modelo 5)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/soljun10gene.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2010</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2009_10/10mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2010"></a><a name="hipotesis2009"></a></span></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Problemas Test de Hipósis (Propuestos el año 2009) </span></strong><span style="font-size: medium;"> </span></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="hipotesis2009"></a></span></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="hipotesis2009"></a></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/hipotesis/09_test.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciados</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Problemas Test de Hipósis (Propuestos el año 2009)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/hipotesis/09_test_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Soluciones</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="hipotesis2009"></a></span></span></span></span><center><a name="hipotesis2009"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a></center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2009"></a><strong><span style="font-family: Arial; font-size: large;">Año 2008-2009</span></strong></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod2(sep).pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Septiembre) de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod2(sep)_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod3(junio).pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 (Junio) de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod3(junio)_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod5.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod5_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2009</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2008_09/09mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2009"></a></span></span></span></span><center><a name="2009"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a> <br /><br /></center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2008"></a><strong><span style="font-family: Arial; font-size: large;">Año 2007-2008</span></strong></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod2_Sep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Septiembre) de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod2_Sep_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod3_jun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 (Junio) de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod3_jun_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod5.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod5_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2008</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2007_08/08mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="2008"></a><a name="proporciones2007"></a></span></span></span></span>
<p><strong><span style="font-family: Arial; font-size: large;">Intervalos de Confianza Proporciones (Propuestos el año 2007) </span></strong><span style="font-size: medium;"> </span></p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="proporciones2007"></a></span></span></span></span>
<p> </p>
<span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="proporciones2007"></a></span></span></span></span><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/proporciones/07_propor.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciados</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Intervalos de Proporciones (Propuestos el año 2007)</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/proporciones/07_propor_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Soluciones</span></a></td>
</tr>
</tbody>
</table>
</center><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><span style="font-family: Arial;"><a name="proporciones2007"></a></span></span></span></span><center><a name="proporciones2007"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a> <br /><br /><a name="2007"></a><strong><span style="font-family: Arial; font-size: large;">Año 2006-2007</span></strong><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod2_Junio.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Junio) de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod2_Junio_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod3_sep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 (Septiembre) de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod3_sep_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod5.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod5_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2007</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2006_07/07_mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><center><a name="2007"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a> <br /><a name="2006"></a><strong><span style="font-family: Arial; font-size: large;">Año 2005-2006</span></strong><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod2_sep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Septiembre) de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod2_sep_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod3_jun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 (Junio) de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod3_jun_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod5.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod5_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2006</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2005_06/06_mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center><center><a name="2006" style="color: #000000; font-family: Arial; letter-spacing: normal; text-align: -webkit-center;"></a><br /><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss.html#cabecera" style="text-decoration: none; color: blue; font-family: Arial; letter-spacing: normal; text-align: -webkit-center;"><img src="http://iesayala.com/selectividadmatematicas/imagen/Flecha-Arriba.gif" border="0" alt="cabecera" width="22" height="22" align="BOTTOM" /></a><span style="color: #000000;"> </span><br style="color: #000000;" /><a name="2005" style="color: #000000; font-family: Arial; letter-spacing: normal; text-align: -webkit-center;"></a><strong><span style="font-family: Arial; font-size: large;">Año 2004-2005</span></strong><center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod2_jun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Junio) de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod2_jun_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod3.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod3_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod5_sep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 (Septiembre) de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod5_sep_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod6.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 de sobrantes del 2005</span></td>
<td align="center"><a href="http://iesayala.com/selectividadmatematicas/ficheros/andaluciaccss/2004_05/05_mod6_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center></center></center></center></center>
<p style="padding-left: 30px;"> </p>
<p style="padding-left: 30px;"><strong><span style="text-decoration: underline;"><span style="font-family: 'comic sans ms',sans-serif;"><span style="font-size: 14pt;"> </span></span></span></strong></p>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">MATEMÁTICAS</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
</div>";s:4:"head";a:10:{s:5:"title";s:69:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Proyecto de Centro";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:101:"/index.php?option=com_content&amp;view=category&amp;id=38&amp;Itemid=201&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:102:"/index.php?option=com_content&amp;view=category&amp;id=38&amp;Itemid=201&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:6:"INICIO";s:4:"link";s:53:"index.php?option=com_content&view=featured&Itemid=250";}i:1;O:8:"stdClass":2:{s:4:"name";s:18:"Proyecto de Centro";s:4:"link";s:58:"index.php?option=com_content&view=article&id=19&Itemid=201";}i:2;O:8:"stdClass":2:{s:4:"name";s:12:"MATEMÁTICAS";s:4:"link";s:60:"index.php?option=com_content&view=category&id=213&Itemid=201";}i:3;O:8:"stdClass":2:{s:4:"name";s:12:"Matemáticas";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
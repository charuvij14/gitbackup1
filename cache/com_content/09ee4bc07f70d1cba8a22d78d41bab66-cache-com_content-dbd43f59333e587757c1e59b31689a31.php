<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:32109:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Artículos</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">TEMA 4: MUESTREO</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 05 Diciembre 2012 10:24</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=23:tema-4-muestreo&amp;catid=238&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=6778d43edd744313230d2341462262ad9a2cf1bd" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2336
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><span style="color: #000080;"><span style="text-decoration: underline;"><strong><br />TEMA 11: MUESTREO. DISTRIBUCIONES MUESTRALES.</strong></span><em><strong><br />&nbsp;</strong></em></span></p>
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>Páginas del libro:</strong></span></p>
<ul>
<li><a target="_blank" href="archivos_ies/muestreo/281-303muestreo.pdf">Tema completo hasta página 303.</a></li>
<li><a target="_blank" href="archivos_ies/muestreo/303-311muestreoejercicios.pdf">Ejercicios, páginas 304 a 311.</a></li>
</ul>
<table style="border-collapse: collapse; width: auto; background-color: transparent; margin: 1px; border: 1px solid #c0c0c0;">
<tbody>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_10muestreo.pdf" title="Download this file (10muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/1muestreo.pdf" title="Download this file (1muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 1.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[ Muestreo. Distribuciones muestrales.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">315 Kb</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_11muestreo.pdf" title="Download this file (11muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/2muestreo.pdf" title="Download this file (2muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 2.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Antes de comenzar... recuerda.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">388 Kb</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_1muestreo.pdf" title="Download this file (1muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/3muestreo.pdf" title="Download this file (3muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 3.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Población y muestra.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">650 Kb</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_2muestreo.pdf" title="Download this file (2muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/4muestreo.pdf" title="Download this file (4muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;"> Muestreo página 4.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Muestreo.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">347 Kb</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_3muestreo.pdf" title="Download this file (3muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/5muestreo.pdf" title="Download this file (5muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 5.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Tipos de muestreo aleatorio.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">261 Kb</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_4muestreo.pdf" title="Download this file (4muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/6muestreo.pdf" title="Download this file (6muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 6.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Muestreo sistemático.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">235 Kb</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_5muestreo.pdf" title="Download this file (5muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/7muestreo.pdf" title="Download this file (7muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 7.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Muestreo estratificado.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">344 Kb</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_6muestreo.pdf" title="Download this file (6muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/8muestreo.pdf" title="Download this file (8muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 8.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Muestreo por conglomerados.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">365 Kb</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_7muestreo.pdf" title="Download this file (7muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 9.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Distribución Binomial.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">367 Kb</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_8muestreo.pdf" title="Download this file (8muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_parent" href="archivos_ies/muestreo/10muestreo.pdf" title="Download this file (10muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 10.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Cálculo de probabilidades en B(n, p)]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">369 Kb</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="archivos_ies/muestreo/11muestreo.pdf" title="Download this file (11muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 11.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">&nbsp;[Distribución Normal.]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">357 Kb</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">
<p><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/12muestreo.pdf" title="Download this file (12muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 12.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Tabla de la Normal N(0,1)</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>375 Kb</p>
</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">
<p><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/13muestreo.pdf" title="Download this file (13muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 13.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Intervalos característicos</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>344 Kb</p>
</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a target="_blank" href="attachments/083_tablas.pdf"></a>
<p><a target="_blank" href="attachments/083_tablas.pdf"></a><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/14muestreo.pdf" title="Download this file (14muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 14.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Aproximación de la Binomial</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>346 Kb</p>
</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a target="_blank" href="attachments/083_tablas.pdf"></a>
<p><a target="_blank" href="attachments/083_tablas.pdf"></a><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/15muestreo.pdff" title="Download this file (15muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 15.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Teorema Central del Límite</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>382 Kb</p>
</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a target="_blank" href="attachments/083_tablas.pdf"></a>
<p><a target="_blank" href="attachments/083_tablas.pdf"></a><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/16muestreo.pdf" title="Download this file (16muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 16.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Distribución de la media</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>346 Kb</p>
</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a target="_blank" href="attachments/083_tablas.pdf"></a>
<p><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/17muestreo.pdf" title="Download this file (17muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 17.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Distribución de la proporción</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>374 Kb</p>
</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a target="_blank" href="attachments/083_tablas.pdf"></a>
<p><a target="_blank" href="attachments/083_tablas.pdf"></a><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/18muestreo.pdf" title="Download this file (18muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 18.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Distribución de la diferencia de medias:<br />No en Selectividad 2011.&nbsp;</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>387 Kb</p>
</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a target="_blank" href="attachments/083_tablas.pdf"></a>
<p><a target="_blank" href="attachments/083_tablas.pdf"></a><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/19muestreo.pdf" title="Download this file (19muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 19.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Problemas resueltos 1</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>326 Kb</p>
</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a target="_blank" href="attachments/083_tablas.pdf"></a>
<p><a target="_blank" href="attachments/083_tablas.pdf"></a><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/20muestreo.pdf" title="Download this file (20muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 20.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Problemas resueltos 2</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p><br />349 Kb</p>
</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a target="_blank" href="attachments/083_tablas.pdf"></a>
<p><a target="_blank" href="attachments/083_tablas.pdf"></a><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/21muestreo.pdf" title="Download this file (21muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 21.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Problemas resueltos 3</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>366 Kb</p>
</td>
</tr>
<tr class="even" style="background-color: #f6f6f6;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a target="_blank" href="attachments/083_tablas.pdf"></a>
<p><a target="_blank" href="attachments/083_tablas.pdf"></a><a href="attachments/095_9muestreo.pdf" title="Download this file (9muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="images/stories/cervantes/22muestreo.pdf" title="Download this file (22muestreo.pdf)" class="at_url" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;">Muestreo página 22.</a></p>
</td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><br />Problemas resueltos 4</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">
<p>376 Kb</p>
</td>
</tr>
<tr class="odd" style="background-color: transparent;">
<td class="at_filename" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;"><a href="attachments/095_8muestreo.pdf" title="Download this file (8muestreo.pdf)" class="at_icon" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;"><img src="components/com_attachments/media/icons/pdf.gif" style="vertical-align: text-bottom; margin: 5px;" /></a><a target="_blank" href="attachments/083_tablas.pdf" title="Tablas de la Binomial y otras">Binomial tabla.</a></td>
<td class="at_description" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: left; border: 1px solid #9a9542;">[Tabla de la Binomial]</td>
<td class="at_file_size" style="padding-top: 2px; padding-right: 3px; padding-bottom: 2px; padding-left: 3px; vertical-align: top; text-align: right; border: 1px solid #9a9542;">369 Kb</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:68:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - PROYECTOS DEL IES";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:0:{}s:6:"module";a:0:{}}
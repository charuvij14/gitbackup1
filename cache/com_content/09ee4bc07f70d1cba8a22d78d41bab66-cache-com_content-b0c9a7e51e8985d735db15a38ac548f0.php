<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7221:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Departamento de Geografía e Historia.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 12 Octubre 2014 09:24</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=165:dep-geografia-historia&amp;catid=102&amp;Itemid=110&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=723e8620075e1c8d4e03d3eec474e2ab81b62298" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 5904
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div>
<h2 style="text-align: center; background-color: #e7eaad;"><strong><span style="text-decoration: underline;"><span style="color: #414141;">GEOGRAFÍA E HISTORIA. CURSO 2014/15</span></span></strong></h2>
<h3><strong><span style="text-decoration: underline;">PROGRAMACIONES.<br /></span></strong></h3>
<p style="padding-left: 30px;"><strong><span style="text-decoration: underline;">ESO</span></strong></p>
<ol style="text-align: justify;">
<li><a href="archivos_ies/14_15/programaciones/geografia/Programacion_1ESO_2014-15.doc" target="_blank"><span style="color: #2a2a2a;"><strong>Programación 1º ESO 2014/15.</strong></span></a></li>
<li><a href="archivos_ies/14_15/programaciones/geografia/Programacion_2ESO_2014-15.doc" target="_blank"><span style="color: #2a2a2a;"><strong>Programación 2º ESO 2014/15.</strong></span></a></li>
<li><a href="archivos_ies/14_15/programaciones/geografia/Programacion_3ESO_2014-15.doc" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #2a2a2a;"><strong><span style="text-decoration: underline;">Programación 3º ESO 2014/15.</span></strong></span></a></li>
<li><a href="archivos_ies/14_15/programaciones/geografia/PROGRAMACION_ASL_3ESO.doc" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #2a2a2a;"><strong><span style="text-decoration: underline;">Programación ASL 3º ESO 2014/15.</span></strong></span></a></li>
<li><a href="archivos_ies/14_15/programaciones/geografia/Programacion_Historia_4ESO2014-2015.doc" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #2a2a2a;"><strong><span style="text-decoration: underline;">Programación de Historia 4º ESO 2014/15.</span></strong></span></a></li>
<li><span style="color: #2a2a2a;"><span><strong><span style="text-decoration: underline;"><a href="archivos_ies/14_15/programaciones/geografia/Programacion_Dicu_4ESO_2014-2015.doc" target="_blank" style="text-decoration: none; color: #2f310d;">Programación de DICU 4º ESO 2014/15.<br /><br />BACHILLERATO<br /><br /></a></span></strong></span></span></li>
<li><a href="archivos_ies/14_15/programaciones/geografia/HMC_2014-2015.docx" target="_blank"><span style="color: #2a2a2a;"><strong>Programación de HMC 1º Bach. 2014/15.</strong></span></a></li>
<li><a href="archivos_ies/14_15/programaciones/geografia/Programcion_GEOGRAFIA_2Bach_2014-15.docx" target="_blank"><span style="color: #2a2a2a;"><strong>Programación de Geografía 2º Bach. 2014/15.</strong></span></a></li>
<li><a href="archivos_ies/14_15/programaciones/geografia/PROGRAMACION_ECONOMIA_14-15.docx" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #2a2a2a;"><strong><span style="text-decoration: underline;">Programación de Economia 1º Bach. 2014/15.</span></strong></span></a></li>
<li><a href="archivos_ies/14_15/programaciones/geografia/PROGRAMACION_ECE_2Bach_14-15.docx" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #2a2a2a;"><strong><span style="text-decoration: underline;">Programación de Economia 2º Bach. 2014/15.</span></strong></span></a></li>
<li><a href="archivos_ies/14_15/programaciones/geografia/Programacion_Antropologia_2014-2015.doc" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #2a2a2a;"><strong><span style="text-decoration: underline;">Programación de Antropología 2º Bach. 2014/15.</span></strong></span></a></li>
<li><span style="color: #2a2a2a;"><span><strong><span style="text-decoration: underline;"><a href="archivos_ies/14_15/programaciones/geografia/Programacion_H_Arte_2Bach.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Programación de Historia del Arte. 2º Bach.<br /><br /></a></span></strong></span></span></li>
</ol>
<p>  </p>
</div>
<div><hr />
<p> </p>
<ul>
<li>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;">12/11/13: Blog personal para Geografía de España de 2º de Bachillerato:</div>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"> </div>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"><a href="http://territoriocervantesgeografiasegundo.blogspot.com/" target="_blank" style="color: #1155cc;">territoriocervantesgeografiasegundo.blogspot.com</a></div>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"> </div>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;">EN CASO DE PROBLEMAS ENTRAR DESDE <a href="http://proxyweb.com.es/browse.php/eDZgubPd/jJTPqrN8/8uxnLC6j/q027EJlv/j2cxusuO/pC78FXxW/a6YsK3MP/kNudrmrm/HgFG/b13/" target="_blank">ESTE ENLACE</a>.</div>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"> </div>
</li>
<li>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;">12/11/13: Blog personal para Geografía e Historia bilingüe</div>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"> </div>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"><a href="http://geographyandhistoryclil.blogspot.com/" target="_blank" style="color: #1155cc;">geographyandhistoryclil.blogspot.com</a></div>
<div style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"> </div>
</li>
<li><a href="http://territorio-cervantes-gr.blogspot.com/" target="_blank" title="Blog del Departamento de Historia." style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;">Blog del Departamento de Historia.</a></li>
</ul>
<div><hr /></div>
</div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:81:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Dpto. de Geografía e Historia";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:25:"Área Socio-Lingüística";s:4:"link";s:59:"index.php?option=com_content&view=article&id=186&Itemid=572";}i:2;O:8:"stdClass":2:{s:4:"name";s:30:"Dpto. de Geografía e Historia";s:4:"link";s:59:"index.php?option=com_content&view=article&id=165&Itemid=110";}}s:6:"module";a:0:{}}
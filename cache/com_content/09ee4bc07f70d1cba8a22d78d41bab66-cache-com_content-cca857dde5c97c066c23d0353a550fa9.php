<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7629:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Presentación</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Presentación</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 01 Junio 2011 06:39</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=146:presentacion&amp;catid=91&amp;Itemid=118&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=fd46a1655a2eb908330d550ab76d997c2622b186" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3003
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><span class="tituloItem"><br /></span></p>
<p><img src="http://www.juntadeandalucia.es/averroes/centros-tic/18700441/helvia/sitio/upload/img/LogodelAMPA.jpg" alt="Logotipo AMPA." title="Logotipo AMPA." border="0" height="62" width="99%" /></p>
<table style="text-align: center;" bgcolor="#d5d9ee" border="0" cellspacing="0" height="832" width="99%">
<tbody>
<tr>
<td><br /></td>
<td style="text-align: center;" class="Estilo18" colspan="3" rowspan="5" valign="top">
<div class="Estilo21">
<p style="text-align: left;">La "<strong>Asociación de Padres de Alumnos del  Instituto de Enseñanza Secundaria Nº3 Cervantes</strong>"   se constituyó el 23  de Marzo de 1992. Esta Asociación, cuyo régimen   viene determinado por lo  dispuesto en sus Estatutos (enlace), carece de   ánimo de lucro y de cualquier  tipo de adoctrinamiento.  Su objetivo    principal es colaborar en la educación de los alumnos como un eslabón   más de la  cadena formada por toda la Comunidad Educativa del Centro.</p>
<div style="text-align: left;">Los <strong>fines</strong> de la Asociación, recogidos en  sus Estatutos son:</div>
<div style="text-align: left;">a) Asistir a los padres, madres o tutores  en todo aquello que concierne a la educación de sus hijos o pupilos.</div>
<div style="text-align: left;">b) Colaborar en las actividades educativas  del Centro y en las actividades complementarias e extraescolares del mismo.</div>
<div style="text-align: left;">c) Organizar actividades culturales y  deportivas.</div>
<div style="text-align: left;">d) Promover la participación de los padres y  madres de los alumnos en la gestión del Centro.</div>
<div style="text-align: left;">e) Asistir a los padres y madres del  alumnado en el ejercicio de su derecho a intervenir en el control y gestión del  Centro.</div>
<div style="text-align: left;">f) Facilitar la representación y  participación de los padres y madres de alumnos en el Consejo Escolar del  Centro.</div>
<div style="text-align: left;">g) Promover el desarrollo de programas de  Educación Familiar.</div>
<div style="text-align: left;">h) Representar a los padres asociados ante  las instancias educativas y otros organismos.</div>
<div style="text-align: left;">i)  Promover la convivencia entre los  padres  y madres y entre éstos y el  profesorado, mediante la puesta en  marcha de  actividades  específicamente destinadas a ello.</div>
<div style="text-align: left;">j)  Defender colectivamente los derechos y   deberes que a la familia y a  sus miembros corresponden en asuntos  relacionados  con la educación.</div>
<div style="text-align: left;">Para la consecución  de estos fines la Asociación fomenta y organiza, de modo genérico, los  siguientes tipos de <strong>actividades</strong>:</div>
<div style="text-align: left;">a)  Planificar las actividades   complementarias y extraescolares que no son  competencia del Consejo  Escolar, o  las que, siendo competencia de  éste se desarrollan en  colaboración.</div>
<div style="text-align: left;">b)  Planificar y colaborar en la puesta en   marcha y funcionamiento de  diferentes servicios escolares que  repercutan en el  bien del alumnado.</div>
<div style="text-align: left;">c) Asesorar a padres y madres del alumnado  sobre las formas de participación en los órganos de gestión del Centro.</div>
<div style="text-align: left;">d) Procurar la celebración de cursos,  conferencias y coloquios de orientación y formación para los padres y madres.</div>
<div style="text-align: left;">e) Actuar  en forma legal, en su ámbito   educativo y territorial, ante las  autoridades u organismos civiles u  otros,  para defender los derechos  de las familias asociadas, y también  para defender  los derechos del  Centro cuando proceda y no vayan contra  los de la Asociación.</div>
<p style="text-align: left;">El  órgano  supremo de gobierno de la  Asociación es la Asamblea General. Se reúne   obligatoriamente una vez al  año, durante el primer trimestre del curso,  en  octubre o noviembre, en  asamblea anual ordinaria.  Normalmente se  presenta un informe de las   actividades del curso anterior, un proyecto  de las actividades previstas  para  el curso en marcha y se aprueban  las cuentas de la asociación,  incluida la  cuota anual (18 € por  familia).</p>
<p style="text-align: left;">Cada  dos  años la Asamblea nombra una  nueva Junta Directiva (enlace). En la  actualidad  está compuesta por  presidente, vicepresidente, secretario,  tesorero y 7 vocales  y ha sido  renovada en el curso 2009-10.</p>
<p style="text-align: left;">La  composición y funcionamiento de la Asamblea General y la Junta Directiva están  detalladas en los <a href="http://www.juntadeandalucia.es/averroes/centros-tic/18700441/helvia/sitio/upload/Estatutos.pdf" target="_blank">Estatutos de la asociación.</a></p>
<p style="text-align: left;">Esta  página, además de informativa, pretende ser un llamamiento para animarnos a <strong>colaborar</strong>. La calidad de la enseñanza  pública también es también "nuestra" responsabilidad.</p>
<p style="text-align: left;">Las formas  de colaborar son  muchas:  desde preparar pequeños eventos, organizar y  colaborar con el instituto   en diversas actividades para alumnos o  padres/madres  o implicarse en  la labor cotidiana de la  Asociación.  Hay todo tipo de tareas,  para los  que disponen de muy poco tiempo y   para aquellos que estén dispuestos a  una mayor dedicación.</p>
<p style="text-align: left;">Y, por supuesto, se  admiten todo tipo de ideas, propuestas o sugerencias.</p>
</div>
</td>
</tr>
</tbody>
</table>
<p> </p>
<hr /></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">AMPA</span> / <span class="art-post-metadata-category-name">Presentación</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:64:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Presentación";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:4:"AMPA";s:4:"link";s:59:"index.php?option=com_content&view=article&id=147&Itemid=117";}i:1;O:8:"stdClass":2:{s:4:"name";s:13:"Presentación";s:4:"link";s:59:"index.php?option=com_content&view=article&id=146&Itemid=118";}}s:6:"module";a:0:{}}
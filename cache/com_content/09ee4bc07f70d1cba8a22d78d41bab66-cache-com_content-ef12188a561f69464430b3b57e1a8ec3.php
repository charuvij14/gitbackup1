<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:4929:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Premios de poesía</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">PREMIOS DE POESÍA</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 16 Diciembre 2012 09:58</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=179:premios-poesia&amp;catid=199&amp;Itemid=84&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=a80d9e056180078fb82b55cdbda2e1377a1b73d0" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2258
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p>&nbsp;</p>
<div style="float: left; display: block; width: 100%;" id="contenido_sitio"><span style="text-decoration: underline;"><strong><span style="font-family: 'Times New Roman'; font-size: medium;">PREMIOS DEL CERTAMEN LITERARIO</span></strong></span></div>
<div style="float: left; display: block; width: 100%;">&nbsp;</div>
<div style="float: left; display: block; width: 100%;"><span style="text-decoration: underline;"><strong><span style="font-family: 'Times New Roman'; font-size: medium;"> </span> </strong></span>
<p style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: medium; font-weight: bold;" class="Estilo8">DATOS DE LOS AUTORES PREMIADOS:</p>
<p style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: medium; font-weight: bold;" class="Estilo8">&nbsp;</p>
<ul>
<li style="font-family: 'Times New Roman'; font-size: medium;"><a href="archivos_ies/actividades/docs/Certamen_literario_para_la_web/El_monstruo_de_Londres_de_Ryan.doc">Título del relato:&nbsp;<em>El monstruo de Londres.</em></a><em> </em></li>
</ul>
<p style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: medium; font-weight: bold;" class="Estilo8">- Nombre y apellidos: Ryan Acosta Babb.</p>
<p style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: medium; font-weight: bold;" class="Estilo8">- Curso: 1º ESO A.</p>
<p style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: medium; font-weight: bold;" class="Estilo8">- Primer Premio de la modalidad narrativa en el ámbito andaluz en el V Certamen Literario del IES Profesor Isidoro Sánchez de Málaga.</p>
<p style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: medium; font-weight: bold;" class="Estilo8">&nbsp;</p>
<ul>
<li style="font-family: 'Times New Roman'; font-size: medium;"><a href="archivos_ies/actividades/docs/Certamen_literario_para_la_web/Viajad_y _sed_felices_de_Carlos.doc">Título del relato:&nbsp;<em>Viajad y sed felices.</em></a><em>&nbsp;</em></li>
</ul>
<p style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: medium; font-weight: bold;" class="Estilo8">- Nombre y apellidos: Carlos Fernando Luna Olivares.</p>
<p style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: medium; font-weight: bold;" class="Estilo8">- Curso: 1º ESO A.</p>
<p style="color: #000033; font-family: 'Times New Roman',Times,serif; font-size: medium; font-weight: bold;" class="Estilo8">- Mención especial del jurado de la modalidad narrativa en el ámbito provincial en el V Certamen Literario del IES Profesor Isidoro Sánchez de Málaga.</p>
<span style="font-family: 'Times New Roman'; font-size: medium;"> </span></div>
<div id="separadorContenido" class="separadorContenido">&nbsp;</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Actividades</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:69:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Premios de poesía";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:18:"Premios de poesía";s:4:"link";s:58:"index.php?option=com_content&view=article&id=179&Itemid=84";}}s:6:"module";a:0:{}}
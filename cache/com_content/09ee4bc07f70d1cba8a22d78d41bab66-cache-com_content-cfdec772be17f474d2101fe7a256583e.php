<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9787:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">ESO</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 05 Diciembre 2012 10:24</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=49:normativa-de-eso&amp;catid=104&amp;Itemid=562&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=11a16c5e2094085210c5c28055f9f6a48c06a53e" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2409
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><table cellpadding="0" cellspacing="0" border="0" width="100%">
<tbody>
<tr>
<td style="font: normal normal normal 14px/normal georgia; color: #cc3300; font-weight: bold; letter-spacing: 0.1em; line-height: 26px; padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;" class="pageName">Legislación educativa andaluza y española de ámbito estatal en vigor en Andalucía</td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333; line-height: 24px;" class="bodyText">
<table cellspacing="1" border="0" width="98%">
<tbody>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" bgcolor="#9AB2CE"><span class="Estilo2">Educación Secundaria</span></td>
</tr>
<tr>
<td style="font: normal normal normal 11px/normal arial; color: #333333;" class="listado"><ol>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden17marzo2011modificaOrdenesEvaluacion.pdf">ORDEN de 17 de marzo de 2011</a>, por la que se modifican las Órdenes que establecen la ordenación de la evaluación en las etapas de educación infantil, educación primaria, educación secundaria obligatoria y bachillerato en Andalucía (BOJA 04-04-2011).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/OrdenEDU9sept2009promocioncursoLOGSE-LOE.pdf">ORDEN EDU/2395/2009</a>, de 9 de septiembre, por la que se regula la promoción de un curso incompleto del sistema educativo definido por la Ley Orgánica 1/1990, de 3 de octubre, de ordenación general del sistema educativo, a otro de la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 12-09-2009).</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/20090122pruebasesomayores18.pdf">ORDEN de 8-1-2009</a>, por la que se regulan las pruebas para la obtención del título de Graduado en Educación Secundaria Obligatoria, para personas mayores de dieciocho años.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc%2017-12-2007%20Evaluacion%20ESO.pdf">INSTRUCCIONES de 17-12-2007,</a> de la Dirección General de Ordenación y Evaluación Educativa, por la que se complementa la normativa sobre evaluación del proceso de aprendizaje del alumnado de Educación Secundaria Obligatoria.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/instruc/Aclaracion%204-10-2007%20Acceso%20ESA2.pdf">Aclaraciones de 4-10-2007,</a> de la Directora General de Formación Profesional y Educación Permanente, sobre el acceso del alumnado al Nivel II de las enseñanzas de Educación Secundaria Obligatoria para las personas adultas.</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2010-8-2007%20Regula%20ESA.pdf">ORDEN de 10-8-2007,</a> por la que se regula la Educación Secundaria Obligatoria para Personas Adultas. (BOJA 31-8-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2010-8-2007%20Curriculo%20Secundaria.pdf">ORDEN de 10-8-2007,</a> por la que se desarrolla el currículo correspondiente a la Educación Secundaria Obligatoria en Andalucía. (BOJA 30-8-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2010-8-2007%20Evaluacion%20Secundaria.pdf">ORDEN de 10-8-2007,</a> por la que se establece la ordenación de la evaluación del proceso de aprendizaje del alumnado de educación secundaria obligatoria en la Comunidad Autónoma de Andalucía. (BOJA 23-8-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto%20231-2007%20Ensenanzas%20Secundaria.pdf">DECRETO 231/2007, de 31 de julio,</a> por el que se establece la ordenación y las enseñanzas correspondientes a la educación secundaria obligatoria en Andalucía. (BOJA 8-8-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%206-6-2007%20Curriculum%20Religion.pdf">ORDEN ECI/1957/2007, de 6 de junio,</a> por la que se establecen los currículos de las enseñanzas de religión católica correspondientes a la educación infantil, a la educación primaria y a la educación secundaria obligatoria. (BOE 3-7-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2019-6-2007%20Documentos%20Evaluacion.pdf">ORDEN ECI/1845/2007, de 19 de junio,</a> por la que se establecen los elementos de los documentos básicos de evaluación de la educación básica regulada por la Ley Orgánica 2/2006, de 3 de mayo, de Educación, así como los requisitos formales derivados del proceso de evaluación que son precisos para garantizar la movilidad del alumnado. (BOE 22-6-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%2015-1-2007%20Inmigrantes%20ATAL.pdf">ORDEN de 15-1-2007,</a> por la que se regulan las medidas y actuaciones a desarrollar para la atención del alumnado inmigrante y, especialmente, las Aulas Temporales de Adaptación Lingüística. (BOJA 14-2-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/rdecre/RD%201631-2006%20Ensenanzas%20ESO.pdf">REAL DECRETO 1631/2006, de 29 de diciembre,</a> por el que se establecen las enseñanzas mínimas correspondientes a la Educación Secundaria Obligatoria. (BOE 5-1-2007)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden%205-9-2006%20Funcion%20Directiva.pdf">ORDEN de 5-9-2006,</a> por la que se amplían las horas de función directiva a los Directores y Directoras de los Institutos de Educación Secundaria y Centros de Enseñanza de Régimen Especial, a excepción de los Conservatorios Elementales de Música. (BOJA 22-9-2006)</li>
<li style="padding-top: 0px; padding-right: 0px; padding-bottom: 10px; padding-left: 0px;"><a style="color: #005fa9; text-decoration: none;" target="_blank" href="http://www.adideandalucia.es/normas/ordenes/orden%2011-5-2006%20gastos%20funcionamiento.pdf">ORDEN de 11-5-2006,</a> conjunta de las Consejerías de Economía y Hacienda y de Educación, por la que se regula la gestión económica de los fondos con destino a inversiones que perciban con cargo al presupuesto de la Consejeríade Educación los centros docentes públicos de educación secundaria, de enseñanzas de régimen especial a excepción de los Conservatorios Elementales de Música, y las Residencias Escolares, dependientes de la Consejería de Educación. (BOJA 25-5-2006)</li>
</ol></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:54:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - ESO";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:7:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:16:"Área Artística";s:4:"link";s:59:"index.php?option=com_content&view=article&id=184&Itemid=570";}i:2;O:8:"stdClass":2:{s:4:"name";s:29:"Dpto. de Educación Plástica";s:4:"link";s:57:"index.php?option=com_content&view=article&id=76&Itemid=98";}i:3;O:8:"stdClass":2:{s:4:"name";s:14:"DIBUJO 4º ESO";s:4:"link";s:58:"index.php?option=com_content&view=article&id=75&Itemid=562";}i:4;O:8:"stdClass":2:{s:4:"name";s:9:"Normativa";s:4:"link";s:60:"index.php?option=com_content&view=category&id=196&Itemid=562";}i:5;O:8:"stdClass":2:{s:4:"name";s:17:"ÚLTIMA NORMATIVA";s:4:"link";s:60:"index.php?option=com_content&view=category&id=104&Itemid=562";}i:6;O:8:"stdClass":2:{s:4:"name";s:3:"ESO";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
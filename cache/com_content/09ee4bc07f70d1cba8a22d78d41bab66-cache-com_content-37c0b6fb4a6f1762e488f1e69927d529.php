<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6081:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">2º BACH A</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">PROYECTO INTEGRADO 2º BACH A</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 01 Febrero 2013 07:08</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=65:proyecto-integrado-2o-bach-a&amp;catid=40&amp;Itemid=245&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=0cfe88f14af71d3ae564ba7bc2b2ddad7a775473" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 5565
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><strong>PÁGINA PRINCIPAL DEL PROYECTO INTEGRADO DE 2º BACHILLERATO A. CURSO 2012/13.</strong></p>
<hr />
<p> </p>
<p><span style="text-decoration: underline;"><strong>GRUPOS Y TRABAJOS:</strong></span></p>
<p> </p>
<table border="0">
<tbody>
<tr>
<td>1.</td>
<td>
<p><strong>MARTÍN GARCÍA FERNÁNDEZ.</strong></p>
<p><strong>ARTURO MARTÍNEZ CALVENTE.</strong></p>
</td>
<td>
<p><span style="text-decoration: underline;"><strong>1ª EVA:</strong></span> TECNOLOGÍA Y MATEMÁTICAS. Presentación.</p>
<p><span style="text-decoration: underline;"><strong>2ª EVA:</strong></span> MATEMÁTICAS APLICADAS A LA GUERRA.</p>
</td>
</tr>
<tr>
<td style="text-align: left;">2.</td>
<td style="text-align: left;">
<p><strong>GUILLERMO QUEVEDO ORTIZ.</strong></p>
<p><strong>IRENE LABRA GARCÍA.</strong></p>
</td>
<td>
<p><span style="text-decoration: underline;"><strong>1ª EVA:</strong></span> ASTRONOMÍA. Presentación.</p>
<p><span style="text-decoration: underline;"><strong>2ª</strong><strong> EVA:</strong></span> ANIMALES EXTINGUIDOS Y EN PELIGRO DE EXTINCIÓN.</p>
</td>
</tr>
<tr>
<td>3.</td>
<td>
<p><strong>MARTA ÁLVAREZ ZAPATA.</strong></p>
<p><strong>ROSA Mª CORTÉS MARTÍN.</strong></p>
<p><strong>SASKIA Mª FERNÁNDEZ PÉREZ DE NAVARRO.</strong></p>
<p><strong>ÁNGELA LARA GÓMEZ.</strong></p>
<p><strong>RAQUEL RUIZ SOLA.</strong></p>
</td>
<td>
<p><span style="text-decoration: underline;"><strong>1ª EVA:</strong></span> SOBRE EL DESCUIDO Y ABANDONO DE LOS ANIMALES.</p>
<p><strong><span style="text-decoration: underline;">2ª EVA:</span></strong> SUEÑOS.</p>
</td>
</tr>
<tr>
<td>4.</td>
<td>
<p><strong>CARLOS MERINO MARTÍN-PEÑASCO.</strong></p>
<p><strong>SERGIO NAVARRO GÓMEZ.</strong></p>
<p><strong>ALICIA BELTRÁN ANTEQUERA.</strong></p>
<p><strong>MARÍA FERNÁNDEZ NEVADO.</strong></p>
</td>
<td>
<p><span style="text-decoration: underline;"><span style="color: #000000; text-decoration: underline;"><strong>1ª EVA:</strong></span></span><span style="color: #000000;">COMPETENCIA APPLE VS. SAMSUNG. Presentación.</span></p>
<p><span style="color: #000000;"><span style="text-decoration: underline;"><strong>2ª</strong><strong> EVA:</strong></span> EL DEPORTE ESPAÑOL.<br /></span></p>
</td>
</tr>
<tr>
<td>5.</td>
<td>
<p><strong>HERMES RODADO DELGADO.</strong></p>
<p><strong>BELÉN CABRERA COBOS.</strong></p>
<p><strong>JORGE DURÁN LLANOS.</strong></p>
<p><strong>Mª ÁNGELES CABRERA FERNÁNDEZ.</strong></p>
<p><strong>MARTA Mª BLÁZQUEZ MARTÍN.</strong></p>
</td>
<td>
<p><span style="text-decoration: underline;"><strong><span style="color: #000000; text-decoration: underline;">1ª EVA:</span></strong></span><span style="color: #000000;">STEPHEN HAWKING. 6 págs.</span></p>
<span style="color: #000000;"> <span style="text-decoration: underline;"><strong>2ª EVA:</strong></span> LA MÚSICA. </span></td>
</tr>
<tr>
<td>6.</td>
<td>
<p><strong>ANA HUETE GALERA.</strong></p>
<p><strong>YLENIA RODRÍGUEZ FERNÁNDEZ.</strong></p>
<p><strong>NAZARET MARTÍNEZ GÓMEZ.</strong></p>
<p><strong>IVÁN FERNÁNDEZ DEL HOYO.D</strong></p>
</td>
<td>
<p><span style="text-decoration: underline;"><strong><span style="color: #000000; text-decoration: underline;">1ª EVA:</span></strong></span><span style="color: #000000;">DROGAS. 12+19págs.</span></p>
<span style="color: #000000;"><strong>  <span style="text-decoration: underline;">2ª EVA:</span></strong> VÍDEO: LA JUVENTUD: OCIO, ESTUDIO, ETC.</span></td>
</tr>
<tr>
<td>7.</td>
<td>
<p><strong>IRENE CENTENO HITA.</strong></p>
<p><strong>LAURA MARTÍN LÓ</strong></p>
</td>
<td>
<p><span style="text-decoration: underline;"><strong><span style="color: #000000; text-decoration: underline;">1ª EVA:</span></strong></span>LEONARDO DA VINCI</p>
<span style="color: #000000;"> <span style="text-decoration: underline;"><strong>2ª EVA:</strong></span> FREUD.</span></td>
</tr>
<tr>
<td> </td>
<td>
<p> </p>
</td>
<td> </td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Proyectos</span> / <span class="art-post-metadata-category-name">PROYECTOS</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:61:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - 2º BACH A";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:13:"PROYECTOS ==>";s:4:"link";s:59:"index.php?option=com_content&view=categories&id=0&Itemid=28";}i:2;O:8:"stdClass":2:{s:4:"name";s:20:"PROYECTOS INTEGRADOS";s:4:"link";s:72:"index.php?option=com_content&view=category&layout=blog&id=229&Itemid=244";}i:3;O:8:"stdClass":2:{s:4:"name";s:10:"2º BACH A";s:4:"link";s:58:"index.php?option=com_content&view=article&id=65&Itemid=245";}}s:6:"module";a:0:{}}
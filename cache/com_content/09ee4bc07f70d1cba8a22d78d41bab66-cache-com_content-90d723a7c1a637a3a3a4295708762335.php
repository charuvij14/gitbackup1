<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:154458:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Artículos</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Nueva web del Departamento de F.I.E.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 31 Marzo 2014 15:06</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=242:web-del-departamento-de-f-i-e&amp;catid=100&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=db66b60141998fcb4f3e49cf53cd50bc65b5200b" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3277
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p>Nueva web del <a href="fie/index.html" target="_blank"><span style="background-color: #ffffff; color: #000000; font-family: Calibri, sans-serif; font-size: 16px; line-height: 23px; text-align: justify;">Departamento de Formación, Evaluación e Innovación Educativa.</span></a></p>
<p><a href="fie/index.html"><span style="background-color: #ffffff; color: #000000; font-family: Calibri, sans-serif; font-size: 1px; line-height: 3px; text-align: justify;"><img src="fie/images/fie.png" border="0" alt="Captura de pantalla." width="605" height="287" /></span></a></p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">DEPARTAMENTOS</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Programaciones</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 06 Noviembre 2015 18:43</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=195:programaciones&amp;catid=100&amp;Itemid=666&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=ee46c6b6ac83bb260ae9dae98e45ea74775792ec" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 33483
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 style="text-align: center;"><span style="font-size: 16pt;"><strong><span style="text-decoration: underline;"><br />Programaciones ABREVIADAS, criterios de evaluación, recuperaciones. </span></strong></span></h2>
<h2 style="text-align: center;"><span style="font-size: 16pt;"><strong><span style="text-decoration: underline;">curso 2015/16</span></strong><br /></span></h2>
<h3><span style="text-decoration: underline;"><strong>BIOLOGÍA - GEOLOGÍA 15/16</strong></span></h3>
<ul style="text-align: justify;">
<li><a href="archivos_ies/15_16/programaciones/BG_Programaciones_2015-2016-resumida.pdf" target="_blank"><span style="color: #2a2a2a;">Resumen de las programaciones del Departamento. </span></a><span style="color: #2a2a2a;">(03/11/15)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;"><strong>CICLO DE ANIMACIÓN</strong></strong></span><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;"><strong><br /></strong></strong></p>
<ul>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Progr_OTL_14-15_EMT.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN OTL 14-15 EMT</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Prog_PROYECTO_14_15_EMT.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN PROYECTO 14-15 EMT</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Prog_Modulo_FCT_2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN MÓDULO FCT 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_SSA.doc" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN SSA 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_FCT.docx" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN FCT 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_DCO.docx" target="_blank" style="text-decoration: none; color: #2f310d;">ProgramacióN DCO 14-15</a></span><span style="color: #7b7b7b;"> </span><span style="color: #7b7b7b;"><span style="color: #414141;">(20/10/14)</span></span></li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_MIS.docx" target="_blank" style="text-decoration: none; color: #2f310d;">ProgramacióN MIS 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Prgrogramacion_Departamento_ASC.docx" target="_blank" style="text-decoration: none; color: #2f310d;">PROGRAMACIÓN DEP. ASC 14-15</a> (20/10/14)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">EDUCACIÓN FÍSICA 15/16<br /></strong></span></p>
<ul>
<li><a href="archivos_ies/15_16/ef/EF_1ESO.doc" target="_blank">Cuadernillo programación 1º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_2ESO.doc" target="_blank">Cuadernillo programación 2º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_3ESO.doc" target="_blank">Cuadernillo programación 3º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_4ESO.doc" target="_blank">Cuadernillo programación 4º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_1_Bachillerato.doc" target="_blank">Cuadernillo programación 1º BACH.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_2_Bachillerato.doc" target="_blank">Cuadernillo programación 2º BACH.</a> (06/10/15)</li>
</ul>
<h3><span style="text-decoration: underline;"><strong>EDUCACIÓN PLÁSTICA:</strong></span></h3>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/15_16/programaciones/">Programación reducida.</a> (13/10/14)<br /></span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/programaciones/ProgramacionDT2_Bach_2014-2015.pdf" target="_blank">Programación de Dibujo Técnico 2º Bachillerato.</a> (23/10/14)<br /><br /></span></li>
</ul>
<p><span style="line-height: 1.25em;"> </span><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">FILOSOFÍA</strong></span></p>
<ul>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/FIL_RESUMEN_CRITERIOS_EV_Y_CALIF.pdf" target="_blank">Programación</a> (03/11/15)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>FÍSICA QUÍMICA.</strong></span></h3>
<ul>
<li><a href="archivos_ies/15_16/programaciones/FQ_RESUMEN_3_ESO.pdf" target="_blank">Resumen de la programación de FQ 3º ESO.</a> <a href="archivos_ies/criterios_evaluacion/Cuadernillo_FQ_3_ESO.pdf" target="_blank" title="Criterios de evaluación de FQ 3º ESO"><span style="color: #414141; letter-spacing: normal; line-height: 16.25px; text-align: left;">(03/11/15)</span></a></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_RESUMEN_4_ESO.pdf" target="_blank">Resumen de la programación de FQ 4º ESO.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_RESUMEN_1_Bach.pdf" target="_blank">Resumen de la programación de FQ 1º Bach.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_FIS_RESUMEN_2_Bach.pdf" target="_blank">Resumen de la programación de Física 2º Bach.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_QU_RESUMEN_2_Bach.pdf" target="_blank">Resumen de la programación de Química 2º Bach.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase;"><span style="text-decoration: underline;">FRANCÉS</span></strong></p>
<ul>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_ESO.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º ESO</span><span><span style="letter-spacing: 1px; line-height: 16.25px;"><span>.</span></span></span></a><span style="line-height: 16.25px;"> (06/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen2_ESO.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 2º ESO</span><span><span style="letter-spacing: 1px; line-height: 16.25px;"><span>.</span></span></span></a><span style="line-height: 16.25px;"> (06/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen3_ESO_FR2.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 3º ESO FR2</span><span><span style="letter-spacing: 1px; line-height: 16.25px;"><span>.</span></span></span></a><span style="line-height: 16.25px;"> (06/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen3_ESOPrimerLenguaExtranj.pdf" target="_blank">Resumen programación 3º ESO Primera Lengua Extranjera.</a> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen4_ESO.pdf" target="_blank">Resumen programación 4º ESO.</a> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachFR2NAvance.pdf" target="_blank"><span style="line-height: 16.25px;">Resumen programación 1º Bach. </span><span style="color: #8e9326;"><span style="letter-spacing: 1px; line-height: 16.25px;"><span style="text-decoration: underline;">FR2 N. Avance.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachFR2NDebut.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º Bach. F</span><span style="color: #8e9326;"><span style="text-decoration: underline;"><span style="line-height: 16.25px;">R2 N. Debut.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachFR2NIntermed.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º Bach. </span><span><span><span style="line-height: 16.25px;">FR2 N. Intermed.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachLenguaExtranI.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º Bach. </span><span style="color: #8e9326;"><span style="letter-spacing: 1px; line-height: 16.25px;"><span style="text-decoration: underline;">Lengua Extranera 1.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen2_BachFR2.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="text-decoration: none; color: #2f310d; line-height: 16.25px;">Resumen programación 2º Bach. 2ª </span><span style="text-decoration: none; color: #8e9326;"><span style="line-height: 16.25px;"><span style="text-decoration: underline;">Lengua Extranera</span></span></span></a><span style="color: #8e9326;"><span style="text-decoration: underline;">.</span></span><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen2_BachLenguaExtranII.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="line-height: 16.25px;">Resumen programación 2º Bach. </span><span style="color: #8e9326;"><span style="line-height: 16.25px;"><span style="text-decoration: underline;">Lengua Extranera 2.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
</ul>
<div style="text-align: justify;">
<h3><strong><span style="text-decoration: underline;">GEOGRAFÍA E HISTORIA.<br /></span></strong></h3>
<ol>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/15_16/programaciones/Historia_PRIMERO_DE_ESO.docx" target="_blank">Programación 1º ESO</a>.</span><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/SEGUNDO_DE_ESO.pdf" target="_blank"><span style="color: #2a2a2a;">Programación 2º ESO.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Historia_TERCERO_DE_ESO.docx" target="_blank"><span style="color: #2a2a2a;">Programación 3º ESO.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Historia_CUARTO_DE_ESO.pdf" target="_blank"><span style="color: #2a2a2a;">Programación 4º ESO.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/HISTORIA_MUNDO_CONTEMPORANEO.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #2a2a2a;">Programación Hª del mundo Contemporáneo 1º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/GEOGRAFIA.pdf" target="_blank"><span style="color: #2a2a2a;">Programación Geografía 2º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/HISTORIA_DE_ESPANA.pdf" target="_blank"><span style="color: #2a2a2a;">Programación Hª de España 2º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Programacion_H_Arte_2Bach1516.pdf" target="_blank"><span style="color: #2a2a2a;">Programación Arte 2º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/Programacion_didactica_CEE.docx" target="_blank">Programación Cultura Emprendedora y Empresarial 1º Bach.</a> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/PROGRAMACION_ECONOMIA%20.docx" target="_blank" style="text-decoration: none; color: #2f310d;">Programación Economía 1º Bach.</a> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/PROGRAMACION_DIDACTICA_ECE%20.docx" target="_blank">Programación Economía de la Empresa 2º Bach.</a> (03/11/15)</span></li>
</ol></div>
<p><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase;"><span style="text-decoration: underline;">INGLÉS</span></strong></p>
<ul>
<li><a href="archivos_ies/14_15/programaciones/Programacion_ingles_2014-2015.pdf" target="_blank" title="Programación de inglés, 14/15." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none; color: #336600; font-size: 11px;">Programación de Inglés, curso 2014/15 </a><span style="line-height: 1.25em;">(02/10/14)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-transform: uppercase; text-decoration: underline;">LATÍN Y GRIEGO</strong> </span></p>
<ul>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/programacion_reducida_latin.pdf" target="_blank">Programación de Latín</a>. </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/programacion_reducida_griego.pdf" target="_blank">Programación de Griego.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-transform: uppercase; text-decoration: underline;">LENGUA ESPAÑOLA</strong></span></p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/programaciones/programacion_Lengua_curso_2014-2015.pdf/archivos_ies/15_16/programaciones/LE_ABREVIADA%202015-16.docx" target="_blank">Programación abreviada de Lengua</a> <span style="line-height: 16.25px;">(03/11/15)</span><br /><br /></li>
</ul>
</ul>
<p> <strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-transform: uppercase; text-decoration: underline;">M<strong>ATEMÁ</strong>TICAS.</strong></p>
<ul style="text-align: justify;">
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_ESO.doc" target="_blank">Resumen de  la programación 1º ESO</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_ESO.doc">Resumen de  la programación. 2<span style="color: #423c33;"><span style="color: #2f310d;">º ESO</span></span></a></span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_3_ESO.doc" target="_blank">Resumen de  la programación. 3<span style="color: #423c33;"><span style="color: #2f310d;">º ESO</span></span></a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_4_ESO.doc" target="_blank"> Resumen de  la programación. 4<span style="color: #423c33;"><span style="color: #2f310d;">º ESO</span></span></a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_padres_ESO_BILIN_15_16.docx" target="_blank">Resumen de  la programación. BILINGÜES</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_Y_T.doc" target="_blank">Resumen de  la programación. </a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_Y_T.doc" target="_blank">1º BACHILLERATO C. Y T.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_SOC.doc" target="_blank">Resumen de  la programación. </a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_SOC.doc" target="_blank">1º BACHILLERATO C. SOC.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_II.doc" target="_blank">Resumen de  la programación. 2</a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_II.doc" target="_blank">º BACHILLERATO MAT II</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_CC_SS_II..doc" target="_blank">Resumen de  la programación. 2</a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_CC_SS_II..doc" target="_blank">º BACHILLERATO C. SOC.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_padres_ESTAD_2BACH_15_16.docx" target="_blank">Resumen de  la programación. 2</a></span><span style="color: #423c33;"><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_padres_ESTAD_2BACH_15_16.docx" target="_blank">º BACHILLERATO ESTADÍSTICA</a> </span></span><strong style="color: #2b2721;"><span style="color: #423c33;"><span style="color: #414141; font-weight: normal; line-height: 16.25px;">(03/11/15)</span></span></strong></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">MÚSICA:</strong></span></p>
<ul style="text-align: justify;">
<li style="text-align: justify;"><span style="color: #8e9326;"><span style="letter-spacing: 1px;"><a href="archivos_ies/15_16/programaciones/MUS_Objetivos_Contenidos_Criterios_Ev1516.pdf" target="_blank">Objetivos, Contenidos y Criterios de Evaluación_2015/16</a> <strong><span style="color: #414141; letter-spacing: normal; text-align: left;"> </span></strong></span></span><span style="line-height: 16.25px; text-align: left;">(03/11/15)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>PLAN DE IGUALDAD</strong></span><span style="color: #2a2a2a; font-size: 13px; font-weight: normal;"> </span></h3>
<ul style="text-align: justify;">
<li><span style="line-height: 1.25em;"><a href="archivos_ies/14_15/programaciones/PROYECTO_PLAN_DE_IGUALDAD_14-15_Jennifer_Valero.pdf" target="_blank">Programación</a> (20/11/14)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>PROGRAMACIÓN BILINGÜE</strong></span><span style="color: #2a2a2a; font-size: 13px; font-weight: normal;"> </span></h3>
<ul style="text-align: justify;">
<li><span style="line-height: 1.25em;"><a href="archivos_ies/14_15/programaciones/Programacion_bilingue_2014-2015b.docx" target="_blank">Programación</a> (13/10/14)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>RELIGIÓN</strong></span></h3>
<ul style="text-align: justify;">
<li><span style="line-height: 1.25em;"><a href=":\Users\Manuel\Documents\0Cervantes15-16\Programaciones/Rel_PROGR_2015_2016.pdf" target="_blank">Programación</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">Tecnología</strong></span></p>
<ul>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologias_2_eso.pdf" target="_blank">Criterios de evaluación Tecnologías 2º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologias_3_eso.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Criterios de evaluación Tecnologías 3º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologia_4_eso.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Criterios de evaluación Tecnologías 4º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologia_1_bach.pdf" target="_blank">Criterios de evaluación Tecnologías 1º Bach.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologia_2_bach.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Criterios de evaluación Tecnologías 2º Bach.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Criterios_evaluacion_3_4_optativa_Informatica_15-16.pdf" target="_blank">Criterios de evaluación Informática 3º y 4º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/Criterios_evaluacion_3_4_optativa_Informatica_15-16.pdf" target="_blank">PROGRAMACIÓN DIDÁCTICA DE INFORMÁTICA 3º Y 4º ESO.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/Programacion_TICBachillerato_15-16.pdf" target="_blank">PROGRAMACIÓN DIDÁCTICA TIC BACHILLERATO.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p> </p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">DEPARTAMENTOS</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Departamento de Matemáticas.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 02 Diciembre 2015 13:29</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=51:dep-matematicas&amp;catid=100&amp;Itemid=103&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=5f4a328d138dbb359876b7e78eceba6cf7511390" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 51952
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; background-color: #ffffff; text-align: justify;"><strong style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><span style="text-decoration: underline;">ENLACES DIRECTOS A LAS SIGUIENTES SECCIONES:</span></strong></h3>
<h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; background-color: #ffffff; text-align: justify;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><a href="index.php?option=com_content&amp;view=article&amp;id=137&amp;Itemid=724" target="_blank">2º Eso</a><strong><span style="text-decoration: underline;"><br /></span></strong></span><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><a href="index.php?option=com_content&amp;view=article&amp;id=93&amp;Itemid=103" target="_blank" title="MAT. I">1º BACH. MAT.I</a><br /><span style="color: #5d5d5d; font-size: 20px; text-align: left; text-transform: uppercase;"><a href="http://matepaco.blogspot.com.es/p/bachillerato-ccss-i.html" target="_blank">1º BACH. MAT. CC.SS I</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=154&amp;Itemid=103" target="_blank" title="MAT. II">2º BACH. MAT. II</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=94&amp;Itemid=103" target="_blank" title="MAT. CC.SS. II">2º BACH. MAT. CC.SS II</a><br /><a href="index.php?option=com_content&amp;view=article&amp;id=103&amp;Itemid=103" target="_blank" title="MAT. CC.SS. II">2º BACH. ESTADÍSTICA<br /></a><a href="index.php?option=com_content&amp;view=article&amp;id=236&amp;Itemid=103" target="_blank" title="PENDIENTES.&quot;">PENDIENTES DE TODOS LOS CURSOS<br /><br /></a><a href="http://matepaco.blogspot.com.es/" target="_blank">UN BUEN bLOG DE MATEMÁTICAS</a></span></span></h3>
<p> </p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: justify; background-color: #ffffff;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><strong><span style="text-decoration: underline;"><span style="color: #ff0000; text-decoration: underline;">novedad:</span></span> <br /></strong>este curso 2014/15 contará con un grupo de estadística en 2º bachillerato cns.<span style="text-decoration: underline;"><br /><br />CURSO 2014/15<br /><br /></span><strong style="color: #2a2a2a; font-size: 13px; text-transform: none; background-color: #ffffff;"><span>IMPORTANCIA DE ELEGIR ESTADÍSTICA COMO OPTATIVA EN 2º BACHILLERATO CIENCIAS<br /><br /></span></strong><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">El alumnado de Ciencias puede subir la nota de Selectividad en la Fase Específica mediante la realización de pruebas de distintas materias. En particular las Matemáticas II suele puntuar con una ponderación de 0,2 en la mayoría de los Grados de Ciencias (subir hasta 2 puntos la nota de la Fase General). El problema es que muchos alumnos se examinan de Matemáticas II en la Fase General al no disponer de otra materia mejor, según sus intereses.<br /></span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Es por ello que el Departamento de Matemáticas informa que este inconveniente se puede subsanar si el alumnado se examinara de Matemáticas CC SS II en la Fase General y de </span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Matemáticas II en la Fase Específica.<br /></span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Como quiera que este alumnado de Ciencias sólo ha cursado la asignatura de Matemáticas II, existe una posibilidad de que completen los conocimientos que les faltan para poder dominar la materia de Matemáticas CC SS II mediante la asignatura de Estadística, que se puede elegir como optativa en 2º de Bachillerato Ciencias.<br /></span><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">Dada la importancia de obtener una buena nota final en Selectividad es por lo que el Departamento de Matemáticas anima al alumnado que actualmente está en fase de matriculación para que seleccione </span><strong style="color: #2a2a2a; font-size: 13px; text-transform: none;">Estadística</strong><span style="color: #2a2a2a; font-size: 13px; text-transform: none;"> como </span><strong style="color: #2a2a2a; font-size: 13px; text-transform: none;">optativa</strong><span style="color: #2a2a2a; font-size: 13px; text-transform: none;">. </span></span><span style="text-align: left; background-color: #ffffff;">Si se necesita más información se puede consultar a cualquier miembro del Departamento, o escribiendo un mensaje a <strong>iescervantesgranada[arroba]gmail.com</strong> que se le haría llegar a los mismos.<br /><br /></span></p>
<p class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.333333969116211px; text-transform: none; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px; text-align: center; background-color: #ffffff;"><strong><span style="text-align: left; background-color: #ffffff;">A CONTINUACIÓN SE PUEDEN VER LOS DISTINTOS GRADOS QUE SE IMPARTEN EN LA UNIVERSIDAD DE GRANADA EN LOS QUE SE CURSA UNA ASIGNATURA DE ESTADÍSTICA:</span></strong><img src="http://www.stei.es/estadistica/images/stories/ugr3.png" border="0" alt="" width="602" height="102" style="text-align: center; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px;" /></p>
<div class="floatbox" style="margin: 0px; padding: 0px; overflow: hidden; color: #646464; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #ffffff;">
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Enfermería</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/enfermeria.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Fisioterapia</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/fisioterapia.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Gestión y Administración Pública (Melilla)</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~eues/webgrupo/Docencia/MonteroAlonso/MonteroEstadistica2.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística II</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Diplomado en Relaciones Laborales</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~ramongs/relacioneslaborales.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~eao" style="text-decoration: none; color: #678925;">Estadística Asistida por Ordenador</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Biología</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioestad/ASIGNATURAS.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioestad/ASIGNATURAS.htm" target="_blank" style="text-decoration: none; color: #678925;">Fundamentos de Biología Aplicada I</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Ciencias de la Actividad Física  y el Deporte</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/inef.htm#1" target="_blank" style="text-decoration: none; color: #678925;">Estadística</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/inef.htm#2" target="_blank" style="text-decoration: none; color: #678925;">Estadística Aplicada a la Actividad Física</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Ciencias y Técnicas Estadísticas</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/ciencias.htm" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Documentación</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~focana/stat.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Economía</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~focana/peef.htm" target="_blank" style="text-decoration: none; color: #678925;">Predicción Económica y Empresarial</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Farmacia</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~focana/mafarma.htm" target="_blank" style="text-decoration: none; color: #678925;">Matemática Aplicada</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Medicina</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/medicina02op.htm#be3" target="_blank" style="text-decoration: none; color: #678925;">Análisis Estadístico con Ordenador de Datos Médicos</a></li>
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/medicina02tr.htm#be2" target="_blank" style="text-decoration: none; color: #678925;">Bioestadística</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Odontología</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~bioest/odonto.htm" target="_blank" style="text-decoration: none; color: #678925;">Estadística Aplicada</a></li>
</ul>
<h4 style="margin-top: 15px; margin-bottom: 10px; font-size: 16px; line-height: 16px; font-family: Arial, Helvetica, sans-serif; color: #50555a; padding-left: 30px;">Licenciado en Sociología</h4>
<ul style="font-size: 13px; text-decoration: none; color: #678925; padding-left: 30px;">
<li style="padding-left: 30px;"><a href="http://www.ugr.es/~ramongs/sociologia.htm" target="_blank" style="text-decoration: none; color: #678925;">Análisis Multivariante</a></li>
</ul>
</div>
<hr />
<div id="contenido_sitio" style="float: CENTER; display: block; width: 98%;">
<table class="MsoNormalTable" style="padding-left: 30px; width: 650px;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 517.45pt; border-width: 1pt; padding: 1.5pt;" colspan="4" valign="top" width="690">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center; padding-left: 30px;" align="center"><strong><span style="text-decoration: underline;"><span style="font-size: 10pt; font-family: 'Century Gothic', sans-serif;">PENDIENTES DE MATEMÁTICAS PARA SEPTIEMBRE DE 2014</span></span></strong></p>
</td>
</tr>
<tr style="height: 112.8pt; padding-left: 10px;">
<td style="border-top-style: none; border-left-width: 1pt; border-bottom-style: none; border-right-width: 1pt; padding: 30pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 1º ESO</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-bottom-style: none; border-left-style: none; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">TEMA 1</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">. Números naturales<br /> <strong>TEMA 2.</strong> Divisibilidad<br /> <strong>TEMA 3.</strong> Fracciones<br /> <strong>TEMA 4.</strong>  Números decimales<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">TEMA 5.</span></strong><span style="font-size: 8pt; font-family: inherit, serif;"> Números enteros<br /> <strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-bottom-style: none; border-left-style: none; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 57pt; text-indent: -18pt; padding-left: 30px;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">TEMA 7.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Sistema Métrico Decimal</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">TEMA 8.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Proporcionalidad numérica<br /> <strong>TEMA 9.</strong> Ángulos, circunferencias y círculos<br /> <strong>TEMA 10.</strong> Polígonos<br /> <strong>TEMA 11.</strong> Funciones y gráficas</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-bottom-style: none; border-left-style: none; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 112.8pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 1</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 2</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="font-family: 'inherit','serif'; color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 6</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 7</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 8</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 10</span></a>. <a href="archivos_ies/13_14/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="border-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 2º ESO</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 1.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números enteros<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">2.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Fracciones<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">3.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números decimales<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">4.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Sistema sexagesimal<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">5.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Expresiones algregráicas<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">6.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Ecu</span><span style="font-size: 8pt; font-family: inherit, serif;">acioens de primer y segundo grado</span></p>
</td>
<td style="width: 5cm; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_7_a_11.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt 9pt 12.65pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Sistemas de ecuaciones</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> <strong>UNIDAD </strong></span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">8.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Proporcionalidad numérica<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">9.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Proporcionalidad geométrica<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">10.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Figuras planas. Áreas<br /> </span><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">11.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Cuerpos geométricos</span></p>
</td>
<td style="width: 5cm; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 1</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 2</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDADES 3 Y 4</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><span style="color: #5d5d5d; letter-spacing: .75pt;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank">UNIDAD 5<br /></a></span></span><a href="archivos_ies/13_14/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank" style="letter-spacing: 0.75pt; font-size: 8pt;">UNIDAD 6</a></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 7</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 8</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9 Y 10</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Cuerpos_geometricos_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 3º ESO</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_1_a_6.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 1.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números racionales<br /> <strong>UNIDAD 2.</strong> Números reales<br /> <strong>UNIDAD 3.</strong> Polinomios<br /> <strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado <br /> <strong>UNIDAD 5.</strong> Sistemas de ecuaciones<br /> <strong>UNIDAD 6.</strong> Proporcionalidad numérica</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_7_a_12.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Progresiones</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 8.</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Lugares geométricos.Figuras planas<br /> <strong>UNIDAD 9.</strong> Cuerpos geométricos<br /> <strong>UNIDAD 10.</strong> Movimientos y semejanza<br /> <strong>UNIDAD 11.</strong> Funciones<br /> <strong>UNIDAD 12.</strong> Funciones lineales y afines</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDADES 1 Y 2</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="font-family: 'inherit','serif'; color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 6</span><span style="color: #5d5d5d; letter-spacing: .75pt;"> </span></a></span></p>
<div class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><a href="archivos_ies/13_14/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank" style="font-size: 7pt;"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 7</span></a></div>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 8</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Movimientos_plano_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 10</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_y_graficas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_lineales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 12</span></a></span></p>
</td>
</tr>
<tr style="height: 132.75pt; padding-left: 30px;">
<td style="border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 1º BACH. MAT. CN.</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/examen1.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 1:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números Reales<br /> <strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas<br /> <strong>UNIDAD 3:</strong> Trigonometría<br /> <strong>UNIDAD 4:</strong> Números Complejos<br /> <strong>UNIDAD 5:</strong> Geometría analítica</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CNAT.pdf" target="_blank"><strong><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></strong></a></span></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Funciones<br /> <strong>UNIDAD 8:</strong> Funciones elementales<br /> <strong>UNIDAD 9:</strong> Límite de una función<br /> <strong>UNIDAD 10:</strong> Derivada de una función<br /> <strong>UNIDAD 11:</strong> Integrales</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px; height: 132.75pt;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 1</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD: 2</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a></span></p>
<div class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank" style="font-size: 7pt;"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDADES: 7 Y 8</span></a></div>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 9</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 10</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Integrales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 11</span></a></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top">
<p class="MsoNormal" style="margin: 16.5pt 0cm; text-align: center;" align="center"><strong><span style="font-family: 'Century Gothic', sans-serif; text-transform: uppercase;">PENDIENTES DE 1º BACH. MAT. CCSS.</span></strong></p>
</td>
<td style="width: 148.1pt; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="197">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="text-decoration: underline;"><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: inherit, serif;">UNIDAD </span></strong><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">1:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Números Reales<br /> <strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 4:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Ecuaciones, inecuaciones y sistemas</span></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; page-break-before: always; padding-left: 30px;"><span style="font-size: 10pt; font-family: 'Times New Roman', serif;"> </span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 0cm 0cm 0.0001pt 33pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol; color: #414141; mso-fareast-language: ES;">·<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><strong><span style="font-size: 8.0pt; mso-bidi-font-size: 10.0pt; font-family: 'Century Gothic','sans-serif'; mso-fareast-font-family: 'Times New Roman'; mso-bidi-font-family: 'Times New Roman'; color: #414141; mso-fareast-language: ES;"><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: 0.75pt; background-image: initial; background-attachment: initial; background-size: initial; background-origin: initial; background-clip: initial; background-position: initial; background-repeat: initial;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></strong></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; text-align: center; padding-left: 30px;" align="center"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> </span></p>
<p class="MsoNormal" style="margin: 9pt 3.75pt; padding-left: 30px;"><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 5:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Funciones<br /> <strong>UNIDAD 6:</strong> Funciones elementales</span><span style="font-size: 8pt; font-family: inherit, serif;"><br /> </span><strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;">UNIDAD 7:</span></strong><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> Límite de una función<br /> <strong>UNIDAD 8:</strong> Derivada de una función<br /> <strong>UNIDAD 11:</strong> Probabilidad</span></p>
</td>
<td style="width: 5cm; border-top-style: none; border-left-style: none; border-bottom-width: 1pt; border-right-width: 1pt; padding: 1.5pt 1.5pt 1.5pt 31px;" valign="top" width="189">
<p class="MsoNormal" style="margin: 9pt 3.75pt;"><span style="font-size: 8pt; font-family: 'Century Gothic', sans-serif;"> <strong><span style="text-decoration: underline;">EJERCICIOS RESUELTOS PARA PRACTICAR:</span></strong></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 1</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 3</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 4</span></a></span></p>
<div class="MsoNormal" style="margin-bottom: 0.0001pt; text-align: center;" align="center"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank" style="font-size: 7pt;"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 5</span></a></div>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 6</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 7</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank"><span style="color: #5d5d5d; letter-spacing: .75pt;">UNIDAD 8</span></a></span></p>
<p class="MsoNormal" style="margin: 0cm 3.75pt 0.0001pt; text-align: center;" align="center"><span style="font-size: 7pt; font-family: 'Century Gothic', sans-serif;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Binomial_resueltos.pdf" target="_blank"><span style="color: #2f310d; letter-spacing: .75pt; text-decoration: none; text-underline: none;">UNIDAD 11</span></a></span></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><strong><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 107%;"> </span></strong></p>
<p class="MsoNormal" style="padding-left: 30px;"><strong><span style="font-size: 12.0pt; mso-bidi-font-size: 11.0pt; line-height: 107%;">LIBROS DE TEXTO DEL DEPARTAMENTO DE MATEMÁTICAS, CURSO 2014/15:</span></strong><span style="color: #5d5d5d; font-size: 31px; font-weight: bold; text-transform: uppercase;"> </span></p>
<table class="MsoTableGrid" style="width: 642px; border: none; padding-left: 30px;" border="1" cellspacing="0" cellpadding="0">
<tbody style="padding-left: 30px;">
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>CURSO</strong></p>
</td>
<td style="width: 106.15pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Materia</strong></p>
</td>
<td style="width: 163.1pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Editorial</strong></p>
</td>
<td style="width: 106.3pt; border-top-color: windowtext; border-right-color: windowtext; border-bottom-color: windowtext; border-top-width: 1pt; border-right-width: 1pt; border-bottom-width: 1pt; border-left-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>EAN</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>1º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana  Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052570</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>2º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana  Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788429438215</p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>3º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana  Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052587</p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>4º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas A</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052112</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>4º ESO</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas B</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana Grazalema, Sl</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">9788483052242</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>1º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas I</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas I</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Santillana</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">La casa del saber (González y otros)</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>1º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas CCSS I</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas aplicadas a las CCSS I</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;">Santillana. </span>La casa del Saber (Escoredo y otros)</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>2º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas II</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas II</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;">Santillana. </span>Matemáticas II. La casa del saber (González y</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">otros)</p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="width: 106.15pt; border-right-color: windowtext; border-bottom-color: windowtext; border-left-color: windowtext; border-right-width: 1pt; border-bottom-width: 1pt; border-left-width: 1pt; border-top-style: none; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>2º BACHILLERATO:</strong></p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><strong>Matemáticas CCSS II</strong></p>
</td>
<td style="width: 106.15pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">Matemáticas aplicadas a las CCSS II</p>
</td>
<td style="width: 163.1pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="217">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;">Santillana. </span>Matemáticas aplicadas a las CCSS II. La casa</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;">del saber (Escoredo y otros)</p>
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
<td style="width: 106.3pt; border-top-style: none; border-left-style: none; border-bottom-color: windowtext; border-bottom-width: 1pt; border-right-color: windowtext; border-right-width: 1pt; padding: 0cm 5.4pt 0cm 35px;" valign="top" width="142">
<p class="MsoNormal" style="margin-bottom: 0.0001pt; padding-left: 30px;"> </p>
</td>
</tr>
</tbody>
</table>
<h1 style="text-align: center; padding-left: 30px;"> <span style="text-align: left;"> </span></h1>
<hr style="padding-left: 30px;" />
<h1 style="text-align: center; padding-left: 30px;"><strong style="text-align: left;"><span style="color: #423c33;">CURSO 2013/14</span></strong></h1>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; padding-left: 30px;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Primera reunión de Coordinación para Selectividad de Matemáticas CC SS II </span></h2>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="color: #2a2a2a;"> </span><span style="text-indent: 0cm; text-transform: none; color: #2a2a2a;"> </span><a href="archivos_ies/13_14/2bach_matccssii/acta_1reunion_m_ccssii.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></li>
</ul>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; padding-left: 30px;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Primera reunión de Coordinación para Selectividad de Matemáticas II </span></h2>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="color: #2a2a2a;"> </span><span style="text-indent: 0cm; text-transform: none; color: #2a2a2a;"> </span><a href="archivos_ies/13_14/2bach_matii/informacionmatematicasii.pdf" target="_blank" style="color: #1155cc;">Ver enlace</a></li>
</ul>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"> </p>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="color: #423c33;">20/12/13:</span></strong></span><strong><span style="color: #423c33;"> </span></strong>Los desafíos matemáticos vuelven a esta casa por Navidad. Como ya sucedió el año pasado, con motivo del sorteo de la <a href="http://elpais.com/tag/loteria_navidad/a/">Lotería de Navidad</a> ofrecemos un nuevo problema. El encargado de presentar el <em>Desafío Extraordinario de Navidad</em> de 2013 es Javier Cilleruelo, profesor de la <a href="http://www.uam.es/">Universidad Autónoma de Madrid</a> y miembro del <a href="http://www.icmat.es/es">Instituto de Ciencias Matemáticas</a> (ICMAT).<br />Entre los acertantes se sorteará una biblioteca matemática como la que ofreció EL PAÍS en el quiosco durante 2011. El ganador recibirá, además, el libro <a href="http://estimulos-matematicos.aprenderapensar.net/">'Desafíos Matemáticos'</a> por cortesía de la <a href="http://www.rsme.es/">Real Sociedad Matemática Española</a>, una publicación de SM en la que se recogen los <a href="http://sociedad.elpais.com/sociedad/2011/07/12/actualidad/1310421608_850215.html">40 desafíos matemáticos</a> que ofrecimos en la web y con los que EL PAÍS y la RSME celebraron el centenario de esta institución hace dos años. Manda tu respuesta antes de las 00.00 horas del domingo 22 de diciembre (la medianoche del sábado al domingo, hora peninsular española) a <span id="cloak78231">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak78231').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy78231 = 'pr&#111;bl&#101;m&#97;m&#97;t&#101;m&#97;t&#105;c&#97;s' + '&#64;';
 addy78231 = addy78231 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 var addy_text78231 = 'pr&#111;bl&#101;m&#97;m&#97;t&#101;m&#97;t&#105;c&#97;s' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak78231').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy78231 + '\'>'+addy_text78231+'<\/a>';
 //-->
 </script> y participa en el sorteo. <br />A continuación, para aclarar dudas y en atención a nuestros lectores sordos, añadimos el enunciado del problema por escrito: <br />El equipo que preparamos los desafíos matemáticos hemos decidido abonarnos durante todo el año a un número de la Lotería. Para elegir ese número, que debe estar comprendido entre el 0 y el 99.999, pusimos como condición que tuviese las cinco cifras distintas y que, además, cumpliese alguna otra propiedad interesante. Finalmente hemos conseguido un número que tiene la siguiente propiedad: si numeramos los meses del año del 1 al 12, en cualquier mes del año ocurre que al restar a nuestro número de lotería el número del mes anterior, el resultado es divisible por el número del mes en el que estemos. Y esto sucede para cada uno de los meses del año. <br />Es decir, si llamamos L a nuestro número, tenemos por ejemplo que en marzo L-2 es divisible entre 3 y en diciembre L-11 es divisible entre 12. <br /><br />El reto que os planteamos es que nos digáis a qué número de Lotería estamos abonados y que nos expliquéis cómo lo habéis encontrado.<br /><br /><strong>OBSERVACIONES IMPORTANTES.</strong> Insistimos en que es importante que hagáis llegar junto con el número el razonamiento de cómo lo habéis hallado. No se considerarán válidas las respuestas que den sólo el número o que lo hayan encontrado probando todos uno a uno (a mano o con un ordenador).</li>
</ul>
<p style="padding-left: 30px;"> </p>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="color: #423c33;"> </span></strong></span></p>
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><a href="archivos_ies/13_14/programaciones/13_14_PROGRAMACION_Mate4.doc" target="_blank"><strong style="color: #2b2721;"><span style="color: #423c33;">PROGRAMACIÓN DE MATEMÁTICAS, CURSO 2013/14.</span></strong></a></li>
<li style="padding-left: 30px;"><strong style="color: #2b2721;"><span style="color: #423c33;"><a href="archivos_ies/13_14/pendientes/Pendientes13_14_b.pdf" target="_blank">FECHAS DE EXÁMENES: PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES.</a><strong style="color: #2b2721;"><span style="color: #423c33;"> <br /><br /></span></strong></span></strong></li>
<li style="padding-left: 30px;"><strong style="color: #2b2721;"><span style="color: #423c33;"><strong style="color: #2b2721;"><span style="color: #423c33;"><a href="archivos_ies/13_14/2bach_matccssii/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2013_2014.pdf" target="_blank">Directrices y orientaciones Selectividad Mat. CC.SS. II curso 2013/14.</a><br /><br /></span></strong></span></strong></li>
<li style="padding-left: 30px;">12/11/2012:  <a href="archivos_ies/13_14/2bach_matccssii/COORDINACION05-11-12_Y_DIRECTRICES_OFICIALES5.pdf" target="_blank">Coordinación de Selectividad de Mat. CC SS II del 05/11/12..</a></li>
<li style="padding-left: 30px;">09/11/2012: <a href="archivos_ies/13_14/2bach_matccssii/Sept_2012_selec_MatCCSSII_y_soluciones2.pdf" target="_blank">Examen y soluciones de Selectividad de MAT_CC_SS_II de sept. 2012.</a></li>
<li style="padding-left: 30px;">20/06/2012: <a href="archivos_ies/13_14/2bach_matccssii/Junio_2012_selec_MatCCSSII_y_soluciones.pdf" target="_blank">Exámen y soluciones de Selectividad de MAT_CC_SS_II de jun. 2012,</a></li>
</ul>
<hr style="padding-left: 30px;" />
<div style="padding-left: 30px;">
<ul style="color: #151509; margin: 1em 0px 1em 2em; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 17px; padding: 0px 0px 0px 30px;">
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS1_ESO.pdf" target="_blank" title="Criterios de evaluación de 1º ESO." style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #336600; font-size: 11px;">Criterios de evaluación de Matemáticas 1º ESO.</a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_2_ESO.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">2º ESO.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_3_ESO.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">3º ESO.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_4_ESO.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">4º ESO.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_CCSS I _1BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas</span></span> </span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">1º Bachillerato CCSS I.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS I _1BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas I</span></span> 1</span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">º Bachillerato CNAT.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_CCSSII_2BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas </span></span>2</span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">º Bachillerato CCSS II.</span></span></a></li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 48px; line-height: 1.25em; background-image: url('templates/otonio6/images/postbullets.png'); background-repeat: no-repeat;"><span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><a href="archivos_ies/criterios_evaluacion/MATEMATICAS_II_2BACH.pdf" target="_blank"><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">Criterios de evaluación de</span></span> <span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; line-height: normal;"><span style="color: #336600;"><span style="font-size: 11px;">Matemáticas II</span></span> 2</span><span style="color: #336600; font-family: Verdana, Arial, Helvetica, sans-serif;"><span style="font-size: 11px;">º Bachillerato CNAT</span></span>.</a></span></li>
</ul>
<p style="padding-left: 30px;"> </p>
<hr style="padding-left: 30px;" />
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=94" target="_blank"><span style="color: #2a2a2a;">ENLACE A ZONA DE 2º BACH. CC.SS. II.</span></a></strong></li>
<li style="padding-left: 30px;"><strong><a href="index.php?option=com_content&amp;view=article&amp;id=154" target="_blank"><span style="color: #2a2a2a;">ENLACE A ZONA DE 2º BACH. CC.NN. II.</span></a></strong></li>
</ul>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"> </p>
<h2 style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES:</span></strong></span></h2>
<p style="padding-left: 30px;"> </p>
<table class="borde_outset" style="text-align: center; color: #2a2a2a; padding-left: 30px; background-color: #e7eaad;" border="1">
<tbody style="padding-left: 30px;">
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 1º ESO</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" style="text-decoration: underline; color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"> <strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>TEMA 1</strong>. Números naturales</p>
<p style="padding-left: 30px;"><strong>TEMA 2.</strong> Divisibilidad</p>
<p style="padding-left: 30px;"><strong>TEMA 3.</strong> Fracciones</p>
<p style="padding-left: 30px;"><strong>TEMA 4.</strong>  Números decimales</p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p style="padding-left: 30px;"><span style="font-family: 'Arial','sans-serif';"> </span></p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="text-decoration: underline; color: #2f310d; background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="text-align: left; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p style="padding-left: 30px;"><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p style="padding-left: 30px;"><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p style="padding-left: 30px;"><strong>TEMA 10.</strong> Polígonos</p>
<p style="padding-left: 30px;"><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p style="padding-left: 30px;"> </p>
</td>
<td style="text-align: center; padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/13_14/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></span></a><hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p style="padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<a href="archivos_ies/13_14/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 2º ESO</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank" style="text-decoration: underline; color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"> <strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 1.</strong> Números enteros</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>2.</strong> Fracciones</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>3.</strong> Números decimales</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>4.</strong> Sistema sexagesimal</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>5.</strong> Expresiones algregráicas</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>6.</strong> Ecu<span style="color: inherit; font-family: inherit; font-size: inherit;">acioens de primer y segundo grado</span></p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_7_a_11.pdf" target="_blank" style="text-decoration: underline; color: #2f310d; background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="text-align: left; padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>7.</strong> Sistemas de ecuaciones</span></p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>8.</strong> Proporcionalidad numérica</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>9.</strong> Proporcionalidad geométrica</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>10.</strong> Figuras planas. Áreas</p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>11.</strong> Cuerpos geométricos</p>
</td>
<td style="text-align: center; padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank">UNIDADES 3 Y 4</a></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/13_14/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></span></a><hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank">UNIDAD 9 Y 10</a></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Cuerpos_geometricos_resueltos.pdf" target="_blank">UNIDAD 11</a><br /></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 3º ESO</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_1_a_6.pdf" target="_blank" style="text-decoration: underline; color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 1.</strong> Números racionales</p>
<p style="padding-left: 30px;"><span style="font-size: inherit;"><strong>UNIDAD 2.</strong> Números reales</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 3.</strong> Polinomios</p>
<p style="padding-left: 30px;"><strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado </p>
<p style="padding-left: 30px;"><strong>UNIDAD 5.</strong> Sistemas de ecuaciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 6.</strong> Proporcionalidad numérica</p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_3_ESO_Temas_7_a_12.pdf" target="_blank" style="text-decoration: underline; color: #2f310d; background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 7.</strong> Progresiones</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 8.</strong> Lugares geométricos. Figuras planas</p>
<p style="padding-left: 30px;"><strong>UNIDAD 9.</strong> Cuerpos geométricos .</p>
<p style="padding-left: 30px;"><strong>UNIDAD 10.</strong> Movimientos y semejanza</p>
<p style="padding-left: 30px;"><strong>UNIDAD 11.</strong> Funciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 12.</strong> Funciones lineales y afines</p>
</td>
<td style="padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDADES 1 Y 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span> </a></p>
<hr style="padding-left: 30px;" />
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /> </strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank">UNIDAD: 7</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank">UNIDAD: 8</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank">UNIDAD 9</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Movimientos_plano_resueltos.pdf" target="_blank">UNIDAD 10</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_y_graficas_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Funciones_lineales_resueltos.pdf" target="_blank">UNIDAD 12</a><br /></span></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 1º BACH. MAT. CN.</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><strong><span style="text-decoration: underline;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/examen1.pdf" target="_blank"><span style="color: #2f310d; text-decoration: underline;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></span></strong></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 1:</strong> Números Reales</p>
<p style="padding-left: 30px;"><strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas</p>
<p style="padding-left: 30px;"><strong>UNIDAD 3:</strong> Trigonometría</p>
<p style="padding-left: 30px;"><strong>UNIDAD 4:</strong> Números Complejos</p>
<p style="padding-left: 30px;"><strong>UNIDAD 5:</strong> Geometría analítica</p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CNAT.pdf" target="_blank"><span style="text-decoration: underline;"><strong><span style="color: #2f310d;"><span style="background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></span></strong></span></a></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="padding-left: 30px;"><strong>UNIDAD 7:</strong> Funciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 8:</strong> Funciones elementales</p>
<p style="padding-left: 30px;"><strong>UNIDAD 9:</strong> Límite de una función.</p>
<p style="padding-left: 30px;"><strong>UNIDAD 10:</strong> Derivada de una función</p>
<p style="padding-left: 30px;"><strong>UNIDAD 11:</strong> Integrales</p>
</td>
<td style="text-align: center; padding-left: 30px;">
<p style="text-align: center; padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 2</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<hr style="padding-left: 30px;" />
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /> </strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank">UNIDADES: 7 Y 8</a></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank">UNIDAD 9</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank">UNIDAD 10</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cn/Integrales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></p>
</td>
</tr>
<tr style="padding-left: 30px;">
<td style="padding-left: 30px;">
<h4 style="text-align: center; padding-left: 30px;">Pendientes de 1º BACH. MAT. CCSS.</h4>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d; text-decoration: underline;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></span></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 17/01/2014</strong></p>
<p style="padding-left: 30px;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>1:</strong> Números Reales</p>
<p style="padding-left: 30px;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 4:</strong> Ecuaciones, inecuaciones y sistemas</p>
<p class="MsoNormal" style="page-break-before: always; padding-left: 30px;"> </p>
</td>
<td style="padding-left: 30px;">
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><strong><a href="archivos_ies/13_14/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="text-decoration: underline;"><span style="color: #2f310d;"><span style="background-color: #e7eaad;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></span></span></a></strong></li>
</ul>
<p style="text-align: center; padding-left: 30px;"><strong>SEGUNDO EXAMEN: 28/03/2014</strong></p>
<p style="text-align: center; padding-left: 30px;"> </p>
<p style="padding-left: 30px;"><strong>UNIDAD 5:</strong> Funciones</p>
<p style="padding-left: 30px;"><strong>UNIDAD 6:</strong> <span style="color: inherit; font-family: inherit; font-size: inherit;">Funciones elementales</span></p>
<p style="padding-left: 30px;"><strong>UNIDAD 7:</strong> Límite de una función</p>
<p style="padding-left: 30px;"><strong>UNIDAD 8:</strong> Derivada de una función</p>
<p style="padding-left: 30px;"><strong>UNIDAD 11:</strong> Probabilidad</p>
</td>
<td style="padding-left: 30px;">
<p style="padding-left: 30px;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 3</a><br /></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 4</a><br /></span></p>
<hr style="padding-left: 30px;" />
<p style="text-align: center; padding-left: 30px;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /> </strong></span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank">UNIDAD 5</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank">UNIDAD 6</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank">UNIDAD 7</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank">UNIDAD 8</a><br /> </span></p>
<p style="text-align: center; padding-left: 30px;"><a href="archivos_ies/13_14/pendientes/mat/1bach_cs/Binomial_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></p>
</td>
</tr>
</tbody>
</table>
</div>
</div>
<div style="float: left; display: block; width: 98%; padding-left: 30px;"><strong style="color: #414141;"><span style="color: #2a2a2a; font-weight: normal;"> </span></strong></div>
<div style="float: left; display: block; width: 98%; padding-left: 60px;"><span style="text-decoration: underline;"><strong style="color: #414141;"><span style="color: #2a2a2a; text-decoration: underline;">Página personal del profesor:</span></strong></span><br />
<ul style="padding-left: 30px;">
<li style="padding-left: 30px;"><span style="font-size: 12pt;">Gerardo Bustos: <a href="http://perso.orange.es/gerbustgut59/" target="_blank" title="http://perso.gratisweb.com/gerardobustos/">entrar.</a></span></li>
</ul>
<p style="padding-left: 30px;"><strong><span style="font-size: 12pt;"><img src="images/stories/cervantes/qr_dep_mat.png" border="0" alt="Código qr del Departamento de Matemáticas." width="196" height="196" /></span></strong></p>
<p style="padding-left: 30px;">Código qr del Dep. de Matemáticas.</p>
</div>
<div class="fechaModif" style="text-align: center; padding-left: 30px;">Ultima actualización: 10/11/2013</div>
<div class="fechaModif" style="text-align: center;"><hr /></div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">DEPARTAMENTOS</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 01 Diciembre 2014 20:15</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=37:relacion-de-departamentos&amp;catid=100&amp;Itemid=59&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=b367c5242ec0caa0bdc4d3970c82bf61bc90758a" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 5307
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div id="contenido_sitio" style="float: left; display: block;"><span style="font-size: 16pt;"> </span></div>
<h2>Relación de Departamentos, CURSO 2014/15<span style="color: #2a2a2a; font-size: 13px;"> </span></h2>
<div style="color: #000000; font-family: Calibri; font-size: 16px; background-color: #ffffff;"> </div>
<table style="width: 100%; font-size: 12px;" border="0" cellspacing="4" cellpadding="0">
<tbody>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; color: #ffffff; padding-left: 15px; font-family: arial, sans-serif; font-size: 14px; font-weight: bold; background-color: #5a8bc7;" align="left" width="30%">Departamento</td>
<td style="margin: 0px; color: #ffffff; padding-left: 15px; font-family: arial, sans-serif; font-size: 14px; font-weight: bold; background-color: #5a8bc7;" align="left" width="70%">Componentes</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"> </td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"><strong><em> </em></strong></td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #ffffff;"><strong>Biología y Geología</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #ffffff;">
<div>Dª María del Mar Martín Molina</div>
<div>Dª María Inmaculada Rojo Camacho: Jefatura del Departamento</div>
<div>Dª Amparo Ibáñez Ausina</div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Dibujo</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p style="color: #ff0000;"><a href="index.php?option=com_content&amp;view=article&amp;id=76:departamento-de-dibujo&amp;catid=97:educacion-plastica&amp;Itemid=98" target="_blank"><span style="color: #000000;">D. Juan Carlos </span><span style="color: #000000;">Lazuen Alcón</span></a></p>
<p style="color: #000000;"><a href="index.php?option=com_content&amp;view=article&amp;id=77:do-emilia-vilchez-campillos&amp;catid=97:educacion-plastica&amp;Itemid=134">Dª Emilia Vílchez Campillos</a>:  Jefatura del Departamento</p>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Economía</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p><a href="index.php?option=com_content&amp;view=article&amp;id=169:economia&amp;catid=76:economia" target="_blank">D. David Maldonado Cambil</a></p>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Educación Física</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Dª María José Muro Jiménez: Jefatura del Departamento<br /><br /></div>
<div>Dª Nuria Manrique Mora<br /><br /></div>
<div>Dª Rosa María Crovetto González</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Filosofía</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p><span style="color: #000000; font-family: Calibri; font-size: 16px;">D. Ricardo de la Blanca Torres</span></p>
<p><a href="index.php?option=com_content&amp;view=article&amp;id=166:dep-filosofia&amp;catid=102:departamentos-de-humanidades" target="_blank">D. José Luis Abian Plaza</a></p>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Física y Química</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p style="font-family: arial, sans-serif;">Dª María Elisa Mazuecos González: Jefatura del Departamento</p>
<p style="font-family: arial, sans-serif;">D. Alfonso Centeno Gómez</p>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Francés</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Dª María Ester Castillo Fernández: Jefatura del Departamento<br /><br /></div>
<div>Dª María Encarnación Peña Fernández<br /><br /></div>
<div>Dª Francisca Peña Fernández<br /><br /></div>
<div>D. Antonio Martín Olmos</div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Geografía e Historia</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>D. Miguel Ángel Hitos Urbano<br /><br /></div>
<div>D. Juan Antonio Pachón Romero<br /><br /></div>
<div>Dª Justina Castillo García<br /><br /></div>
<div>Dª María del Carmen Palma Moreno: Jefatura del Departamento<br /><br /></div>
<div>D. Santiago Freire Morales<br /><br /></div>
<div>Dª María del Pilar Vicente Zapata<br /><br /></div>
<div>D. Miguel Ángel Salort Medina</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p><strong>Inglés</strong></p>
</td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Dª Silvia Navas-Parejo Casado<br /><br /></div>
<div>D. Carlos Agustín Fernández-Amigo Sánchez<br /><br /></div>
<div>Dª Mercedes León Ramos<br /><br /></div>
<div>Dª Francisca Pérez Román<br /><br /></div>
<div>Dª Leonor Aranda Castro: Jefatura del Departamento<br /><br /></div>
<div>Dª María Sacramento Ortiz Comerma<br /><br /></div>
<div>Dª Mª Sandra González Molina</div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Informática</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p>Dª <span style="font-size: 12px;">Esperanza </span><span style="font-size: 12px;">Vaquera Heredia</span></p>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Latín</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div style="text-align: justify;">  </div>
<div style="text-align: justify;"><span style="font-size: 12px;">Dª María Dolores Zambrano Torres : Jefatura del Departamento</span></div>
<div style="text-align: justify;"><span style="font-size: 12px;"> </span></div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"><strong>Lengua y Literatura</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;">
<div>Dª Encarnación Jiménez Ávila: Jefatura del Departamento<br /><br /></div>
<div>D. Manuel Valle García<br /><br /></div>
<div>D. Felipe Bueno Maqueda<br /><br /></div>
<div>Dª Concepción Moles Ocaña<br /><br /></div>
<div>D. Manuel Jesús Gilabert Gómez<br /><br /></div>
<div>Dª Rosalía Parras Chica<br /><br /></div>
<div>Dª Silvia Gálvez Montes</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Matemáticas</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>D. Natalio Jesús Rodríguez Cano: Jefatura del Departamento<br /><br /></div>
<div>D. Gerardo Bustos Gutiérrez<br /><br /></div>
<div>Dª Magdalena Carretero Rivas<br /><br /></div>
<div>Dª María Candelas González Dengra<br /><br /></div>
<div>D. Miguel Anguita Gay<br /><br /></div>
<div>Dª Clotilde García Sánchez<br /><br />Dª Olga María de la Higuera Vílchez</div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"><strong>Música</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;">
<div>
<p style="font-family: arial, sans-serif;"><span style="font-size: 12px;">Dª María Angustias Sánchez-Montes González: Jefatura del Departamento</span></p>
</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Orientación Educativa</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p>D. José Luis Augustin Morales: Jefatura del Departamento</p>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"><strong>Pedagogía Terapéutica</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;">
<p>Dª Ana Belén Ramos Rodenes</p>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #ffffff;"><strong>Religión</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p>Dª Francisca García Guirado</p>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Tecnología</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Dª Isabel Madrid Barbero: Jefatura del Departamento<br /><br /></div>
<div>Dª Jennifer Francisca Valero Sánchez</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #ffffff;"><strong>Ciclo Superior</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Encarnación Marín Torres. (Servicios a la Comunidad)<br /><br /></div>
<div>D. Juan Manuel Prieto Tellado. (Intervención Sociocomunitaria) <br /><br /></div>
<div>D. Andrés Arcas Díaz (Intervención Sociocomunitaria): Jefatura del Departamento</div>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
            </div>";s:4:"head";a:10:{s:5:"title";s:68:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - PROYECTOS DEL IES";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:87:"/index.php?option=com_content&amp;view=category&amp;id=100&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:88:"/index.php?option=com_content&amp;view=category&amp;id=100&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:0:{}s:6:"module";a:0:{}}
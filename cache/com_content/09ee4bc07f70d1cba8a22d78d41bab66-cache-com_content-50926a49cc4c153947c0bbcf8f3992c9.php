<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:22739:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Coeducación</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Área Científica-Tecnológica</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 22 Septiembre 2014 09:04</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=185:area-cientifica-tecnologica&amp;catid=101&amp;Itemid=571&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=12a73bb612d8eb0a4c6b6c7757a2458077f79f26" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2433
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><ol>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=161&amp;Itemid=105" target="_blank" title="BIOLOGÍA GEOLOGÍA.">Dpto. de Biología y Geología.</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=160&amp;Itemid=104" target="_blank" title="FÍSICA Y QUÍMICA.">Dpto. de Física y Química.</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=51&amp;Itemid=103" target="_blank" title="MATEMÁTICAS.">Dpto. de Matemáticas.</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=159&amp;Itemid=106" target="_blank" title="TECNOLOGÍA, INFORMÁTICA.">Dpto. de Tecnología, Informática..</a></li>
</ol></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">DEPARTAMENTOS DE CIENCIAS</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Dep. Tecnología Informática</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Sábado, 15 Diciembre 2012 21:20</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=159:dep-tecnologia-informatica&amp;catid=101&amp;Itemid=106&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=59cc8bcfa520c984cb4a1f1d6e1e71a3b6fa6c6b" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 4119
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><ul style="margin: 1em 0px 1em 2em; padding: 0px; text-align: left; color: #151509; line-height: 17px; font-family: Arial, Helvetica, sans-serif; font-size: 14px;">
<li><a style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #129d0e; font-size: 11px;" title="" href="archivos_ies/dep_tecnologia/Criterios_evaluacion_3_y_4_optativa_Informatica.pdf" target="_blank">Criterios de evaluación de Informática curso 2012/13 - 3º y 4º de ESO.</a></li>
<li><a style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #129d0e; font-size: 11px;" title="" href="archivos_ies/dep_tecnologia/criterios_evaluacion_tecnologia_aplicada_1_eso.pdf" target="_blank">Dpto. Tecnología. Síntesis de programación del área de tecnologías. Asignatura: Tecnologías 1º E.S.O. Curso: 2012/2013.</a></li>
<li><a style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #129d0e; font-size: 11px;" title="" href="archivos_ies/dep_tecnologia/criterios_evaluacion_tecnologias_2_eso.pdf" target="_blank">Dpto. Tecnología Síntesis de programación del área de tecnologías. Asignatura: Tecnologías 2º E.S.O. Curso: 2012/2013.</a></li>
<li><a style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #129d0e; font-size: 11px;" title="" href="archivos_ies/dep_tecnologia/criterios_evaluacion_tecnologias_3_eso.pdf" target="_blank">Dpto. Tecnología Síntesis de programación del área de tecnologías. Asignatura: Tecnologías 3º E.S.O. Curso: 2012/2013</a></li>
<li><a href="archivos_ies/TEC_Pendientes.pdf" target="_blank">Dpto. Tecnología Síntesis de programación del área de tecnologías. Asignatura: Tecnologías 4º E.S.O. Curso: 2012/2013.</a></li>
<li><a href="archivos_ies/TEC_Pendientes.pdf" target="_blank"><span style="color: #9b3f17;" color="#9b3f17">Recuperación de pendientes de cursos anteriores de Tecnología.</span></a> 15/12/12.</li>
</ul>
<p style="margin-top: 0.5em; margin-right: 0px; margin-bottom: 0.5em; margin-left: 0px; font-style: normal; font-weight: normal; font-size: 12px; text-align: justify;">&nbsp;</p>
<hr />
<p>&nbsp;</p>
<p>&nbsp;</p>
<hr />
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="text-decoration: underline;"><strong>DATOS DEL CURSO 2010/11.</strong></span></p>
<table style="margin-left: 2.75pt; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 481.9pt; padding: 2.75pt; border: 1pt double black;" valign="top" width="643">
<p style="text-align: center;" align="center"><b><span style="text-decoration: underline;"><span style="font-family: Arial, sans-serif;">NOTA INFORMATIVA</span></span></b></p>
<p><span style="font-family: Arial, sans-serif;">El examen de recuperación de Tecnología será<b> el 2 de Septiembre de 2011&nbsp;</b>por la tarde,de <b>19'30 a 21'00 horas</b> </span></p>
<p><span style="font-family: Arial, sans-serif;">Os recuerdo que ese mismo día debéis entregar el <b>Proyecto + Memoria</b> además del <b>libro de Tecnología.</b></span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">Más abajo tenéis enlaces tanto a exámenes realizados a lo largo del curso como a partes de la memoria del proyecto realizadas por algunos de vuestros compañeros y que os pueden servir como referencia para elaborar la vuestra. Todos los archivos están en formato <b>pdf</b>, por lo que tendréis que tener instalado un lector de este tipo de documentos (Adobe Reader o similar), además algunos están comprimidos en formato <b>zip</b>, por lo que necesitaréis un “descompresor” tipo WinZip, 7Zip o similar.</span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">Enlaces a exámenes realizados a lo largo del curso:</span></p>
<p>&nbsp;</p>
<p><span style="text-decoration: underline;"><span style="font-family: Arial, sans-serif;"><a href="archivos_ies/dep_tecnologia/examenes_2ESO_curso_2010_11.zip" target="_blank">Exámenes de 2º de ESO</a></span></span></p>
<p>&nbsp;</p>
<p><span style="text-decoration: underline;"><span style="font-family: Arial, sans-serif;"><a href="archivos_ies/dep_tecnologia/examenes_3ESO_curso_2010_11.zip" target="_blank">Exámenes de 3º de ESO</a></span></span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">En cuanto a las partes de la memoria del proyecto:</span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">Para la <span style="text-decoration: underline;"><a href="archivos_ies/dep_tecnologia/presentacion.zip" target="_blank">Presentación</a></span> tenéis 3 ejemplos (dos de ellos similares, Paula y <a href="archivos_ies/dep_tecnologia/calculos_virginia_3B001.pdf" target="_blank">Virginia</a>, que optan por describir lo que se va a hacer y que ya venía en las fotocopias que os dí; el otro, de Luis B., hace una descripción más detalla de todo el proceso), podéis utilizar cualquiera de ellos u otro que los mejore.</span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">Un apartado que casi nadie ha incluido pero que es interesante es el de las <span style="text-decoration: underline;"><a href="archivos_ies/dep_tecnologia/partes_del_proyecto.zip" target="_blank">Partes</a></span> del proyecto, os adjunto los ejemplos de Irene, que utiliza el dibujo en perspectiva para distinguirlas; y el de Virginia que lo hace sobre las vistas del proyecto.</span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">Para las <span style="text-decoration: underline;"><a href="archivos_ies/dep_tecnologia/vistas.zip" target="_blank">Vistas</a></span>, tenéis dos ejemplos, ambos incompletos: el de Lucía O. es perfecto en cuanto a acotación y perfección en el dibujo pero no mantiene la proporción en las diferentes vistas, el de Irene sí que mantiene esa proporción pero la acotación es bastante pobre.</span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">En el <span style="text-decoration: underline;"><a href="archivos_ies/dep_tecnologia/despiece.zip" target="_blank">Despiece</a></span>, dos ejemplos más, el de Virginia, que no indica la escala y hay que hacerlo y el de Lucía O.</span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">El apartado/os de<a href="archivos_ies/dep_tecnologia/fabricacion_y_montaje.zip" target="_blank"> <span style="text-decoration: underline;">Proceso de Fabricación y Montaje</span></a>, se puede hacer conjunto, como lo hace Virginia pero hay que tener cuidado de describirlo todo con detalle; o se puede hacer por separado (mejor) como hacen Irene y Luis B., distinguiendo cada parte del proyecto para así facilitar la descripción.</span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">Para<a href="archivos_ies/dep_tecnologia/materiales.zip" target="_blank"> </a><span style="text-decoration: underline;"><a href="archivos_ies/dep_tecnologia/materiales.zip" target="_blank">Materiales y Herramientas</a>, </span>tenéis los ejemplos de Irene y Luis B., procurad describir las características.</span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">Por último, los apartados de <span style="text-decoration: underline;"><a href="archivos_ies/dep_tecnologia/problemas_solucion_irene_ 3A001.pdf" target="_blank">Problemas y Soluciones</a></span>, en el que Irene lo presenta en forma de tabla; y el de <span style="text-decoration: underline;"><a href="archivos_ies/dep_tecnologia/calculos_virginia_3B001.pdf" target="_blank">Cálculos</a></span>, de Virginia.</span></p>
<p>&nbsp;</p>
<p><span style="font-family: Arial, sans-serif;">Un saludo y disculpad por los problemas técnicos que ha habido con la web antigua del Instituto, que era donde estaban disponibles todos estos documentos.</span></p>
<p><span style="font-family: Arial, sans-serif;">&nbsp;</span></p>
<hr /></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Departamento de Física y Química.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 14 Septiembre 2015 20:13</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=160:dep-fq&amp;catid=101&amp;Itemid=104&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=7fe9e78662240a0f780bb9f9b06ee4ceede7618f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 4890
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 style="text-align: center;"><span style="text-decoration: underline;"><strong>FÍSICA QUÍMICA. CURSO 2015/16</strong></span></h2>
<p><span style="text-decoration: underline;"><strong>...</strong></span></p>
<p><span style="text-decoration: underline;"><strong> </strong></span></p>
<p><span style="text-decoration: underline;"><strong>__________________________________</strong></span></p>
<p><span style="text-decoration: underline;"><strong>Curso anterior:</strong></span></p>
<ul>
<li>Resumen de la programación de FQ 3º ESO. (13/10/14)</li>
<li>Resumen de la programación de FQ 4º ESO. (13/10/14)</li>
<li>Resumen de la programación de FQ 1º Bach. (13/10/14)</li>
<li>Resumen de la programación de Física 2º Bach. (13/10/14)</li>
<li>Resumen de la programación de Química 2º Bach. (13/10/14)</li>
<li><span style="line-height: 1.25em;">Programación Bachillerato</span> (13/10/14)</li>
<li><span style="line-height: 1.25em;">Programación ESO</span> (13/10/14)</li>
</ul>
<p> </p>
<ul>
<li>Química Orgánica. (12/01/14)</li>
<li>Ejercicios de formulación orgánica. (17/01/2014)</li>
</ul>
<hr />
<div class="fechaModif" style="text-align: center;"> </div>
<div class="fechaModif"> <strong><span style="color: #71cd63; font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; text-decoration: underline; font-size: 14pt; background-color: #e7eaad; text-align: justify;">Formulación: (22/10/2014)</span></strong></div>
<h5 style="padding-left: 30px; background-color: #e7eaad;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; color: #71cd63;">1. 3º ESO</span></span></strong></h5>
<h5 style="padding-left: 30px; background-color: #e7eaad;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; color: #71cd63;">2. 4º ESO</span></span></strong></h5>
<h5 style="padding-left: 30px; background-color: #e7eaad;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; color: #71cd63;">3. BACHILLERATO</span></span></strong></h5></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Departamento de Física y Química. (3)</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 14 Septiembre 2015 20:12</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=296:dep-fq-3&amp;catid=101&amp;Itemid=188&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=af8e24f81aa3ee55fdc242965b435af2fb5287ad" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 120
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 style="text-align: center;"><span style="text-decoration: underline;"><strong>FÍSICA QUÍMICA. CURSO 2014/15.</strong></span></h2>
<ul>
<li><a href="archivos_ies/14_15/programaciones/RESUMEN_FQ_3ESO.doc" target="_blank">Resumen de la programación de FQ 3º ESO.</a> (13/10/14)<a href="archivos_ies/criterios_evaluacion/Cuadernillo_FQ_3_ESO.pdf" target="_blank" title="Criterios de evaluación de FQ 3º ESO"><br /></a></li>
<li><a href="archivos_ies/14_15/programaciones/RESUMEN_FQ_4ESO.doc" target="_blank">Resumen de la programación de FQ 4º ESO.</a> (13/10/14)</li>
<li><a href="archivos_ies/14_15/programaciones/RESUMEN_FQ_1Bach.doc" target="_blank">Resumen de la programación de FQ 1º Bach.</a> (13/10/14)</li>
<li><a href="archivos_ies/14_15/programaciones/RESUMEN_FISICA_2Bach.doc" target="_blank">Resumen de la programación de Física 2º Bach.</a> (13/10/14)</li>
<li><a href="archivos_ies/14_15/programaciones/RESUMEN_QUIMICA_2Bach.doc" target="_blank">Resumen de la programación de Química 2º Bach.</a> (13/10/14)</li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/14_15/programaciones/FQ_BAC_14-15.doc" target="_blank">Programación Bachillerato</a></span> (13/10/14)</li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/14_15/programaciones/FQ_ES0_4-15.doc" target="_blank">Programación ESO</a></span> (13/10/14)</li>
</ul>
<p> </p>
<ul>
<li><a href="archivos_ies/13_14/fq/FORM-ORGANICA.doc" target="_blank">Química Orgánica.</a> (12/01/14)</li>
<li><a href="archivos_ies/13_14/fq/ejercicios_formulacion_organica.doc" target="_blank">Ejercicios de formulación orgánica.</a> (17/01/2014)</li>
</ul>
<hr />
<div class="fechaModif" style="text-align: center;"> </div>
<div class="fechaModif"> <strong><span style="color: #71cd63; font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; text-decoration: underline; font-size: 14pt; background-color: #e7eaad; text-align: justify;">Formulación: (22/10/2014)</span></strong></div>
<h5 style="padding-left: 30px; background-color: #e7eaad;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; color: #71cd63;">1. <a href="archivos_ies/14_15/fq/3ESO_Formulacion.doc" target="_blank">3º ESO</a></span></span></strong></h5>
<h5 style="padding-left: 30px; background-color: #e7eaad;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; color: #71cd63;">2. <a href="archivos_ies/14_15/fq/4ESO_Formulacion_inorganica.doc" target="_blank">4º ESO</a></span></span></strong></h5>
<h5 style="padding-left: 30px; background-color: #e7eaad;"><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; color: #71cd63;">3. <a href="archivos_ies/14_15/fq/Bachiller_Formulacion_inorganica.doc" target="_blank">BACHILLERATO</a></span></span></strong></h5></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                            <div class="item column-2">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Dep. Biología Geología</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Sábado, 19 Octubre 2013 07:23</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=161:dep-biologia-geologia&amp;catid=101&amp;Itemid=105&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=c04858a948083a2f4b69bf6cb41f18bc24881680" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 4165
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div>
<h4 style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-decoration: underline;">BIOLOGÍA - GEOLOGÍA 2013-14.</strong></span></h4>
<ul style="text-align: justify;">
<li><span style="color: #2a2a2a;"><a href="archivos_ies/13_14/programaciones/Programaciones_bio_geo_PADRES%202013-2014.doc">Resumen de las programaciones del Departamento.</a> (19/10/13)</span></li>
</ul>
<ul>
<li><a href="archivos_ies/criterios_evaluacion/Programacion_BIOLOGIA_2012_2013.pdf" target="_blank">Programación completa de Biología 2012-13.</a></li>
</ul>
</div>
<div><hr /></div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
            </div>";s:4:"head";a:10:{s:5:"title";s:63:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Coeducación";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:102:"/index.php?option=com_content&amp;view=category&amp;id=101&amp;Itemid=188&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:103:"/index.php?option=com_content&amp;view=category&amp;id=101&amp;Itemid=188&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:12:"Coeducación";s:4:"link";s:59:"index.php?option=com_content&view=article&id=133&Itemid=188";}i:2;O:8:"stdClass":2:{s:4:"name";s:13:"Departamentos";s:4:"link";s:60:"index.php?option=com_content&view=category&id=195&Itemid=188";}i:3;O:8:"stdClass":2:{s:4:"name";s:25:"DEPARTAMENTOS DE CIENCIAS";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:4637:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Aprender jugando</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Sábado, 09 Febrero 2013 21:27</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=194:aprender-jugando&amp;catid=194&amp;Itemid=587&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=f62815102e862a415217a445fb05b82155e34d3f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 6326
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h4>¿Qué es Testeando?</h4>
<p class="parrafoAyuda">Testeando es una <strong>herramienta educativa, lúdica y gratuita pensada para profesores y estudiantes</strong> de los colegios españoles y latinoamericanos. Consiste en un<strong> juego de preguntas y respuestas</strong> tipo test o trivial, agrupadas por cursos y asignaturas que responden al desarrollo curricular del periodo educativo. Actualmente, la web incluye <strong>1.262 test de 70 asignaturas distintas con 26.348 preguntas</strong><!--, y se a&ntilde;aden en torno a <b>1.000 nuevas preguntas</b> <b>cada mes</b>-->, todas ellas de redacción propia.</p>
<p class="parrafoAyuda">Las asignaturas de cada curso se dividen en diversos test que corresponden con sus diferentes unidades temáticas. En cada partida, el juego plantea <strong>diez preguntas con cuatro respuestas distintas</strong>, de las que solo una es correcta. Las preguntas acertadas puntúan positivamente en función del tiempo tardado en responder, mientras que las falladas restan puntos al resultado final. Durante el transcurso del juego, el alumno puede hacer uso de hasta tres comodines (<strong>50%</strong>, <strong>2x1</strong>, <strong>La clase</strong>) para responder a las preguntas.</p>
<p class="parrafoAyuda">Tras cada partida, el alumno puede volver a jugar al mismo test con las <strong>mismas preguntas</strong> para repasar conceptos, o jugar con <strong>nuevas preguntas</strong> sobre el mismo tema. También puede cambiar de test o de asignatura en cualquier momento.</p>
<p class="parrafoAyuda">Testeando ofrece <strong>varias modalidades de juego</strong> (<strong>Clásico</strong>, <strong>Tríplex</strong>, <strong>Infinítum</strong>…) con distintas reglas (número de preguntas, tiempo, uso de comodines…) para que el alumno o el profesor decida en cada momento cual es el más adecuada, en función del tiempo disponible en clase, la dificultad del test, los conocimientos del alumno…</p>
<p class="parrafoAyuda">El objetivo final de Testando es conseguir que los alumnos <strong>refuercen y complementen los conceptos aprendidos en el colegio</strong> de una forma divertida y amena, y ofrecer para tal fin una herramienta para que los profesores puedan utilizarla <strong>en clase o recomendarla para casa</strong>. Recientes estudios han destacado <a href="http://goo.gl/WIAYv" target="_blank" style="color: #000000;"><strong>la importancia de la realización de test</strong></a> como forma de reforzar los contenidos aprendidos en clase, mientras que el uso de videojuegos es cada vez más <a href="http://goo.gl/7QFZY" target="_blank" style="color: #000000;"><strong>valorado por el profesorado</strong></a>.</p>
<hr />
<h4><a href="http://www.testeando.es/acercade.asp" target="_blank">¿Qué es Testeando? Ver toda la descripción.</a></h4>
<p> </p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">IES Miguel de Cervantes- General</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:67:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Aprender jugando";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:3:"UGR";s:4:"link";s:59:"index.php?option=com_content&view=article&id=192&Itemid=587";}i:1;O:8:"stdClass":2:{s:4:"name";s:32:"IES Miguel de Cervantes- General";s:4:"link";s:60:"index.php?option=com_content&view=category&id=194&Itemid=587";}i:2;O:8:"stdClass":2:{s:4:"name";s:16:"Aprender jugando";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
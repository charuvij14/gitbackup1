<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:4796:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Biblioteca: libros.</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Sábado, 28 Septiembre 2013 15:04</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=149:libros-de-la-biblioteca&amp;catid=237&amp;Itemid=231&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=796b425c4b3c79e6c3560c72d46fb7395cce98a5" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 6930
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p> </p>
<p><span style="font-size: 14pt; color: purple;">Todos los fondos bibliográficos del </span><span style="font-size: 14pt; color: #993300;">I.E.S. Miguel de Cervantes</span><span style="font-size: 14pt; color: purple;"> están catalogados e informatizados con el programa oficial de gestión de bibliotecas </span><span style="font-size: 14pt; color: #339966;">Abies </span><span style="font-size: 14pt; color: purple;">y se encuentran a disposición de toda la comunidad educativa en horarios de mañana. </span></p>
<p><span style="font-size: 14pt; color: purple;">Con la intención de facilitar su consulta o acceso, mostramos los enlaces correspondientes desde la convicción de que el libro es insustituible en el desarrollo del lenguaje, del sentido analítico y crítico, del autoconocimiento y conocimiento del mundo que nos rodea y, sobre todo, para la satisfacción de la infinita e insaciable necesidad de aventura de la juventud.</span></p>
<ul>
<li style="color: #800080; font-family: 'Times New Roman', Georgia, Times, serif; font-size: 19px; font-weight: bold;"><span style="color: #3b484e; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: normal;"><strong><span style="font-size: 12pt;"><span style="font-size: 10pt;">Consulta de los libros de la Biblioteca:</span>   <span style="font-size: 14pt;"><a href="abiex/index.php" target="_blank" style="font-size: 12px;">Formato 1</a><span style="font-size: 10pt;"> -</span><span style="font-size: 10pt;"> </span><span style="font-size: 12pt;"><a href="libros/index.php" target="_blank" style="font-size: 12px;">Formato 2</a></span></span></span></strong></span></li>
</ul>
<ul>
<li style="color: #800080; font-family: 'Times New Roman', Georgia, Times, serif; font-size: 19px; font-weight: bold;">Bibliotecas digitales y virtuales:
<ul>
<li style="color: #800080; font-family: 'Times New Roman', Georgia, Times, serif; font-size: 19px; font-weight: bold;"><span style="font-size: 10pt;"><a href="http://www.wdl.org/es/" target="_blank">Biblioteca Digital Mundial.</a></span></li>
<li style="color: #800080; font-family: 'Times New Roman', Georgia, Times, serif; font-size: 19px; font-weight: bold;"><span style="font-size: 10pt;"><a href="http://www.bibliotecasvirtuales.com/" target="_blank">Bibliotecas virtuales.</a></span></li>
<li style="color: #800080; font-family: 'Times New Roman', Georgia, Times, serif; font-size: 19px; font-weight: bold;"><span style="font-size: 10pt;"><a href="http://es.wikisource.org/wiki/Francisco_de_Quevedo" target="_blank">Francisco de Quevedo.</a></span></li>
</ul>
</li>
<li style="color: #800080; font-family: 'Times New Roman', Georgia, Times, serif; font-size: 19px; font-weight: bold;">Libros interactivos:
<ul style="color: #800080; font-family: 'Times New Roman', Georgia, Times, serif; font-size: 19px; font-weight: bold;">
<li><span style="font-size: 10pt;"><a href="http://quijote.bne.es/libro.html" target="_blank">El Quijote interactivo.</a></span></li>
<li><span style="font-size: 10pt;"><a href="http://www.ellibrototal.com/ltotal/" target="_blank">El Libro Total.</a></span></li>
</ul>
</li>
</ul></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:70:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Biblioteca: libros.";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"BIBLIOTECA";s:4:"link";s:59:"index.php?option=com_content&view=article&id=149&Itemid=154";}i:1;O:8:"stdClass":2:{s:4:"name";s:19:"Biblioteca: libros.";s:4:"link";s:59:"index.php?option=com_content&view=article&id=149&Itemid=231";}}s:6:"module";a:0:{}}
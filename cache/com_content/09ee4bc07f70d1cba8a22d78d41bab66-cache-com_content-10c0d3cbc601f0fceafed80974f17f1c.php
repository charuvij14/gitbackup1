<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:58616:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Artículos</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Pendientes de Matemáticas. Curso 2015/16</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 17 Diciembre 2015 18:54</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=236:pendientes-de-matematicas-curso-2013-14&amp;catid=129&amp;Itemid=671&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=dcfffb05f8c265de9208eebc0b69af7255db034a" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 72798
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p> </p>
<hr />
<div id="contenido_sitio" style="text-align: justify; background-color: #e7eaad; float: left; width: 793.796875px;">
<h2><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES:</span></strong></span></h2>
<table style="width: 90%;" border="0">
<tbody>
<tr>
<td style="font-family: arial,sans-serif; margin: 0px;">
<div id="contenido_sitio" style="font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13.3333339691162px; text-align: justify; float: left; width: 793.796875px; background-color: #e7eaad;">
<p style="font-size: 13.3333px; color: inherit; padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">CURSO 215/16:</span></strong></span></p>
<p style="font-size: 13.3333px; color: inherit;"> </p>
<ul style="font-size: 13.3333px; color: inherit;">
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_1_Cal.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Pendientes de 1º y 2º ESO.</span></a></strong></li>
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_2_Cal.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Pendientes de 3º ESO y 1º Bachillerato.</span></a></strong></li>
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_3_materia.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Distribución de materia de alumnos pendientes de cursos anteriores.</span></a></strong></li>
</ul>
</div>
</td>
</tr>
</tbody>
</table>
<table class="borde_outset" style="text-align: center; color: #2a2a2a;" border="1">
<tbody>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" title="Pulsar para descargar." style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a>,</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,11,12,13,19,21,28.</li>
<li><strong>Tema 2:</strong> 1,6,8,14,16,17,18,19,20,23.</li>
<li><strong>Tema 3:</strong> 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43.</li>
<li><strong>Tema 4:</strong> Todos.</li>
<li><strong>Tema 5:</strong> 1,2,5,8,9,10,11,12,13.</li>
<li><strong>Tema 6:</strong> 1,2,8,11,29,31,32.</li>
</ul>
</ul>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 18/01/2016</strong></span></p>
<p><strong>TEMA 1</strong>. Números naturales</p>
<p><strong>TEMA 2.</strong> Divisibilidad</p>
<p><strong>TEMA 3.</strong> Fracciones</p>
<p><strong>TEMA 4.</strong>  Números decimales</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p><span style="font-family: Arial, sans-serif;"> </span></p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR.  </a></strong></span><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">Solamente los siguientes números por tema:</a>
<ul>
<li><strong>Tema 7:</strong> 1,4,6,7,8,9,10,11,12,13.</li>
<li><strong>Tema 8:</strong> 2,3,6,7,9,11,12,13,14,15.</li>
<li><strong>Tema 9: TODOS.</strong></li>
<li><strong>Tema 10:</strong> 1,3,4,5,6,7,8,11,13,14.</li>
<li><strong>Tema 11:</strong> <strong>TODOS.</strong></li>
</ul>
</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 04/04/2016</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p><strong>TEMA 10.</strong> Polígonos</p>
<p><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p> </p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1</strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/15_16/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a><hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<a href="archivos_ies/15_16/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 2º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span><span style="font-size: 13px;"> Solamente los siguientes números por tema:</span></li>
<ul>
<li><strong>Tema 1:</strong> 1,6,9,10,12,16,17,18.</li>
<li><strong>Tema 2:</strong>  1,3,5,6,7, 11,13,15,16.</li>
<li><strong>Tema 3:</strong> 1,2,5,9,11,12,16,17,18,20.</li>
<li><strong>Tema 4:</strong>  1,5,8,11,12,13,16,19.</li>
<li><strong>Tema 5:</strong>  1,2,3,4,7,8,9, 10,11,12.</li>
<li></li>
</ul>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 21/01/2016</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números enteros</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>2.</strong> Fracciones</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>3.</strong> Números decimales</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>4.</strong> Sistema sexagesimal</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>5.</strong> Expresiones algebráicas</p>
<p> </p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/14_15/mate/pendientes_2_eso_parte_2c.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> (Actualizado 13/10/15)</strong></span></li>
<li><strong>Y los del Tema 6:</strong>  1,6,7,8,9,13,14,15.</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 07/04/2016</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>6.</strong><span style="font-weight: normal;"> Ecu</span><span style="color: inherit; font-family: inherit; font-size: inherit;">aciones de primer y segundo grado</span></strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>7.</strong> Sistemas de ecuaciones</span></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>8.</strong> Proporcionalidad numérica</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>9.</strong> Proporcionalidad geométrica</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>10.</strong> Figuras planas. Áreas</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank">UNIDADES 3 Y 4</a></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<br /><hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-decoration: underline;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank">UNIDAD 9 Y 10</a></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 3º ESO</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></span></span><span style="color: #2f310d;"> extraídos del siguiente DOCUMENTO COMPLETO.</span><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_1_a_5.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></span></strong></a></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 18/01/2016</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números racionales</p>
<p><span style="font-size: inherit;"><strong>UNIDAD 2.</strong> Números reales</span></p>
<p><strong>UNIDAD 3.</strong> Polinomios</p>
<p><strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado </p>
<p><strong>UNIDAD 5.</strong> Sistemas de ecuaciones</p>
<p><strong> </strong></p>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> </span></span></strong><strong><span style="color: #2f310d;">extraídos del siguiente DOCUMENTO COMPLETO.</span></strong><br /><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_6_a_10.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></span></strong></a></li>
<li></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 04/04/2016</strong></span></p>
<p><strong>UNIDAD 6.</strong> Proporcionalidad numérica</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 7.</strong> Progresiones</span></p>
<p><strong>UNIDAD 8.</strong> Lugares geométricos. Figuras planas</p>
<p><strong>UNIDAD 9.</strong> Cuerpos geométricos .</p>
<p><strong> </strong></p>
</td>
<td>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDADES 1 Y 2</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<hr />
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span> </a></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank">UNIDAD: 7</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank">UNIDAD: 8</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º BACH. MAT. CCNN.</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/1bach_cnat_ptes_examen1.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a><br /></span></span></strong></li>
</ul>
<p style="text-align: center;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;"><span style="text-decoration: underline;">PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM</span><br /></strong></p>
<p><strong>UNIDAD 1:</strong> Números Reales</p>
<p><strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas</p>
<p><strong>UNIDAD 3:</strong> Trigonometría</p>
<p><strong>UNIDAD 4:</strong> Números Complejos</p>
<p><strong>UNIDAD 5:</strong> Geometría analítica</p>
<p><strong>UNIDAD 6: </strong>Cónicas</p>
</td>
<td>
<ul>
<li><strong><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/1bach_cnat_ptes_examen2.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a><br /></span></strong></li>
</ul>
<p style="text-align: center;"><strong><span style="text-decoration: underline;">SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM</span><br /></strong></p>
<p><strong>UNIDAD 7:</strong> Funciones</p>
<p><strong>UNIDAD 8:</strong> Funciones elementales</p>
<p><strong>UNIDAD 9:</strong> Límite de una función.</p>
<p><strong>UNIDAD 10:</strong> Derivada de una función</p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 2</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 6<br /></span></a></p>
<hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank">UNIDADES: 7 Y 8</a></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank">UNIDAD 10</a></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º BACH. MAT. CCSS.</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></span></li>
</ul>
<p style="text-align: center;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;"><span style="text-decoration: underline;">PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM</span></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>1:</strong> Números Reales</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 2</strong><strong>:</strong> Aritmética Mercantil</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span></p>
<p><strong>UNIDAD 4:</strong> Ecuaciones, inecuaciones y sistemas</p>
<p class="MsoNormal" style="page-break-before: always;"> </p>
</td>
<td>
<ul>
<li><strong><a href="archivos_ies/15_16/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></li>
</ul>
<p style="text-align: center;"><strong><span style="text-decoration: underline;">SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM</span></strong></p>
<p style="text-align: center;"> </p>
<p><strong>UNIDAD 5:</strong> Funciones</p>
<p><strong>UNIDAD 6:</strong> <span style="color: inherit; font-family: inherit; font-size: inherit;">Funciones elementales</span></p>
<p><strong>UNIDAD 7:</strong> Límite de una función</p>
<p><strong>UNIDAD 8:</strong> Derivada de una función</p>
<p><strong>UNIDAD 9:</strong> Estadística unidimensional</p>
</td>
<td>
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Aritmetica_mercantil_resueltos.pdf" target="_blank"><span style="text-align: left;"><span style="text-align: left;">UNIDAD 2</span></span></a></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 3</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 4</a><br /></span></p>
<hr />
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank">UNIDAD 5</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank">UNIDAD 6</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/matematicas/Estadistica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 9</span></a></p>
</td>
</tr>
</tbody>
</table>
</div>
<div style="text-align: justify; background-color: #e7eaad; float: left; width: 793.796875px;"><strong style="color: #414141;"><span style="color: #2a2a2a; font-weight: normal;"> </span></strong></div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Matemáticas</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">1º BACH A</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 13 Mayo 2015 15:28</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=93:1o-bach-c&amp;catid=129&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=a9e4a14b4b7b2db37c0092e048aea82c407b4c10" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 14884
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h4><span style="text-decoration: underline;"><strong>1º BACHILLERATO A. CURSO 2014/15.</strong></span></h4>
<ol>
<li><span style="color: #2a2a2a;"> <a href="archivos_ies/14_15/Radicales-soluciones.docx" target="_blank">Relación de ejercicios de radicales.</a> (22/09/14)</span></li>
<li><span style="color: #2a2a2a;"> <a href="archivos_ies/14_15/mate/logaritmos_e.pdf" target="_blank">Logaritmos.</a> (09/10/14)</span></li>
<li><span style="color: #2a2a2a;"> <a href="archivos_ies/14_15/mate/NUMEROS_COMPLEJOS.pdf" target="_blank">Números complejos.</a> (18/01/15)</span></li>
<li><span style="color: #2a2a2a;"> <a href="archivos_ies/14_15/calculo_limites_funciones-ejercicios.pdf" target="_blank">Cálculo de límites de funciones.</a> (13/05/15)</span></li>
</ol>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/bilingues/1bachcienciasSoluciones_de_los_ejercicios_de_funciones.docx" target="_blank">1º Bach. Ciencias. Soluciones de los ejercicios de funciones.</a> (05/04/2015)</span></li>
</ul>
<p><span style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"> </span></p>
<hr />
<p><span style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"> </span></p>
<ul>
<li><a href="archivos_ies/173_Calculo_de_limites.pdf" target="_blank">Cálculo de límites.</a></li>
<li><a href="archivos_ies/173_Derivadas.pdf" target="_blank">Derivadas.</a></li>
<li><a href="archivos_ies/173_Ejercicios_con_derivadas.pdf" target="_blank">Ejercicios con derivadas.</a></li>
<li><a href="archivos_ies/173_Ejercicios_de_combinatoria.pdf" target="_blank">Ejercicios de combinatoria.</a></li>
<li><a href="archivos_ies/173_Ejercicios_de_repaso_de_algebra%20.pdf" target="_blank">Ejercicios de repaso álgebra.</a></li>
<li><a href="archivos_ies/173_Ejercicios_de_representacion_de_funciones.pdf" target="_blank">Ejercicios de representación de funciones.</a></li>
<li><a href="archivos_ies/173_Fracciones_algebraicas.pdf" target="_blank">Fracciones algebráicas.</a></li>
<li><a href="archivos_ies/matematicas/FUNCIONES_DEFINIDAS_A_TROZOS.pdf" target="_blank">Funciones definidas a trozos.</a> (27/01/13)</li>
<li><a href="archivos_ies/173_Probabilidad_Selectividad.pdf" target="_blank">Probabilidad Selectividad.</a></li>
<li><a href="archivos_ies/FUNCIONES_selectividad.pdf" target="_blank"><span style="color: #0066cc;">FUNCIONES, Selectividad.</span></a> (20/01/2013)</li>
</ul></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Matemáticas</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">2º ESO 2014/15</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 23 Septiembre 2014 10:21</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=97:2o-eso-c-y-d&amp;catid=129&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=21cf2ab44453bbf240fb17d7806630d66ff4021b" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 10084
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 20px; padding: 5px 5px 5px 35px; color: #2a2a2a; font-size: 13.3333px; text-transform: none; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; background-color: #ffffff; text-align: justify;"><span style="color: #5d5d5d; font-size: 26px; text-align: left; text-transform: uppercase;"><strong><span style="text-decoration: underline;">CURSO 2014/15<br /><br /></span></strong></span><a href="archivos_ies/Prog_2ESO2011_11.pdf" target="_blank">Programación DE 2º ESO curso 2014/15</a></h3></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Matemáticas</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Mas tarde</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 19 Octubre 2011 18:23</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=99:mas-tarde&amp;catid=129&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=ac2dc75a442211537d083f3c3029c22728319f91" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1904
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1><span style="font-size: 10pt;">¡He dicho que más tarde pondré las soluciones, cuando se vea el tema en clase. Es por vuestro bien!</span></h1>
<p><a href="index.php?option=com_content&amp;view=article&amp;id=67:dep-matematicas&amp;catid=101&amp;Itemid=103">&lt;--Volver.</a></p></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                            <div class="item column-2">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">ESTADÍSTICA 2º BACHILLERATO CIENCIAS</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 13 Abril 2015 11:36</span> | <span class="art-postauthoricon">Escrito por manguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=103:manguitapublico&amp;catid=129&amp;Itemid=693&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=1c90278d0ccefecc6fabb3841b654e8261c4e00f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 20540
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 style="text-align: center;"><span style="text-decoration: underline;"><strong>CURSO ACADÉMICO 2014/15</strong></span></h1>
<p> ______________________________________________________________________</p>
<p><span style="text-decoration: underline;"><strong>TRABAJOS DE LA ASIGNATURA EN LA 2ª EVALUACIÓN:</strong></span></p>
<ol>
<li><a href="fie/ryan/" target="_blank"><span style="color: #2a2a2a;">RYAN - MIGUEL - PABLO - EUSEBIO: Análisis sobre la influencia de la música sobre el poder de la concentración</span></a></li>
</ol>
<p><span style="color: #2a2a2a;">_______________________________________________________________________</span></p>
<p> </p>
<h3><span style="text-decoration: underline;"><strong>ESTADISTICA</strong></span></h3>
<p><a href="archivos_ies/14_15/mate/PROGRAMACION_DE_ESTADISTICA.pdf" target="_blank">PROGRAMACIÓN COMPLETA.</a></p>
<p> </p>
<p><a href="archivos_ies/14_15/mate/programacion_estadistica.pdf" target="_blank">Programación.</a></p>
<ul>
<li><a href="archivos_ies/14_15/mate/ESTADISTICA_MCGRAWHILL.pdf" target="_blank">Descriptiva MCGraw Hill.</a></li>
<li><a href="archivos_ies/14_15/mate/curso_estadistica.pdf" target="_blank">Curso de Estadística completo.</a></li>
<li><a href="archivos_ies/14_15/mate/Estadistica2.pdf" target="_blank">Presentación PPT.</a></li>
</ul>
<p style="padding-left: 30px;"><span style="color: #414141;">Unidad 1: Lenguaje estadístico y </span><span style="color: #414141;">Distribuciones Unidimensionales.</span></p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/mate/1Nociones_Generales.pdf" target="_blank">Nociones generales.</a></li>
<li><a href="archivos_ies/14_15/mate/2Tablas%20-Gráficos_problemas.pdf" target="_blank">Tablas y gráficos, ejercicios.</a></li>
<li><a href="archivos_ies/14_15/mate/3graficos.pdf" target="_blank">Diferentes gráficos.</a></li>
<li><a href="archivos_ies/14_15/mate/4Parametros_Estadisticos.pdf" target="_blank">Parámetros estadísticos.</a></li>
<li><a href="archivos_ies/14_15/mate/Ej_resueltos_Excel.pdf" target="_blank">Ejercicios resueltos con Excel.</a></li>
</ul>
</ul>
<ul>
<li>Unidad 2: Distribuciones bidimensionales</li>
<li>Unidad 3: Teoría de Conjuntos</li>
<li>Unidad 4: Técnicas para contar. Combinatoria</li>
<li>Unidad 5: Sucesos aleatorios. Probabilidad</li>
<li>Unidad 6: Modelos probabilísticos discretos</li>
<li>Unidad 7: Modelos probabilísticos continuos</li>
<li><span style="text-decoration: underline;"><strong>Unidad 8: Muestreo</strong></span><br /><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/2bach_matccssii/muestreoteoria.pdf" target="_blank">MUESTREO. DISTRIBUCIONES MUESTRALES</a> - <a href="archivos_ies/13_14/2bach_matccssii/muestreoejercicios.pdf" target="_blank">EJERCICIOS</a> - <a href="archivos_ies/13_14/2bach_matccssii/MUESTREOSolucionario.pdf" target="_blank">SOLUCIONES<br /><br /></a></span></span></span></strong><a href="archivos_ies/13_14/2bach_matccssii/muestreo_resumen.pdf" target="_blank"><span style="color: #2f310d; font-family: Arial,Helvetica,sans-serif;"><span style="font-size: 19px; letter-spacing: 1.33333px;"><strong>Resumen de muestreo.<br /><br /></strong></span></span></a><strong><span style="color: #3366ff; text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201345.pdf">Selectividad resueltos: 2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201244.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201149.pdf">2011</a> ; </span><span style="text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201045.pdf">2010</a></span>; </strong><a href="http://emestrada.files.wordpress.com/2010/02/2009_t6.pdf"><span style="color: #3366ff;"><strong>2009</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2008_t6.pdf"><span style="color: #3366ff;"><strong>2008</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2007_t6.pdf"><span style="color: #3366ff;"><strong>2007</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2006_t6.pdf"><span style="color: #3366ff;"><strong>2006</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2005_t6.pdf"><span style="color: #3366ff;"><strong>2005</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/200414.pdf"><span style="color: #0000ff;">2004</span></a></span></strong><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong><span style="color: #3366ff;">;</span></strong></span> </a><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/200310.pdf"><span style="color: #0000ff;">2003</span></a></span></strong><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/2002_t6.pdf"><strong><span style="color: #3366ff;">2002</span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/2001_t6.pdf"><strong><span style="color: #3366ff;">2001<br /><br /></span></strong></a></li>
<li><strong><span style="text-decoration: underline;">Unidad 9: Introducción a la Inferencia.<span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; color: #71cd63; text-decoration: underline;"><br /></span></span></span><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/2bach_matccssii/inferencia_tema.pdf" target="_blank">INFERENCIA ESTADÍSTICA. ESTIMACIÓN. CONTRASTE DE HIPÓTESIS</a> - SOLUCIONES<br /></span></span></span></strong><strong><span style="color: #3366ff; text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201345.pdf">Selectividad resueltos:  </a></span></strong><strong><span style="color: #3366ff;"><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/201346.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201232.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201163.pdf"><span style="color: #0000ff;">2011</span></a></span>; <a href="http://emestrada.files.wordpress.com/2010/02/201054.pdf">2010</a></span></strong></li>
</ul>
<p><strong><span style="color: #3366ff;"> </span></strong></p>
<p><a href="archivos_ies/14_15/mate/excel" target="_blank">EJERCICIOS RESUELTOS CON EXCEL. TODOS LOS TEMAS.</a></p>
<h4 style="font-size: 11px;">Unidad 6: <a href="archivos_ies/14_15/mate/Unidad10.Calculodeprobabilidades.pdf" target="_blank">Cálculo de probabilidades</a></h4>
<h4 style="font-size: 11px;">Unidad 9: <a href="archivos_ies/14_15/mate/Unidad11.Lasmuestrasestadisticas.pdf" target="_blank">Las muestras estadísticas</a></h4>
<h4 style="font-size: 11px;">Unidad 10: <a href="archivos_ies/14_15/mate/Unidad12.Inferenciaestadistica.Estimaciondelamedia.pdf" target="_blank">Inferencia estadística. Estimación de la media</a></h4>
<h4 style="font-size: 11px;">Problema tipo: <a href="archivos_ies/14_15/mate/Inter_COnf_Media_ProblemaICm.pdf" target="_blank">Intervalo de confianza para la media</a></h4>
<h4 style="font-size: 11px;">Unidad 10: <a href="archivos_ies/14_15/mate/Unidad13.Inferenciaestadistica.Estimaciondeunaproporcion.pdf" target="_blank">Inferencia estadística. Estimación de una proporción</a></h4>
<h4 style="font-size: 11px;">Unidad 10: <a href="archivos_ies/14_15/mate/Unidad14.Inferenciaestadistica.Contrastesdehipotesis.pdf" target="_blank">Inferencia estadística. Contrastes de hipótesis</a></h4>
<hr />
<p> </p>
<h3 class="post-title entry-title" style="font-size: 17px; font-family: 'Lucida Grande', 'Trebuchet MS'; letter-spacing: -1px; color: #ff6633;"> Se pueden usar también los temas de Estadística y Probabilidad que aparecen aquí:</h3>
<p> </p>
<h3><span style="text-decoration: underline;"><strong>2º BACHILLERATO CC SS II</strong></span></h3>
<h4 style="font-size: 11px;">Unidad 1: <a href="archivos_ies/14_15/mate/Unidad1.Sistemasdeecuaciones.MetododeGauss.pdf" target="_blank">Sistemas de ecuaciones. Método de Gauss</a><a href="http://sites.google.com/site/finacanocuenca/pruebas/Unidad1.Sistemasdeecuaciones.MetododeGauss.pdf"></a><a href="http://sites.google.com/site/finacanocuenca/pruebas/Unidad2.Matrices.pdf"></a></h4>
<h4 style="font-size: 11px;">Unidad 2: <a href="archivos_ies/14_15/mate/Unidad2.Matrices.pdf" target="_blank">Matrices</a></h4>
<h4 style="font-size: 11px;">Unidad 3: <a href="archivos_ies/14_15/mate/Unidad3.Determinantes.pdf" target="_blank">Determinantes</a></h4>
<h4 style="font-size: 11px;">Unidad 4: <a href="archivos_ies/14_15/mate/Unidad4.Programacionlineal.pdf" target="_blank">Programación lineal</a></h4>
<h4 style="font-size: 11px;">Ejemplo de <a href="archivos_ies/14_15/mate/Problemadeltransporte.Ejemplo.pdf" target="_blank">“Problema del transporte”</a></h4>
<h4 style="font-size: 11px;">Unidad 5: <a href="archivos_ies/14_15/mate/Unidad5.Limitesdefunciones.Continuidad.pdf" target="_blank">Límites de funciones. Continuidad</a></h4>
<p style="padding-left: 60px;">FUNCIONES ELEMENTALES: <a href="archivos_ies/14_15/mate/teoria.pdf" target="_blank">Teoría</a>  -  <a href="archivos_ies/14_15/mate/funciones_elementales.pdf" target="_blank">Ejercicios resueltos</a></p>
<h4 style="font-size: 11px;">Unidad 6: <a href="archivos_ies/14_15/mate/Unidad6.Derivadas.Tecnicasdederivacion.pdf" target="_blank">Derivadas. Técnicas de derivación</a></h4>
<h4 style="font-size: 11px;">Unidad 7: <a href="archivos_ies/14_15/mate/Unidad7.Aplicacionesdelasderivadas.pdf" target="_blank">Aplicaciones de las derivadas</a></h4>
<h4 style="font-size: 11px;">Unidad 10: <a href="archivos_ies/14_15/mate/Unidad10.Calculodeprobabilidades.pdf" target="_blank">Cálculo de probabilidades</a></h4>
<h4 style="font-size: 11px;">Unidad 11: <a href="archivos_ies/14_15/mate/Unidad11.Lasmuestrasestadisticas.pdf" target="_blank">Las muestras estadísticas</a></h4>
<h4 style="font-size: 11px;">Unidad 12: <a href="archivos_ies/14_15/mate/Unidad12.Inferenciaestadistica.Estimaciondelamedia.pdf" target="_blank">Inferencia estadística. Estimación de la media</a></h4>
<h4 style="font-size: 11px;">Problema tipo: <a href="archivos_ies/14_15/mate/Inter_COnf_Media_ProblemaICm.pdf" target="_blank">Intervalo de confianza para la media</a></h4>
<h4 style="font-size: 11px;">Unidad 13: <a href="archivos_ies/14_15/mate/Unidad13.Inferenciaestadistica.Estimaciondeunaproporcion.pdf" target="_blank">Inferencia estadística. Estimación de una proporción</a></h4>
<h4 style="font-size: 11px;">Unidad 14: <a href="archivos_ies/14_15/mate/Unidad14.Inferenciaestadistica.Contrastesdehipotesis.pdf" target="_blank">Inferencia estadística. Contrastes de hipótesis</a></h4>
<p> </p>
<p> </p>
<p><span style="text-decoration: underline;"><strong>2º BACHILLERATO CC SS II. ORIENTACIONES:</strong></span></p>
<p><span style="text-decoration: underline;"><strong><a href="archivos_ies/14_15/mate/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2013_2014.pdf" target="_blank">Documento completo.</a><br /></strong></span></p>
<p> </p>
<p><span style="text-decoration: underline;"><strong>CONTENIDOS</strong></span></p>
<p><span style="text-decoration: underline;"><strong> </strong></span></p>
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>PROBABILIDAD Y ESTADÍSTICA</strong></span></p>
<p><span style="text-decoration: underline;"><strong> </strong></span></p>
<ul>
<li>Profundización en los conceptos de probabilidades a priori y a posteriori, probabilidad compuesta, condicionada y total.</li>
<li>Teorema de Bayes.</li>
<li>Implicaciones prácticas de los teoremas: Central del límite, de aproximación de la Binomial a la Normal y Ley de los Grandes Números.</li>
<li>Problemas relacionados con la elección de las muestras. Condiciones de representatividad. Parámetros de una población.</li>
<li>Distribuciones de probabilidad de las medias y proporciones muestrales.</li>
<li>Intervalo de confianza para el parámetro p de una distribución binomial y para la media de una distribución normal de desviación típica conocida.</li>
<li>Contraste de hipótesis para la proporción de una distribución binomial y para la media o diferencias de medias de distribuciones normales con desviación típica conocida.</li>
</ul>
<p style="text-align: justify;"> </p>
<p> </p>
<p><span style="text-decoration: underline;"><strong>OBJETIVOS:</strong></span></p>
<p><span style="text-decoration: underline;"><strong> </strong></span></p>
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>PROBABILIDAD</strong></span></p>
<p> </p>
<ul>
<li>Conocer la terminología básica del Cálculo de Probabilidades.</li>
<li>Construir el espacio muestral asociado a un experimento aleatorio simple. Describir sucesos y efectuar operaciones con ellos.</li>
<li>Asignar probabilidades a sucesos aleatorios simples y compuestos, dependientes o independientes, utilizando técnicas personales de recuento, diagramas de árbol o tablas de contingencia.</li>
<li>Calcular probabilidades de sucesos utilizando las propiedades básicas de la probabilidad, entre ellas la regla de Laplace para sucesos equiprobables.</li>
<li>Construir el espacio muestral asociado a un experimento aleatorio, dado un suceso condicionante. Calcular probabilidades condicionadas.</li>
<li>Determinar si dos sucesos son independientes o no.</li>
<li>sucesos dependientes o independientes.</li>
<li>Conocer y aplicar el teorema de la probabilidad total y el teorema de Bayes, utilizando adecuadamente los conceptos de probabilidades a priori y a posteriori.</li>
</ul>
<p style="padding-left: 30px;"><br /><span style="text-decoration: underline;"><strong>INFERENCIA</strong></span></p>
<p> </p>
<ul>
<li>Conocer el vocabulario básico de la Inferencia Estadística: población, individuos, muestra, tamaño de la población, tamaño de la muestra, muestreo aleatorio.</li>
<li>Conocer algunos tipos de muestreo aleatorio: muestreo aleatorio simple y muestreo aleatorio estratificado.</li>
<li>Conocer empíricamente la diferencia entre los valores de algunos parámetros estadísticos de la población y de las muestras (proporción, media).</li>
<li>Conocer la distribución en el muestreo de la media aritmética de las muestras de una población de la que se sabe que sigue<br />una ley Normal.</li>
<li>Aplicar el resultado anterior al cálculo de probabilidades de la media muestral, para el caso de poblaciones Normales con<br />media y varianza conocidas.</li>
<li>Conocer cómo se distribuye, de manera aproximada, la proporción muestral para el caso de muestras de tamaño grande (no<br />inferior a 100).</li>
<li>Conocer el concepto de intervalo de confianza.</li>
<li>A la vista de una situación real de carácter económico o social, modelizada por medio de una distribución Normal (con varianza<br />conocida) o Binomial, el alumno debe saber:</li>
<li>Determinar un intervalo de confianza para la proporción en una población, a partir de una muestra aleatoria grande.</li>
<li>Determinar un intervalo de confianza para la media de una población Normal con varianza conocida, a partir de una muestra<br />aleatoria.</li>
<li>Determinar el tamaño muestral mínimo necesario para acotar el error cometido al estimar, por un intervalo de confianza, la<br />proporción poblacional para cualquier valor dado del nivel de confianza.</li>
<li>Determinar el tamaño muestral mínimo necesario para acotar el error cometido al estimar, por un intervalo de confianza, la<br />media de una población Normal, con varianza conocida, para cualquier valor dado del nivel de confianza.</li>
<li>Conocer el Teorema Central del límite y aplicarlo para hallar la distribución de la media muestral de una muestra de gran<br />tamaño, siempre que se conozca la desviación típica de la distribución de la variable aleatoria de la que procede la muestra.</li>
<li>Conocer el concepto de contraste de hipótesis y de nivel de significación de un contraste.</li>
<li>A la vista de una situación real de carácter económico o social, modelizada por medio de una distribución Normal (con varianza<br />conocida) o Binomial, el alumno debe saber:</li>
<li>Determinar las regiones de aceptación y de rechazo de la hipótesis nula en un contraste de hipótesis, unilateral o bilateral,<br />sobre el valor de una proporción y decidir, a partir de una muestra aleatoria adecuada, si se rechaza o se acepta la hipótesis<br />nula a un nivel de significación dado.</li>
<li>Determinar las regiones de aceptación y de rechazo de la hipótesis nula en un contraste de hipótesis, unilateral o bilateral,<br />sobre la media de una distribución Normal con varianza conocida, y decidir, a partir de una muestra aleatoria adecuada, si se<br />rechaza o se acepta la hipótesis nula a un nivel de significación dado.</li>
</ul>
<p> </p>
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>NOMENCLATURA Y NOTACIÓN UTILIZADA EN LAS PRUEBAS</strong></span></p>
<p> </p>
<ul>
<li>A<sup>C</sup> indica el contrario del suceso A.</li>
<li>El muestreo aleatorio simple se entenderá siempre “con reemplazamiento”.</li>
<li>Se entenderá por muestras grandes aquellas de tamaño n ≥ 30.</li>
<li>En los contrastes de hipótesis α indicará el nivel de significación.</li>
</ul>
<p><a href="archivos_ies/Probabilidad_resueltos.pdf" target="_blank"><strong>EJERCICIOS DE PROBABILIDAD RESUELTOS.</strong></a></p>
<p><a href="attachments/083_Muestras_estadisticas_soluciones.pdf" target="_blank"><strong>EJERCICIOS DE MUESTREO RESUELTOS.</strong></a></p>
<p><a href="archivos_ies/Inferencia_resueltos.pdf" target="_blank"><strong>EJERCICIOS DE INFERENCIA RESUELTOS.</strong></a></p>
<p> </p>
<hr />
<p> </p>
<p style="margin-top: 0px; margin-bottom: 0px; color: #000000; font-family: 'Times New Roman'; font-size: medium;"><span style="text-decoration: underline;">PENDIENTES</span></p>
<p style="margin-top: 0px; margin-bottom: 0px; color: #000000; font-family: 'Times New Roman'; font-size: medium;">Actividades de Matemáticas Aplicadas a las CCSS I para alumnos de 2º de Bachillerato que tienen pendiente esta asignatura - <a href="archivos_ies/mcs1_pendientes_1_2010-2011.pdf" target="_blank">Primera parte</a>.</p>
<hr />
<p><a href="index.php?option=com_content&amp;view=article&amp;id=33" target="_blank">ZONA PRIVADA DE MIGUEL ANGUITA.</a></p>
<p style="margin-top: 0px; margin-bottom: 0px; color: #000000; font-family: 'Times New Roman'; font-size: medium;"> </p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Matemáticas</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
                            <div class="items-row cols-2 row-1">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Dª Clotilde García Sánchez</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 29 Septiembre 2011 18:22</span> | <span class="art-postauthoricon">Escrito por Cloti GS</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=101:do-clotilde-garcia-sanchez&amp;catid=129&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=8f2a735db9eae6a50fc30c5ecb7a1afc26d81023" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3380
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2><span style="text-decoration: underline;"><strong>Página personal de la profesora&nbsp;Dª Clotilde&nbsp;García Sánchez.</strong></span></h2>
<p><span style="font-family: arial, sans-serif; font-size: x-small;" size="2" face="arial, sans-serif"><br /></span></p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Matemáticas</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
            </div>";s:4:"head";a:10:{s:5:"title";s:68:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - PROYECTOS DEL IES";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:87:"/index.php?option=com_content&amp;view=category&amp;id=129&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:88:"/index.php?option=com_content&amp;view=category&amp;id=129&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:0:{}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:37108:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Selectividad 2015</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 30 Junio 2015 16:15</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=286:selectividad-2015&amp;catid=104&amp;Itemid=599&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=3245c918bb139bfa3fc55220dbd64c02a07421f6" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 11300
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE 2015<br /><br /></span></h1>
<ul style="text-align: justify;">
<li><strong style="color: #2a2a2a;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/15  (MIÉRCOLES) POR LA TARDE EN <a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a;"> </span><strong style="color: #2a2a2a;">EXÁMENES QUE HAN SALIDO EN JUNIO 2015 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<p style="text-align: justify;"><span style="text-decoration: underline;"><strong>JUNIO 2015 - EXÁMENES</strong></span></p>
<p style="text-align: justify;"><strong>16/06/15. </strong>Martes. primer día: sin incidencias, todos a su hora con los nervios habituales.</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>COMENTARIO DE TEXTO RELACIONADO CON LA LENGUA CASTELLANA Y LA LITERATURA II</li>
<li>HISTORIA DE ESPAÑA</li>
<li>HISTORIA DE LA FILOSOFÍA</li>
<li>IDIOMA EXTRANJERO</li>
</ul>
<p style="text-align: justify;"><strong>17/06/15.</strong> Miércoles. Una incidencia. Una alumna no se presenta al examen, nervios, llamada al instituto, había decidido no presentarse ¡pero se matriculó! hubiera necesitado saberlo con antelación. Nada más destacable.</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>HISTORIA DEL ARTE</li>
<li>MATEMÁTICAS II <span style="line-height: 16.6399993896484px;">Y COLISIÓN</span></li>
<li>TÉCNICAS DE EXPR. GRÁFICO-PLASTICAS</li>
<li>QUÍMICA</li>
<li>ELECTROTÉCNIA</li>
<li>LITERATURA UNIVERSAL</li>
<li>LENGUAJE Y PRÁCTICA MUSICAL</li>
<li>TECNOLOGÍA INDUSTRIAL II</li>
<li>MATEMÁT. APLIC. A LAS CC. SOCIALES II Y COLISIÓN</li>
</ul>
<p style="text-align: justify;"><strong>18/06/15. </strong>Jueves. Dos alumnos que no se presentan, uno que llega tarde por la tarde, cosas del bus que lo ha perdido, ¡pero es que no hay taxis!</p>
<ul class="selectividad" style="color: #000000; font-family: verdana; line-height: 16.6399993896484px;">
<li>ANÁLISIS MUSICAL II (¡YA!)</li>
<li>DISEÑO <span style="line-height: 16.6399993896484px;">(¡YA!)</span></li>
<li>GEOGRAFÍA</li>
<li>BIOLOGÍA</li>
<li>DIBUJO TÉCNICO II</li>
<li>CIENCIAS DE LA TIERRA Y MEDIOAMBIENTALES</li>
<li>ECONOMÍA DE LA EMPRESA</li>
<li>GRIEGO II</li>
<li>HISTORIA DE LA MÚSICA Y LA DANZA</li>
<li>DIBUJO ARTÍSTICO II</li>
<li>FÍSICA</li>
<li>LATÍN II</li>
</ul>
<p style="text-align: justify;">Los criterios de corrección ya <a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_criterios_correccion.php" target="_blank" title="Criterios de corrección oficiales.">han sido publicados oficialmente</a> y añadidos en cada asignatura abajo.</p>
<p style="text-align: justify;">Las soluciones: más adelante. Ya están las de Matemáticas II.</p>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Biología</strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br />Ciencias de la Tierra y Medioamb.</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Comentario de Texto, Lengua Castellana y Literatura</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Artístico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Técnico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Economía <br />de la Empresa</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Diseño</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Electrotecnia</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Biologia_2014_15.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></span></strong></a><strong style="font-size: inherit;">Colisión</strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Biologiia.pdf" target="_blank">Crit. Correc.</a><br /><a href="archivos_ies/14_15/selectividad/criterios/Biologiia_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/CTM_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Ciencias%20De%20La%20Tierra.pdf" target="_blank">Crit. Correc.<br /></a><br /><a href="archivos_ies/14_15/selectividad/criterios/Ciencias%20De%20La%20Tierra_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong><br /> <a href="archivos_ies/14_15/selectividad/Comentario_lengua_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Comentario%20Texto%20Lengua%20Castellana%20Y%20Literatura.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> <a href="archivos_ies/14_15/selectividad/D_Artistico_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Colisión</span></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Dibujo%20Artiistico.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/D_Tecnico_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/DIBUJO_PAU_junio_2015_c.pdf" target="_blank">Soluciones</a><br />(Prof.  Manuel Martínez Vela<br />IES P. MANJÓN)<br /><a href="archivos_ies/14_15/selectividad/criterios/Dibujo%20Teecnico.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Economia_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Economiia%20De%20La%20Empresa.pdf" target="_blank">Crit. Correc.<br /></a><br /><a href="archivos_ies/14_15/selectividad/criterios/Economiia%20De%20La%20Empresa_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Dise%C3%B1o_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Diseno.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Disenno.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td align="center" bgcolor="#abFDe8">
<p style="text-align: center;"><a href="archivos_ies/14_15/selectividad/Electrotecnia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Colisión</strong></p>
<p style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Electrotecnia.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Crit. Correc.<br /><br /></a><a href="archivos_ies/14_15/selectividad/criterios/Electrotecnia_incom.pdf" target="_blank" style="font-size: inherit; text-decoration: none; color: #2f310d;">Crit. Incomp.</a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Dise%C3%B1o.pdf" target="_blank"><br /></a>Física</strong></td>
<td style="text-align: center;" align="center"><strong><br />Geografía</strong></td>
<td style="text-align: center;" align="center"><strong><br />Griego II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de España</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de la Filosofía</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia <br />del Arte</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong>Latín II </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong>Alemán</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Fisica_2014_15d.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-decoration: none; color: inherit; font-size: inherit; font-weight: normal; font-family: inherit; text-align: left;">Soluciones<a href="archivos_ies/13_14/selectividad/FISICA_2014_Junio.pdf" target="_blank"><br /></a></strong><a href="archivos_ies/14_15/selectividad/criterios/Fiisica.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Fiisica_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><a href="archivos_ies/14_15/selectividad/Geografia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="37" height="37" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Geografiia.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Geografiia_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> <a href="archivos_ies/14_15/selectividad/Griego_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Griego.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Griego_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong> <a href="archivos_ies/14_15/selectividad/Historia_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20Espanna.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><a href="archivos_ies/14_15/selectividad/Comentario_filosofia_2014_15.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></strong></a></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20La%20Filosofiia.pdf" target="_blank">Crit. Correc.<strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong> </strong><a href="archivos_ies/14_15/selectividad/Historia_Arte_2014_15.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Historia%20Del%20Arte.pdf" target="_blank">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong> </strong><a href="archivos_ies/14_15/selectividad/Latin_junio_e_incompatibilidades_2014_15.pdf"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Latiin.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /></a><br style="font-weight: normal; text-align: center;" /><a href="archivos_ies/14_15/selectividad/criterios/Latiin_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong></p>
</td>
<td align="center">
<p><strong style="text-align: center;"><span style="color: #2f310d;"><span style="margin-right: auto; margin-left: auto;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; margin-right: auto; margin-left: auto; display: block;" /></span></span></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"></a><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Alemaan.pdf" target="_blank">Crit. Correc.</a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Francés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Inglés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Literatura Universal</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas Aplicadas a las Ciencias Sociales II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Matemáticas II</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong><br /> Química </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong> </strong><strong>Técnicas de Exp. Gráf. Plást. (no disponible)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Tecnología Industrial II</strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Frances_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Francees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDD6">
<p><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Ingles.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Inglees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Lit_Universal_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Literatura%20Universal.pdf" target="_blank">Crit. Correc.<br /><br /></a><a href="archivos_ies/14_15/selectividad/criterios/Literatura%20Universal_incom.pdf" target="_blank">Crit. Incomp.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><a href="archivos_ies/14_15/selectividad/Mat_CCSSII_2014_15.pdf" target="_blank"><strong><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></strong></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Soluciones_MCCSSII_Junio.pdf" target="_blank">Soluciones<br /></a>(emestrda.net)</strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Criterios_cor_Matemaaticas%20Aplicadas.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a><br /></strong></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">_________</strong></p>
<strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/Mat_CCSSII_COLISIION_2014_15.pdf" target="_blank">Colisión<br /></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Criterios_cor_Matemaaticas%20Aplicadas_incom.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Incomp.</a></strong><br />_________<br /><br /></strong></td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/selectividad/Mat_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a>NUEVO: <br /></span></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/15_mod_soljun.pdf" target="_blank">Soluciones<br /><strong style="color: #000000; font-family: Arial; font-size: medium; letter-spacing: normal; text-align: -webkit-center;"><span style="font-size: 8;">(D. Germán Jesús Rubio Luna</span></strong></a>)<br /><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><a href="archivos_ies/14_15/selectividad/criterios/Matemaaticas.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a></strong></strong><br />_________<br /><a href="archivos_ies/14_15/selectividad/Mat_II_COLISION_2014_15b.pdf" target="_blank">Colisión<br /></a><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Matemaaticas_incom.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Incomp.</a></strong><br />_________</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Quimica_2014_15.pdf" target="_blank"><span style="color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></span></a></strong></p>
<strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/QUIMICA_JUNIO_2015.pdf" target="_blank">Soluciones<br /></a></strong>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">(emestrda.net)<br /><br /></strong><a href="archivos_ies/14_15/selectividad/criterios/Quiimica.pdf" target="_blank" style="font-weight: normal;">Crit. Correc.</a></strong></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abFDe8">
<p><strong style="text-align: left;"><span style="color: #2f310d;"><br /><a href="archivos_ies/14_15/selectividad/Tecn_Expr_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></span></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Teecnicas%20De%20Expresioon.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;"><span style="color: #2f310d;"><span style="font-weight: normal;">Crit. Correc.</span></span></strong></strong><strong><br /></strong></a></p>
</td>
<td align="center" bgcolor="#abF856">
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a><a href="archivos_ies/14_15/selectividad/Tec_Industrial_II_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc; margin-right: auto; margin-left: auto; display: block;" /></a></span></p>
<p><span style="color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Tecnologiia%20Industrial.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.<br /><br /></a></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Tecnologiia%20Industrial_incom.pdf" target="_blank" style="font-weight: normal;">Crit. Incomp.</a></strong><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a></span><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong>HISTORIA DE LA<br /></strong>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">MÚSICA Y LA DANZA</strong><strong> </strong><strong style="color: inherit; font-family: inherit; font-size: inherit; background-color: #fafbf0;"> </strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br /></strong><strong>LENGUAJE Y PRÁCTICA<br />MUSICAL</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><br /><strong>ANÁLISIS<br />MUSICAL II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br /></strong><strong>Lengua Extranjera II (Italiano)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Lengua Extranjera II (Portugués)</strong><strong> </strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"> </td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"> </td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#abF856">
<p><strong><a href="archivos_ies/14_15/selectividad/Historia_musica_danza_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/14_15/selectividad/criterios/Historia%20De%20La%20Muusica%20Y%20Danza.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p><strong style="color: inherit; font-family: inherit;"> <a href="archivos_ies/14_15/selectividad/Lenguaje_y_pr_musical_2014_15.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #abF856;" /></a></strong></p>
<p><a href="archivos_ies/14_15/selectividad/criterios/Lenguaje%20Y%20Praactica%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#abF856">
<p> <strong style="color: inherit; font-family: inherit; font-size: inherit; background-color: #abf856;"><a href="archivos_ies/14_15/selectividad/analisis_musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #abF856;" /></a></strong><span style="color: inherit; font-family: inherit; font-size: inherit;"> </span></p>
<p> <a href="archivos_ies/14_15/selectividad/criterios/Anaalisis%20Musical.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"><br /><br /><br /><br /> <a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Italiano.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"><br /><br /><br /><br /> <a href="archivos_ies/14_15/selectividad/criterios/Lengua%20Extranjera%20Portuguees.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-weight: normal;">Crit. Correc.</a></td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"> </td>
<td style="text-align: center;" align="center" bgcolor="#ffffff"> </td>
<td align="center" bgcolor="#ffffff"> </td>
</tr>
</tbody>
</table>
<p style="text-align: justify;"> (<a href="index.php?option=com_content&amp;view=article&amp;id=243:selectividad-2014&amp;catid=108:bachillerato&amp;Itemid=147&amp;highlight=WyJzZWxlY3RpdmlkYWQiLDIwMTQsInNlbGVjdGl2aWRhZCAyMDE0Il0=" target="_blank" title="Selectividad 2014.">Exámenes de Selectividad 2014 junio</a> y <a href="index.php?option=com_content&amp;view=article&amp;id=262kYWQiXQ==" target="_blank">septiembre</a>)</p>
<ul style="text-align: justify;">
<li>
<h3 style="margin: 1px;"><span style="text-align: center; color: #2a2a2a;">22 DE ABRIL A 4 DE JUNIO: REGISTRO PARA LAS PAU.</span></h3>
</li>
<li><span style="color: #7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;">LA MATRÍCULA SE REALIZARÁ DEL 2 AL 4 DE JUNIO Y DEL 3 AL 5 DE SEPTIEMBRE.</span></li>
</ul>
<ul style="text-align: justify;">
<li>
<h3 style="margin: 1px;">PARA CONSEGUIR EL PIN (REGISTRO PARA LAS PAU) ENTRAR EN LA SIGUIENTE WEB:</h3>
</li>
</ul>
<p style="text-align: center;"><strong><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank"><span style="color: #ffffff;">HTTPS://OFICINAVIRTUAL.UGR.ES/APLI/SOLICITUDPAU/SELECTIVIDAD</span>00-MENU.JSP</a></strong></p>
<p style="text-align: justify;"> <a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/documentacion/Parametros_AyB_UA_Bachillerato_2014_2015.pdf" target="_blank" style="font-size: 18px; font-weight: bold; text-align: left; text-transform: uppercase;">PARÁMETROS DE PONDERACIÓN, CONSULTA ESTÁTICA.</a></p>
<ul style="text-align: justify;">
<li>
<p style="margin: 1px;"><strong><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/g_b_parametros_top.php" target="_blank">CONSULTA DINÁMICA</a>.</strong></p>
</li>
<li>
<h4 style="margin: 1px;"><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/" target="_blank">DISTRITO ÚNICO ANDALUZ.</a></h4>
</li>
</ul>
<ul style="text-align: justify;">
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI ENCUENTRO DE LA UNIVERSIDAD CON LOS CENTROS.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">PRESENTACIÓN: REGISTRO PARA LA <span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">PRESENTACIÓN: MATRÍCULA DE <span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank"><span class="highlight" style="padding: 1px 4px;">SELECTIVIDAD</span>: ORIENTACIONES.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">WEB DE LA UNIVERSIDAD PARA REGISTRO Y MATRÍCULA.<br /><br /></a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">OTROS DATOS DE INTERÉS: </a></h4>
</li>
</ul>
<ol style="text-align: justify;"><ol>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">INSCRIPCIÓN</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PLAZOS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">MATRICULACIÓN</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PRECIOS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">CALENDARIO DE LAS PRUEBAS DÍA A DÍA</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">NOTA DE ACCESO</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">EXÁMENES DE OTROS AÑOS Y ORIENTACIONES</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">ORDEN DE PREFERENCIA DE CARRERAS</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PROCEDIMIENTO DE MATRÍCULA EN LA UINIVERSIDAD</a></h4>
</li>
<li>
<h4 style="margin: 1px;"><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">PLAZOS</a></h4>
</li>
</ol></ol>
<h4> </h4></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Normativa</span> / <span class="art-post-metadata-category-name">ÚLTIMA NORMATIVA</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:68:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Selectividad 2015";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:16:"Oferta Educativa";s:4:"link";s:59:"index.php?option=com_content&view=article&id=196&Itemid=599";}i:1;O:8:"stdClass":2:{s:4:"name";s:9:"Normativa";s:4:"link";s:60:"index.php?option=com_content&view=category&id=196&Itemid=599";}i:2;O:8:"stdClass":2:{s:4:"name";s:17:"ÚLTIMA NORMATIVA";s:4:"link";s:60:"index.php?option=com_content&view=category&id=104&Itemid=599";}i:3;O:8:"stdClass":2:{s:4:"name";s:17:"Selectividad 2015";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:21770:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">ESTADÍSTICA 2º BACHILLERATO CIENCIAS</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 13 Abril 2015 11:36</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >manguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=103:manguitapublico&amp;catid=129&amp;Itemid=693&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=1c90278d0ccefecc6fabb3841b654e8261c4e00f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 20540
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 style="text-align: center;"><span style="text-decoration: underline;"><strong>CURSO ACADÉMICO 2014/15</strong></span></h1>
<p> ______________________________________________________________________</p>
<p><span style="text-decoration: underline;"><strong>TRABAJOS DE LA ASIGNATURA EN LA 2ª EVALUACIÓN:</strong></span></p>
<ol>
<li><a href="fie/ryan/" target="_blank"><span style="color: #2a2a2a;">RYAN - MIGUEL - PABLO - EUSEBIO: Análisis sobre la influencia de la música sobre el poder de la concentración</span></a></li>
</ol>
<p><span style="color: #2a2a2a;">_______________________________________________________________________</span></p>
<p> </p>
<h3><span style="text-decoration: underline;"><strong>ESTADISTICA</strong></span></h3>
<p><a href="archivos_ies/14_15/mate/PROGRAMACION_DE_ESTADISTICA.pdf" target="_blank">PROGRAMACIÓN COMPLETA.</a></p>
<p> </p>
<p><a href="archivos_ies/14_15/mate/programacion_estadistica.pdf" target="_blank">Programación.</a></p>
<ul>
<li><a href="archivos_ies/14_15/mate/ESTADISTICA_MCGRAWHILL.pdf" target="_blank">Descriptiva MCGraw Hill.</a></li>
<li><a href="archivos_ies/14_15/mate/curso_estadistica.pdf" target="_blank">Curso de Estadística completo.</a></li>
<li><a href="archivos_ies/14_15/mate/Estadistica2.pdf" target="_blank">Presentación PPT.</a></li>
</ul>
<p style="padding-left: 30px;"><span style="color: #414141;">Unidad 1: Lenguaje estadístico y </span><span style="color: #414141;">Distribuciones Unidimensionales.</span></p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/mate/1Nociones_Generales.pdf" target="_blank">Nociones generales.</a></li>
<li><a href="archivos_ies/14_15/mate/2Tablas%20-Gráficos_problemas.pdf" target="_blank">Tablas y gráficos, ejercicios.</a></li>
<li><a href="archivos_ies/14_15/mate/3graficos.pdf" target="_blank">Diferentes gráficos.</a></li>
<li><a href="archivos_ies/14_15/mate/4Parametros_Estadisticos.pdf" target="_blank">Parámetros estadísticos.</a></li>
<li><a href="archivos_ies/14_15/mate/Ej_resueltos_Excel.pdf" target="_blank">Ejercicios resueltos con Excel.</a></li>
</ul>
</ul>
<ul>
<li>Unidad 2: Distribuciones bidimensionales</li>
<li>Unidad 3: Teoría de Conjuntos</li>
<li>Unidad 4: Técnicas para contar. Combinatoria</li>
<li>Unidad 5: Sucesos aleatorios. Probabilidad</li>
<li>Unidad 6: Modelos probabilísticos discretos</li>
<li>Unidad 7: Modelos probabilísticos continuos</li>
<li><span style="text-decoration: underline;"><strong>Unidad 8: Muestreo</strong></span><br /><strong><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/2bach_matccssii/muestreoteoria.pdf" target="_blank">MUESTREO. DISTRIBUCIONES MUESTRALES</a> - <a href="archivos_ies/13_14/2bach_matccssii/muestreoejercicios.pdf" target="_blank">EJERCICIOS</a> - <a href="archivos_ies/13_14/2bach_matccssii/MUESTREOSolucionario.pdf" target="_blank">SOLUCIONES<br /><br /></a></span></span></span></strong><a href="archivos_ies/13_14/2bach_matccssii/muestreo_resumen.pdf" target="_blank"><span style="color: #2f310d; font-family: Arial,Helvetica,sans-serif;"><span style="font-size: 19px; letter-spacing: 1.33333px;"><strong>Resumen de muestreo.<br /><br /></strong></span></span></a><strong><span style="color: #3366ff; text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201345.pdf">Selectividad resueltos: 2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201244.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201149.pdf">2011</a> ; </span><span style="text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201045.pdf">2010</a></span>; </strong><a href="http://emestrada.files.wordpress.com/2010/02/2009_t6.pdf"><span style="color: #3366ff;"><strong>2009</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2008_t6.pdf"><span style="color: #3366ff;"><strong>2008</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2007_t6.pdf"><span style="color: #3366ff;"><strong>2007</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2006_t6.pdf"><span style="color: #3366ff;"><strong>2006</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong>; </strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/2005_t6.pdf"><span style="color: #3366ff;"><strong>2005</strong></span></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/200414.pdf"><span style="color: #0000ff;">2004</span></a></span></strong><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><span style="color: #3366ff;"><strong><span style="color: #3366ff;">;</span></strong></span> </a><strong><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/200310.pdf"><span style="color: #0000ff;">2003</span></a></span></strong><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/2002_t6.pdf"><strong><span style="color: #3366ff;">2002</span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/20109.pdf"><strong><span style="color: #3366ff;">; </span></strong></a><a href="http://emestrada.files.wordpress.com/2010/02/2001_t6.pdf"><strong><span style="color: #3366ff;">2001<br /><br /></span></strong></a></li>
<li><strong><span style="text-decoration: underline;">Unidad 9: Introducción a la Inferencia.<span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; letter-spacing: 1pt; color: #71cd63; text-decoration: underline;"><br /></span></span></span><span style="font-size: 14pt;"><span class="tituloItem" style="font-family: Arial,Helvetica,sans-serif; letter-spacing: 1pt; color: #71cd63;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/2bach_matccssii/inferencia_tema.pdf" target="_blank">INFERENCIA ESTADÍSTICA. ESTIMACIÓN. CONTRASTE DE HIPÓTESIS</a> - SOLUCIONES<br /></span></span></span></strong><strong><span style="color: #3366ff; text-decoration: underline;"><a href="http://emestrada.files.wordpress.com/2010/02/201345.pdf">Selectividad resueltos:  </a></span></strong><strong><span style="color: #3366ff;"><span style="color: #0000ff;"><a href="http://emestrada.files.wordpress.com/2010/02/201346.pdf">2013</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201232.pdf">2012</a>; <a href="http://emestrada.files.wordpress.com/2010/02/201163.pdf"><span style="color: #0000ff;">2011</span></a></span>; <a href="http://emestrada.files.wordpress.com/2010/02/201054.pdf">2010</a></span></strong></li>
</ul>
<p><strong><span style="color: #3366ff;"> </span></strong></p>
<p><a href="archivos_ies/14_15/mate/excel" target="_blank">EJERCICIOS RESUELTOS CON EXCEL. TODOS LOS TEMAS.</a></p>
<h4 style="font-size: 11px;">Unidad 6: <a href="archivos_ies/14_15/mate/Unidad10.Calculodeprobabilidades.pdf" target="_blank">Cálculo de probabilidades</a></h4>
<h4 style="font-size: 11px;">Unidad 9: <a href="archivos_ies/14_15/mate/Unidad11.Lasmuestrasestadisticas.pdf" target="_blank">Las muestras estadísticas</a></h4>
<h4 style="font-size: 11px;">Unidad 10: <a href="archivos_ies/14_15/mate/Unidad12.Inferenciaestadistica.Estimaciondelamedia.pdf" target="_blank">Inferencia estadística. Estimación de la media</a></h4>
<h4 style="font-size: 11px;">Problema tipo: <a href="archivos_ies/14_15/mate/Inter_COnf_Media_ProblemaICm.pdf" target="_blank">Intervalo de confianza para la media</a></h4>
<h4 style="font-size: 11px;">Unidad 10: <a href="archivos_ies/14_15/mate/Unidad13.Inferenciaestadistica.Estimaciondeunaproporcion.pdf" target="_blank">Inferencia estadística. Estimación de una proporción</a></h4>
<h4 style="font-size: 11px;">Unidad 10: <a href="archivos_ies/14_15/mate/Unidad14.Inferenciaestadistica.Contrastesdehipotesis.pdf" target="_blank">Inferencia estadística. Contrastes de hipótesis</a></h4>
<hr />
<p> </p>
<h3 class="post-title entry-title" style="font-size: 17px; font-family: 'Lucida Grande', 'Trebuchet MS'; letter-spacing: -1px; color: #ff6633;"> Se pueden usar también los temas de Estadística y Probabilidad que aparecen aquí:</h3>
<p> </p>
<h3><span style="text-decoration: underline;"><strong>2º BACHILLERATO CC SS II</strong></span></h3>
<h4 style="font-size: 11px;">Unidad 1: <a href="archivos_ies/14_15/mate/Unidad1.Sistemasdeecuaciones.MetododeGauss.pdf" target="_blank">Sistemas de ecuaciones. Método de Gauss</a><a href="http://sites.google.com/site/finacanocuenca/pruebas/Unidad1.Sistemasdeecuaciones.MetododeGauss.pdf"></a><a href="http://sites.google.com/site/finacanocuenca/pruebas/Unidad2.Matrices.pdf"></a></h4>
<h4 style="font-size: 11px;">Unidad 2: <a href="archivos_ies/14_15/mate/Unidad2.Matrices.pdf" target="_blank">Matrices</a></h4>
<h4 style="font-size: 11px;">Unidad 3: <a href="archivos_ies/14_15/mate/Unidad3.Determinantes.pdf" target="_blank">Determinantes</a></h4>
<h4 style="font-size: 11px;">Unidad 4: <a href="archivos_ies/14_15/mate/Unidad4.Programacionlineal.pdf" target="_blank">Programación lineal</a></h4>
<h4 style="font-size: 11px;">Ejemplo de <a href="archivos_ies/14_15/mate/Problemadeltransporte.Ejemplo.pdf" target="_blank">“Problema del transporte”</a></h4>
<h4 style="font-size: 11px;">Unidad 5: <a href="archivos_ies/14_15/mate/Unidad5.Limitesdefunciones.Continuidad.pdf" target="_blank">Límites de funciones. Continuidad</a></h4>
<p style="padding-left: 60px;">FUNCIONES ELEMENTALES: <a href="archivos_ies/14_15/mate/teoria.pdf" target="_blank">Teoría</a>  -  <a href="archivos_ies/14_15/mate/funciones_elementales.pdf" target="_blank">Ejercicios resueltos</a></p>
<h4 style="font-size: 11px;">Unidad 6: <a href="archivos_ies/14_15/mate/Unidad6.Derivadas.Tecnicasdederivacion.pdf" target="_blank">Derivadas. Técnicas de derivación</a></h4>
<h4 style="font-size: 11px;">Unidad 7: <a href="archivos_ies/14_15/mate/Unidad7.Aplicacionesdelasderivadas.pdf" target="_blank">Aplicaciones de las derivadas</a></h4>
<h4 style="font-size: 11px;">Unidad 10: <a href="archivos_ies/14_15/mate/Unidad10.Calculodeprobabilidades.pdf" target="_blank">Cálculo de probabilidades</a></h4>
<h4 style="font-size: 11px;">Unidad 11: <a href="archivos_ies/14_15/mate/Unidad11.Lasmuestrasestadisticas.pdf" target="_blank">Las muestras estadísticas</a></h4>
<h4 style="font-size: 11px;">Unidad 12: <a href="archivos_ies/14_15/mate/Unidad12.Inferenciaestadistica.Estimaciondelamedia.pdf" target="_blank">Inferencia estadística. Estimación de la media</a></h4>
<h4 style="font-size: 11px;">Problema tipo: <a href="archivos_ies/14_15/mate/Inter_COnf_Media_ProblemaICm.pdf" target="_blank">Intervalo de confianza para la media</a></h4>
<h4 style="font-size: 11px;">Unidad 13: <a href="archivos_ies/14_15/mate/Unidad13.Inferenciaestadistica.Estimaciondeunaproporcion.pdf" target="_blank">Inferencia estadística. Estimación de una proporción</a></h4>
<h4 style="font-size: 11px;">Unidad 14: <a href="archivos_ies/14_15/mate/Unidad14.Inferenciaestadistica.Contrastesdehipotesis.pdf" target="_blank">Inferencia estadística. Contrastes de hipótesis</a></h4>
<p> </p>
<p> </p>
<p><span style="text-decoration: underline;"><strong>2º BACHILLERATO CC SS II. ORIENTACIONES:</strong></span></p>
<p><span style="text-decoration: underline;"><strong><a href="archivos_ies/14_15/mate/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2013_2014.pdf" target="_blank">Documento completo.</a><br /></strong></span></p>
<p> </p>
<p><span style="text-decoration: underline;"><strong>CONTENIDOS</strong></span></p>
<p><span style="text-decoration: underline;"><strong> </strong></span></p>
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>PROBABILIDAD Y ESTADÍSTICA</strong></span></p>
<p><span style="text-decoration: underline;"><strong> </strong></span></p>
<ul>
<li>Profundización en los conceptos de probabilidades a priori y a posteriori, probabilidad compuesta, condicionada y total.</li>
<li>Teorema de Bayes.</li>
<li>Implicaciones prácticas de los teoremas: Central del límite, de aproximación de la Binomial a la Normal y Ley de los Grandes Números.</li>
<li>Problemas relacionados con la elección de las muestras. Condiciones de representatividad. Parámetros de una población.</li>
<li>Distribuciones de probabilidad de las medias y proporciones muestrales.</li>
<li>Intervalo de confianza para el parámetro p de una distribución binomial y para la media de una distribución normal de desviación típica conocida.</li>
<li>Contraste de hipótesis para la proporción de una distribución binomial y para la media o diferencias de medias de distribuciones normales con desviación típica conocida.</li>
</ul>
<p style="text-align: justify;"> </p>
<p> </p>
<p><span style="text-decoration: underline;"><strong>OBJETIVOS:</strong></span></p>
<p><span style="text-decoration: underline;"><strong> </strong></span></p>
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>PROBABILIDAD</strong></span></p>
<p> </p>
<ul>
<li>Conocer la terminología básica del Cálculo de Probabilidades.</li>
<li>Construir el espacio muestral asociado a un experimento aleatorio simple. Describir sucesos y efectuar operaciones con ellos.</li>
<li>Asignar probabilidades a sucesos aleatorios simples y compuestos, dependientes o independientes, utilizando técnicas personales de recuento, diagramas de árbol o tablas de contingencia.</li>
<li>Calcular probabilidades de sucesos utilizando las propiedades básicas de la probabilidad, entre ellas la regla de Laplace para sucesos equiprobables.</li>
<li>Construir el espacio muestral asociado a un experimento aleatorio, dado un suceso condicionante. Calcular probabilidades condicionadas.</li>
<li>Determinar si dos sucesos son independientes o no.</li>
<li>sucesos dependientes o independientes.</li>
<li>Conocer y aplicar el teorema de la probabilidad total y el teorema de Bayes, utilizando adecuadamente los conceptos de probabilidades a priori y a posteriori.</li>
</ul>
<p style="padding-left: 30px;"><br /><span style="text-decoration: underline;"><strong>INFERENCIA</strong></span></p>
<p> </p>
<ul>
<li>Conocer el vocabulario básico de la Inferencia Estadística: población, individuos, muestra, tamaño de la población, tamaño de la muestra, muestreo aleatorio.</li>
<li>Conocer algunos tipos de muestreo aleatorio: muestreo aleatorio simple y muestreo aleatorio estratificado.</li>
<li>Conocer empíricamente la diferencia entre los valores de algunos parámetros estadísticos de la población y de las muestras (proporción, media).</li>
<li>Conocer la distribución en el muestreo de la media aritmética de las muestras de una población de la que se sabe que sigue<br />una ley Normal.</li>
<li>Aplicar el resultado anterior al cálculo de probabilidades de la media muestral, para el caso de poblaciones Normales con<br />media y varianza conocidas.</li>
<li>Conocer cómo se distribuye, de manera aproximada, la proporción muestral para el caso de muestras de tamaño grande (no<br />inferior a 100).</li>
<li>Conocer el concepto de intervalo de confianza.</li>
<li>A la vista de una situación real de carácter económico o social, modelizada por medio de una distribución Normal (con varianza<br />conocida) o Binomial, el alumno debe saber:</li>
<li>Determinar un intervalo de confianza para la proporción en una población, a partir de una muestra aleatoria grande.</li>
<li>Determinar un intervalo de confianza para la media de una población Normal con varianza conocida, a partir de una muestra<br />aleatoria.</li>
<li>Determinar el tamaño muestral mínimo necesario para acotar el error cometido al estimar, por un intervalo de confianza, la<br />proporción poblacional para cualquier valor dado del nivel de confianza.</li>
<li>Determinar el tamaño muestral mínimo necesario para acotar el error cometido al estimar, por un intervalo de confianza, la<br />media de una población Normal, con varianza conocida, para cualquier valor dado del nivel de confianza.</li>
<li>Conocer el Teorema Central del límite y aplicarlo para hallar la distribución de la media muestral de una muestra de gran<br />tamaño, siempre que se conozca la desviación típica de la distribución de la variable aleatoria de la que procede la muestra.</li>
<li>Conocer el concepto de contraste de hipótesis y de nivel de significación de un contraste.</li>
<li>A la vista de una situación real de carácter económico o social, modelizada por medio de una distribución Normal (con varianza<br />conocida) o Binomial, el alumno debe saber:</li>
<li>Determinar las regiones de aceptación y de rechazo de la hipótesis nula en un contraste de hipótesis, unilateral o bilateral,<br />sobre el valor de una proporción y decidir, a partir de una muestra aleatoria adecuada, si se rechaza o se acepta la hipótesis<br />nula a un nivel de significación dado.</li>
<li>Determinar las regiones de aceptación y de rechazo de la hipótesis nula en un contraste de hipótesis, unilateral o bilateral,<br />sobre la media de una distribución Normal con varianza conocida, y decidir, a partir de una muestra aleatoria adecuada, si se<br />rechaza o se acepta la hipótesis nula a un nivel de significación dado.</li>
</ul>
<p> </p>
<p style="padding-left: 30px;"><span style="text-decoration: underline;"><strong>NOMENCLATURA Y NOTACIÓN UTILIZADA EN LAS PRUEBAS</strong></span></p>
<p> </p>
<ul>
<li>A<sup>C</sup> indica el contrario del suceso A.</li>
<li>El muestreo aleatorio simple se entenderá siempre “con reemplazamiento”.</li>
<li>Se entenderá por muestras grandes aquellas de tamaño n ≥ 30.</li>
<li>En los contrastes de hipótesis α indicará el nivel de significación.</li>
</ul>
<p><a href="archivos_ies/Probabilidad_resueltos.pdf" target="_blank"><strong>EJERCICIOS DE PROBABILIDAD RESUELTOS.</strong></a></p>
<p><a href="attachments/083_Muestras_estadisticas_soluciones.pdf" target="_blank"><strong>EJERCICIOS DE MUESTREO RESUELTOS.</strong></a></p>
<p><a href="archivos_ies/Inferencia_resueltos.pdf" target="_blank"><strong>EJERCICIOS DE INFERENCIA RESUELTOS.</strong></a></p>
<p> </p>
<hr />
<p> </p>
<p style="margin-top: 0px; margin-bottom: 0px; color: #000000; font-family: 'Times New Roman'; font-size: medium;"><span style="text-decoration: underline;">PENDIENTES</span></p>
<p style="margin-top: 0px; margin-bottom: 0px; color: #000000; font-family: 'Times New Roman'; font-size: medium;">Actividades de Matemáticas Aplicadas a las CCSS I para alumnos de 2º de Bachillerato que tienen pendiente esta asignatura - <a href="archivos_ies/mcs1_pendientes_1_2010-2011.pdf" target="_blank">Primera parte</a>.</p>
<hr />
<p><a href="index.php?option=com_content&amp;view=article&amp;id=33" target="_blank">ZONA PRIVADA DE MIGUEL ANGUITA.</a></p>
<p style="margin-top: 0px; margin-bottom: 0px; color: #000000; font-family: 'Times New Roman'; font-size: medium;"> </p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Matemáticas</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:63:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - ESTADÍSTICA";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:8:"manguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:30:"Área Científica-Tecnológica";s:4:"link";s:59:"index.php?option=com_content&view=article&id=185&Itemid=571";}i:2;O:8:"stdClass":2:{s:4:"name";s:21:"Dpto. de Matemáticas";s:4:"link";s:58:"index.php?option=com_content&view=article&id=51&Itemid=103";}i:3;O:8:"stdClass":2:{s:4:"name";s:12:"ESTADÍSTICA";s:4:"link";s:59:"index.php?option=com_content&view=article&id=103&Itemid=693";}}s:6:"module";a:0:{}}
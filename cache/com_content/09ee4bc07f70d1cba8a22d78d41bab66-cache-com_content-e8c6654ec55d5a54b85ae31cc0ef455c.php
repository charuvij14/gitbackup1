<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12088:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">1º ESO</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 23 Junio 2015 07:57</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=288:1-eso&amp;catid=99&amp;Itemid=725&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=c98356729dc27dce57819b560df1f1e83ae2f02b" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1912
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 5px; padding: 5px; font-size: 13.3333px; text-align: center; text-transform: none; color: #2a2a2a; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 25px; background-color: #ffffff;"><a href="archivos_ies/14_15/programaciones/Cuadernillo_padres_1ESO_14_15e.pdf" target="_blank">PROGRAMACIÓN DE 1º ESO DE MATEMÁTICAS<br />CURSO 2014/15</a></h3>
<p style="font-size: 11.8181819915771px; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;"><strong style="font-size: 14pt;"><span style="color: #000000;">MATEMÁTICAS DE 1º ESO. CURSO 2014/15</span></strong></span></p>
<p style="text-align: center; background-color: #e7eaad;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR</strong></span><strong style="text-align: left; color: #414141;"> PARA SEPTIEMBRE 2015</strong><strong>:</strong></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><strong style="font-size: 14pt;"><span style="color: #000000;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank" style="font-size: 13px; font-weight: normal; background-color: #e7eaad;"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a></span></strong></p>
<p style="font-size: 11.8181819915771px; text-align: center; background-color: #ffffff;"> </p>
<hr style="text-align: center; background-color: #e7eaad;" />
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><strong style="font-size: 14pt;"><span style="color: #000000;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank" style="font-size: 13px; font-weight: normal; background-color: #e7eaad;">UNIDAD 11</a> </span></span></strong><span style="text-align: left;">(No estudiado en el curso 14/15)</span></p>
<ul style="background-color: #e7eaad;">
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" title="Pulsar para descargar." style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> EN SEPTIEMBRE 2015,</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,11,12,13,19,21,28.</li>
<li><strong>Tema 2:</strong>1,6,8,14,16,17,18,19,20,23.</li>
<li><strong>Tema 3:</strong> 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43.</li>
<li><strong>Tema 4:</strong> Todos.</li>
<li><strong>Tema 5:</strong> 1,2,5,8,9,10,11,12,13.</li>
<li><strong>Tema 6:</strong> 1,2,8,11,29,31,32.</li>
</ul>
</ul>
<p style="background-color: #e7eaad;"><strong>TEMA 1</strong>. Números naturales</p>
<p style="background-color: #e7eaad;"><strong>TEMA 2.</strong> Divisibilidad</p>
<p style="background-color: #e7eaad;"><strong>TEMA 3.</strong> Fracciones</p>
<p style="background-color: #e7eaad;"><strong>TEMA 4.</strong>  Números decimales</p>
<p style="background-color: #e7eaad;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p style="background-color: #e7eaad;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p style="text-align: center; background-color: #ffffff;"><span style="text-align: left;"><span style="font-family: Arial, sans-serif; background-color: #e7eaad;"> </span> </span></p>
<ul style="background-color: #e7eaad;">
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR<strong style="color: #414141; letter-spacing: normal; text-align: left;"> EN SEPTIEMBRE 2015</strong>.  </a></strong></span><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">Solamente los siguientes números por tema:</a>
<ul style="margin-top: 0.5em; margin-bottom: 0.5em;">
<li><strong>Tema 7:</strong>1,4,6,7,8,9,10,11,12,13.</li>
<li><strong>Tema 8:</strong>2,3,6,7,9,11,12,13,14,15.</li>
<li><strong>Tema 9: TODOS.</strong></li>
<li><strong>Tema 10:</strong>1,3,4,5,6,7,8,11,13,14.</li>
<li><strong>Tema 11:</strong> <strong>TODOS.</strong></li>
</ul>
</li>
</ul>
<p style="text-align: left; background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">TEMA 7.</strong><span style="color: inherit; font-family: inherit; font-size: inherit;"> Sistema Métrico Decimal</span></p>
<p style="background-color: #e7eaad;"><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p style="background-color: #e7eaad;"><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p style="background-color: #e7eaad;"><strong>TEMA 10.</strong> Polígonos</p>
<p style="background-color: #e7eaad;"><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p style="text-align: center; background-color: #ffffff;"><span style="text-align: left;"><span style="background-color: #e7eaad;"> </span> </span></p>
<p style="text-align: center; background-color: #ffffff;"><span style="text-align: left;"> </span></p>
<table class="borde_outset" style="text-align: center; color: #2a2a2a;" border="1">
<tbody>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" title="Pulsar para descargar." style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a>,</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,11,12,13,19,21,28.</li>
<li><strong>Tema 2:</strong>1,6,8,14,16,17,18,19,20,23.</li>
<li><strong>Tema 3:</strong> 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43.</li>
<li><strong>Tema 4:</strong> Todos.</li>
<li><strong>Tema 5:</strong> 1,2,5,8,9,10,11,12,13.</li>
<li><strong>Tema 6:</strong> 1,2,8,11,29,31,32.</li>
</ul>
</ul>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 22/01/2015</strong></span></p>
<p><strong>TEMA 1</strong>. Números naturales</p>
<p><strong>TEMA 2.</strong> Divisibilidad</p>
<p><strong>TEMA 3.</strong> Fracciones</p>
<p><strong>TEMA 4.</strong>  Números decimales</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p><span style="font-family: Arial, sans-serif;"> </span></p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR.  </a></strong></span><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">Solamente los siguientes números por tema:</a>
<ul style="margin-top: 0.5em; margin-bottom: 0.5em;">
<li><strong>Tema 7:</strong>1,4,6,7,8,9,10,11,12,13.</li>
<li><strong>Tema 8:</strong>2,3,6,7,9,11,12,13,14,15.</li>
<li><strong>Tema 9: TODOS.</strong></li>
<li><strong>Tema 10:</strong>1,3,4,5,6,7,8,11,13,14.</li>
<li><strong>Tema 11:</strong> <strong>TODOS.</strong></li>
</ul>
</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 16/04/2015</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p><strong>TEMA 10.</strong> Polígonos</p>
<p><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p> </p>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">MATEMÁTICAS</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:58:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - 1º ESO";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:30:"Área Científica-Tecnológica";s:4:"link";s:59:"index.php?option=com_content&view=article&id=185&Itemid=571";}i:2;O:8:"stdClass":2:{s:4:"name";s:21:"Dpto. de Matemáticas";s:4:"link";s:58:"index.php?option=com_content&view=article&id=51&Itemid=103";}i:3;O:8:"stdClass":2:{s:4:"name";s:7:"1º ESO";s:4:"link";s:59:"index.php?option=com_content&view=article&id=288&Itemid=725";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:11544:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">El Niño de las Pinturas</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">EL NIÑO DE LAS PINTURAS</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 16 Diciembre 2012 09:21</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=174:nino-pinturas&amp;catid=199&amp;Itemid=78&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=f1db393d5551d94a9bbeb5588a2e3df723bd2051" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2765
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p>&nbsp;</p>
<table style="width: 98%; font-family: 'Times New Roman'; font-size: medium;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td bgcolor="#000033" valign="top" rowspan="3" width="4">&nbsp;</td>
</tr>
<tr>
<td bgcolor="#eef0f9" height="36" width="8">&nbsp;</td>
<td style="color: #0033cc; font-weight: normal; font-family: 'Times New Roman',Times,serif; font-size: xx-large;" class="Estilo21" bgcolor="#eef0f9" valign="top" width="95%">
<p style="color: #000033; font-size: medium;" class="Estilo18" align="center"><img border="0" alt="." src="archivos_ies/actividades/imagenes/ninopinturas/nptitulo.jpg" width="90%" height="149" /></p>
<p style="color: #000033; font-size: medium;" class="Estilo18" align="left"><br /><span style="color: #000033;" class="Estilo27"><a href="archivos_ies/actividades/imagenes/ninopinturas/NP%20008.jpg" target="_blank"><img border="0" hspace="5" alt="." vspace="5" align="right" src="archivos_ies/actividades/imagenes/ninopinturas/NP%20008P.jpg" width="200" height="150" /></a><a href="archivos_ies/actividades/imagenes/ninopinturas/NP%20007.jpg" target="_blank"><img class="foto_izd" border="0" hspace="5" alt="." vspace="5" align="left" src="archivos_ies/actividades/imagenes/ninopinturas/NP%20007P.jpg" width="200" height="150" /></a><span style="font-family: 'Times New Roman',Times,serif;" class="Estilo23">Literatura y&nbsp; música; lectura y melodía que la acompase; viaje a mundos posibles o imposibles con sus sonidos imaginados.</span></span></p>
<p>Esa fue la idea que propusimos a Raúl Ruiz,<em> Sex o El niño de las pinturas</em> para que, con su exuberante impronta creativa, la plasmara en los seis muros de hormigón que jalonan la escalinata de entrada al I.E.S. Miguel de Cervantes de Granada.</p>
<p style="color: #000033; font-size: medium;" class="Estilo18" align="left"><span style="color: #000033;" class="Estilo27"><br /><em><em><em><em><em><em><em><em><a href="archivos_ies/actividades/imagenes/ninopinturas/NP%20006.jpg" target="_blank"><img class="foto_izd" border="0" hspace="5" vspace="5" align="left" src="archivos_ies/actividades/imagenes/ninopinturas/NP%20006P.jpg" width="200" height="287" /></a></em></em></em></em></em></em></em></em></span><span style="font-family: 'Times New Roman',Times,serif;" class="Estilo23">Además dedar entrada en un centro educativo a este arte urbano, a veces tan denostado, pero tan cercano a la sensibilidad de la juventudactual, y darle certificado de calidad, aceptación y garantía de continuidad, queríamos que fueran representación gráfica, improntavisual y tarjeta de presentación de unas inquietudes que van del desarrollo de la inteligencia y sensibilidad, al fomento de la lectura y la</span><span style="color: #000033;" class="Estilo27"> <em><em><em><em><a href="archivos_ies/actividades/imagenes/ninopinturas/NP%20005.jpg" target="_blank"><em><em><em><em> </em></em></em></em></a><em><em><em><em><a href="archivos_ies/actividades/html/imagenes/ninopinturas/NP%20005.jpg" target="_blank"><img border="0" hspace="5" alt="." vspace="5" align="left" src="archivos_ies/actividades/imagenes/ninopinturas/NP%20005P.jpg" width="200" height="169" /></a></em></em></em></em></em></em><a href="archivos_ies/actividades/imagenes/ninopinturas/NP%20004.jpg" target="_blank"><img border="0" hspace="5" alt="." vspace="5" align="left" src="archivos_ies/actividades/imagenes/ninopinturas/NP%20004P.jpg" width="200" height="150" /></a></em></em></span><span style="font-family: 'Times New Roman',Times,serif;" class="Estilo23">emotividad; del conocimiento del mundo y sus secretos, al disfrute de&nbsp; sus ecos melodiosos o estridentes. Libros, música y pintura; literatura, melodías y colorido universal para endulzar la vida.<br /></span></p>
<p style="font-family: 'Times New Roman',Times,serif;" class="Estilo23" align="left"><span style="font-family: 'Times New Roman',Times,serif; font-size: medium; color: #000033;" class="Estilo29">Cada mañana, al subir la escalinata, tras el deslumbrante fogonazo multicolor de los murales, la vista se irá posando en cada uno de ellos para, finalmente, recorrer la secuencia escrita que los une y les da sentido global:</span></p>
<p style="font-family: 'Times New Roman',Times,serif; font-size: medium; color: #000033;" class="Estilo29" align="left"><em>Con pasos pequeños, las palabras se convierten </em>(1º Izq.)&nbsp;<em>en la música del tiempo. </em>(1º Der.)</p>
<p style="font-family: 'Times New Roman',Times,serif; font-size: medium;" class="Estilo28" align="left"><span style="font-family: 'Times New Roman',Times,serif; font-size: medium; color: #000033;" class="Estilo29">O este otro:&nbsp;<em>Los caminos antiguos </em>(2º Izq.)&nbsp;<em>nos avisan de que </em>(2º Der.)&nbsp;<em>el silencio del mundo no existe</em> (3º Izq.)<em> para la mirada que escucha. </em>(3º Der.)<a href="archivos_ies/actividades/imagenes/ninopinturas/NP%20001.jpg" target="_blank"><img class="foto_izd" border="0" hspace="5" alt="." vspace="5" align="left" src="archivos_ies/actividades/imagenes/ninopinturas/NP%20001P.jpg" width="200" height="150" /></a></span><br /><a href="archivos_ies/actividades/imagenes/ninopinturas/ENP%20014.jpg" target="_blank"><img class="foto_izd" border="0" hspace="5" alt="." vspace="5" align="left" src="archivos_ies/actividades/imagenes/ninopinturas/ENP%20014P.jpg" width="200" height="117" /></a><span style="font-family: 'Times New Roman',Times,serif; font-size: medium; color: #000033;" class="Estilo29">Un extasiado D. Quijote arrancará a nuestros inquietos jóvenes una leve sonrisa y les recordará que es posible vivir e imaginar un mundo mejor a través de la lectura y que en ella pueden encontrar satisfacción a su insaciable necesidad de aventura. El busto de Cervantes, acompañado de un incrédulo Quijote ante la visión de su deseada Dulcinea y con un mundo detrás que se difumina y queda en silencio, preside uno de los murales y hace sentir la llamada de la ilusión, de la fantasía y la nobleza de las causas justas.</span><em><a href="archivos_ies/actividades/html/imagenes/ninopinturas/ENP%20006.jpg" target="_blank"><em>&nbsp;</em></a></em></p>
<p style="color: #000033;" class="Estilo27" align="center"><em><a href="archivos_ies/actividades/imagenes/ninopinturas/ENP%20006.jpg" target="_blank"><em>&nbsp;</em></a><em><a href="archivos_ies/actividades/html/imagenes/ninopinturas/ENP%20006.jpg" target="_blank"><em><em>&nbsp;</em></em></a><em><em><a href="archivos_ies/actividades/html/imagenes/ninopinturas/ENP%20006.jpg" target="_blank"><span style="font-family: 'Times New Roman',Times,serif;" class="Estilo23"><img border="0" hspace="5" alt="." vspace="5" align="left" src="archivos_ies/actividades/imagenes/ninopinturas/ENP%20006P.jpg" width="400" height="184" /></span></a></em></em></em></em></p>
<p style="font-family: 'Times New Roman',Times,serif; font-size: medium; color: #000033;" class="Estilo29" align="left">La literatura, especialmente la frase perturbadora ysugerente y también el micro relato, ha ido siempre unida a la pintura de este artista excelente y cervantino. Lo califico de excelente, porque su arte llega a emocionar; y de cervantino, porque Raúl es miembro activo de esa familia universal, alimentada no sólo por el espíritu soñador de D. Quijote, sino también por su ejemplo de honestidad y valentía, por su satisfacción por el trabajo hecho a conciencia, por su lucha por las causas justas y, sobre todo, por su inquebrantable rebeldía ante la injusticia, las desigualdades y los abusos de los poderosos.&nbsp;&nbsp;<br />Y todas estas reflexiones no han sido gratuitamente traídas a colación, sino que se desprenden de su larga e intensa trayectoria artística, no visible en la quietud utópica, aristocrática e interesada de los museos, sino en la gratuidad altruista de paredones olvidados.</p>
<p style="color: #000033; font-size: medium;" class="Estilo18" align="center"><span style="font-family: 'Times New Roman',Times,serif;" class="Estilo23"><em><a href="archivos_ies/actividades/imagenes/ninopinturas/ENP%20009.jpg" target="_blank"><img border="0" hspace="5" alt="." vspace="5" align="baseline" src="archivos_ies/actividades/imagenes/ninopinturas/ENP%20009P.jpg" width="300" height="117" /></a></em></span></p>
<p style="font-family: 'Times New Roman',Times,serif; font-size: medium; color: #000033;" class="Estilo29" align="left">Esgrimiendo como lanza un bote de spray, y a galope del caballo de la trepidante vida moderna, ha tratado de agitar la conciencia social, reivindicar la usurpada parcela de libertad individual y gritar, en nombre de todas las conciencias de bien, aquello de&nbsp;<em>¿Quién juega con nuestros hijos?</em>,&nbsp;<em>¿Dónde nos llevan nuestros pasos?, </em>o<em>Cansado de no encontrar respuestas, decidí cambiar las preguntas,</em> o esta otra tan triste y realista como vitalista,&nbsp;&nbsp;<em>El mundo está oscuro, ilumina tu parte</em>.<br />Literatura, música y pintura unidas para recordar a la juventud los comportamientos de los personajes modélicos, para inculcar los valores, para enseñar conductas, para expresar sentimientos...</p>
<p style="color: #000033; font-size: medium; font-style: italic; font-weight: bold; font-family: 'Times New Roman',Times,serif;" class="Estilo24" align="left">(Juan Naveros Sánchez, profesor del I.E.S. Miguel de Cervantes).</p>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Actividades</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:75:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - El Niño de las Pinturas";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:24:"El Niño de las Pinturas";s:4:"link";s:58:"index.php?option=com_content&view=article&id=174&Itemid=78";}}s:6:"module";a:0:{}}
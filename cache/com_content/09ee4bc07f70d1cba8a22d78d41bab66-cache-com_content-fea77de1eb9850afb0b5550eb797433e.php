<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9811:"<div class="blog"><div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Área Artística</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 19 Septiembre 2014 15:21</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=184:area-artistica&amp;catid=70&amp;Itemid=570&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=e1626a2f60c294cdc500c742cd5c1ad56838d1fb" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2955
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><ol>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=76&amp;Itemid=98">Dpto. Dibujo.</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=73&amp;Itemid=96">Dpto. Educación Física.</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=162&amp;Itemid=96">Dpto. Música.</a></li>
</ol></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Dª Nuria Manrique Mora</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 27 Septiembre 2012 19:00</span> | <span class="art-postauthoricon">Escrito por Dª Nuria Manrique Morá</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=72:do-nuria-manrique-mora&amp;catid=70&amp;Itemid=143&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=1501c11602f735943bb76366fff81a13ac6ddff9" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 14570
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2><span style="text-decoration: underline;"><strong>Página personal de la profesora de Educación Física:</strong></span></h2>
<h2><span style="text-decoration: underline;"><strong>Dª Nuria Manrique Mora</strong></span></h2>
<p style="font-size: 11.818181991577148px; font-family: 'Times New Roman';"><span style="text-decoration: underline;"><strong style="color: #000000; font-size: 19.09090805053711px;">APUNTES DE EDUCACIÓN FÍSICA 2º ESO BILINGÜE.</strong></span></p>
<p style="font-size: 11.818181991577148px; font-family: 'Times New Roman';">Para poder ver los apuntes siguientes hay que entrar con clave y acceder desde el menú superior ALUMNADO &nbsp;&gt; 2º ESO BILINGÜE. o desde <a target="_blank" href="index.php?option=com_content&amp;view=article&amp;id=171&amp;Itemid=241">este enlace.</a></p>
<ul style="color: #151509; margin: 1em 0px 1em 2em; padding: 0px; font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 16.363636016845703px; list-style-type: none; text-align: left;">
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 18px; line-height: 1.25em; background-image: url(templates/otonio6/images/postbullets.png); overflow: auto; background-repeat: no-repeat no-repeat;">What equipment is necessary. Vocabulary</li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 18px; line-height: 1.25em; background-image: url(templates/otonio6/images/postbullets.png); overflow: auto; background-repeat: no-repeat no-repeat;">Warm-up</li>
<li style="margin: 0.2em 0px; padding: 0px 0px 0px 18px; line-height: 1.25em; background-image: url(templates/otonio6/images/postbullets.png); overflow: auto; background-repeat: no-repeat no-repeat;">Preventing injuries. (27/09/12)</li>
</ul>
<p><span style="text-decoration: underline;"><strong><br /></strong></span></p>
<br /></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Educación Fisica</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 27 Octubre 2015 18:01</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=73:educacion-fisica&amp;catid=70&amp;Itemid=96&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=f7c56cb23160c10206e44d1b4e2d48470e98a5d1" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 20012
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><span style="color: #414141;"> </span></p>
<h4 style="padding-left: 30px;"><strong><span style="color: #222222; font-family: arial, sans-serif; text-decoration: underline;">2º de Bachillerato: Optativa de EducaciÓn Física. CURSO 2015/16</span></strong></h4>
<ul>
<li><span style="color: #222222; font-family: arial, sans-serif; font-size: 12.8px; background-color: #ffffff;"><a href="archivos_ies/15_16/ef/Trabajo_doping.pdf" target="_blank">Actividad nº1 - Optativa Educación Física 2º de Bachillerato.</a> (07/10/15)</span></li>
<li><span style="color: #222222; font-family: arial, sans-serif; font-size: 12.8px; background-color: #ffffff;"><a href="archivos_ies/15_16/ef/DEPARTAMENTO_DE_DUCACION_FISICA_1.docx" target="_blank">Trabajo para 1º Bachillerato.</a> (15/10/15)</span></li>
<li><span style="color: #222222; font-family: arial, sans-serif; font-size: 12.8px; background-color: #ffffff;"><a href="archivos_ies/15_16/ef/DEPARTAMENTO_DE_DUCACION_FISICA_2.docx" target="_blank">Trabajo para 3º ESO.</a> (15/10/15)</span></li>
</ul>
<h4 style="padding-left: 30px;"> <span style="text-decoration: underline;"><strong>4º eso. educación física. curso 2015/16</strong></span></h4>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/15_16/ef/DEPARTAMENTO_DE_EDUCACION_FISICA.docx" target="_blank">Trabajo</a> (26/10/15)</span></li>
</ul>
<p> </p>
<hr />
<h4 style="padding-left: 30px;"><strong><span style="text-decoration: underline;">CUADERNILLOS DE PROGRAMACIONES DE E.F. CURSO 2015/16</span></strong></h4>
<ul>
<li><a href="archivos_ies/15_16/programaciones/programacion_ef_15_16.docx" target="_blank">PROGRAMACIÓN COMPLETA 2015/16</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_1ESO.doc" target="_blank">Cuadernillo programación 1º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_2ESO.doc" target="_blank">Cuadernillo programación 2º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_3ESO.doc" target="_blank">Cuadernillo programación 3º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_4ESO.doc" target="_blank">Cuadernillo programación 4º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_1_Bachillerato.doc" target="_blank">Cuadernillo programación 1º BACH.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_2_Bachillerato.doc" target="_blank">Cuadernillo programación 2º BACH.</a> (06/10/15)</li>
</ul>
<p> </p>
<div class="fechaModif"><hr />
<p><span style="text-decoration: underline;"><strong>Páginas personales del profesorado del Departamento:</strong></span></p>
</div>
<div class="fechaModif">
<ul>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=72:do-nuria-manrique-mora&amp;catid=70:educacion-fisica" target="_blank">Dª Nuria Manrique Mora.</a></li>
<li><a href="http://nuriaeducacionfisica.blogspot.com/" target="_blank">Blog de Nuria Manrique sobre Educación Física.</a><span style="color: #2a2a2a;"> </span></li>
</ul>
</div>
<div class="fechaModif"><hr /></div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
</div>";s:4:"head";a:10:{s:5:"title";s:62:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Componentes";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:101:"/index.php?option=com_content&amp;view=category&amp;id=70&amp;Itemid=143&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:102:"/index.php?option=com_content&amp;view=category&amp;id=70&amp;Itemid=143&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"Componentes";s:4:"link";s:58:"index.php?option=com_content&view=article&id=78&Itemid=143";}i:1;O:8:"stdClass":2:{s:4:"name";s:13:"Departamentos";s:4:"link";s:60:"index.php?option=com_content&view=category&id=195&Itemid=143";}i:2;O:8:"stdClass":2:{s:4:"name";s:18:"Educación Física";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7459:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Dpto. de Latín y Griego</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Departamento de  Latín y Griego.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Sábado, 28 Septiembre 2013 14:47</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=164:dep-latin-y-griego&amp;catid=102&amp;Itemid=111&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=6d4ba61ab9502ecf5c2adcb704638d1883988a00" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 4029
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #ffffff;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Historia de Grecia, 1º de Bachillerato (23/09/13)</span></h2>
<p> <a href="https://www.youtube.com/watch?v=cyvNgDMZEdw" target="_blank" style="color: #1155cc;">Ver vídeo</a></p>
<hr />
<p><span style="text-decoration: underline;"><strong><br />ENLACES MUY INTERESANTES:</strong> </span></p>
<ul>
<li><a href="archivos_ies/44Equilibrio.pps" target="_blank"><strong>Sobre la relajación.</strong></a></li>
<li><a href="archivos_ies/44Cementerio.docx" target="_blank"><strong>Etimología de la palabra "cementerio"</strong></a>.</li>
<li><strong style="color: #484137; text-align: justify; background-color: #ffffff;">20/02/13.  <a href="http://paleorama.wordpress.com/2013/02/19/entrevista-al-filologo-antoni-biosca-sobre-la-importancia-del-latin/" target="_blank">Entrevista al filólogo Antoni Biosca sobre la importancia del Latín.</a></strong></li>
<li>
<div style="color: #222222; font-family: arial, sans-serif; font-size: 13px; font-weight: normal; text-align: start;"><strong style="color: #484137; text-align: justify; background-color: #ffffff;"><span style="color: #222222; font-family: arial, sans-serif; font-size: 13px; font-weight: normal; text-align: start;"><strong style="font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13px; color: #484137; text-align: justify;">20/02/13.  </strong><a href="http://www.youtube.com/watch?v=Rc9RnwPXoJ0&amp;feature=youtu.be" target="_blank">Video elaborado por alumnos de un instituto de Lorca.</a></span></strong></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: 13px; font-weight: normal; text-align: start;"><a href="http://www.youtube.com/watch?v=Rc9RnwPXoJ0&amp;feature=youtu.be" target="_blank"><img src="https://www-gm-opensocial.googleusercontent.com/gadgets/proxy/refresh=3600&amp;container=gm&amp;gadget=http%3A%2F%2Fwww.gstatic.com%2Fig%2Fmodules%2Fgm%2Fyoutube%2Fcard-youtube.xml/http://i.ytimg.com/vi/Rc9RnwPXoJ0/default.jpg" border="0" style="color: #423c33; font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13px;" /></a></div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: 13px; font-weight: normal; text-align: start;"> </div>
<div style="color: #222222; font-family: arial, sans-serif; font-size: 13px; font-weight: normal; text-align: start;"> </div>
</li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">13/12/12.  </span></strong><strong><span style="color: #0066cc;"><a href="archivos_ies/TENSION_EN_LAS_AULAS.pdf">TENSION EN LAS AULAS.</a> Aportación del Departamento de Latín y Griego.</span></strong></li>
<li style="text-align: justify;"><strong><span style="color: #0066cc;">07/11/12. </span></strong><span style="color: #0066cc;"><strong><a href="archivos_ies/ENSENANZA_Y_CONOCIMIENTO.doc" target="_blank">ENSEÑANZA Y CONOCIMIENTO.</a> Aportación del Departamento de Latín y Griego.</strong></span></li>
</ul>
<ul>
<li>21/02/13.  <a href="archivos_ies/latin-griego/nueva_gramatica.pps" target="_blank">Nueva gramática</a> (PPS)</li>
</ul>
<p><strong>Escritos de protesta sobre el Anteproyecto de Ley Orgánica para la Mejora de la Calidad Educativa en relación con los Estudios Clásicos:</strong></p>
<ol>
<li><a href="archivos_ies/latin-griego/carta_Mariano_Rajoy.doc" target="_blank" style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none; color: #336600; font-size: 11px;">Carta al Presidente del Gobierno.</a></li>
<li><a href="archivos_ies/latin-griego/EscritoWertUCM.doc" target="_blank" style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none; color: #336600; font-size: 11px;">Carta al Ministro de Edcuación.</a></li>
</ol>
<p> </p>
<hr />
<ul>
<li><a href="archivos_ies/criterios_evaluacion/Resumen_progr_griego_padres_2012_13.doc" target="_blank" title="Criterios de evaluación de Griego." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none; color: #336600; font-size: 11px;">Criterios de evaluación de Griego 2012/13.</a> (16/10/12)</li>
<li><a href="archivos_ies/criterios_evaluacion/Resumen_progr_latin_padres_2012_13.doc" target="_blank" title="Criterios de evaluación de Latín." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none; color: #336600; font-size: 11px;">Criterios de evaluación de Latín 2012/13.</a> (16/10/12)</li>
<li><a href="archivos_ies/criterios_evaluacion/PROGRAMACION_DE_GRIEGO_2012-2013.pdf" target="_blank">Programación de Griego.</a> (26/10/12)</li>
<li><a href="archivos_ies/criterios_evaluacion/PROGRAMACION_DE_LATIN_2012-13.docx" target="_blank">Programación de Latín.</a> (09/11/12)</li>
<li></li>
<li><a href="archivos_ies/GR_Pendientes.pdf" target="_blank"><span style="color: #9b3f17;">Recuperación de pendientes de cursos anteriores de Griego.</span></a> 15/12/12. </li>
<li><a href="archivos_ies/LAT_Pendientes.pdf" target="_blank"><span style="color: #9b3f17;">Recuperación de pendientes de cursos anteriores de Latín.</span></a> 15/12/12.</li>
</ul>
<p> </p>
<hr /></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">DEPARTAMENTOS DE HUMANIDADES</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:75:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Dpto. de Latín y Griego";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:25:"Área Socio-Lingüística";s:4:"link";s:59:"index.php?option=com_content&view=article&id=186&Itemid=572";}i:2;O:8:"stdClass":2:{s:4:"name";s:24:"Dpto. de Latín y Griego";s:4:"link";s:59:"index.php?option=com_content&view=article&id=164&Itemid=111";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:83597:"<div class="blogpprincipal_"><div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">FP</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 20 Febrero 2013 06:22</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=199:fpe&amp;catid=242&amp;Itemid=603&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 3272
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p> </p>
<h1 style="background-color: #fbe9c5;"><span style="text-decoration: underline;">ciclo superior de:</span></h1>
<h1 style="background-color: #fbe9c5;"><span style="text-decoration: underline;"> animación socio-cultural</span>    </h1>
<div class="jsn-article-content" style="text-align: justify; background-color: #fbe9c5;">
<h2 style="padding-left: 30px;"><strong> </strong></h2>
<table style="width: 687px;" border="0">
<tbody>
<tr style="background-color: #4786be;">
<td style="text-align: center; width: 270px;"><span style="color: #ffffff;"><strong>Materias Obligatorias</strong></span></td>
<td style="text-align: center; width: 50px;"><span style="color: #ffffff;"><strong>Horas totales</strong></span></td>
<td style="text-align: center; width: 99px;"><span style="color: #ffffff;"><strong>Horas semanales</strong></span></td>
</tr>
<tr>
<td style="width: 270px;">Animación de ocio y tiempo libre</td>
<td style="text-align: center; width: 50px;">160</td>
<td style="width: 99px; text-align: center;"> 5</td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">Desarrollo comunitario</td>
<td style="text-align: center; width: 50px;">160</td>
<td style="width: 99px; text-align: center;"> 5</td>
</tr>
<tr>
<td style="width: 270px;">Animación cultural</td>
<td style="text-align: center; width: 50px;">160</td>
<td style="width: 99px; text-align: center;"> 5</td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">Organización y gestión de una pequeña empresa de actividades de tiempo libre y socioeducativas</td>
<td style="text-align: center; width: 50px;">96</td>
<td style="width: 99px; text-align: center;"> 3</td>
</tr>
<tr>
<td style="width: 270px;">Animación y dinámica de grupos</td>
<td style="text-align: center; width: 50px;">128</td>
<td style="width: 99px; text-align: center;"> 4</td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">Metodología de la intervención social</td>
<td style="text-align: center; width: 50px;">160</td>
<td style="width: 99px; text-align: center;"> 5</td>
</tr>
<tr>
<td style="width: 270px;">Los servicios sociocomunitarios en Andalucía</td>
<td style="text-align: center; width: 50px;">32</td>
<td style="width: 99px; text-align: center;"> 1</td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">Formación y orientación laboral</td>
<td style="text-align: center; width: 50px;">64</td>
<td style="width: 99px; text-align: center;"> 2</td>
</tr>
<tr>
<td style="width: 270px;">
<p>Proyecto integrado <span style="color: inherit; font-family: inherit; font-size: inherit;">(*)</span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;">Formación en centros de trabajo (*)</span></p>
</td>
<td style="text-align: center; width: 50px;">
<p style="text-align: center;"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">740 </span></p>
</td>
<td style="width: 99px;"> </td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">
<p> <span style="color: #000000; font-size: medium; text-align: justify;">(*) Las 740 horas se reparten entre el Proyecto Integrado y la Formación en Centros de Trabajo.</span></p>
<p> </p>
</td>
<td style="text-align: center; width: 50px;"> </td>
<td style="width: 99px;"> </td>
</tr>
</tbody>
</table>
</div>
<div class="jsn-article-content" style="text-align: justify; background-color: #fbe9c5;"> <span style="color: #000000; font-size: medium;">Una vez cursado el Ciclo Superior de Grado Superior que consta de 17OO horas de las cuales 860 son de carácter lectivo y 740 de formación práctica en Centros de Trabajo, se puede acceder a estudios universitarios relacionados con la temática del Ciclo Superior de Animación Sociocultural como son los siguientes:</span></div>
<ul style="color: #000000; font-family: 'Times New Roman'; font-size: medium;">
<li>Pedagogía.</li>
<li>Magisterio</li>
<li>Logopedia</li>
<li>Trabajo Social.</li>
</ul>
<div class="jsn-article-content" style="text-align: justify; background-color: #fbe9c5; padding-left: 30px;"><strong style="text-transform: uppercase; color: #4786be; font-size: 10pt;"> </strong></div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Horario de Secretaría</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:08</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=204:horario&amp;catid=242&amp;Itemid=610&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 2943
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading"><span style="text-decoration: underline;"><strong>SECRETARÍA: Horario de atención al público:</strong></span></h2>
<div class="plg_fa_karmany"> 
<h2 class="text-info">De Lunes a Viernes: de 9,00 a 12,00 horas</h2>
<span style="color: #366999;"><span style="color: #366999;"><br /></span></span></div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Reserva de plaza</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 06 Marzo 2014 18:57</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=205:reserva-de-plaza&amp;catid=242&amp;Itemid=612&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 28998
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading" style="text-align: center;"><span style="text-decoration: underline;"><strong>Reserva de plaza PARA EL CURSO 2014/15</strong></span></h2>
<div class="plg_fa_karmany"> </div>
<div id="articlepxfontsize1">
<div style="text-align: justify;">El plazo de preinscripción para la matrícula en Educación Secundaria Obligatoria y Bachillerato, en todas sus modalidades, será durante todo el mes de MARZO. <br /><br /></div>
La documentación requerida es:
<ul class="list-arrow arrow-red">
<li>Solicitud de escolarización, que se puede recoger en Secretaría, o descargarla desde aquí para <a href="archivos_ies/solicitud_admision.pdf" target="_blank">ESO o Bahillerato</a> y para <a href="archivos_ies/solicitud_plaza_grado_superior.pdf" target="_blank">FP Grado _Superior</a> - <a href="archivos_ies/solicitud_reserva_plaza.pdf" target="_blank">Reserva de puesto escolar</a></li>
</ul>
<hr />
<p> </p>
<h1 class="portlet-title"><span class="portlet-title-text">Información General</span></h1>
<menu id="portlet-topper-toolbar_ABACO_WEB_WAR_abacoportlet_INSTANCE_J8tH" class="portlet-topper-toolbar"></menu>
<div class="detalle_html">
<p style="text-align: center;"><img src="archivos_ies/13_14/bannerinfantil0-3anos.jpg" border="0" alt="Escolarización 2013-2014 (EDUC2013_728X90.jpg)" title="Escolarización 2013-2014 (EDUC2013_728X90.jpg)" style="width: 580px; height: 72px; border: 0;" /></p>
<p><strong>La Consejería de Educación, Cultura y Deporte convoca el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato para el curso escolar 2014/15.</strong></p>
<p>Durante todo el mes de marzo permanecerá abierto en Andalucía el plazo para la presentación de solicitudes de admisión correspondientes al curso escolar 2014/15 en la red de centros sostenidos con fondos públicos. Se inicia de este modo el proceso de escolarización en el que participan todos los niños y niñas que se incorporan por primera vez al sistema educativo, tanto en centros públicos como privados concertados, así como el alumnado que cambie de centro escolar.</p>
<p>Entre los días 1 y 31 de marzo, ambos inclusive, deberán presentar su solicitud los escolares de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.</p>
<p>Continúa publicada, con nuevas mejoras, la aplicación para smartphones que permite realizar consultas de centros así como de los puntos del baremo por domicilio.</p>
<p>Por su parte, los centros educativos publican la relación de puestos escolares vacantes así como la información sobre las direcciones catastrales comprendidas en sus zonas de influencia y zonas limítrofes. Cuando la oferta de plazas coincida o sea superior a la demanda, los solicitantes serán admitidos y solo en aquellos casos donde no se pueda atender toda la demanda, se procederá a la baremación de las solicitudes. </p>
<p>Los centros harán pública en su tablón de anuncios, antes del 22 de abril, la relación de solicitantes con la puntuación asignada y, a partir de esa fecha, se abrirá un plazo de 10 días lectivos para la presentación de alegaciones. Si tras la aplicación de los criterios de baremación (hermanos en el centro, domicilio familiar o laboral, por trabajar la madre o el padre en el centro, renta anual, discapacidad, familia numerosa o monoparental) se producen empates, se aplicará el resultado del sorteo público que se celebrará el día 14 de mayo de 2014. La relación definitiva de admitidos y no admitidos saldrá el 20 de mayo.</p>
<p>Para atender consultas sobre todo el proceso de escolarización, la Consejería ha habilitado un teléfono gratuito de información (900 848 000), en horario ininterrumpido de 8.00 a 19.00 horas.</p>
</div>
<h3 class="tituloCaja">Tríptico informativo curso escolar 2014 / 15</h3>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/Triptico_informativo.pdf" target="_blank" title="Visualizar el documento Se abre en una nueva ventana"> <strong>Tríptico informativo (PDF / 221,48Kb)</strong> </a></li>
<li class="icopdf"><a href="archivos_ies/13_14/Cartel_informativo.pdf" target="_blank" title="Visualizar el documento Se abre en una nueva ventana"> <strong>Cartel informativo (PDF / 106,04Kb)</strong></a><a href="http://portal.ced.junta-andalucia.es/educacion/webportal/abaco-portlet/content/2edb31e2-36d8-4023-a893-4c7a4513cda7" target="_blank" title="Visualizar el documento Se abre en una nueva ventana"> </a></li>
</ul>
<hr />
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/solicitud_admision.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO III (PDF / 285,66Kb)</strong> <br /> Solicitud de admisión en segundo ciclo de Educación Infantil, Educación Primaria, Educación Especial, E.S.O. y Bachillerato </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20IV.%20Matrícula%20en%20segundo%20ciclo%20de%20Educación%20Infantil%20y%20Educación%20Primaria.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO IV (PDF / 225,72Kb)</strong> <br /> Matrícula en segundo ciclo de Educación Infantil y Educación Primaria </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20V.%20Matrícula%20en%20Educación%20Especial.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO V (PDF / 80,03Kb)</strong> <br /> Matrícula en Educación Especial </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20VI.%20Matrícula%20en%20Educación%20Secundaria%20Obligatoria.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO VI (PDF / 102,24Kb)</strong> <br /> Matrícula en Educación Secundaria Obligatoria </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20VII.%20Matrícula%20en%20Programas%20de%20Cualificación%20Profesional%20Inicial.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO VII (PDF / 56,89Kb)</strong> <br /> Matrícula en Programas de Cualificación Profesional Inicial </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20VIII.%20Matrícula%20en%20Bachillerato.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO VIII (PDF / 95,32Kb)</strong> <br /> Matrícula en Bachillerato </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20IX.%20Solicitud%20Procedimiento%20Extraordinario%20de%20Admisión.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO IX (PDF / 306,73Kb)</strong> <br /> Solicitud Procedimiento Extraordinario de Admisión </a></li>
</ul>
<div class="paginacion"> </div>
<h1 class="portlet-title"><span class="portlet-title-text">Presentación telemática de la solicitud</span></h1>
<menu id="portlet-topper-toolbar_ABACO_WEB_WAR_abacoportlet_INSTANCE_sHc8" class="portlet-topper-toolbar"></menu>
<div class="detalle_html">
<div class="tituloCajaColor">SECRETARÍA VIRTUAL</div>
<p>La solicitud de admisión podrá realizarse a través de la <strong>Secretaría Virtual</strong> de conformidad con lo establecido en el <strong>artículo 49 del Decreto 40/2011, de 22 de febrero.</strong></p>
<p>Para realizar la presentación telemática se requiere un certificado digital reconocido para cada uno de los miembros de la unidad familiar que deban firmar la solicitud.</p>
<p>La persona que tramita la solicitud no podrá realizar la presentación telemática de la misma y obtener el correspondiente recibo electrónico, hasta que todos los miembros de la unidad familiar que deban firmar alguna autorización, la hayan firmado electrónicamente.</p>
</div>
<h3 class="tituloCaja">Tramitación telemática</h3>
<ul id="aui_3_2_0_1169" class="listContRelacionados">
<li id="aui_3_2_0_1167">
<div class="divHiddenLeft"><a href="https://www.juntadeandalucia.es/educacion/secretariavirtual/" target="_blank" title="Se abre en una nueva ventana"> <strong class="enlaceExterior">Acceso a la Secretaría Virtual</strong></a><a href="https://www.juntadeandalucia.es/educacion/secretariavirtual/" target="_blank" title="Se abre en una nueva ventana"> </a></div>
</li>
</ul>
</div>
<hr />
<p> </p>
<h1 class="portlet-title"><span class="portlet-title-text">Normativa</span></h1>
<menu id="portlet-topper-toolbar_ABACO_NORMATIVAS_WAR_abacoportlet" class="portlet-topper-toolbar"></menu>
<ul class="listado">
<li><strong><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/infantil-primaria-eso-bachillerato/normativa/-/normativas/detalle/decreto-40-2011-de-22-de-febrero-por-el-que-se-regulan-los-criterios-y-el-procedimiento-de-admision" target="_blank" title="DECRETO 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.">DECRETO 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato. </a></strong></li>
</ul>
<ul class="listado">
<li><strong><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/infantil-primaria-eso-bachillerato/normativa/-/normativas/detalle/correccion-de-errores-al-decreto-40-2011" target="_blank" title="CORRECCIÓN de errores al Decreto 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato">CORRECCIÓN de errores al Decreto 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato </a></strong></li>
</ul>
<ul class="listado">
<li>24 de febrero de 2011<br /> <strong><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/infantil-primaria-eso-bachillerato/normativa/-/normativas/detalle/orden-de-24-de-febrero-de-2011-por-la-que-se-desarrolla-el-procedimiento-de-admision-del-alumnado" target="_blank" title="ORDEN de 24 de febrero de 2011, por la que se desarrolla el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.">ORDEN de 24 de febrero de 2011, por la que se desarrolla el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato. </a></strong></li>
</ul>
<p> </p>
<hr />
<p> </p>
<h1 id="aui_3_2_0_1198" class="portlet-title"><span id="aui_3_2_0_1197" class="portlet-title-text">Preguntas frecuentes</span></h1>
<menu id="portlet-topper-toolbar_faqPortlet_WAR_porletEducacion_INSTANCE_6uYZ" class="portlet-topper-toolbar"></menu>
<div class="entry"><span style="display: block; text-align: right; padding: 0pt 20px; margin-bottom: -10px; font-size: small;"> </span></div>
<div id="aui_3_2_0_1190" class="entry"><span id="aui_3_2_0_1189" style="display: block; text-align: right; padding: 0pt 20px; margin-bottom: -10px; font-size: small;"><a rel="jfaq_collapse"></a></span>
<p id="aui_3_2_0_1202">¿Tiene que presentar solicitud de admisión para acceder al segundo ciclo de educación infantil el alumnado matriculado en el primer ciclo de educación infantil?.</p>
<dl class="simple_jfaq"><dt id="aui_3_2_0_1203" style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>SÍ. </strong>Según lo establecido en el artículo 34.3 del Decreto 149/2009, de 12 de mayo, por el que se regulan los centros que imparten el primer ciclo de educación infantil, a los efectos de admisión del alumnado no podrán establecerse adscripciones del primer al segundo ciclo de educación infantil. En cualquier caso, para acceder al segundo ciclo de educación infantil en centros sostenidos con fondos públicos se deberá participar en el procedimiento de admisión establecido en el Decreto 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Quiénes deben solicitar una plaza escolar?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<ul>
<li style="text-align: justify;">Quien acceda por primera vez al segundo ciclo de Educación Infantil (3, 4 ó 5 años) para cursar este ciclo en un centro público o privado concertado.</li>
<li style="text-align: justify;">Quien acceda por primera vez a la Educación Primaria sin haber cursado antes el segundo ciclo de infantil.</li>
<li style="text-align: justify;">Quien acceda por primera vez a la Educación Primaria habiendo cursado el segundo ciclo de Educación Infantil en un centro privado o esté matriculado, en el curso 2013/14, en el tercer curso del segundo ciclo de Educación Infantil (5 años) en un centro privado concertado cuando el curso en el que está matriculado no está concertado.</li>
<li style="text-align: justify;">Quien opte por otro centro docente público o privado concertado distinto del que le corresponde por adscripción.</li>
<li style="text-align: justify;">Quien haya sido informado por la dirección del centro público o la titularidad del centro privado concertado en el que finaliza estudios que está adscrito a varios centros (adscripción múltiple) mediante el Anexo II de la Orden vigente que regula la admisión.</li>
<li style="text-align: justify;">Quien estando matriculado en el curso 2013/14 en un centro público o privado concertado desee cambiar a otro centro a un curso sostenido con fondos públicos.</li>
</ul>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Dónde puedo encontrar la normativa que rige el proceso de admisión del mes de marzo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">En la web de la Consejería de Educación, Cultura y Deporte <span>(<a href="http://www.juntadeandalucia.es/educacion">www.juntadeandalucia.es/educacion/</a>) </span>y en los tablones de anuncios de cada centro docente tanto público como privado concertado durante todo el proceso.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuántas solicitudes se pueden presentar en el proceso de admisión?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">En el proceso de admisión solo se podrá presentar <strong><span>una solicitud por alumno o alumna,</span></strong> para la enseñanza y curso en el que se solicita la admisión del alumno o alumna. Dicha solicitud se presentará preferentemente en el centro docente donde se solicita la admisión prioritariamente.</p>
<p style="text-align: justify;"><strong><span style="text-decoration: underline;">El solicitante debe quedarse con una copia del impreso de solicitud registrado por el centro</span></strong></p>
<p style="text-align: justify;"> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué ocurriría si se presenta la solicitud de admisión fuera de plazo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">Que perdería la prioridad que en su caso le hubiese correspondido, en relación con las presentadas en plazo.</p>
<p style="text-align: justify;">Una vez concluida la adjudicación de plazas escolares, las comisiones territoriales de garantías de admisión adjudicarán una plaza escolar a los alumnos o alumnas cuya solicitud de admisión fue presentada fuera del plazo establecido.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué ocurrirá si se presenta más de una solicitud para centros diferentes?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">Que el alumno o alumna perderá todos los derechos de prioridad que puedan corresponderle.</p>
<p style="text-align: justify;">Una vez concluida la adjudicación de plazas escolares, las comisiones territoriales de garantías de admisión adjudicarán una plaza escolar a los alumnos o alumnas que presentaron más de una solicitud.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué alumnado<strong> <span style="text-decoration: underline;">no tiene</span></strong> que presentar solicitud de admisión durante el mes de marzo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>1.- El alumnado que en el curso escolar 2013/14 esté matriculado en un centro público o privado concertado en un curso sostenido con fondos públicos y desee continuar en dicho centro en el curso escolar 2014/15 en un curso sostenido con fondos públicos.</p>
<div> 2.- Aquel alumnado que en el curso 2013/14 esté matriculado, en el último curso de alguna de las etapas educativas de un centro público o privado concertado y vaya a cursar el primer curso de la etapa siguiente en otro centro al que esté adscrito, salvo que concurran las circunstancias previstas en la adscripción múltiple. De la adscripción autorizada será informado por la dirección del centro público o la titularidad del centro privado concertado en el que se encuentra matriculado en el curso 2013/14. </div>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Dónde debe presentarse la solicitud de admisión?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Preferentemente en el centro docente en el que se solicita la admisión como prioritario, sin perjuicio de lo establecido en el artículo 38.4 de la Ley 30/1992, de 26 de noviembre, y en los artículos 82 y 83 de la Ley 9/2007, de 22 de octubre, en cuyo caso, para agilizar el procedimiento, podrá remitirse copia autenticada al centro al que se dirige la solicitud. </p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuál es el plazo de presentación de solicitudes de admisión?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><span style="text-decoration: underline;">Durante todo el mes de marzo</span> está abierto el plazo para presentar solicitudes de admisión para un puesto escolar en centros públicos y privados concertados para las enseñanzas correspondientes al segundo ciclo de la educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.</p>
<div><span style="text-decoration: underline;">Antes del plazo establecido los centros no podrán recepcionar solicitudes de admisión</span>.</div>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Puede la persona solicitante indicar más de una enseñanza en su solicitud?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>NO</strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Puede la persona solicitante de plaza escolar modificar algún dato de la solicitud de admisión después de haberla presentado?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Una vez entregado el impreso de solicitud, los datos contenidos en el mismo sólo se podrán alterar mediante comunicación por escrito de la persona solicitante dirigida a la persona que ejerce la dirección del centro público o la persona física o jurídica titular del centro privado concertado, y durante el plazo establecido para la presentación de solicitudes.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Están obligados los centros a recepcionar las solicitudes aunque no hayan publicado vacantes para el curso solicitado?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>Sí. </strong>Los centros deben recepcionar todas las solicitudes que se presenten en el mismo (aunque el centro no tenga plazas escolares vacantes para la enseñanza y curso que se solicita). </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Dónde puedo consultar las áreas de influencia de un centro?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">Las áreas de influencia de cada centro se publicarán, antes de comenzar el mes de marzo, en el tablón de anuncios de la Delegación Territorial de la Consejería de Educación, Cultura y Deporte.<br />Además, cada centro debe,  tal como le hayan sido remitidas por la Delegación Territorial, publicarlas en el tablón de anuncios con anterioridad al inicio del período establecido para la presentación de solicitudes. si bien debe advertirse que la información mostrada a partir del Sistema de Información Geográfico Educasig es meramente informativa y no genera expectativas ni derechos al usuario. </p>
<p style="text-align: justify;">Además se podrán consultar, a  título informativo, en el Visor de Escolarización de de la Consejería de Educación, Cultura y Deporte <span style="text-align: left; text-transform: none; background-color: #ffffff; text-indent: 0px; display: inline ! important; font: small/15px arial,sans-serif; white-space: normal; float: none; letter-spacing: normal; color: #009933; word-spacing: 0px;"><a name="Portal_Educasig" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/principal.html" target="_blank">www.juntadeandalucia.es/educacion/educasig</a>  <span style="color: #000000; font-size: small;"> si bien debe advertirse que la información mostrada a partir del Sistema de Información Geográfico Educasig es <span style="text-decoration: underline;">meramente informativa </span>y no genera expectativas ni derechos al usuario. </span></span></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Puedo presentar la solicitud de admisión en un centro distinto de aquél en el que deseo que el alumno o alumna sea admitido/a como prioritario?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">Sí, aunque preferiblemente debe entregarla en el centro en el que desea que el alumno/a sea admitido/a como prioritario.</p>
<p style="text-align: justify;">Igualmente, conforme a lo que establece el artículo 38.4 de la Ley 30/92 de Régimen Jurídico de las Administraciones Públicas y del Procedimiento Administrativo Común, la solicitud y la documentación se podrán presentar en los registros de cualquier órgano administrativo, que pertenezca a la Administración General del Estado, a la de cualquier Administración de las Comunidades Autónomas y en las oficinas de Correos en la forma reglamentariamente establecida.<br />En caso de que se usara esta posibilidad para agilizar el procedimiento se remitirá copia autenticada de la documentación al centro docente en el que se solicita el puesto escolar.</p>
</dd><dt style="cursor: pointer;">Por qué criterios se regirá, en su caso, la admisión del alumnado?</dt><dd style="display: block;">
<div>De conformidad con la normativa vigente, el orden de admisión se regirá por los siguientes criterios:</div>
<div> </div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Existencia de hermanos o hermanas matriculados/as en el centro docente o de padres, madres o tutores o guardadores legales que trabajen en el mismo.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Proximidad del domicilio familiar o del lugar de trabajo del padre, de la madre o de la persona tutora o guardadora legal.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Renta anual de la unidad familiar.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Concurrencia de discapacidad en el alumno o alumna o en sus padres, madres o tutores o guardadores legales, o en alguno de sus hermanos o hermanas o menores en acogimiento en la misma unidad familiar. En el segundo ciclo de la educación infantil se considerará también la presencia en el alumnado de trastornos del desarrollo.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- <span>Que el alumno o alumna pertenezca a una familia con la condición de numerosa.</span></div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Que el alumno o alumna pertenezca a una familia con la condición de monoparental y sea menor de edad o mayor de edad sujeto a patria potestad prorrogada o tutela.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Para las enseñanzas de Bachillerato, además de los criterios establecidos anteriormente, se considerará el expediente académico de los alumnos y alumnas.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;"> </div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;"> </div>
<p> </p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué ocurre cuando varios hermanos o hermanas solicitan plaza escolar en un mismo centro docente para distintos cursos?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<div style="text-align: justify;">En el caso de que existan dos o más solicitudes de admisión de hermanos o hermanas en un mismo centro docente para distintos cursos, cuando uno de ellos resulte admitido, se concederá a los demás 16 puntos.</div>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Deben relacionarse en la solicitud centros subsidiarios?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>Sí</strong>. Se pueden indicar hasta cuatro centros subsidiarios que serán considerados si el alumno o alumna no resulta admitido en el centro solicitado como prioritario. Para cada uno de los centros solicitados el Consejo Escolar del centro público o la persona física o jurídica titular del centro privado concertado establecerá la puntuación que corresponda en función de los distintos apartados del baremo. </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuándo se entiende que el alumno o alumna pertenece a una familia con la condición de monoparental?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">De conformidad con la normativa de admisión "se entenderá que el alumno o alumna menor de edad o mayor de edad sujeto a patria potestad prorrogada o tutela pertenece a una familia con la condición de monoparental cuando su patria potestad esté ejercida por una sola persona o cuando siendo ejercida por dos personas, exista orden de alejamiento de una de ellas con respecto a la otra con la que convive el alumno o alumna".</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué ocurre cuando varios hermanos o hermanas solicitan plaza escolar en el mismo centro docente y para el mismo curso?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">De acuerdo con la normativa de aplicación en el caso de que varios hermanos o hermanas soliciten una plaza escolar en el mismo centro docente y para el mismo curso de una de las etapas educativas, la admisión de uno de ellos supondrá la admisión de los demás. <br /> </p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué requisitos son necesarios para acceder a las distintas etapas educativas?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">1. Primer curso del segundo ciclo de la <strong>educación infantil</strong>:</p>
<div style="text-align: justify; text-indent: 35.25pt;">- 3 años cumplidos antes del 31 de diciembre de 2014.</div>
<div style="text-align: justify; margin-left: 35.25pt;"> </div>
<div style="text-align: justify;">2. Primer curso de <strong>educación primaria</strong>:</div>
<div style="text-align: justify;"> </div>
<div style="text-align: justify; margin-left: 35.4pt;">- 6 años cumplidos antes del 31 de diciembre de 2014, salvo autorización específica.</div>
<div style="text-align: justify; margin-left: 35.4pt;"> </div>
<div style="text-align: justify;">3. Primer curso de <strong>educación secundaria obligatoria:</strong></div>
<div style="text-align: justify; margin-left: 35.25pt;"> </div>
<div style="text-align: justify; margin-left: 35.25pt;">- Haber finalizado la educación primaria.</div>
<div style="text-align: justify;"> </div>
<div style="text-align: justify;">4. Primer curso de <strong>Bachillerato:</strong></div>
<div style="text-align: justify; margin-left: 35.4pt;"> </div>
<div style="text-align: justify; margin-left: 35.4pt;"><strong>- </strong>Poseer el título de Graduado en Educación Secundaria Obligatoria o el de Técnico de Formación Profesional.</div>
<div style="text-align: justify; margin-left: 35.4pt;"> </div>
<div style="text-align: justify; margin-left: 35.4pt;">- Para el primer curso de Bachillerato en la modalidad de artes también se podrá acceder con el título de Técnico de Artes Plásticas y Diseño. </div>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p style="text-align: justify;">¿Cuándo se considera que la persona que ostenta la representación o la guarda legal del alumno o alumna tiene su puesto de trabajo habitual en el centro docente?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">De conformidad con lo establecido en el artículo 9 de la Orden de 24 de febrero de 2011, a los efectos de lo establecido en el artículo 12 del Decreto 40/2010<span>, se considera que la persona que ostenta la representación o la guarda legal del alumno o alumna tiene su puesto de trabajo habitual en el centro docente si ejerce en el mismo una actividad laboral continuada con una jornada mínima de diez horas semanales y forma parte del siguiente personal:</span></p>
<div style="margin-top: 0cm; margin-right: -7.1pt; margin-bottom: 0.0001pt; text-align: justify; text-indent: 36pt;"> a) Personal al servicio de la Administración de la Junta de Andalucía o de los Ayuntamientos correspondientes que presta servicio en el centro docente público.</div>
<div style="margin-top: 0cm; margin-right: -7.1pt; margin-bottom: 0.0001pt; text-align: justify; text-indent: 36pt;">b) Personal que tenga una relación contractual con el centro docente.</div>
<div style="margin-top: 0cm; margin-right: -7.1pt; margin-bottom: 0.0001pt; text-align: justify; text-indent: 36pt;">c) Personal empleado de las empresas que presten servicios en el centro docente.</div>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita el lugar de trabajo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<ul>
<li style="text-align: justify;">En el caso de que la actividad laboral se realice por cuenta ajena, será necesario presentar la vida laboral de la persona cuyo lugar de trabajo va a ser tenido en consideración y una certificación expedida al efecto por la persona titular de la empresa o por la persona responsable de personal de la misma que deberá contener el domicilio del lugar del trabajo.</li>
<li style="text-align: justify;">En el caso de que se desarrolle la actividad laboral por cuenta propia, se deberá presentar una certificación acreditativa del alta en el Impuesto de Actividades Económicas y una declaración responsable de la persona cuyo lugar de trabajo va a ser tenido en consideración sobre la vigencia de la misma.</li>
<li style="text-align: justify;">En el supuesto de que no exista obligación legal de estar dado de alta en el citado impuesto, se acreditará mediante la presentación de alguno de los siguientes documentos:</li>
</ul>
<p style="text-align: justify;">             a) Copia autenticada de la correspondiente licencia de apertura expedida por el Ayuntamiento respectivo.</p>
<p style="text-align: justify;">             b) Copia sellada por el Ayuntamiento de la declaración responsable o comunicación previa presentada ante el mismo.</p>
<p style="text-align: justify;">             c) Alta en la Seguridad Social y una declaración responsable de la persona interesada sobre la vigencia de la misma.</p>
<p style="text-align: justify;"> </p>
<p style="text-align: justify;"> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Puede acreditarse el domicilio habitual de la persona solicitante si ésta es mayor de edad con certificación expedida por el ayuntamiento?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>SÍ.</strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita el domicilio familiar?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p> <span>Si se solicita plaza para un alumno o alumna menor de edad o mayor de edad sujeto a patria potestad prorrogada,<strong> la persona que firma la solicitud declara que convive con el alumno o alumna y ejerce su guarda y custodia</strong> (REPRESENTANTE O GUARDADOR 1 en la solicitud). <strong>Si opta por el domicilio familiar podrá autorizar en la solicitud a la Consejería para recabar el domicilio familiar por medios informáticos o telemáticos.</strong> Cuando la información obtenida no coincida con el domicilio que consta en la solicitud, la persona solicitante deberá aportar previo requerimiento de la dirección del centro público o la persona física o jurídica del centro privado concertado, el CERTIFICADO DE EMPADRONAMIENTO expedido por el Ayuntamiento.<br /></span></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué domicilio del alumno o alumna se considera en el procedimiento de admisión?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Aquel por el que haya optado en la solicitud de plaza escolar.</p>
<p><strong>En el Anexo III, de solicitud de plaza escolar, la persona que firma la solicitud - que convive con el alumno o alumna menor de edad y ejerce su guarda y custodia -, indica si opta POR EL DOMICILIO FAMILIAR del alumno o alumna o POR EL LUGAR DE TRABAJO del represente o guardador legal.</strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué domicilio se considera como domicilio familiar en el caso de alumnos o alumnas menores en acogimiento residencial?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>De conformidad con lo establecido en la normativa, en los casos de menores cuya medida de protección a la infancia sea el acogimiento residencial, el domicilio del centro de protección en el que resida tendrá la consideración de domicilio familiar.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita la condición de familia numerosa?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Con la copia autenticada del título oficial de familia numerosa que esté en vigor cuando no se haya autorizado a la Consejería para recabar la información de la Consejería competente en la materia o cuando habiéndose indicado en la solicitud dicha autorización la información no se haya obtenido. En este último supuesto, la dirección del centro público o la persona física o jurídica del centro privado concertado le habrá requerido fehacientemente dicha acreditación.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita la condición de familia monoparental?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Mediante copia autenticada del Libro de Familia, dicha copia deberá incluir las páginas escritas pudiendo sustituirse las páginas no escritas por una diligencia en la última página escrita en la que el funcionario que la autentique deje constancia de qué páginas están en blanco.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita la discapacidad?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">De conformidad con la normativa de admisión  las personas mayores de edad de la unidad familiar del alumno o alumna que se encuentren en esta situación deberán autorizar a la Consejería competente en materia de Educación en el modelo de solicitud de admisión, para recabar la información necesaria a la Consejería competente en la materia. En el caso de alumnos o alumnas menores de edad o mayores de edad sujetos a patria potestad prorrogada o tutela, serán sus padres, madres o tutores o guardadores legales los que realicen dicha autorización.</p>
<p style="text-align: justify;">Si no se pudiera obtener la información referida, la persona solicitante a requerimiento de la dirección del centro público o la persona física o jurídica titular del centro concertado, deberá presentar los correspondientes certificados de los dictámenes de discapacidad emitidos por el órgano competente de la Administración de la Junta de Andalucía o, en su caso, de otras Administraciones públicas.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>Si no se ha autorizado en la solicitud a la Consejería para recabar de la Consejería competente la pertenencia del alumno o alumna a familia con la condición de numerosa y  se tiene el titulo de familia numerosa caducado, ¿qué se debe hacer?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Presentar la solicitud de reconocimiento o renovación del mismo, y aportar  éste o su renovación a la mayor brevedad, y siempre antes de la resolución del procedimiento de admisión.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuál es el máximo permitido que pueden cobrar los centros docentes concertados?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>En ningún caso podrán los centros docentes públicos o privados concertados percibir cantidades de las familias por recibir las enseñanzas de carácter gratuito, imponer a las familias la obligación de hacer aportaciones a fundaciones o asociaciones ni establecer servicios obligatorios, asociados a las enseñanzas, que requieran aportación económica, por parte de las familias del alumnado. (Artículo 88.1 de la Ley Orgánica 2/2006, de 3 de mayo, de educación).</p>
<p>En el marco de lo dispuesto en el artículo 51 de la Ley Orgánica 8/1985, de 3 de julio, reguladora del Derecho a la Educación, quedan excluidos de esta categoría las actividades extraescolares, las complementarias y los servicios escolares que, en todo caso, tendrán carácter voluntario.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Las personas solicitantes de plaza escolar están obligadas a presentar, con carácter previo, documentación sobre la renta anual de la unidad familiar cuando pretendan la valoración de este criterio de admisión?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>No</strong>. Sólo en el caso de que la Administración Tributaria no disponga de la información de carácter tributario que se precisa para la acreditación de la renta anual, previo requerimiento de la dirección del centro público o de la persona física o jurídica titular del centro privado concertado, deberá aportar una certificación de haberes, declaración jurada o cualquier otro documento de cada una de las personas mayores de 16 años de la unidad familiar.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuál es plazo para solicitar la bonificación de los servicios complementarios (comedor, aula matinal y actividades extraescolares)?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>El plazo para solicitar la bonificación de los servicios complementarios es del 1 al 7 de septiembre de 2014.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Constituye un criterio para la admisión del alumnado la presencia en el mismo de trastornos del desarollo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Solamente en el segundo ciclo de Educación Infantil.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se valora la discapacidad o trastorno del desarrollo en el alumno o alumna?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>La valoración de que el alumno o alumna presenta una discapacidad o un trastorno del desarrollo, será de dos puntos. <strong>En el caso de que en el alumno o alumna confluyan las dos circunstancias, la valoración será, asimismo, de dos puntos.</strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuándo se tendrá en cuenta el criterio de presentar trastornos del desarrollo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>El criterio de presentar trastorno del desarrollo sólo se tendrá en cuenta en el segundo ciclo de Educación Infantil cuando el alumno o alumna disponga del informe correspondiente, emitido por el Centro de Atención Infantil Temprana en el que esté recibiendo tratamiento.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Las personas solicitantes de puesto escolar están obligadas a firmar la autorización para que la Agencia Estatal de Administración Tributaria suministre la información sobre la renta anual de la unidad familiar?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Sólo en el caso de que deseen que el criterio de renta anual de la unidad familiar sea valorado.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuál es el<strong>  IPREM </strong>(Indicador Público de Renta de Efectos Múltiples) de aplicación en el procedimiento de admisión que se inicia el 1 de marzo y su cuantía?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>1.- El IPREM a aplicar en el procedimiento de admisión en curso, regulado por el Decreto 40/2011, de 22 febrero, y Orden de 24 de febrero de 2011 que lo desarrolla será el del ejercicio 2012.</p>
<p>2.- En cuanto a su cuantía, debemos acudir a la disposición adicional decimocuarta de la Ley 2/2012, de 29 de junio, de Presupuestos Generales del Estado para el año 2012 (hasta su entrada en vigor, se mantuvieron prorrogados los Presupuestos del 2011). En virtud de la misma, y de conformidad con lo establecido por el artículo 2.2 del Real Decreto-ley 3/2004, de 25 de junio, el IPREM anual es de 6.390,13 euros.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué es copia autenticada?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>El Decreto 204/1995, de 29 de agosto,por el que se establecen medidas organizativas para los servicios administrativos de atención directa a los ciudadanos (BOJA de 26 de octubre, núm. 136), en el artículo 21 establece:</p>
<p>Art. 21 <strong><span style="text-decoration: underline;">Tipos de copias de documentos.</span></strong></p>
<p>Las copias de documentos en el ámbito de actuación de la Administración de la Junta de Andalucía, comprenderá los siguientes supuestos:</p>
<p>1. Las copias auténticas de documentos administrativos expedidos por el mismo órgano que emitió el documentos original.</p>
<p>2. <strong><span style="text-decoration: underline;">Las copias autenticadas de documentos privados y públicos, mediante cotejo con el original y en las que se estampará si procediera la correspondiente diligencia de compulsa.</span></strong></p>
<p>El artículo 22 del referido Decreto  recoge la competencia para la expedición de copias. En el apartado 2, establece "la expedición de copias autenticadas de documentos administrativos de la Junta de Andalucía, corresponderá a las jefaturas de sección u órgano asimilado dependiente del órgano que hubiera emitido el documento original y que tengan encomendada las funciones de tramitación o custodia del expediente a que pertenece dicho documento original"</p>
<div> </div>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p style="text-align: justify;"><strong>Para que los ciudadanos interpongan recurso de alzada, ¿deben haber presentado con anterioridad la correspondiente reclamación o alegaciones?</strong></p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">No es necesario haber presentado alegaciones contra la puntuación total obtenida en aplicación del baremo, ante la dirección de los centros docentes públicos o ante la persona física o jurídica titular de los centros docentes privados concertados, para interponer recurso de alzada contra las resoluciones de admisión del alumnado.</p>
<p style="text-align: justify;">El recurso de alzada se rige por lo establecido en al artículo 52 del decreto 40/2011, y en los artículos 107.1, 114 y 115 de la Ley 30/1992.</p>
<p style="text-align: justify;"> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>Al estar matriculado en Educación Secundaria Obligatoria para personas adultas, ¿podría matricularme en otra enseñanza?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Sólo si una de ellas se realiza en la modalidad semipresencial o a distancia.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Podría matricularme en un mismo año académico en enseñanzas elementales y profesionales de música y de danza?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong><span style="font-size: 12pt;">-No.</span></strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Podría matricularme en Bachillerato estando matriculado en un ciclo formativo de Formación Profesional?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Sólo si el módulo profesional que está realizando no requiere su presencia habitual y continuada en el centro.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuándo puedo solicitar los servicios complementarios (comedor, aula matinal y actividades extraescolares)?<br /> </p>
<dl id="jfaq" class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd id="aui_3_2_0_1211" style="display: block;">
<p style="text-align: justify;">La solicitud de los servicios complementarios se realiza durante el periodo de matriculación del alumnado, del 1 al 8 de junio en los centros públicos que imparten Educación Infantil (segundo ciclo) y Educación Primaria, y del 1 al 10 de julio en los centros públicos que imparten Educación Secundaria.</p>
<p style="text-align: justify;">La información de los servicios complementarios que ofertan los centros está a disposición del público en los tablones de anuncios de los mismos.</p>
<p id="aui_3_2_0_1210" style="text-align: justify;">Ninguna circunstancia que esté exclusivamente relacionada con la intención de solicitar dichos servicios podrá ser tenida en cuenta como criterio de admisión.<br /> </p>
</dd></dl></div>
<hr />
<p> </p>
<div class="detalle_html">
<p style="text-align: center;"><map name="Map"> 
<area title="Huelva - Se abre en ventana nueva" shape="poly" coords="7,74,4,56,14,41,24,31,46,41,44,48,34,54,40,62,38,86,19,72" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=HU" alt="Huelva" target="_blank" />
 
<area title="Sevilla - Se abre en ventana nueva" shape="poly" coords="47,40,54,39,61,28,65,32,75,49,72,52,76,55,85,51,97,71,87,79,80,84,72,84,65,84,56,88,47,89,38,89,41,65,36,55" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=SE" alt="Sevilla" target="_blank" />
 
<area title="Cádiz - Se abre en ventana nueva" shape="poly" coords="38,89,35,96,44,114,56,123,65,127,74,113,65,104,73,98,72,91,75,90,79,92,82,84,68,86" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=CA" alt="Cádiz" target="_blank" />
 
<area title="Córdoba - Se abre en ventana nueva" shape="poly" coords="67,32,70,29,67,19,81,6,91,5,112,17,116,27,110,33,112,51,120,64,112,67,110,74,105,72,97,71,83,50,76,54,70,34" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=CO" alt="Córdoba" target="_blank" />
 
<area title="Jaén - Se abre en ventana nueva" shape="poly" coords="113,18,171,6,180,20,172,36,164,49,152,55,139,57,124,66,120,65,114,53,111,40,117,28" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=JA" alt="Jaén" target="_blank" />
 
<area title="Granada - Se abre en ventana nueva" shape="poly" coords="176,29,190,36,186,54,172,66,172,72,164,70,152,95,139,97,128,95,112,86,109,81,110,73,117,68,127,66,142,57,155,54,168,43" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=GR" alt="Granada" target="_blank" />
 
<area title="Almería - Se abre en ventana nueva" shape="poly" coords="191,38,201,39,198,48,213,63,202,85,191,96,185,92,175,92,169,99,152,95,166,73,173,73,174,66,181,60,188,52" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=AL" alt="Almería" target="_blank" />
 
<area title="Málaga - Se abre en ventana nueva" shape="poly" coords="75,112,68,105,76,97,74,92,78,92,83,83,97,72,109,75,109,82,128,95,105,97,95,106,84,106,77,113" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=MA" alt="Málaga" target="_blank" />
 </map></p>
<div class="tituloCaja" style="text-align: left;">Consulta detallada de centros:</div>
<p>Consulta detallada de centros, enseñanzas autorizadas, adscripciones, servicios ofertados en el curso escolar 2014/2015.</p>
<p style="text-align: center;"><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/consulta-de-centros" target="_blank">Consulta de centros 2014/2015</a> <a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/consulta-de-centros"><img src="http://portal.ced.junta-andalucia.es/educacion/webportal/ishare-servlet/content/4e0e2b65-ba3c-4f79-8f34-ba1ef6dd083d" border="0" alt="Enlace" title="Enlace" style="width: 12px; height: 12px;" /></a></p>
<div class="tituloCaja" style="text-align: left;">Consulta de áreas de Influencia:</div>
<p style="text-align: left;">Pulse en una provincia para ver las áreas de influencia:</p>
<p style="text-align: center;"><a href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=GR"><img src="archivos_ies/13_14/mapa_andalucia2.jpg" border="0" alt="Imagen localización de centros (mapa_andalucia2.jpg)" title="Imagen localización de centros (mapa_andalucia2.jpg)" width="217" style="border: 0;" /></a></p>
<p style="text-align: center;"> </p>
<p style="text-align: center;">Descárguese la aplicación en versión de Smartphone para consultar los centros y las puntuaciones del procedimiento.</p>
<p style="text-align: center;"><a href="https://itunes.apple.com/es/app/iescolariza/id606516551?mt=8" target="_blank"><img src="archivos_ies/13_14/appstore_mini.png" border="0" alt="App Store Banner" title="App Store Banner" style="width: 137px; height: 45px; border: 0;" /></a> <a href="https://play.google.com/store/apps/details?id=es.juntadeandalucia.ced.escolarizacion" target="_blank"><img src="archivos_ies/13_14/google_play_mini.png" border="0" alt="Google Play Banner" title="Google Play Banner" style="text-align: justify; width: 131px; height: 43px; border: 0;" /></a></p>
</div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-3">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Matrícula y anulación</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:10</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=206:matricula-y-anulacion&amp;catid=242&amp;Itemid=613&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 2798
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Matrícula y anulación de matrícula</h2>
<p class="text-info"><strong><span style="color: #003366;">Plazo de Matrícula</span></strong></p>
<div id="articlepxfontsize1">
<ul>
<li>Del 1 al 10 de Julio</li>
<li>Del 1 al 8 de Septiembre</li>
</ul>
<p class="text-alert"><span style="color: #800000;"><strong>Anulación de Matrícula</strong></span></p>
<ul class="list-arrow arrow-blue">
<li><span style="text-decoration: underline;"><strong>En E.S.O. (con 16 años cumplidos) y BACHILLER:</strong></span></li>
</ul>
<div style="text-align: justify;">La dirección de los centros docentes, a petición razonada del alumnado o, si es menor de edad, de su padre, madre o tutor legal, antes del:</div>
<ul>
<li>30 de abril de cada curso</li>
</ul>
<div style="text-align: justify;">y cuando las causas alegadas imposibiliten la asistencia del alumno o alumna a clase, podrá dejar sin efectos su matrícula en E.S.O., Bachillerato o Educación Secundaria para Adultos. En este caso, la matrícula no será computada a los efectos del número máximo de años de permanencia en la enseñanza.<br /><br /></div>
<ul class="list-arrow arrow-blue">
<li><span style="text-decoration: underline;"><strong>En CICLOS FORMATIVOS:</strong></span></li>
</ul>
<div style="text-align: justify;">La anulación de la matrícula será del curso completo y se podrá llevar a cabo hasta dos meses antes de la celebración de la Evaluación Ordinaria del curso correspondiente, o lo que es lo mismo:</div>
<ul class="list-icon icon-calendar">
<li>Primer Curso: hasta el 31 de Marzo</li>
<li>Segundo Curso (Grado Superior): hasta el 31 de Enero</li>
</ul>
<div style="text-align: justify;">Para anular la matrícula es necesario cumplimentar el impreso de solicitud y presentarlo en la Secretaría del Centro.</div>
<ul class="list-icon icon-article">
<li>Descargar <span style="text-decoration: underline;"><strong>impreso</strong></span> de Anulación de Matrícula</li>
</ul>
</div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Listas de admitidos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 11 Junio 2014 10:16</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=207:listas-de-admitidos&amp;catid=242&amp;Itemid=614&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 4440
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div class="cajacomentario" style="margin: 2px 5px 20px; padding: 5px; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; text-align: center;">
<h2 style="text-align: center;"><span style="text-decoration: underline;"><strong><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal; text-decoration: underline;">NOVEDAD:</span></strong></span></h2>
<h3 style="text-align: center;"><span style="text-decoration: underline;"><a href="archivos_ies/13_14/Listados_de_admitidos.pdf" target="_blank" title="Lista de admitidos para el curso 2014/15."><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal; text-decoration: underline;">SE DA A CONOCER LA LISTA DE ADMITIDOS PARA EL PRÓXIMO CURSO 2014/15 EN EL IES MIGUEL DE CERVANTES</span></a></span></h3>
<center>
<h3 style="padding-left: 60px;"><span style="text-decoration: underline;"><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal; text-decoration: underline;"><a href="archivos_ies/13_14/Listado_de_alumnado_admitido_fase2.pdf" target="_blank"><strong>LISTADO DEL ALUMNADO ADMITIDO: 2ª FASE</strong></a></span></span><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal;"> (27/05/2014)</span><span style="text-decoration: underline;"><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal; text-decoration: underline;"><br /></span></span></h3>
</center>
<p> </p>
<span style="color: #414141; font-family: 'Century Gothic',Arial,Helvetica,sans-serif; font-size: 13px; line-height: normal;">(Actualizado el 27/05/14)</span></div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                            <div class="item column-2">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Ciclos Formativos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:12</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=208:ciclos-formativos&amp;catid=242&amp;Itemid=615&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 3062
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1>Ciclos Formativos</h1>
<ul>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/" target="_blank">PORTAL OFICIAL DE FORMACIÓN PROFESIONAL</a></li>
</ul>
<ul>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=209&amp;Itemid=616">Admisión en los Ciclos</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=211&amp;Itemid=617">Prueba de acceso</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=212&amp;Itemid=618">Pruebas Extraordinarias</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=213&amp;Itemid=619">Convalidaciones</a></li>
</ul>
<p> </p></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
                            <div class="items-row cols-2 row-1">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Admisión en Ciclos Formativos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:11</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=209:admision-en-ciclos-formativos&amp;catid=242&amp;Itemid=616&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 2564
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Admisión Ciclos Formativos</h2>
<p><strong>I. NORMATIVA REGULADORA</strong></p>
<ul class="list-icon icon-article">
<li><a href="http://www.juntadeandalucia.es/boja/2007/107/d1.pdf" target="_blank">Orden de 14 de Mayo de 2007, (BOJA nº 107 de 31-05-2007)</a></li>
</ul>
<p>II.<strong> DOCUMENTOS DESCARGABLES Y RELLENABLES</strong></p>
<ul class="list-icon icon-download">
<li><span class="jsn-listbullet"><a href="archivos_ies/solicitud_plaza_grado_superior.pdf" target="_blank">Admisión</a><br /></span></li>
<li><a href="archivos_ies/solicitud_reserva_plaza.pdf" target="_blank">Reserva de puesto escolar</a></li>
<li><a href="archivos_ies/anexo_ii_solicitud_gs_rellenable.pdf" target="_blank">Inscripción Prueba de Acceso (anexo II)</a></li>
<li><a href="archivos_ies/temario-parte-comun-anexo-v.pdf" target="_blank">Temario parte común (anexo V)</a></li>
<li><a href="archivos_ies/GS%20ANEXO%20VI.pdf" target="_blank">Opciones (anexo VI)</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1645-contenidos-y-criterios-de-evaluacion-grado-superior" target="_blank"><strong>CONTENIDOS Y CRITERIOS DE EVALUACIÓN</strong></a></li>
<li><a href="archivos_ies/EXENCIONES%20GS%20completo%20BUENO.pdf" target="_blank">Exención de materias de Bachillerato (anexo VIII)</a></li>
<li><a href="archivos_ies/solicitud_convo_gracia.pdf" target="_blank">Solicitud de convocatoria extraordinaria</a></li>
<li><a href="archivos_ies/solicitud_de_convalidaciones.pdf" target="_blank">Solicitud de convalidaciones</a></li>
</ul>
<ul>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso" target="_blank">PRUEBAS DE ACCESO: ÍNDICE</a> (Web Consejerái)</li>
</ul>
<p> </p>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion" target="_blank">Para más información</a></li>
</ul>
<p><strong>III. OFERTA</strong><br />La oferta educativa está disponible en la web del Instituto, en los Institutos, en las Delegaciones Provinciales y en la Web de la Consejería de Educación:</p>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/educacion" target="_blank">Web de la Consejería</a></li>
</ul>
<p><strong>IV. DOCUMENTACIÓN</strong></p>
<ul style="list-style-type: disc;">
<li>Cada solicitante presentará UNA ÚNICA SOLICITUD</li>
<li>Para presentar la solicitud es imprescindible estar en posesión de los requisitos académicos exigidos para realizar los estudios solicitados.</li>
<li>Impreso de solicitud: Anexo I para los Ciclos de Grado Medio o Anexo II para los Ciclos de Grado Superior, debidamente cumplimentado. En él se relacionarán, por orden de preferencia, los códigos de los ciclos formativos que se desee cursar, indicando en cada caso, si el ciclo es de Régimen General (G) o de Educación de Adultos (A) y los códigos de los centros correspondientes. Asimismo se acompañará de la siguiente documentación: </li>
</ul>
<p> </p>
<ol style="list-style-type: lower-alpha;">
<li style="list-style-type: none;"><ol style="list-style-type: lower-alpha;">
<li>Fotocopia del DNI, Pasaporte, Libro de Familia u otro documento oficial acreditativo de la identidad y de la edad.</li>
<li>Certificación académica personal, donde conste la nota media de los estudios realizados, o certificado de haber superado la prueba de acceso. </li>
<li>Fotocopia del dictamen emitido por el organismo público competente, en caso de padecer minusvalía.</li>
<li>Los solicitantes con estudios realizados en el extranjero deberán presentar, junto a la documentación antes indicada, la homologación de su titulación o, en su caso, el volante de solicitud de homologación sellado por la Delegación Provincial del Ministerio de Educación y Ciencia (tiene validez provisional durante 6 meses). </li>
</ol></li>
</ol>
<p><strong>V. LUGAR DE PRESENTACIÓN</strong></p>
<ol style="list-style-type: lower-alpha;">
<li>En el Centro Educativo solicitado en primer lugar.</li>
<li>En los registros de cualquier órgano administrativo, perteneciente a la Administración General del Estado, a la Administración de las Comunidades Autónomas.</li>
<li>Por Correo Certificado. </li>
</ol>
<p><strong>Nota:</strong> La presentación de solicitudes en más de un centro educativo dará lugar a que sólo se tenga en cuenta la última presentada. <br /><br /><strong>VI. PLAZOS DE PRESENTACIÓN</strong></p>
<ul class="list-icon icon-calendar">
<li>Del 1 al 25 de junio para el alumnado de 4º de ESO, 2º ESA o 2º de Bachillerato que haya aprobado todo el curso en junio o la Prueba de Acceso en este mismo periodo.</li>
<li>Del 1 al 10 de septiembre para el alumnado de 4º de ESO, 2º ESA o 2º de Bachillerato que haya aprobado todo el curso en septiembre o la Prueba de Acceso en este mismo periodo.</li>
<li>Del 3 al 5 de octubre (Procedimiento Extraordinario), para el alumnado que no haya presentado Solicitud en los dos procesos anteriores.</li>
</ul>
<p><strong>Nota:</strong> La solicitud será única para todo el proceso. La duplicidad de solicitud en junio y septiembre implica la desestimación. <br /><br /><strong>VI. PUBLICACIÓN DE DATOS PROVISIONALES</strong></p>
<ul class="list-icon icon-calendar">
<li>29 de junio, para las solicitudes presentadas entre el 1 y el 25 de junio.</li>
<li>13 de septiembre, para las solicitudes presentadas del 1 al 10 de septiembre.</li>
<li>9 de octubre, para las solicitudes del Proceso Extraordinario presentadas del 3 al 5 de octubre.</li>
</ul>
<p><strong>Nota:</strong> Es aconsejable consultar los listados provisionales cuando se publiquen en el tablón de anuncios del Centro o en la web de la Consejería de Educación (Novedades)<br /><br /><strong> VII. PERIODO DE RECLAMACIONES</strong></p>
<ul class="list-icon icon-calendar">
<li>Del 29 de junio al 4 de julio para el alumnado solicitante de junio.</li>
<li>13 y 14 de septiembre para el alumnado solicitante de septiembre.</li>
<li>9 y 10 de octubre para las solicitudes del Proceso Extraordinario.</li>
</ul>
<p><strong>Nota:</strong> Las reclamaciones se presentarán en el centro solicitado en primer lugar. Todas aquellas que se presenten fuera de plazo serán desestimadas. <br /><br /><strong>VIII. ADJUDICACIÓN DE PUESTOS ESCOLARES</strong></p>
<ul class="list-icon icon-calendar">
<li>10 de julio, 1ª adjudicación.</li>
<li>23 de julio, 2ª adjudicación.</li>
<li>20 de septiembre, 3ª adjudicación.</li>
<li>26 de septiembre, 4ª adjudicación.</li>
<li>11 de octubre, Adjudicación Extraordinaria</li>
</ul>
<p><strong>IX. MATRÍCULA O RESERVA DE PLAZA</strong><br />La matrícula se realizará en el centro educativo donde se ha obtenido plaza, y es obligatoria si dicha plaza corresponde a la solicitada en primer lugar.<br />La reserva se realizará, mediante un Anexo V, en el centro educativo donde se ha obtenido plaza y es obligatoria, (si no se opta por la matrícula) con el fin de obtener una opción más favorable en el proceso, en caso de no obtener plaza en la primera opción. Mientras no se haga efectiva la matrícula, es obligatoria en cada adjudicación realizar la reserva correspondiente. <br />Los plazos establecidos son los siguientes:</p>
<ul class="list-icon icon-calendar">
<li>Del 25 al 30 de junio, para el alumnado que repite curso y 2º curso.</li>
<li>Del 10 al 16 de julio, para la 1ª adjudicación.</li>
<li>Del 3 al 8 de septiembre, para la 2ª adjudicación.</li>
<li>Del 20 al 24 de septiembre, para la 3ª adjudicación.</li>
<li>Del 26 al 28 de septiembre, para la 4ª adjudicación.</li>
<li>Del 15 al 16 de octubre, para la Matrícula Extraordinaria</li>
</ul>
<p><strong>Nota: </strong>No realizar matrícula o reserva en los plazos establecidos para ello, supondrá la renuncia a seguir participando en el proceso. La reserva se repetirá en cada adjudicación, aunque se haya obtenido el mismo centro.</p>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/" target="_blank">Más Información</a></li>
<li><a href="https://www.juntadeandalucia.es/educacion/secretariavirtual/consultas/acceso.html?idConsulta=45" target="_blank">Consulta Personalizada de las Adjudicaciones</a></li>
</ul></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                            <div class="item column-2">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Escolarización</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:09</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=210:escolarizacion&amp;catid=242&amp;Itemid=611&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 2749
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2>ESCOLARIZACIÓN</h2>
<ul>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=205&amp;Itemid=612">Reserva de plaza</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=206&amp;Itemid=613">Matricula y anulación</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=207&amp;Itemid=614">Lista de admitidos</a></li>
</ul>
<p> </p></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postcontent">
<div class="items-more">

<div class="items-more">

<h3>Más Artículos...</h3>
<ol>
	<li>
		<a href="/index.php?option=com_content&amp;view=article&amp;id=211:prueba-de-acceso&amp;catid=242&amp;Itemid=617">
			Prueba de acceso</a>
	</li>
	<li>
		<a href="/index.php?option=com_content&amp;view=article&amp;id=212:pruebas-extraordinarias&amp;catid=242&amp;Itemid=618">
			Pruebas extraordinarias</a>
	</li>
	<li>
		<a href="/index.php?option=com_content&amp;view=article&amp;id=213:convalidaciones&amp;catid=242&amp;Itemid=619">
			Convalidaciones</a>
	</li>
</ol>
</div>
</div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postcontent">
<div class="pagination"><p class="counter">Página 1 de 2</p><ul><li class="pagination-start"><span class="pagenav">Iniciar</span></li><li class="pagination-prev"><span class="pagenav">Previo</span></li><li><span class="pagenav">1</span></li><li><a title="2" href="/index.php?option=com_content&amp;view=category&amp;id=242&amp;Itemid=250&amp;limitstart=8" class="pagenav">2</a></li><li class="pagination-next"><a title="Siguiente" href="/index.php?option=com_content&amp;view=category&amp;id=242&amp;Itemid=250&amp;limitstart=8" class="pagenav">Siguiente</a></li><li class="pagination-end"><a title="Fin" href="/index.php?option=com_content&amp;view=category&amp;id=242&amp;Itemid=250&amp;limitstart=8" class="pagenav">Fin</a></li></ul></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:57:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - INICIO";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:102:"/index.php?option=com_content&amp;view=category&amp;id=242&amp;Itemid=250&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:103:"/index.php?option=com_content&amp;view=category&amp;id=242&amp;Itemid=250&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"Secretaría";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
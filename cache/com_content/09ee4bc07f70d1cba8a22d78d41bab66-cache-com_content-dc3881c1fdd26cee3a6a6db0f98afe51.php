<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7458:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">2º BACH B</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">PROYECTO INTEGRADO 2º BACH B</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 01 Febrero 2013 09:09</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=64:proyecto-integrado-2o-bach-b&amp;catid=40&amp;Itemid=246&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=bee6bd9667bfab999170c2b7c4168997ed7d4a74" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 5581
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><span style="text-decoration: underline;"><strong>PÁGINA PRINCIPAL DEL PROYECTO INTEGRADO DE 2º BACHILLERATO B. CURSO 2012/13.</strong></span></p>
<hr />
<p> </p>
<p>GRUPOS Y TRABAJOS:</p>
<p> </p>
<table border="0">
<tbody>
<tr>
<td><strong><span style="text-decoration: underline;"><span style="color: #000000; text-decoration: underline;">1.</span></span></strong></td>
<td>
<p><strong><span style="text-decoration: underline;"><span style="color: #000000; text-decoration: underline;">LIDIA PÉREZ BAENA.</span></span></strong></p>
<p><strong><span style="text-decoration: underline;"><span style="color: #000000; text-decoration: underline;">MAYTE LUZÓN GONZÁLEZ.</span></span></strong></p>
<p><strong><span style="text-decoration: underline;"><span style="color: #000000; text-decoration: underline;">XAVIER MARCOS NAVARRO.</span></span></strong></p>
<p><strong><span style="text-decoration: underline;"><span style="color: #000000; text-decoration: underline;">ANGUSTIAS MUÑOZ DOMINGO.</span></span></strong></p>
</td>
<td>
<p><strong><span style="text-decoration: underline;"><span style="color: #ff0000; text-decoration: underline;">1ª EVA:</span></span></strong><span style="color: #ff0000;">CATÁSTROFES DEL MUNDO Y SUS POSIBLES PREDICCIONES. Presentación.</span></p>
<strong><span style="text-decoration: underline;">2ª EVA:</span> </strong><span style="color: #ff0000;">CATÁSTROFES DEL MUNDO Y SUS POSIBLES PREDICCIONES. Presentación.</span></td>
</tr>
<tr>
<td style="text-align: left;">2.</td>
<td style="text-align: left;">
<p>PATRICIA OJEL-JARAMILLO LÓPEZ.</p>
<p style="text-align: left;">PATRICIA MORENO ESTÉBANÉ.</p>
<p>SANDRA RAMÍREZ ROBLEDILLO.</p>
<p>ALFONSO MORÓN ALCÁNTARA</p>
</td>
<td>
<p><strong><span style="text-decoration: underline;">1ª EVA:</span></strong> FALSEDAD DE LA LLEGADA DEL HOMBRE A LA LUNA. DEMOSTRACIÓN.</p>
  <strong><span style="text-decoration: underline;">2ª EVA:</span></strong> DISEÑO DE CIRCUITO ELECTRÓNICO.<strong><span style="text-decoration: underline;"><br /></span></strong></td>
</tr>
<tr>
<td>3.</td>
<td>
<p>PEDRO DANIEL HERRERA LÓPEZ.</p>
<p>LUCIANO NICOLÁS BAGATOLLI SALVINI</p>
</td>
<td>
<p><strong><span style="text-decoration: underline;">1ª EVA:</span></strong> INICIOS DEL ROCK.</p>
<strong>  <span style="text-decoration: underline;">2ª EVA:</span></strong> LA HISTORIA DE LAS VIDEOCONSOLAS.</td>
</tr>
<tr>
<td>4.</td>
<td>
<p>ANDREA MARTÍNEZ LÓPEZ.</p>
<p>NOELIA FERNÁNDEZ SALMERÓN.</p>
</td>
<td>
<p><strong><span style="text-decoration: underline;">1ª EVA:</span> </strong><span style="font-size: 10pt;">CONSECUENCIAS DE LA ADICCIÓN A LAS NUEVAS TECNOLOGÍAS.</span></p>
<p><br /><span style="text-decoration: underline;"><strong>2ª EVA:</strong></span> ANIMALES EXTAÑOS DEL MUNDO.<strong><span style="text-decoration: underline;"><br /></span></strong></p>
</td>
</tr>
<tr>
<td>5.</td>
<td>
<p><span style="text-decoration: underline;"><strong>JESÚS CAÑIZARES LÓPEZ.</strong></span></p>
<p><span style="text-decoration: underline;"><strong>JOSÉ MARÍA GUERRERO PÉREZ.</strong></span></p>
<p><span style="text-decoration: underline;"><strong>JOSÉ MANUEL ROBLEDO REYES.</strong></span></p>
<p><span style="text-decoration: underline;"><strong>SERGIO MOLINA DÍEZ</strong></span></p>
</td>
<td>
<p><strong><span style="text-decoration: underline;">1ª EVA:</span></strong> <span style="color: #ff0000;">EL</span><span style="color: #ff0000;"> MINICAR 3JS. Presentación.</span></p>
<br />  <span style="text-decoration: underline;"><strong>2ª EVA:</strong></span> <span style="color: #ff0000;">EL</span><span style="color: #ff0000;"> MINICAR 3JS. Continuación.</span></td>
</tr>
<tr>
<td>6.</td>
<td>
<p><span style="text-decoration: underline;"><strong>ANDRÉS GIL CARMONA.</strong></span></p>
<p><span style="text-decoration: underline;"><strong>ÁLVARO MATEOS TOSET</strong></span></p>
<p><span style="text-decoration: underline;"><strong>DANIEL CABALLERO DÍAZ.</strong></span></p>
<p><span style="text-decoration: underline;"><strong>CARLOS RODRÍGUEZ FERNÁNDEZ</strong></span></p>
</td>
<td>
<p><strong><span style="text-decoration: underline;">1ª EVA:</span> </strong><span style="color: #ff0000;">REACCIONES</span><span style="color: #ff0000;"> QUÍMICAS. ESPUMA. Vídeo.</span></p>
  <strong><span style="text-decoration: underline;">2ª EVA:</span></strong> EXPLOSIONES INCONTROLADAS.<strong><span style="text-decoration: underline;"><br /></span></strong></td>
</tr>
<tr>
<td>7.</td>
<td>
<p><span style="text-decoration: underline;"><strong>LAURA CUEVAS BIEDMA.</strong></span></p>
<p><span style="text-decoration: underline;"><strong>MARÍA TORRES PASTOR.</strong></span></p>
<p><span style="text-decoration: underline;"><strong>ANTONIO JOSÉ RODRÍGUEZ DE HARO.</strong></span></p>
</td>
<td>
<p><strong><span style="text-decoration: underline;">1ª EVA:</span></strong> <span style="font-size: 10pt;"><span style="color: #ff0000;">POSIBLE</span></span><strong><span style="font-size: 10pt;"><span style="color: #ff0000;"> FIN DEL MUNDO.</span> <span style="color: #ff0000;">Presentación.</span></span></strong></p>
<p><strong><span style="text-decoration: underline;">2ª EVA:</span></strong> SUEÑOS.<strong><span style="text-decoration: underline;"><br /></span></strong></p>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Proyectos</span> / <span class="art-post-metadata-category-name">PROYECTOS</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:61:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - 2º BACH B";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:13:"PROYECTOS ==>";s:4:"link";s:59:"index.php?option=com_content&view=categories&id=0&Itemid=28";}i:2;O:8:"stdClass":2:{s:4:"name";s:20:"PROYECTOS INTEGRADOS";s:4:"link";s:72:"index.php?option=com_content&view=category&layout=blog&id=229&Itemid=244";}i:3;O:8:"stdClass":2:{s:4:"name";s:10:"2º BACH B";s:4:"link";s:58:"index.php?option=com_content&view=article&id=64&Itemid=246";}}s:6:"module";a:0:{}}
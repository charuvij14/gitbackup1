<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6079:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">FP</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 20 Febrero 2013 06:22</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=199:fpe&amp;catid=242&amp;Itemid=603&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=63251b391e736810a7602e72d1914aa0f6c00fb9" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3277
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p> </p>
<h1 style="background-color: #fbe9c5;"><span style="text-decoration: underline;">ciclo superior de:</span></h1>
<h1 style="background-color: #fbe9c5;"><span style="text-decoration: underline;"> animación socio-cultural</span>    </h1>
<div class="jsn-article-content" style="text-align: justify; background-color: #fbe9c5;">
<h2 style="padding-left: 30px;"><strong> </strong></h2>
<table style="width: 687px;" border="0">
<tbody>
<tr style="background-color: #4786be;">
<td style="text-align: center; width: 270px;"><span style="color: #ffffff;"><strong>Materias Obligatorias</strong></span></td>
<td style="text-align: center; width: 50px;"><span style="color: #ffffff;"><strong>Horas totales</strong></span></td>
<td style="text-align: center; width: 99px;"><span style="color: #ffffff;"><strong>Horas semanales</strong></span></td>
</tr>
<tr>
<td style="width: 270px;">Animación de ocio y tiempo libre</td>
<td style="text-align: center; width: 50px;">160</td>
<td style="width: 99px; text-align: center;"> 5</td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">Desarrollo comunitario</td>
<td style="text-align: center; width: 50px;">160</td>
<td style="width: 99px; text-align: center;"> 5</td>
</tr>
<tr>
<td style="width: 270px;">Animación cultural</td>
<td style="text-align: center; width: 50px;">160</td>
<td style="width: 99px; text-align: center;"> 5</td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">Organización y gestión de una pequeña empresa de actividades de tiempo libre y socioeducativas</td>
<td style="text-align: center; width: 50px;">96</td>
<td style="width: 99px; text-align: center;"> 3</td>
</tr>
<tr>
<td style="width: 270px;">Animación y dinámica de grupos</td>
<td style="text-align: center; width: 50px;">128</td>
<td style="width: 99px; text-align: center;"> 4</td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">Metodología de la intervención social</td>
<td style="text-align: center; width: 50px;">160</td>
<td style="width: 99px; text-align: center;"> 5</td>
</tr>
<tr>
<td style="width: 270px;">Los servicios sociocomunitarios en Andalucía</td>
<td style="text-align: center; width: 50px;">32</td>
<td style="width: 99px; text-align: center;"> 1</td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">Formación y orientación laboral</td>
<td style="text-align: center; width: 50px;">64</td>
<td style="width: 99px; text-align: center;"> 2</td>
</tr>
<tr>
<td style="width: 270px;">
<p>Proyecto integrado <span style="color: inherit; font-family: inherit; font-size: inherit;">(*)</span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;">Formación en centros de trabajo (*)</span></p>
</td>
<td style="text-align: center; width: 50px;">
<p style="text-align: center;"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">740 </span></p>
</td>
<td style="width: 99px;"> </td>
</tr>
<tr style="background-color: #efefef;">
<td style="width: 270px;">
<p> <span style="color: #000000; font-size: medium; text-align: justify;">(*) Las 740 horas se reparten entre el Proyecto Integrado y la Formación en Centros de Trabajo.</span></p>
<p> </p>
</td>
<td style="text-align: center; width: 50px;"> </td>
<td style="width: 99px;"> </td>
</tr>
</tbody>
</table>
</div>
<div class="jsn-article-content" style="text-align: justify; background-color: #fbe9c5;"> <span style="color: #000000; font-size: medium;">Una vez cursado el Ciclo Superior de Grado Superior que consta de 17OO horas de las cuales 860 son de carácter lectivo y 740 de formación práctica en Centros de Trabajo, se puede acceder a estudios universitarios relacionados con la temática del Ciclo Superior de Animación Sociocultural como son los siguientes:</span></div>
<ul style="color: #000000; font-family: 'Times New Roman'; font-size: medium;">
<li>Pedagogía.</li>
<li>Magisterio</li>
<li>Logopedia</li>
<li>Trabajo Social.</li>
</ul>
<div class="jsn-article-content" style="text-align: justify; background-color: #fbe9c5; padding-left: 30px;"><strong style="text-transform: uppercase; color: #4786be; font-size: 10pt;"> </strong></div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:53:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - FP";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:16:"Oferta Educativa";s:4:"link";s:59:"index.php?option=com_content&view=article&id=196&Itemid=599";}i:1;O:8:"stdClass":2:{s:4:"name";s:2:"FP";s:4:"link";s:59:"index.php?option=com_content&view=article&id=199&Itemid=603";}}s:6:"module";a:0:{}}
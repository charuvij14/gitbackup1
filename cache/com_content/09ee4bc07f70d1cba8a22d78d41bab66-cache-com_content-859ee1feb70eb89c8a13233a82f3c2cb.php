<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5267:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Ed. Plástica 1º ESO</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 12 Abril 2015 18:41</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=280:ed-plastica-1-eso&amp;catid=97&amp;Itemid=718&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=782d1d9e93495c73cda88f2c82af2f0fc0565a5c" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1328
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 style="background-color: #e7eaad;"><span style="text-decoration: underline;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt;">EDUCACIÓN PLÁSTICA Y VISUAL.</span></span></h1>
<h3 style="background-color: #e7eaad;"><span style="text-decoration: underline;"><strong><span style="color: #2a2a2a;">1º ESO (12/04/15)</span></strong></span></h3>
<p style="padding-left: 30px;"><a href="archivos_ies/14_15/dibujo/1eso/Portada-1esoNN.pdf" target="_blank"><span style="text-decoration: underline;"><strong><span style="color: #2a2a2a;">PORTADA.</span></strong></span></a></p>
<ol style="text-align: justify; background-color: #e7eaad;">
<li><a href="archivos_ies/14_15/dibujo/1eso/L1_Horizontales_y_verticales-1eso.pdf" target="_blank" title="Lámina 1.">Lámina 1.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L2_Lineas%20oblicuas_y_color-1eso.pdf" target="_blank">Lámina 2.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L3_Tramas_puntos-1eso.pdf" target="_blank">Lámina 3.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L4_Claroscuro1eso.pdf" target="_blank">Lámina 4.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L5_Colores_complementarios-1eso.pdf" target="_blank">Lámina 5.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L6_frios_y_calidos-1eso.pdf" target="_blank">Lámina 6.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L7_Anamorfosis-1eso.pdf" target="_blank">Lámina 7.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L8_La_forma_II-1eso.pdf" target="_blank">Lámina 8.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L9_Escalas-1eso.pdf" target="_blank">Lámina 9.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L10_Doriforo-1eso.pdf" target="_blank">Lámina 10.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L11_Formas_geome_1eso.pdf" target="_blank">Lámina 11.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L12_Poligonillos-1eso.pdf" target="_blank">Lámina 12.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L13_segun_lado-1eso.pdf" target="_blank">Lámina 13.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L14-1eso.pdf" target="_blank">Lámina 14.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L15_Proyeccion_conica_II-1eso.pdf" target="_blank">Lámina 15.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L16_Conica_III-1eso.pdf" target="_blank">Lámina 16.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/Planta_alzado_y_perfil-1eso.pdf" target="_blank">Lámina 17.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L18-1eso.pdf" target="_blank">Lámina 18.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L19-1eso.pdf" target="_blank">Lámina 19.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L20-1eso.pdf" target="_blank">Lámina 20.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L21-1eso.pdf" target="_blank">Lámina 21.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L22-1eso.pdf" target="_blank">Lámina 22.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L23-1eso.pdf" target="_blank">Lámina 23.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L24-1eso.pdf" target="_blank">Lámina 24.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L25-1eso.pdf" target="_blank">Lámina 25.</a></li>
<li><a href="archivos_ies/14_15/dibujo/1eso/L26-1eso.pdf" target="_blank">Lámina 26.</a></li>
</ol></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">Educación Plástica</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:72:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - ED. PLÁSTICA 1º ESO";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:16:"Área Artística";s:4:"link";s:59:"index.php?option=com_content&view=article&id=184&Itemid=570";}i:2;O:8:"stdClass":2:{s:4:"name";s:29:"Dpto. de Educación Plástica";s:4:"link";s:57:"index.php?option=com_content&view=article&id=76&Itemid=98";}i:3;O:8:"stdClass":2:{s:4:"name";s:21:"ED. PLÁSTICA 1º ESO";s:4:"link";s:59:"index.php?option=com_content&view=article&id=280&Itemid=718";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12831:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 09 Septiembre 2014 18:28</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=139:selectividad&amp;catid=59&amp;Itemid=218&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=5052aebe8b96e58132f68d05417cb9343eacbeba" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 21564
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p>&nbsp;</p>
<div id="pagina">
<h1 id="titulo_pagina"><span class="texto_titulo">Inscripción Pruebas de Acceso a la Universidad</span></h1>
<a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion?theme=pdf" id="enlace_pdf" title="Versión en PDF">Descargar versión en PDF</a>
<div id="contenido" class="sec_interior">
<div class="toc">
<div id="toc__inside">
<ul class="toc">
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_requisitos" class="toc">Requisitos</a></span></div>
</li>
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_plazos_y_solicitud" class="toc">Plazos y solicitud</a></span></div>
</li>
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_precios_de_inscripcion" class="toc">Precios de inscripción</a></span></div>
</li>
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_hoja_informativa" class="toc">Hoja informativa</a></span></div>
</li>
<li class="level1">
<div class="li"><span class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/fechas_inscripcion#__doku_adaptacion_de_los_examenes_para_estudiantes_con_discapacidad" class="toc">Adaptación de los exámenes para estudiantes con discapacidad</a></span></div>
</li>
</ul>
</div>
</div>
<div class="content_doku">
<h1><a id="__doku_requisitos"></a>Requisitos</h1>
<div class="level1">
<ul>
<li class="level1">
<div class="li">Estar en posesión del <strong>Título de Bachillerato</strong> para la realización de la prueba completa.</div>
</li>
<li class="level1">
<div class="li">Estar en posesión del <strong>Título de técnico superior de formación profesional, técnico superior de artes plásticas y diseño, o técnico deportivo superior o título equivalente</strong>, únicamente a efectos de la <em class="u">fase específica voluntaria</em> para mejorar la nota de admisión a las enseñanzas oficiales de grado.</div>
</li>
</ul>
</div>
<h1><a id="__doku_plazos_y_solicitud"></a>Plazos y solicitud</h1>
<div class="level1">
<p>Todos los/as estudiantes que estén interesados en participar en la Prueba de Acceso a la Universidad tanto en su fase general como específica, ya sean alumnos/as que se presentan por primera vez o para mejorar nota, en cualquiera de sus convocatorias Ordinaria o Extraordinaria, deberán realizar los siguientes pasos en los plazos establecidos:</p>
</div>
<h2><a id="__doku_primer_pasoregistrarse"></a>PRIMER PASO: REGISTRARSE</h2>
<div class="level2">
<p>Hay que cumplimentar el trámite de <strong>REGISTRO</strong> a través del portal WEB de Selectividad:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" class="urlextern" title="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></div>
</li>
</ul>
<p><strong>PLAZO DE REGISTRO</strong> SELECTIVIDAD CONVOCATORIA 2014:</p>
<table class="inline">
<tbody>
<tr><th>Convocatoria Ordinaria</th><th>Convocatoria Extraordinaria</th></tr>
<tr>
<td class="leftalign">del 22 de abril al 4 de junio 2014</td>
<td class="leftalign">del 1 de agosto al 5 de septiembre 2014</td>
</tr>
</tbody>
</table>
<p>Aquí puedes encontrar una presentación que te ayudará a realizar el registro:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad1" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad1">Alumnos que este curso aprobarán el 2º de Bachillerato o el 2º curso del CFGS en un centro de Granada, Ceuta, Melilla o centros adscritos a la Universidad de Granada en Marruecos </a></div>
</li>
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad2" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad2">Alumnos que se han presentado alguna vez a Selectividad en la Universidad de Granada</a></div>
</li>
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad3" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionregistroselectividad3">Alumnos procedentes de otras universidades y que nunca se han presentado a Selectividad en la Universidad de Granada</a></div>
</li>
</ul>
</div>
<h2><a id="__doku_segundo_pasomatricularse"></a>SEGUNDO PASO: MATRICULARSE</h2>
<div class="level2">
<p>Una vez hecho el registro hay que realizar la inscripción (<strong>MATRÍCULA</strong>) en la prueba a través del portal WEB de Selectividad:</p>
<ul class="enlaces">
<li class="level1">
<div class="li"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" class="urlextern" title="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></div>
</li>
</ul>
<p><strong>PLAZO DE MATRÍCULA</strong> SELECTIVIDAD CONVOCATORIA 2014:</p>
<table class="inline">
<tbody>
<tr><th>Convocatoria Ordinaria</th><th>Convocatoria Extraordinaria</th></tr>
<tr>
<td class="leftalign">2 al 4 de junio 2014</td>
<td class="leftalign">3 al 5 de septiembre 2014</td>
</tr>
</tbody>
</table>
<ul class="departamento">
<li class="level1 level1_primero impar">
<div class="li"><strong>MUY IMPORTANTE:</strong></div>
<ul>
<li class="level4">
<div class="li"><strong>La matrícula se podrá realizar hasta las 24:00h del día en que finaliza el plazo y no se admitirán pagos posteriores a las 14:00h del día siguiente a dicha finalización.</strong></div>
</li>
</ul>
</li>
</ul>
<ul class="enlaces">
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectividadgranada" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectividadgranada"> Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Granada y provincia </a></div>
</li>
<li class="level1">
<div class="li"><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectivdadceutamelillamarruecos" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/presentacionmatriculaselectivdadceutamelillamarruecos"> Presentación en Powerpoint de los pasos a seguir para realizar la MATRICULA para alumnos de Ceuta, Melilla y Marruecos </a></div>
</li>
</ul>
<p><strong>NOTA CONVOCATORIA SEPTIEMBRE: Solo el ingreso</strong> mediante carta de pago se podrá realizar hasta el día 8 de septiembre debido a que los días 6 y 7 de septiembre son sábado y domingo respectivamente; aunque la carta de pago se debe haber obtenido en el plazo del 3 al 5 de septiembre.</p>
</div>
<h1><a id="__doku_precios_de_inscripcion"></a>Precios de inscripción</h1>
<div class="level1">
<ul>
<li class="level1">
<div class="li">Los derechos de examen se podrán ingresar mediante <strong>Carta de Pago</strong> en cualquier sucursal de <em>Caja Granada o Caja Rural de Granada</em> o mediante <strong>pago con tarjeta</strong>. Ambas opciones estarán reflejadas en la solicitud de inscripción que deberá realizar en esta misma WEB.</div>
</li>
</ul>
<ul>
<li class="level1">
<div class="li">La cantidad a ingresar será:</div>
</li>
</ul>
<ul class="departamento">
<li class="level1 par">
<div class="li"><strong>Precios públicos ordinarios:</strong></div>
<ul>
<li class="level4">
<div class="li"><strong>Fase General</strong>: 58.70 € .</div>
</li>
<li class="level4">
<div class="li"><strong>Fase Específica</strong>: 14.70 € x número de materias de las que se examina.</div>
</li>
</ul>
</li>
</ul>
<ul class="departamento">
<li class="level1 impar">
<div class="li"><strong>Precios públicos para beneficiarios Familia Numerosa:</strong></div>
<ul>
<li class="level4">
<div class="li">De categoría <strong>general</strong>: ......................... Reducción del 50%.</div>
</li>
<li class="level4">
<div class="li">De categoría <strong>especial</strong>: ........................ Exentos de pago.</div>
</li>
</ul>
</li>
</ul>
<ul class="departamento">
<li class="level1 par">
<div class="li"><strong>Precios públicos para personas con discapacidad</strong>:</div>
<ul>
<li class="level4">
<div class="li">Exentos de pago.</div>
</li>
<li class="level4">
<div class="li">tendrán la consideración de personas con discapacidad aquellas a quienes se les haya reconocido un grado de minusvalía igual o superior al 33 % por la Consejería para la Igualdad y Bienestar Social de la Junta de Andalucía u órgano competente en la comunidad de procedencia del interesado.</div>
</li>
</ul>
</li>
<li class="level1 level1_ultimo impar">
<div class="li"><strong>IMPORTANTE:</strong></div>
<ul>
<li class="level4">
<div class="li">Los alumnos que tengan exención total de precios públicos bien por <em class="u"><strong>FAMILIA NUMEROSA DE CATEGORÍA ESPECIAL</strong></em>, o <em class="u"><strong>DISCAPACIDAD</strong></em> deberán acceder a la misma página web y seguir el mismo proceso de matriculación que finalizará con la impresión del RESGUARDO, quedando ya matriculados. El reguardo deberán llevarlo el día del examen.</div>
</li>
<li class="level4">
<div class="li">Los documentos justificativos de familia numerosa como de discapacidad deben estar actualizados en el momento de realizar la inscripción.</div>
</li>
</ul>
</li>
</ul>
</div>
<h1><a id="__doku_hoja_informativa"></a>Hoja informativa</h1>
<div class="level1">
<p><a href="http://serviciodealumnos.ugr.es/pages/pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02" class="wikilink2" title="pruebas_acceso/selectividad/alumnos/hojainformativapruebadeaccesoalauniversidad2014rev02"> Hoja informativa inscripción/horarios Selectividad </a></p>
</div>
<h1><a id="__doku_adaptacion_de_los_examenes_para_estudiantes_con_discapacidad"></a>Adaptación de los exámenes para estudiantes con discapacidad</h1>
<div class="level1">
<p>Todos los alumnos que padezcan alguna discapacidad y que necesiten condiciones especiales para la realización de las pruebas deberán:</p>
<ul>
<li class="level1">
<div class="li">Comunicarlo a este Servicio y justificarlo mediante informe del Centro en el que consten las adaptaciones que necesite, y certificado expedido por la Consejería para la igualdad y bienestar social de la Junta de Andalucía o la administración correspondiente en otra Comunidad, en el que conste el grado de minusvalía.</div>
</li>
</ul>
<p>Los documentos acreditativos deberán estar actualizados.</p>
</div>
</div>
</div>
</div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:67:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Selectividad_OLD";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:12:"SELECTIVIDAD";s:4:"link";s:59:"index.php?option=com_content&view=article&id=259&Itemid=218";}i:1;O:8:"stdClass":2:{s:4:"name";s:8:"ALUMNADO";s:4:"link";s:60:"index.php?option=com_content&view=category&id=222&Itemid=218";}i:2;O:8:"stdClass":2:{s:4:"name";s:12:"Selectividad";s:4:"link";s:59:"index.php?option=com_content&view=category&id=59&Itemid=218";}i:3;O:8:"stdClass":2:{s:4:"name";s:16:"Selectividad_OLD";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:29079:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Coeducación</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Feria Intercultural de la Tareas Domésticas</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 25 Mayo 2011 20:06</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=133:feria-intercultural-de-la-tareas-domesticas&amp;catid=132&amp;Itemid=188&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=f202cb45881f0ef0c34eca75c2c5274ba41f2bab" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 5678
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div align="center" style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: center;">
<p><span style="text-decoration: underline; font-size: 14pt;"><strong>FERIA INTERCULTURAL DE LAS TAREAS DOMÉSTICAS. </strong></span><br /><span style="font-size: 14pt;">DEL 2 AL 4 DE MARZO DE 2011.&nbsp;</span><br /><span style="font-size: 14pt;">IES MIGUEL DE CERVANTES.&nbsp;</span><br /><span style="font-size: 14pt;">GRANADA.</span></p>
</div>
<div align="center" style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: center;"><span style="font-family: Calibri; font-size: 16px; line-height: 18px; color: #000000;">En relación con la celebración el 8 de marzo del "Día de la Mujer Trabajadora" y como parte de las actividades programadas en el "Plan de Igualdad de Hombres y Mujeres" que se desarrolla en el IES Miguel de Cervantes, del 2 al&nbsp;&nbsp;4 de marzo se celebrará en nuestro instituto una</span></div>
<div align="center" style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: center;"><span style="text-decoration: underline;"><b><span style="color: black; font-size: 12pt; line-height: 18px;"><span style="font-family: Calibri;">FERIA INTERCULTURAL DE LAS TAREAS DOMÉSTICAS</span></span></b></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 0pt; margin-left: 0cm; line-height: normal; text-align: justify;"><span style="color: black; font-size: 12pt;"><span style="font-family: Calibri;">con el lema:</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 0pt; margin-left: 0cm; line-height: normal; text-align: justify;"></div>
<div align="center" style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 0pt; margin-left: 0cm; line-height: normal; text-align: center;"><span style="color: black; font-size: 12pt;"><span style="font-family: Calibri;">"TODOS CUIDAMOS PORQUE A TODOS NOS GUSTA QUE NOS CUIDEN.&nbsp;"</span></span></div>
<div align="center" style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 0pt; margin-left: 0cm; line-height: normal; text-align: center;"><span style="color: black; font-size: 12pt;"><span style="font-family: Calibri;"><br /></span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 0pt; margin-left: 0cm; line-height: normal; text-align: justify;"></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="color: black; font-size: 12pt; line-height: 18px;"><span style="font-family: Calibri;">La vida cotidiana y familiar ha variado sustancialmente con respecto a formas del pasado. Sin embargo, todavía la mujer lleva casi toda la carga del trabajo doméstico. Por ello,&nbsp;&nbsp;desde el IES se quiere trabajar la idea de que las labores domésticas es cosa de todos los miembros del hogar y que cada uno debe adquirir unas responsabilidades para hacer más fácil la convivencia.</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 12pt; line-height: 18px;"><span style="font-family: Calibri;"><span style="color: #000000;">Educar en el reparto de tareas es una formación necesaria que beneficia a los alumnos, no solo porque adquieren conocimientos que les son útiles, sino porque también les educa en la convivencia, en el respeto y la consideración hacia el trabajo, ya sea propio o ajeno</span>.</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 0pt; margin-left: 0cm; line-height: normal; text-align: justify;"><span style="color: black; font-size: 12pt;"><span style="font-family: Calibri;">Los talleres versarán sobre tres temas principales:</span></span></div>
<ol style="margin-top: 0cm;">
<li style="color: black; line-height: normal; margin-top: 0cm; margin-right: 0cm; margin-bottom: 0pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 12pt;"><span style="font-family: Calibri;">Cuidado de los más pequeños</span></span></li>
<li style="color: black; line-height: normal; margin-top: 0cm; margin-right: 0cm; margin-bottom: 0pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 12pt;"><span style="font-family: Calibri;">Cuidado de la ropa (lavado, planchado, costura)</span></span></li>
<li style="color: black; line-height: normal; margin-top: 0cm; margin-right: 0cm; margin-bottom: 0pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 12pt;"><span style="font-family: Calibri;">Cocina (elaboración de menús, lista de compras, manipulación alimentos..)</span></span></li>
</ol>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; line-height: normal; text-align: justify;"><span style="color: black; font-size: 12pt;"><span style="font-family: Calibri;">Se realizarán los días 2 y 3 de marzo en horario de mañana. Todos los alumnos del centro participarán, debiendo cumplimentar un pasaporte conciliador que irán sellando conforme participen en cada uno de los talleres.</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; line-height: normal; text-align: justify;"><span style="color: black; font-size: 12pt;"><span style="font-family: Calibri;">Los monitores de los talleres serán padres y madres, así como profesores y profesoras del centro.</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; line-height: normal; text-align: justify;"><span style="color: black; font-size: 12pt;"><span style="font-family: Calibri;">No se pretende que los alumnos aprendan, en tan poco tiempo, a realizar las tareas domésticas. Se trata más bien de un acto simbólico con el que dar un mensaje que, evidentemente, son las familias las encargadas de afianzar.</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; font-size: 12pt;"><span style="line-height: 18px;">O</span><span style="line-height: 18px;"><span style="color: #000000;">tro objetivo es incorporar la realidad intercultural de nuestro centro como elemento enriquecedor. Así el </span></span><span style="color: #000000;">viernes&nbsp;&nbsp;4 de marzo&nbsp;&nbsp;se realizarán una serie de actividades relacionadas con este objetivo:</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;">
<ul>
<li><span style="font-family: Calibri; font-size: 16px; color: #000000;">&nbsp;Muestra de comidas del mundo, elaboradas por las familias de los alumnos.</span></li>
<li><span style="font-family: Calibri; font-size: 16px; color: #000000;">&nbsp;Recital de poesías del mundo,&nbsp;&nbsp;a cargo de alumnos de diferentes nacionalidades.</span></li>
<li><span style="font-family: Calibri; font-size: 16px; color: #000000;">&nbsp;Breves charlas sobre algún aspecto de su actividad profesional a cargo de "madres" de alumnos del centro.</span></li>
</ul>
</div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="color: #000000;"><b><span style="font-family: Calibri;">COLABORACIÓN DE LAS FAMILIAS.</span></b></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; color: #000000; font-size: 12pt;">La actividad es organizada por el IES pero, para poder llevarla a cabo, es imprescindible la colaboración de las familias de los alumnos del centro. Ésta puede consistir en:</span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;">
<ul>
<li><span style="font-family: Calibri; font-size: 16px; color: #000000;">intervenir como monitores en los talleres, para ello sería fantástico que acudieran "padres" o "abuelos", pero la realidad se impone y, por supuesto, también son bienvenidas&nbsp;&nbsp;madres y abuelas .</span></li>
<li><span style="font-family: Calibri; font-size: 16px; color: #000000;">contribuir a los talleres aportando el material necesario (que una vez concluido les será devuelto). Relación de material por talleres:</span></li>
</ul>
</div>
<br /> 
<table border="1" cellpadding="0" cellspacing="0" style="border-collapse: collapse; width: 603px;">
<tbody>
<tr>
<td valign="top" width="115" style="background-color: transparent; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 86.4pt; border: 1pt solid windowtext;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; color: #000000;">Cuidado de los más pequeños</span></div>
</td>
<td valign="top" width="115" style="background-color: transparent; border-bottom-color: windowtext; border-bottom-width: 1pt; border-bottom-style: solid; border-left-color: #f0f0f0; border-left-width: initial; border-left-style: initial; border-right-color: windowtext; border-right-width: 1pt; border-right-style: solid; border-top-color: windowtext; border-top-width: 1pt; border-top-style: solid; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 86.45pt;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; color: #000000;">Lavado&nbsp;&nbsp;ropa</span></div>
</td>
<td valign="top" width="115" style="background-color: transparent; border-bottom-color: windowtext; border-bottom-width: 1pt; border-bottom-style: solid; border-left-color: #f0f0f0; border-left-width: initial; border-left-style: initial; border-right-color: windowtext; border-right-width: 1pt; border-right-style: solid; border-top-color: windowtext; border-top-width: 1pt; border-top-style: solid; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 86.45pt;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; color: #000000;">Planchado ropa</span></div>
</td>
<td valign="top" width="115" style="background-color: transparent; border-bottom-color: windowtext; border-bottom-width: 1pt; border-bottom-style: solid; border-left-color: #f0f0f0; border-left-width: initial; border-left-style: initial; border-right-color: windowtext; border-right-width: 1pt; border-right-style: solid; border-top-color: windowtext; border-top-width: 1pt; border-top-style: solid; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 86.45pt;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; color: #000000;">Costura</span></div>
</td>
<td valign="top" width="142" style="background-color: transparent; border-bottom-color: windowtext; border-bottom-width: 1pt; border-bottom-style: solid; border-left-color: #f0f0f0; border-left-width: initial; border-left-style: initial; border-right-color: windowtext; border-right-width: 1pt; border-right-style: solid; border-top-color: windowtext; border-top-width: 1pt; border-top-style: solid; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 106.2pt;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; color: #000000;">Cocina</span></div>
</td>
</tr>
<tr>
<td valign="top" width="115" style="background-color: transparent; border-bottom-color: windowtext; border-bottom-width: 1pt; border-bottom-style: solid; border-left-color: windowtext; border-left-width: 1pt; border-left-style: solid; border-right-color: windowtext; border-right-width: 1pt; border-right-style: solid; border-top-color: #f0f0f0; border-top-width: initial; border-top-style: initial; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 86.4pt;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Bebés de juguete</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Biberones para los bebés</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Chupetes</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Pañales</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Toquillas</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Cunitas</span></span></div>
</td>
<td valign="top" width="115" style="background-color: transparent; border-bottom-color: windowtext; border-bottom-width: 1pt; border-bottom-style: solid; border-left-color: #f0f0f0; border-left-width: initial; border-left-style: initial; border-right-color: windowtext; border-right-width: 1pt; border-right-style: solid; border-top-color: #f0f0f0; border-top-width: initial; border-top-style: initial; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 86.45pt;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Tablas de lavar</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Barreños</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Cuerdas</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Pinzas de ropa</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Ropa o trapos</span></span></div>
</td>
<td valign="top" width="115" style="background-color: transparent; border-bottom-color: windowtext; border-bottom-width: 1pt; border-bottom-style: solid; border-left-color: #f0f0f0; border-left-width: initial; border-left-style: initial; border-right-color: windowtext; border-right-width: 1pt; border-right-style: solid; border-top-color: #f0f0f0; border-top-width: initial; border-top-style: initial; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 86.45pt;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Planchas</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Tablas de planchar</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Perchas</span></span></div>
</td>
<td valign="top" width="115" style="background-color: transparent; border-bottom-color: windowtext; border-bottom-width: 1pt; border-bottom-style: solid; border-left-color: #f0f0f0; border-left-width: initial; border-left-style: initial; border-right-color: windowtext; border-right-width: 1pt; border-right-style: solid; border-top-color: #f0f0f0; border-top-width: initial; border-top-style: initial; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 86.45pt;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Agujas coser</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Hilos</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Telas o ropa para practicar</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Agujas hacer punto</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Lana</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">tijeras</span></span></div>
</td>
<td valign="top" width="142" style="background-color: transparent; border-bottom-color: windowtext; border-bottom-width: 1pt; border-bottom-style: solid; border-left-color: #f0f0f0; border-left-width: initial; border-left-style: initial; border-right-color: windowtext; border-right-width: 1pt; border-right-style: solid; border-top-color: #f0f0f0; border-top-width: initial; border-top-style: initial; padding-bottom: 0cm; padding-left: 5.4pt; padding-right: 5.4pt; padding-top: 0cm; width: 106.2pt;">
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Pequeño horno eléctrico</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Pequeño infiernillo eléctrico</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Sartenes</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Paletas</span></span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-size: 9pt; line-height: 13px; color: #000000;"><span style="font-family: Calibri;">Cuencos</span></span></div>
</td>
</tr>
</tbody>
</table>
<br />
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;">
<ul>
<li><span style="font-family: Calibri; font-size: 16px; color: #000000;">-aportando algún plato al menú intercultural del viernes. Preferentemente recetas de otros países. Si fuese necesario el centro está dispuesto a colaborar aportando el importe de los ingredientes para quién lo solicite.</span></li>
<li><span style="font-family: Calibri; font-size: 16px; color: #000000;">&nbsp;madres que se ofrezcan a dar una pequeña charla relacionada con su actividad profesional (10-15 minutos).</span></li>
</ul>
</div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; color: #000000; font-size: 12pt;">Para una mejor organización es necesario que nos enviéis un correo a&nbsp;<b><span id="cloak54108">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak54108').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy54108 = '&#97;mp&#97;&#105;&#101;sc&#101;rv&#97;nt&#101;s' + '&#64;';
 addy54108 = addy54108 + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 var addy_text54108 = '&#97;mp&#97;&#105;&#101;sc&#101;rv&#97;nt&#101;s' + '&#64;' + 'gm&#97;&#105;l' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak54108').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy54108 + '\'>'+addy_text54108+'<\/a>';
 //-->
 </script></b> indicándonos en que actividad podéis colaborar.</span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; color: #000000; font-size: 12pt;"> Los talleres se desarrollarán de 9 a 2 de la mañana. No es necesario que estéis todo ese tiempo, lo que si necesitamos es hacer un horario en el que sepamos exactamente quién puede venir y a qué horas. También necesitamos saber que material podéis aportar para los talleres y, por supuesto, que madres están dispuestas a dar una pequeña charla.</span></div>
<div style="margin-top: 0cm; margin-right: 0cm; margin-bottom: 10pt; margin-left: 0cm; text-align: justify;"><span style="font-family: Calibri; color: #000000; font-size: 12pt;">
<p style="margin-top: 0.5em; margin-right: 0px; margin-bottom: 0.5em; margin-left: 0px; font-style: normal; font-weight: normal; font-size: 12px; text-align: justify;"><a style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;" title="Download this file (1burka.pdf)" href="attachments/093_1burka.pdf" class="at_icon"><img style="vertical-align: text-bottom; margin: 5px;" src="components/com_attachments/media/icons/pdf.gif" /></a><a target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;" title="Download this file (1burka.pdf)" href="attachments/093_1burka.pdf" class="at_url">Burka: la cárcel de tela 1</a></p>
<p style="margin-top: 0.5em; margin-right: 0px; margin-bottom: 0.5em; margin-left: 0px; font-style: normal; font-weight: normal; font-size: 12px; text-align: justify;"><a style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;" title="Download this file (1burka.pdf)" href="attachments/093_1burka.pdf" class="at_url"></a><a style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;" title="Download this file (2burka.pdf)" href="attachments/093_2burka.pdf" class="at_icon"><img style="vertical-align: text-bottom; margin: 5px;" src="components/com_attachments/media/icons/pdf.gif" /></a><a target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;" title="Download this file (2burka.pdf)" href="attachments/093_2burka.pdf" class="at_url">Burka: la cárcel de tela 2</a></p>
<p style="margin-top: 0.5em; margin-right: 0px; margin-bottom: 0.5em; margin-left: 0px; font-style: normal; font-weight: normal; font-size: 12px; text-align: justify;"><a style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;" title="Download this file (2burka.pdf)" href="attachments/093_2burka.pdf" class="at_url"></a><a style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;" title="Download this file (4burka.jpg)" href="attachments/093_4burka.jpg" class="at_icon"><img style="vertical-align: text-bottom; margin: 5px;" src="components/com_attachments/media/icons/image.gif" /></a><a target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold;" title="Download this file (4burka.jpg)" href="attachments/093_4burka.jpg" class="at_url">Burka: la cárcel de tela: imágenes</a></p>
<p style="margin-top: 0.5em; margin-right: 0px; margin-bottom: 0.5em; margin-left: 0px; font-style: normal; font-weight: normal; font-size: 12px; text-align: justify;"><a style="font-family: Arial, Helvetica, sans-serif; text-decoration: none; color: #977702; font-weight: bold; padding-right: 4px;" title="Download this file (1colaboracion_familias.jpg)" href="attachments/093_4burka.jpg" class="at_icon"><img style="vertical-align: text-bottom; margin: 5px;" src="components/com_attachments/media/icons/image.gif" /></a><a target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;" title="Download this file (1colaboracion_familias.jpg)" href="images/jpg/1colaboracion_familias.jpg" class="at_url">Colaboración con las familias anverso</a></p>
<p style="margin-top: 0.5em; margin-right: 0px; margin-bottom: 0.5em; margin-left: 0px; font-style: normal; font-weight: normal; font-size: 12px; text-align: justify;"><img style="vertical-align: text-bottom; margin: 5px;" src="components/com_attachments/media/icons/image.gif" /> <a target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;" title="Download this file (2colaboracion_familias.jpg)" href="images/jpg/2colaboracion_familias.jpg" class="at_url">Colaboración con las familias reverso</a></p>
<br /></span></div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Actividades</span> / <span class="art-post-metadata-category-name">Coeducación</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:63:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Coeducación";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:12:"Coeducación";s:4:"link";s:59:"index.php?option=com_content&view=article&id=133&Itemid=188";}}s:6:"module";a:0:{}}
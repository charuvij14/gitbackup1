<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:24707:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">BACHILLERATO</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Selectividad 2014</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 20 Junio 2014 11:06</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=243:selectividad-2014&amp;catid=108&amp;Itemid=147&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=91e372d59cf69cff0547d4d91e51bb5d5635fb27" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 12311
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE 2014</span></h1>
<ul>
<li><strong style="color: #2a2a2a; font-size: 13px;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/06/14  (MARTES) POR LA TARDE EN <a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a; font-size: 13px;"> </span><strong style="color: #2a2a2a; font-size: 13px;">EXÁMENES QUE HAN SALIDO EN JUNIO 2014 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #2a2a2a; font-size: 13px; text-decoration: underline;">JUNIO 2014 - EXÁMENES</strong></span></p>
<p><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_criterios_correccion.php" target="_blank"><span style="text-decoration: underline;"><strong style="color: #2a2a2a; font-size: 13px; text-decoration: underline;">Criterios de correción oficiales. (19/06/14)</strong></span></a></p>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000; background-color: #e7eaad;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr align="center">
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Biología</strong></p>
</td>
<td style="text-align: center;" align="center" valign="middle" bgcolor="#FEFDD6"><strong><br />Ciencias de la Tierra y Medioambientales</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Comentario de Texto, Lengua Castellana y Literatura</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Artístico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Dibujo Técnico II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Economía de la Empresa</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Diseño</strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong> </strong></p>
<p><strong>Electrotecnia</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><span style="text-decoration: underline;"><a href="archivos_ies/13_14/selectividad/Biologia_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" /></a></span></strong><a href="archivos_ies/13_14/selectividad/Biologia_Colision_Junio2014.pdf" target="_blank" style="font-size: inherit;"><strong><span>Colisión</span></strong></a></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Biologia_AB.pdf" target="_blank"><strong><span>Crit. Correc.</span></strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" width="35" height="35" align="middle" /><br /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_CTM.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><br /> <a href="archivos_ies/13_14/selectividad/Lengua_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Lengua.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a><a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Colision_Junio2014.pdf" target="_blank"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Colisión</span></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_D_Artistico.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_D_Tecnico.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Economia.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Diseño_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank">Crit. Correc.</a><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank"></a></strong></p>
</td>
<td align="center">
<p style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank"><strong><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></strong><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Colisión</strong></a></p>
<p style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Electrotecnia.pdf" target="_blank"><strong style="text-align: center; color: inherit; font-family: inherit; font-size: inherit;">Crit. Correc.</strong></a></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;"><strong><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Diseño.pdf" target="_blank"><br /></a>Física</strong></td>
<td style="text-align: center;" align="center"><strong><br />Geografía</strong></td>
<td style="text-align: center;" align="center"><strong><br />Griego II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de España</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia de la Filosofía</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Historia del Arte</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong>Latín II </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><strong>Alemán</strong></p>
</td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><a href="archivos_ies/13_14/selectividad/Fisica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/FISICA_2014_Junio.pdf" target="_blank" style="font-size: inherit; font-weight: normal; text-decoration: none; color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluciones<br /></strong></a><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Fisica.pdf" target="_blank">Crit. Correc.</a></strong></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Geografia_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Geografia.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/Griego_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Griego_II.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> <a href="archivos_ies/13_14/selectividad/H_España_Junio2014.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_H_España.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Filosofia_AyB.pdf" target="_blank"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Filosofia.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong><strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/H_Arte_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_H_Arte.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong> </strong><strong><a href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_LatinAB.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td align="center">
<p><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="display: block; margin-left: auto; margin-right: auto;" /></a></strong></p>
<p><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_AlemanAB.pdf" target="_blank"><strong>Crit. Correc.</strong></a><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /></a></strong></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
<td align="center"><strong> </strong></td>
</tr>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Francés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Lengua Extranjera II (Inglés)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Literatura Universal</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong>Matemáticas Aplicadas a las Ciencias Sociales II</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Matemáticas II</strong></td>
<td align="center" bgcolor="#FEFDD6">
<p><strong><br /> Química </strong></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong> </strong><strong style="text-align: center;">Técnicas de Exp. Gráf. Plást. (no disponible)</strong></td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6"><strong><br />Tecnología Industrial II</strong></td>
</tr>
<tr>
<td style="text-align: center;">
<p><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Frances.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Ingles.pdf" target="_blank"><strong>Crit. Correc.</strong></a></p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Lit_Universal.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluciones</strong></a></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_MAT_CCSS_IIAB.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="text-align: center;">Crit. Correc.</strong></strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></a></strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;">Soluciones</a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Matematicas_II2.pdf" target="_blank"><span style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">Crit. Correc.</strong> </span></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></span></a></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Quimica.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Crit. Correc.</strong><strong> </strong></a></p>
</td>
<td style="text-align: center;" align="center">
<p><strong style="text-align: left;"><span style="color: #2f310d;"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" /></span></strong></p>
<p><a href="archivos_ies/13_14/selectividad/Criterios_correccion_Tec_Exp_Graf_Plastica.pdf" target="_blank"><strong style="text-align: left;"><span style="color: #2f310d;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">Crit. Correc.</strong></span></strong><strong> </strong></a></p>
</td>
<td align="center">
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /><img src="archivos_ies/13_14/selectividad/pdf.gif" border="0" align="middle" style="display: block; margin-left: auto; margin-right: auto;" /></a></span></p>
<p><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Criterios_correccion_T_IndustrialAB.pdf" target="_blank"><strong style="color: #000000; text-align: center;">Crit. Correc.</strong></a><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><br /></a></span><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<ul style="color: #2a2a2a; font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13px; line-height: normal;">
<li>
<h3><span style="text-align: center; color: #2a2a2a;">22 de abril a 4 de junio: registro para las PAU.</span></h3>
</li>
<li><span style="color: #7b7b7b; font-size: 20px; font-weight: bold; text-transform: uppercase;">La matrícula se realizará del 2 al 4 de Junio y del 3 al 5 de septiembre.</span></li>
</ul>
<ul>
<li>
<h3>Para conseguir el PIN (registro para las PAU) entrar en la siguiente web:</h3>
</li>
</ul>
<h3 style="text-align: center; background-color: #1188ff;"><strong><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank"><span style="color: #ffffff;">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</span></a></strong></h3>
<p> </p>
<ul style="font-size: 13px; text-align: justify; background-color: #e7eaad;">
<li>
<h4><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/documentacion/Parametros_AyB_UA_Bachillerato_2014_2015.pdf" target="_blank">PARÁMETROS DE PONDERACIÓN, Consulta estática.</a></h4>
<p><strong><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/g_b_parametros_top.php" target="_blank">CONSULTA DINÁMICA</a>.</strong></p>
</li>
<li>
<h4><a href="http://www.juntadeandalucia.es/economiainnovacionyciencia/sguit/" target="_blank">DISTRITO ÚNICO ANDALUZ.</a></h4>
</li>
</ul>
<ul>
<li>
<h4><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI encuentro de la universidad con los centros.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">Presentación: registro para la selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">Presentación: matrícula de selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank">Selectividad: orientaciones.<br /><br /></a></h4>
</li>
<li>
<h4><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Web de la Universidad para registro y matrícula.<br /><br /></a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Otros datos de interés: </a></h4>
</li>
</ul>
<ol><ol>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Inscripción</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Matriculación</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Precios</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Calendario de las pruebas día a día</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Nota de acceso</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Exámenes de otros años y orientaciones</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Orden de preferencia de carreras</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Procedimiento de matrícula en la UIniversidad</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
</ol></ol>
<h4> </h4>
<p><span style="color: #2a2a2a;"> </span></p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">BACHILLERATO</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:68:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Selectividad 2014";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:3:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Materiales";s:4:"link";s:72:"index.php?option=com_content&view=category&layout=blog&id=223&Itemid=145";}i:1;O:8:"stdClass":2:{s:4:"name";s:12:"BACHILLERATO";s:4:"link";s:60:"index.php?option=com_content&view=category&id=108&Itemid=147";}i:2;O:8:"stdClass":2:{s:4:"name";s:17:"Selectividad 2014";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
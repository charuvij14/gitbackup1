<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:15366:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Prueba de acceso</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:13</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=211:prueba-de-acceso&amp;catid=242&amp;Itemid=617&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=2a92dc263128a8db6e3c8385f832f846511e3822" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2417
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Prueba de Acceso a Ciclos Formativos</h2>
<h3 class="plg_fa_karmany"><span style="color: #003366;"><strong>Información General</strong></span></h3>
<div id="articlepxfontsize1">
<div style="text-align: justify;"><strong>LUGAR Y FECHAS DE INSCRIPCIÓN</strong></div>
<div style="text-align: justify;">Cada año, y con antelación suficiente, la Consejería de Educación determinará los Centros en los que se realizarán dichas Pruebas. Las fechas de inscripción serán las siguientes:
<ul class="list-icon icon-calendar">
<li>Para la Convocatoria Ordinaria: La Primera Quincena de Mayo.</li>
<li>Para la Convocatoria Extraordinaria: La Segunda Quincena de Julio.</li>
</ul>
<strong>LUGAR Y FECHAS DE REALIZACIÓN</strong>
<div style="text-align: justify;">Las Pruebas se realizarán en el Centro en el que se presentó la solicitud. Las fechas de realización son las siguientes:
<ul class="list-icon icon-calendar">
<li>Convocatoria Ordinaria: el 5 de Junio de cada año (si fuera sábado o festivo, el día hábil siguiente).</li>
<li>Convocatoria Extraordinaria: el 7 de Septiembre de cada año (si este fuera domingo sería el 5 de Septiembre y si fuera sábado o festivo, el siguiente día hábil).</li>
</ul>
</div>
<div style="text-align: justify;"><strong>EFECTOS DE HABER SUPERADO LA PRUEBA </strong></div>
<div style="text-align: justify;"><ol>
<li>La superación de la Prueba de Acceso a la Formación Profesional Específica faculta a las personas que no poseen requisitos académicos para cursar las enseñanzas correspondientes a los Ciclos Formativos de F.P.</li>
<li>La superación de la citada Prueba no supone la posesión de ninguna titulación académica ni tampoco equivale a haber superado alguna área de la ESO o alguna materia de Bachillerato.</li>
<li>La superación de la Prueba no garantiza ni proporciona el derecho a una puesto escolar. Para ser admitidos/as en un Ciclo Formativo, es requisito indispensable presentar solicitud de admisión en el Centro donde se desee cursar estudios, del 1 al 25 de junio o del 1 al 10 de septiembre de cada año, y una vez superada la prueba.</li>
<li>Una vez superada la Prueba, sus efectos tendrán validez en todo el territorio español.</li>
</ol></div>
<div style="text-align: justify;"><strong>RECLAMACIONES </strong></div>
<ul class="list-arrow arrow-blue">
<li><strong>En Primera Instancia</strong><br />Los/as interesados/as o su padres, si son menores de edad, si no estuvieran conformes con la Calificación obtenida en la Prueba, podrán presentar ante la Comisión de valoración la correspondiente reclamación, en el plazo de 2 días hábiles desde la publicación de las actas. En el plazo máximo de 2 días hábiles, la Comisión deberá resolver todas las reclamaciones presentadas.</li>
<li><strong>En Segunda Instancia</strong><br />En caso de que los/as interesados/as o su padres, si son menores de edad, no estuvieran conformes con la resolución de la Comisión, podrán presentar recurso de alzada ante la persona titular de la Delegación Provincial de Educación en el plazo de un mes, de conformidad con lo establecido en los artículos 114 y 115 de la Ley 30/1992, de 26 de noviembre.</li>
</ul>
</div>
<h3 class="text-tip"><span style="color: #800000;"><strong>Pruebas de Acceso de Grado Medio</strong></span></h3>
<div style="text-align: justify;"><strong>NORMATIVA</strong></div>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/boja/2008/90/d2.pdf" target="_blank">Orden de 23 de Abril de 2008 (BOJA nº 90 de 07-05-2008)</a></li>
</ul>
<div style="text-align: justify;"><strong>DOCUMENTOS </strong></div>
<ul class="list-icon icon-download">
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1640-requisitos-y-estructura-grado-medio" target="_blank">Requisitos y estructura</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1644-contenidos-y-criterios-de-evaluacion-grado-medio" target="_blank">Temario de contenidos (Anexo III)</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1642-exenciones-grado-medio" target="_blank">Exenciones de materias ESO (Anexo IV)</a></li>
</ul>
<div style="text-align: justify;"><br /><strong>SOLICITANTES</strong></div>
<div style="text-align: justify;">Las personas que deseen realizar la Prueba de Acceso a un Ciclo Formativo de Grado Medio, deben tener <strong>cumplidos 17 años</strong>, o cumplirlos en el año natural de celebración de la Prueba. Para ello deberán presentar la Solicitud de Inscripción según el Modelo Anexo I, en el Centro en el que se pretenda realizar la Prueba.</div>
<div style="text-align: justify;"><br /><strong>ESTRUCTURA Y CONTENIDOS DE LA PRUEBA</strong></div>
<div style="text-align: justify;">Las Pruebas se estructurarán en tres partes:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-alpha;">
<li>Comunicación</li>
<li>Social</li>
<li>Científico-tecnológica</li>
</ol></div>
<div style="text-align: justify;">Los contenidos aparecen recogidos en el Anexo III.</div>
<div style="text-align: justify;">Para la homogeneizar criterios, la Dirección General facilitará a la Comisión de las Pruebas de Acceso la Prueba ya elaborada, así como los criterios de corrección.</div>
<div style="text-align: justify;"><br /><strong>EXENCIONES DE REALIZACIÓN</strong></div>
<div style="text-align: justify;">Estarán <strong>exentas de realizar la totalidad de la Prueba de Acceso </strong>aquellas personas que reúnan alguno de los siguientes requisitos:<ol style="list-style-type: lower-roman;">
<li>Tener superada la Prueba de Acceso a la Universidad para Mayores de 25 años.</li>
<li>Tener superada la Prueba de Acceso a un Grado Superior.</li>
</ol></div>
<h3 class="text-comment"><span style="color: #9b2626;"><strong>Pruebas de Acceso de Grado Superior</strong></span></h3>
<strong>NORMATIVA</strong>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/boja/2008/90/d2.pdf" target="_blank">Orden de 23 de Abril de 2008 (BOJA nº 90 de 07-05-2008)</a></li>
</ul>
<strong>DOCUMENTOS DESCARGABLES</strong>
<ul class="list-icon icon-download">
<li><a href="archivos_ies/anexo_ii_solicitud_gs_rellenable.pdf" target="_blank">Inscripción Prueba de Acceso (anexo II)</a></li>
<li><a href="archivos_ies/temario-parte-comun-anexo-v.pdf" target="_blank">Temario parte común (anexo V)</a></li>
<li><a href="archivos_ies/GS%20ANEXO%20VI.pdf" target="_blank">Opciones (anexo VI)</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1645-contenidos-y-criterios-de-evaluacion-grado-superior" target="_blank"><strong>CONTENIDOS Y CRITERIOS DE EVALUACIÓN</strong></a></li>
<li><a href="archivos_ies/EXENCIONES%20GS%20completo%20BUENO.pdf" target="_blank">Exención de materias de Bachillerato (anexo VIII)</a></li>
</ul>
<strong>SOLICITANTES</strong>
<div style="text-align: justify;">Las personas que deseen realizar la Prueba de Acceso a un Ciclo Formativo de Grado Superior, deberán cumplir alguno de los siguientes requisitos:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li>Tener<strong> cumplidos 19 años</strong>, o cumplirlos a lo largo del año natural de celebración de la Prueba.</li>
<li>Tener <strong>cumplidos 18 años</strong>, o cumplirlos a lo largo del año natural de celebración de la Prueba, y estar en posesión de un Título de Grado Medio relacionado con aquel de Grado Superior al que se desea acceder.</li>
</ol></div>
<div style="text-align: justify;">Para ello deberán presentar la Solicitud de Inscripción según el Modelo Anexo II, en el Centro en el que se pretenda realizar la Prueba.<br /><br /></div>
<div style="text-align: justify;"><strong>ESTRUCTURA Y CONTENIDOS DE LA PRUEBA</strong></div>
<div style="text-align: justify;">La Prueba contará de dos partes:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-alpha;">
<li>Parte Común.</li>
<li>Parte Específica.</li>
</ol></div>
<div style="text-align: justify;">a)<strong> La Parte Común</strong>, constará de 3 ejercicios:</div>
<div style="text-align: justify;"><ol style="list-style-type: upper-roman;">
<li>Lengua Española</li>
<li>Matemáticas</li>
<li>Lengua Extranjera</li>
</ol></div>
<div style="text-align: justify;">Esta parte se considerará superada cuando habiendo obtenido al menos 3 puntos en cada ejercicio, la media aritmética sea igual o superior a 5 puntos. Los contenidos de esta parte aparecen recogidos en el Anexo V.</div>
<div style="text-align: justify;"><br />b) En la <strong>Parte Específica</strong>, se ofertarán 3 materias distintas, según el Ciclo al que se pretende acceder. Estas materias vienen recogidas en el Anexo VI, debiendo el/la alumno/a elegir 2.</div>
<div style="text-align: justify;"><br />Por ejemplo, para Administración y Finanzas (Opción A), las materias ofertadas son:</div>
<div style="text-align: justify;"><ol style="list-style-type: upper-roman;">
<li>Economía de la Empresa</li>
<li>Geografía</li>
<li>Segunda Lengua Extranjera</li>
</ol></div>
<div style="text-align: justify;">Los contenidos de esta parte aparecen recogidos en el Anexo VII.</div>
<div style="text-align: justify;">Esta parte se considerará superada cuando habiendo obtenido al menos 3 puntos en cada ejercicio, la media aritmética sea igual o superior a 5 puntos.</div>
<div style="text-align: justify;">Para la homogeneizar criterios, la Dirección General facilitará a la Comisión de las Pruebas de Acceso la Prueba ya elaborada, así como los criterios de corrección.</div>
<div style="text-align: justify;"><br /><strong>EXENCIONES DE REALIZACIÓN</strong></div>
<div style="text-align: justify;"><strong>Exención Total</strong>: Estarán exentas de realizar la totalidad de la Prueba de Acceso aquellas personas que reúnan el siguiente requisito:</div>
<div style="text-align: justify;">
<ul style="list-style-type: disc;">
<li>Tener superada la Prueba de Acceso a la Universidad para Mayores de 25 años.</li>
</ul>
</div>
<div style="text-align: justify;"><strong>Exención Parcial</strong>: Estarán exentas de realizar alguna de las partes de la Prueba de Acceso aquellas personas que reúnan alguno de los siguientes requisitos:</div>
<div style="text-align: justify;"><br />DE LA PARTE ESPECÍFICA</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li>Estar en posesión del Título de Técnico relacionado con aquel Ciclo al que se pretende acceder.</li>
<li>Estar en posesión de un certificado de profesionalidad de alguna de las familias profesionales incluidas en la opción por la que se presenta, de un nivel competencial 2 ó superior.</li>
<li>Acreditar una experiencia laboral de al menos un año con jornada completa en el campo profesional correspondiente a la familia de la opción por la que se presente, que se deberá justificar de la siguiente forma:</li>
</ol></div>
<div style="text-align: justify; margin-left: 30px;">
<ul style="list-style-type: circle;">
<li>Trabajadores por cuenta ajena: Mediante Certificación de la Tesorería General de la Seguridad Social.</li>
<li>Trabajadores pro cuenta propia: Mediante Certificación del periodo de cotización en régimen de autónomos.</li>
<li>Tener aprobadas las materias de Bachillerato de las que conste la parte específica.</li>
</ul>
</div>
<div style="text-align: justify;">DE LA PARTE COMÚN</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li>Tener aprobadas las materias de Bachillerato que constituyen la parte Común de la Prueba.</li>
<li>Tener superada la Prueba de Acceso a otro Ciclo Formativo de Grado Superior.</li>
</ol></div>
<div style="text-align: justify;"><strong>Nota:</strong> Las exenciones de los apartados anteriores podrán ser acumulables.<br /><br /></div>
<h3 class="text-alert"><span style="color: #800000;"><strong>Evaluación y Calificación de las pruebas</strong></span></h3>
La calificación de cada una de las partes de la Prueba será numérica, entre 0 y 10. <br />
<ul style="list-style-type: circle;">
<li>La nota final de la Prueba se calculará siempre que se obtenga al menos una puntuación de 4 en cada una de las partes, y será la media aritmética de éstas, expresada con 2 decimales.</li>
<li>Para aquellos/as que hayan realizado y superado el Curso de Preparación de la Prueba de Acceso, a la nota final obtenida en la Prueba de Acceso se le sumará el resultado de multiplicar la nota obtenida en el Curso de Preparación multiplicada por el coeficiente 0,15. </li>
<li>Se considera superada la Prueba de Acceso cuando la nota final sea igual o superior a 5 puntos, siendo la calificación máxima de 10 puntos.</li>
<li>A efectos de cálculo de la nota final de la Prueba, no se tendrán en cuenta las partes de la Prueba exentas, haciéndolo constar así en las actas de evaluación mediante la expresión EX en la casilla correspondiente.</li>
<li>El acta de la convocatoria ordinaria se deberá exponer en el Tablón de Anuncios a partir del 2º día hábil, contado desde el de celebración de la Prueba.</li>
<li>El acta de la convocatoria extraordinaria se deberá exponer en el Tablón de Anuncios el día siguiente de celebración de la Prueba.</li>
</ul>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:67:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Prueba de acceso";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:17:"Ciclos formativos";s:4:"link";s:59:"index.php?option=com_content&view=article&id=208&Itemid=615";}i:1;O:8:"stdClass":2:{s:4:"name";s:16:"Prueba de acceso";s:4:"link";s:59:"index.php?option=com_content&view=article&id=211&Itemid=617";}}s:6:"module";a:0:{}}
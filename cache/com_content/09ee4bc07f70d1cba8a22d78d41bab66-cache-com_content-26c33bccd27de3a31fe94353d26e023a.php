<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5738:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">2º ESO BILINGÜE2012-13</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 29 Septiembre 2013 19:16</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=214:2o-eso-bilinguee-2&amp;catid=144&amp;Itemid=617&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=e9b7b6f4c023119eafef06bdca4878314faeb226" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 8106
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p style="font-size: 11.818181991577148px;"><span style="text-decoration: underline;"><strong style="color: #000000; font-size: 19.09090805053711px;">APUNTES DE EDUCACIÓN FÍSICA 2º ESO BILINGÜE.</strong></span></p>
<p style="font-size: 11.818181991577148px;"><span style="text-decoration: underline;"><strong>PROFESORA:</strong></span><strong> Dª Nuria Manrique Morá.</strong></p>
<p style="font-size: 11.818181991577148px;"> </p>
<ul>
<li><a href="archivos_ies/bilingues/2eso/What_equipment_is_necessary.Vocabulary.pdf" target="_blank">1.- What equipment is necessary. Vocabulary</a></li>
<li><a href="archivos_ies/bilingues/2eso/Warm-up.pdf" target="_blank">2.- Warm-up</a></li>
<li><a href="archivos_ies/bilingues/2eso/PREVENTING_INJURIES.pdf" target="_blank">3.- Preventing injuries.</a> (27/09/12)</li>
<li><a href="archivos_ies/bilingues/2eso/HEALTH_AND_HYGIENE.pdf" target="_blank">4.- HEALTH AND HYGIENE.</a> (14/10/12)</li>
</ul>
<p> </p>
<hr />
<p><span style="text-decoration: underline;"><strong style="color: #000000; font-size: 19.09090805053711px;">APUNTES DE MATEMÁTICAS 2º ESO BILINGÜE.</strong></span></p>
<p><span style="text-decoration: underline;"><strong>PROFESORA:</strong></span><strong> Dª Clotilde García Sánchez.</strong></p>
<ul>
<li><a href="archivos_ies/bilingues/2eso/mat2extraactchap1.pdf" target="_blank">1.- CHAPTER 1: INTEGERS.</a> (08/10/12)</li>
<li><a href="archivos_ies/bilingues/2eso/integers_and_science.pdf" target="_blank">2.- Integers and science.</a> (08/10/12)</li>
<li><a href="archivos_ies/bilingues/2eso/PROBLEMS_WITH_FRACTIONS.pdf" target="_blank">3.- PROBLEMS WITH FRACTIONS.</a> (07/11/12)</li>
<li><a href="archivos_ies/bilingues/2eso/Decimals_fractions_and_percentages.pdf" target="_blank">4.- Decimals, fractions and percentages.</a> (07/11/12)</li>
<li><a href="archivos_ies/bilingues/2eso/Exercises_Decimals_fractions_and_percentages.pdf" target="_blank">5.- Exercises, Decimals, fractions and percentages.</a> (07/11/12)</li>
<li><a href="archivos_ies/bilingues/2eso/Actividad_Mixed_fraction(soluciones).pdf" target="_blank">6.- Actividad: Mixed fraction (soluciones).</a> (08/11/12)</li>
<li><strong><a href="archivos_ies/bilingues/2eso/-Conversiones_Decimals_fractions_percentages.pdf" target="_blank">7.- Conversiones Decimals, fractions, percentages.</a> (23/11/12)<br /></strong></li>
<li><strong><a href="archivos_ies/bilingues/2eso/Actividad_Fractions_using_reciprocal_soluciones.pdf" target="_blank">8.- Actividad: Fractions using reciprocal (soluciones)</a> (26/11/12)</strong></li>
<li><a href="archivos_ies/bilingues/2eso/-Solutions_Convert_Decimals_fractions_percentages.pdf" target="_blank"><span style="color: #0066cc;">9.- Solutions. Conver _Decimals fractions, percentages.</span></a> (04/12/12)</li>
<li><strong><a href="archivos_ies/Sexagesimal_system.pdf" target="_blank"><strong><span style="color: #0000ff;">10.- Sexagesimal system.</span></strong></a> (19/12/12)</strong></li>
<li><strong><a href="archivos_ies/bilingues/2eso/Vocabulary_Algebra.pdf" target="_blank"><strong><span style="color: #0000ff;">11.- Vocabulary Algebra.</span></strong></a> (19/01/13)</strong></li>
<li><a href="archivos_ies/bilingues/2eso/OLIMPIADA_MATEMATICA1.pdf" target="_blank"><strong>12.- OLIMPIADA MATEMATICA. (28/01/13)</strong></a></li>
<li><a href="archivos_ies/bilingues/2eso/soluciones_libroblanco_polinomios.pdf" target="_blank">13.- Soluciones libro blanco polinomios.</a> (28/01/13)</li>
<li><strong><a href="archivos_ies/bilingues/2eso/worsheet_Origins_of_Algebra_Solution.pdf" target="_blank">14.- Worsheet Origins of Algebra Solution.</a> (08/02/13)</strong></li>
<li><strong><a href="archivos_ies/bilingues/2eso/Vocabulary_eqation.pdf" target="_blank">15.- Vocabulary equation.</a> (08/02/13)<br /></strong></li>
<li><strong><a href="archivos_ies/bilingues/2eso/soluciones_libro_blanco2.docx" target="_blank">16.- Soluciones libro_blanco.</a> (08/02/13)<br /></strong></li>
</ul>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;"> </p>
<hr /></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:75:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - 2º ESO BILINGÜE2012-13";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:5:{i:0;O:8:"stdClass":2:{s:4:"name";s:17:"Ciclos formativos";s:4:"link";s:59:"index.php?option=com_content&view=article&id=208&Itemid=615";}i:1;O:8:"stdClass":2:{s:4:"name";s:16:"Prueba de acceso";s:4:"link";s:59:"index.php?option=com_content&view=article&id=211&Itemid=617";}i:2;O:8:"stdClass":2:{s:4:"name";s:8:"ALUMNADO";s:4:"link";s:60:"index.php?option=com_content&view=category&id=222&Itemid=617";}i:3;O:8:"stdClass":2:{s:4:"name";s:24:"Grupos del curso 2012/13";s:4:"link";s:60:"index.php?option=com_content&view=category&id=144&Itemid=617";}i:4;O:8:"stdClass":2:{s:4:"name";s:24:"2º ESO BILINGÜE2012-13";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
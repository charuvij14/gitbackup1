<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:20368:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Artículos</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">ÁREA T.I.C.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 09 Septiembre 2014 14:41</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=188:area-t-i-c&amp;catid=40&amp;Itemid=631&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=9a267c24f5ecbb277f1362079bab8efcf33f0c3d" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 32095
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 class="colorProtege" style="margin-top: 0px; margin-bottom: 0.5em; font-size: 20px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-align: justify; color: #116090;"><span style="text-decoration: underline;">NOTICIAS SOBRE LAS TIC. SEGURIDAD, VIRUS, CONSEJOS...</span></h1>
<p>&nbsp;</p>
<h3 class="colorProtege" style="margin-top: 0px; margin-bottom: 0.5em; font-size: 15px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; line-height: 19px; text-align: justify; text-transform: none; color: #116090 !important;">¿Qué puedo hacer para mantener seguros los dispositivos móviles y fijos?</h3>
<p style="margin: 1em 0px; color: #000000; font-size: 13px; line-height: 19px; text-align: justify;">Para evitar sustos innecesarios y estar protegido ante posibles amenazas, es necesario que sigas unas recomendaciones básicas para proteger tus dispositivos.</p>
<p style="margin: 1em 0px; color: #000000; font-size: 13px; line-height: 19px; text-align: justify;">Desde la OSI (Oficina de Seguridad del Internauta del Ministerio de Industria) te enseñan cómo hacerlo. Lee las recomendaciones sobre el correcto uso de los ordenadores portátiles y los teléfonos móviles:</p>
<ul class="listaEnlacesProtege" style="margin-top: 0px; margin-bottom: 1.5em; list-style-type: disc; color: #000000; font-size: 13px; line-height: 19px; text-align: justify;">
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/protegete/protege-tu-movil-y-portatil/portatiles" target="_blank" style="color: #14618f;">Portátiles</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/protegete/protege-tu-movil-y-portatil/moviles" target="_blank" style="color: #14618f;">Móviles</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/es/protegete/protege-tu-ordenador" target="_blank" style="color: #14618f;">Ordenador</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://menores.osi.es/" target="_blank"></a><a href="http://menores.osi.es/" target="_blank" style="color: #14618f;">Protege a menores</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/es/protegete/protegete-en-internet" target="_blank" style="color: #14618f;">Protégete en Internet</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/es/protegete/consejos" target="_blank" style="color: #14618f;">Guía rápida de consejos</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/es/te-ayudamos/desinfecta-tu-ordenador" target="_blank" style="color: #14618f;">Desinfecta tu ordenador</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/recursos/conan" target="_blank" style="color: #14618f;">CONAN: asegura tu PC</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/es/recursos/comprueba-ordenador-actualizado" target="_blank" style="color: #14618f;">OSI: actualiza tu PC</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/es/recursos/utiles-gratuitos" target="_blank" style="color: #14618f;">Útiles gratuitos</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/es/recursos/paginas-evangelizadoras" target="_blank" style="color: #14618f;">Uso seguro y responsable de Internet</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/es/actualidad/blog" target="_blank" style="color: #14618f;">Blog de la OSI&nbsp;</a></li>
<li style="margin-left: 0px; padding-top: 0.1em; padding-bottom: 0.2em; padding-left: 2em; list-style: none; background-image: url('http://www.osi.es/sites/all/themes/fusion/theme_custom/images/flecha_azul.gif'); background-attachment: scroll !important; background-color: transparent !important; background-position-x: 0px !important;"><a href="http://www.osi.es/es/actualidad/avisos" target="_blank" style="color: #14618f;">ALERTAS Y AVISOS&nbsp;</a></li>
</ul>
<hr />
<h1 class="title" style="margin-top: 0px; margin-bottom: 0.5em; font-size: 23px; font-weight: normal; line-height: 1.1em; color: #450367; text-decoration: underline; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; text-transform: none;">Criptolocker: virus que cifra los ficheros del ordenador</h1>
<div class="field field-type-text field-field-descripcion" style="margin: 0px; padding: 0px; color: #000000; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; line-height: 19px;">
<div class="field-items" style="margin: 0px; padding: 0px;">
<div class="field-item" style="margin: 0px; padding: 0px;">
<p style="margin: 0px 0px 1em;">Se ha detectado un nuevo virus conocido como Cryptolocker que es capaz de cifrar ficheros de un ordenador infectado por éste, para a continuación, solicitar al usuario una cantidad económica a cambio de la “supuesta” clave privada para descifrar dichos ficheros.</p>
<p style="margin: 0px 0px 1em;">Cryptolocker consigue colarse en los ordenadores de los usuarios a través de enlaces o ficheros adjuntos en redes sociales, mensajería instantánea o email. Intenta engañar a las víctimas simulando ser un mensaje importante de un contacto confiable. Los expertos en seguridad lo etiquetan como un&nbsp;<em>ransomware</em>&nbsp;por el «secuestro» de ficheros.</p>
</div>
</div>
</div>
<p>&nbsp;</p>
<div class="field field-type-text field-field-affected-resources" style="margin: 0px; padding: 0px; color: #000000; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; line-height: 19px;">
<h3 class="field-label" style="margin-top: 0px; margin-bottom: 0.5em; font-size: 15px;">Recursos afectados</h3>
<div class="field-items" style="margin: 0px; padding: 0px;">
<div class="field-item" style="margin: 0px; padding: 0px;">
<p style="margin: 0px 0px 1em;">El virus afecta a ordenadores con&nbsp;<acronym style="color: #008800; border-width: 0px 0px 1px; border-bottom-style: dotted; cursor: help;" title="Un sistema operativo (SO) es un conjunto de programas destinados a permitir la comunicación del usuario con un dispositivo electrónico. Se encuentra instalado en la gran mayoría de los aparatos electrónicos que se utilizan en nuestra vida diaria (teléfonos móviles, videoconsolas, ordenadores).">sistema operativo</acronym>&nbsp;Windows.</p>
</div>
</div>
</div>
<p><a href="http://www.osi.es/es/actualidad/avisos/2013/09/criptolocker-virus-que-cifra-los-ficheros-del-ordenador?origen=boletin" target="_blank"><span style="color: #5f5649; font-size: 20px; font-weight: bold; text-transform: uppercase; line-height: 19px;">VER Solución</span></a></p>
<hr />
<h5 class="title" style="margin-top: 0px; margin-bottom: 0.5em; padding-left: 10px; font-size: 17px; color: #000000; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; line-height: 19px; text-transform: none;"><a href="http://www.osi.es/es/actualidad/avisos/2013/09/hesperbot-troyano-que-afecta-usuarios-de-banca-electronica-en-europa-y-tur" style="text-decoration: underline; color: #12608f;" title="Hesperbot: Troyano que afecta a usuarios de banca electrónica en Europa y Turquía">Hesperbot: Troyano que afecta a usuarios de banca electrónica en Europa y Turquía</a></h5>
<div class="descript" style="margin: 0px; padding: 0px;">El laboratorio de investigación y análisis de malware de ESET ha descubierto un nuevo y peligroso troyano bancario, Win32/Spy.Hesperbot, con funcionalidad similar y objetivos idénticos a los conocidos Zeus o SpyEye. De momento ha afectado a usuarios de banca electrónica en República Checa, Portugal, Reino Unido y Turquía, aunque según<span class="1" style="margin: 0px; padding: 0px;">...</span><br /><br /><a href="http://www.osi.es/es/actualidad/avisos/2013/09/hesperbot-troyano-que-afecta-usuarios-de-banca-electronica-en-europa-y-tur" class="readm" style="text-decoration: none; color: #12608f !important;" title="Leer más">Leer más</a></div>
<div class="descript" style="margin: 0px; padding: 0px;"><hr /></div>
<h2 class="contentheading">Consejos TICs</h2>
<p>Algunos consejos a tener en cuenta por los miembros de la comunidad educativa. Son sólo una opinión personal . Si alguién está en desacuerdo o bien quiere aportar alguno más, por favor use la zona de comentarios.</p>
<blockquote>
<ul style="list-style-type: disc;">
<li><span><strong><em>Controle el uso de la red a menores. No les deje navegar solos.</em></strong></span></li>
<li><span><strong><em>Utilice filtros de contenidos. La Junta de Andalucia ofrece filtros gratuitos.</em></strong></span></li>
<li><span><strong><em>Mantenga en lo posible el anonimato y cuidado con los desconocidos.</em></strong></span></li>
<li><span><strong><em>Use passwords complicados.</em></strong></span></li>
<li><span><strong><em>Vigile los posibles intentos de estafa.</em></strong></span></li>
<li><span><strong><em>Cuidado con las descargas.</em></strong></span></li>
<li><span><strong><em>No toda la información que encuentra en la red es fiable.</em></strong></span></li>
<li><span><strong><em>Actualice el antivirus y active el cortafuegos.</em></strong></span></li>
<li><span><strong><em>Y recuerde que las TICs son una herramienta que nos facilitan nuestro trabajo para ganar tiempo y poder estar más con nuestra familia, amigos etc...</em></strong></span></li>
</ul>
</blockquote>
<table style="width: 100%;">
<tbody>
<tr>
<td width="4%" height="15">&nbsp;</td>
<td width="96%">&nbsp;</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<table style="width: 95%;">
<tbody>
<tr>
<td align="center" width="13%"><img src="http://www.inteco.es/extfrontinteco/img/Image/intecocert/boletines/icono_nota.png" alt=" " border="0" /></td>
<td width="87%"><a href="http://www.osi.es/es/actualidad/blog/2013/02/05/conectate-y-respeta--netiquetate" target="_blank">Conéctate y respeta - netiquétate</a> <br />Hoy, 5 de febrero, se celebra el Día Internacional de la Internet Segura. Nos unimos a esta celebración publicando unos consejos sobre la netiqueta.</td>
</tr>
</tbody>
</table>
<table style="width: 100%;">
<tbody>
<tr>
<td width="4%" height="15"><img src="http://www.inteco.es/extfrontinteco/img/Image/intecocert/boletinesosi/faviconosi.png" alt="" border="0" /></td>
<td width="96%">Noticias de seguridad</td>
</tr>
</tbody>
</table>
<ul>
<li>&nbsp;<a href="http://www.zonavirus.com/noticias/2013/nueva-oleada-de-mails-maliciosos-con-falso-remitente-policiagobiernoes-anexando-malware.asp" target="_blank">Nueva oleada de mails maliciosos con falso remitente <span id="cloak55507">Esta dirección de correo electrónico está protegida contra spambots. Usted necesita tener Javascript activado para poder verla.</span><script type='text/javascript'>
 //<!--
 document.getElementById('cloak55507').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy55507 = 'p&#111;l&#105;c&#105;&#97;' + '&#64;';
 addy55507 = addy55507 + 'g&#111;b&#105;&#101;rn&#111;' + '&#46;' + '&#101;s';
 document.getElementById('cloak55507').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy55507 + '\'>' + addy55507+'<\/a>';
 //-->
 </script>, anexando malware</a> <span>(Fuente: Zonavirus)</span></li>
<li><span><a href="http://www.genbeta.com/redes-sociales/los-datos-de-250-000-usuarios-de-twitter-comprometidos" target="_blank">Los datos de 250.000 usuarios de Twitter comprometidos</a> <span>(Fuente: Genbeta)</span></span></li>
<li><span><span><a href="http://blogs.eset-la.com/laboratorio/2013/02/01/detectando-aplicaciones-maliciosas-facebook/" target="_blank">Detectando aplicaciones maliciosas en Facebook</a> <span>(Fuente: <a href="http://eset-la.com/" target="_blank">eset-la.com</a>)</span></span></span></li>
<li><span><span><span><a href="http://terminosycondiciones.es/2013/02/01/gossip-13-detalles-sobre-sus-terminos-y-condiciones/" target="_blank">Gossip - 13 detalles sobre sus términos y condiciones</a> <span>(Fuente: Términos y condiciones)</span></span></span></span></li>
<li><span><span><span><span><a href="http://www.genbeta.com/navegadores/firefox-busca-mayor-estabilidad-y-java-silverlight-y-flash-seran-bloqueados" target="_blank">Firefox busca mayor estabilidad y Java, Silverlight y Flash serán bloqueados</a> <span>(Fuente: Genbeta)</span></span></span></span></span></li>
<li><span><span><span><span><span><a href="http://bitelia.com/2013/01/monitorizando-tu-red-wifi-en-busca-de-intrusos" target="_blank">Monitoriza tu red Wi-Fi en busca de intrusos</a> <span>(Fuente: Bitelia)</span></span></span></span></span></span></li>
<li><span><span><span><span><span><span><a href="http://www.securitybydefault.com/2013/01/cualquiera-puede-ver-tu-foto-de-perfil.html" target="_blank">Cualquiera puede ver tu foto de perfil en Whatsapp</a> <span>(Fuente: Security By Default)</span></span></span></span></span></span></span></li>
<li><span><span><span><span><span><span><span><a href="http://www.antena3.com/noticias/tecnologia/cientos-webcams-personales-emiten-internet-que-sus-duenos-sepan_2013012900257.html" target="_blank">Cientos de webcams personales emiten en Internet sin que sus dueños lo sepan</a> <span>(Fuente: Antena 3)</span></span></span></span></span></span></span></span></li>
<li><span><span><span><span><span><span><span><span><a href="http://www.elmundo.es/elmundo/2013/01/28/navegante/1359402509.html" target="_blank">WhatsApp, acusada en Holanda y Canadá de violar la privacidad de sus usuarios</a> <span>(Fuente: El Mundo)</span></span></span></span></span></span></span></span></span></li>
<li><span><span><span><span><span><span><span><span><span><a href="http://seifreed.com/2013/01/29/atacando-a-traves-del-informer/" target="_blank">Atacando a través del "informer"</a> <span>(Fuente: Caminando entre bits...)</span></span></span></span></span></span></span></span></span></span></li>
<li><span><span><span><span><span><span><span><span><span><span><a href="http://unaaldia.hispasec.com/2013/01/publicada-la-leccion-17-de-la.html" target="_blank">Publicada la Lección 17 de la enciclopedia intypedia " Datos Personales Guía de Seguridad para Usuarios"</a> <span>(Fuente: Hispasec Sistemas)</span></span></span></span></span></span></span></span></span></span></span></li>
<li><span><span><span><span><span><span><span><span><span><span><span><a href="http://www.nacionred.com/proteccion-de-datos/dia-de-la-proteccion-de-datos-infografia-y-advertencia-del-parlamento-europeo" target="_blank">Día de la Protección de Datos [Infografía] y advertencia del Parlamento Europeo</a> <span>(Fuente: Nación Red)</span></span></span></span></span></span></span></span></span></span></span></span></li>
</ul></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Proyectos</span> / <span class="art-post-metadata-category-name">PROYECTOS</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:68:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - PROYECTOS DEL IES";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:0:{}s:6:"module";a:0:{}}
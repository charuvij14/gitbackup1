<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:55866:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Reserva de plaza</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 06 Marzo 2014 18:57</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=205:reserva-de-plaza&amp;catid=242&amp;Itemid=612&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=50aee3ba0b568297979695c0e02d38bf01f32f58" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 29003
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading" style="text-align: center;"><span style="text-decoration: underline;"><strong>Reserva de plaza PARA EL CURSO 2014/15</strong></span></h2>
<div class="plg_fa_karmany"> </div>
<div id="articlepxfontsize1">
<div style="text-align: justify;">El plazo de preinscripción para la matrícula en Educación Secundaria Obligatoria y Bachillerato, en todas sus modalidades, será durante todo el mes de MARZO. <br /><br /></div>
La documentación requerida es:
<ul class="list-arrow arrow-red">
<li>Solicitud de escolarización, que se puede recoger en Secretaría, o descargarla desde aquí para <a href="archivos_ies/solicitud_admision.pdf" target="_blank">ESO o Bahillerato</a> y para <a href="archivos_ies/solicitud_plaza_grado_superior.pdf" target="_blank">FP Grado _Superior</a> - <a href="archivos_ies/solicitud_reserva_plaza.pdf" target="_blank">Reserva de puesto escolar</a></li>
</ul>
<hr />
<p> </p>
<h1 class="portlet-title"><span class="portlet-title-text">Información General</span></h1>
<menu id="portlet-topper-toolbar_ABACO_WEB_WAR_abacoportlet_INSTANCE_J8tH" class="portlet-topper-toolbar"></menu>
<div class="detalle_html">
<p style="text-align: center;"><img src="archivos_ies/13_14/bannerinfantil0-3anos.jpg" border="0" alt="Escolarización 2013-2014 (EDUC2013_728X90.jpg)" title="Escolarización 2013-2014 (EDUC2013_728X90.jpg)" style="width: 580px; height: 72px; border: 0;" /></p>
<p><strong>La Consejería de Educación, Cultura y Deporte convoca el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato para el curso escolar 2014/15.</strong></p>
<p>Durante todo el mes de marzo permanecerá abierto en Andalucía el plazo para la presentación de solicitudes de admisión correspondientes al curso escolar 2014/15 en la red de centros sostenidos con fondos públicos. Se inicia de este modo el proceso de escolarización en el que participan todos los niños y niñas que se incorporan por primera vez al sistema educativo, tanto en centros públicos como privados concertados, así como el alumnado que cambie de centro escolar.</p>
<p>Entre los días 1 y 31 de marzo, ambos inclusive, deberán presentar su solicitud los escolares de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.</p>
<p>Continúa publicada, con nuevas mejoras, la aplicación para smartphones que permite realizar consultas de centros así como de los puntos del baremo por domicilio.</p>
<p>Por su parte, los centros educativos publican la relación de puestos escolares vacantes así como la información sobre las direcciones catastrales comprendidas en sus zonas de influencia y zonas limítrofes. Cuando la oferta de plazas coincida o sea superior a la demanda, los solicitantes serán admitidos y solo en aquellos casos donde no se pueda atender toda la demanda, se procederá a la baremación de las solicitudes. </p>
<p>Los centros harán pública en su tablón de anuncios, antes del 22 de abril, la relación de solicitantes con la puntuación asignada y, a partir de esa fecha, se abrirá un plazo de 10 días lectivos para la presentación de alegaciones. Si tras la aplicación de los criterios de baremación (hermanos en el centro, domicilio familiar o laboral, por trabajar la madre o el padre en el centro, renta anual, discapacidad, familia numerosa o monoparental) se producen empates, se aplicará el resultado del sorteo público que se celebrará el día 14 de mayo de 2014. La relación definitiva de admitidos y no admitidos saldrá el 20 de mayo.</p>
<p>Para atender consultas sobre todo el proceso de escolarización, la Consejería ha habilitado un teléfono gratuito de información (900 848 000), en horario ininterrumpido de 8.00 a 19.00 horas.</p>
</div>
<h3 class="tituloCaja">Tríptico informativo curso escolar 2014 / 15</h3>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/Triptico_informativo.pdf" target="_blank" title="Visualizar el documento Se abre en una nueva ventana"> <strong>Tríptico informativo (PDF / 221,48Kb)</strong> </a></li>
<li class="icopdf"><a href="archivos_ies/13_14/Cartel_informativo.pdf" target="_blank" title="Visualizar el documento Se abre en una nueva ventana"> <strong>Cartel informativo (PDF / 106,04Kb)</strong></a><a href="http://portal.ced.junta-andalucia.es/educacion/webportal/abaco-portlet/content/2edb31e2-36d8-4023-a893-4c7a4513cda7" target="_blank" title="Visualizar el documento Se abre en una nueva ventana"> </a></li>
</ul>
<hr />
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/solicitud_admision.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO III (PDF / 285,66Kb)</strong> <br /> Solicitud de admisión en segundo ciclo de Educación Infantil, Educación Primaria, Educación Especial, E.S.O. y Bachillerato </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20IV.%20Matrícula%20en%20segundo%20ciclo%20de%20Educación%20Infantil%20y%20Educación%20Primaria.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO IV (PDF / 225,72Kb)</strong> <br /> Matrícula en segundo ciclo de Educación Infantil y Educación Primaria </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20V.%20Matrícula%20en%20Educación%20Especial.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO V (PDF / 80,03Kb)</strong> <br /> Matrícula en Educación Especial </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20VI.%20Matrícula%20en%20Educación%20Secundaria%20Obligatoria.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO VI (PDF / 102,24Kb)</strong> <br /> Matrícula en Educación Secundaria Obligatoria </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20VII.%20Matrícula%20en%20Programas%20de%20Cualificación%20Profesional%20Inicial.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO VII (PDF / 56,89Kb)</strong> <br /> Matrícula en Programas de Cualificación Profesional Inicial </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20VIII.%20Matrícula%20en%20Bachillerato.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO VIII (PDF / 95,32Kb)</strong> <br /> Matrícula en Bachillerato </a></li>
</ul>
<ul class="listado_documentos">
<li class="icopdf"><a href="archivos_ies/13_14/ANEXO%20IX.%20Solicitud%20Procedimiento%20Extraordinario%20de%20Admisión.pdf" target="_blank" title="Pulse para visualizar el documento"> <strong>ANEXO IX (PDF / 306,73Kb)</strong> <br /> Solicitud Procedimiento Extraordinario de Admisión </a></li>
</ul>
<div class="paginacion"> </div>
<h1 class="portlet-title"><span class="portlet-title-text">Presentación telemática de la solicitud</span></h1>
<menu id="portlet-topper-toolbar_ABACO_WEB_WAR_abacoportlet_INSTANCE_sHc8" class="portlet-topper-toolbar"></menu>
<div class="detalle_html">
<div class="tituloCajaColor">SECRETARÍA VIRTUAL</div>
<p>La solicitud de admisión podrá realizarse a través de la <strong>Secretaría Virtual</strong> de conformidad con lo establecido en el <strong>artículo 49 del Decreto 40/2011, de 22 de febrero.</strong></p>
<p>Para realizar la presentación telemática se requiere un certificado digital reconocido para cada uno de los miembros de la unidad familiar que deban firmar la solicitud.</p>
<p>La persona que tramita la solicitud no podrá realizar la presentación telemática de la misma y obtener el correspondiente recibo electrónico, hasta que todos los miembros de la unidad familiar que deban firmar alguna autorización, la hayan firmado electrónicamente.</p>
</div>
<h3 class="tituloCaja">Tramitación telemática</h3>
<ul id="aui_3_2_0_1169" class="listContRelacionados">
<li id="aui_3_2_0_1167">
<div class="divHiddenLeft"><a href="https://www.juntadeandalucia.es/educacion/secretariavirtual/" target="_blank" title="Se abre en una nueva ventana"> <strong class="enlaceExterior">Acceso a la Secretaría Virtual</strong></a><a href="https://www.juntadeandalucia.es/educacion/secretariavirtual/" target="_blank" title="Se abre en una nueva ventana"> </a></div>
</li>
</ul>
</div>
<hr />
<p> </p>
<h1 class="portlet-title"><span class="portlet-title-text">Normativa</span></h1>
<menu id="portlet-topper-toolbar_ABACO_NORMATIVAS_WAR_abacoportlet" class="portlet-topper-toolbar"></menu>
<ul class="listado">
<li><strong><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/infantil-primaria-eso-bachillerato/normativa/-/normativas/detalle/decreto-40-2011-de-22-de-febrero-por-el-que-se-regulan-los-criterios-y-el-procedimiento-de-admision" target="_blank" title="DECRETO 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.">DECRETO 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato. </a></strong></li>
</ul>
<ul class="listado">
<li><strong><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/infantil-primaria-eso-bachillerato/normativa/-/normativas/detalle/correccion-de-errores-al-decreto-40-2011" target="_blank" title="CORRECCIÓN de errores al Decreto 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato">CORRECCIÓN de errores al Decreto 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato </a></strong></li>
</ul>
<ul class="listado">
<li>24 de febrero de 2011<br /> <strong><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/infantil-primaria-eso-bachillerato/normativa/-/normativas/detalle/orden-de-24-de-febrero-de-2011-por-la-que-se-desarrolla-el-procedimiento-de-admision-del-alumnado" target="_blank" title="ORDEN de 24 de febrero de 2011, por la que se desarrolla el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.">ORDEN de 24 de febrero de 2011, por la que se desarrolla el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato. </a></strong></li>
</ul>
<p> </p>
<hr />
<p> </p>
<h1 id="aui_3_2_0_1198" class="portlet-title"><span id="aui_3_2_0_1197" class="portlet-title-text">Preguntas frecuentes</span></h1>
<menu id="portlet-topper-toolbar_faqPortlet_WAR_porletEducacion_INSTANCE_6uYZ" class="portlet-topper-toolbar"></menu>
<div class="entry"><span style="display: block; text-align: right; padding: 0pt 20px; margin-bottom: -10px; font-size: small;"> </span></div>
<div id="aui_3_2_0_1190" class="entry"><span id="aui_3_2_0_1189" style="display: block; text-align: right; padding: 0pt 20px; margin-bottom: -10px; font-size: small;"><a rel="jfaq_collapse"></a></span>
<p id="aui_3_2_0_1202">¿Tiene que presentar solicitud de admisión para acceder al segundo ciclo de educación infantil el alumnado matriculado en el primer ciclo de educación infantil?.</p>
<dl class="simple_jfaq"><dt id="aui_3_2_0_1203" style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>SÍ. </strong>Según lo establecido en el artículo 34.3 del Decreto 149/2009, de 12 de mayo, por el que se regulan los centros que imparten el primer ciclo de educación infantil, a los efectos de admisión del alumnado no podrán establecerse adscripciones del primer al segundo ciclo de educación infantil. En cualquier caso, para acceder al segundo ciclo de educación infantil en centros sostenidos con fondos públicos se deberá participar en el procedimiento de admisión establecido en el Decreto 40/2011, de 22 de febrero, por el que se regulan los criterios y el procedimiento de admisión del alumnado en los centros docentes públicos y privados concertados para cursar las enseñanzas de segundo ciclo de educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Quiénes deben solicitar una plaza escolar?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<ul>
<li style="text-align: justify;">Quien acceda por primera vez al segundo ciclo de Educación Infantil (3, 4 ó 5 años) para cursar este ciclo en un centro público o privado concertado.</li>
<li style="text-align: justify;">Quien acceda por primera vez a la Educación Primaria sin haber cursado antes el segundo ciclo de infantil.</li>
<li style="text-align: justify;">Quien acceda por primera vez a la Educación Primaria habiendo cursado el segundo ciclo de Educación Infantil en un centro privado o esté matriculado, en el curso 2013/14, en el tercer curso del segundo ciclo de Educación Infantil (5 años) en un centro privado concertado cuando el curso en el que está matriculado no está concertado.</li>
<li style="text-align: justify;">Quien opte por otro centro docente público o privado concertado distinto del que le corresponde por adscripción.</li>
<li style="text-align: justify;">Quien haya sido informado por la dirección del centro público o la titularidad del centro privado concertado en el que finaliza estudios que está adscrito a varios centros (adscripción múltiple) mediante el Anexo II de la Orden vigente que regula la admisión.</li>
<li style="text-align: justify;">Quien estando matriculado en el curso 2013/14 en un centro público o privado concertado desee cambiar a otro centro a un curso sostenido con fondos públicos.</li>
</ul>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Dónde puedo encontrar la normativa que rige el proceso de admisión del mes de marzo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">En la web de la Consejería de Educación, Cultura y Deporte <span>(<a href="http://www.juntadeandalucia.es/educacion">www.juntadeandalucia.es/educacion/</a>) </span>y en los tablones de anuncios de cada centro docente tanto público como privado concertado durante todo el proceso.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuántas solicitudes se pueden presentar en el proceso de admisión?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">En el proceso de admisión solo se podrá presentar <strong><span>una solicitud por alumno o alumna,</span></strong> para la enseñanza y curso en el que se solicita la admisión del alumno o alumna. Dicha solicitud se presentará preferentemente en el centro docente donde se solicita la admisión prioritariamente.</p>
<p style="text-align: justify;"><strong><span style="text-decoration: underline;">El solicitante debe quedarse con una copia del impreso de solicitud registrado por el centro</span></strong></p>
<p style="text-align: justify;"> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué ocurriría si se presenta la solicitud de admisión fuera de plazo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">Que perdería la prioridad que en su caso le hubiese correspondido, en relación con las presentadas en plazo.</p>
<p style="text-align: justify;">Una vez concluida la adjudicación de plazas escolares, las comisiones territoriales de garantías de admisión adjudicarán una plaza escolar a los alumnos o alumnas cuya solicitud de admisión fue presentada fuera del plazo establecido.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué ocurrirá si se presenta más de una solicitud para centros diferentes?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">Que el alumno o alumna perderá todos los derechos de prioridad que puedan corresponderle.</p>
<p style="text-align: justify;">Una vez concluida la adjudicación de plazas escolares, las comisiones territoriales de garantías de admisión adjudicarán una plaza escolar a los alumnos o alumnas que presentaron más de una solicitud.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué alumnado<strong> <span style="text-decoration: underline;">no tiene</span></strong> que presentar solicitud de admisión durante el mes de marzo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>1.- El alumnado que en el curso escolar 2013/14 esté matriculado en un centro público o privado concertado en un curso sostenido con fondos públicos y desee continuar en dicho centro en el curso escolar 2014/15 en un curso sostenido con fondos públicos.</p>
<div> 2.- Aquel alumnado que en el curso 2013/14 esté matriculado, en el último curso de alguna de las etapas educativas de un centro público o privado concertado y vaya a cursar el primer curso de la etapa siguiente en otro centro al que esté adscrito, salvo que concurran las circunstancias previstas en la adscripción múltiple. De la adscripción autorizada será informado por la dirección del centro público o la titularidad del centro privado concertado en el que se encuentra matriculado en el curso 2013/14. </div>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Dónde debe presentarse la solicitud de admisión?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Preferentemente en el centro docente en el que se solicita la admisión como prioritario, sin perjuicio de lo establecido en el artículo 38.4 de la Ley 30/1992, de 26 de noviembre, y en los artículos 82 y 83 de la Ley 9/2007, de 22 de octubre, en cuyo caso, para agilizar el procedimiento, podrá remitirse copia autenticada al centro al que se dirige la solicitud. </p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuál es el plazo de presentación de solicitudes de admisión?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><span style="text-decoration: underline;">Durante todo el mes de marzo</span> está abierto el plazo para presentar solicitudes de admisión para un puesto escolar en centros públicos y privados concertados para las enseñanzas correspondientes al segundo ciclo de la educación infantil, educación primaria, educación especial, educación secundaria obligatoria y bachillerato.</p>
<div><span style="text-decoration: underline;">Antes del plazo establecido los centros no podrán recepcionar solicitudes de admisión</span>.</div>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Puede la persona solicitante indicar más de una enseñanza en su solicitud?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>NO</strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Puede la persona solicitante de plaza escolar modificar algún dato de la solicitud de admisión después de haberla presentado?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Una vez entregado el impreso de solicitud, los datos contenidos en el mismo sólo se podrán alterar mediante comunicación por escrito de la persona solicitante dirigida a la persona que ejerce la dirección del centro público o la persona física o jurídica titular del centro privado concertado, y durante el plazo establecido para la presentación de solicitudes.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Están obligados los centros a recepcionar las solicitudes aunque no hayan publicado vacantes para el curso solicitado?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>Sí. </strong>Los centros deben recepcionar todas las solicitudes que se presenten en el mismo (aunque el centro no tenga plazas escolares vacantes para la enseñanza y curso que se solicita). </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Dónde puedo consultar las áreas de influencia de un centro?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">Las áreas de influencia de cada centro se publicarán, antes de comenzar el mes de marzo, en el tablón de anuncios de la Delegación Territorial de la Consejería de Educación, Cultura y Deporte.<br />Además, cada centro debe,  tal como le hayan sido remitidas por la Delegación Territorial, publicarlas en el tablón de anuncios con anterioridad al inicio del período establecido para la presentación de solicitudes. si bien debe advertirse que la información mostrada a partir del Sistema de Información Geográfico Educasig es meramente informativa y no genera expectativas ni derechos al usuario. </p>
<p style="text-align: justify;">Además se podrán consultar, a  título informativo, en el Visor de Escolarización de de la Consejería de Educación, Cultura y Deporte <span style="text-align: left; text-transform: none; background-color: #ffffff; text-indent: 0px; display: inline ! important; font: small/15px arial,sans-serif; white-space: normal; float: none; letter-spacing: normal; color: #009933; word-spacing: 0px;"><a name="Portal_Educasig" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/principal.html" target="_blank">www.juntadeandalucia.es/educacion/educasig</a>  <span style="color: #000000; font-size: small;"> si bien debe advertirse que la información mostrada a partir del Sistema de Información Geográfico Educasig es <span style="text-decoration: underline;">meramente informativa </span>y no genera expectativas ni derechos al usuario. </span></span></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Puedo presentar la solicitud de admisión en un centro distinto de aquél en el que deseo que el alumno o alumna sea admitido/a como prioritario?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">Sí, aunque preferiblemente debe entregarla en el centro en el que desea que el alumno/a sea admitido/a como prioritario.</p>
<p style="text-align: justify;">Igualmente, conforme a lo que establece el artículo 38.4 de la Ley 30/92 de Régimen Jurídico de las Administraciones Públicas y del Procedimiento Administrativo Común, la solicitud y la documentación se podrán presentar en los registros de cualquier órgano administrativo, que pertenezca a la Administración General del Estado, a la de cualquier Administración de las Comunidades Autónomas y en las oficinas de Correos en la forma reglamentariamente establecida.<br />En caso de que se usara esta posibilidad para agilizar el procedimiento se remitirá copia autenticada de la documentación al centro docente en el que se solicita el puesto escolar.</p>
</dd><dt style="cursor: pointer;">Por qué criterios se regirá, en su caso, la admisión del alumnado?</dt><dd style="display: block;">
<div>De conformidad con la normativa vigente, el orden de admisión se regirá por los siguientes criterios:</div>
<div> </div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Existencia de hermanos o hermanas matriculados/as en el centro docente o de padres, madres o tutores o guardadores legales que trabajen en el mismo.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Proximidad del domicilio familiar o del lugar de trabajo del padre, de la madre o de la persona tutora o guardadora legal.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Renta anual de la unidad familiar.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Concurrencia de discapacidad en el alumno o alumna o en sus padres, madres o tutores o guardadores legales, o en alguno de sus hermanos o hermanas o menores en acogimiento en la misma unidad familiar. En el segundo ciclo de la educación infantil se considerará también la presencia en el alumnado de trastornos del desarrollo.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- <span>Que el alumno o alumna pertenezca a una familia con la condición de numerosa.</span></div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Que el alumno o alumna pertenezca a una familia con la condición de monoparental y sea menor de edad o mayor de edad sujeto a patria potestad prorrogada o tutela.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;">- Para las enseñanzas de Bachillerato, además de los criterios establecidos anteriormente, se considerará el expediente académico de los alumnos y alumnas.</div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;"> </div>
<div style="text-align: justify; text-indent: -18pt; margin-left: 36pt;"> </div>
<p> </p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué ocurre cuando varios hermanos o hermanas solicitan plaza escolar en un mismo centro docente para distintos cursos?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<div style="text-align: justify;">En el caso de que existan dos o más solicitudes de admisión de hermanos o hermanas en un mismo centro docente para distintos cursos, cuando uno de ellos resulte admitido, se concederá a los demás 16 puntos.</div>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Deben relacionarse en la solicitud centros subsidiarios?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>Sí</strong>. Se pueden indicar hasta cuatro centros subsidiarios que serán considerados si el alumno o alumna no resulta admitido en el centro solicitado como prioritario. Para cada uno de los centros solicitados el Consejo Escolar del centro público o la persona física o jurídica titular del centro privado concertado establecerá la puntuación que corresponda en función de los distintos apartados del baremo. </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuándo se entiende que el alumno o alumna pertenece a una familia con la condición de monoparental?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">De conformidad con la normativa de admisión "se entenderá que el alumno o alumna menor de edad o mayor de edad sujeto a patria potestad prorrogada o tutela pertenece a una familia con la condición de monoparental cuando su patria potestad esté ejercida por una sola persona o cuando siendo ejercida por dos personas, exista orden de alejamiento de una de ellas con respecto a la otra con la que convive el alumno o alumna".</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué ocurre cuando varios hermanos o hermanas solicitan plaza escolar en el mismo centro docente y para el mismo curso?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">De acuerdo con la normativa de aplicación en el caso de que varios hermanos o hermanas soliciten una plaza escolar en el mismo centro docente y para el mismo curso de una de las etapas educativas, la admisión de uno de ellos supondrá la admisión de los demás. <br /> </p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué requisitos son necesarios para acceder a las distintas etapas educativas?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">1. Primer curso del segundo ciclo de la <strong>educación infantil</strong>:</p>
<div style="text-align: justify; text-indent: 35.25pt;">- 3 años cumplidos antes del 31 de diciembre de 2014.</div>
<div style="text-align: justify; margin-left: 35.25pt;"> </div>
<div style="text-align: justify;">2. Primer curso de <strong>educación primaria</strong>:</div>
<div style="text-align: justify;"> </div>
<div style="text-align: justify; margin-left: 35.4pt;">- 6 años cumplidos antes del 31 de diciembre de 2014, salvo autorización específica.</div>
<div style="text-align: justify; margin-left: 35.4pt;"> </div>
<div style="text-align: justify;">3. Primer curso de <strong>educación secundaria obligatoria:</strong></div>
<div style="text-align: justify; margin-left: 35.25pt;"> </div>
<div style="text-align: justify; margin-left: 35.25pt;">- Haber finalizado la educación primaria.</div>
<div style="text-align: justify;"> </div>
<div style="text-align: justify;">4. Primer curso de <strong>Bachillerato:</strong></div>
<div style="text-align: justify; margin-left: 35.4pt;"> </div>
<div style="text-align: justify; margin-left: 35.4pt;"><strong>- </strong>Poseer el título de Graduado en Educación Secundaria Obligatoria o el de Técnico de Formación Profesional.</div>
<div style="text-align: justify; margin-left: 35.4pt;"> </div>
<div style="text-align: justify; margin-left: 35.4pt;">- Para el primer curso de Bachillerato en la modalidad de artes también se podrá acceder con el título de Técnico de Artes Plásticas y Diseño. </div>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p style="text-align: justify;">¿Cuándo se considera que la persona que ostenta la representación o la guarda legal del alumno o alumna tiene su puesto de trabajo habitual en el centro docente?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">De conformidad con lo establecido en el artículo 9 de la Orden de 24 de febrero de 2011, a los efectos de lo establecido en el artículo 12 del Decreto 40/2010<span>, se considera que la persona que ostenta la representación o la guarda legal del alumno o alumna tiene su puesto de trabajo habitual en el centro docente si ejerce en el mismo una actividad laboral continuada con una jornada mínima de diez horas semanales y forma parte del siguiente personal:</span></p>
<div style="margin-top: 0cm; margin-right: -7.1pt; margin-bottom: 0.0001pt; text-align: justify; text-indent: 36pt;"> a) Personal al servicio de la Administración de la Junta de Andalucía o de los Ayuntamientos correspondientes que presta servicio en el centro docente público.</div>
<div style="margin-top: 0cm; margin-right: -7.1pt; margin-bottom: 0.0001pt; text-align: justify; text-indent: 36pt;">b) Personal que tenga una relación contractual con el centro docente.</div>
<div style="margin-top: 0cm; margin-right: -7.1pt; margin-bottom: 0.0001pt; text-align: justify; text-indent: 36pt;">c) Personal empleado de las empresas que presten servicios en el centro docente.</div>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita el lugar de trabajo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<ul>
<li style="text-align: justify;">En el caso de que la actividad laboral se realice por cuenta ajena, será necesario presentar la vida laboral de la persona cuyo lugar de trabajo va a ser tenido en consideración y una certificación expedida al efecto por la persona titular de la empresa o por la persona responsable de personal de la misma que deberá contener el domicilio del lugar del trabajo.</li>
<li style="text-align: justify;">En el caso de que se desarrolle la actividad laboral por cuenta propia, se deberá presentar una certificación acreditativa del alta en el Impuesto de Actividades Económicas y una declaración responsable de la persona cuyo lugar de trabajo va a ser tenido en consideración sobre la vigencia de la misma.</li>
<li style="text-align: justify;">En el supuesto de que no exista obligación legal de estar dado de alta en el citado impuesto, se acreditará mediante la presentación de alguno de los siguientes documentos:</li>
</ul>
<p style="text-align: justify;">             a) Copia autenticada de la correspondiente licencia de apertura expedida por el Ayuntamiento respectivo.</p>
<p style="text-align: justify;">             b) Copia sellada por el Ayuntamiento de la declaración responsable o comunicación previa presentada ante el mismo.</p>
<p style="text-align: justify;">             c) Alta en la Seguridad Social y una declaración responsable de la persona interesada sobre la vigencia de la misma.</p>
<p style="text-align: justify;"> </p>
<p style="text-align: justify;"> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Puede acreditarse el domicilio habitual de la persona solicitante si ésta es mayor de edad con certificación expedida por el ayuntamiento?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>SÍ.</strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita el domicilio familiar?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p> <span>Si se solicita plaza para un alumno o alumna menor de edad o mayor de edad sujeto a patria potestad prorrogada,<strong> la persona que firma la solicitud declara que convive con el alumno o alumna y ejerce su guarda y custodia</strong> (REPRESENTANTE O GUARDADOR 1 en la solicitud). <strong>Si opta por el domicilio familiar podrá autorizar en la solicitud a la Consejería para recabar el domicilio familiar por medios informáticos o telemáticos.</strong> Cuando la información obtenida no coincida con el domicilio que consta en la solicitud, la persona solicitante deberá aportar previo requerimiento de la dirección del centro público o la persona física o jurídica del centro privado concertado, el CERTIFICADO DE EMPADRONAMIENTO expedido por el Ayuntamiento.<br /></span></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué domicilio del alumno o alumna se considera en el procedimiento de admisión?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Aquel por el que haya optado en la solicitud de plaza escolar.</p>
<p><strong>En el Anexo III, de solicitud de plaza escolar, la persona que firma la solicitud - que convive con el alumno o alumna menor de edad y ejerce su guarda y custodia -, indica si opta POR EL DOMICILIO FAMILIAR del alumno o alumna o POR EL LUGAR DE TRABAJO del represente o guardador legal.</strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué domicilio se considera como domicilio familiar en el caso de alumnos o alumnas menores en acogimiento residencial?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>De conformidad con lo establecido en la normativa, en los casos de menores cuya medida de protección a la infancia sea el acogimiento residencial, el domicilio del centro de protección en el que resida tendrá la consideración de domicilio familiar.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita la condición de familia numerosa?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Con la copia autenticada del título oficial de familia numerosa que esté en vigor cuando no se haya autorizado a la Consejería para recabar la información de la Consejería competente en la materia o cuando habiéndose indicado en la solicitud dicha autorización la información no se haya obtenido. En este último supuesto, la dirección del centro público o la persona física o jurídica del centro privado concertado le habrá requerido fehacientemente dicha acreditación.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita la condición de familia monoparental?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Mediante copia autenticada del Libro de Familia, dicha copia deberá incluir las páginas escritas pudiendo sustituirse las páginas no escritas por una diligencia en la última página escrita en la que el funcionario que la autentique deje constancia de qué páginas están en blanco.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se acredita la discapacidad?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">De conformidad con la normativa de admisión  las personas mayores de edad de la unidad familiar del alumno o alumna que se encuentren en esta situación deberán autorizar a la Consejería competente en materia de Educación en el modelo de solicitud de admisión, para recabar la información necesaria a la Consejería competente en la materia. En el caso de alumnos o alumnas menores de edad o mayores de edad sujetos a patria potestad prorrogada o tutela, serán sus padres, madres o tutores o guardadores legales los que realicen dicha autorización.</p>
<p style="text-align: justify;">Si no se pudiera obtener la información referida, la persona solicitante a requerimiento de la dirección del centro público o la persona física o jurídica titular del centro concertado, deberá presentar los correspondientes certificados de los dictámenes de discapacidad emitidos por el órgano competente de la Administración de la Junta de Andalucía o, en su caso, de otras Administraciones públicas.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>Si no se ha autorizado en la solicitud a la Consejería para recabar de la Consejería competente la pertenencia del alumno o alumna a familia con la condición de numerosa y  se tiene el titulo de familia numerosa caducado, ¿qué se debe hacer?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Presentar la solicitud de reconocimiento o renovación del mismo, y aportar  éste o su renovación a la mayor brevedad, y siempre antes de la resolución del procedimiento de admisión.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuál es el máximo permitido que pueden cobrar los centros docentes concertados?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>En ningún caso podrán los centros docentes públicos o privados concertados percibir cantidades de las familias por recibir las enseñanzas de carácter gratuito, imponer a las familias la obligación de hacer aportaciones a fundaciones o asociaciones ni establecer servicios obligatorios, asociados a las enseñanzas, que requieran aportación económica, por parte de las familias del alumnado. (Artículo 88.1 de la Ley Orgánica 2/2006, de 3 de mayo, de educación).</p>
<p>En el marco de lo dispuesto en el artículo 51 de la Ley Orgánica 8/1985, de 3 de julio, reguladora del Derecho a la Educación, quedan excluidos de esta categoría las actividades extraescolares, las complementarias y los servicios escolares que, en todo caso, tendrán carácter voluntario.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Las personas solicitantes de plaza escolar están obligadas a presentar, con carácter previo, documentación sobre la renta anual de la unidad familiar cuando pretendan la valoración de este criterio de admisión?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong>No</strong>. Sólo en el caso de que la Administración Tributaria no disponga de la información de carácter tributario que se precisa para la acreditación de la renta anual, previo requerimiento de la dirección del centro público o de la persona física o jurídica titular del centro privado concertado, deberá aportar una certificación de haberes, declaración jurada o cualquier otro documento de cada una de las personas mayores de 16 años de la unidad familiar.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuál es plazo para solicitar la bonificación de los servicios complementarios (comedor, aula matinal y actividades extraescolares)?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>El plazo para solicitar la bonificación de los servicios complementarios es del 1 al 7 de septiembre de 2014.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Constituye un criterio para la admisión del alumnado la presencia en el mismo de trastornos del desarollo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Solamente en el segundo ciclo de Educación Infantil.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cómo se valora la discapacidad o trastorno del desarrollo en el alumno o alumna?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>La valoración de que el alumno o alumna presenta una discapacidad o un trastorno del desarrollo, será de dos puntos. <strong>En el caso de que en el alumno o alumna confluyan las dos circunstancias, la valoración será, asimismo, de dos puntos.</strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuándo se tendrá en cuenta el criterio de presentar trastornos del desarrollo?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>El criterio de presentar trastorno del desarrollo sólo se tendrá en cuenta en el segundo ciclo de Educación Infantil cuando el alumno o alumna disponga del informe correspondiente, emitido por el Centro de Atención Infantil Temprana en el que esté recibiendo tratamiento.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Las personas solicitantes de puesto escolar están obligadas a firmar la autorización para que la Agencia Estatal de Administración Tributaria suministre la información sobre la renta anual de la unidad familiar?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Sólo en el caso de que deseen que el criterio de renta anual de la unidad familiar sea valorado.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuál es el<strong>  IPREM </strong>(Indicador Público de Renta de Efectos Múltiples) de aplicación en el procedimiento de admisión que se inicia el 1 de marzo y su cuantía?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>1.- El IPREM a aplicar en el procedimiento de admisión en curso, regulado por el Decreto 40/2011, de 22 febrero, y Orden de 24 de febrero de 2011 que lo desarrolla será el del ejercicio 2012.</p>
<p>2.- En cuanto a su cuantía, debemos acudir a la disposición adicional decimocuarta de la Ley 2/2012, de 29 de junio, de Presupuestos Generales del Estado para el año 2012 (hasta su entrada en vigor, se mantuvieron prorrogados los Presupuestos del 2011). En virtud de la misma, y de conformidad con lo establecido por el artículo 2.2 del Real Decreto-ley 3/2004, de 25 de junio, el IPREM anual es de 6.390,13 euros.</p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Qué es copia autenticada?</p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>El Decreto 204/1995, de 29 de agosto,por el que se establecen medidas organizativas para los servicios administrativos de atención directa a los ciudadanos (BOJA de 26 de octubre, núm. 136), en el artículo 21 establece:</p>
<p>Art. 21 <strong><span style="text-decoration: underline;">Tipos de copias de documentos.</span></strong></p>
<p>Las copias de documentos en el ámbito de actuación de la Administración de la Junta de Andalucía, comprenderá los siguientes supuestos:</p>
<p>1. Las copias auténticas de documentos administrativos expedidos por el mismo órgano que emitió el documentos original.</p>
<p>2. <strong><span style="text-decoration: underline;">Las copias autenticadas de documentos privados y públicos, mediante cotejo con el original y en las que se estampará si procediera la correspondiente diligencia de compulsa.</span></strong></p>
<p>El artículo 22 del referido Decreto  recoge la competencia para la expedición de copias. En el apartado 2, establece "la expedición de copias autenticadas de documentos administrativos de la Junta de Andalucía, corresponderá a las jefaturas de sección u órgano asimilado dependiente del órgano que hubiera emitido el documento original y que tengan encomendada las funciones de tramitación o custodia del expediente a que pertenece dicho documento original"</p>
<div> </div>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p style="text-align: justify;"><strong>Para que los ciudadanos interpongan recurso de alzada, ¿deben haber presentado con anterioridad la correspondiente reclamación o alegaciones?</strong></p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p style="text-align: justify;">No es necesario haber presentado alegaciones contra la puntuación total obtenida en aplicación del baremo, ante la dirección de los centros docentes públicos o ante la persona física o jurídica titular de los centros docentes privados concertados, para interponer recurso de alzada contra las resoluciones de admisión del alumnado.</p>
<p style="text-align: justify;">El recurso de alzada se rige por lo establecido en al artículo 52 del decreto 40/2011, y en los artículos 107.1, 114 y 115 de la Ley 30/1992.</p>
<p style="text-align: justify;"> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>Al estar matriculado en Educación Secundaria Obligatoria para personas adultas, ¿podría matricularme en otra enseñanza?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Sólo si una de ellas se realiza en la modalidad semipresencial o a distancia.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Podría matricularme en un mismo año académico en enseñanzas elementales y profesionales de música y de danza?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p><strong><span style="font-size: 12pt;">-No.</span></strong></p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Podría matricularme en Bachillerato estando matriculado en un ciclo formativo de Formación Profesional?</p>
<p> </p>
<dl class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd style="display: block;">
<p>Sólo si el módulo profesional que está realizando no requiere su presencia habitual y continuada en el centro.</p>
<p> </p>
</dd><dt style="cursor: pointer;"></dt></dl>
<p>¿Cuándo puedo solicitar los servicios complementarios (comedor, aula matinal y actividades extraescolares)?<br /> </p>
<dl id="jfaq" class="simple_jfaq"><dt style="cursor: pointer;"></dt><dd id="aui_3_2_0_1211" style="display: block;">
<p style="text-align: justify;">La solicitud de los servicios complementarios se realiza durante el periodo de matriculación del alumnado, del 1 al 8 de junio en los centros públicos que imparten Educación Infantil (segundo ciclo) y Educación Primaria, y del 1 al 10 de julio en los centros públicos que imparten Educación Secundaria.</p>
<p style="text-align: justify;">La información de los servicios complementarios que ofertan los centros está a disposición del público en los tablones de anuncios de los mismos.</p>
<p id="aui_3_2_0_1210" style="text-align: justify;">Ninguna circunstancia que esté exclusivamente relacionada con la intención de solicitar dichos servicios podrá ser tenida en cuenta como criterio de admisión.<br /> </p>
</dd></dl></div>
<hr />
<p> </p>
<div class="detalle_html">
<p style="text-align: center;"><map name="Map"> 
<area title="Huelva - Se abre en ventana nueva" shape="poly" coords="7,74,4,56,14,41,24,31,46,41,44,48,34,54,40,62,38,86,19,72" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=HU" alt="Huelva" target="_blank" />
 
<area title="Sevilla - Se abre en ventana nueva" shape="poly" coords="47,40,54,39,61,28,65,32,75,49,72,52,76,55,85,51,97,71,87,79,80,84,72,84,65,84,56,88,47,89,38,89,41,65,36,55" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=SE" alt="Sevilla" target="_blank" />
 
<area title="Cádiz - Se abre en ventana nueva" shape="poly" coords="38,89,35,96,44,114,56,123,65,127,74,113,65,104,73,98,72,91,75,90,79,92,82,84,68,86" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=CA" alt="Cádiz" target="_blank" />
 
<area title="Córdoba - Se abre en ventana nueva" shape="poly" coords="67,32,70,29,67,19,81,6,91,5,112,17,116,27,110,33,112,51,120,64,112,67,110,74,105,72,97,71,83,50,76,54,70,34" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=CO" alt="Córdoba" target="_blank" />
 
<area title="Jaén - Se abre en ventana nueva" shape="poly" coords="113,18,171,6,180,20,172,36,164,49,152,55,139,57,124,66,120,65,114,53,111,40,117,28" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=JA" alt="Jaén" target="_blank" />
 
<area title="Granada - Se abre en ventana nueva" shape="poly" coords="176,29,190,36,186,54,172,66,172,72,164,70,152,95,139,97,128,95,112,86,109,81,110,73,117,68,127,66,142,57,155,54,168,43" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=GR" alt="Granada" target="_blank" />
 
<area title="Almería - Se abre en ventana nueva" shape="poly" coords="191,38,201,39,198,48,213,63,202,85,191,96,185,92,175,92,169,99,152,95,166,73,173,73,174,66,181,60,188,52" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=AL" alt="Almería" target="_blank" />
 
<area title="Málaga - Se abre en ventana nueva" shape="poly" coords="75,112,68,105,76,97,74,92,78,92,83,83,97,72,109,75,109,82,128,95,105,97,95,106,84,106,77,113" href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=MA" alt="Málaga" target="_blank" />
 </map></p>
<div class="tituloCaja" style="text-align: left;">Consulta detallada de centros:</div>
<p>Consulta detallada de centros, enseñanzas autorizadas, adscripciones, servicios ofertados en el curso escolar 2014/2015.</p>
<p style="text-align: center;"><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/consulta-de-centros" target="_blank">Consulta de centros 2014/2015</a> <a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/consulta-de-centros"><img src="http://portal.ced.junta-andalucia.es/educacion/webportal/ishare-servlet/content/4e0e2b65-ba3c-4f79-8f34-ba1ef6dd083d" border="0" alt="Enlace" title="Enlace" style="width: 12px; height: 12px;" /></a></p>
<div class="tituloCaja" style="text-align: left;">Consulta de áreas de Influencia:</div>
<p style="text-align: left;">Pulse en una provincia para ver las áreas de influencia:</p>
<p style="text-align: center;"><a href="http://www.juntadeandalucia.es/educacion/educasig/educacion/educasig/principal.html?delegacion=GR"><img src="archivos_ies/13_14/mapa_andalucia2.jpg" border="0" alt="Imagen localización de centros (mapa_andalucia2.jpg)" title="Imagen localización de centros (mapa_andalucia2.jpg)" width="217" style="border: 0;" /></a></p>
<p style="text-align: center;"> </p>
<p style="text-align: center;">Descárguese la aplicación en versión de Smartphone para consultar los centros y las puntuaciones del procedimiento.</p>
<p style="text-align: center;"><a href="https://itunes.apple.com/es/app/iescolariza/id606516551?mt=8" target="_blank"><img src="archivos_ies/13_14/appstore_mini.png" border="0" alt="App Store Banner" title="App Store Banner" style="width: 137px; height: 45px; border: 0;" /></a> <a href="https://play.google.com/store/apps/details?id=es.juntadeandalucia.ced.escolarizacion" target="_blank"><img src="archivos_ies/13_14/google_play_mini.png" border="0" alt="Google Play Banner" title="Google Play Banner" style="text-align: justify; width: 131px; height: 43px; border: 0;" /></a></p>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:67:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Reserva de plaza";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:15:"Escolarización";s:4:"link";s:59:"index.php?option=com_content&view=article&id=210&Itemid=611";}i:1;O:8:"stdClass":2:{s:4:"name";s:16:"Reserva de plaza";s:4:"link";s:59:"index.php?option=com_content&view=article&id=205&Itemid=612";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:38535:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Bachillerato</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 06 Marzo 2014 18:28</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=198:bachillerato&amp;catid=241&amp;Itemid=602&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=2acb0fb4b20c99a700e39ab0f74afff5610b64fd" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 25218
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 style="text-align: center;"><span style="text-decoration: underline;">Bachillerato</span>    </h1>
<div class="jsn-article-content">
<h4> </h4>
<ul>
<li><a href="archivos_ies/13_14/1314-OPTATIVAS.pdf" target="_blank"><span style="color: #2a2a2a;">Asignaturas optativas y de libre disposición en pdf.</span></a></li>
<li><a href="archivos_ies/13_14/ofertaeducativa-ESO-BACHILLERATO-1314.pdf" target="_blank"><span style="color: #2a2a2a;">Oferta educativa ESO y Bachillerato en pdf.</span></a></li>
</ul>
<p><span style="color: #2a2a2a;"> </span></p>
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 3.5pt;" valign="top" width="576">
<p class="MsoNormal" style="text-align: center;" align="center"><strong><span style="font-size: 14.0pt; color: #632423;">1º  BACHILLERATO  (30 horas semanales)</span></strong></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">MATERIAS COMUNES (OBLIGATORIAS)    (18 h) </span></strong></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">CIENCIAS PARA EL MUNDO CONTEMPORÁNEO. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN FÍSICA. <strong>(2 h)</strong> </span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FILOSOFÍA Y CIUDADANÍA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo1;"><!--[if !supportLists]--><strong><span style="color: #632423;">4.<span style="font-weight: normal; font-size: 7pt; font-family: 'Times New Roman';">    </span></span></strong><!--[endif]--><span style="color: #632423;">IDIOMA (INGLÉS O FRANCÉS). <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">5.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">LENGUA CASTELLANA Y LITERATURA I. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo1;"><!--[if !supportLists]--><strong><span style="color: #632423;">6.<span style="font-weight: normal; font-size: 7pt; font-family: 'Times New Roman';">    </span></span></strong><!--[endif]--><span style="color: #632423;">PROYECTO INTEGRADO.<strong> (1 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">7.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">SEGUNDO IDIOMA (FRANCÉS O INGLÉS). <strong>(2 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo1;"><!--[if !supportLists]--><strong><span style="color: #632423;">8.<span style="font-weight: normal; font-size: 7pt; font-family: 'Times New Roman';">    </span></span></strong><!--[endif]--><span style="color: #632423;">RELIGIÓN  o  ATERNATIVA. <strong>(1 h)</strong></span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">            </span></strong></p>
<p class="MsoNormal"><strong><span style="color: #632423;">MATERIAS DE MODALIDAD</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">MODALIDAD DE CIENCIAS Y TECNOLOGÍA. <strong>(12 h)</strong></span></span></p>
<p class="MsoNormal" style="margin-left: 70.8pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 36.0pt; text-indent: -14.7pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">DIBUJO TÉCNICO. (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 36.0pt; text-indent: -14.7pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FÍSICA Y QUÍMICA. (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 36.0pt; text-indent: -14.7pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">MATEMÁTICAS. (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 70.8pt;"><span style="color: #632423;"> </span></p>
<ol style="margin-top: 0cm;" type="1" start="1">
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo5;">BIOLOGÍA Y GEOLOGÍA. (<strong>4 h</strong>)</li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo5;">FÍSICA Y QUÍMICA. (<strong>4 h</strong>)</li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo5;">MATEMÁTICAS. (<strong>4 h</strong>)</li>
</ol>
<p class="MsoNormal" style="margin-left: 70.8pt;"><span style="color: #632423;"> </span></p>
<ol style="margin-top: 0cm;" type="1" start="1">
<li class="MsoNormal" style="color: #632423; mso-list: l4 level1 lfo6;">FÍSICA Y QUÍMICA. (<strong>4 h</strong>)</li>
<li class="MsoNormal" style="color: #632423; mso-list: l4 level1 lfo6;">MATEMÁTICAS. (<strong>4 h</strong>)</li>
<li class="MsoNormal" style="color: #632423; mso-list: l4 level1 lfo6;">TECNOLOGÍA INDUSTRIAL. (<strong>4 h</strong>)</li>
</ol>
<p class="MsoNormal" style="margin-left: 36.0pt; tab-stops: 36.0pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 36.0pt; tab-stops: 36.0pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 36.0pt; tab-stops: 36.0pt;"><span style="text-decoration: underline;"><span style="color: #632423;">MODALIDAD DE HUMANIDADES Y CIENCIAS SOCIALES.  <strong>(12 h)</strong></span></span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><span style="color: #632423;">            <span style="text-decoration: underline;">HUMANIDADES</span></span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l5 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">GRIEGO I. (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l5 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">HISTORIA DEL MUNDO CONTEMPORÁNEO. (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l5 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">LATÍN I. (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 106.2pt; text-align: justify; text-indent: -106.2pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-align: justify; text-indent: -106.2pt;"><span style="color: #632423;">                                    <span style="text-decoration: underline;">SOCIALES</span></span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">ECONOMÍA. (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">HISTORIA DEL MUNDO CONTEMPORÁNEO. (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">MATEMÁTICAS APLICADAS A LAS CCSS. (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo4;"><span style="font-size: 20px; font-weight: bold; text-transform: uppercase; color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo4;"><span style="font-size: 20px; font-weight: bold; text-transform: uppercase; color: #632423;"> </span><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase;"><span style="color: #632423;">PROYECTO INTEGRADO DE CARÁCTER PRÁCTICO (1 h)</span></strong></p>
<h3 class="MsoNormal" style="margin-left: 106.2pt; text-align: left;"> </h3>
<p class="MsoNormal" style="text-align: justify;"><span style="color: #632423;"> </span><span style="color: #632423;">   Esta asignatura es de carácter obligatorio para todos los alumnos que cursan  1º de BACHILLERATO y consiste en un trabajo de investigación de una hora semanal, sobre los siguientes temas propuestos:</span></p>
<p class="MsoNormal" style="text-align: justify;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">Modalidad de Humanidades y Ciencias Sociales.</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<ol style="margin-top: 0cm;" type="1" start="1">
<li class="MsoNormal" style="color: #632423; mso-list: l0 level1 lfo1;">Aprender a razonar II  (Filosofía) / Deporte y Salud II  (Ed. Física) / Taller Artístico (EPV)</li>
<li class="MsoNormal" style="color: #632423; mso-list: l0 level1 lfo1;">Taller de Prensa II  (Lengua C L.) / Mitología y Creación artística-literaria (Griego)</li>
</ol>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<p class="MsoNormal"><strong><span style="color: #632423;">Modalidad de Ciencias y Tecnología.</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<ol style="margin-top: 0cm;" type="1" start="1">
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2;">Aprender a razonar II (Filosofía) / Deporte y Salud II  (Ed. Física) / Taller de Prensa II  (Lengua C.)                </li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2;">Genética y Salud II  (Bio / Geo) </li>
</ol>
<p> </p>
<p> </p>
<div align="center">
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 3.5pt;" valign="top" width="576">
<p class="MsoNormal" style="text-align: center;" align="center"><strong><span style="font-size: 14.0pt; color: #632423;">2º  BACHILLERATO  (30 horas semanales)</span></strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="color: #632423;"> </span></strong></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="text-decoration: underline;"><span style="color: #632423;"> </span></span></strong></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="text-decoration: underline;"><span style="color: #632423;"> </span></span></strong></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="text-decoration: underline;"><span style="color: #632423;">MATERIAS COMUNES (15 horas)</span></span></strong></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">LENGUA CASTELLANA Y LITERATURA II. (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">IDIOMA (INGLÉS II, o FRANCÉS II). (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">HISTORIA DE LA FILOSOFÍA. (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">HISTORIA DE ESPAÑA. (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">5.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">PROYECTO INTEGRADO. (1 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l3 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">6.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">RELIGIÓN o  ALTERNATIVA.  (1 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 54.0pt;"><strong><span style="color: #632423;"> </span></strong></p>
<p class="MsoNormal"><strong><span style="text-decoration: underline;"><span style="color: #632423;">MATERIAS DE MODALIDAD. (Itinerarios formativos)</span></span></strong></p>
<p class="MsoNormal"><strong><span style="color: #632423;">            <span style="text-decoration: underline;">MODALIDAD DE CIENCIAS DE LA NATURALEZA Y DE LA SALUD</span></span></strong></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><span style="color: #632423;">            <span style="text-decoration: underline;">ITINERARIO CIENCIAS E INGENIERÍA <strong>(12 h)</strong></span>:</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">DIBUJO TÉCNICO II. (4 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FÍSICA. (4 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">MATEMÁTICAS II. (4 h)</span></p>
<p style="margin: 0cm; margin-bottom: .0001pt; text-indent: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">ITINERARIO CIENCIAS DE LA SALUD <strong>(12 h)</strong></span></span><strong><span style="color: #632423;">:</span></strong></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l6 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">BIOLOGÍA. (4 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l6 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">QUÍMICA. (4 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l6 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">MATEMÁTICAS II. (4 h)</span></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><span style="color: #632423;"> </span></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="color: #632423;">            <span style="text-decoration: underline;">MODALIDAD DE HUMANIDADES Y CIENCIAS SOCIALES </span></span></strong></p>
<p style="margin: 0cm; margin-bottom: .0001pt; text-indent: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">ITINERARIO DE HUMANIDADES <strong>(12 h)</strong></span></span><span style="color: #632423;">:</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l4 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">GRIEGO II. (4 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l4 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">HISTORIA DEL ARTE. (4 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l4 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">LATÍN II. (4 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">ITINERARIO DE CIENCIAS SOCIALES <strong>(12 h)</strong></span></span><span style="color: #632423;">:</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l1 level1 lfo5;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">ECONOMÍA Y ORGANIZACIÓN DE EMPRESAS. (4 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l1 level1 lfo5;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">GEOGRAFÍA. (4 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l1 level1 lfo5;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">MATEMÁTICAS APLICADAS A LAS CCSS II. (4 h)</span></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="text-decoration: underline;"><span style="color: #632423;">MATERIAS OPTATIVAS</span></span></strong></p>
<p class="MsoNormal" style="margin-left: 35.4pt;"><strong><span style="color: #632423;">A) MATERIAS PROPIAS DE MODALIDAD ENTRE LAS NO ELEGIDAS   (3 h)</span></strong></p>
<p class="MsoNormal" style="margin-left: 35.4pt;"><strong><span style="color: #632423;">B) MATERIAS OPTATIVAS  (3 h)</span></strong></p>
<p class="MsoNormal" style="margin-left: 35.4pt;"><strong><span style="color: #632423;">-------------------------------------------------------------------</span></strong></p>
<p class="MsoNormal" style="margin-left: 35.4pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">CIENCIAS Y TECNOLOGÍA (elegir una)</span></span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l5 level1 lfo6;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">SEGUNDO IDIOMA (FRANCÉS O INGLÉS). (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l5 level1 lfo6;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN FÍSICA. (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l5 level1 lfo6;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INFORMÁTICA. (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l5 level1 lfo6;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">ESTADÍSTICA. (3h)</span></p>
<p class="MsoNormal" style="margin-left: 35.4pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">HUMANIDADES Y CIENCIAS SOCIALES (elegir una)</span></span></p>
<p style="margin: 0cm 0cm 0.0001pt 106.8pt; text-indent: -85.5pt;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">     </span></span><!--[endif]--><span style="color: #632423;">SEGUNDO IDIOMA (FRANCÉS O INGLÉS). (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo7;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">     </span></span><span style="color: #632423;">EDUCACIÓN FÍSICA. (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo7;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">     </span></span><span style="color: #632423;">INFORMÁTICA. (3 h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo7;"><span style="text-indent: -85.5pt; color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">     </span></span><span style="text-indent: -85.5pt; color: #632423;">LITERATURA UNIVERSAL. (3h)</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo7;"><span style="text-indent: -85.5pt; color: #632423;"> </span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo7;"><span style="text-indent: -85.5pt; color: #632423;"> </span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo7;"> </p>
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt;" valign="top" width="576">
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-size: 18.0pt; color: #632423;">CURSO 2013-2014:</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-size: 18.0pt; color: #632423;">ASIGNATURAS OPTATIVAS Y DE LIBRE DISPOSICIÓN QUE SE PODRÁN ELEGIR EN EL INSTITUTO MIGUEL DE CERVANTES</span></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 3.5pt;" valign="top" width="576">
<p class="MsoNormal" style="text-align: center; mso-layout-grid-align: none; text-autospace: none;" align="center"><strong><span style="font-size: 14.0pt; color: #632423;">1º BACHILLERATO (Se elegirá un bloque de materias de Modalidad)</span></strong></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="text-align: center; text-indent: 35.4pt;" align="center"><strong><span style="text-decoration: underline;"><span style="font-size: 10.0pt; color: #632423;">MODALIDAD DE CIENCIAS Y TECNOLOGÍA</span></span></strong></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">OPCIÓN A:</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;">            MATEMÁTICAS + FÍSICA Y QUÍMICA + DIBUJO TÉCNICO.</span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">OPCIÓN B:</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;">            MATEMÁTICAS + FÍSICA Y QUÍMICA +BIOLOGÍA Y GEOLOGÍA.</span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">OPCIÓN C:</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;">            MATEMÁTICAS + FÍSICA Y QUÍMICA +TECNOLOGÍA INDUSTRIAL.</span></p>
<p class="MsoNormal" style="margin-left: 70.8pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 36.0pt; text-align: center; tab-stops: 36.0pt;" align="center"><strong><span style="text-decoration: underline;"><span style="font-size: 10.0pt; color: #632423;">MODALIDAD DE HUMANIDADES Y CIENCIAS SOCIALES</span></span></strong></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">OPCIÓN D:</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;">            HIST. MUNDO CONTEMP + GRIEGO I + LATÍN I.</span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">OPCIÓN E:</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;">            HIST. MUNDO CONTEMP. +ECONOMÍA + MATEM. APLICADAS CCSS.</span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<div align="center">
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 3.5pt;" valign="top" width="576">
<p class="MsoNormal" style="text-align: center;" align="center"><strong><span style="font-size: 14.0pt; color: #632423;">2º  BACHILLERATO</span></strong></p>
<p class="MsoNormal" style="text-align: center;" align="center"><strong><span style="font-size: 14.0pt; color: #632423;">(Se elegirá un itinerario de Modalidad, y una asignatura Optativa)</span></strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="text-align: center; text-indent: 35.4pt;" align="center"><strong><span style="text-decoration: underline;"><span style="font-size: 10.0pt; color: #632423;">MODALIDAD DE CIENCIAS Y TECNOLOGÍA</span></span></strong></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="color: #632423;">ITINERARIO A:</span></strong></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 54.0pt;"><span style="color: #632423;">MATEMÁTICAS II + FÍSICA + DIBUJO TÉCNICO II</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 54.0pt;"><span style="color: #632423;"> </span></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="color: #632423;">ITINERARIO B:</span></strong></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 54.0pt;"><span style="color: #632423;">MATEMÁTICAS II + QUÍMICA + BIOLOGÍA.</span></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="color: #632423;"> </span></strong></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="color: #632423;">OPTATIVA  (Se elegirá una)</span></strong></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l1 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">SEGUNDO IDIOMA (FRANCÉS O INGLÉS).</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l1 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN FÍSICA.</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l1 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INFORMÁTICA.</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l1 level1 lfo1;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">ESTADÍSTICA.</span></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="text-decoration: underline;"><span style="color: #632423;"> </span></span></strong></p>
<p class="MsoNormal" style="margin-left: 36.0pt; text-align: center; tab-stops: 36.0pt;" align="center"><strong><span style="text-decoration: underline;"><span style="font-size: 10.0pt; color: #632423;">MODALIDAD DE HUMANIDADES Y CIENCIAS SOCIALES</span></span></strong></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="color: #632423;">ITINERARIO C:</span></strong></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 54.0pt;"><span style="color: #632423;">HISTORIA DEL ARTE + GRIEGO II + LATÍN II.</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 54.0pt;"><span style="color: #632423;"> </span></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="color: #632423;">ITINERARIO D:</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;">            GEOGRAFÍA + ECON. Y ORG. EMPR. + MATEM. APLICADAS CCSS II.</span></p>
<p style="margin: 0cm; margin-bottom: .0001pt;"><strong><span style="color: #632423;"> </span></strong><strong><span style="color: #632423;">OPTATIVA  (Se elegirá una)</span></strong></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><span style="color: #632423;">SEGUNDO IDIOMA (FRANCÉS O INGLÉS).</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><span style="color: #632423;">EDUCACIÓN FÍSICA.</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><span style="color: #632423;">INFORMÁTICA.</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo2;"><span style="text-indent: -85.5pt; color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><span style="text-indent: -85.5pt; color: #632423;">LITERATURA UNIVERSAL.</span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo2;"><span style="text-indent: -85.5pt; color: #632423;"> </span></p>
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo2;"> </p>
<hr />
<p style="margin-top: 0cm; margin-right: 0cm; margin-bottom: .0001pt; margin-left: 106.8pt; text-indent: -85.5pt; mso-list: l0 level1 lfo2;"><span style="text-indent: -85.5pt; color: #632423;"> </span></p>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">IES Miguel de Cervantes- General</span> / <span class="art-post-metadata-category-name">Jefatura de Estudios</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:63:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Bachillerato";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:16:"Oferta Educativa";s:4:"link";s:59:"index.php?option=com_content&view=article&id=196&Itemid=599";}i:1;O:8:"stdClass":2:{s:4:"name";s:12:"Bachillerato";s:4:"link";s:59:"index.php?option=com_content&view=article&id=198&Itemid=602";}}s:6:"module";a:0:{}}
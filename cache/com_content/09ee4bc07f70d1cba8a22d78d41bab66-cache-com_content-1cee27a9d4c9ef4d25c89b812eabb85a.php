<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:5533:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">CURSO 2013/14</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Localización</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 05 Diciembre 2012 10:24</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=18:localizacion&amp;catid=43&amp;Itemid=196&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=4d551d8f711956afaa50ec828065f35712b36eec" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 7819
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.es/maps?q=google+maps+Camino%2Bde%2Blos%2Bneveros%2B18008%2BGranada&amp;ie=UTF8&amp;hq=&amp;hnear=Camino+de+los+Neveros,+18008+Granada,+Andaluc%C3%ADa&amp;gl=es&amp;ll=37.160659,-3.584204&amp;spn=0.023941,0.036478&amp;t=f&amp;z=14&amp;ecpose=37.14284462,-3.58420372,2689.57,0,44.982,0&amp;output=embed"></iframe><br /><small><a href="http://maps.google.es/maps?q=google+maps+Camino%2Bde%2Blos%2Bneveros%2B18008%2BGranada&amp;ie=UTF8&amp;hq=&amp;hnear=Camino+de+los+Neveros,+18008+Granada,+Andaluc%C3%ADa&amp;gl=es&amp;ll=37.160659,-3.584204&amp;spn=0.023941,0.036478&amp;t=f&amp;z=14&amp;ecpose=37.14284462,-3.58420372,2689.57,0,44.982,0&amp;source=embed" style="color:#0000FF;text-align:left" target="_blank">Ver mapa más grande</a></small><br /></span><span size="1" style="font-size: xx-small;"><br /></span></p>
<table cellpadding="5" cellspacing="5" border="0">
<tbody>
<tr>
<td cellpadding="5">
<div>
<p><span style="font-size: 10pt;"><span style="font-family: comic sans ms,sans-serif;">Situado en un <strong>barrio residencial</strong> de clase media-alta, el IES recibe alumnos tanto del propio barrio (Bola de Oro), como de algunos pueblos de la cercanía: Cenes de la Vega, Lancha de Cenes, Güéjar, Quéntar, Pinos Genil, así como de barrios cercanos: Camino Bajo de Huétor, Cervantes, Carretera de la Sierra, Zaidín y Realejo. Los alumnos de fuera de Granada acceden diariamente en autobuses que facilita gratuitamente la Consejería de Educación. </span></span></p>
<p><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">La <strong>situación física del edificio</strong>, sobre una pequeña colina colindante con el Serrallo, se eleva sobre el Zaidín y la ciudad de Granada, descansa junto al cambio de vertiente que al noreste se abre sobre el Genil. Esta altura le dota de </span><br /><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">vistas hacia el casco histórico y Sierra Nevada.</span></p>
<p><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">&nbsp;</span><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">La zona donde se ubica está bastante bien provista de i<strong>nfraestructuras educativas</strong>, ya que en sus cercanías podemos encontrar el IES Zaidín-Vergeles y el IES Pedro Soto de Rojas, así como algunos centros privados-concertados como el Colegio Sagrado Corazón o Salesianos. Esto supone una importante oferta educativa en una zona donde no falta la demanda, pero donde quizás no es suficiente para equilibrar su reparto entre los puestos escolares existentes y la </span><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">diversidad de opciones posibles.</span></p>
<p><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">El Instituto está <strong>bien comunicado</strong>. En una calle adyacente dispone de una parada del autobús urbano, concretamente de la línea 9, que comunica con el centro de la ciudad, al que también puede acercarse andando en unos diez minutos. La variedad </span><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;">de vías de circulación, no alteran –al tratarse de un barrio residencial- la tranquilidad necesaria para los fines educativos que se persiguen.</span></p>
<p><span style="font-family: 'comic sans ms', sans-serif; font-size: 10pt;"><br /></span></p>
<div><img src="images/stories/cervantes/_localizacion1.jpg" width="250" height="167" alt="_localizacion1" /></div>
</div>
</td>
</tr>
</tbody>
</table>
<div style="text-align: -webkit-center;"><span face="arial" style="font-family: arial;"><span style="font-size: 11px;"><br /></span></span></div></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:64:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Localización";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:6:"INICIO";s:4:"link";s:53:"index.php?option=com_content&view=featured&Itemid=250";}i:1;O:8:"stdClass":2:{s:4:"name";s:13:"Localización";s:4:"link";s:58:"index.php?option=com_content&view=article&id=18&Itemid=196";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:70785:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">E.S.O.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 06 Marzo 2014 18:26</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=197:eso&amp;catid=41&amp;Itemid=601&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=89dc6c62b62b69a83cfd68057197c2d78c0ae28b" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 29156
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Educación Secundaria Obligatoria</h2>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/13_14/Estructura_4ESO_IES_%20MdC_1314.pdf" target="_blank">Estructura de 4º ESO en pdf.</a></span></li>
<li><a href="archivos_ies/13_14/1314-OPTATIVAS.pdf" target="_blank"><span style="color: #2a2a2a;">Asignaturas optativas y de libre disposición en pdf.</span></a></li>
<li><a href="archivos_ies/13_14/ofertaeducativa-ESO-BACHILLERATO-1314.pdf" target="_blank"><span style="color: #2a2a2a;">Oferta educativa ESO y Bachillerato en pdf.</span></a></li>
</ul>
<p><span style="color: #2a2a2a;"> </span></p>
<div align="center">
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 307.35pt; border-color: windowtext; border-width: 1pt; padding: 0cm 3.5pt; text-align: center;" valign="top" width="276">
<p class="MsoNormal"><strong><span style="font-size: 14.0pt; color: #632423;">1º  E.S.O.  (30 horas semanales)</span></strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<h4><span style="mso-bidi-font-size: 12.0pt; color: #632423;"> </span><span style="color: #632423;">MATERIAS COMUNES (OBLIGATORIAS) (28 h)</span></h4>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<ol style="margin-top: 0cm;" type="1" start="1">
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">CIENCIAS DE LA NATURALEZA.<strong>(3 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">CIENCIAS SOCIALES, GEOGRAFÍA E HISTORIA. <strong>(3 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">EDUCACIÓN FÍSICA. <strong>(2 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">EDUCACIÓN PLÁSTICA Y VISUAL. <strong>(2 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">IDIOMA (INGLÉS O FRANCÉS). <strong>(4 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">LENGUA CASTELLANA Y LITERATURA. <strong>(5 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">MATEMÁTICAS. <strong>(5 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">MÚSICA. <strong>(2 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">RELIGIÓN O Hª Y CULTURA RELIGIONES, O ATENCIÓN EDUCATIVA (No evaluable). (<strong>1 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;">Tutoría. (<strong>1 h)</strong></li>
</ol>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">MATERIAS OPTATIVAS (y Libre Disposición)-(Se elegirá una de 2h)</span></strong></p>
<p class="MsoHeader"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 36.45pt; text-indent: -15.15pt; mso-list: l0 level2 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FRANCÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 36.45pt; text-indent: -15.15pt; mso-list: l0 level2 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INGLÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 36.45pt; text-indent: -15.15pt; mso-list: l0 level2 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TALLER DE LECTURA.</span> </p>
<p class="MsoNormal" style="margin-left: 36.45pt; text-indent: -15.15pt; mso-list: l0 level2 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TECNOLOGÍA APLICADA.</span></p>
<p> </p>
<div id="articlepxfontsize1" style="padding-left: 60px;">
<div align="center">
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 309.05pt; border-color: windowtext; border-width: 1pt; padding: 0cm 3.5pt;" valign="top" width="379">
<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size: 14.0pt; color: #632423;">2º E.S.O.  (30 horas semanales)</span></strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<h4><span style="mso-bidi-font-size: 12.0pt; color: #632423;">MATERIAS COMUNES (OBLIGATORIAS) (28 h)</span></h4>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><strong><span style="color: #632423;">1.<span style="font-weight: normal; font-size: 7pt; font-family: 'Times New Roman';">      </span></span></strong><!--[endif]--><span style="color: #632423;">CIENCIAS DE LA NATURALEZA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">CIENCIAS SOCIALES, GEOGRAFÍA E HISTORIA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN FÍSICA. <strong>(2 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN PLÁSTICA Y VISUAL. <strong>(2 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">5.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">IDIOMA (INGLÉS O FRANCÉS). <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">6.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">LENGUA CASTELLANA Y LITERATURA. <strong>(4 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">7.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">MATEMÁTICAS. <strong>(4 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">8.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">MÚSICA. <strong>(2 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">9.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">TECNOLOGÍA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">10.<span style="font-size: 7pt; font-family: 'Times New Roman';">  </span></span><!--[endif]--><span style="color: #632423;">RELIGIÓN O Hª Y CULTURA RELIGIONES, O ATENCIÓN EDUCATIVA (No evaluable). (<strong>1 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l1 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">11.<span style="font-size: 7pt; font-family: 'Times New Roman';">  </span></span><!--[endif]--><span style="color: #632423;">Tutoría. (<strong>1 h)</strong></span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">MATERIAS OPTATIVAS (y Libre Disposición)-(Se elegirá una de 2h)</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l0 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FRANCÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l0 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INGLÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l0 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TALLER DE LECTURA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l0 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l0 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><span style="color: #632423;"> </span></p>
<div align="center">
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 309.05pt; border-color: windowtext; border-width: 1pt; padding: 0cm 3.5pt;" valign="top" width="279">
<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size: 14.0pt; color: #632423;">3º E.S.O.  (30 horas semanales)</span></strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<h1><span style="mso-bidi-font-size: 12.0pt; color: #632423;"> </span></h1>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<h4><span style="mso-bidi-font-size: 12.0pt; color: #632423;">MATERIAS COMUNES (OBLIGATORIAS) (28 h)</span></h4>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><strong><span style="color: #632423;">1.<span style="font-weight: normal; font-size: 7pt; font-family: 'Times New Roman';">      </span></span></strong><!--[endif]--><span style="color: #632423;">CIENCIAS DE LA NATURALEZA. <strong>(4 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">CIENCIAS SOCIALES, GEOGRAFÍA E HISTORIA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN FÍSICA. <strong>(2 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN  PARA  LA  CIUDADANÍA. <strong>(1 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">5.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">IDIOMA (INGLÉS O FRANCÉS). <strong>(4 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">6.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">LENGUA CASTELLANA Y LITERATURA. <strong>(4 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">7.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">MATEMÁTICAS. <strong>(4 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">8.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">MÚSICA. <strong>(2 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">9.<span style="font-size: 7pt; font-family: 'Times New Roman';">      </span></span><!--[endif]--><span style="color: #632423;">TECNOLOGÍAS. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">10.<span style="font-size: 7pt; font-family: 'Times New Roman';">  </span></span><!--[endif]--><span style="color: #632423;">RELIGIÓN O Hª Y CULTURA RELIGIONES, O ATENCIÓN EDUCATIVA (No evaluable). (<strong>1 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 57.3pt; text-indent: -18.0pt; mso-list: l0 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">11.<span style="font-size: 7pt; font-family: 'Times New Roman';">  </span></span><!--[endif]--><span style="color: #632423;">Tutoría. (<strong>1 h)</strong></span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">MATERIAS OPTATIVAS (y Libre Disposición)-(Se elegirá una de 2h)</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FRANCÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INGLÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INFORMÁTICA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TÉCNICAS GRÁFICAS.</span></p>
<p class="MsoNormal" style="text-align: justify; mso-layout-grid-align: none; text-autospace: none;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<div align="center">
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 373.95pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt;" valign="top" width="499">
<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size: 14.0pt; color: #632423;">3º E.S.O.   DIVERSIFICACIÓN (30 horas semanales)</span></strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<h4><span style="mso-bidi-font-size: 12.0pt; color: #632423;">MATERIAS COMUNES (OBLIGATORIAS) (28 h)</span></h4>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">ÁMBITO CIENTÍFICO – TECNOLÓGICO. <strong>(8 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">ÁMBITO SOCIO – LINGUISTICO. <strong>(7 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN FÍSICA. <strong>(2 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN  PARA  LA  CIUDADANÍA. <strong>(1 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">5.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">IDIOMA (INGLÉS). <strong>(4 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">6.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TECNOLOGÍAS. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">7.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">RELIGIÓN O Hª Y CULTURA RELIGIONES, O ATENCIÓN EDUCATIVA (No evaluable). (<strong>1 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">8.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">Tutoría. (<strong>1 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">9.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">Tutoría de Orientación. (<strong>1 h)</strong></span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">MATERIAS OPTATIVAS (y Libre Disposición)-(Se elegirá una de 2h)</span></strong></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l2 level1 lfo4; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FRANCÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l2 level1 lfo4; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INGLÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l2 level1 lfo4; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INFORMÁTICA.</span></p>
<p class="MsoNormal" style="text-align: left;" align="right"><span style="font-size: 10pt; text-align: justify;"><span style="text-indent: -14.15pt; text-align: left; color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><span style="text-indent: -14.15pt; text-align: left; color: #632423;">TÉCNICAS GRÁFICAS.</span></span></p>
<p class="MsoNormal" style="text-align: left;" align="right"><span style="font-size: 10pt; text-align: justify;"> </span></p>
<div align="center">
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 294.9pt; border-color: windowtext; border-width: 1pt; padding: 0cm 3.5pt;" valign="top" width="260">
<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size: 14.0pt; color: #632423;">4º E.S.O.  (30 horas semanales)</span></strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<h4><span style="mso-bidi-font-size: 12.0pt; color: #632423;">MATERIAS COMUNES (OBLIGATORIAS) (21 h)</span></h4>
<p class="MsoNormal" style="margin-left: 35.45pt; mso-layout-grid-align: none; text-autospace: none;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">CIENCIAS SOCIALES, GEOGRAFÍA E HISTORIA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN ÉTICO-CÍVICA. <strong>(2 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN FÍSICA. <strong>(2 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">IDIOMA (INGLÉS O FRANCÉS). <strong>(4 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">5.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">LENGUA CASTELLANA Y LITERATURA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">6.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">MATEMÁTICAS- A o B (<strong>4 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">7.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">PROYECTO INTEGRADO. (<strong>1 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">8.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">RELIGIÓN O Hª Y CULTURA RELIGIONES, O ATENCIÓN EDUCATIVA (No evaluable). (<strong>1 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">9.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">Tutoría. (<strong>1 h)</strong></span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">MATERIAS DE MODALIDAD-OPCIONES.</span></strong></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">ITINERARIO A   (orientado al BHº Humanidades y Ciencias Sociales)</span></span></p>
<p class="MsoNormal" style="margin-left: 24.55pt;"><span style="color: #632423;">    (MATEMÁTICAS-A. (<strong>4 h</strong>))</span></p>
<p class="MsoNormal"><span style="color: #632423;">            --------------</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN PLÁTICA.<strong> (3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">LATÍN (materia vinculada) <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">MÚSICA.<strong> (3 h)</strong></span></p>
<p class="MsoNormal"><span style="color: #632423;">            ---------------</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l0 level1 lfo2;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">SEGUNDO IDIOMA (FRANCÉS O INGLÉS). <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;"> </span></span></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">ITINERARIO B   (orientado al BHº de Ciencias y Tecnología)</span></span></p>
<p class="MsoNormal" style="margin-left: 24.55pt;"><span style="color: #632423;">    (MATEMÁTICAS-B. (<strong>4 h</strong>))</span></p>
<p class="MsoNormal"><span style="color: #632423;">            --------------</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">BIOLOGÍA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FÍSICA Y QUÍMICA (materia vinculada).<strong> (3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INFORMÁTICA. (<strong>3 h</strong>)</span></p>
<p class="MsoNormal"><span style="color: #632423;">            ---------------</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo3;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">SEGUNDO IDIOMA (FRANCÉS O INGLÉS). <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;"> </span></span></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">ITINERARIO C   (orientado al BHº Artístico, Ciclos Form. y Vida Laboral)</span></span></p>
<p class="MsoNormal" style="margin-left: 24.55pt;"><span style="color: #632423;">    (MATEMÁTICAS-A. (<strong>4 h</strong>))</span></p>
<p class="MsoNormal"><span style="color: #632423;">            --------------</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l4 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">BIOLOGÍA. (<strong>3 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l4 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN PLÁSTICA.<strong> (3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l4 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TECNOLOGÍA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal"><span style="color: #632423;">            ---------------</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l4 level1 lfo4;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">SEGUNDO IDIOMA (FRANCÉS O INGLÉS). <strong>(3 h)</strong></span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="text-decoration: underline;"><span style="color: #632423;">ITINERARIO D   (orientado al BHº Artístico, Ciclos Form. y Vida Laboral)</span></span></p>
<p class="MsoNormal" style="margin-left: 24.55pt;"><span style="color: #632423;">    (MATEMÁTICAS-B. (<strong>4 h</strong>))</span></p>
<p class="MsoNormal"><span style="color: #632423;">            --------------</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l2 level1 lfo5;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FÍSICA Y QUÍMICA. (<strong>3 h</strong>)</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l2 level1 lfo5;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN PLÁSTICA.<strong> (3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l2 level1 lfo5;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TECNOLOGÍA. <strong>(3 h)</strong></span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt;"><span style="color: #632423;">     ---------------</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l2 level1 lfo5;"><!--[if !supportLists]--><strong><span style="color: #632423;">4.<span style="font-weight: normal; font-size: 7pt; font-family: 'Times New Roman';">    </span></span></strong><!--[endif]--><span style="color: #632423;">SEGUNDO IDIOMA (FRANCÉS O INGLÉS). <strong>(3 h)</strong></span></p>
<strong><span style="font-size: 12.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: #632423; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA;"><br style="mso-special-character: line-break; page-break-before: always;" clear="all" /></span></strong>
<p class="MsoNormal"><strong><span style="color: #632423;">PROYECTO INTEGRADO DE CARÁCTER PRÁCTICO   (1 h)</span></strong></p>
<p class="MsoNormal" style="text-align: justify;"><span style="color: #632423;">            Esta asignatura es de carácter obligatorio para todos los alumnos que cursan  4º de E.S.O. y consiste en un trabajo de investigación de una hora semanal, sobre los siguientes temas propuestos:</span></p>
<p class="MsoNormal" style="text-align: justify;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">Itinerario A</span></strong><span style="color: #632423;"> (orientado al Bachillerato de Humanidades y Ciencias Sociales)</span></p>
<ol style="margin-top: 0cm;" type="1" start="1">
<li class="MsoNormal" style="color: #632423; mso-list: l0 level1 lfo1;">Aprender a razonar I (Filosofía) / Deporte y Salud I (Ed. Física) / Taller de Prensa I (Lengua C.)</li>
</ol>
<p class="MsoNormal"><strong><span style="color: #632423;">Itinerario B</span></strong><span style="color: #632423;"> (orientado al Bachillerato de Ciencias y Tecnología)</span></p>
<ol style="margin-top: 0cm;" type="1" start="2">
<li class="MsoNormal" style="color: #632423; mso-list: l0 level1 lfo1;">Aprender a razonar I  (Filosofía) / Deporte y Salud I  (Ed. Física) / Taller de Prensa I  (Lengua C.) / Genética y Salud I  (Bio / Geo)</li>
</ol>
<p class="MsoNormal"><strong><span style="color: #632423;">Itinerario C</span></strong><span style="color: #632423;"> (orientado al Bachillerato Artístico, Ciclos Formativos y Vida Laboral)</span></p>
<ol style="margin-top: 0cm;" type="1" start="3">
<li class="MsoNormal" style="color: #632423; mso-list: l0 level1 lfo1;">Aprender a razonar I (Filosofía) / Deporte y Salud I (Ed. Física) / Taller de Prensa I (Lengua C.)</li>
</ol>
<p class="MsoNormal" style="margin-left: 36.0pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 36.0pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 36.0pt;"><span style="color: #632423;">-------------------------------------------------------------------------------------------------</span></p>
<p class="MsoNormal" style="margin-left: 36.0pt;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<div align="center">
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 365.4pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt;" valign="top" width="487">
<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size: 14.0pt; color: #632423;">4º E.S.O. DIVERSIFICACIÓN(30 horas semanales)</span></strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<h4><span style="mso-bidi-font-size: 12.0pt; color: #632423;">MATERIAS COMUNES (OBLIGATORIAS) (23 h)</span></h4>
<ol style="margin-top: 0cm;" type="1" start="1">
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;">ÁMBITO CIENTÍFICO – TECNOLÓGICO. <strong>(7 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;">ÁMBITO SOCIO – LINGUISTICO. <strong>(8 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;">EDUCACIÓN  ÉTICO – CÍVICA. <strong>(2 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;">EDUCACIÓN FÍSICA. <strong>(2 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;">IDOMA (INGLÉS). <strong>(4 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;">RELIGIÓN O Hª Y CULTURA RELIGIONES, O ATENCIÓN EDUCATIVA (No evaluable). (<strong>1 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;">Tutoría. (<strong>1 h)</strong></li>
<li class="MsoNormal" style="color: #632423; mso-list: l1 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;">Tutoría de Orientación. (<strong>1 h)</strong></li>
</ol>
<p class="MsoNormal" style="margin-left: 36.0pt; mso-layout-grid-align: none; text-autospace: none;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 70.8pt; mso-layout-grid-align: none; text-autospace: none;"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">MATERIAS OPTATIVAS (y Libre Disposición)-(Se elegirá una de 3h)</span></strong></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN PLÁSTICA</span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INFORMÁTICA.</span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TECNOLOGÍA.</span></p>
<h1><span style="mso-bidi-font-size: 12.0pt; color: #632423; mso-bidi-font-weight: normal;"> </span></h1>
<p class="MsoNormal" style="text-align: left;" align="right"><span style="font-size: 10pt; text-align: justify;"> </span></p>
<strong style="text-align: center;"><span style="text-decoration: underline;"><span style="font-size: 18.0pt;" lang="ES-TRAD">ESTRUCTURA DE 4º E.S.O. CURSO 2013/14</span></span></strong><strong style="text-align: center;"><span style="text-decoration: underline;"><span style="font-size: 18.0pt;" lang="ES-TRAD">.</span></span></strong><br />
<p class="MsoNormal"><strong><span style="text-decoration: underline;"><span style="font-size: 10.0pt;" lang="ES-TRAD"> </span></span></strong></p>
<strong style="color: #414141;"><span style="font-size: 10.0pt;" lang="ES-TRAD">A. Materias:</span></strong><br />
<p class="MsoListParagraphCxSpFirst" style="margin-left: 2cm; text-indent: -21.25pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;" lang="ES-TRAD">·<span style="font-size: 7pt; font-family: 'Times New Roman';">           </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">CSGH (Ciencias Sociales, Geografía e Historia).</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;" lang="ES-TRAD">·<span style="font-size: 7pt; font-family: 'Times New Roman';">           </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">EEC (Educación Ético-Cívica).</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;" lang="ES-TRAD">·<span style="font-size: 7pt; font-family: 'Times New Roman';">           </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">EF (Educación Física).</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;" lang="ES-TRAD">·<span style="font-size: 7pt; font-family: 'Times New Roman';">           </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">IDIO (Idioma).</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;" lang="ES-TRAD">·<span style="font-size: 7pt; font-family: 'Times New Roman';">           </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">LCL (Lengua Castellana y Literatura).</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;" lang="ES-TRAD">·<span style="font-size: 7pt; font-family: 'Times New Roman';">           </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">PRO (Proyecto Integrado).</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;" lang="ES-TRAD">·<span style="font-size: 7pt; font-family: 'Times New Roman';">           </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">REL o ATED (Religión o Atención Educativa).</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; font-family: Symbol; mso-fareast-font-family: Symbol; mso-bidi-font-family: Symbol;" lang="ES-TRAD">·<span style="font-size: 7pt; font-family: 'Times New Roman';">           </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">(Tutoría)</span></p>
<p class="MsoListParagraphCxSpMiddle"><span style="font-size: 10.0pt;" lang="ES-TRAD"> </span></p>
<p class="MsoListParagraphCxSpMiddle" style="text-indent: -18pt;"><!--[if !supportLists]--><strong><span style="font-size: 10.0pt; mso-fareast-font-family: 'Times New Roman';" lang="ES-TRAD">B.<span style="font-weight: normal; font-size: 7pt; font-family: 'Times New Roman';">      </span></span></strong><!--[endif]--><strong><span style="font-size: 10.0pt;" lang="ES-TRAD">Itinerarios (IE Miguel de Cervantes. Curso 2013-2014):</span></strong></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt; background-color: yellow; background-position: initial initial; background-repeat: initial initial;"><!--[if !supportLists]--><strong><span style="font-size: 10.0pt; mso-fareast-font-family: 'Times New Roman';" lang="ES-TRAD">a.<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><span style="font-size: 10.0pt; mso-ansi-language: EN-US;" lang="EN-US">MAT-A        + EPV    + <span style="text-decoration: underline;">MUS</span>   + LAT                    →           Bach. </span><span style="font-size: 10.0pt;" lang="ES-TRAD">HyCS</span></strong></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt; background-color: yellow; background-position: initial initial; background-repeat: initial initial;"><strong><!--[if !supportLists]--><span style="font-size: 10.0pt; mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-US;" lang="EN-US">b.<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><span style="font-size: 10.0pt; mso-ansi-language: EN-US;" lang="EN-US">MAT-B        + BYG   + FYQ    + <span style="text-decoration: underline;">INF</span>                     →           Bach. CyT</span></strong></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt; background-color: yellow; background-position: initial initial; background-repeat: initial initial;"><strong><!--[if !supportLists]--><span style="font-size: 10.0pt; mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-US;" lang="EN-US">c.<span style="font-size: 7pt; font-family: 'Times New Roman';">          </span></span><!--[endif]--><span style="font-size: 10.0pt; mso-ansi-language: EN-US;" lang="EN-US">MAT-B        + EPV    + FYQ    + <span style="text-decoration: underline;">TEC</span>                    →           Bach. Art</span></strong></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt; background-color: yellow; background-position: initial initial; background-repeat: initial initial;"><strong><!--[if !supportLists]--><span style="font-size: 10.0pt; mso-fareast-font-family: 'Times New Roman'; mso-ansi-language: EN-US;" lang="EN-US">d.<span style="font-size: 7pt; font-family: 'Times New Roman';">         </span></span><!--[endif]--><span style="font-size: 10.0pt; mso-ansi-language: EN-US;" lang="EN-US">MAT-A        + BYG   + EPV    + <span style="text-decoration: underline;">TEC</span>                    →           Bach. Art</span></strong></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 35.45pt;"><strong><span style="font-size: 10.0pt; mso-ansi-language: EN-US;" lang="EN-US">                               </span><span style="font-size: 10.0pt;" lang="ES-TRAD">(<span style="text-decoration: underline;">2º IDIO</span>)</span></strong></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 2cm; text-indent: -21.25pt; background-color: yellow; background-position: initial initial; background-repeat: initial initial;"><strong><!--[if !supportLists]--><span style="font-size: 10.0pt; mso-fareast-font-family: 'Times New Roman';" lang="ES-TRAD">e.<span style="font-size: 7pt; font-family: 'Times New Roman';">          </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">DC                </span><span style="font-size: 10.0pt; mso-ansi-language: EN-US;" lang="EN-US">                + EPV</span></strong></p>
<p class="MsoListParagraphCxSpMiddle"><span style="font-size: 10.0pt;" lang="ES-TRAD"> </span></p>
<p class="MsoListParagraphCxSpMiddle"><span style="font-size: 10.0pt;" lang="ES-TRAD"><strong>Leyenda:</strong></span></p>
<p class="MsoListParagraphCxSpLast" style="margin-left: 60.55pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; mso-fareast-font-family: 'Times New Roman';" lang="ES-TRAD">-<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><span style="font-size: 10.0pt;" lang="ES-TRAD">MAT-A: Matemáticas A; MAT-B: Matemáticas B; EPV: Educación Plástica y Visual; </span><span style="font-size: 10pt;">MUS: Música; LAT: Latín; BYG: Biología y Geología; INF: Informática; FYQ: Física y Química; TEC: Tecnología; 2º IDIO: 2º Idioma; DC: Diversificación Curricular.</span></p>
<p class="MsoListParagraphCxSpFirst" style="margin-left: 60.55pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; mso-fareast-font-family: 'Times New Roman';" lang="ES-TRAD">-<span style="font-size: 7pt; font-family: 'Times New Roman';">       </span></span><span style="font-size: 10.0pt;" lang="ES-TRAD">Las asignaturas subrayadas son las que el alumno o alumna podía cambiar por el 2º Idioma.</span></p>
<p class="MsoListParagraphCxSpLast" style="margin-left: 60.55pt; text-indent: -18pt;"><!--[if !supportLists]--><span style="font-size: 10.0pt; mso-fareast-font-family: 'Times New Roman';" lang="ES-TRAD">-<span style="font-size: 7pt; font-family: 'Times New Roman';">          </span></span><!--[endif]--><span style="font-size: 10.0pt;" lang="ES-TRAD">En DC, tienen todos los alumnos EPV, aunque se les había ofertado, bien INF, o bien TEC.</span></p>
<p class="MsoNormal"><span style="font-size: 10.0pt;" lang="ES-TRAD"> </span></p>
<p class="MsoNormal" style="margin-left: 35.25pt;"><span style="font-size: 10.0pt;" lang="ES-TRAD"><strong>Itinerarios por grupos de 4º ESO:</strong></span></p>
<p class="MsoListParagraphCxSpFirst" style="margin-left: 53.25pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo4; background: #F2DBDB;"><!--[if !supportLists]--><span style="font-size: 10.0pt; line-height: 115%; mso-fareast-font-family: 'Times New Roman';" lang="ES-TRAD">-<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">          </span></span><!--[endif]--><span style="font-size: 10.0pt; line-height: 115%;" lang="ES-TRAD">El itinerario “a”, se corresponde con el itinerario del grupo 4º ESO-A.</span></p>
<p class="MsoListParagraphCxSpFirst" style="margin-left: 53.25pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo4; background: #F2DBDB;"><span style="text-indent: -18pt; font-size: 10pt; line-height: 115%;" lang="ES-TRAD">-<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">          </span></span><span style="text-indent: -18pt; font-size: 10pt; line-height: 115%;" lang="ES-TRAD">El itinerario “b”, se corresponde con el itinerario del grupo 4º ESO-B.</span></p>
<p class="MsoListParagraphCxSpMiddle" style="margin-left: 53.25pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo4; background: #F2DBDB;"><!--[if !supportLists]--><span style="font-size: 10.0pt; line-height: 115%; mso-fareast-font-family: 'Times New Roman';" lang="ES-TRAD">-<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">          </span></span><!--[endif]--><span style="font-size: 10.0pt; line-height: 115%;" lang="ES-TRAD">Los itinerarios “c” y “e”, se corresponden con los itinerarios del grupo 4º ESO-C.</span></p>
<p class="MsoListParagraphCxSpLast" style="margin-left: 53.25pt; mso-add-space: auto; text-indent: -18.0pt; mso-list: l0 level1 lfo4; background: #F2DBDB;"><!--[if !supportLists]--><span style="font-size: 10.0pt; line-height: 115%; mso-fareast-font-family: 'Times New Roman';" lang="ES-TRAD">-<span style="font-size: 7pt; line-height: normal; font-family: 'Times New Roman';">          </span></span><!--[endif]--><span style="font-size: 10.0pt; line-height: 115%;" lang="ES-TRAD">El itinerario “d”, se corresponde con el itinerario del grupo 4º ESO-D.</span></p>
<p class="MsoNormal"><span style="font-size: 10.0pt; line-height: 115%;" lang="ES-TRAD"> </span></p>
<p class="MsoNormal"><span style="font-size: 10.0pt; line-height: 115%;" lang="ES-TRAD"> </span></p>
<p class="MsoNormal"> </p>
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt;" valign="top" width="576">
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-size: 18.0pt; color: #632423;">CURSO 2013-2014:</span></p>
<p class="MsoNormal" style="text-align: center;" align="center"><span style="font-size: 18.0pt; color: #632423;">ASIGNATURAS OPTATIVAS Y DE LIBRE DISPOSICIÓN QUE SE PODRÁN ELEGIR EN EL INSTITUTO MIGUEL DE CERVANTES</span></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<div align="center">
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 429.95pt; border-color: windowtext; border-width: 1pt; padding: 0cm 3.5pt;" valign="top" width="573">
<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size: 14.0pt; color: #632423;">1º  E.S.O.  (Se elegirá una asignatura de 2 horas a la semana)</span></strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<p class="MsoNormal" style="margin-left: 36.45pt; text-indent: -15.15pt; mso-list: l0 level2 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FRANCÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 36.45pt; text-indent: -15.15pt; mso-list: l0 level2 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INGLÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 36.45pt; text-indent: -15.15pt; mso-list: l0 level2 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TALLER DE LECTURA.</span></p>
<p class="MsoNormal" style="margin-left: 36.45pt; text-indent: -15.15pt; mso-list: l0 level2 lfo1; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TECNOLOGÍA APLICADA.</span></p>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt;" valign="top" width="576">
<p class="MsoNormal"><strong><span style="font-size: 14.0pt; color: #632423;">2º E.S.O.  (Se elegirá una asignatura de 2 horas a la semana)</span></strong></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><strong><span style="color: #632423;"> </span></strong></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo4; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FRANCÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo4; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INGLÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l1 level1 lfo4; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TALLER DE LECTURA.</span></p>
<p class="MsoNormal" style="text-align: justify; mso-layout-grid-align: none; text-autospace: none;"><strong><span style="color: #632423;"> </span></strong></p>
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt;" valign="top" width="576">
<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size: 14.0pt; color: #632423;">3º E.S.O.  (Se elegirá una asignatura de 2 horas a la semana)</span></strong></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">FRANCÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INGLÉS-2º IDIOMA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INFORMÁTICA.</span></p>
<p class="MsoNormal" style="margin-left: 2.0cm; text-indent: -14.15pt; mso-list: l3 level1 lfo2; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">4.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TÉCNICAS GRÁFICAS.</span></p>
<p class="MsoNormal" style="text-align: justify; mso-layout-grid-align: none; text-autospace: none;"><span style="color: #632423;"> </span></p>
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt;" valign="top" width="576">
<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size: 14.0pt; color: #632423;">4º E.S.O.  (Se elegirá un itinerario de tres asignaturas)</span></strong></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><span style="font-size: 10.0pt; color: #632423;">Notas:</span></p>
<p class="MsoNormal"><span style="font-size: 10.0pt; color: #632423;">- Una de las asignaturas del itinerario elegido, podrá sustituirse por el Segundo Idioma (Francés o Inglés).</span></p>
<p class="MsoNormal"><span style="font-size: 10.0pt; color: #632423;">- Cada itinerario elegido, lleva asociado unas Matemáticas (A o B).</span></p>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">ITINERARIO A:</span></strong></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="color: #632423;">MAT-A + E PLÁSTICA + LATÍN + MÚSICA  (+ 2º IDIOMA)</span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">ITINERARIO B:</span></strong></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="color: #632423;">MAT-B + BIOLOG.+ FÍS. Y QUÍM. + INFORMÁT.  (+ 2º IDIOMA).</span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">ITINERARIO C:</span></strong></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="color: #632423;">MAT-A + BIOLOG.+ E. PLÁSTICA + TECNOLOGÍA  (+ 2º IDIOMA).</span></p>
<p class="MsoNormal"><strong><span style="color: #632423;">ITINERARIO D:</span></strong></p>
<p class="MsoNormal" style="text-indent: 35.4pt;"><span style="color: #632423;">MAT-B + FÍS. Y QUÍM.+ E. PLÁSTICA + TECNOLOGÍA  (+ 2º IDIOMA).</span></p>
<p class="MsoNormal" style="text-align: justify; mso-layout-grid-align: none; text-autospace: none;"><span style="color: #632423;"> </span></p>
<table class="MsoNormalTable" style="border: none;" border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="width: 432.2pt; border-color: windowtext; border-width: 1pt; padding: 0cm 5.4pt;" valign="top" width="576">
<p class="MsoNormal" style="text-align: center;"><strong><span style="font-size: 14.0pt; color: #632423;">4º E.S.O.- DICU (Se elegirá una asignatura de 3 horas a la semana)</span></strong></p>
</td>
</tr>
</tbody>
</table>
<p class="MsoNormal"><span style="color: #632423;"> </span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">1.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">EDUCACIÓN PLÁSTICA</span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">2.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">INFORMÁTICA.</span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; text-indent: -14.15pt; mso-list: l2 level1 lfo3; mso-layout-grid-align: none; text-autospace: none;"><!--[if !supportLists]--><span style="color: #632423;">3.<span style="font-size: 7pt; font-family: 'Times New Roman';">    </span></span><!--[endif]--><span style="color: #632423;">TECNOLOGÍA.</span></p>
<p><span style="font-size: 12.0pt; font-family: 'Times New Roman','serif'; mso-fareast-font-family: 'Times New Roman'; color: #632423; mso-ansi-language: ES; mso-fareast-language: ES; mso-bidi-language: AR-SA;"><br style="mso-special-character: line-break; page-break-before: always;" clear="all" /> </span></p>
<p class="MsoNormal" style="margin-left: 35.45pt; mso-layout-grid-align: none; text-autospace: none;"><span style="color: #632423;"> </span></p>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">IES Miguel de Cervantes- General</span> / <span class="art-post-metadata-category-name">Organización</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:57:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - E.S.O.";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:16:"Oferta Educativa";s:4:"link";s:59:"index.php?option=com_content&view=article&id=196&Itemid=599";}i:1;O:8:"stdClass":2:{s:4:"name";s:6:"E.S.O.";s:4:"link";s:59:"index.php?option=com_content&view=article&id=197&Itemid=601";}}s:6:"module";a:0:{}}
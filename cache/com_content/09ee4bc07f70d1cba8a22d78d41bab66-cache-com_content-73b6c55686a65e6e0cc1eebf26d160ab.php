<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:7173:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Técnicas de estudio</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 22 Septiembre 2015 18:22</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=77:do-emilia-vilchez-campillos&amp;catid=97&amp;Itemid=563&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=ff07f6e9aac00cc8e91b425e56e416a548ad4a3e" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 7982
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1><span style="text-decoration: underline;"><span style="font-family: arial black,avant garde; font-size: 14pt;"><strong>EDUCACIÓN PLÁSTICA Y VISUAL.</strong></span></span></h1>
<h3><span style="text-decoration: underline;"><strong><span style="color: #2a2a2a; text-decoration: underline;">2º ESO</span></strong> </span></h3>
<ol>
<li><a href="archivos_ies/14_15/dibujo/UD1OIE2.pdf" target="_blank" title="Tema 1.">Tema 1.<br /></a></li>
<li><a href="archivos_ies/14_15/dibujo/UD2OIE2.pdf" target="_blank" title="Tema 2.">Tema 2.</a><span style="color: #2a2a2a;"> </span></li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T2_lam1_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T2_lam2_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;"><span style="color: #414141;">Lámina 2.</span></span></a></li>
</ol>
<li><a href="archivos_ies/14_15/dibujo/Uni_3.pdf" title="Tema 3."><span style="color: #2f310d;">Tema </span>3.</a></li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T3_lam1_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T3_lam2_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;"><span style="color: #414141;">Lámina 2.</span></span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T3_lam3_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 3.</span></a></li>
</ol>
<li><a href="archivos_ies/14_15/dibujo/Unidad_4_2_eso.pdf" target="_blank">Tema 4.</a><span style="color: #414141;"><br /></span></li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T4_lam1_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T4_lam2_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 2.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T4_lam3_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 3.</span></a></li>
</ol>
<li><a href="archivos_ies/14_15/dibujo/Tema5_2eso.pdf" target="_blank">Tema 5.</a><span><br /></span></li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T5_lam1_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T5_lam2_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 2.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T5_lam3_2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 3.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T5_lam4_2eso.jpg" target="_blank"><span style="color: #414141;"><span style="color: #414141;"><span style="color: #2f310d;">Lámina </span>4.</span></span></a></li>
</ol>
<li><a href="archivos_ies/14_15/dibujo/tema-6-2-eso.pdf" target="_blank">Tema 6.</a> (07/01/15)</li>
<ol>
<li><a href="archivos_ies/14_15/dibujo/T6-lm1.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 1.</span></a></li>
<li><a href="archivos_ies/14_15/dibujo/T6lam2-2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #414141;">Lámina 2.</span></a></li>
</ol>
<li><a href="archivos_ies/15_16/dibujo/Tema7nuevo.pdf" target="_blank">Tema 7.</a> (22/09/15)</li>
</ol><ol><ol>
<li><span style="color: #414141;"><a href="archivos_ies/14_15/dibujo/T7-lm1.jpg" target="_blank" style="text-decoration: none; color: #2f310d;">Lámina 1.</a> (23/02/15)</span></li>
<li><span style="color: #414141;"><a href="archivos_ies/14_15/dibujo/T7lam2-2eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;">Lámina 2.</a></span> (23/02/15)</li>
<li><span style="color: #414141;"><a href="archivos_ies/14_15/dibujo/T7Lam3-3eso.jpg" target="_blank" style="text-decoration: none; color: #2f310d;">Lámina 3.</a></span> (23/02/15)</li>
</ol></ol>
<p style="padding-left: 30px;"><span style="color: #414141;">8. <a href="archivos_ies/14_15/dibujo/T8.pdf" target="_blank">Tema 8.</a> (06/04/15)</span></p>
<ol><ol>
<li><span style="background-color: initial;"><a href="archivos_ies/14_15/dibujo/T8_lam1_2eso.jpg" target="_blank">Lámina 1.</a>  (14/04/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T8_lam2_2eso.jpg" target="_blank">Lámina 2.</a> (14/04/15)</li>
</ol></ol>
<p style="padding-left: 30px;"><span style="color: #414141;">9. <a href="archivos_ies/14_15/dibujo/T9_EP_2_ESO.pdf" target="_blank">Tema 9.</a> (03/05/15)</span></p>
<ol>
<li><span style="background-color: initial;"><a href="archivos_ies/14_15/dibujo/T9_lam1_2eso.jpg" target="_blank">Lámina 1.</a>  (03/05/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T9-lam2-2eso.jpg" target="_blank">Lámina 2.</a> (03/05/15)</li>
<li><a href="archivos_ies/14_15/dibujo/T9_lam3_2eso.jpg" target="_blank">Lámina 3.</a> (03/05/15)</li>
</ol><hr />
<ul>
<li><span style="color: #2a2a2a;">3º ESO</span></li>
<li><span style="color: #2a2a2a;">RECUPERACIÓN DE PENDIENTES</span></li>
</ul></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:80:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Dª Emilia Vílchez Campillos";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:6:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:21:"Dpto. de Orientación";s:4:"link";s:59:"index.php?option=com_content&view=article&id=104&Itemid=574";}i:2;O:8:"stdClass":2:{s:4:"name";s:20:"Técnicas de estudio";s:4:"link";s:59:"index.php?option=com_content&view=article&id=140&Itemid=134";}i:3;O:8:"stdClass":2:{s:4:"name";s:13:"Departamentos";s:4:"link";s:60:"index.php?option=com_content&view=category&id=195&Itemid=134";}i:4;O:8:"stdClass":2:{s:4:"name";s:20:"Educación Plástica";s:4:"link";s:59:"index.php?option=com_content&view=category&id=97&Itemid=134";}i:5;O:8:"stdClass":2:{s:4:"name";s:29:"Dª Emilia Vílchez Campillos";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
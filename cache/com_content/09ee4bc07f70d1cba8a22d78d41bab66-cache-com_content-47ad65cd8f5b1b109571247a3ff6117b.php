<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:24995:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Programaciones</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 06 Noviembre 2015 18:43</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=195:programaciones&amp;catid=100&amp;Itemid=666&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=ee46c6b6ac83bb260ae9dae98e45ea74775792ec" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 33495
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 style="text-align: center;"><span style="font-size: 16pt;"><strong><span style="text-decoration: underline;"><br />Programaciones ABREVIADAS, criterios de evaluación, recuperaciones. </span></strong></span></h2>
<h2 style="text-align: center;"><span style="font-size: 16pt;"><strong><span style="text-decoration: underline;">curso 2015/16</span></strong><br /></span></h2>
<h3><span style="text-decoration: underline;"><strong>BIOLOGÍA - GEOLOGÍA 15/16</strong></span></h3>
<ul style="text-align: justify;">
<li><a href="archivos_ies/15_16/programaciones/BG_Programaciones_2015-2016-resumida.pdf" target="_blank"><span style="color: #2a2a2a;">Resumen de las programaciones del Departamento. </span></a><span style="color: #2a2a2a;">(03/11/15)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;"><strong>CICLO DE ANIMACIÓN</strong></strong></span><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;"><strong><br /></strong></strong></p>
<ul>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Progr_OTL_14-15_EMT.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN OTL 14-15 EMT</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Prog_PROYECTO_14_15_EMT.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN PROYECTO 14-15 EMT</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Prog_Modulo_FCT_2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN MÓDULO FCT 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_SSA.doc" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN SSA 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_FCT.docx" target="_blank" style="text-decoration: none; color: #2f310d; font-size: 13px; text-transform: none;">ProgramacióN FCT 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_DCO.docx" target="_blank" style="text-decoration: none; color: #2f310d;">ProgramacióN DCO 14-15</a></span><span style="color: #7b7b7b;"> </span><span style="color: #7b7b7b;"><span style="color: #414141;">(20/10/14)</span></span></li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Andres_MIS.docx" target="_blank" style="text-decoration: none; color: #2f310d;">ProgramacióN MIS 14-15</a> </span>(20/10/14)</li>
<li><span style="color: #7b7b7b;"><a href="archivos_ies/14_15/programaciones/Prgrogramacion_Departamento_ASC.docx" target="_blank" style="text-decoration: none; color: #2f310d;">PROGRAMACIÓN DEP. ASC 14-15</a> (20/10/14)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">EDUCACIÓN FÍSICA 15/16<br /></strong></span></p>
<ul>
<li><a href="archivos_ies/15_16/ef/EF_1ESO.doc" target="_blank">Cuadernillo programación 1º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_2ESO.doc" target="_blank">Cuadernillo programación 2º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_3ESO.doc" target="_blank">Cuadernillo programación 3º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_4ESO.doc" target="_blank">Cuadernillo programación 4º ESO.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_1_Bachillerato.doc" target="_blank">Cuadernillo programación 1º BACH.</a> (06/10/15)</li>
<li><a href="archivos_ies/15_16/ef/EF_2_Bachillerato.doc" target="_blank">Cuadernillo programación 2º BACH.</a> (06/10/15)</li>
</ul>
<h3><span style="text-decoration: underline;"><strong>EDUCACIÓN PLÁSTICA:</strong></span></h3>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/15_16/programaciones/">Programación reducida.</a> (13/10/14)<br /></span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/programaciones/ProgramacionDT2_Bach_2014-2015.pdf" target="_blank">Programación de Dibujo Técnico 2º Bachillerato.</a> (23/10/14)<br /><br /></span></li>
</ul>
<p><span style="line-height: 1.25em;"> </span><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">FILOSOFÍA</strong></span></p>
<ul>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/FIL_RESUMEN_CRITERIOS_EV_Y_CALIF.pdf" target="_blank">Programación</a> (03/11/15)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>FÍSICA QUÍMICA.</strong></span></h3>
<ul>
<li><a href="archivos_ies/15_16/programaciones/FQ_RESUMEN_3_ESO.pdf" target="_blank">Resumen de la programación de FQ 3º ESO.</a> <a href="archivos_ies/criterios_evaluacion/Cuadernillo_FQ_3_ESO.pdf" target="_blank" title="Criterios de evaluación de FQ 3º ESO"><span style="color: #414141; letter-spacing: normal; line-height: 16.25px; text-align: left;">(03/11/15)</span></a></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_RESUMEN_4_ESO.pdf" target="_blank">Resumen de la programación de FQ 4º ESO.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_RESUMEN_1_Bach.pdf" target="_blank">Resumen de la programación de FQ 1º Bach.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_FIS_RESUMEN_2_Bach.pdf" target="_blank">Resumen de la programación de Física 2º Bach.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FQ_QU_RESUMEN_2_Bach.pdf" target="_blank">Resumen de la programación de Química 2º Bach.</a> <span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase;"><span style="text-decoration: underline;">FRANCÉS</span></strong></p>
<ul>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_ESO.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º ESO</span><span><span style="letter-spacing: 1px; line-height: 16.25px;"><span>.</span></span></span></a><span style="line-height: 16.25px;"> (06/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen2_ESO.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 2º ESO</span><span><span style="letter-spacing: 1px; line-height: 16.25px;"><span>.</span></span></span></a><span style="line-height: 16.25px;"> (06/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen3_ESO_FR2.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 3º ESO FR2</span><span><span style="letter-spacing: 1px; line-height: 16.25px;"><span>.</span></span></span></a><span style="line-height: 16.25px;"> (06/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen3_ESOPrimerLenguaExtranj.pdf" target="_blank">Resumen programación 3º ESO Primera Lengua Extranjera.</a> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen4_ESO.pdf" target="_blank">Resumen programación 4º ESO.</a> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachFR2NAvance.pdf" target="_blank"><span style="line-height: 16.25px;">Resumen programación 1º Bach. </span><span style="color: #8e9326;"><span style="letter-spacing: 1px; line-height: 16.25px;"><span style="text-decoration: underline;">FR2 N. Avance.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachFR2NDebut.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º Bach. F</span><span style="color: #8e9326;"><span style="text-decoration: underline;"><span style="line-height: 16.25px;">R2 N. Debut.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachFR2NIntermed.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º Bach. </span><span><span><span style="line-height: 16.25px;">FR2 N. Intermed.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen1_BachLenguaExtranI.pdf" target="_blank"><span style="color: #2f310d; text-decoration: none; line-height: 16.25px;">Resumen programación 1º Bach. </span><span style="color: #8e9326;"><span style="letter-spacing: 1px; line-height: 16.25px;"><span style="text-decoration: underline;">Lengua Extranera 1.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen2_BachFR2.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="text-decoration: none; color: #2f310d; line-height: 16.25px;">Resumen programación 2º Bach. 2ª </span><span style="text-decoration: none; color: #8e9326;"><span style="line-height: 16.25px;"><span style="text-decoration: underline;">Lengua Extranera</span></span></span></a><span style="color: #8e9326;"><span style="text-decoration: underline;">.</span></span><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/FR_ProgramResumen2_BachLenguaExtranII.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="line-height: 16.25px;">Resumen programación 2º Bach. </span><span style="color: #8e9326;"><span style="line-height: 16.25px;"><span style="text-decoration: underline;">Lengua Extranera 2.</span></span></span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
</ul>
<div style="text-align: justify;">
<h3><strong><span style="text-decoration: underline;">GEOGRAFÍA E HISTORIA.<br /></span></strong></h3>
<ol>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/15_16/programaciones/Historia_PRIMERO_DE_ESO.docx" target="_blank">Programación 1º ESO</a>.</span><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/SEGUNDO_DE_ESO.pdf" target="_blank"><span style="color: #2a2a2a;">Programación 2º ESO.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Historia_TERCERO_DE_ESO.docx" target="_blank"><span style="color: #2a2a2a;">Programación 3º ESO.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Historia_CUARTO_DE_ESO.pdf" target="_blank"><span style="color: #2a2a2a;">Programación 4º ESO.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/HISTORIA_MUNDO_CONTEMPORANEO.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><span style="color: #2a2a2a;">Programación Hª del mundo Contemporáneo 1º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/GEOGRAFIA.pdf" target="_blank"><span style="color: #2a2a2a;">Programación Geografía 2º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/HISTORIA_DE_ESPANA.pdf" target="_blank"><span style="color: #2a2a2a;">Programación Hª de España 2º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Programacion_H_Arte_2Bach1516.pdf" target="_blank"><span style="color: #2a2a2a;">Programación Arte 2º Bach.</span></a><span style="line-height: 16.25px;"> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/Programacion_didactica_CEE.docx" target="_blank">Programación Cultura Emprendedora y Empresarial 1º Bach.</a> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/PROGRAMACION_ECONOMIA%20.docx" target="_blank" style="text-decoration: none; color: #2f310d;">Programación Economía 1º Bach.</a> (03/11/15)</span></li>
<li><span style="line-height: 16.25px;"><a href="archivos_ies/15_16/programaciones/PROGRAMACION_DIDACTICA_ECE%20.docx" target="_blank">Programación Economía de la Empresa 2º Bach.</a> (03/11/15)</span></li>
</ol></div>
<p><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase;"><span style="text-decoration: underline;">INGLÉS</span></strong></p>
<ul>
<li><a href="archivos_ies/14_15/programaciones/Programacion_ingles_2014-2015.pdf" target="_blank" title="Programación de inglés, 14/15." style="font-family: Verdana, Arial, Helvetica, sans-serif; text-decoration: none; color: #336600; font-size: 11px;">Programación de Inglés, curso 2014/15 </a><span style="line-height: 1.25em;">(02/10/14)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-transform: uppercase; text-decoration: underline;">LATÍN Y GRIEGO</strong> </span></p>
<ul>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/programacion_reducida_latin.pdf" target="_blank">Programación de Latín</a>. </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/programacion_reducida_griego.pdf" target="_blank">Programación de Griego.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-transform: uppercase; text-decoration: underline;">LENGUA ESPAÑOLA</strong></span></p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/programaciones/programacion_Lengua_curso_2014-2015.pdf/archivos_ies/15_16/programaciones/LE_ABREVIADA%202015-16.docx" target="_blank">Programación abreviada de Lengua</a> <span style="line-height: 16.25px;">(03/11/15)</span><br /><br /></li>
</ul>
</ul>
<p> <strong style="color: #7b7b7b; font-size: 20px; text-align: left; text-transform: uppercase; text-decoration: underline;">M<strong>ATEMÁ</strong>TICAS.</strong></p>
<ul style="text-align: justify;">
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_ESO.doc" target="_blank">Resumen de  la programación 1º ESO</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_ESO.doc">Resumen de  la programación. 2<span style="color: #423c33;"><span style="color: #2f310d;">º ESO</span></span></a></span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_3_ESO.doc" target="_blank">Resumen de  la programación. 3<span style="color: #423c33;"><span style="color: #2f310d;">º ESO</span></span></a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_4_ESO.doc" target="_blank"> Resumen de  la programación. 4<span style="color: #423c33;"><span style="color: #2f310d;">º ESO</span></span></a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_padres_ESO_BILIN_15_16.docx" target="_blank">Resumen de  la programación. BILINGÜES</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_Y_T.doc" target="_blank">Resumen de  la programación. </a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_Y_T.doc" target="_blank">1º BACHILLERATO C. Y T.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_SOC.doc" target="_blank">Resumen de  la programación. </a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_1_BACHILL_C_SOC.doc" target="_blank">1º BACHILLERATO C. SOC.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_II.doc" target="_blank">Resumen de  la programación. 2</a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_II.doc" target="_blank">º BACHILLERATO MAT II</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_CC_SS_II..doc" target="_blank">Resumen de  la programación. 2</a></span><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_2_BACHILL_M_CC_SS_II..doc" target="_blank">º BACHILLERATO C. SOC.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="color: #423c33;"> <a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_padres_ESTAD_2BACH_15_16.docx" target="_blank">Resumen de  la programación. 2</a></span><span style="color: #423c33;"><span style="color: #423c33;"><a href="archivos_ies/15_16/programaciones/mat/Cuadernillo_padres_ESTAD_2BACH_15_16.docx" target="_blank">º BACHILLERATO ESTADÍSTICA</a> </span></span><strong style="color: #2b2721;"><span style="color: #423c33;"><span style="color: #414141; font-weight: normal; line-height: 16.25px;">(03/11/15)</span></span></strong></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">MÚSICA:</strong></span></p>
<ul style="text-align: justify;">
<li style="text-align: justify;"><span style="color: #8e9326;"><span style="letter-spacing: 1px;"><a href="archivos_ies/15_16/programaciones/MUS_Objetivos_Contenidos_Criterios_Ev1516.pdf" target="_blank">Objetivos, Contenidos y Criterios de Evaluación_2015/16</a> <strong><span style="color: #414141; letter-spacing: normal; text-align: left;"> </span></strong></span></span><span style="line-height: 16.25px; text-align: left;">(03/11/15)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>PLAN DE IGUALDAD</strong></span><span style="color: #2a2a2a; font-size: 13px; font-weight: normal;"> </span></h3>
<ul style="text-align: justify;">
<li><span style="line-height: 1.25em;"><a href="archivos_ies/14_15/programaciones/PROYECTO_PLAN_DE_IGUALDAD_14-15_Jennifer_Valero.pdf" target="_blank">Programación</a> (20/11/14)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>PROGRAMACIÓN BILINGÜE</strong></span><span style="color: #2a2a2a; font-size: 13px; font-weight: normal;"> </span></h3>
<ul style="text-align: justify;">
<li><span style="line-height: 1.25em;"><a href="archivos_ies/14_15/programaciones/Programacion_bilingue_2014-2015b.docx" target="_blank">Programación</a> (13/10/14)</span></li>
</ul>
<h3><span style="text-decoration: underline;"><strong>RELIGIÓN</strong></span></h3>
<ul style="text-align: justify;">
<li><span style="line-height: 1.25em;"><a href=":\Users\Manuel\Documents\0Cervantes15-16\Programaciones/Rel_PROGR_2015_2016.pdf" target="_blank">Programación</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p><span style="text-decoration: underline;"><strong style="color: #7b7b7b; font-size: 20px; text-transform: uppercase; text-decoration: underline;">Tecnología</strong></span></p>
<ul>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologias_2_eso.pdf" target="_blank">Criterios de evaluación Tecnologías 2º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologias_3_eso.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Criterios de evaluación Tecnologías 3º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologia_4_eso.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Criterios de evaluación Tecnologías 4º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologia_1_bach.pdf" target="_blank">Criterios de evaluación Tecnologías 1º Bach.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/criterios_evaluacion_tecnologia_2_bach.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">Criterios de evaluación Tecnologías 2º Bach.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><a href="archivos_ies/15_16/programaciones/Criterios_evaluacion_3_4_optativa_Informatica_15-16.pdf" target="_blank">Criterios de evaluación Informática 3º y 4º ESO.</a><span style="line-height: 1.25em;"> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/Criterios_evaluacion_3_4_optativa_Informatica_15-16.pdf" target="_blank">PROGRAMACIÓN DIDÁCTICA DE INFORMÁTICA 3º Y 4º ESO.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
<li><span style="line-height: 1.25em;"><a href="archivos_ies/15_16/programaciones/Programacion_TICBachillerato_15-16.pdf" target="_blank">PROGRAMACIÓN DIDÁCTICA TIC BACHILLERATO.</a> </span><span style="line-height: 16.25px;">(03/11/15)</span></li>
</ul>
<p> </p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">DEPARTAMENTOS</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:65:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - PROGRAMACIONES";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:14:"PROGRAMACIONES";s:4:"link";s:59:"index.php?option=com_content&view=article&id=195&Itemid=666";}}s:6:"module";a:0:{}}
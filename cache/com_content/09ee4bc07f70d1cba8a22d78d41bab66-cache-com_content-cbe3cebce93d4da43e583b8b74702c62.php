<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:47110:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Dpto. de Filosofía</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">BECAS</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 29 Enero 2015 12:35</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=275:becas&amp;catid=143&amp;Itemid=716&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=7cebb0cefb7790461dac0109669f68ceb955a83e" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1682
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="titulo_detalle" style="margin-top: 0px; margin-bottom: 0px; font-size: 15.6000003814697px; font-weight: bold; text-align: justify; color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; text-transform: none; background-color: #ffffff;"><span style="text-decoration: underline;"><strong> </strong></span></h2>
<h2 class="titulo_detalle" style="margin-top: 0px; margin-bottom: 0px; font-size: 15.6000003814697px; font-weight: bold; text-align: justify; color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; text-transform: none; background-color: #ffffff;"><span style="text-decoration: underline;"><strong>ESTADO ACTUAL DE LAS CONVOCATORIAS DE BECAS</strong></span></h2>
<ul>
<li><a href="archivos_ies/14_15/becas_del_27_1_al_9_2_2015.pdf" target="_blank"><strong>Becas y ayudas: </strong><span style="color: #2a2a2a;">Quincena del 27.01.2015 al 09.02.2015.</span></a></li>
</ul>
<div class="separacionCapas" style="margin: 0px; padding: 0px; color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px; line-height: 15px; height: 15px !important; background-color: #ffffff;"> </div>
<div class="contenido_detalle" style="margin: 0px; padding: 0px; text-align: justify; overflow: hidden; float: left; width: 590.421875px; color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px; line-height: 15px; background-color: #ffffff;">
<div class="detalle_html" style="margin: 0px; padding: 0px; overflow: hidden;">
<div style="margin: 0px; padding: 0px;"><strong>CURSO 2014-2015:</strong></div>
<ul style="margin-right: 1em; margin-left: 20px; list-style: disc outside; font-size: 11.8181819915771px;">
<li>Convocatoria de becas para enseñanzas postobligatorias no universitarias de carácter general: <span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes para estudiantes no universitarios </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">30 de septiembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación.</span></li>
<li>Convocatoria de Ayudas para el alumnado con Necesidad Específica de Apoyo Educativo:<span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">30 de septiembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación.</span></li>
<li>BECA 6000: <span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">30 de septiembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación.</span></li>
</ul>
<ul style="margin-right: 1em; margin-left: 20px; list-style: disc outside; font-size: 11.8181819915771px;">
<li>Beca Andalucía Segunda Oportunidad:<span style="font-size: 11.8181819915771px;"> </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">30 de septiembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación</span><span style="font-size: 11.8181819915771px;">.</span></li>
<li><strong>Beca Adriano</strong>: <span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">8 de noviembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación</span>.</li>
</ul>
<div style="margin: 0px; padding: 0px;"> </div>
<div style="margin: 0px; padding: 0px;"><strong>CURSO 2013-2014:</strong></div>
<ul style="margin-right: 1em; margin-left: 20px; list-style: disc outside;">
<li>Ayudas Individualizadas para el Transportes Escolar: <span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">finalizado </span><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el plazo de presentación de solicitudes </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">el</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;"> </span><strong style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">8 de noviembre de 2014</strong><span style="color: #3d3d3d; font-family: Arial, Helvetica, Verdana, sans-serif; font-size: 12px;">, los expedientes se encuentran en fase de grabación y baremación</span>.</li>
</ul>
</div>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">ÚLTIMA NORMATIVA</span> / <span class="art-post-metadata-category-name">NOVEDADES</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">LOMCE</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 13 Marzo 2015 16:52</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=274:lomce&amp;catid=143&amp;Itemid=109&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=cbb2f777f7691ec7c5b07e014812bfa7f04d52fb" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 19690
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><span style="color: #222222; font-family: arial, sans-serif;"><span style="color: #222222; font-family: arial, sans-serif; font-weight: normal; text-align: start;"><span style="text-decoration: underline;"><strong>ÚLTIMA HORA: NORMATIVA ESTATAL.</strong></span><br /><a href="index.php?option=com_content&amp;view=article&amp;id=274" target="_blank">BOE: RD 1105/2014, establece currÍculum básico de ESO y Bachillerato, adaptado a la LOMCE.</a><br /><a href="archivos_ies/14_15/BOE-A-2015-738.pdf" target="_blank">BOE: <span style="color: #000000; font-family: Helvetica, Arial, sans-serif, Verdana; font-size: 13.2800006866455px; line-height: 16.6000003814697px;">Orden ECD/65/2015, se describen las relaciones entre las competencias, los contenidos y los criterios de evaluación de EP, ESO y Bachillerato.</span></a><br />Falta que la </span><span style="color: #222222; font-family: arial, sans-serif; font-weight: normal; text-align: left;">legislación andaluza se adapte a este RD. VER -&gt;  </span><a href="http://www.adideandalucia.es/normas/proyectos/BorradorInstruccionesSecundaria_feb2015.pdf" target="_blank" style="font-size: 13px; color: #005fa9;">Proyecto de INSTRUCCIONES</a><span style="font-size: 13px; text-align: left; color: #414141;"> </span><span style="font-size: 13px; text-align: left; color: #414141;">de la Secretaría General de Educación de la Consejería de Educación, Cultura y Deporte, sobre la ordenación educativa de la Educación Secundaria Obligatoria y Bachillerato y otras consideraciones para el curso escolar 2015-2016 (Borrador de 16/02/2015).</span></span></p>
<p><span style="color: #222222; font-family: arial, sans-serif;"><span style="font-size: 13px; text-align: left; color: #414141;">_______________________________________________________</span></span></p>
<p><span style="color: #222222; font-family: arial, sans-serif;"> </span></p>
<p><span style="color: #222222; font-family: arial, sans-serif;">El sábado 3 de enero se publicó en el BOE el Real Decreto 1105/2014, que establece el curriculum básico de la ESO y del Bachillerato, adaptado a la LOMCE. Es de suponer que la legislación andaluza se adapte a este RD, aunque no se sabe cuándo se publicará esta legislación autonómica.</span></p>
<div style="color: #222222; font-family: arial, sans-serif;"><span style="text-decoration: underline;"><strong>Algunos comentarios de carácter general:</strong></span></div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- Se redefinen las competencias básicas en el artículo 2. Las nuevas competencias son:</div>
<div style="color: #222222; font-family: arial, sans-serif;"><ol>
<li style="margin-left: 15px;">Comunicación lingüística.</li>
<li style="margin-left: 15px;">Competencia matemática y competencias básicas en ciencia y tecnología.</li>
<li style="margin-left: 15px;">Competencia digital.</li>
<li style="margin-left: 15px;">Aprender a aprender.</li>
<li style="margin-left: 15px;">Competencias sociales y cívicas.</li>
<li style="margin-left: 15px;">Sentido de iniciativa y espíritu emprendedor.</li>
<li style="margin-left: 15px;">Conciencia y expresiones culturales.</li>
</ol></div>
<div style="color: #222222; font-family: arial, sans-serif;">- La estructura de las materias por nivel es la que ya establecía la LOMCE, así por ejemplo se habla de asignaturas troncales, específicas y de libre configuración. En el anexo I del RD se recoge el curriculum básico de las materias troncales, en el anexo II el de las materias específicas.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- Se definen como elementos transversales, que se trabajarán en todas las materias la compresión lectora, la expresión oral, la expresión escrita, la comunicación audiovisual y las TIC.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- El capítulo II trata de la ESO, con la organización de materias de la LOMCE, especifado en los artículos 13 y 14 de este RD. Son importantes los artículos 21 y 22, que tratan de la famosa reválida de la ESO y de los mecanismos de promoción respectivamente. Hay una novedad interesante: Los alumnos repetirán curso cuando suspendan más de dos materias o si suspenden Lengua y Matemáticas simultáneamente. Como ya sucedía los alumnos sólo pueden repetir el mismo curso una vez, y pueden repetir dos veces en la ESO. La edad límite de permanencia en ESO es de 19 años cumplidos dentro del curso académico.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- El capítulo III trata sobre el Bachillerato, cuya estructura desgrana en los artículos 26,27,28. El artículo 31 define la reválida para la obtención del título de Bachillerato. En cuanto a la promoción, teniendo en cuenta que hay un plazo de cuatro años para hacer los dos cursos, sólo puede repetirse una vez cada nivel salvo casos excepcionales que determinará el equipo educativo. En cuanto a la continuidad entre materias, para cursar determinadas materias de 2º será necesario haber impartido la correspondiente de 1º (p.e. para hacer Matemáticas II tiene que haber cursado Matemáticas I), si bien el profesorado que imparte la materia de 2º podrá permitir saltarse esta norma si el alumno tiene posibilidades de hacer la asignatura con aprovechamiento.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- No hay novedades significativas en lo tocante a documentos oficiales de evaluación (actas, expedientes académicos etc.).</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">- Quedan derogados los Reales Decretos 1631/2006 (de la ESO) y 1467/2007 (del Bachillerato).</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;"><span style="color: #ff0000;">CALENDARIO DE IMPLANTACIÓN:</span></div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">CURSO 2015/2016:     1º Y 3º DE ESO. 1º DE BACHILLERATO.</div>
<div style="color: #222222; font-family: arial, sans-serif;"><span style="color: #ff0000;">_____________________________________________________________________________________________</span></div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">CURSO 2016/2017:     2º Y 4º DE ESO. 2º DE BACHILLERATO.</div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 </div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 PRIMERA REVÁLIDA DE ESO. SIN EFECTOS ACADÉMICOS.</div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 </div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 PRIMERA REVÁLIDA DE BACHILLERATO. CON EFECTOS PARA EL INGRESO A LA UNIVERSIDAD,</div>
<div style="color: #222222; font-family: arial, sans-serif;">                                 PERO SIN EFECTOS PARA LA OBTENCIÓN DEL TÍTULO DE BACHILLER.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;">ENTRADA EN VIGOR DEL REAL DECRETO: A PARTIR DEL 3 DE ENERO DE 2015.</div>
<div style="color: #222222; font-family: arial, sans-serif;"> </div>
<div style="color: #222222; font-family: arial, sans-serif;"><a href="archivos_ies/14_15/RD1105-2014CurriculoSecundaria.pdf" target="_blank">VER EL REAL DECRETO ÍNTEGRO --&gt;<br /><br /><br /></a>___________________________________________________</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">ÚLTIMA NORMATIVA</span> / <span class="art-post-metadata-category-name">NOVEDADES</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Biblioweb</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 27 Octubre 2014 11:38</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=269:biblioweb&amp;catid=143&amp;Itemid=109&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=e4a52ecbedee8acc01b2ad6f682b5368a162704f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 11101
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p> Nueva web de consulta de los libros de la biblioteca del Instituto:</p>
<div class="tronco" style="font-family: Metrophobic, Arial, Helvetica, sans-serif; margin: 0px; padding: 0px; position: relative; color: #000000; font-size: 15px;">
<div id="principal" style="margin: auto; padding: 0px 0px 20px; width: 960px; font-size: 14px;">
<div id="ColumnaIzq" style="margin: 50px 18px 28px 0px; padding: 0px; width: 729.59375px; float: left; position: relative; border-top-left-radius: 16px; border-top-right-radius: 16px; border-bottom-right-radius: 16px; border-bottom-left-radius: 16px; box-shadow: #4b5a59 0px 0px 8px; background: #ffffff;">
<div id="Cabecera" style="margin: 0px; padding: 20px; width: 729.59375px; box-shadow: none; border: none; background: transparent;">
<div class="cuadro" style="margin: auto; padding: 0px; width: 656.625px;">
<div class="cuadroTitulos" style="margin: 6px 0px 0px; padding: 0px; width: 459.625px; height: 100px; float: left;">
<div class="titulo" style="margin: 0px; padding: 0px 0px 10px 12px; width: 459.625px; font-size: 1.8em; font-variant: small-caps; text-align: center; position: relative; text-shadow: rgba(0, 0, 0, 0.498039) 1px 1px 0px; font-weight: bold; top: -10px;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb.">Centro IES Miguel de Cervantes</a></div>
<div class="titulo2" style="margin: 0px; padding: 0px 0px 0px 12px; width: 459.625px; font-size: 1.08em; font-variant: small-caps; text-align: center; vertical-align: middle; outline: none; position: relative; color: #444444; top: -15px;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."> </a></div>
</div>
</div>
</div>
<div id="cssmenu" style="margin: 0px; padding: 100px 20px 15px; width: 729.59375px; position: relative;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><span style="color: #964b4b;"><span style="font-size: 1.32em; border-left-color: #9cbcb8; border-left-style: solid; border-left-width: 6px; padding: 2px 4px; margin-right: 16px; border-bottom-color: #9cbcb8; border-bottom-width: 2px; border-bottom-style: solid;"><strong>Búsqueda</strong></span></span> <span style="color: #964b4b;"><span style="font-size: 1.32em; border-left-color: #9cbcb8; border-left-style: solid; border-left-width: 6px; padding: 2px 4px; margin-right: 16px;"><strong>Información</strong></span></span> <span style="color: #964b4b;"><span style="font-size: 1.32em; border-left-color: #9cbcb8; border-left-style: solid; border-left-width: 6px; padding: 2px 4px; margin-right: 16px;"><strong>Tablón</strong></span></span> <span style="color: #7b2f35;"><span style="font-size: 1.32em; border-left-color: #deb8bb; border-left-style: solid; border-left-width: 6px; padding: 2px 4px; margin-right: 16px;"><strong>Administración</strong></span></span><span style="float: right; margin-top: -1px; margin-right: 56px;"><img src="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/archivos/pag_mas.png" border="0" style="width: 18px;" /></span><span style="opacity: 0.4; float: right; margin-top: 2px;"><img src="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/archivos/pag_menos.png" border="0" style="width: 15px;" /></span></a></div>
<div class="cuadro3" style="margin: 0px; padding: 0px 0px 40px; box-shadow: none; position: relative; background: transparent;">
<div class="cabeceraCuadro2" style="margin: 0px 0px 40px -10.9375px; padding: 12px 0px 0px; width: 751.46875px; position: relative; font-size: 1.16em; color: #ffffff; text-align: center; text-shadow: rgba(0, 0, 0, 0.498039) 1px 1px 0px; font-weight: bold; height: 30px; box-shadow: #4b5a59 0px 4px 8px; border-top-left-radius: 4px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; background: #964b4b;">
<div style="margin: 0px; padding: 0px;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb.">BUSCADOR</a></div>
</div>
<div class="cuadro" style="margin: auto; padding: 0px; width: 656.625px;"><form style="margin: 0px; padding: 0px;" action="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/Resultado.php" method="GET">
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_principal" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 1px; font-weight: bold; color: #ffffff; text-shadow: #000000 0px 0px 4px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #262826;" for="nombre1">Busqueda Global</label><input id="nombre1" class="input input_principal" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #000000;" type="text" name="busqueda" /></a></div>
<div class="separador" style="margin: 10px 0px 20px; padding: 0px; box-shadow: #4b5a59 0px 1px 2px; height: 2px; overflow: hidden; border-top-left-radius: 4px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; width: 656.625px; background: #9dadab;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."> </a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro " style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="titulo">Título</label><input id="titulo" class="input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="titulo" /></a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="autor">Autor</label><input id="autor" class="input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="autor" /></a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="editorial">Editorial</label><input id="editorial" class="input ac_input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="editorial" /></a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="coleccion">Colección</label><input id="coleccion" class="input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="coleccion" /></a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="isbn">Código barras/ISBN</label><input id="isbn" class="input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="isbn" /></a></div>
<div class="separador" style="margin: 10px 0px 20px; padding: 0px; box-shadow: #4b5a59 0px 1px 2px; height: 2px; overflow: hidden; border-top-left-radius: 4px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; width: 656.625px; background: #9dadab;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."> </a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="descriptores">Descriptor</label><input id="descriptores" class="input ac_input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="descriptores" /></a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="aplicaciones">Aplicaciones</label><input id="aplicaciones" class="input ac_input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="aplicaciones" /></a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="cdu">CDU</label><input id="cdu" class="input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="cdu" /></a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="ubicacion">Ubicación</label><input id="ubicacion" class="input ac_input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="ubicacion" /></a></div>
<div class="hidden" style="margin: 0px; padding: 0px; overflow: hidden;"><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><label class="busqueda_filtro" style="width: 183.84375px; overflow: hidden; display: block; float: left; font-size: 1.16em; padding-left: 13.125px; margin-bottom: 10px; padding-top: 1px; padding-bottom: 0px; color: #333333; text-shadow: #cccccc 1px 1px; border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; background: #cbcfd8;" for="tipofondo">Tipo documento</label><input id="tipofondo" class="input ac_input" style="margin: 0px 0px 10px; padding-top: 0px; padding-bottom: 0px; padding-left: 13.125px; width: 459.625px; border-right-color: #888888; border-bottom-color: #888888; border-left-color: #888888; border-style: none solid solid; font-size: 1.16em; float: left; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 4px; border-bottom-left-radius: 0px; color: #777777;" type="text" name="tipofondo" /></a></div>
<a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/biblioweb/mod/Busqueda/" target="_blank" title="Biblioweb."><input class="submit" style="margin: 20px auto auto; padding: 4px 0px; width: 105.046875px; border-style: solid; border-color: #4b5a59; display: block; font-size: 1.08em; font-weight: bold; box-shadow: #666666 0px 0px 16px; color: #333333; text-shadow: #ffffff 0px 0px 4px; border-top-left-radius: 4px; border-top-right-radius: 4px; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; background: #9dadab;" type="submit" value="Buscar" /></a></form></div>
</div>
</div>
</div>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">ÚLTIMA NORMATIVA</span> / <span class="art-post-metadata-category-name">NOVEDADES</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Selectividad Septiembre 2014</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 16 Septiembre 2014 17:24</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=254:selectividad-septiembre-2014&amp;catid=143&amp;Itemid=109&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=b7963a994088db33f2f4d18f51273a8ea4628fdc" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 14560
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p>&nbsp;</p>
<div style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; font-size: 14px; vertical-align: baseline; background-color: #ffffff; color: #313131; font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif; font-weight: normal; line-height: 25.59375px; text-transform: none;">
<div class="cajacomentario" style="box-sizing: border-box; margin: 2px 5px 20px; padding: 5px; border: 2px solid #cccccc; outline: 0px; vertical-align: baseline; border-top-left-radius: 8px; border-top-right-radius: 0px; border-bottom-right-radius: 8px; border-bottom-left-radius: 8px; min-height: 68px;">
<h4 style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent; text-align: center;"><span style="text-decoration: underline;"><strong style="background-color: transparent; text-align: center;">SELECTIVIDAD SEPTIEMBRE 2014&nbsp;</strong></span></h4>
<div style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent; text-align: center;"><a href="archivos_ies/14_15/SELECTIVIDAD_SEPTIEMBRE_2014.pdf" target="_blank" title="Abrir pdf con la información."><strong style="background-color: transparent; text-align: center;"><img style="border: 0;" src="archivos_ies/14_15/selectividad_sept_2014.jpg" alt="" width="449" height="749" border="0" /></strong></a></div>
<div class="col_1 mb mt" style="box-sizing: border-box; margin-right: 0px; margin-left: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent; display: inline-block; float: left; width: 791.5px; margin-top: 10px !important; margin-bottom: 10px !important;">
<div id="spamcomentario48171" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-color: transparent;"><a style="box-sizing: border-box; text-decoration: none; color: #6666ff; display: inline-block; outline: none;" title="Marcar como spam">&nbsp;</a></div>
</div>
</div>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">ÚLTIMA NORMATIVA</span> / <span class="art-post-metadata-category-name">NOVEDADES</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                            <div class="item column-2">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 07 Febrero 2012 19:51</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=52:pizarra-digital&amp;catid=143&amp;Itemid=237&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=4a48fd980a1ae4cabfea8f33437a207aeff9c0f3" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3267
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p style="text-align: justify; padding-left: 30px;"><span style="font-size: 14pt;"><strong>*************************************************</strong></span></p>
<p style="text-align: justify; padding-left: 120px;"><span style="font-size: 14pt;"><strong><span style="text-decoration: underline;">CURSO SOBRE PIZARRAS DIGITALES</span></strong></span></p>
<p style="text-align: justify; padding-left: 30px;"><span style="font-size: 14pt;"><strong>*************************************************</strong></span></p>
<ul>
<li><a target="_blank" href="archivos_ies/pizarra_digital/Inspirate_con_ActivInspire.pdf" title="Pizarra digital, manual de usuario.">Manual de usuario del programa ActiveInspire.</a>&nbsp;<ol>
<li><a target="_blank" href="archivos_ies/pizarra_digital/Resumen_Activinspire_capitulos_1_10_B.pdf">Primer resumen capítulos 1 al 10.</a></li>
<li><a target="_blank" href="archivos_ies/pizarra_digital/Practicas_ActivInspire.pdf">15 prácticas.</a></li>
<li><a target="_blank" href="archivos_ies/pizarra_digital/Tecnicas_ActivInspire.pdf">15 técnicas.</a></li>
<li><a target="_blank" href="archivos_ies/pizarra_digital/Fichas_ActivInspire.pdf">Resumen del significado de los iconos en fichas.</a></li>
</ol></li>
<li><a target="_blank" href="archivos_ies/pizarra_digital/Instalacion_pizarra_digital.pdf">Instalación de la pizarra digital.</a></li>
<li><a target="_blank" href="http://www.youtube.com/watch?v=jLpJbOttwWY&amp;feature=player_embedded">Climograma hecho por alumnos.</a></li>
<li><a target="_blank" href="archivos_ies/pizarra_digital/curso-lesson-activity-toolkit.pdf">Uso del software de la pizarra (Notebook)</a></li>
</ul>
<p><span style="text-decoration: underline; font-size: 12pt;"><strong>Enlaces a páginas webs relacionadas con las pizarras digitales:</strong></span></p>
<ul>
<li><a href="http://www.delicious.com/escuelatic20/pdi">http://www.delicious.com/escuelatic20/pdi</a>&nbsp;</li>
</ul></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
            </div>";s:4:"head";a:10:{s:5:"title";s:70:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Dpto. de Filosofía";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:102:"/index.php?option=com_content&amp;view=category&amp;id=143&amp;Itemid=109&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:103:"/index.php?option=com_content&amp;view=category&amp;id=143&amp;Itemid=109&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:6:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:25:"Área Socio-Lingüística";s:4:"link";s:59:"index.php?option=com_content&view=article&id=186&Itemid=572";}i:2;O:8:"stdClass":2:{s:4:"name";s:19:"Dpto. de Filosofía";s:4:"link";s:59:"index.php?option=com_content&view=article&id=166&Itemid=109";}i:3;O:8:"stdClass":2:{s:4:"name";s:9:"Normativa";s:4:"link";s:60:"index.php?option=com_content&view=category&id=196&Itemid=109";}i:4;O:8:"stdClass":2:{s:4:"name";s:17:"ÚLTIMA NORMATIVA";s:4:"link";s:60:"index.php?option=com_content&view=category&id=104&Itemid=109";}i:5;O:8:"stdClass":2:{s:4:"name";s:9:"NOVEDADES";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
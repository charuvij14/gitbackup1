<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6795:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Artículos</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Zona del Dibujo Técnico 2º Bachillerato</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 28 Octubre 2013 10:57</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=78:educacion-plastica&amp;catid=97&amp;Itemid=143&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=a6ef8601b1d78c9accdffab646b6245e103d88d3" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 4900
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h3 style="text-align: center;"><span style="text-decoration: underline;"><strong>DIBUJO TÉCNICO 2013-14.</strong></span></h3>
<ul style="text-align: justify;">
<li><span style="color: #2a2a2a;">Resumen de las programaciones del Departamento</span><span style="color: #2a2a2a;">.</span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/13_14/programaciones/Programacion_2Bach_13-14.pdf" target="_blank">Programación de Dibujo Técnico 2º Bachillerato.</a> (28/10/13)</span></li>
</ul>
<ul>
<li><a href="archivos_ies/13_14/programaciones/Directrices_orientaciones_%20DTII_13_14.pdf" target="_blank">Directrices y orientaciones de Dibujo Técnico II para 2º de Bachillerato.</a> (19/10/13)</li>
</ul>
<h6><span style="text-decoration: underline;"><span style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff; text-decoration: underline;">Sistema Diédrico Aplicación de perpendicularidad recta-plano. (19/10/13)</span></span></h6>
<ul>
<li><a href="archivos_ies/13_14/dibujo/Aplicacion_de_perepndicularidad_1.pdf" target="_blank"><strong style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;">Aplicación de perepndicularidad 1</strong></a></li>
<li><a href="archivos_ies/13_14/dibujo/Aplicacion_de_perepndicularidad_2.pdf" target="_blank"><strong style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;">Aplicación de perepndicularidad 2</strong></a></li>
<li><a href="archivos_ies/13_14/dibujo/Aplicacion_de_perepndicularidad_3.pdf" target="_blank"><strong style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;">Aplicación de perepndicularidad 3</strong></a></li>
</ul>
<p><strong style="color: #222222; font-family: arial, sans-serif; background-color: #ffffff;"> </strong></p>
<p><span style="text-decoration: underline; font-family: 'arial black', 'avant garde'; font-size: 12pt;"><strong>Equivalencia de formas planas 2º Bachillerato:</strong></span></p>
<ul>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><strong><a href="archivos_ies/Equivalencias.pdf" target="_blank">Equivalencias</a><br /> </strong></span></li>
<li><a class="at_url" href="archivos_ies/062_Equivalencia%20I%20C.pdf" target="_blank" title="Download this file (Equivalencia I C.pdf)" style="color: #c6c37b; font-weight: bold;">Equivalencia I C.pdf</a></li>
<li><a class="at_url" href="archivos_ies/062_Equivalencia%20II%20C.pdf" target="_blank" title="Download this file (Equivalencia II C.pdf)" style="text-decoration: none; font-weight: bold;">Equivalencia II C.pdf</a></li>
<li><a class="at_url" href="archivos_ies/062_Equivalencia%20III%20C.pdf" target="_blank" title="Download this file (Equivalencia III C.pdf)" style="text-decoration: none; font-weight: bold;">Equivalencia III C.pdf</a></li>
<li><a class="at_url" href="archivos_ies/062_Equivalencia%20III%20Sol.pdf" target="_blank" title="Download this file (Equivalencia III Sol.pdf)" style="text-decoration: none; font-weight: bold;">Equivalencia III Sol.pdf</a></li>
</ul>
<p><span style="text-decoration: underline; font-family: 'arial black', 'avant garde'; font-size: 12pt;"><strong>Ejercicios resueltos:</strong></span></p>
<ul>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><a href="archivos_ies/dep_dibujo/ejercicios_resueltos_de_triangulos.pdf" target="_blank">De triángulos.</a></span></li>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><a href="archivos_ies/dep_dibujo/ejercicios_resueltos_de_cuadrilateros.pdf" target="_blank">De cuadriláteros.</a></span></li>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><a href="archivos_ies/dep_dibujo/PAU_2010_Ejercicio_resuelto_Enunciado.pdf" target="_blank">Ejercicio de Selectividad 2010</a>, <a href="archivos_ies/dep_dibujo/PAU_2010_Ejercicio_resuelto_Solucion.pdf" target="_blank">resuelto y comentado.</a><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"> (añadido el 24/04/12)</span><a href="archivos_ies/dep_dibujo/ejercicios_resueltos_de_cuadrilateros.pdf" target="_blank"><br /></a></span></li>
<li><span style="font-family: 'arial black', 'avant garde'; font-size: 13px;"><span style="font-size: 10pt; font-family: arial, helvetica, sans-serif;"><a href="archivos_ies/dep_dibujo/examen_sistema_diedrico_opIyII_enun_y_soluciones_2011_12.zip" target="_blank">EXAMEN SISTEMA DIÉDRICO. CURSO 2011/12. Opción I y II. Enunciado y soluciones.2º Bachillerato.</a> </span></span><span style="font-family: arial, helvetica, sans-serif; font-size: 13px;">(añadido el 24/04/12) (Botón derecho &gt; Guardar destino como... ) ES UN ZIP.</span></li>
</ul></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">Educación Plástica</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:68:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - PROYECTOS DEL IES";s:11:"description";s:27:"http://youtu.be/koWeNRieX2I";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:0:{}s:6:"module";a:0:{}}
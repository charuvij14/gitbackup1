<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:40997:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Dpto. de Economía</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Selectividad Septiembre 2014: EXÁMENES</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 18 Septiembre 2014 20:34</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=262:selectividad-septi-2015&amp;catid=140&amp;Itemid=99&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=9e4f26da3e9d8ee760e3f1bf0af7e28f0a071b9d" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 14063
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1 style="text-align: center;"><span style="text-decoration: underline;">ZONA DEDICADA A LA SELECTIVIDAD DE JUNIO Y SEPTIEMBRE 2014</span></h1>
<ul>
<li><strong style="color: #2a2a2a; font-size: 13px;">LAS NOTAS SE PUEDEN CONSULTAR EN INTERNET A PARTIR DEL DÍA 24/09/14 &nbsp;(Miércoles) EN&nbsp;<a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">ESTE ENLACE.</a></strong></li>
<li><span style="color: #2a2a2a; font-size: 13px;">&nbsp;</span><strong style="color: #2a2a2a; font-size: 13px;">EXÁMENES QUE HAN SALIDO EN JUNIO Y SEPTIEMBRE 2014 DE LAS DISTINTAS MATERIAS:</strong></li>
</ul>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; color: #000000; background-color: #e7eaad;" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td style="text-align: center;" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Biología</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>CTM</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Comentario de Texto, Lengua&nbsp;</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Dibujo Art. II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Dibujo Téc. II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Economía de la Empresa</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Diseño</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Electrotec.</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td style="text-align: center;">
<p><span style="font-size: 10pt;"><strong>Junio<a href="archivos_ies/13_14/selectividad/Biologia_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" width="35" height="35" align="middle" border="0" /></a> </strong><a href="archivos_ies/13_14/selectividad/Biologia_Colision_Junio2014.pdf" target="_blank" style="font-size: inherit;"><strong>Colisión</strong></a> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/CTM_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" width="35" height="35" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/Lengua_Junio2014.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Junio2014.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a><a href="archivos_ies/13_14/selectividad/Dibujo_Artistico_Colision_Junio2014.pdf" target="_blank"><span style="color: inherit; font-family: inherit; text-align: left;">Colisión</span></a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Dibujo_Tecnico_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Economia_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Diseño_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
<p style="text-align: center;"><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Electrotecnia_Colision_Junio2014.pdf" target="_blank">Colisión</a></strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><span style="font-size: 10pt;">&nbsp;<a href="archivos_ies/13_14/selectividad/biologia_sept_2014.pdf" target="_blank" title="Biología."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/ctm_sept_2014.pdf" target="_blank" title="CTM."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/lengua_sept_2014.pdf" target="_blank" title="Lengua.">Sept.<strong style="color: #000000; text-align: center;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/dt_sept_2014.pdf" target="_blank" title="Dibujo Técnico II."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/economia_sept_2014.pdf" target="_blank" title="Economía."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/electrotecnia_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Electrotecnia.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
</tr>
<tr bgcolor="#FEFDD6">
<td style="text-align: center;">
<p><span style="font-size: 12pt;"><strong>Física</strong></span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 12pt;"><strong>Geografía</strong></span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 12pt;"><strong>Griego II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>H. de España</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>H. Filosofía</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Historia del Arte</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Latín II</strong><strong>&nbsp;</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Lengua Extranjera II (Alemán)</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Fisica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Geografia_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/Griego_AyB.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;<a href="archivos_ies/13_14/selectividad/H_España_Junio2014.pdf" target="_blank"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a> </strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Filosofia_AyB.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a>&nbsp;</strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;</strong><strong><a href="archivos_ies/13_14/selectividad/H_Arte_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a></strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong>&nbsp;</strong><strong><a href="archivos_ies/13_14/selectividad/Latin_AyB.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></a></strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong style="text-align: center;"><a href="archivos_ies/13_14/selectividad/Aleman_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><span style="font-size: 10pt;"><strong>&nbsp;<a href="archivos_ies/13_14/selectividad/fisica_sept_2014.pdf" target="_blank" title="Física."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a></strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/geografia_sept_2014.pdf" target="_blank" title="Geografía."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;"><strong style="color: #000000;">Sept.<strong><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong>&nbsp;<a href="archivos_ies/13_14/selectividad/griego_sept_2014.pdf" target="_blank" title="Griego."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></a></strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/historia_sept_2014.pdf" target="_blank" title="Historia de España."><strong style="color: #000000; text-align: -webkit-center; background-color: #ffffff;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/filosofia_sept_2014.pdf" target="_blank" style="text-align: -webkit-center; background-color: #ffffff;" title="Filosofía.">Sept<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong>&nbsp;<a href="archivos_ies/13_14/selectividad/h_arte_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Historia del Arte.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a></strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/latin_sept_2014.pdf" target="_blank" title="Latín."><strong style="color: #000000; font-size: 13px; text-align: -webkit-center; background-color: #ffffff;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></strong></a>&nbsp;</strong></span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><strong>&nbsp;</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td style="text-align: center;" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Lengua Extranjera II (Francés)</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Inglés</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Lit. Uni.</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Mat.Apl.a las CCSS II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Mat. II</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong style="text-align: left;">&nbsp;Química</strong><strong>&nbsp;</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>&nbsp;</strong><strong style="text-align: center;">Téc. .Expr. Gráf. Plást.(no disponible)</strong></span></p>
</td>
<td style="text-align: center;" align="center" bgcolor="#FEFDD6">
<p><span style="font-size: 12pt;"><strong>Tecnología Industrial II</strong></span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td style="text-align: center;">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Frances_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Ingles_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Lit_Universal_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong><a href="archivos_ies/13_14/selectividad/Mat_CCSSII_soljun2014.pdf" target="_blank"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;">Soluc.</strong></a> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong><strong style="color: inherit; font-family: inherit; font-size: inherit; text-align: left;"><a href="archivos_ies/13_14/selectividad/Mat_II_soljun2014.pdf" target="_blank" style="font-size: inherit;">Soluc.</a></strong><span style="color: inherit; font-family: inherit; text-align: left;">&nbsp;</span></span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong><a href="archivos_ies/13_14/selectividad/Quimica_Junio2014.pdf" target="_blank" style="text-decoration: none; color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </strong><strong>&nbsp;</strong> </span></p>
</td>
<td style="text-align: center;" align="center">
<p><span style="font-size: 10pt;"><strong style="text-align: left;"><span style="color: #2f310d;"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </span> </strong><strong>&nbsp;</strong> </span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><span style="color: #2f310d;"><a href="archivos_ies/13_14/selectividad/Tecn_Industrial_Junio2014.pdf" target="_blank"><strong style="color: #000000; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;">Junio</span> </strong><img style="border-style: solid; border-color: #bcbcbc; cursor: se-resize !important; display: block; margin-left: auto; margin-right: auto;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /> </a> </span><strong style="color: inherit; font-family: inherit; font-size: inherit;">&nbsp;</strong> </span></p>
</td>
</tr>
<tr bgcolor="#ffffff">
<td>
<p><a href="archivos_ies/13_14/selectividad/frances_sept_2014.pdf" target="_blank" title="Francés."><span style="font-size: 10pt;">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></span></a></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/ingles_sept_2014.pdf" target="_blank" style="text-align: -webkit-center; background-color: #ffffff;" title="Inglés.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/lit_uni_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Literatura Universal.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/mat_ccss_ii_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Matemáticas Aplicadas a las Ciencias Sociales II.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/mat_ii_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Matemáticas II.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/quimica_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Química.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;">&nbsp;</span></p>
</td>
<td align="center">
<p><span style="font-size: 10pt;"><a href="archivos_ies/13_14/selectividad/tec_indus_ii_sept_2014.pdf" target="_blank" style="font-size: 13px; text-align: -webkit-center; background-color: #ffffff;" title="Tecnología Industrial II.">Sept.<strong style="color: #000000;"><img style="border-style: solid; border-color: #bcbcbc;" src="archivos_ies/13_14/selectividad/pdf.gif" alt="" align="middle" border="0" /></strong></a>&nbsp;</span></p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<ul>
<li>
<h3>Para ver las notas entrar en la siguiente web:</h3>
</li>
</ul>
<h3 style="background-color: #1188ff;"><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp</a></h3>
<p>&nbsp;</p>
<ul>
<li>
<h4><a href="archivos_ies/13_14/VI_Encuentro_con_los_Centros.pdf" target="_blank">VI encuentro de la universidad con los centros.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_REGISTRO_SELECTIVIDAD.pps" target="_blank">Presentación: registro para la selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/PRESENTACION_MATRICULA_SELECTIVDAD.pps" target="_blank">Presentación: matrícula de selectividad.<br /><br /></a></h4>
</li>
<li>
<h4><a href="archivos_ies/13_14/SELECTIVIDAD_ORIENTACION.pptx" target="_blank">Selectividad: orientaciones.<br /><br /></a></h4>
</li>
<li>
<h4><a href="https://oficinavirtual.ugr.es/apli/solicitudPAU/selectividad00-menu.jsp" target="_blank">Web de la Universidad para registro y matrícula.<br /><br /></a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Otros datos de interés:&nbsp;</a></h4>
</li>
</ul>
<ol><ol>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Inscripción</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Matriculación</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Precios</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Calendario de las pruebas día a día</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Nota de acceso</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Exámenes de otros años y orientaciones</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Orden de preferencia de carreras</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Procedimiento de matrícula en la UIniversidad</a></h4>
</li>
<li>
<h4><a href="index.php?option=com_content&amp;view=article&amp;id=139&amp;Itemid=218" target="_blank">Plazos</a></h4>
</li>
</ol></ol>
<p>&nbsp;</p>
<p><span style="color: #2a2a2a;">&nbsp;</span></p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Novedades</span> / <span class="art-post-metadata-category-name">Novedades de Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Intercambio Brooklyn, NY abril 2011</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 15 Septiembre 2011 20:58</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=53:intercambio-brooklyn-ny-abril-2011&amp;catid=140&amp;Itemid=99&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=5f53a9798e7f7a02031604753e4051fe31c94ebf" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1702
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><iframe width="425" height="349" src="http://www.youtube.com/embed/xiFxyNxAQR0?hl=es&fs=1" frameborder="0" allowfullscreen></iframe></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Novedades</span> / <span class="art-post-metadata-category-name">Novedades de Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Exámenes de septiembre 2011</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 21 Agosto 2011 15:40</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=56:examenes-de-septiembre-2011&amp;catid=140&amp;Itemid=99&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=d370af9c5b1bdd01a22424d73ce5338f2b149d8f" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2569
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><table valign="center" align="center" cellpadding="1" cellspacing="0" border="0" style="border: 0px solid #8799f9; width: 100%; background-color: #ffffff;">
<tbody>
<tr bgcolor="#efbc38" align="center" valign="middle" style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid;">
<td align="center" colspan="4" style="text-align: center; border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid;"><b><span style="color: #ff0000;"><strong><span style="font-size: 12pt;">CALENDARIO DE EXÁMENES DE SEPTIEMBRE 2011</span></strong></span></b></td>
</tr>
<tr class="textogris" style="border-color: #8799f9 #8799f9 currentColor; border-bottom-width: 1px; border-bottom-style: solid;">
<td class="textogris" style="border-color: #8799f9; text-align: right;">
<div style="text-align: left;"></div>
<ul>
<li>
<div style="text-align: left;"><span style="font-size: 12px;"><strong>Calendario de <a target="_blank" href="images/archivos/calendario_examenes_septiembre_2011.pdf" style="color: #977702; font-family: Arial, Helvetica, sans-serif; text-decoration: underline;">exámenes de septiembre 2011</a><br /> </strong></span></div>
</li>
</ul>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Novedades</span> / <span class="art-post-metadata-category-name">Novedades de Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Secretaría</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 19 Febrero 2013 18:10</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=55:secretaria&amp;catid=140&amp;Itemid=99&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=e6e90e2112e6875dcc5477f9fab47b6e5c2d201e" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1422
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p> </p>
<ul>
<li><span style="text-decoration: underline;">Calendario de<a href="images/archivos/calendario_examenes_septiembre_2011.pdf" target="_blank" style="font-family: Arial, Helvetica, sans-serif; text-decoration: underline; color: #977702;"> exámenes de septiembre 2011</a>.</span></li>
</ul></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Novedades</span> / <span class="art-post-metadata-category-name">Novedades de Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
            </div>";s:4:"head";a:10:{s:5:"title";s:69:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Dpto. de Economía";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:101:"/index.php?option=com_content&amp;view=category&amp;id=140&amp;Itemid=99&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:102:"/index.php?option=com_content&amp;view=category&amp;id=140&amp;Itemid=99&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:5:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:25:"Área Socio-Lingüística";s:4:"link";s:59:"index.php?option=com_content&view=article&id=186&Itemid=572";}i:2;O:8:"stdClass":2:{s:4:"name";s:18:"Dpto. de Economía";s:4:"link";s:58:"index.php?option=com_content&view=article&id=169&Itemid=99";}i:3;O:8:"stdClass":2:{s:4:"name";s:9:"Novedades";s:4:"link";s:59:"index.php?option=com_content&view=category&id=230&Itemid=99";}i:4;O:8:"stdClass":2:{s:4:"name";s:24:"Novedades de Secretaría";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:10238:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Admisión en Ciclos Formativos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:11</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=209:admision-en-ciclos-formativos&amp;catid=242&amp;Itemid=616&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=c4b4d6c7f4e4c762d2ba78bf11f508ae80a0043d" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2564
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Admisión Ciclos Formativos</h2>
<p><strong>I. NORMATIVA REGULADORA</strong></p>
<ul class="list-icon icon-article">
<li><a href="http://www.juntadeandalucia.es/boja/2007/107/d1.pdf" target="_blank">Orden de 14 de Mayo de 2007, (BOJA nº 107 de 31-05-2007)</a></li>
</ul>
<p>II.<strong> DOCUMENTOS DESCARGABLES Y RELLENABLES</strong></p>
<ul class="list-icon icon-download">
<li><span class="jsn-listbullet"><a href="archivos_ies/solicitud_plaza_grado_superior.pdf" target="_blank">Admisión</a><br /></span></li>
<li><a href="archivos_ies/solicitud_reserva_plaza.pdf" target="_blank">Reserva de puesto escolar</a></li>
<li><a href="archivos_ies/anexo_ii_solicitud_gs_rellenable.pdf" target="_blank">Inscripción Prueba de Acceso (anexo II)</a></li>
<li><a href="archivos_ies/temario-parte-comun-anexo-v.pdf" target="_blank">Temario parte común (anexo V)</a></li>
<li><a href="archivos_ies/GS%20ANEXO%20VI.pdf" target="_blank">Opciones (anexo VI)</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1645-contenidos-y-criterios-de-evaluacion-grado-superior" target="_blank"><strong>CONTENIDOS Y CRITERIOS DE EVALUACIÓN</strong></a></li>
<li><a href="archivos_ies/EXENCIONES%20GS%20completo%20BUENO.pdf" target="_blank">Exención de materias de Bachillerato (anexo VIII)</a></li>
<li><a href="archivos_ies/solicitud_convo_gracia.pdf" target="_blank">Solicitud de convocatoria extraordinaria</a></li>
<li><a href="archivos_ies/solicitud_de_convalidaciones.pdf" target="_blank">Solicitud de convalidaciones</a></li>
</ul>
<ul>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso" target="_blank">PRUEBAS DE ACCESO: ÍNDICE</a> (Web Consejerái)</li>
</ul>
<p> </p>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion" target="_blank">Para más información</a></li>
</ul>
<p><strong>III. OFERTA</strong><br />La oferta educativa está disponible en la web del Instituto, en los Institutos, en las Delegaciones Provinciales y en la Web de la Consejería de Educación:</p>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/educacion" target="_blank">Web de la Consejería</a></li>
</ul>
<p><strong>IV. DOCUMENTACIÓN</strong></p>
<ul style="list-style-type: disc;">
<li>Cada solicitante presentará UNA ÚNICA SOLICITUD</li>
<li>Para presentar la solicitud es imprescindible estar en posesión de los requisitos académicos exigidos para realizar los estudios solicitados.</li>
<li>Impreso de solicitud: Anexo I para los Ciclos de Grado Medio o Anexo II para los Ciclos de Grado Superior, debidamente cumplimentado. En él se relacionarán, por orden de preferencia, los códigos de los ciclos formativos que se desee cursar, indicando en cada caso, si el ciclo es de Régimen General (G) o de Educación de Adultos (A) y los códigos de los centros correspondientes. Asimismo se acompañará de la siguiente documentación: </li>
</ul>
<p> </p>
<ol style="list-style-type: lower-alpha;">
<li style="list-style-type: none;"><ol style="list-style-type: lower-alpha;">
<li>Fotocopia del DNI, Pasaporte, Libro de Familia u otro documento oficial acreditativo de la identidad y de la edad.</li>
<li>Certificación académica personal, donde conste la nota media de los estudios realizados, o certificado de haber superado la prueba de acceso. </li>
<li>Fotocopia del dictamen emitido por el organismo público competente, en caso de padecer minusvalía.</li>
<li>Los solicitantes con estudios realizados en el extranjero deberán presentar, junto a la documentación antes indicada, la homologación de su titulación o, en su caso, el volante de solicitud de homologación sellado por la Delegación Provincial del Ministerio de Educación y Ciencia (tiene validez provisional durante 6 meses). </li>
</ol></li>
</ol>
<p><strong>V. LUGAR DE PRESENTACIÓN</strong></p>
<ol style="list-style-type: lower-alpha;">
<li>En el Centro Educativo solicitado en primer lugar.</li>
<li>En los registros de cualquier órgano administrativo, perteneciente a la Administración General del Estado, a la Administración de las Comunidades Autónomas.</li>
<li>Por Correo Certificado. </li>
</ol>
<p><strong>Nota:</strong> La presentación de solicitudes en más de un centro educativo dará lugar a que sólo se tenga en cuenta la última presentada. <br /><br /><strong>VI. PLAZOS DE PRESENTACIÓN</strong></p>
<ul class="list-icon icon-calendar">
<li>Del 1 al 25 de junio para el alumnado de 4º de ESO, 2º ESA o 2º de Bachillerato que haya aprobado todo el curso en junio o la Prueba de Acceso en este mismo periodo.</li>
<li>Del 1 al 10 de septiembre para el alumnado de 4º de ESO, 2º ESA o 2º de Bachillerato que haya aprobado todo el curso en septiembre o la Prueba de Acceso en este mismo periodo.</li>
<li>Del 3 al 5 de octubre (Procedimiento Extraordinario), para el alumnado que no haya presentado Solicitud en los dos procesos anteriores.</li>
</ul>
<p><strong>Nota:</strong> La solicitud será única para todo el proceso. La duplicidad de solicitud en junio y septiembre implica la desestimación. <br /><br /><strong>VI. PUBLICACIÓN DE DATOS PROVISIONALES</strong></p>
<ul class="list-icon icon-calendar">
<li>29 de junio, para las solicitudes presentadas entre el 1 y el 25 de junio.</li>
<li>13 de septiembre, para las solicitudes presentadas del 1 al 10 de septiembre.</li>
<li>9 de octubre, para las solicitudes del Proceso Extraordinario presentadas del 3 al 5 de octubre.</li>
</ul>
<p><strong>Nota:</strong> Es aconsejable consultar los listados provisionales cuando se publiquen en el tablón de anuncios del Centro o en la web de la Consejería de Educación (Novedades)<br /><br /><strong> VII. PERIODO DE RECLAMACIONES</strong></p>
<ul class="list-icon icon-calendar">
<li>Del 29 de junio al 4 de julio para el alumnado solicitante de junio.</li>
<li>13 y 14 de septiembre para el alumnado solicitante de septiembre.</li>
<li>9 y 10 de octubre para las solicitudes del Proceso Extraordinario.</li>
</ul>
<p><strong>Nota:</strong> Las reclamaciones se presentarán en el centro solicitado en primer lugar. Todas aquellas que se presenten fuera de plazo serán desestimadas. <br /><br /><strong>VIII. ADJUDICACIÓN DE PUESTOS ESCOLARES</strong></p>
<ul class="list-icon icon-calendar">
<li>10 de julio, 1ª adjudicación.</li>
<li>23 de julio, 2ª adjudicación.</li>
<li>20 de septiembre, 3ª adjudicación.</li>
<li>26 de septiembre, 4ª adjudicación.</li>
<li>11 de octubre, Adjudicación Extraordinaria</li>
</ul>
<p><strong>IX. MATRÍCULA O RESERVA DE PLAZA</strong><br />La matrícula se realizará en el centro educativo donde se ha obtenido plaza, y es obligatoria si dicha plaza corresponde a la solicitada en primer lugar.<br />La reserva se realizará, mediante un Anexo V, en el centro educativo donde se ha obtenido plaza y es obligatoria, (si no se opta por la matrícula) con el fin de obtener una opción más favorable en el proceso, en caso de no obtener plaza en la primera opción. Mientras no se haga efectiva la matrícula, es obligatoria en cada adjudicación realizar la reserva correspondiente. <br />Los plazos establecidos son los siguientes:</p>
<ul class="list-icon icon-calendar">
<li>Del 25 al 30 de junio, para el alumnado que repite curso y 2º curso.</li>
<li>Del 10 al 16 de julio, para la 1ª adjudicación.</li>
<li>Del 3 al 8 de septiembre, para la 2ª adjudicación.</li>
<li>Del 20 al 24 de septiembre, para la 3ª adjudicación.</li>
<li>Del 26 al 28 de septiembre, para la 4ª adjudicación.</li>
<li>Del 15 al 16 de octubre, para la Matrícula Extraordinaria</li>
</ul>
<p><strong>Nota: </strong>No realizar matrícula o reserva en los plazos establecidos para ello, supondrá la renuncia a seguir participando en el proceso. La reserva se repetirá en cada adjudicación, aunque se haya obtenido el mismo centro.</p>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/" target="_blank">Más Información</a></li>
<li><a href="https://www.juntadeandalucia.es/educacion/secretariavirtual/consultas/acceso.html?idConsulta=45" target="_blank">Consulta Personalizada de las Adjudicaciones</a></li>
</ul></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:60:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Admisión";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:17:"Ciclos formativos";s:4:"link";s:59:"index.php?option=com_content&view=article&id=208&Itemid=615";}i:1;O:8:"stdClass":2:{s:4:"name";s:9:"Admisión";s:4:"link";s:59:"index.php?option=com_content&view=article&id=209&Itemid=616";}}s:6:"module";a:0:{}}
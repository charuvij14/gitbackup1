<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:60766:"<div class="blog">    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postcontent">
<div class="cat-children">
        <h3>Subcategorías</h3>
        
	<ul>
					<li class="first">
						<span class="item-title"><a href="/index.php?option=com_content&amp;view=category&amp;id=104&amp;Itemid=717">
				ÚLTIMA NORMATIVA</a>
			</span>

										<div class="category-desc">
					<table valign="center" align="center" cellpadding="1" cellspacing="0" border="0" style="border-collapse: collapse; width: auto; background-color: #ffffff; margin: 1px;">
<tbody>
<tr bgcolor="#efbc38" style="border-top-color: #8799f9; border-right-color: #8799f9; border-left-color: #8799f9; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: initial;">
<td align="center" style="border-top-color: #8799f9; border-right-color: #8799f9; border-bottom-color: initial; border-left-color: #8799f9; vertical-align: top; text-align: center; border-width: 1px; border-style: solid; padding: 2px;">
<p style="color: #333333;"><span style="color: #ff0000;"><strong><span style="font-size: 12pt;">ÚLTIMA NORMATIVA (11/10/2011)</span></strong></span></p>
</td>
</tr>
<tr class="textogris" style="border-color: #8799f9;">
<td class="textogris" style="vertical-align: top; text-align: center; padding: 2px; border: 1px solid #8799f9;">
<table cellspacing="3" border="1" style="border-collapse: collapse; width: 528px; height: 1px; margin: 1px;">
<tbody>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-11</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Atención a la Diversidad</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc5oct2011ProgramaProfundiza.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 5 de octubre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-11</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Organización y Funcionamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc5oct2011ProgramaProfundiza.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 5 de octubre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el funcionamiento del programa de profundización de conocimientos “PROFUNDIZA” para el curso 2011-2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-06</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación Compensatoria</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/circulares/Circular29sept2011ProgramaAcompanamiento.pdf" style="text-decoration: none; color: #005fa9;">CIRCULAR de 29 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-06</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Acompañamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/circulares/Circular29sept2011ProgramaAcompanamiento.pdf" style="text-decoration: none; color: #005fa9;">CIRCULAR de 29 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, por la que se aclaran determinados aspectos relacionados con la puesta en funcionamiento del Programa de Acompañamiento Escolar.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-05</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación Compensatoria</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc30sept2011PlanAcompanamientoExtranjero.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 30 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-05</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Adscripción / Integración / Especialidades</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden23sept2011FormacionEquivalenteMaster.pdf" style="text-decoration: none; color: #005fa9;">ORDEN EDU/2645/2011, de 23 de septiembre</a>, por la que se establece la formación equivalente a la formación pedagógica y didáctica exigida para aquellas personas que estando en posesión de una titulación declarada equivalente a efectos de docencia no pueden realizar los estudios de máster (BOE 05-10-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-10-05</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Acompañamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc30sept2011PlanAcompanamientoExtranjero.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 30 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar en Lengua Extranjera destinado a centros públicos de Educación Primaria para el curso 2011/2012.</td>
</tr>
</tbody>
</table>
<table cellspacing="3" border="1" style="border-collapse: collapse; width: 528px; margin: 1px;">
<tbody>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;">2011-09-30</p>
</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;"><span style="font-size: 11px;">Acceso a la universidad</span></p>
</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resolucion29abril2010AnexoIactualizacion12julio2011.pdf" style="text-decoration: none; color: #005fa9;">Anexo I de la Resolución de 29 de abril de 2010</a>, de la Secretaría de Estado de Educación y Formación Profesional, por la que se establecen las instrucciones para el cálculo de la nota media que debe figurar en las credenciales de convalidación y homologación de estudios y títulos extranjeros con el bachiller españolen el apartado Estudios y Títulos no Universitarios (actualización de 12-07-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación de Personas Adultas</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc23sept2011PruebasLibresBach2011.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 23 de septiembre de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, sobre la realización de las pruebas para la obtención del título de Bachiller para personas mayores de 20 años en la convocatoria de 2011.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Asistencia Jurídica / Responsabilidad / Abstención</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resolucion16sept2011AsistenciaJuridica.pdf" style="color: #005fa9;">RESOLUCIÓN de 16 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se dispone la publicación del Pacto de Mesa Sectorial en materia de prestación de asistencia jurídica gratuita al personal docente no universitario y al personal de Administración y Servicios de los centros docentes públicos y de los servicios educativos dependientes de la Consejería competente en materia de educación (BOJA 29-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden22sept2011SubvencionesAuxiliaresConversacion.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 22 de septiembre de 2011</a>, por la que se establecen las modalidades de provisión y las bases reguladoras para la concesión de subvenciones a auxiliares de conversación, y se efectúa convocatoria para el curso 2011/12 (BOJA 29-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-29</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Calidad y Mejora de los Rendimientos Escolares</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden26sept2011ProgramaCalidad.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 26 de septiembre de 2011</a>, por la que se regula el Programa de calidad y mejora de los rendimientos escolares en los centros docentes públicos (BOJA 29-09-2011)</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Educación Compensatoria</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2011ProgramaAcompEscolar.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 15 de septiembre de 2011</a>, de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapa de Educación Secundaria Obligatoria.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Órganos Unipersonales</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc26mayo2011FormInicialDirectores.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCION de 26 de mayo de 2011</a> de la Dirección General de Profesorado y Gestión de Recursos Humanos por la que se oferta la formación inicial para la dirección de centros docentes a otros directores y directoras que lo soliciten.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Programa de Acompañamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc15sept2011ProgramaAcompEscolar.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 15 de septiembre de 2011</a>, de la dirección General de Participación e Innovación Educativa, sobre el desarrollo del Programa de Acompañamiento Escolar destinado al alumnado escolarizado en segundo o tercer ciclo de la etapa de Educación Primaria o en el primer, segundo o tercer curso de la etapa de Educación Secundaria Obligatoria.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-26</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Deporte Escolar</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc6sept2011ProgramaEscuelasDeportivas.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 6 de septiembre de 2011</a> de la Dirección General de Participación e Innovación Educativa sobre el programa Escuelas Deportivas para el curso escolar 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-24</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Jornada Personal Docente</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc16sept2011LiberadosSindicales.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 16 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-24</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Organización y Funcionamiento</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc16sept2011LiberadosSindicales.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 16 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, sobre criterios para la elaboración del horario del personal docente con liberación sindical parcial.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-21</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Arte Dramático</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc20sept2011EnsArtisticasSupGrado1112.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 20 de Septiembre de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-21</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Danza</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc20sept2011EnsArtisticasSupGrado1112.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 20 de Septiembre de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-21</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Música</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc20sept2011EnsArtisticasSupGrado1112.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 20 de Septiembre de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por las que se organizan algunos aspectos de las Enseñanzas Artísticas Superiores de Grado para el curso académico 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-09-16</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc12sept2011BachillerBaccalaureat.pdf" style="text-decoration: none; color: #005fa9;">INSTRUCCIONES de 12 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el programa de doble titulación BACHILLER-BACCALAURÉAT en centros docentes de la Comunidad Autónoma de Andalucía.</td>
</tr>
</tbody>
</table>
<div style="text-align: justify;">
<table cellspacing="3" border="1" width="98%" style="border-collapse: collapse; width: auto; margin: 1px;">
<tbody>
<tr>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-16</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Formación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/NotaInformativa16septiembre2011FCT.pdf">NOTA INFORMATIVA de 16 de septiembre de 2011</a> de la Dirección General de Formación Profesional y Educación Permanente sobre la nueva Orden reguladora de los módulos de Formación en Centros de Trabajo y de Proyecto.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-15</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc7sept2011PruebasBachillerato.pdf">RESOLUCIÓN de 7 de septiembre de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-15</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Educación de Personas Adultas</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc7sept2011PruebasBachillerato.pdf">RESOLUCIÓN de 7 de septiembre de 2011</a>, de la Dirección General de Formación Profesional y Educación Permanente, por la que se convocan las pruebas para la obtención del título de Bachiller para personas mayores de veinte años (BOJA 13-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-14</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Inspección Educativa: Oposiciones</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc2sept2011AdmitidosDefOposInsp.pdf">RESOLUCIÓN de 2 de septiembre de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas definitivas de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía convocados por Orden de 26 de abril de 2011 (BOJA 14-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-14</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Organización y Funcionamiento</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc30agosto2011CelebracionLorca.pdf">RESOLUCIÓN de 30 de agosto de 2011</a>, de la Dirección General de Ordenación y Evaluación Educativa, por la que se dictan instrucciones para la realización en los centros docentes de la Comunidad Autónoma Andaluza de actividades orientadas a conmemorar la figura de Federico García Lorca (BOJA 14-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-13</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Centros Docentes</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden21julio2011ModificacionCentrosPrimaria.pdf">ORDEN de 21 de julio de 2011</a>, por la que se modifican escuelas infantiles de segundo ciclo, colegios de educación primaria, colegios de educación infantil y primaria y un centro específico de educación especial, así como colegios públicos rurales (BOJA 13-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Calendario y Jornada Escolar</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Nota7sept2011CalenCentrosSupArtisticos1112.pdf">NOTA INFORMATIVA de 7 de septiembre de 2011</a> de la Dirección General de Ordenación y Evaluación Educativa sobre el calendario escolar de los centros superiores de enseñanzas artísticas para el curso 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Escuela Oficial de Idiomas</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc9sept2011CALonline1112.pdf">INSTRUCCIONES de 9 de septiembre de 2011</a>, de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc9sept2011CALonline1112.pdf">INSTRUCCIONES de 9 de septiembre de 2011</a>, de las Direcciones Generales de Planificación y Centros y de Ordenación y Evaluación Educativa, por las que se autorizan los cursos de actualización lingüística del idioma inglés en la modalidad "on line", para el curso 2011/12, dirigido al profesorado implicado en el desarrollo del currículo integrado de las lenguas y a la Inspección Educativa, y se regula la admisión y matriculación en el mismo, así como determinados aspectos sobre su organización y funcionamiento.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc8sept2011IdiomaChino.pdf">INSTRUCCIONES de 8 de septiembre de 2011</a>, de la Dirección General de Participación e Innovación Educativa, sobre el programa de implantación de la Enseñanza de la Lengua China en centros docentes andaluces de Educación Primaria y Secundaria.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-07</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Escuela Oficial de Idiomas</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc28julio2011ProgramaThatsEnglish1112.pdf">INSTRUCCIONES de 28 de julio de 2011</a> de la Dirección General de Formación Profesional y Educación Permanente para el funcionamiento del programa “That´S English!” en el curso 2011/2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-07</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Plan de Plurilingüismo</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc2sept2011OrgFuncCentrosBilingues.pdf">INSTRUCCIONES de 2 de septiembre de 2011</a> conjuntas de la Dirección General de Participación e Innovación Educativa y de la Dirección General de Formación Profesional y Educación Permanente del Profesorado sobre la organización y funcionamiento de la enseñanza bilingüe para el curso 2011-2012.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-07</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Programas de Cualificación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden1agosto2011BasesSubvencionesPCPI.pdf">ORDEN de 1 de agosto de 2011</a>, por la que se aprueban las bases reguladoras para la concesión de subvenciones en régimen de concurrencia competitiva a Corporaciones Locales, asociaciones profesionales y organizaciones no gubernamentales para el desarrollo de los módulos obligatorios de los Programas de Cualificación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 07-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-05</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Atención a la Diversidad</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc1sept2011ProcAltasCapacidades.pdf">INSTRUCCIONES de 1 de septiembre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el procedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-05</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Educación Especial</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instruc1sept2011ProcAltasCapacidades.pdf">INSTRUCCIONES de 1 de septiembre de 2011</a> de la Dirección General de Participación e Innovación Educativa por las que se regula el procedimiento para la aplicación del protocolo para la detección y evaluación del alumnado con necesidades específicas de apoyo educativo por presentar altas capacidades intelectuales.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-09-02</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Artes Plásticas y Diseño</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden18agosto2011CurriculoCiclosArtesPlasticasCeramica.pdf">ORDEN de 18 de agosto de 2011</a>, por la que se desarrolla el currículo correspondiente a los títulos de Técnico de Artes Plásticas y Diseño en Alfarería y en Decoración Cerámica y los títulos de Técnico superior de Artes Plásticas y Diseño en Cerámica Artística, en Modelismo y Matricería Cerámica y en Recubrimientos Cerámicos, pertenecientes a la familia profesional artística de la Cerámica Artística (BOJA 02-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-31</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/ModifInstruc6julio2011PremiosBachillerato.pdf">Modificación a las INSTRUCCIONES de 6 de julio de 2011</a> de la Dirección General de Ordenación y Evaluación Educativa, sobre los premios extraordinarios de bachillerato correspondientes al curso 2010/2011.</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-23</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Arte Dramático</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.juntadeandalucia.es/boja/boletines/2011/165/d/7.html">DECRETO 259/2011, de 26 de julio</a>, por el que se establecen las enseñanzas artísticas superiores de Grado en Arte Dramático en Andalucía (BOJA 23-08-2011).&nbsp;<a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto259-2011ArteDramatico.pdf">Descargar disposición en pdf (20,8 MB)</a></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-23</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Música</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.juntadeandalucia.es/boja/boletines/2011/165/d/8.html">DECRETO 260/2011, de 26 de julio</a>, por el que se establecen las enseñanzas artísticas superiores de Grado en Música en Andalucía (BOJA 23-08-2011).&nbsp;<a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto260-2011Musica.pdf">Descargar disposición en pdf (51,4 MB)</a></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-22</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Danza</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.juntadeandalucia.es/boja/boletines/2011/164/d/7.html">DECRETO 258/2011, de 26 de julio</a>, por el que se establecen las enseñanzas artísticas superiores de Grado en Danza en Andalucía (BOJA 22-08-2011).&nbsp;<a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto258-2011Danza.pdf">Descargar disposición en pdf (77 MB)</a></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-19</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Acceso a la universidad</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.iesmigueldecervantes.es/normas/acuerdos/CorreccionErroresAcuerdo11marzo2011GradoUniversidad.pdf">CORRECCIÓN de errores del Acuerdo de 11 de marzo de 2011</a>, de la Dirección General de Universidades, Comisión del Distrito Único Universitario de Andalucía, por el que se establece el procedimiento para el ingreso en los estudios universitarios de Grado (BOJA 19-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-19</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Procedimiento Disciplinario / Incompatibilidades</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden2agosto2011PotestadDisciplinariaDirectores.pdf">ORDEN de 2 de agosto de 2011</a>, por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de los centros públicos de educación no universitaria (BOJA 19-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-19</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Órganos Unipersonales</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden2agosto2011PotestadDisciplinariaDirectores.pdf">ORDEN de 2 de agosto de 2011</a>, por la que se regula el procedimiento para el ejercicio de la potestad disciplinaria de los directores y directoras de los centros públicos de educación no universitaria (BOJA 19-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-16</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Inspección Educativa: Oposiciones</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/CorreccionErrataResolucion27junio2011OposicionesInspeccion.pdf">Corrección de errata de la RESOLUCIÓN de 27 de junio de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al Cuerpo de Inspectores de Educación en plazas de ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 16-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-11</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Centros Docentes</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden20julio2011AutorizacionEnsenanzas1112.pdf">ORDEN de 20 de julio de 2011</a>, por la que se modifica la autorización de enseñanzas en centros docentes públicos a partir del curso escolar 2011/12 (BOJA 11-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Cooperación con Entidades Locales</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden6julio2011SubvencionesEscuelasMusicaDanza.pdf">ORDEN de 6 de julio de 2011</a>, por la que se establecen las bases reguladoras para la concesión de subvenciones a las Escuelas de Música y Danza dependientes de entidades locales y se efectúa su convocatoria para el año 2012 (BOJA 09-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-09</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Cultura Emprendedora</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/CorreccionErroresDecreto219CulturaEmprendedora.pdf">CORRECCIÓN de errores del Decreto 219/2011</a>, de 28 de junio, por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el sistema educativo público de Andalucía (BOJA 09-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-08-08</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">Centros Docentes</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden15julio2011PuestosCentrosInfantil.pdf">ORDEN de 15 de julio de 2011</a>, por la que se autoriza la denominación específica, así como el número de puestos escolares de primer ciclo de educación infantil, a determinadas escuelas infantiles de titularidad municipal (BOJA 08-08-2011).</td>
</tr>
<tr>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;">2011-08-03</p>
</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Bachillerato</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/CorreccionErroresOrden15junio2011PremiosBachillerato.pdf" style="text-decoration: none; color: #005fa9;">CORRECCIÓN de errores de la Orden de 15 de junio de 2011</a>, por la que se convocan los premios extraordinarios de bachillerato correspondientes al curso académico 2010/2011 (BOJA 03-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-03</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Danza</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto253ModificaEnsenanzasProfesionalesDanza.pdf" style="text-decoration: none; color: #005fa9;">DECRETO 253/2011, de 19 de julio</a>, por el que se modifica el Decreto 240/2007, de 4 de septiembre, por el que se establece la ordenación y currículo de las enseñanzas profesionales de danza en Andalucía (BOJA 03-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-03</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Funcionarios Docentes: Oposiciones / Temarios / Fase de prácticas</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden18julio2011NombramientoMaestrosPracticas.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 18 de julio de 2011</a>, por la que se hacen públicas las listas del personal seleccionado en el procedimiento selectivo para el ingreso en el Cuerpo de Maestros, y se le nombra con carácter provisional funcionario en prácticas (BOJA 03-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-02</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Leyes Ordinarias</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/Leyes/Ley27-2011ActualizacionSeguridadSocial.pdf" style="text-decoration: none; color: #005fa9;">LEY 27/2011, de 1 de agosto</a>, sobre actualización, adecuación y modernización del sistema de Seguridad Social (BOE 02-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-08-02</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Leyes Ordinarias</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/Leyes/Ley26-2011DerechosPersonasDiscapacidad.pdf" style="text-decoration: none; color: #005fa9;">LEY 26/2011, de 1 de agosto</a>, de adaptación normativa a la Convención Internacional sobre los Derechos de las Personas con Discapacidad (BOE 02-08-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Formación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/OrdenMEC18julio2011AyudasEstudiosExtranjero.pdf" style="text-decoration: none; color: #005fa9;">ORDEN EDU/2127/2011, de 18 de julio</a>, por la que se establecen las bases y se convocan ayudas para el alumnado que curse estudios en niveles no universitarios en el exterior (BOE 28-07-2011). (Plazo: hasta el 26-09-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-28</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Formación Profesional Inicial</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/OrdenMEC15julio2011PremiosNacionalesFPGradoSuperior.pdf" style="text-decoration: none; color: #005fa9;">ORDEN EDU/2128/2011, de 15 de julio</a>, por la que se crean y regulan los Premios Nacionales de Formación Profesional de grado superior establecidos por la Ley Orgánica 2/2006, de 3 de mayo, de Educación (BOE 28-07-2011).</td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-27</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><strong>Organización y Funcionamiento</strong></td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><strong><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/AclaracionesROCsecundaria27julio2011.pdf" style="text-decoration: none; color: #005fa9;">Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria</a>, aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 27 de julio de 2011).</strong></td>
</tr>
<tr>
<td width="70" style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">2011-07-26</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;">Conciertos Educativos</td>
<td style="vertical-align: top; text-align: left; font: normal normal normal 11px/normal arial; color: #333333; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden4julio2011ConciertosEductivos.pdf" style="text-decoration: none; color: #005fa9;">ORDEN de 4 de julio de 2011</a>, por la que se resuelve la convocatoria de la Orden que se indica para el acceso al régimen de conciertos educativos o la renovación o modificación de los mismos con centros docentes privados de la Comunidad Autónoma de Andalucía, a partir del curso académico 2011/12 (BOJA 26-07-2011).</td>
</tr>
</tbody>
</table>
</div>
<br />
<div style="text-align: center;"></div>
<div></div>
<table align="center" width="98%" cellspacing="4" border="0" class="datos" style="border-collapse: collapse; width: auto; margin: 1px;">
<tbody>
<tr>
<td colspan="2" class="cabecera" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"></td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">
<p style="color: #333333;">2011-07-23</p>
</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/rdecre/RD879-2011TDSSalvamentoySocorrismo.pdf">REAL DECRETO 879/2011, de 24 de junio</a>, por el que se establece el título de Técnico Deportivo Superior en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BPE 23-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-22</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/rdecre/RD878-2011TDSalvamentoySocorrismo.pdf">REAL DECRETO 878/2011, de 24 de junio</a>, por el que se establece el título de Técnico Deportivo en Salvamento y Socorrismo y se fijan sus enseñanzas mínimas y los requisitos de acceso (BOE 22-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-22</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/instruc/Instrucciones22julio2011GratuidadLibrosTexto.pdf">INSTRUCCIONES de 22 de julio de 2011</a>, de la Dirección General de Participación e Innovación Educativa,&nbsp;<strong>sobre el programa de gratuidad de los libros de texto para el curso escolar 2010/2011.</strong></td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-21</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden5julio2011Becas6000.pdf">ORDEN de 5 de julio de 2011</a>, conjunta de las Consejerías de Educación y Empleo, por la que se establecen las&nbsp;<strong>Bases Reguladoras de la Beca 6000</strong>, dirigida a facilitar la permanencia en el Sistema Educativo del alumnado de Bachillerato y de Ciclos Formativos de Grado Medio de Formación Profesional Inicial y se efectúa su convocatoria para el curso 2011/2012 (BOJA 21-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-21</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto227-2011SupervisionLibrosTexto.pdf">DECRETO 227/2011 de 5 de julio</a>, por el que se regula el depósito, el registro y la supervisión de los&nbsp;<strong>libros de texto</strong>, así como el<strong> procedimiento de selección </strong>de los mismos por los centros docentes públicos de Andalucía (BOJA 21-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-15</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/AclaracionesROC-Secundaria20junio2011.pdf">Aclaraciones en torno al Reglamento Orgánico de los institutos de Educación Secundaria</a>, aprobado por el DECRETO 327/2010, de 13 de julio, y a la Orden de 20 de agosto de 2010, por la que se regula la&nbsp;<strong>Organización y Funcionamiento de los institutos de Educación Secundaria, así como el horario de los centros, del alumnado y del profesorado (actualización de 20 de junio de 2011).</strong></td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-14</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/decretos/Decreto219_2011PlanFomentoCulturaEmprendedora.pdf">DECRETO 219/2011, de 28 de junio</a>, por el que se aprueba el Plan para el Fomento de la Cultura Emprendedora en el Sistema Educativo Público de Andalucía (BOJA 14-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-12</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden29junio2011AutorizacionCentrosBilingues.pdf">ORDEN de 29 de junio de 2011</a>, por la que se establece el procedimiento para la autorización de la enseñanza bilingüe en los centros docentes de titularidad privada (BOJA 12-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-12</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/ordenes/Orden28junio2011EnsenanzaBilingue.pdf">ORDEN de 28 de junio de 2011</a>, por la que&nbsp;<strong>se regula la enseñanza bilingüe en los centros docentes </strong>de la Comunidad Autónoma de Andalucía (BOJA 12-07-2011).</td>
</tr>
<tr>
<td align="center" width="70" valign="top" style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;">2011-07-12</td>
<td style="vertical-align: top; text-align: left; padding: 2px; border: 1px solid #9a9542;"><a target="_blank" href="http://www.adideandalucia.es/normas/resoluc/Resoluc27junio2011ListasProvOposicionesInspeccion.pdf">RESOLUCIÓN de 27 de junio de 2011</a>, de la Dirección General de Profesorado y Gestión de Recursos Humanos, por la que se declaran aprobadas las listas provisionales de las personas admitidas y excluidas en los procedimientos selectivos para el acceso al cuerpo de Inspectores de Educación en plazas del ámbito de gestión de la Comunidad Autónoma de Andalucía, convocados por Orden que se cita (BOJA 12-07-2011).</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>				</div>
			            
						<dl>
				<dt>
					Número de Artículos:				</dt>
				<dd>
					9				</dd>
			</dl>
			
					</li>
																									</ul>
    </div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:64:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Estadísticas";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:102:"/index.php?option=com_content&amp;view=category&amp;id=196&amp;Itemid=717&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:103:"/index.php?option=com_content&amp;view=category&amp;id=196&amp;Itemid=717&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"Estadísticas";s:4:"link";s:59:"index.php?option=com_content&view=article&id=277&Itemid=717";}i:1;O:8:"stdClass":2:{s:4:"name";s:9:"Normativa";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
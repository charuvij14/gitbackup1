<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:8543:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">CURSO 2014/15</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Aviso legal.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 06 Diciembre 2012 10:45</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=170:aviso-legal&amp;catid=194&amp;Itemid=507&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=7f2007c057d5fc1f556ed389a9a1c15a6bc9297e" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 7435
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p><span style="color: #000000; font-size: 18.18px; font-weight: bold; background-color: #ffffff;">Aviso legal IES Miguel de Cervantes Granada</span></p>
<table class="contentpaneopen" style="margin: 20px 0px; padding: 0px; color: #333333; font-family: Arial, Helvetica, sans-serif; font-size: 13.63px; background-color: #ffffff;">
<tbody style="margin: 0px; padding: 0px;">
<tr style="margin: 0px; padding: 0px;">
<td style="margin: 0px; padding: 0px;" valign="top">
<p style="margin-top: 0.5em; margin-bottom: 0.5em;">La persona usuaria accederá al Portal y hará uso del contenido del mismo conforme a la legalidad vigente en cada momento y a las normas de la buena fe, la moral y las buenas costumbres. La persona usuaria no podrá usar el presente sitio web con ninguna finalidad o propósito que sea ilegal o esté prohibido por los presentes términos de uso, quedando prohibida cualquier actuación en perjuicio del IES Miguel de Cervantes de Granada o de terceros.<br style="margin: 0px; padding: 0px;" /><br style="margin: 0px; padding: 0px;" />Especialmente la persona usuaria no podrá acceder al Portal de modo que dañe, deteriore, inutilice o sobrecargue los servicios y/o información ofrecida, no podrá interferir el uso de dichos servicios y/o información por otros terceros, no podrá intentar el acceso ni acceder a sitios, servicios, sistemas informáticos del Portal o a redes conectadas al Portal sin autorización cuando la misma sea preceptiva para el acceso, ni mediante actos de intrusión (hacking) o por cualquier otro medio no autorizado.<br style="margin: 0px; padding: 0px;" /><br style="margin: 0px; padding: 0px;" />El <span style="color: #000000; background-color: #ffffff;">IES Miguel de Cervantes de Granada</span> se reserva el derecho a suspender temporalmente el acceso al Portal, sin previo aviso, de forma discrecional y temporal.</p>
<p style="margin-top: 0.5em; margin-bottom: 0.5em;"><strong style="margin: 0px; padding: 0px;">Derechos de propiedad industrial e intelectual</strong></p>
<p style="margin-top: 0.5em; margin-bottom: 0.5em;"><br style="margin: 0px; padding: 0px;" />Las eventuales referencias que se hagan en el Portal a cualquier producto, servicio, proceso, enlace, hipertexto o cualquier otra información, utilizando la marca, el nombre comercial, o el nombre del fabricante o suministrador, que sean de titularidad de terceros, no constituye ni implica respaldo, relación jurídica con el titular, patrocinio o recomendación por parte del <span style="color: #000000; background-color: #ffffff;">IES Miguel de Cervantes de Granada</span>. Todos los contenidos que aparezcan en el Portal y dispongan de copyright pertenecen a sus respectivos propietarios.</p>
<p style="margin-top: 0.5em; margin-bottom: 0.5em;"><strong style="margin: 0px; padding: 0px;">Exención de responsabilidad</strong></p>
<p style="margin-top: 0.5em; margin-bottom: 0.5em;"><br style="margin: 0px; padding: 0px;" /><span style="color: #000000; background-color: #ffffff;">El IES Miguel de Cervantes de Granada</span> no garantiza la ausencia de virus en los servicios y contenidos del Portal, ya sean prestados directamente o por terceros, incluidas las conexiones a través de "links", por lo que no será responsable de los daños y perjuicios que se puedan causar como consecuencia de la existencia de dichos virus. <br style="margin: 0px; padding: 0px;" /><br style="margin: 0px; padding: 0px;" />No se garantiza la exactitud ni actualización de la información contenida en el Portal, por ello el <span style="color: #000000; background-color: #ffffff;">IES Miguel de Cervantes de Granada</span> no se hace responsable de los daños que puedan ser causados por la utilización de la misma. <br style="margin: 0px; padding: 0px;" /><br style="margin: 0px; padding: 0px;" /><span style="color: #000000; background-color: #ffffff;">El </span><span style="color: #000000; background-color: #ffffff;">IES Miguel de Cervantes de Granada</span> no se hace responsable en ningún caso de ningún daño que se pudiera causar a un tercero por los usuarios del Portal como consecuencia del uso ilegal o inadecuado del mismo, ni como consecuencia de los contenidos e informaciones accesibles o facilitadas a través de ella, ni de los sitios vinculados a la misma. Los responsables serán los usuarios o terceros causantes de los daños. <br style="margin: 0px; padding: 0px;" /><br style="margin: 0px; padding: 0px;" /><span style="color: #000000; background-color: #ffffff;">El </span><span style="color: #000000; background-color: #ffffff;">IES Miguel de Cervantes de Granada</span> no se hace responsable de los daños y perjuicios que se pudieran causar a un tercero como consecuencia de la retirada de contenidos del Portal, quedando totalmente prohibida cualquier actuación en perjuicio del <span style="color: #000000; background-color: #ffffff;">IES Miguel de Cervantes de Granada</span> o de terceros derivada de la retirada de los citados contenidos. <br style="margin: 0px; padding: 0px;" /><br style="margin: 0px; padding: 0px;" /><span style="color: #000000; background-color: #ffffff;">El </span><span style="color: #000000; background-color: #ffffff;">IES Miguel de Cervantes de Granada</span> no garantiza la licitud, fiabilidad, utilidad, veracidad, exactitud, exhaustividad y actualidad de los contenidos y servicios del Portal ya sean prestados directamente o por terceros, incluidos los links, por lo que no será responsable de los daños y perjuicios que pudieran sufrir los usuarios como consecuencia de la ilicitud, no fiabilidad, inutilidad, inexactitud, falta de veracidad, no exhaustividad y/o no actualidad de los servicios y/o contenidos.</p>
<p style="margin-top: 0.5em; margin-bottom: 0.5em;"> </p>
<p id="aui-3-2-0PR1-1109" style="margin-top: 0.5em; margin-bottom: 0.5em;"><strong id="aui-3-2-0PR1-1106" style="margin: 0px; padding: 0px;">Normativa y jurisdicción aplicable<br /></strong><br style="margin: 0px; padding: 0px;" />Será de aplicación el ordenamiento jurídico español.<br style="margin: 0px; padding: 0px;" /><br style="margin: 0px; padding: 0px;" />Cualquier disputa en relación con el Portal se sustanciará ante la jurisdicción española, sometiéndose las partes a los Juzgados y Tribunales de la ciudad de Granada, con expresa renuncia a cualquier otro fuero si lo tuvieren y fuera diferente de lo reseñado.</p>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">IES Miguel de Cervantes- General</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:62:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - AVISO LEGAL";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:6:"INICIO";s:4:"link";s:53:"index.php?option=com_content&view=featured&Itemid=250";}i:1;O:8:"stdClass":2:{s:4:"name";s:11:"AVISO LEGAL";s:4:"link";s:59:"index.php?option=com_content&view=article&id=170&Itemid=507";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:6590:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Gaudeamus Hominem</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">GAUDEAMUS HOMINEM</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 16 Diciembre 2012 09:32</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=175:gaudeamus&amp;catid=199&amp;Itemid=79&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=0560f2e156130251564bce38a598365e986a149a" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2072
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p style="text-align: center;"><span style="font-family: 'Times New Roman'; font-size: medium;"><span style="color: #99ccff; font-family: 'Times New Roman',Times,serif; font-size: xx-large; font-weight: bold;" class="Estilo13">Gaudeamus hominem o El sueño del progreso</span></span></p>
<div style="font-family: 'Times New Roman'; font-size: medium;"><span style="color: #663300; font-family: 'Times New Roman',Times,serif; font-size: medium;" class="Estilo6"><img class="Estilo6" border="0" hspace="5" vspace="5" align="right" src="archivos_ies/actividades/imagenes/iesmigueldecervantesFotos/teatro08/IMG_2732.JPG" width="267" height="200" /><img border="0" hspace="5" vspace="5" align="right" src="archivos_ies/actividades/imagenes/iesmigueldecervantesFotos/teatro08/IMG_2720.JPG" width="267" height="200" /><span style="color: #000033;" class="Estilo11">Durante todos los lunes y viernes del mes de Abril&nbsp; de 2009 se ha venido representando en el Salón de Usos Múltiples del I.E.S. Miguel de Cervantes de Granada, para alumnos y público en general, la obra teatral "Gaudeamus hominem o El sueño del progreso", como una actividad más del Plan de Lectura y Biblioteca, auspiciado por la Consejería de Educación y Ciencia. El guión ha sido concebido por el director del grupo de teatro que la representa, Juan Naveros Sánchez, para adolescentes y adultos de una sociedad cada día más egocéntrica e insensible a los verdaderos problemas de una humanidad que, en los albores del siglo XXI, se pasea por la luna, pero no ha podido o querido acabar con las guerras, con el hambre, con la corrupción, con los abusos de los poderosos, con los delirios de los dictadores... Una sociedad, en fin, que es cómplice, consciente o inconscientemente, de la transmutación de los más nobles valores de la condición humana y se conforma o consiente que sus hijos se eduquen bajo los dictados de una única moral: la de hacer fortuna como sea y casi a costa de lo que sea.<br />Sobre el escenario, un monólogo difícil, emocionante y provocador. Una mujer de origen aristocrático, bajo los efectos de unas copas de más, que la hacen especialmente locuaz, y las oscilaciones de un carácter inestable y esquizofrénico, mezcla recuerdos de su infancia y juventud con duras acusaciones a nuestro moderno mundo de ambiciones, despilfarro e insensibilidad. Por si fuera poco, la amarga pesadumb<img border="0" hspace="5" vspace="5" align="right" src="archivos_ies/actividades/imagenes/iesmigueldecervantesFotos/teatro08/IMG_2740.JPG" width="267" height="200" />re del amor despreciado persiste clavad<img border="0" hspace="5" vspace="5" align="right" src="archivos_ies/actividades/imagenes/iesmigueldecervantesFotos/teatro08/IMG_2721.JPG" width="267" height="200" />o en su memoria por el recuerdo de esas cápsulas o concentraciones de vida y de pasión que son los boleros y, en concreto, tres de ellos, cantados en directo, cuyo recuerdo desestabilizan aún más su frágil mente e inestable afectividad. En los momentos de máxima alteración anímica y angustia, sólo encuentra alivio en la poesía, "único y verdadero consuelo de las almas en pena".<br />Se trata de un teatro comprometido y de gestos y silencios elocuentes; teatro en estado puro, que pretende provocar, a través de sus escasos cuarenta y cinco minutos de duración, un breve ejercicio de higiene mental colectiva y una amplia y profunda reflexión individual. Y ya para acabar, quisiera subrayar uno de los objetivos principales de la divulgación de esta noticia que es el de estimular a todos los nostálgicos del teatro y de su inserción en el sistema educativo que, aburridos por el excesivo encorsetamiento de la enseñanza actual, no encuentran ni tiempo ni ánimo para reivindicar el papel&nbsp; insustituible del teatro en la formación integral del individuo.<br />De paso, rendir homenaje y reconocimiento público a unos alumnos que, contra viento y marea, han sido capaces de sobrellevar la privación de todos los recreos del curso (no hay disponible otra franja horaria) y enorgullecerse presentando a sus compañeros y público el resultado de un trabajo bien hecho.</span></span></div>
<div style="font-family: 'Times New Roman'; font-size: medium;"><span style="color: #663300; font-family: 'Times New Roman',Times,serif; font-size: medium;" class="Estilo6"><span style="color: #000033;" class="Estilo11">&nbsp;</span></span></div>
<p>&nbsp;</p>
<div class="fechaModif">Ultima actualización: 13/11/2010</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Actividades</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:68:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Gaudeamus Hominem";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:17:"Gaudeamus Hominem";s:4:"link";s:58:"index.php?option=com_content&view=article&id=175&Itemid=79";}}s:6:"module";a:0:{}}
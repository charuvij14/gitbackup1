<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:43050:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Dpto. de Animación Sociocultural</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Convalidaciones</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 29 Octubre 2013 18:16</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=213:convalidaciones&amp;catid=242&amp;Itemid=619&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=490ed8b50da7d6d91358692b4a1ec3415575d7d6" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2627
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Convalidaciones en Ciclos Formativos</h2>
<p><strong>NORMATIVA</strong></p>
<ul class="list-icon icon-download">
<li><a href="http://www.boe.es/boe/dias/1998/05/08/pdfs/A15389-15460.pdf" target="_blank">Real Decreto 777/1998 de 30 de Abril (Art. 12)</a></li>
<li><a href="http://www.boe.es/boe/dias/2002/01/09/pdfs/A00982-01021.pdf" target="_blank">Orden del Ministerio de Educación de 20 de diciembre de 2001 (Anexo II)</a></li>
<li><a href="http://www.juntadeandalucia.es/boja/2003/11/d5.pdf" target="_blank">Orden de la Consejería de Educación de 16 diciembre de 2002 (Anexo I)</a></li>
</ul>
<p><strong>DOCUMENTOS DESCARGABLES</strong></p>
<ul class="list-icon icon-download">
<li><a href="archivos_ies/solicitud_de_convalidaciones.pdf" target="_blank">Solicitud de convalidación</a></li>
</ul></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Pruebas extraordinarias</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:14</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=212:pruebas-extraordinarias&amp;catid=242&amp;Itemid=618&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=2b647198e2357e2e1ab5674856add48058084b9d" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2435
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Pruebas extraordinarias en Ciclos Formativos </h2>
<div id="articlepxfontsize1"><strong>NORMATIVA</strong>
<ul class="list-icon icon-article">
<li><a href="http://www.juntadeandalucia.es/boja/2010/202/d2.pdf" target="_blank">ORDEN de 29 de septiembre de 2010 (BOJA nº 202 de 15-10-2010).</a></li>
</ul>
<strong>DOCUMENTOS DESCARGABLES</strong>
<ul class="list-icon icon-download">
<li><a href="archivos_ies/solicitud_convo_gracia.pdf" target="_blank">Impreso de Solicitud</a></li>
</ul>
<strong>PRUEBA EXTRAORDINARIA DE EVALUACIÓN (LOGSE)</strong>
<div style="text-align: justify;">El alumnado que haya<strong> agotado el número máximo de veces que puede tener calificación final en un módulo profesional determinado (4 convocatorias)</strong>, podrá solicitar convocatoria extraordinaria siempre que concurran algunas de las circunstancias que se establecen a continuación:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li><strong>Enfermedad prolongada</strong> o accidente del alumno o alumna.</li>
<li><strong>Incorporación</strong> o desempeño de un <strong>puesto de trabajo</strong> en un horario incompatible con las enseñanzas del ciclo formativo.</li>
<li>Por <strong>cuidado de hijo o hija</strong> menor de 16 meses o por accidente grave, enfermedad grave y hospitalización del cónyuge o análogo familiar hasta el segundo grado de parentesco por consaguinidad o afinidad. </li>
</ol><strong>PLAZO Y PRESENTACIÓN DE SOLICITUDES</strong></div>
<div style="text-align: justify;">La solicitud a la que se refiere el apartado anterior estará dirigida al Director o Directora del Centro donde la persona solicitante cursó por última vez el módulo profesional correspondiente.<strong> A dicha solicitud se adjuntarán los documentos acreditativos o explicación motivada de la circunstancia que concurra</strong>.</div>
<ul class="list-icon icon-calendar">
<li>Plazo de presentación de las solicitudes: <strong>Del 1 al 30 de Octubre de cada año.</strong></li>
</ul>
<div style="text-align: justify;">En caso de presentación incompleta se requerirá a la persona solicitante para que en el plazo de <strong>diez días</strong> acompañe los documentos preceptivos, con indicación de que, si así no lo hiciera, se le tendrá por desistido de su petición, archivándose sin más trámite.<br /><br /></div>
<div style="text-align: justify;"><strong>DESARROLLO DE LAS PRUEBAS</strong></div>
<div style="text-align: justify;">Cuando se trate de una resolución favorable, la Dirección del Centro educativo comunicará al Departamento o Departamentos correspondientes que procedan a la organización de las pruebas extraordinarias de evaluación de los módulos profesionales correspondientes, que <strong>serán antes de la finalización del Primer Trimestre del curso académico.</strong> <br />En ningún caso la autorización de la prueba extraordinaria conllevará una nueva matriculación, excepto si se trata del módulo profesional de Formación en Centros de Trabajo.<br /><br /></div>
<div style="text-align: justify;"><strong>CONTENIDO DE LAS PRUEBAS</strong></div>
<div style="text-align: justify;">Las pruebas extraordinarias de evaluación se adecuarán al currículo oficial establecido en la normativa vigente que regula cada uno de los títulos de Formación Profesional Específica, teniendo en cuenta los objetivos generales, las capacidades terminales, los criterios de evaluación y los contenidos correspondientes al módulo profesional del que se trate.</div>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Prueba de acceso</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:13</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=211:prueba-de-acceso&amp;catid=242&amp;Itemid=617&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=2a92dc263128a8db6e3c8385f832f846511e3822" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2418
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Prueba de Acceso a Ciclos Formativos</h2>
<h3 class="plg_fa_karmany"><span style="color: #003366;"><strong>Información General</strong></span></h3>
<div id="articlepxfontsize1">
<div style="text-align: justify;"><strong>LUGAR Y FECHAS DE INSCRIPCIÓN</strong></div>
<div style="text-align: justify;">Cada año, y con antelación suficiente, la Consejería de Educación determinará los Centros en los que se realizarán dichas Pruebas. Las fechas de inscripción serán las siguientes:
<ul class="list-icon icon-calendar">
<li>Para la Convocatoria Ordinaria: La Primera Quincena de Mayo.</li>
<li>Para la Convocatoria Extraordinaria: La Segunda Quincena de Julio.</li>
</ul>
<strong>LUGAR Y FECHAS DE REALIZACIÓN</strong>
<div style="text-align: justify;">Las Pruebas se realizarán en el Centro en el que se presentó la solicitud. Las fechas de realización son las siguientes:
<ul class="list-icon icon-calendar">
<li>Convocatoria Ordinaria: el 5 de Junio de cada año (si fuera sábado o festivo, el día hábil siguiente).</li>
<li>Convocatoria Extraordinaria: el 7 de Septiembre de cada año (si este fuera domingo sería el 5 de Septiembre y si fuera sábado o festivo, el siguiente día hábil).</li>
</ul>
</div>
<div style="text-align: justify;"><strong>EFECTOS DE HABER SUPERADO LA PRUEBA </strong></div>
<div style="text-align: justify;"><ol>
<li>La superación de la Prueba de Acceso a la Formación Profesional Específica faculta a las personas que no poseen requisitos académicos para cursar las enseñanzas correspondientes a los Ciclos Formativos de F.P.</li>
<li>La superación de la citada Prueba no supone la posesión de ninguna titulación académica ni tampoco equivale a haber superado alguna área de la ESO o alguna materia de Bachillerato.</li>
<li>La superación de la Prueba no garantiza ni proporciona el derecho a una puesto escolar. Para ser admitidos/as en un Ciclo Formativo, es requisito indispensable presentar solicitud de admisión en el Centro donde se desee cursar estudios, del 1 al 25 de junio o del 1 al 10 de septiembre de cada año, y una vez superada la prueba.</li>
<li>Una vez superada la Prueba, sus efectos tendrán validez en todo el territorio español.</li>
</ol></div>
<div style="text-align: justify;"><strong>RECLAMACIONES </strong></div>
<ul class="list-arrow arrow-blue">
<li><strong>En Primera Instancia</strong><br />Los/as interesados/as o su padres, si son menores de edad, si no estuvieran conformes con la Calificación obtenida en la Prueba, podrán presentar ante la Comisión de valoración la correspondiente reclamación, en el plazo de 2 días hábiles desde la publicación de las actas. En el plazo máximo de 2 días hábiles, la Comisión deberá resolver todas las reclamaciones presentadas.</li>
<li><strong>En Segunda Instancia</strong><br />En caso de que los/as interesados/as o su padres, si son menores de edad, no estuvieran conformes con la resolución de la Comisión, podrán presentar recurso de alzada ante la persona titular de la Delegación Provincial de Educación en el plazo de un mes, de conformidad con lo establecido en los artículos 114 y 115 de la Ley 30/1992, de 26 de noviembre.</li>
</ul>
</div>
<h3 class="text-tip"><span style="color: #800000;"><strong>Pruebas de Acceso de Grado Medio</strong></span></h3>
<div style="text-align: justify;"><strong>NORMATIVA</strong></div>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/boja/2008/90/d2.pdf" target="_blank">Orden de 23 de Abril de 2008 (BOJA nº 90 de 07-05-2008)</a></li>
</ul>
<div style="text-align: justify;"><strong>DOCUMENTOS </strong></div>
<ul class="list-icon icon-download">
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1640-requisitos-y-estructura-grado-medio" target="_blank">Requisitos y estructura</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1644-contenidos-y-criterios-de-evaluacion-grado-medio" target="_blank">Temario de contenidos (Anexo III)</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1642-exenciones-grado-medio" target="_blank">Exenciones de materias ESO (Anexo IV)</a></li>
</ul>
<div style="text-align: justify;"><br /><strong>SOLICITANTES</strong></div>
<div style="text-align: justify;">Las personas que deseen realizar la Prueba de Acceso a un Ciclo Formativo de Grado Medio, deben tener <strong>cumplidos 17 años</strong>, o cumplirlos en el año natural de celebración de la Prueba. Para ello deberán presentar la Solicitud de Inscripción según el Modelo Anexo I, en el Centro en el que se pretenda realizar la Prueba.</div>
<div style="text-align: justify;"><br /><strong>ESTRUCTURA Y CONTENIDOS DE LA PRUEBA</strong></div>
<div style="text-align: justify;">Las Pruebas se estructurarán en tres partes:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-alpha;">
<li>Comunicación</li>
<li>Social</li>
<li>Científico-tecnológica</li>
</ol></div>
<div style="text-align: justify;">Los contenidos aparecen recogidos en el Anexo III.</div>
<div style="text-align: justify;">Para la homogeneizar criterios, la Dirección General facilitará a la Comisión de las Pruebas de Acceso la Prueba ya elaborada, así como los criterios de corrección.</div>
<div style="text-align: justify;"><br /><strong>EXENCIONES DE REALIZACIÓN</strong></div>
<div style="text-align: justify;">Estarán <strong>exentas de realizar la totalidad de la Prueba de Acceso </strong>aquellas personas que reúnan alguno de los siguientes requisitos:<ol style="list-style-type: lower-roman;">
<li>Tener superada la Prueba de Acceso a la Universidad para Mayores de 25 años.</li>
<li>Tener superada la Prueba de Acceso a un Grado Superior.</li>
</ol></div>
<h3 class="text-comment"><span style="color: #9b2626;"><strong>Pruebas de Acceso de Grado Superior</strong></span></h3>
<strong>NORMATIVA</strong>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/boja/2008/90/d2.pdf" target="_blank">Orden de 23 de Abril de 2008 (BOJA nº 90 de 07-05-2008)</a></li>
</ul>
<strong>DOCUMENTOS DESCARGABLES</strong>
<ul class="list-icon icon-download">
<li><a href="archivos_ies/anexo_ii_solicitud_gs_rellenable.pdf" target="_blank">Inscripción Prueba de Acceso (anexo II)</a></li>
<li><a href="archivos_ies/temario-parte-comun-anexo-v.pdf" target="_blank">Temario parte común (anexo V)</a></li>
<li><a href="archivos_ies/GS%20ANEXO%20VI.pdf" target="_blank">Opciones (anexo VI)</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1645-contenidos-y-criterios-de-evaluacion-grado-superior" target="_blank"><strong>CONTENIDOS Y CRITERIOS DE EVALUACIÓN</strong></a></li>
<li><a href="archivos_ies/EXENCIONES%20GS%20completo%20BUENO.pdf" target="_blank">Exención de materias de Bachillerato (anexo VIII)</a></li>
</ul>
<strong>SOLICITANTES</strong>
<div style="text-align: justify;">Las personas que deseen realizar la Prueba de Acceso a un Ciclo Formativo de Grado Superior, deberán cumplir alguno de los siguientes requisitos:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li>Tener<strong> cumplidos 19 años</strong>, o cumplirlos a lo largo del año natural de celebración de la Prueba.</li>
<li>Tener <strong>cumplidos 18 años</strong>, o cumplirlos a lo largo del año natural de celebración de la Prueba, y estar en posesión de un Título de Grado Medio relacionado con aquel de Grado Superior al que se desea acceder.</li>
</ol></div>
<div style="text-align: justify;">Para ello deberán presentar la Solicitud de Inscripción según el Modelo Anexo II, en el Centro en el que se pretenda realizar la Prueba.<br /><br /></div>
<div style="text-align: justify;"><strong>ESTRUCTURA Y CONTENIDOS DE LA PRUEBA</strong></div>
<div style="text-align: justify;">La Prueba contará de dos partes:</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-alpha;">
<li>Parte Común.</li>
<li>Parte Específica.</li>
</ol></div>
<div style="text-align: justify;">a)<strong> La Parte Común</strong>, constará de 3 ejercicios:</div>
<div style="text-align: justify;"><ol style="list-style-type: upper-roman;">
<li>Lengua Española</li>
<li>Matemáticas</li>
<li>Lengua Extranjera</li>
</ol></div>
<div style="text-align: justify;">Esta parte se considerará superada cuando habiendo obtenido al menos 3 puntos en cada ejercicio, la media aritmética sea igual o superior a 5 puntos. Los contenidos de esta parte aparecen recogidos en el Anexo V.</div>
<div style="text-align: justify;"><br />b) En la <strong>Parte Específica</strong>, se ofertarán 3 materias distintas, según el Ciclo al que se pretende acceder. Estas materias vienen recogidas en el Anexo VI, debiendo el/la alumno/a elegir 2.</div>
<div style="text-align: justify;"><br />Por ejemplo, para Administración y Finanzas (Opción A), las materias ofertadas son:</div>
<div style="text-align: justify;"><ol style="list-style-type: upper-roman;">
<li>Economía de la Empresa</li>
<li>Geografía</li>
<li>Segunda Lengua Extranjera</li>
</ol></div>
<div style="text-align: justify;">Los contenidos de esta parte aparecen recogidos en el Anexo VII.</div>
<div style="text-align: justify;">Esta parte se considerará superada cuando habiendo obtenido al menos 3 puntos en cada ejercicio, la media aritmética sea igual o superior a 5 puntos.</div>
<div style="text-align: justify;">Para la homogeneizar criterios, la Dirección General facilitará a la Comisión de las Pruebas de Acceso la Prueba ya elaborada, así como los criterios de corrección.</div>
<div style="text-align: justify;"><br /><strong>EXENCIONES DE REALIZACIÓN</strong></div>
<div style="text-align: justify;"><strong>Exención Total</strong>: Estarán exentas de realizar la totalidad de la Prueba de Acceso aquellas personas que reúnan el siguiente requisito:</div>
<div style="text-align: justify;">
<ul style="list-style-type: disc;">
<li>Tener superada la Prueba de Acceso a la Universidad para Mayores de 25 años.</li>
</ul>
</div>
<div style="text-align: justify;"><strong>Exención Parcial</strong>: Estarán exentas de realizar alguna de las partes de la Prueba de Acceso aquellas personas que reúnan alguno de los siguientes requisitos:</div>
<div style="text-align: justify;"><br />DE LA PARTE ESPECÍFICA</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li>Estar en posesión del Título de Técnico relacionado con aquel Ciclo al que se pretende acceder.</li>
<li>Estar en posesión de un certificado de profesionalidad de alguna de las familias profesionales incluidas en la opción por la que se presenta, de un nivel competencial 2 ó superior.</li>
<li>Acreditar una experiencia laboral de al menos un año con jornada completa en el campo profesional correspondiente a la familia de la opción por la que se presente, que se deberá justificar de la siguiente forma:</li>
</ol></div>
<div style="text-align: justify; margin-left: 30px;">
<ul style="list-style-type: circle;">
<li>Trabajadores por cuenta ajena: Mediante Certificación de la Tesorería General de la Seguridad Social.</li>
<li>Trabajadores pro cuenta propia: Mediante Certificación del periodo de cotización en régimen de autónomos.</li>
<li>Tener aprobadas las materias de Bachillerato de las que conste la parte específica.</li>
</ul>
</div>
<div style="text-align: justify;">DE LA PARTE COMÚN</div>
<div style="text-align: justify;"><ol style="list-style-type: lower-roman;">
<li>Tener aprobadas las materias de Bachillerato que constituyen la parte Común de la Prueba.</li>
<li>Tener superada la Prueba de Acceso a otro Ciclo Formativo de Grado Superior.</li>
</ol></div>
<div style="text-align: justify;"><strong>Nota:</strong> Las exenciones de los apartados anteriores podrán ser acumulables.<br /><br /></div>
<h3 class="text-alert"><span style="color: #800000;"><strong>Evaluación y Calificación de las pruebas</strong></span></h3>
La calificación de cada una de las partes de la Prueba será numérica, entre 0 y 10. <br />
<ul style="list-style-type: circle;">
<li>La nota final de la Prueba se calculará siempre que se obtenga al menos una puntuación de 4 en cada una de las partes, y será la media aritmética de éstas, expresada con 2 decimales.</li>
<li>Para aquellos/as que hayan realizado y superado el Curso de Preparación de la Prueba de Acceso, a la nota final obtenida en la Prueba de Acceso se le sumará el resultado de multiplicar la nota obtenida en el Curso de Preparación multiplicada por el coeficiente 0,15. </li>
<li>Se considera superada la Prueba de Acceso cuando la nota final sea igual o superior a 5 puntos, siendo la calificación máxima de 10 puntos.</li>
<li>A efectos de cálculo de la nota final de la Prueba, no se tendrán en cuenta las partes de la Prueba exentas, haciéndolo constar así en las actas de evaluación mediante la expresión EX en la casilla correspondiente.</li>
<li>El acta de la convocatoria ordinaria se deberá exponer en el Tablón de Anuncios a partir del 2º día hábil, contado desde el de celebración de la Prueba.</li>
<li>El acta de la convocatoria extraordinaria se deberá exponer en el Tablón de Anuncios el día siguiente de celebración de la Prueba.</li>
</ul>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Escolarización</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:09</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=210:escolarizacion&amp;catid=242&amp;Itemid=611&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=884998093e6e5b640c31018c05faab8ca855ef82" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2751
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2>ESCOLARIZACIÓN</h2>
<ul>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=205&amp;Itemid=612">Reserva de plaza</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=206&amp;Itemid=613">Matricula y anulación</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=207&amp;Itemid=614">Lista de admitidos</a></li>
</ul>
<p> </p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                            <div class="item column-2">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Ciclos Formativos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:12</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=208:ciclos-formativos&amp;catid=242&amp;Itemid=615&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=1ed708e4d35b2b29e6a4eb09891bcbeb137d34ea" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 3063
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h1>Ciclos Formativos</h1>
<ul>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/" target="_blank">PORTAL OFICIAL DE FORMACIÓN PROFESIONAL</a></li>
</ul>
<ul>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=209&amp;Itemid=616">Admisión en los Ciclos</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=211&amp;Itemid=617">Prueba de acceso</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=212&amp;Itemid=618">Pruebas Extraordinarias</a></li>
<li><a href="index.php?option=com_content&amp;view=article&amp;id=213&amp;Itemid=619">Convalidaciones</a></li>
</ul>
<p> </p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
                            <div class="items-row cols-2 row-1">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Admisión en Ciclos Formativos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:11</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=209:admision-en-ciclos-formativos&amp;catid=242&amp;Itemid=616&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=c4b4d6c7f4e4c762d2ba78bf11f508ae80a0043d" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2565
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h2 class="contentheading">Admisión Ciclos Formativos</h2>
<p><strong>I. NORMATIVA REGULADORA</strong></p>
<ul class="list-icon icon-article">
<li><a href="http://www.juntadeandalucia.es/boja/2007/107/d1.pdf" target="_blank">Orden de 14 de Mayo de 2007, (BOJA nº 107 de 31-05-2007)</a></li>
</ul>
<p>II.<strong> DOCUMENTOS DESCARGABLES Y RELLENABLES</strong></p>
<ul class="list-icon icon-download">
<li><span class="jsn-listbullet"><a href="archivos_ies/solicitud_plaza_grado_superior.pdf" target="_blank">Admisión</a><br /></span></li>
<li><a href="archivos_ies/solicitud_reserva_plaza.pdf" target="_blank">Reserva de puesto escolar</a></li>
<li><a href="archivos_ies/anexo_ii_solicitud_gs_rellenable.pdf" target="_blank">Inscripción Prueba de Acceso (anexo II)</a></li>
<li><a href="archivos_ies/temario-parte-comun-anexo-v.pdf" target="_blank">Temario parte común (anexo V)</a></li>
<li><a href="archivos_ies/GS%20ANEXO%20VI.pdf" target="_blank">Opciones (anexo VI)</a></li>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso/1645-contenidos-y-criterios-de-evaluacion-grado-superior" target="_blank"><strong>CONTENIDOS Y CRITERIOS DE EVALUACIÓN</strong></a></li>
<li><a href="archivos_ies/EXENCIONES%20GS%20completo%20BUENO.pdf" target="_blank">Exención de materias de Bachillerato (anexo VIII)</a></li>
<li><a href="archivos_ies/solicitud_convo_gracia.pdf" target="_blank">Solicitud de convocatoria extraordinaria</a></li>
<li><a href="archivos_ies/solicitud_de_convalidaciones.pdf" target="_blank">Solicitud de convalidaciones</a></li>
</ul>
<ul>
<li><a href="http://formacionprofesional.ced.junta-andalucia.es/index.php/pruebas-de-acceso" target="_blank">PRUEBAS DE ACCESO: ÍNDICE</a> (Web Consejerái)</li>
</ul>
<p> </p>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion" target="_blank">Para más información</a></li>
</ul>
<p><strong>III. OFERTA</strong><br />La oferta educativa está disponible en la web del Instituto, en los Institutos, en las Delegaciones Provinciales y en la Web de la Consejería de Educación:</p>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/educacion" target="_blank">Web de la Consejería</a></li>
</ul>
<p><strong>IV. DOCUMENTACIÓN</strong></p>
<ul style="list-style-type: disc;">
<li>Cada solicitante presentará UNA ÚNICA SOLICITUD</li>
<li>Para presentar la solicitud es imprescindible estar en posesión de los requisitos académicos exigidos para realizar los estudios solicitados.</li>
<li>Impreso de solicitud: Anexo I para los Ciclos de Grado Medio o Anexo II para los Ciclos de Grado Superior, debidamente cumplimentado. En él se relacionarán, por orden de preferencia, los códigos de los ciclos formativos que se desee cursar, indicando en cada caso, si el ciclo es de Régimen General (G) o de Educación de Adultos (A) y los códigos de los centros correspondientes. Asimismo se acompañará de la siguiente documentación: </li>
</ul>
<p> </p>
<ol style="list-style-type: lower-alpha;">
<li style="list-style-type: none;"><ol style="list-style-type: lower-alpha;">
<li>Fotocopia del DNI, Pasaporte, Libro de Familia u otro documento oficial acreditativo de la identidad y de la edad.</li>
<li>Certificación académica personal, donde conste la nota media de los estudios realizados, o certificado de haber superado la prueba de acceso. </li>
<li>Fotocopia del dictamen emitido por el organismo público competente, en caso de padecer minusvalía.</li>
<li>Los solicitantes con estudios realizados en el extranjero deberán presentar, junto a la documentación antes indicada, la homologación de su titulación o, en su caso, el volante de solicitud de homologación sellado por la Delegación Provincial del Ministerio de Educación y Ciencia (tiene validez provisional durante 6 meses). </li>
</ol></li>
</ol>
<p><strong>V. LUGAR DE PRESENTACIÓN</strong></p>
<ol style="list-style-type: lower-alpha;">
<li>En el Centro Educativo solicitado en primer lugar.</li>
<li>En los registros de cualquier órgano administrativo, perteneciente a la Administración General del Estado, a la Administración de las Comunidades Autónomas.</li>
<li>Por Correo Certificado. </li>
</ol>
<p><strong>Nota:</strong> La presentación de solicitudes en más de un centro educativo dará lugar a que sólo se tenga en cuenta la última presentada. <br /><br /><strong>VI. PLAZOS DE PRESENTACIÓN</strong></p>
<ul class="list-icon icon-calendar">
<li>Del 1 al 25 de junio para el alumnado de 4º de ESO, 2º ESA o 2º de Bachillerato que haya aprobado todo el curso en junio o la Prueba de Acceso en este mismo periodo.</li>
<li>Del 1 al 10 de septiembre para el alumnado de 4º de ESO, 2º ESA o 2º de Bachillerato que haya aprobado todo el curso en septiembre o la Prueba de Acceso en este mismo periodo.</li>
<li>Del 3 al 5 de octubre (Procedimiento Extraordinario), para el alumnado que no haya presentado Solicitud en los dos procesos anteriores.</li>
</ul>
<p><strong>Nota:</strong> La solicitud será única para todo el proceso. La duplicidad de solicitud en junio y septiembre implica la desestimación. <br /><br /><strong>VI. PUBLICACIÓN DE DATOS PROVISIONALES</strong></p>
<ul class="list-icon icon-calendar">
<li>29 de junio, para las solicitudes presentadas entre el 1 y el 25 de junio.</li>
<li>13 de septiembre, para las solicitudes presentadas del 1 al 10 de septiembre.</li>
<li>9 de octubre, para las solicitudes del Proceso Extraordinario presentadas del 3 al 5 de octubre.</li>
</ul>
<p><strong>Nota:</strong> Es aconsejable consultar los listados provisionales cuando se publiquen en el tablón de anuncios del Centro o en la web de la Consejería de Educación (Novedades)<br /><br /><strong> VII. PERIODO DE RECLAMACIONES</strong></p>
<ul class="list-icon icon-calendar">
<li>Del 29 de junio al 4 de julio para el alumnado solicitante de junio.</li>
<li>13 y 14 de septiembre para el alumnado solicitante de septiembre.</li>
<li>9 y 10 de octubre para las solicitudes del Proceso Extraordinario.</li>
</ul>
<p><strong>Nota:</strong> Las reclamaciones se presentarán en el centro solicitado en primer lugar. Todas aquellas que se presenten fuera de plazo serán desestimadas. <br /><br /><strong>VIII. ADJUDICACIÓN DE PUESTOS ESCOLARES</strong></p>
<ul class="list-icon icon-calendar">
<li>10 de julio, 1ª adjudicación.</li>
<li>23 de julio, 2ª adjudicación.</li>
<li>20 de septiembre, 3ª adjudicación.</li>
<li>26 de septiembre, 4ª adjudicación.</li>
<li>11 de octubre, Adjudicación Extraordinaria</li>
</ul>
<p><strong>IX. MATRÍCULA O RESERVA DE PLAZA</strong><br />La matrícula se realizará en el centro educativo donde se ha obtenido plaza, y es obligatoria si dicha plaza corresponde a la solicitada en primer lugar.<br />La reserva se realizará, mediante un Anexo V, en el centro educativo donde se ha obtenido plaza y es obligatoria, (si no se opta por la matrícula) con el fin de obtener una opción más favorable en el proceso, en caso de no obtener plaza en la primera opción. Mientras no se haga efectiva la matrícula, es obligatoria en cada adjudicación realizar la reserva correspondiente. <br />Los plazos establecidos son los siguientes:</p>
<ul class="list-icon icon-calendar">
<li>Del 25 al 30 de junio, para el alumnado que repite curso y 2º curso.</li>
<li>Del 10 al 16 de julio, para la 1ª adjudicación.</li>
<li>Del 3 al 8 de septiembre, para la 2ª adjudicación.</li>
<li>Del 20 al 24 de septiembre, para la 3ª adjudicación.</li>
<li>Del 26 al 28 de septiembre, para la 4ª adjudicación.</li>
<li>Del 15 al 16 de octubre, para la Matrícula Extraordinaria</li>
</ul>
<p><strong>Nota: </strong>No realizar matrícula o reserva en los plazos establecidos para ello, supondrá la renuncia a seguir participando en el proceso. La reserva se repetirá en cada adjudicación, aunque se haya obtenido el mismo centro.</p>
<ul class="list-icon icon-online">
<li><a href="http://www.juntadeandalucia.es/educacion/webportal/web/portal-escolarizacion/" target="_blank">Más Información</a></li>
<li><a href="https://www.juntadeandalucia.es/educacion/secretariavirtual/consultas/acceso.html?idConsulta=45" target="_blank">Consulta Personalizada de las Adjudicaciones</a></li>
</ul></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                            <div class="item column-2">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Listas de admitidos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Miércoles, 11 Junio 2014 10:16</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=207:listas-de-admitidos&amp;catid=242&amp;Itemid=614&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=3299b291f4c8a93cb20f96e2454c0a7d1534a922" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 4442
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div class="cajacomentario" style="margin: 2px 5px 20px; padding: 5px; border: 2px solid #cccccc; outline: 0px none; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 68px; text-align: center;">
<h2 style="text-align: center;"><span style="text-decoration: underline;"><strong><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal; text-decoration: underline;">NOVEDAD:</span></strong></span></h2>
<h3 style="text-align: center;"><span style="text-decoration: underline;"><a href="archivos_ies/13_14/Listados_de_admitidos.pdf" target="_blank" title="Lista de admitidos para el curso 2014/15."><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal; text-decoration: underline;">SE DA A CONOCER LA LISTA DE ADMITIDOS PARA EL PRÓXIMO CURSO 2014/15 EN EL IES MIGUEL DE CERVANTES</span></a></span></h3>
<center>
<h3 style="padding-left: 60px;"><span style="text-decoration: underline;"><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal; text-decoration: underline;"><a href="archivos_ies/13_14/Listado_de_alumnado_admitido_fase2.pdf" target="_blank"><strong>LISTADO DEL ALUMNADO ADMITIDO: 2ª FASE</strong></a></span></span><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal;"> (27/05/2014)</span><span style="text-decoration: underline;"><span style="font-family: Tahoma,sans-serif; color: green; font-style: normal; text-decoration: underline;"><br /></span></span></h3>
</center>
<p> </p>
<span style="color: #414141; font-family: 'Century Gothic',Arial,Helvetica,sans-serif; font-size: 13px; line-height: normal;">(Actualizado el 27/05/14)</span></div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Secretaría</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postcontent">
<div class="items-more">

<div class="items-more">

<h3>Más Artículos...</h3>
<ol>
	<li>
		<a href="/index.php?option=com_content&amp;view=article&amp;id=206:matricula-y-anulacion&amp;catid=242&amp;Itemid=613">
			Matrícula y anulación</a>
	</li>
	<li>
		<a href="/index.php?option=com_content&amp;view=article&amp;id=205:reserva-de-plaza&amp;catid=242&amp;Itemid=612">
			Reserva de plaza</a>
	</li>
	<li>
		<a href="/index.php?option=com_content&amp;view=article&amp;id=204:horario&amp;catid=242&amp;Itemid=610">
			Horario de Secretaría</a>
	</li>
	<li>
		<a href="/index.php?option=com_content&amp;view=article&amp;id=199:fpe&amp;catid=242&amp;Itemid=603">
			FP</a>
	</li>
</ol>
</div>
</div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:84:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Dpto. de Animación Sociocultural";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:102:"/index.php?option=com_content&amp;view=category&amp;id=242&amp;Itemid=101&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:103:"/index.php?option=com_content&amp;view=category&amp;id=242&amp;Itemid=101&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:28:"Área Formación Profesional";s:4:"link";s:59:"index.php?option=com_content&view=article&id=187&Itemid=573";}i:2;O:8:"stdClass":2:{s:4:"name";s:33:"Dpto. de Animación Sociocultural";s:4:"link";s:59:"index.php?option=com_content&view=article&id=156&Itemid=101";}i:3;O:8:"stdClass":2:{s:4:"name";s:11:"Secretaría";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
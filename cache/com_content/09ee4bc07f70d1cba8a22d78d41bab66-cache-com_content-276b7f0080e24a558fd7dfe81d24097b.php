<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:30331:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">4º ESO DIBUJO TÉCNICO</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 19 Mayo 2015 10:47</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=75:4o-eso-dibujo-tecnico&amp;catid=97&amp;Itemid=562&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=4452ae84bcf09dfec1c46f6960e1cb7c46ed8322" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 9759
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div style="text-align: center;">
<h3 style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt; text-decoration: underline;">LAMINAS DEL CURSO 2014 - 2015</span></h3>
<h3 style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt; text-decoration: underline;">DIBUJO 4º DE ESO.</span></h3>
</div>
<div style="text-align: center;"> </div>
<ol>
<li><a href="archivos_ies/14_15/dibujo/Unidad_1_Lenguaje_imagen.pdf" target="_blank" title="Tema 1.">TEMA 1.</a></li>
<ul>
<li><a href="archivos_ies/14_15/dibujo/T1_lam1%20.pdf" target="_blank">Lámina 1.</a></li>
<li><a href="archivos_ies/14_15/dibujo/T1_lam2%20.pdf" target="_blank">Lámina 2.</a></li>
<li><a href="archivos_ies/14_15/dibujo/T1_lam3%20.pdf" target="_blank">Lámina 3.</a></li>
<li><a href="archivos_ies/14_15/dibujo/T1_lam4.pdf" target="_blank">Lámina 4.</a></li>
<li><a href="archivos_ies/14_15/dibujo/T1_lam5%20.pdf" target="_blank">Lámina 5.</a></li>
</ul>
<li><a href="archivos_ies/14_15/dibujo/Unidad_2_4_eso.pdf" target="_blank" title="Tema 2.">TEMA 2.</a></li>
<ul>
<li><a href="archivos_ies/14_15/dibujo/lamina1_tema2_4eso.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a></li>
<li style="padding-left: 11px; overflow-y: hidden; background-image: url('templates/verde10/images/postbullets.png'); background-position: 0% 0%; background-repeat: no-repeat;"><a href="archivos_ies/14_15/dibujo/lamina2_tema2_4eso.jpg" target="_blank">Lámina 2.</a></li>
<li style="padding-left: 11px; overflow-y: hidden; background-image: url('templates/verde10/images/postbullets.png'); background-position: 0% 0%; background-repeat: no-repeat;"><a href="archivos_ies/14_15/dibujo/lamina3_tema2_4eso.jpg" target="_blank">Lámina 3.</a></li>
<li style="padding-left: 11px; overflow-y: hidden; background-image: url('templates/verde10/images/postbullets.png'); background-position: 0% 0%; background-repeat: no-repeat;"><a href="archivos_ies/14_15/dibujo/lamina4_tema2_4eso.jpg" target="_blank">Lámina 4.</a></li>
<li style="padding-left: 11px; overflow-y: hidden; background-image: url('templates/verde10/images/postbullets.png'); background-position: 0% 0%; background-repeat: no-repeat;"><a href="archivos_ies/14_15/dibujo/lamina5_tema2_4eso.jpg" target="_blank">Lámina 5.</a></li>
</ul>
<li><a href="archivos_ies/14_15/dibujo/Unidad_3_4eso.pdf" target="_blank">TEMA 3.</a><span style="color: #2a2a2a;"> </span></li>
</ol>
<ul>
<ul>
<li><a href="archivos_ies/14_15/dibujo/lamina1_tema3_4eso.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina2_tema3_4eso.jpg" target="_blank">Lámina 2.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina3_tema3_4eso.jpg" target="_blank">Lámina 3.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina4_tema3_4eso.jpg" target="_blank">Lámina 4.</a></li>
<li style="padding-left: 0px; overflow-y: visible; background: none;"></li>
<li style="padding-left: 0px; overflow-y: visible; background: none;"></li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="color: #414141; background-color: initial;">4. </span><a href="archivos_ies/14_15/dibujo/Tema_4_4eso.pdf" target="_blank" style="background-color: initial;">TEMA 4.</a><span style="color: #2a2a2a;"> </span></p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/dibujo/lamina1_tema4.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina2_tema4.jpg" target="_blank">Lámina 2.</a></li>
<li><a href="archivos_ies/14_15/dibujo/lamina3_tema4.jpg" target="_blank">Lámina 3.</a></li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="color: #414141; background-color: initial;">5. <a href="archivos_ies/14_15/dibujo/Tema5_4_eso.pdf" target="_blank">TEMA 5.</a> (07/01/15)</span> </p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/dibujo/lam1_Tema5.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a> (27/01/15)</li>
<li><a href="archivos_ies/14_15/dibujo/lam2_Tema5.jpg" target="_blank">Lámina 2.</a> (27/01/15)</li>
<li><a href="archivos_ies/14_15/dibujo/lam3_Tema5.jpg" target="_blank">Lámina 3.</a> (27/01/15)</li>
<li><a href="archivos_ies/14_15/dibujo/lam4_Tema5.jpg" target="_blank" style="text-decoration: none; color: #2f310d;">Lámina 4.</a> (27/01/15)</li>
<li><a href="archivos_ies/14_15/dibujo/lam5_Tema5.jpg" target="_blank">Lámina 5.</a> (27/01/15)</li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="color: #414141; background-color: initial;"> 6. <a href="archivos_ies/14_15/dibujo/Tema_6_EP.pdf" target="_blank">TEMA 6.</a> (03/03/15)</span> </p>
<ul>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/dibujo/T6-lam-einstein.jpg" target="_blank">Lámina 1.</a> (06/04/15)</span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/dibujo/T6-lam2-meninas.JPG" target="_blank">Lámina 2.</a> (06/04/15)</span></li>
</ul>
</ul>
<p style="padding-left: 30px;"> 7. <a href="archivos_ies/14_15/dibujo/Tema7.pdf" target="_blank">TEMA 7. </a>(06(04/15)</p>
<ul>
<ul>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/dibujo/T7lam1.jpg" target="_blank">Lámina 1.</a> (15/05/15)</span></li>
<li><span style="color: #2a2a2a;"><a href="archivos_ies/14_15/dibujo/T7lam2.jpg" target="_blank">Lámina 2.</a> (15/05/15)</span></li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="background-color: initial;"> 8. </span><a href="archivos_ies/14_15/dibujo/Tema8_FH11.pdf" target="_blank" style="background-color: initial;">TEMA 8.</a> <span style="background-color: initial;">(15(05/15)</span> </p>
<ul>
<ul>
<li><a href="archivos_ies/14_15/dibujo/T8_lam1-4eso.jpg" target="_blank" style="background-color: initial;">Lámina 1.</a> <span style="color: #2a2a2a;">(19/05/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T8_lam2-4eso.jpg" target="_blank">Lámina 2.</a> <span style="color: #2a2a2a;">(19/05/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T8_lam3-4eso.jpg" target="_blank">Lámina 3.</a> <span style="color: #2a2a2a;">(19/05/15)</span></li>
<li><a href="archivos_ies/14_15/dibujo/T8_lam4-4eso.jpg" target="_blank">Lámina 4.</a> <span style="color: #2a2a2a;">(19/05/15)</span></li>
</ul>
</ul>
<p style="padding-left: 30px;"><span style="background-color: initial;"> 9. <a href="archivos_ies/14_15/dibujo/Tema_9.pdf" target="_blank">TEMA 9.</a> (19/05/15)</span></p>
<div style="text-align: center;"><hr /></div>
<div style="text-align: center;"> </div>
<div style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt; text-decoration: underline;">LAMINAS DEL CURSO 2012 - 2013</span></div>
<div style="text-align: center;"><span style="font-family: 'arial black', 'avant garde'; font-size: 14pt; text-decoration: underline;">DIBUJO 4º DE ESO.</span></div>
<div style="text-align: center;"> </div>
<div style="border: 0pt; text-align: center;">
<table align="center">
<tbody>
<tr>
<td style="text-align: center;" align="CENTER" valign="MIDDLE">
<ul>
<li style="text-align: justify;"><span style="font-family: arial, helvetica, sans-serif; font-size: small;"><a href="archivos_ies/dep_dibujo/Lamina_1_4_ESO.pdf" target="_blank">Lámina 1.</a>   Construcciones fundamentales. Operaciones con segmentos.</span></li>
<li style="text-align: justify;"><span style="font-family: arial, helvetica, sans-serif; font-size: small;"><a href="archivos_ies/dep_dibujo/Lamina_2_4_ESO.pdf" target="_blank">Lámina 2.</a>   Construcciones fundamentales. Paralelismo.</span></li>
<li style="text-align: justify;"><span style="font-family: arial, helvetica, sans-serif; font-size: small;"><a href="archivos_ies/dep_dibujo/Lamina_3_4_ESO.pdf" target="_blank">Lámina 3.</a>   Construcciones fundamentales. Perpendicularidad.</span></li>
<li style="text-align: justify;"><span style="font-family: arial, helvetica, sans-serif; font-size: small;"><a href="archivos_ies/dep_dibujo/Lamina_4_4_ESO.pdf" target="_blank">Lámina 4.</a>   Construcciones fundamentales. Estudio de la circunferencia.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_5_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 5.</a><span style="font-size: small;">   Construcciones fundamentales. Ángulos.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_6_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 6.</a><span style="font-size: small;">   Construcciones fundamentales. Construcción de ángulos.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_7_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 7.</a><span style="font-size: small;">   </span><span style="font-size: small;">Construcciones fundamentales. Aplicación. Segmentos y ángulos 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_8_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 8.</a><span style="font-size: small;">   </span><span style="font-size: small;">Construcciones fundamentales. Aplicación. Segmentos y ángulos 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_9_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 9.</a><span style="font-size: small;">   Triángulos 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_10_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 10.</a><span style="font-size: small;"> Triángulos 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_11_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 11.</a><span style="font-size: small;"> Triángulos 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_12_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 12.</a><span style="font-size: small;"> Triángulos 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_13_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 13.</a><span style="font-size: small;"> Triángulos 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_14_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 14.</a><span style="font-size: small;"> Triángulos 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_15_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 15.</a><span style="font-size: small;"> Aplicación de triángulos 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_16_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 16.</a><span style="font-size: small;"> Aplicación de triángulos 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_17_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 17.</a><span style="font-size: small;"> Aplicación de triángulos 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_18_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 18.</a><span style="font-size: small;"> Aplicación de triángulos 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_19_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 19.</a><span style="font-size: small;"> Cuadriláteros 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_20_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 20.</a><span style="font-size: small;"> Cuadriláteros 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_21_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 21.</a><span style="font-size: small;"> Aplicación de cuadriláteros 1</span><a href="archivos_ies/dep_dibujo/Lamina_21_4_ESO.pdf" target="_blank" style="font-size: small;">.</a></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_22_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 22.</a><span style="font-size: small;"> Aplicación de cuadriláteros 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_23_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 23.</a><span style="font-size: small;"> Polígonos Regulares.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_24_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 24.</a><span style="font-size: small;"> Polígonos Regulares 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_25_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 25.</a><span style="font-size: small;"> Polígonos Regulares 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_26_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 26.</a><span style="font-size: small;"> Polígonos Regulares 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_27_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 27.</a><span style="font-size: small;"> Aplicación de polígonos regulares 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_28_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 28.</a><span style="font-size: small;"> Aplicación de polígonos regulares 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_29_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 29.</a><span style="font-size: small;"> Tangencias 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_30_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 30.</a><span style="font-size: small;"> Tangencias 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_31_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 31.</a><span style="font-size: small;"> Tangencias 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_32_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 32.</a><span style="font-size: small;"> Tangencias 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_33_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 33.</a><span style="font-size: small;"> Tangencias 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_34_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 34.</a><span style="font-size: small;"> Tangencias 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_35_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 35.</a><span style="font-size: small;"> Tangencias 7.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_36_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 36.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_37_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 37.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_38_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 38.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_39_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 39.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_40_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 40.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_41_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 41.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_42_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 42.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 7.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_43_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 43.</a><span style="color: #655c4e; font-size: small;"> Movimientos en el plano 8.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_44_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 44.</a><span style="color: #655c4e; font-size: small;"> Curvas cónicas. Elipse.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_45_4_ESO.pdf" target="_blank" style="color: #655c4e; font-size: small;">Lámina 45.</a><span style="color: #655c4e; font-size: small;"> Curvas cónicas. Parábola.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_46_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 46.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Curvas cónicas. Hipérbola.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_47_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 47.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">  Colores fríos. Aplicación 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_48_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 48.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_49_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 49.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_50_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 50.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_51_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 51.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 4</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_52_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 52.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_53b_4_ESO.pdf" style="font-family: inherit; font-size: inherit;">Lámina 53.</a><span style="font-family: inherit; font-size: inherit;">   Vistas normalizadas 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_54_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 54.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 7.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_55_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 55.</a><span style="font-family: inherit; font-size: small; color: #655c4e;">   Vistas normalizadas 8.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_56_4_ESO.pdf" style="font-family: inherit; font-size: small;">Lámina 56</a><span style="font-family: inherit; font-size: small; color: #655c4e;">.   Vistas normalizadas 9.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_57_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 57.</a><span style="font-size: small;">   </span><span style="font-size: small;">Vistas normalizadas 10.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_58_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 58.</a><span style="font-size: small;">   </span><span style="font-size: small;">Vistas normalizadas 11.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_59_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 59.</a><span style="font-size: small;">   Isométrica 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_60_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 60.</a>  <span style="font-size: small;"> Isométrica 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_61_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 61.</a>  <span style="font-size: small;"> Isométrica 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_62_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 62.</a>  <span style="font-size: small;"> Isométrica 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_63_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 63.</a>  <span style="font-size: small;"> Isométrica 5.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_64_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 64.</a>  <span style="font-size: small;"> Isométrica 5b.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_65_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 65.</a>  <span style="font-size: small;"> Isométrica 6.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_66_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 66.</a>  <span style="font-size: small;"> Isométrica 7.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_67_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 67.</a><span style="font-size: small;">   Normalización I.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_68_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 68.</a>  <span style="font-size: small;"> Normalización II.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_69_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 69.</a>  <span style="font-size: small;"> Normalización III.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_70_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 70.</a>  <span style="font-size: small;"> Normalización IV.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_71_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 71.</a><span style="font-size: small;">   Normalización V</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_72_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 72.</a><span style="font-size: small;">   Normalización VI.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_73_4_ESOerrores.pdf" target="_blank" style="font-size: small;">Lámina 73.</a><span style="font-size: small;">   Normalización VII.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_74_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 74.</a><span style="font-size: small;">   Normalización VIII.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_75_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 75.</a><span style="font-size: small;">   Normalización IX.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_76_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 76.</a><span style="font-size: small;">   Normalización X.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_77_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 77.</a><span style="font-size: small;">   Croquización I.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_78_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 78.</a><span style="font-size: small;">   Croquización II.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_79_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 79.</a><span style="font-size: small;">   Gradaciones I.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_80_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 80.</a><span style="font-size: small;">   Gradaciones II.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_81_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 81.</a><span style="font-size: small;">   Gradaciones III</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_82_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 82.</a><span style="font-size: small;">   Gradaciones IV</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_83_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 83.</a><span style="font-size: small;">   Dibujo científico I.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_84_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 84.</a><span style="font-size: small;">   Dibujo científico II.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_85_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 85.</a><span style="font-size: small;">   Dibujo científico III.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_86_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 86.</a><span style="font-size: small;">   Dibujo científico IV.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_87_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 87.</a><span style="font-size: small;">   Dibujo científico V.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_88_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 88.</a><span style="font-size: small;">   Dibujo científico VI.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_89_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 89.</a><span style="font-size: small;">   Perspectiva cónica 1.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_90_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 90.</a><span style="font-size: small;">   Perspectiva cónica 2.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_91_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 91.</a><span style="font-size: small;">   Perspectiva cónica 3.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_92_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 92.</a><span style="font-size: small;">   Perspectiva cónica 4.</span></li>
<li style="text-align: justify;"><a href="archivos_ies/dep_dibujo/Lamina_93_4_ESO.pdf" target="_blank" style="font-size: small;">Lámina 93.</a><span style="font-size: small;">   Perspectiva cónica 5.</span></li>
</ul>
</td>
</tr>
</tbody>
</table>
<p style="text-align: left;"> </p>
<div><span style="font-size: x-small;"> </span></div>
</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Departamentos</span> / <span class="art-post-metadata-category-name">Educación Plástica</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:65:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - DIBUJO 4º ESO";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:16:"Área Artística";s:4:"link";s:59:"index.php?option=com_content&view=article&id=184&Itemid=570";}i:2;O:8:"stdClass":2:{s:4:"name";s:29:"Dpto. de Educación Plástica";s:4:"link";s:57:"index.php?option=com_content&view=article&id=76&Itemid=98";}i:3;O:8:"stdClass":2:{s:4:"name";s:14:"DIBUJO 4º ESO";s:4:"link";s:58:"index.php?option=com_content&view=article&id=75&Itemid=562";}}s:6:"module";a:0:{}}
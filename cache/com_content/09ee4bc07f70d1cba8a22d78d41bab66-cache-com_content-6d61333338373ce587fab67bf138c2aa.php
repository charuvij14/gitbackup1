<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:176694:"<div class="blog"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Dpto. de Lengua Castellana</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="items-leading">
            <div class="leading-0">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">3º ESO</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 23 Junio 2015 08:02</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=289:3-eso&amp;catid=99&amp;Itemid=726&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=248188c1ceb28ca8a70b09427ede2922f3749637" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1534
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 5px; padding: 5px; font-size: 13.3333px; text-align: center; text-transform: none; color: #2a2a2a; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 25px; background-color: #ffffff;"><a href="archivos_ies/14_15/programaciones/Cuadernillo_padres_3ESO_14_15e.pdf" target="_blank">PROGRAMACIÓN DE 3º ESO DE MATEMÁTICAS<br />CURSO 2014/15</a></h3>
<p style="font-size: 11.8181819915771px; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;"><strong style="font-size: 14pt;"><span style="color: #000000;">MATEMÁTICAS DE 3º ESO. CURSO 2014/15</span></strong></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR PARA SEPTIEMBRE 2015</strong></span></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDADES 1 Y 2</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<hr style="background-color: #e7eaad;" />
<p style="text-align: center; background-color: #e7eaad;"><span style="text-decoration: underline;"><a href="archivos_ies/13_14/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span> </a></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank">UNIDAD: 7</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank">UNIDAD: 8</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank">UNIDAD 9</a></span></p>
<table class="borde_outset" style="text-align: center; color: #2a2a2a;" border="1">
<tbody>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 3º ESO</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></span></span><span style="color: #2f310d;"> extraídos del siguiente DOCUMENTO COMPLETO.</span><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_1_a_5.pdf" target="_blank"><strong><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></strong></a></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 21/01/2015</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números racionales</p>
<p><span style="font-size: inherit;"><strong>UNIDAD 2.</strong> Números reales</span></p>
<p><strong>UNIDAD 3.</strong> Polinomios</p>
<p><strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado </p>
<p><strong>UNIDAD 5.</strong> Sistemas de ecuaciones</p>
<p><strong> </strong></p>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> </span></span></strong><strong><span style="color: #2f310d;">extraídos del siguiente DOCUMENTO COMPLETO.</span></strong><br /><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_6_a_10.pdf" target="_blank"><strong><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></strong></a></li>
<li></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 15/04/2015</strong></span></p>
<p><strong>UNIDAD 6.</strong> Proporcionalidad numérica</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 7.</strong> Progresiones</span></p>
<p><strong>UNIDAD 8.</strong> Lugares geométricos. Figuras planas</p>
<p><strong>UNIDAD 9.</strong> Cuerpos geométricos .</p>
<p><strong>UNIDAD 10.</strong></p>
</td>
</tr>
</tbody>
</table>
<p style="font-size: 11.8181819915771px; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;"><strong style="font-size: 14pt;"><span style="color: #000000;"> </span></strong></span></p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">MATEMÁTICAS</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-1">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">1º ESO</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 23 Junio 2015 07:57</span> | <span class="art-postauthoricon">Escrito por Miguel Anguita</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=288:1-eso&amp;catid=99&amp;Itemid=725&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=c98356729dc27dce57819b560df1f1e83ae2f02b" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 1904
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 5px; padding: 5px; font-size: 13.3333px; text-align: center; text-transform: none; color: #2a2a2a; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 25px; background-color: #ffffff;"><a href="archivos_ies/14_15/programaciones/Cuadernillo_padres_1ESO_14_15e.pdf" target="_blank">PROGRAMACIÓN DE 1º ESO DE MATEMÁTICAS<br />CURSO 2014/15</a></h3>
<p style="font-size: 11.8181819915771px; text-align: center; background-color: #ffffff;"><span style="text-decoration: underline;"><strong style="font-size: 14pt;"><span style="color: #000000;">MATEMÁTICAS DE 1º ESO. CURSO 2014/15</span></strong></span></p>
<p style="text-align: center; background-color: #e7eaad;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR</strong></span><strong style="text-align: left; color: #414141;"> PARA SEPTIEMBRE 2015</strong><strong>:</strong></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><strong style="font-size: 14pt;"><span style="color: #000000;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank" style="font-size: 13px; font-weight: normal; background-color: #e7eaad;"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a></span></strong></p>
<p style="font-size: 11.8181819915771px; text-align: center; background-color: #ffffff;"> </p>
<hr style="text-align: center; background-color: #e7eaad;" />
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><strong style="font-size: 14pt;"><span style="color: #000000;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank" style="font-size: 13px; font-weight: normal; background-color: #e7eaad;">UNIDAD 11</a> </span></span></strong><span style="text-align: left;">(No estudiado en el curso 14/15)</span></p>
<ul style="background-color: #e7eaad;">
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" title="Pulsar para descargar." style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> EN SEPTIEMBRE 2015,</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,11,12,13,19,21,28.</li>
<li><strong>Tema 2:</strong>1,6,8,14,16,17,18,19,20,23.</li>
<li><strong>Tema 3:</strong> 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43.</li>
<li><strong>Tema 4:</strong> Todos.</li>
<li><strong>Tema 5:</strong> 1,2,5,8,9,10,11,12,13.</li>
<li><strong>Tema 6:</strong> 1,2,8,11,29,31,32.</li>
</ul>
</ul>
<p style="background-color: #e7eaad;"><strong>TEMA 1</strong>. Números naturales</p>
<p style="background-color: #e7eaad;"><strong>TEMA 2.</strong> Divisibilidad</p>
<p style="background-color: #e7eaad;"><strong>TEMA 3.</strong> Fracciones</p>
<p style="background-color: #e7eaad;"><strong>TEMA 4.</strong>  Números decimales</p>
<p style="background-color: #e7eaad;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p style="background-color: #e7eaad;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p style="text-align: center; background-color: #ffffff;"><span style="text-align: left;"><span style="font-family: Arial, sans-serif; background-color: #e7eaad;"> </span> </span></p>
<ul style="background-color: #e7eaad;">
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR<strong style="color: #414141; letter-spacing: normal; text-align: left;"> EN SEPTIEMBRE 2015</strong>.  </a></strong></span><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">Solamente los siguientes números por tema:</a>
<ul style="margin-top: 0.5em; margin-bottom: 0.5em;">
<li><strong>Tema 7:</strong>1,4,6,7,8,9,10,11,12,13.</li>
<li><strong>Tema 8:</strong>2,3,6,7,9,11,12,13,14,15.</li>
<li><strong>Tema 9: TODOS.</strong></li>
<li><strong>Tema 10:</strong>1,3,4,5,6,7,8,11,13,14.</li>
<li><strong>Tema 11:</strong> <strong>TODOS.</strong></li>
</ul>
</li>
</ul>
<p style="text-align: left; background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">TEMA 7.</strong><span style="color: inherit; font-family: inherit; font-size: inherit;"> Sistema Métrico Decimal</span></p>
<p style="background-color: #e7eaad;"><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p style="background-color: #e7eaad;"><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p style="background-color: #e7eaad;"><strong>TEMA 10.</strong> Polígonos</p>
<p style="background-color: #e7eaad;"><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p style="text-align: center; background-color: #ffffff;"><span style="text-align: left;"><span style="background-color: #e7eaad;"> </span> </span></p>
<p style="text-align: center; background-color: #ffffff;"><span style="text-align: left;"> </span></p>
<table class="borde_outset" style="text-align: center; color: #2a2a2a;" border="1">
<tbody>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" title="Pulsar para descargar." style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a>,</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,11,12,13,19,21,28.</li>
<li><strong>Tema 2:</strong>1,6,8,14,16,17,18,19,20,23.</li>
<li><strong>Tema 3:</strong> 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43.</li>
<li><strong>Tema 4:</strong> Todos.</li>
<li><strong>Tema 5:</strong> 1,2,5,8,9,10,11,12,13.</li>
<li><strong>Tema 6:</strong> 1,2,8,11,29,31,32.</li>
</ul>
</ul>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 22/01/2015</strong></span></p>
<p><strong>TEMA 1</strong>. Números naturales</p>
<p><strong>TEMA 2.</strong> Divisibilidad</p>
<p><strong>TEMA 3.</strong> Fracciones</p>
<p><strong>TEMA 4.</strong>  Números decimales</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p><span style="font-family: Arial, sans-serif;"> </span></p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR.  </a></strong></span><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">Solamente los siguientes números por tema:</a>
<ul style="margin-top: 0.5em; margin-bottom: 0.5em;">
<li><strong>Tema 7:</strong>1,4,6,7,8,9,10,11,12,13.</li>
<li><strong>Tema 8:</strong>2,3,6,7,9,11,12,13,14,15.</li>
<li><strong>Tema 9: TODOS.</strong></li>
<li><strong>Tema 10:</strong>1,3,4,5,6,7,8,11,13,14.</li>
<li><strong>Tema 11:</strong> <strong>TODOS.</strong></li>
</ul>
</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 16/04/2015</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p><strong>TEMA 10.</strong> Polígonos</p>
<p><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p> </p>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">MATEMÁTICAS</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
                    <div class="leading-2">
            <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon"><a href="/index.php?option=com_content&amp;view=article&amp;id=154:2o-bachillerato-b&amp;catid=99&amp;Itemid=670" class="PostHeader">2º BACHILLERATO A - B</a></span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Sábado, 15 Noviembre 2014 17:37</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=154:2o-bachillerato-b&amp;catid=99&amp;Itemid=670&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | Visitas: 70563
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><center style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif;">
<ul>
<li>
<h2 style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: -webkit-center; text-transform: none;"><span style="font-size: x-large;"><a href="http://emestrada.wordpress.com/category/matematicas-ii/" target="_blank">Resolución de todos los exámenes de selectividad desde 2000.</a><br /></span></h2>
</li>
</ul>
<h2 style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: -webkit-center; text-transform: none;"> </h2>
<hr />
<h2 style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif; text-align: -webkit-center; text-transform: none;"><span style="font-size: x-large;">Exámenes resueltos de Selectividad de Matemáticas II de Andalucía</span></h2>
<p style="padding-left: 30px;"><span style="font-size: x-large;"> </span></p>
<p><span style="font-size: medium; text-align: start; font-family: Verdana;">Realizados por:</span></p>
<p style="font-size: medium;" align="center"><span style="font-family: Arial;"><strong><span style="font-size: large;">D. Germán Jesús Rubio Luna, Catedrático de Matemáticas </span></strong><span style="font-size: large;"> <br /></span><strong><span style="font-size: large;">del IES Francisco Ayala de Granada</span>.</strong></span></p>
<table id="table20" width="58%" border="1">
<tbody>
<tr>
<td align="center"><span style="font-family: 'Times New Roman'; font-size: small;"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2013" style="text-decoration: none; color: blue;">Sobrantes del año 2013</a></strong></span></td>
<td align="center"><strong><span style="font-family: 'Times New Roman'; font-size: small;"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2012" style="text-decoration: none; color: blue;">Sobrantes del año 2012</a></span></strong></td>
</tr>
<tr>
<td align="center"><span style="font-family: 'Times New Roman'; font-size: small;"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2011" style="text-decoration: none; color: blue;">Sobrantes del año 2011</a></strong></span></td>
<td align="center"><strong><span style="font-family: 'Times New Roman'; font-size: small;"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2010" style="text-decoration: none; color: blue;">Sobrantes del año 2010</a></span></strong></td>
</tr>
<tr>
<td align="center"><span style="font-family: 'Times New Roman'; font-size: small;"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2009" style="text-decoration: none; color: blue;"><strong>Sobrantes del año 2009</strong></a></span></td>
<td align="center"><strong><span style="font-family: 'Times New Roman'; font-size: small;"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2008" style="text-decoration: none; color: blue;">Sobrantes del año 2008</a></span></strong></td>
</tr>
<tr>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2007" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 2007</span></a></strong></td>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2006" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 2006</span></a></strong></td>
</tr>
<tr>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2005" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 2005</span></a></strong></td>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2004" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 2004</span></a></strong></td>
</tr>
<tr>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2003" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 2003</span></a></strong></td>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2002" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 2002</span></a></strong></td>
</tr>
<tr>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2001" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 2001</span></a></strong></td>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#2000" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 2000</span></a></strong></td>
</tr>
<tr>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#1999" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 1999</span></a></strong></td>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#1998" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 1998</span></a></strong></td>
</tr>
<tr>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#1997" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 1997</span></a></strong></td>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#1996" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Sobrantes del año 1996</span></a></strong></td>
</tr>
<tr>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#libro_00" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Libro del año 1999-2000</span></a></strong></td>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#libro_99" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Libro del año 1998-1999</span></a></strong></td>
</tr>
<tr>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#libro_98" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Libro del año 1997-1998</span></a></strong></td>
<td align="center"><strong><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia.html#libro_97" style="text-decoration: none; color: blue;"><span style="font-family: 'Times New Roman'; font-size: small;">Libro del año 1996-1997</span></a></strong></td>
</tr>
</tbody>
</table>
</center><center style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif;"></center><center style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif;">
<p><strong><span style="font-family: Arial; font-size: large;">Sobrantes del Año 2014</span><span style="font-size: medium;"> <img src="http://www.iesayala.com/selectividadmatematicas/imagen/nuevo1.gif" border="0" width="38" height="13" /></span></strong><span style="font-size: medium;"> </span></p>
<p> </p>
<center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod1_exjun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio (modelo 1) del 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod1_soljun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod2_exjun_espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio Colisiones (modelo 2) del 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod2_soljun_espe.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod3_exjun_inci.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio Incidencias (modelo 3) del 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod3_soljun_inci.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod4_exsep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre (modelo 4) del 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod4_solsep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod5_exsep_R1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre Reserva 1(modelo 5) del 2014</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod5_solsep_R1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2014/14_mod6_exsep_R2.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre Reserva 2(modelo 6) del 2014</span></td>
<td align="center"><span style="font-size: xx-small;">Solución<br /><br /></span></td>
</tr>
</tbody>
</table>
</center></center><center style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif;"></center><center style="color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif;"><a name="2013" style="color: red;"></a><br />
<p style="display: inline !important;"><strong><span style="font-family: Arial; font-size: large;"> </span></strong></p>
<p style="display: inline !important;"><strong><span style="font-family: Arial; font-size: large;"> </span></strong></p>
<p style="display: inline !important;"><strong><span style="font-family: Arial; font-size: large;"> </span></strong></p>
<p style="display: inline !important;"><strong><span style="font-family: Arial; font-size: large;"> </span></strong></p>
<p style="display: inline !important;"><strong><span style="font-family: Arial; font-size: large;"> </span></strong></p>
<p style="display: inline !important;"><strong><span style="font-family: Arial; font-size: large;">Sobrantes del Año 2013</span><span style="font-size: medium;"> </span></strong></p>
<a name="2013" style="color: red;"></a><br />
<p> </p>
<center>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_mod1.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio, reserva 2 (modelo 1) del 2013</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_mod1_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_exsep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre (modelo 2) del 2013</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_solsep.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_mod3.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre, reserva 1 (modelo 3) del 2013</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_mod3_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_mod4.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Septiembre, reserva 2 (modelo 4) del 2013</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_mod4_sol.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_exjun_esp.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio, colisiones (Modelo 5) del 2013</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_soljun_esp.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_exjun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen de Junio (modelo 6) del 2013</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2013/13_soljun.pdf" style="text-decoration: none; color: blue;"><span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
</center></center>
<p><strong><span style="font-family: Arial; font-size: large;"> </span></strong></p>
<p><strong><span style="font-family: Arial; font-size: large;">Sobrantes del Año 2012</span><span style="font-size: medium;"> </span></strong></p>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/jun12espe.pdf"> <span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 (Junio Específico) de 2012</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/soljun12espe.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/mod2_12.pdf"> <span style="font-size: xx-small;"><span style="font-size: xx-small;">Enunciado</span></span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 del 2012</span></td>
<td align="center"><span style="font-size: xx-small;"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/mod2_12_sol.pdf">Solución</a></span></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/sep12gene.pdf"> <span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 (Septiembre común) de 2012</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/solsep12gene.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/jun12gene.pdf"> <span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 (Junio General) de 2012</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/soljun12gene.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/mod5_12.pdf"> <span style="font-size: xx-small;"><span style="font-size: xx-small;">Enunciado</span></span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 del 2012</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/mod5_12_sol.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/mod6_12.pdf"> <span style="font-size: xx-small;"><span style="font-size: xx-small;">Enunciado</span></span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 del 2012</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2012/mod6_12_sol.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
<p> <strong><span style="font-family: Arial; font-size: large;">Sobrantes del Año 2011</span></strong></p>
<table id="table1" width="57%" border="1">
<tbody>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/jun11espe2.pdf"> <span style="font-size: xx-small;"><span style="font-size: xx-small;">Enunciado</span></span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 1 (Junio Específico 2) de 2011</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/jun11espe2_sol.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/mod_sep_11.pdf"> <span style="font-size: xx-small;"><span style="font-size: xx-small;">Enunciado</span></span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 2 (Septiembre) de 2011</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/mod_sep_11_sol.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/mod3_11.pdf"> <span style="font-size: xx-small;"><span style="font-size: xx-small;">Enunciado</span></span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 3 del 2011</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/mod3_11_sol.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/mod4_11.pdf"> <span style="font-size: xx-small;"><span style="font-size: xx-small;">Enunciado</span></span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 4 del 2011</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/mod4_11_sol.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/jun11espe.pdf"> <span style="font-size: xx-small;"><span style="font-size: xx-small;">Enunciado</span></span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 5 (Junio Específico 1) de 2011</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/soljun11espe.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
<tr>
<td align="center" width="67"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/jun11gene.pdf"> <span style="font-size: xx-small;">Enunciado</span></a></td>
<td align="center" width="370"><span style="font-size: small;">Examen Modelo 6 (Junio General) de 2011</span></td>
<td align="center"><a href="http://www.iesayala.com/selectividadmatematicas/ficheros/andalucia/2011/soljun11gene.pdf"> <span style="font-size: xx-small;">Solución</span></a></td>
</tr>
</tbody>
</table>
<p><span style="font-family: Verdana;"> </span></p>
<p> </p>
<div>
<div class="titulo_evento" style="color: #ff9900; font-size: 17px; font-weight: bold; margin-top: 5px; margin-bottom: 10px;" align="center">
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #e7eaad;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;">Selectividad, orientaciones y pruebas de otros años:</span></h2>
<p style="margin-top: 5px; margin-bottom: 10px; font-size: 13px; font-weight: normal; background-color: #e7eaad; color: #000000; font-family: Verdana, Arial, Helvetica, sans-serif;" align="center">Si desea conocer el tamaño del archivo a descargar, deje el ratón sobre el icono del archivo correspondiente. </p>
<p style="color: #2a2a2a; font-size: 13px; font-weight: normal; text-align: justify; background-color: #e7eaad;"> </p>
<table style="font-family: Verdana, Arial, Helvetica, sans-serif; text-align: left; background-color: #e7eaad; color: #000000;" width="90%" border="1" cellspacing="0" align="center">
<tbody>
<tr>
<td><strong>Asignatura</strong></td>
<td align="center"><strong>Orientaciones <br />2013/2014</strong></td>
<td align="center"><strong>2005</strong></td>
<td align="center"><strong>2006</strong></td>
<td align="center"><strong>2007</strong></td>
<td align="center"><strong>2008</strong></td>
<td align="center"><strong>2009</strong></td>
<td align="center"><strong>2010</strong></td>
<td align="center"><strong>2011</strong></td>
<td align="center"><strong>2012</strong></td>
<td align="center"><strong>2013</strong></td>
</tr>
<tr>
<td>Análisis Musical II</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_analisis_musical_ii_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="3.07 Mb" title="3.07 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="5.31 Mb" title="5.31 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="170.95 Mb" title="170.95 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_analisis_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="265.22 Mb" title="265.22 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Biología</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_biologia_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="2.78 Mb" title="2.78 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.09 Mb" title="2.09 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.29 Mb" title="1.29 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.26 Mb" title="1.26 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.07 Mb" title="2.07 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.54 Mb" title="2.54 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_biologia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Ciencias de la Tierra y Medioambientales</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_ciencias_de_la_tierra_y_medioambientales_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.25 Mb" title="0.25 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.64 Mb" title="0.64 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.44 Mb" title="1.44 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="14.79 Mb" title="14.79 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.47 Mb" title="1.47 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.88 Mb" title="1.88 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ciencias_tierra.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.45 Mb" title="1.45 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Comentario de Texto,Lengua Castellana y Literatura</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_comentario_texto_lengua_castellana_y_literatura_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_castellana.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.4 Mb" title="0.4 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Dibujo Artístico II</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_artistico_ii_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.77 Mb" title="0.77 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="13.76 Mb" title="13.76 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.22 Mb" title="2.22 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.34 Mb" title="1.34 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="5.2 Mb" title="5.2 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="27.54 Mb" title="27.54 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="13.52 Mb" title="13.52 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="59.45 Mb" title="59.45 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_artistico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.72 Mb" title="3.72 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Dibujo Técnico II</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_dibujo_tecnico_ii_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.4 Mb" title="0.4 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.56 Mb" title="2.56 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.86 Mb" title="6.86 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="8.05 Mb" title="8.05 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="15.45 Mb" title="15.45 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.99 Mb" title="12.99 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_dibujo_tecnico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.97 Mb" title="6.97 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Diseño</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_diseno_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.44 Mb" title="0.44 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.33 Mb" title="0.33 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fundamentos_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.38 Mb" title="12.38 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.66 Mb" title="0.66 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.12 Mb" title="1.12 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.76 Mb" title="3.76 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fundamento_diseno.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Economía de la Empresa</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_economia_de_la_empresa_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.32 Mb" title="0.32 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.31 Mb" title="0.31 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.82 Mb" title="1.82 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.6 Mb" title="2.6 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.96 Mb" title="3.96 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_economia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.38 Mb" title="1.38 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Electrotecnia</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_electrotecnia_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.46 Mb" title="0.46 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.09 Mb" title="1.09 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.71 Mb" title="0.71 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.52 Mb" title="0.52 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_electrotecnia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Física</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_fisica_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.28 Mb" title="0.28 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.25 Mb" title="0.25 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.39 Mb" title="1.39 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.36 Mb" title="0.36 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_fisica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Geografía</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_geografia_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.28 Mb" title="0.28 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.25 Mb" title="1.25 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.09 Mb" title="3.09 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.29 Mb" title="3.29 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.32 Mb" title="1.32 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.41 Mb" title="12.41 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.91 Mb" title="0.91 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.11 Mb" title="2.11 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.86 Mb" title="3.86 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_geografia.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.98 Mb" title="1.98 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Griego II</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_griego_ii_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.54 Mb" title="0.54 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.6 Mb" title="0.6 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.35 Mb" title="2.35 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="4.69 Mb" title="4.69 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.57 Mb" title="0.57 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_griego.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.51 Mb" title="0.51 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Historia de España</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_espanna_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.17 Mb" title="0.17 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.74 Mb" title="0.74 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.73 Mb" title="1.73 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.77 Mb" title="1.77 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_historico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.87 Mb" title="1.87 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia de la Filosofía</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_filosofia_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.68 Mb" title="0.68 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_texto_filosofico.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Historia de la Música y de la Danza</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_de_la_musica_y_de_la_danza_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="27.28 Mb" title="27.28 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="22.08 Mb" title="22.08 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.51 Mb" title="1.51 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="13.4 Mb" title="13.4 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.38 Mb" title="2.38 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="318.43 Mb" title="318.43 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="10.61 Mb" title="10.61 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="93.24 Mb" title="93.24 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_musica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="101.86 Mb" title="101.86 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Historia del Arte</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_historia_del_arte_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.31 Mb" title="0.31 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.67 Mb" title="0.67 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.16 Mb" title="3.16 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="8.71 Mb" title="8.71 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.91 Mb" title="6.91 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="3.5 Mb" title="3.5 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="18.28 Mb" title="18.28 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_historia_arte.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.06 Mb" title="1.06 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Latín II</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_latin_ii_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_latin.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.94 Mb" title="0.94 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.94 Mb" title="1.94 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1 Mb" title="1 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_latinII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.55 Mb" title="0.55 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Alemán)</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(aleman)_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.15 Mb" title="0.15 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.08 Mb" title="0.08 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.26 Mb" title="0.26 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.3 Mb" title="0.3 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.35 Mb" title="1.35 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.39 Mb" title="0.39 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.3 Mb" title="1.3 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_aleman.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Francés)</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(frances)_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.1 Mb" title="0.1 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.84 Mb" title="0.84 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_frances.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Inglés)</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(ingles)_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.99 Mb" title="0.99 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.68 Mb" title="1.68 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.06 Mb" title="2.06 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.18 Mb" title="1.18 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="12.19 Mb" title="12.19 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_ingles.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.74 Mb" title="1.74 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Lengua Extranjera II (Italiano)</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(italiano)_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.06 Mb" title="0.06 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.07 Mb" title="0.07 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.16 Mb" title="0.16 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.79 Mb" title="0.79 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.29 Mb" title="0.29 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.59 Mb" title="0.59 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.75 Mb" title="0.75 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_italiano.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.47 Mb" title="0.47 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Lengua Extranjera II (Portugués)</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lengua_extranjera_(portugues)_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.32 Mb" title="0.32 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.78 Mb" title="0.78 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.77 Mb" title="0.77 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_portugues.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Lenguaje y Práctica Musical</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_lenguaje_y_practica_musical_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.83 Mb" title="0.83 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"> </td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="92.65 Mb" title="92.65 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="2.31 Mb" title="2.31 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="80.36 Mb" title="80.36 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_lengua_practica_musical.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="64.72 Mb" title="64.72 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Literatura Universal</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_literatura_universal_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.14 Mb" title="0.14 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"> </td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.24 Mb" title="0.24 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.76 Mb" title="0.76 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.73 Mb" title="0.73 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_literatura_universal.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Matemáticas Aplicadas a las Ciencias Sociales II</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_aplicadas_a_las_ccss_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.19 Mb" title="0.19 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.21 Mb" title="0.21 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.38 Mb" title="0.38 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.97 Mb" title="0.97 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.88 Mb" title="0.88 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="6.51 Mb" title="6.51 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.42 Mb" title="1.42 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicas_aplicadas.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.53 Mb" title="1.53 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Matemáticas II</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_matematicas_ii_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="4.39 Mb" title="4.39 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.9 Mb" title="0.9 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.43 Mb" title="0.43 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.54 Mb" title="1.54 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.65 Mb" title="1.65 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.04 Mb" title="1.04 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_matematicasII.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.13 Mb" title="1.13 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Química</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_quimical_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.18 Mb" title="0.18 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.34 Mb" title="0.34 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.37 Mb" title="0.37 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.63 Mb" title="0.63 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.19 Mb" title="1.19 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.01 Mb" title="1.01 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.5 Mb" title="0.5 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.66 Mb" title="1.66 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_quimica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.84 Mb" title="1.84 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td bgcolor="#FEFDD6">Técnicas de Expresión Gráfico Plásticas</td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnicas_de_expresion_grafico-plastica_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.45 Mb" title="0.45 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.11 Mb" title="1.11 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.85 Mb" title="0.85 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.93 Mb" title="0.93 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.28 Mb" title="1.28 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="10.44 Mb" title="10.44 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.86 Mb" title="0.86 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="4.72 Mb" title="4.72 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="8.63 Mb" title="8.63 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center" bgcolor="#FEFDD6"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnicas_expresion_grafica.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.41 Mb" title="1.41 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
</tr>
<tr>
<td>Tecnología Industrial II</td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/criterios_selectividad/directrices_y_orientaciones_tecnologia_industrial_ii_2013_2014.pdf"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/pdf.gif" border="0" alt="0.13 Mb" title="0.13 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2005/sel_2005_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.09 Mb" title="0.09 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2006/sel_2006_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.12 Mb" title="0.12 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2007/sel_2007_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.22 Mb" title="0.22 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2008/sel_2008_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.27 Mb" title="0.27 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2009/sel_2009_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.14 Mb" title="1.14 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2010/sel_2010_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.35 Mb" title="0.35 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2011/sel_2011_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.07 Mb" title="1.07 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2012/sel_2012_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="1.02 Mb" title="1.02 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a></td>
<td align="center"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/paginas/distrito/examenes_sel_m25/selectividad_2013/sel_2013_tecnologia_industrial.zip"><img src="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/imagenes/winzip.gif" border="0" alt="0.94 Mb" title="0.94 Mb" align="middle" style="border-style: solid; border-color: #bcbcbc;" /></a><br /><br /></td>
</tr>
</tbody>
</table>
<h2 style="margin: 12pt 0cm; font-family: Arial, sans-serif; font-style: italic; font-weight: bold; font-size: 14pt; text-align: justify; text-transform: none; color: #222222; text-indent: 0cm; background-color: #e7eaad;"><span style="font-family: Tahoma, sans-serif; color: green; font-style: normal;"> Fechas de Selectividad curso 2013/14: </span></h2>
<ul style="color: #2a2a2a; font-size: 13px; font-weight: normal; text-align: justify; background-color: #e7eaad;">
<li><strong><span style="color: #2a2a2a; font-weight: normal;"><strong>Convocatoria ordinaria: </strong></span></strong><span style="color: #2a2a2a;">12, 13 y 14 de junio de 2014</span></li>
<li><strong style="color: #2a2a2a;">Convocatoria extraordinaria:</strong><span style="color: #2a2a2a;"> </span><span style="color: #2a2a2a;">16, 17 y 18 de septiembre de 2014</span></li>
</ul>
<p style="padding-left: 30px; color: #2a2a2a; font-size: 13px; font-weight: normal; text-align: justify; background-color: #e7eaad;"><a href="http://www.juntadeandalucia.es/innovacioncienciayempresa/sguit/g_b_calendario.php" target="_blank" style="color: #1155cc;">-&gt; Ver enlace para horarios de los exámenes</a></p>
</div>
</div>
<p align="left"><span style="font-family: comic sans ms,sans-serif;"><span style="font-size: 10pt;"> </span></span></p></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
        </div>
            </div>
                    <div class="items-row cols-2 row-0">
           <div class="item column-1">
    <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">2º ESO</span></h2>
<div class="art-postheadericons art-metadata-icons">
Visitas: 5013
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div>
<p> </p>
<h3 class="cajacomentario" style="margin-top: 2px; margin-bottom: 5px; padding: 5px; font-size: 13.3333px; text-transform: none; color: #2a2a2a; border: 2px solid #cccccc; outline: none 0px; vertical-align: baseline; border-radius: 8px 0px 8px 8px; min-height: 25px; text-align: center; background-color: #ffffff;"><a href="archivos_ies/Prog_2ESO2011_11.pdf" target="_blank">PROGRAMACIÓN DE 2º ESO DE MATEMÁTICAS<br /> CURSO 2014/15</a></h3>
<p style="font-size: 11.8181819915771px; text-align: center;"><span style="text-decoration: underline;"><strong style="font-size: 14pt;"><span style="color: #000000;">MATEMÁTICAS DE 2º ESO. CURSO 2014/15</span></strong></span></p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="text-align: center; background-color: #e7eaad;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR</strong></span><strong> PARA SEPTIEMBRE 2015</strong><strong>:</strong></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank">UNIDADES 3 Y 4</a></span></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a></p>
<p style="font-size: 11.818181991577148px;"> </p>
<hr style="text-align: center; background-color: #e7eaad;" />
<p style="text-align: center; background-color: #e7eaad;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank">UNIDAD 7</a></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank">UNIDAD 9 Y 10</a></span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Cuerpos_geometricos_resueltos.pdf" target="_blank">UNIDAD 11</a> (No estudiado en el curso 14/15)</span></p>
<p style="text-align: center; background-color: #e7eaad;"><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/2eso/Cuerpos_geometricos_resueltos.pdf" target="_blank" style="text-decoration: none; color: #2f310d;">UNIDAD 12</a> (No estudiado en el curso 14/15)</span></p>
<p style="font-size: 11.818181991577148px;"> </p>
<p style="font-size: 11.818181991577148px;">________________________________________________________________________________________________________</p>
<ul style="background-color: #e7eaad;">
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a>  EN SEPTIEMBRE 2015</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,6,9,10,12,16,17,18.</li>
<li><strong>Tema 2:</strong>  1,3,5,6,7, 11,13,15,16.</li>
<li><strong>Tema 3:</strong> 1,2,5,9,11,12,16,17,18,20.</li>
<li><strong>Tema 4:</strong>  1,5,8,11,12,13,16,19.</li>
<li><strong>Tema 5:</strong>  1,2,3,4,7,8,9, 10,11,12.</li>
<li><strong>Tema 6:</strong>  1,6,7,8,9,13,14,15.</li>
</ul>
</ul>
<p style="text-align: left; background-color: #e7eaad;"><strong style="text-align: left;">UNIDAD 1.</strong><span style="text-align: left;"> Números enteros</span></p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>2.</strong> Fracciones</p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>3.</strong> Números decimales</p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>4.</strong> Sistema sexagesimal</p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>5.</strong> Expresiones algebráicas</p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>6.</strong> Ecu<span style="color: inherit; font-family: inherit; font-size: inherit;">aciones de primer y segundo grado</span></p>
</div>
<div><strong style="color: #414141; background-color: #e7eaad;"> </strong></div>
<div><strong style="color: #414141; background-color: #e7eaad;"><a href="archivos_ies/14_15/mate/pendientes_2_eso_parte_2c.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong><strong style="color: #414141; background-color: #e7eaad;">  EN SEPTIEMBRE 2015</strong><strong style="color: #414141; background-color: #e7eaad;"> </strong></div>
<div>
<p style="background-color: #e7eaad;"><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>7.</strong> Sistemas de ecuaciones</span></p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>8.</strong> Proporcionalidad numérica</p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>9.</strong> Proporcionalidad geométrica</p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>10.</strong> Figuras planas. Áreas</p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>11.</strong> Cuerpos geométricos</p>
<p style="background-color: #e7eaad;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>12.</strong>Volumen de cuerpos geométricos</p>
</div>
<hr />
<table class="borde_outset" style="text-align: center; color: #2a2a2a;" border="1">
<tbody>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" title="Pulsar para descargar." style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a>,</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,11,12,13,19,21,28.</li>
<li><strong>Tema 2:</strong>1,6,8,14,16,17,18,19,20,23.</li>
<li><strong>Tema 3:</strong> 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43.</li>
<li><strong>Tema 4:</strong> Todos.</li>
<li><strong>Tema 5:</strong> 1,2,5,8,9,10,11,12,13.</li>
<li><strong>Tema 6:</strong> 1,2,8,11,29,31,32.</li>
</ul>
</ul>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;">PRIMER EXAMEN: 22/01/2015</strong></span></p>
<p><strong>TEMA 1</strong>. Números naturales</p>
<p><strong>TEMA 2.</strong> Divisibilidad</p>
<p><strong>TEMA 3.</strong> Fracciones</p>
<p><strong>TEMA 4.</strong>  Números decimales</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p><span style="font-family: Arial, sans-serif;"> </span></p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR.  </a></strong></span><a href="archivos_ies/13_14/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">Solamente los siguientes números por tema:</a>
<ul style="margin-top: 0.5em; margin-bottom: 0.5em;">
<li><strong>Tema 7:</strong>1,4,6,7,8,9,10,11,12,13.</li>
<li><strong>Tema 8:</strong>2,3,6,7,9,11,12,13,14,15.</li>
<li><strong>Tema 9: TODOS.</strong></li>
<li><strong>Tema 10:</strong>1,3,4,5,6,7,8,11,13,14.</li>
<li><strong>Tema 11:</strong> <strong>TODOS.</strong></li>
</ul>
</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 16/04/2015</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p><strong>TEMA 10.</strong> Polígonos</p>
<p><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p> </p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1</strong></span></p>
<p><a href="archivos_ies/13_14/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p><a href="archivos_ies/13_14/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p><a href="archivos_ies/13_14/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p><a href="archivos_ies/13_14/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p><a href="archivos_ies/13_14/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/13_14/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a><hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/13_14/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<a href="archivos_ies/13_14/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></td>
</tr>
</tbody>
</table>
<p> </p>
<p> </p>
<p style="font-size: 11.818181991577148px;"> </p></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
    </div>
                    <span class="row-separator"></span>
</div>
                <div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postcontent">
<div class="cat-children">
        <h3>Subcategorías</h3>
        
	<ul>
					<li class="last">
						<span class="item-title"><a href="/index.php?option=com_content&amp;view=category&amp;id=129&amp;Itemid=112">
				Matemáticas</a>
			</span>

										<div class="category-desc">
					<table class="headerContenidoUnderline" style="border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #666666;" width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td height="23">
<div class="headerContenidoPath" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; color: #333333; text-decoration: none;">Inicio &gt; DEPARTAMENTOS &gt; Matemáticas</div>
</td>
<td>
<div class="headerContenidoFecha" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; text-decoration: none; float: right; color: #129d0e;">Martes 01 de febrero de 2011</div>
</td>
</tr>
</tbody>
</table>
<div id="contenido" class="contenido" style="padding-top: 10px; padding-right: 20px; padding-bottom: 10px; padding-left: 20px;">
<div style="float: left;"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,32,18" data="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/skins/helvia/img/encabezado_interior.swf?colorin=45BB34&amp;texto=DEPARTAMENTOS" type="application/x-shockwave-flash" width="405" height="17">    </object>                  </div>
<div class="iconosTituloSeccion" style="float: right;"><img src="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/skins/helvia/img/btn_enviarnota.gif" border="0" alt="Enviar por email" style="cursor: pointer;" /> <img src="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/skins/helvia/img/btn_imprimir.gif" border="0" alt="Imprimir" style="cursor: pointer;" /> <a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/print.cgi?wid_seccion=5&amp;wid_item=95&amp;wOut=topdf" target="_blank"><img src="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/skins/helvia/img/btn_pdf.gif" border="0" alt="Generar PDF" /></a></div>
<div style="clear: both;"> </div>
<div id="tituloItem" class="blockTituloItem" style="display: block; margin-top: 15px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; border-bottom-width: 2px; border-bottom-style: dotted; border-bottom-color: #aaaaaa; float: left; width: 95%; padding: 0px;">
<div class="tituloIzq" style="float: left;"><img src="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/skins/helvia/img/vineta.gif" border="0" alt="vineta" /> <span class="tituloItem" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; letter-spacing: 1pt; color: #71cd63;">Departamento de Matemáticas.</span></div>
</div>
<div id="contenido_sitio" style="width: 95%; float: left; display: block;">
<table class="borde_outset" style="text-align: center; border: 3px outset #666666;" border="1">
<tbody>
<tr>
<td>
<p style="text-align: center;">1º ESO A</p>
</td>
<td>
<p style="text-align: center;">2º ESO A</p>
</td>
<td>
<p style="text-align: center;">3º ESO A</p>
</td>
<td>
<p style="text-align: center;">4º ESO A</p>
</td>
</tr>
<tr>
<td>
<p style="text-align: center;">1º ESO B</p>
</td>
<td>
<p style="text-align: center;">2º ESO B</p>
</td>
<td>
<p style="text-align: center;">3º ESO B</p>
</td>
<td>
<p style="text-align: center;">4º ESO B</p>
</td>
</tr>
<tr>
<td>
<p style="text-align: center;">1º ESO C</p>
</td>
<td>
<p style="text-align: center;">2º ESO C</p>
</td>
<td>
<p style="text-align: center;">3º ESO C</p>
</td>
<td>
<p style="text-align: center;">4º ESO C</p>
</td>
</tr>
<tr>
<td>
<p style="text-align: center;">1º ESO D</p>
</td>
<td>
<p style="text-align: center;">2º ESO D</p>
</td>
<td>
<p style="text-align: center;">3º ESO D</p>
</td>
<td>
<p style="text-align: center;">4º ESO D</p>
</td>
</tr>
<tr>
<td>
<p style="text-align: center;">1º BACH A</p>
</td>
<td>
<p style="text-align: center;">1º BACH B</p>
</td>
<td>
<p style="text-align: center;">1º BACH C</p>
</td>
<td rowspan="2">
<p style="text-align: center;">CICLO SUPERIOR</p>
</td>
</tr>
<tr>
<td>
<p style="text-align: center;"><a id="81" href="index.php?option=com_content&amp;view=article&amp;id=81:2bacha&amp;catid=38&amp;Itemid=69" target="_blank" title="Página web de 2º BACH A MAT II." style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">2º BACH A</a></p>
</td>
<td>
<p style="text-align: center;">2º BACH B</p>
</td>
<td>
<p style="text-align: center;"><a href="index.php?option=com_content&amp;view=article&amp;id=82:2bachc&amp;catid=38&amp;Itemid=69" target="_blank" title="Página web de 2º BACH. II CC SS. Profesor Miguel Anguita." style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">2º BACH C</a></p>
</td>
</tr>
<tr>
<td>PENDIENTES DE 1º ESO</td>
<td>PENDIENTES DE 2º ESO</td>
<td>PENDIENTES DE 3º ESO</td>
<td><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/index.cgi?wid_seccion=1&amp;wid_item=89" target="_blank" title="Página web de pendientes de 1º BACH." style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">PENDIENTES DE 1º BACH</a></td>
</tr>
<tr>
<td>PROYECTO INTEGRADO 2º BACH</td>
<td>LECTURAS MATEMÁTICAS</td>
<td>PASATIEMPOS MATEMÁTICOS</td>
<td>SELECTIVIDAD 2º BACH</td>
</tr>
</tbody>
</table>
<ul>
<li><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/upload/CriteriosEvMAT.pdf" target="_blank" title="CRITERIOS DE EVALUACIÓN DE MATEMÁTICAS." style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">CRITERIOS DE EVALUACIÓN DE MATEMÁTICAS. CURSO 2010/11.<br /></a></li>
</ul>
<ul>
<li><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/index.cgi?wid_seccion=1&amp;wid_item=2" title="index.cgi?wid_seccion=1&amp;wid_item=2" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Matemáticas CC SS II de 2º Bachillerato.</a></li>
<li><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/index.cgi?wid_seccion=1&amp;wid_item=8" title="index.cgi?wid_seccion=1&amp;wid_item=8" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Matemáticas II de 2º Bachillerato.</a></li>
<li><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/index.cgi?wid_seccion=1&amp;wid_item=89" title="index.cgi?wid_seccion=1&amp;wid_item=89" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Pendientes de 1º  de Bachillerato CC SS I.</a></li>
<li><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/index.cgi?wid_seccion=1&amp;wid_item=77" title="index.cgi?wid_seccion=1&amp;wid_item=77" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Proyecto Integrado de 2º de Bachillerato, curso 2009/10.</a></li>
<li><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/index.cgi?wid_seccion=1&amp;wid_item=71" title="index.cgi?wid_seccion=1&amp;wid_item=71" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Lecturas recomendadas 2009/10.</a></li>
<li><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/index.cgi?wid_seccion=1&amp;wid_item=78" title="index.cgi?wid_seccion=1&amp;wid_item=78" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">1º de ESO.</a></li>
<li>2º de ESO.</li>
<li>3º de ESO.</li>
<li>4º de ESO.</li>
<li>1º de Bachillerato CC SS i.</li>
<li>1º de Bachillerato I.</li>
<li><a href="http://redcentros.ced.junta-andalucia.es/centros-tic/18700441/helvia/sitio/index.cgi?wid_seccion=1&amp;wid_item=79" title="index.cgi?wid_seccion=1&amp;wid_item=79" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;">Pendientes.</a><hr /></li>
</ul>
<p>Páginas personales del profesorado:</p>
<ul>
<li>Gerardo Bustos. <a href="http://perso.gratisweb.com/gerardobustos/" target="_blank" title="http://perso.gratisweb.com/gerardobustos/" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><strong><span style="font-family: Arial, Verdana, sans-serif; color: #000000; font-size: 14pt;"><span style="font-size: 16px;">Entrar</span></span></strong></a>.</li>
<li> <a href="index.php?option=com_content&amp;view=article&amp;id=83:manguita&amp;catid=38&amp;Itemid=69">Miguel Anguita</a>.</li>
</ul>
<table border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="text-align: center;"><a href="http://www.estadisticasgratis.com/" target="_blank" style="color: #129d0e; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal; text-decoration: underline;"><img src="http://traces.estadisticasgratis.com/trace.php?page=642482&amp;url=http%3A//www.estadisticasgratis.com/ins_url_run.php%3FURL%3Dhttp%253A%252F%252Fwww.juntadeandalucia.es%252Faverroes%252Fcentros-tic%252F18700441%252Fhelvia%252Fsitio%252Findex.cgi%253Fwid_seccion%253D5%2526wid_item%253D95%26Title%3DDepartamento+de+MAtem%25E1ticas.+IES+Miguel+de+Cervantes.+Granada.%26Description%3DDepartamento+de+MAtem%25E1ticas.+IES+Miguel+de+Cervantes.+Granada.%26userstring%3Dndcz%26cboCateg%3D6%26cboFont%3DCLOUDS%26InicioContador%3D0%26AccesoEstad%3D0%26btnAcceder%3DRegistrar+URL&amp;title=Estad%EDsticas%20y%20contadores%20web%20gratis&amp;referer=http%3A//www.estadisticasgratis.com/ins_url.php%3Ferror%3D2&amp;resolution=1280x800&amp;color=32&amp;language=es&amp;java=1&amp;cookies=1&amp;browser=Netscape&amp;os=Windows%20NT&amp;nuser=1&amp;visitor=0.635547011392191&amp;rnum0.5834838040173054&amp;digits=6&amp;canal=1" border="0" alt="Estadisticas y contadores web gratis" /></a></td>
</tr>
<tr>
<td style="text-align: center;"><span id="EstadCode" style="font-size: 11px; font-family: arial;"><a id="LinkKey0" href="http://www.estadisticasgratis.com/" style="color: #129d0e; font-family: arial; font-size: 11px; font-weight: normal; text-decoration: underline;">Contadores Gratis</a></span></td>
</tr>
</tbody>
</table>
</div>
<div class="fechaModif" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; font-weight: bold; text-align: left; color: #666666; text-decoration: none;">Ultima actualización: 03/11/2010</div>
</div>				</div>
			            
						<dl>
				<dt>
					Número de Artículos:				</dt>
				<dd>
					12				</dd>
			</dl>
			
					</li>
				</ul>
    </div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:77:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Dpto. de Lengua Castellana";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:2:{s:101:"/index.php?option=com_content&amp;view=category&amp;id=99&amp;Itemid=112&amp;format=feed&amp;type=rss";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:102:"/index.php?option=com_content&amp;view=category&amp;id=99&amp;Itemid=112&amp;format=feed&amp;type=atom";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:560:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});window.addEvent('domready', function() {
			$$('.hasTip').each(function(el) {
				var title = el.get('title');
				if (title) {
					var parts = title.split('::', 2);
					el.store('tip:title', parts[0]);
					el.store('tip:text', parts[1]);
				}
			});
			var JTooltips = new Tips($$('.hasTip'), { maxTitleChars: 50, fixed: false});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:5:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}i:1;O:8:"stdClass":2:{s:4:"name";s:25:"Área Socio-Lingüística";s:4:"link";s:59:"index.php?option=com_content&view=article&id=186&Itemid=572";}i:2;O:8:"stdClass":2:{s:4:"name";s:26:"Dpto. de Lengua Castellana";s:4:"link";s:59:"index.php?option=com_content&view=article&id=163&Itemid=112";}i:3;O:8:"stdClass":2:{s:4:"name";s:12:"MATEMÁTICAS";s:4:"link";s:60:"index.php?option=com_content&view=category&id=213&Itemid=112";}i:4;O:8:"stdClass":2:{s:4:"name";s:12:"Matemáticas";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
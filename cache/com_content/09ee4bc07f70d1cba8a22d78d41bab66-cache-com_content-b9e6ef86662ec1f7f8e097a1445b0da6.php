<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:12231:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">CURSO ACADÉMICO 2014/15</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Lunes, 01 Diciembre 2014 20:15</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=37:relacion-de-departamentos&amp;catid=100&amp;Itemid=59&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=b367c5242ec0caa0bdc4d3970c82bf61bc90758a" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 5308
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div id="contenido_sitio" style="float: left; display: block;"><span style="font-size: 16pt;"> </span></div>
<h2>Relación de Departamentos, CURSO 2014/15<span style="color: #2a2a2a; font-size: 13px;"> </span></h2>
<div style="color: #000000; font-family: Calibri; font-size: 16px; background-color: #ffffff;"> </div>
<table style="width: 100%; font-size: 12px;" border="0" cellspacing="4" cellpadding="0">
<tbody>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; color: #ffffff; padding-left: 15px; font-family: arial, sans-serif; font-size: 14px; font-weight: bold; background-color: #5a8bc7;" align="left" width="30%">Departamento</td>
<td style="margin: 0px; color: #ffffff; padding-left: 15px; font-family: arial, sans-serif; font-size: 14px; font-weight: bold; background-color: #5a8bc7;" align="left" width="70%">Componentes</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"> </td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"><strong><em> </em></strong></td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #ffffff;"><strong>Biología y Geología</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #ffffff;">
<div>Dª María del Mar Martín Molina</div>
<div>Dª María Inmaculada Rojo Camacho: Jefatura del Departamento</div>
<div>Dª Amparo Ibáñez Ausina</div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Dibujo</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p style="color: #ff0000;"><a href="index.php?option=com_content&amp;view=article&amp;id=76:departamento-de-dibujo&amp;catid=97:educacion-plastica&amp;Itemid=98" target="_blank"><span style="color: #000000;">D. Juan Carlos </span><span style="color: #000000;">Lazuen Alcón</span></a></p>
<p style="color: #000000;"><a href="index.php?option=com_content&amp;view=article&amp;id=77:do-emilia-vilchez-campillos&amp;catid=97:educacion-plastica&amp;Itemid=134">Dª Emilia Vílchez Campillos</a>:  Jefatura del Departamento</p>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Economía</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p><a href="index.php?option=com_content&amp;view=article&amp;id=169:economia&amp;catid=76:economia" target="_blank">D. David Maldonado Cambil</a></p>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Educación Física</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Dª María José Muro Jiménez: Jefatura del Departamento<br /><br /></div>
<div>Dª Nuria Manrique Mora<br /><br /></div>
<div>Dª Rosa María Crovetto González</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Filosofía</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p><span style="color: #000000; font-family: Calibri; font-size: 16px;">D. Ricardo de la Blanca Torres</span></p>
<p><a href="index.php?option=com_content&amp;view=article&amp;id=166:dep-filosofia&amp;catid=102:departamentos-de-humanidades" target="_blank">D. José Luis Abian Plaza</a></p>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Física y Química</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p style="font-family: arial, sans-serif;">Dª María Elisa Mazuecos González: Jefatura del Departamento</p>
<p style="font-family: arial, sans-serif;">D. Alfonso Centeno Gómez</p>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Francés</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Dª María Ester Castillo Fernández: Jefatura del Departamento<br /><br /></div>
<div>Dª María Encarnación Peña Fernández<br /><br /></div>
<div>Dª Francisca Peña Fernández<br /><br /></div>
<div>D. Antonio Martín Olmos</div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Geografía e Historia</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>D. Miguel Ángel Hitos Urbano<br /><br /></div>
<div>D. Juan Antonio Pachón Romero<br /><br /></div>
<div>Dª Justina Castillo García<br /><br /></div>
<div>Dª María del Carmen Palma Moreno: Jefatura del Departamento<br /><br /></div>
<div>D. Santiago Freire Morales<br /><br /></div>
<div>Dª María del Pilar Vicente Zapata<br /><br /></div>
<div>D. Miguel Ángel Salort Medina</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p><strong>Inglés</strong></p>
</td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Dª Silvia Navas-Parejo Casado<br /><br /></div>
<div>D. Carlos Agustín Fernández-Amigo Sánchez<br /><br /></div>
<div>Dª Mercedes León Ramos<br /><br /></div>
<div>Dª Francisca Pérez Román<br /><br /></div>
<div>Dª Leonor Aranda Castro: Jefatura del Departamento<br /><br /></div>
<div>Dª María Sacramento Ortiz Comerma<br /><br /></div>
<div>Dª Mª Sandra González Molina</div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Informática</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p>Dª <span style="font-size: 12px;">Esperanza </span><span style="font-size: 12px;">Vaquera Heredia</span></p>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Latín</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div style="text-align: justify;">  </div>
<div style="text-align: justify;"><span style="font-size: 12px;">Dª María Dolores Zambrano Torres : Jefatura del Departamento</span></div>
<div style="text-align: justify;"><span style="font-size: 12px;"> </span></div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"><strong>Lengua y Literatura</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;">
<div>Dª Encarnación Jiménez Ávila: Jefatura del Departamento<br /><br /></div>
<div>D. Manuel Valle García<br /><br /></div>
<div>D. Felipe Bueno Maqueda<br /><br /></div>
<div>Dª Concepción Moles Ocaña<br /><br /></div>
<div>D. Manuel Jesús Gilabert Gómez<br /><br /></div>
<div>Dª Rosalía Parras Chica<br /><br /></div>
<div>Dª Silvia Gálvez Montes</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Matemáticas</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>D. Natalio Jesús Rodríguez Cano: Jefatura del Departamento<br /><br /></div>
<div>D. Gerardo Bustos Gutiérrez<br /><br /></div>
<div>Dª Magdalena Carretero Rivas<br /><br /></div>
<div>Dª María Candelas González Dengra<br /><br /></div>
<div>D. Miguel Anguita Gay<br /><br /></div>
<div>Dª Clotilde García Sánchez<br /><br />Dª Olga María de la Higuera Vílchez</div>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"><strong>Música</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;">
<div>
<p style="font-family: arial, sans-serif;"><span style="font-size: 12px;">Dª María Angustias Sánchez-Montes González: Jefatura del Departamento</span></p>
</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Orientación Educativa</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p>D. José Luis Augustin Morales: Jefatura del Departamento</p>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;"><strong>Pedagogía Terapéutica</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #e2e6ec;">
<p>Dª Ana Belén Ramos Rodenes</p>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #ffffff;"><strong>Religión</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<p>Dª Francisca García Guirado</p>
</td>
</tr>
<tr style="background-color: #e2e6ec;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;"><strong>Tecnología</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Dª Isabel Madrid Barbero: Jefatura del Departamento<br /><br /></div>
<div>Dª Jennifer Francisca Valero Sánchez</div>
</td>
</tr>
<tr style="background-color: #ffffff;">
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif; background-color: #ffffff;"><strong>Ciclo Superior</strong></td>
<td style="margin: 0px; padding-left: 15px; font-family: arial, sans-serif;">
<div>Encarnación Marín Torres. (Servicios a la Comunidad)<br /><br /></div>
<div>D. Juan Manuel Prieto Tellado. (Intervención Sociocomunitaria) <br /><br /></div>
<div>D. Andrés Arcas Díaz (Intervención Sociocomunitaria): Jefatura del Departamento</div>
</td>
</tr>
</tbody>
</table></div>
</div>
<div class="cleared"></div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:93:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Departamentos del IES Miguel de Cervantes.";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:13:"ÁREAS Y DPT.";s:4:"link";s:57:"index.php?option=com_content&view=article&id=37&Itemid=59";}}s:6:"module";a:0:{}}
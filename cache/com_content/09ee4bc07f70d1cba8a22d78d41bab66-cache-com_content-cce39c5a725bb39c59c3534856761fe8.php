<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:16114:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Documentos</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Martes, 01 Octubre 2013 17:20</span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=144:documentos&amp;catid=241&amp;Itemid=609&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=0f69454eaeb4b110373d8c58b1443547a0ab58a4" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 4136
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><div>
<h3 style="text-align: left;"><span style="text-decoration: underline;"><span style="color: #663300;"><strong>DOCUMENTOS RELACIONADOS CON LA JEFATURA DE ESTUDIOS</strong></span></span></h3>
<ul>
<li><strong><span style="color: #663300;">Alumnos y Padres de Alumnos</span></strong></li>
<ul>
<li><a href="archivos_ies/Faltasalumnos.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #663300; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px; background-color: #f8f8f8;">Justificación de faltas y autorización para ausentarse del Centro.</span></span></a></li>
<li><a href="archivos_ies/justificacion_de_faltas.docx" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #663300; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px; background-color: #f8f8f8;">Solicitud de Justificación de Ausencias de Alumnos</span></span></a></li>
<li><a href="archivos_ies/reclamaciones_de_las_calificaciones_finales.docx" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #663300; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px; background-color: #f8f8f8;">Procedimiento Reclamaciones Calificaciones Finales</span></span><br /></a></li>
<li><a href="archivos_ies/Plano_Centro_2012-2013.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #663300; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px; background-color: #f8f8f8;">Ubicación de las aulas</span></span></a></li>
</ul>
</ul>
<ul>
<li><strong><span style="color: #663300;"> Departamentos Didácticos</span></strong></li>
<ul>
<li><a href="archivos_ies/memoria_de_departamento_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><strong><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px;"><span style="color: #663300;"><span style="font-weight: normal; background-color: #f8f8f8;">Memoria de Departamento rellenable</span></span><br /></strong></strong></a></li>
<li><strong><a href="archivos_ies/coordinacion_de_la_programacin_eso_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Seguimiento programación en la ESO</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/coordinacion_de_la_programacin_bachillerato_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Seguimiento programación en Bachillerato</a></strong></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/programacion_de_aula_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Programación de Aula</a></strong></strong></strong></li>
</ul>
</ul>
<ul>
<li><strong> </strong><strong><span style="color: #663300;">Personal del Centro</span></strong></li>
<ul>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/circular_permisos_y_licencias.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Circular de Licencias y Permisos</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/instruccion_2_2013.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Instrucción 2/2013, puesta en práctica medidas fiscales</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/solicitud_propuesta_de_abono.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Solicitud propuesta de abono incapacidad temporal</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/cuadro_resumen_permisos_y_licencias.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Cuadro resumen de permisos y licencias</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/grados_de_parentesco.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Grados de parentesco</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/solicitud_permisos_y_licencias_RELLENABLE.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Solicitud de Permisos y Licencias Personal (rellenable)</a></strong></li>
<li><strong style="color: #000000; font-family: Arial, Helvetica, sans-serif; font-size: 12px; line-height: 18px; background-color: #e5e5e5;"><a href="archivos_ies/FaltasdelPersonal.docx" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Justificación de faltas del profesorado y PAS </a></strong></li>
<li></li>
</ul>
<li><strong><span style="color: #663300;">Guías de evaluación de las Competencias básicas</span></strong></li>
<ul>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_competencias_basicas_social.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Competencia básica Social y Ciudadana</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_competencias_basicas_lengua.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Comunicación lingüística (lengua española)</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_competencias_basicas_lengua_extranjera.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Comunicación lingüística (lenguas extranjeras)</a></span></strong></strong></span></span></li>
<li><a href="archivos_ies/guia_evaluacion_competencias_basicas_conocimiento.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;"><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><span style="font-weight: normal; background-color: #f8f8f8;">Conocimiento e interacción con el mundo físico y natural</span></span></strong></strong></span></span></a></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_competencias_basicas_matematicas.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Razonamiento matemático</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_evaluacion_destrezas_lectoras.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Guía de evaluación de destrezas lectoras</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/guia_indicadores_homologados.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Guías de Indicadores Homologados: Autoevaluación de centros docentes de Andalucía</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/pisa_2009.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Informe PISA 2009</a></span></strong></strong></span></span></li>
<li><span style="color: #000000; font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 12px; line-height: 18px;"><strong><strong style="background-color: #e5e5e5;"><span style="color: #663300;"><a href="archivos_ies/informe-espanol-panorama-educacion-ocde.pdf" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-weight: normal; letter-spacing: normal; text-align: start; background-color: #f8f8f8;">Indicadores de la OCDE 2009. Informe español</a></span></strong></strong></span></span></li>
<li></li>
</ul>
</ul>
</div>
<div>
<div>
<ul>
<li><strong><span style="color: #663300;">Tutorías</span></strong><span> </span></li>
<ul>
<li><span><a href="archivos_ies/memoria_de_tutora_RELLENABLE.doc" target="_blank" style="outline-style: none; text-decoration: initial; color: #663300; font-family: Arial, Helvetica, sans-serif; font-size: 12px; letter-spacing: normal; line-height: 18px; text-align: start; background-color: #f8f8f8;">Memoria de Tutoría</a></span></li>
</ul>
</ul>
</div>
<div> </div>
<div style="text-align: -webkit-auto;"> </div>
</div>
<div class="fechaModif"> </div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">IES Miguel de Cervantes- General</span> / <span class="art-post-metadata-category-name">Jefatura de Estudios</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:61:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Documentos";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";N;}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:1:{i:0;O:8:"stdClass":2:{s:4:"name";s:10:"Documentos";s:4:"link";s:59:"index.php?option=com_content&view=article&id=144&Itemid=609";}}s:6:"module";a:0:{}}
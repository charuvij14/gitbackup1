<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:3947:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">CURSO 2014/15</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Nueva normativa sobre permisos y licencias.</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Viernes, 08 Febrero 2013 07:42</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=193:nueva-normativa-en-materia-de-permisos-y-licencias&amp;catid=194&amp;Itemid=507&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=dfafbeeb37b26d7a2e4ee680a153a1aace0f7d0c" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 6950
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><h4><span style="text-decoration: underline;">Nueva normativa en materia de permisos y licencias.</span></h4>
<div> </div>
<div>En materia de permisos y licencias nos regíamos por una circular publicada el 6 de abril de 2005. Esta circular tenía carencias muy claras, por ejemplo, no contemplaba la justificación de ausencias por cuidado de menores, a no ser q<span>ue fuera por enfermedad grave, lo que hacía difícil justificar faltas de pocos días por tener que cuidar de un hijo aquejado por afecciones leves, salvo que pasáramos por urgencias.</span></div>
<div style="font-family: arial,helvetica,sans-serif; font-size: 13.33px; font-style: normal; background-color: transparent;"> </div>
<div>Pues bien, se ha publicado una nueva circular que sustituye a la anterior. Se adjunta el texto completo y se recomienda leer con atención, pues contiene novedades interesantes. Contempla:</div>
<ul>
<li>Días de permiso por enfermedad infecciosa de niños menores de nueve años </li>
<li>Introduce algunas figuras de conciliación de la vida laboral y familiar.</li>
<li>En las últimas páginas viene un impreso rellenable de justificación de ausencias, puede emplearse para rellenar desde un ordendor e imprimirse cuando se necesite.</li>
</ul>
<p><a href="archivos_ies/Circular6febrero2013LicenciasPermisos.pdf" target="_blank">Texto completo de la circular.</a></p>
<p> <a href="stories/cervantes/Resumen_permisos_licencias1.png" target="_blank"><img src="stories/cervantes/Resumen_permisos_licencias1.png" border="0" width="671" height="410" style="border: 0;" /></a></p>
<p><a href="stories/cervantes/Resumen_permisos_licencias2.jpg" target="_blank"><img src="stories/cervantes/Resumen_permisos_licencias2.jpg" border="0" width="678" height="507" /></a></p></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">IES Miguel de Cervantes- General</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:94:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Nueva normativa sobre permisos y licencias.";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:4:{i:0;O:8:"stdClass":2:{s:4:"name";s:6:"INICIO";s:4:"link";s:53:"index.php?option=com_content&view=featured&Itemid=250";}i:1;O:8:"stdClass":2:{s:4:"name";s:11:"AVISO LEGAL";s:4:"link";s:59:"index.php?option=com_content&view=article&id=170&Itemid=507";}i:2;O:8:"stdClass":2:{s:4:"name";s:32:"IES Miguel de Cervantes- General";s:4:"link";s:60:"index.php?option=com_content&view=category&id=194&Itemid=507";}i:3;O:8:"stdClass":2:{s:4:"name";s:43:"Nueva normativa sobre permisos y licencias.";s:4:"link";s:0:"";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:9645:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Poemas para  una crisis</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">POEMAS PARA UNA CRISIS</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Domingo, 16 Diciembre 2012 09:44</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=178:poemas&amp;catid=199&amp;Itemid=83&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=e3b029288346c86943e1d87fbfd77717c3ec758d" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 2419
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p>&nbsp;</p>
<div style="float: left; display: block; width: 100%;" id="contenido_sitio">
<table style="width: 100%; font-family: 'Times New Roman'; font-size: medium;" border="0" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td bgcolor="#eef0f9">&nbsp;</td>
<td bgcolor="#eef0f9" valign="top">
<p style="color: #000033;" class="Estilo11" align="justify"><img border="0" src="archivos_ies/actividades/imagenes/poemas/titulo.jpg" width="100%" height="118" /></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><span style="color: #000033; font-size: 12pt;" class="Estilo11">Biblioteca del Instituto de Enseñanza Secundaria&nbsp;<em>Miguel de Cervantes</em> de Granada, a las 9’25 horas del lunes 1 de Diciembre de 2008. El poeta&nbsp;<strong>Ignacio López de Aberasturi</strong>, con la puntualidad y el entusiasmo del que actúa por vocación y devoción, recita y explica sus poemas a un nutrido grupo de alumnos, previamente informados y formados, que tratan de captar directamente las urdimbres de la inspiración, las sutilezas de una exquisita expresión, los connotados referentes existenciales..., con que se teje la buena poesía.<br />Aunque se trata de una actividad enmarcada en el&nbsp;<em>Plan de Lectura y Biblioteca</em>, fue programada y apoyada por el claustro de profesores desde el convencimiento de que el encuentro de un autor con unos lectores, que durante cierto tiempo han mantenido un diálogo silencioso, es insustituible para la reflexión y formación de la sensibilidad. El recital pone cara, voz, tono y estilo a nuestro sigiloso interlocutor y posibilita la oportunidad de requerir respuestas a quien tantas preguntas nos ha suscitado. Y eso fue lo que hizo el nutrido grupo de alumnos asistentes, evidenciar sus inquietudes desde&nbsp; preguntas espontáneas referidas a influencias literarias, origen de la vocación poética y lecturas de iniciación, hasta el papel que juega la poesía en un mundo tan mediatizado y poco propicio a la reflexión.<br /><br /></span></p>
<p style="color: #000033; font-size: medium;" class="Estilo18" align="center"><img src="archivos_ies/actividades/imagenes/poemas/IMG_3373%20[640x480].JPG" width="100%" height="480" /></p>
<p>&nbsp;</p>
<p style="color: #000033;" class="Estilo11" align="justify"><span style="font-size: 12pt;">Si, como es el caso, nuestro interlocutor es un poeta, se propicia la ocasión de identificar las emociones, tímidamente atisbadas por el lector y sabiamente inducidas por el autor entre ritmos, cadencias, resonancias, palabras justas y escogidas. Porque la poesía es una escritura de alta tensión emocional, espacial y temporal, capaz de germinar en un receptivo lector todo tipo de emotividades, incluida la estética. Un buen poema es un instante de vida concentrada en frasco pequeño, como el gran perfume, quintaesencia destilada de miradas, palabras, oídos, tactos...<br /><br /></span></p>
<p style="color: #000033; font-size: medium;" class="Estilo18" align="center"><img border="0" src="archivos_ies/actividades/imagenes/poemas/IMG_3380%20%5B640x480%5D.JPG" width="100%" height="480" /></p>
<p>&nbsp;</p>
<p style="color: #000033;" class="Estilo11" align="justify"><span style="font-size: 12pt;">Sabemos que escribir bien es el resultado subsiguiente a un acopio de lecturas selectas, una práctica adecuada y cierta sensibilidad innata o adquirida; y que escribir de manera singular es el logro de quienes, además, son capaces de emocionar al lector. A este grupo pertenecen los poetas de verdad, POETAS con mayúscula, que afrontan el difícil reto que les plantea un mundo moderno insensible, arrogante, embaucador y materialista. Frente a tan poderoso señor, la humildad de un compendio de escasas palabras, sentimientos universales implícitamente insinuados, reflexiones morales, deseadas utopías, gritos silenciosos, equidistancia entre dicha y desdicha, pudor y desahogo, conciencia íntima y colectiva, génesis de sensaciones, sosiego, reflexión...</span></p>
<p>&nbsp;</p>
<p style="color: #000033; font-size: medium;" class="Estilo18" align="center"><img border="0" src="archivos_ies/actividades/imagenes/poemas/IMG_3384%20%5B640x480%5D.JPG" width="100%" height="432" /></p>
<p style="color: #000033; font-size: medium;" class="Estilo18" align="justify">&nbsp;</p>
<p style="color: #000033; font-size: medium;" class="Estilo18" align="justify"><span style="color: #000033; font-size: 12pt;" class="Estilo11">De todo esto hizo gala nuestro invitado, El POETA Ignacio López de Aberasturi, activo colaborador de talleres de poesía y revistas literarias. Desglosó minuciosamente la amplia gama de matices de su magnífica y selecta obra literaria, recogida, hasta la fecha, en tres libros:&nbsp;<br />-<strong>Los términos de la entrega. </strong>Diputación de Córdoba. Colección Polifemo, 1993.&nbsp;<br />-<strong>Las armas depuestas</strong>. Diputación de Granada. Colección Genil, 1996.<br />-&nbsp;<strong>Salón de mapas</strong>. Inédito.<br />Para finalizar, transcribimos uno de los poemas más comentados en este acto, como muestra de esta joven poesía, concebida como reflexión moral, en la que se reivindica la conciencia y la memoria de los acontecimientos que tienen lugar a nuestro alrededor, a la vez que intenta hacer sentir con el poema. Los sentimientos personales quedan en un discreto segundo lugar o muy tamizados.</span></p>
<p style="color: #000033; font-size: medium;" class="Estilo18" align="justify"><span style="color: #000033; font-size: 12pt;" class="Estilo11">&nbsp;</span></p>
<p style="color: #000033; font-size: medium; text-align: center;" class="Estilo18" align="center"><span style="color: #000033; font-size: 12pt;" class="Estilo11"><em>YO NO SÉ CONTAR</em><br />Desde la noche&nbsp;<br />vienen los días.</span></p>
<p style="text-align: center;">Desde detrás de la noche<br />vienen estos muertos. Una docena o más.<br />Acaso miles.<br />Desde todas las esquinas caen&nbsp;<br />muertos,<br />niños quebrados,<br />quince, doscientas treinta manos<br />apagadas,<br />no sé contar,<br />más o menos mil,<br />más o menos más.<br />Sin pulso en el camastro,<br />muertos llenos de sombra,<br />etíopes,<br />hindúes,<br />de frío<br />o por consenso,<br />muertos que no se llaman,<br />muertos imperdonables,<br />y acá otros más, yo no sé contar,<br />y más muertos bajando por la ladera.<br />Ya no pregunto<br />por la piel de los cometas<br />ni espero a Dios a estas alturas,<br />sólo busco una frontera, el ministerio,<br />o siquiera un pozo para estallar la desazón<br />de no saber contar los muertos que me llegan:<br />dieciocho, ciento ochenta&nbsp;<br />o un cero más a la derecha.<br />Para esto yo no sirvo.<br />No me argumenten.<br />Lo que yo sé son<br />tres,<br />cuatro,<br />los dedos de la mano,<br />tantos puntos&nbsp;<br />o los impresos necesarios.<br />Pero yo no sé contar los muertos.<br />¿Acaso se suman uno encima de otro,<br />o debo ir restando los ancianos?<br />¿o las mujeres solas?<br />¿o los bienaventurados?</p>
<p style="text-align: center;">Yo no sé contar&nbsp;<br />y desde esta noche<br />vendrán más días,<br />y desde esta noche&nbsp;<br />se preparan más muertos.</p>
<p>&nbsp;</p>
<p><strong>(Juan Naveros Sánchez – Coordinador del Plan de Lectura y Biblioteca del I.E.S. Miguel de Cervantes).</strong></p>
</td>
</tr>
</tbody>
</table>
</div>
<div class="fechaModif">Ultima actualización: 13/11/2010</div>
<div id="separadorContenido" class="separadorContenido">&nbsp;</div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-name">Actividades</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:74:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - Poemas para  una crisis";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:2:{i:0;O:8:"stdClass":2:{s:4:"name";s:11:"ACTIVIDADES";s:4:"link";s:59:"index.php?option=com_content&view=category&id=199&Itemid=76";}i:1;O:8:"stdClass":2:{s:4:"name";s:23:"Poemas para  una crisis";s:4:"link";s:58:"index.php?option=com_content&view=article&id=178&Itemid=83";}}s:6:"module";a:0:{}}
<?php die("Access Denied"); ?>#x#a:4:{s:4:"body";s:26245:"<div class="item-page"><div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Artículos</span></h2>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
<div class="art-box art-post">
    <div class="art-box-body art-post-body">
<div class="art-post-inner">
<div class="art-postmetadataheader">
<h2 class="art-postheader"><span class="art-postheadericon">Pendientes de Matemáticas. Curso 2015/16</span></h2>
<div class="art-postheadericons art-metadata-icons">
<span class="art-postdateicon">Última actualización en Jueves, 17 Diciembre 2015 18:54</span> | <span class="art-postauthoricon">Escrito por <a href="/index.php?option=com_contact&amp;view=contact&amp;id=" >Miguel Anguita</a></span> | <a href="/index.php?option=com_content&amp;view=article&amp;id=236:pendientes-de-matematicas-curso-2013-14&amp;catid=129&amp;Itemid=671&amp;tmpl=component&amp;print=1&amp;layout=default&amp;page=" title="Imprimir" onclick="window.open(this.href,'win2','status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no'); return false;" rel="nofollow"><img src="/templates/verde10/images/system/printButton.png" alt="Imprimir"  /></a> | <a href="/index.php?option=com_mailto&amp;tmpl=component&amp;template=verde10&amp;link=dcfffb05f8c265de9208eebc0b69af7255db034a" title="Email" onclick="window.open(this.href,'win2','width=400,height=350,menubar=yes,resizable=yes'); return false;"><img src="/templates/verde10/images/system/emailButton.png" alt="Email"  /></a> | Visitas: 72813
</div>
</div>
<div class="art-postcontent">
<div class="art-article"><p> </p>
<hr />
<div id="contenido_sitio" style="text-align: justify; background-color: #e7eaad; float: left; width: 793.796875px;">
<h2><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">PENDIENTES DE MATEMÁTICAS DE CURSOS ANTERIORES:</span></strong></span></h2>
<table style="width: 90%;" border="0">
<tbody>
<tr>
<td style="font-family: arial,sans-serif; margin: 0px;">
<div id="contenido_sitio" style="font-family: 'Century Gothic', Arial, Helvetica, sans-serif; font-size: 13.3333339691162px; text-align: justify; float: left; width: 793.796875px; background-color: #e7eaad;">
<p style="font-size: 13.3333px; color: inherit; padding-left: 30px;"><span style="text-decoration: underline;"><strong><span style="font-family: arial, sans-serif;">CURSO 215/16:</span></strong></span></p>
<p style="font-size: 13.3333px; color: inherit;"> </p>
<ul style="font-size: 13.3333px; color: inherit;">
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_1_Cal.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Pendientes de 1º y 2º ESO.</span></a></strong></li>
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_2_Cal.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Pendientes de 3º ESO y 1º Bachillerato.</span></a></strong></li>
<li><strong><a href="archivos_ies/15_16/programaciones/RecMatematicas_3_materia.pdf" target="_blank"><span style="font-family: arial, sans-serif; font-size: 13.3333px; color: inherit; font-weight: normal;">Distribución de materia de alumnos pendientes de cursos anteriores.</span></a></strong></li>
</ul>
</div>
</td>
</tr>
</tbody>
</table>
<table class="borde_outset" style="text-align: center; color: #2a2a2a;" border="1">
<tbody>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_1_a_6.pdf" target="_blank" title="Pulsar para descargar." style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a>,</strong></span> Solamente los siguientes números por tema:</li>
<ul>
<li><strong>Tema 1:</strong> 1,11,12,13,19,21,28.</li>
<li><strong>Tema 2:</strong> 1,6,8,14,16,17,18,19,20,23.</li>
<li><strong>Tema 3:</strong> 6,10,16 (y ordenar de menor a mayor), 29,39,40,41,43.</li>
<li><strong>Tema 4:</strong> Todos.</li>
<li><strong>Tema 5:</strong> 1,2,5,8,9,10,11,12,13.</li>
<li><strong>Tema 6:</strong> 1,2,8,11,29,31,32.</li>
</ul>
</ul>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 18/01/2016</strong></span></p>
<p><strong>TEMA 1</strong>. Números naturales</p>
<p><strong>TEMA 2.</strong> Divisibilidad</p>
<p><strong>TEMA 3.</strong> Fracciones</p>
<p><strong>TEMA 4.</strong>  Números decimales</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 5.</strong> Números enteros</span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 6.</strong> Iniciación al álgebra</span></p>
<p><span style="font-family: Arial, sans-serif;"> </span></p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR.  </a></strong></span><a href="archivos_ies/15_16/pendientes/mat/Pendientes_1_ESO_Temas_7_a_11.pdf" target="_blank" style="color: #2f310d;">Solamente los siguientes números por tema:</a>
<ul>
<li><strong>Tema 7:</strong> 1,4,6,7,8,9,10,11,12,13.</li>
<li><strong>Tema 8:</strong> 2,3,6,7,9,11,12,13,14,15.</li>
<li><strong>Tema 9: TODOS.</strong></li>
<li><strong>Tema 10:</strong> 1,3,4,5,6,7,8,11,13,14.</li>
<li><strong>Tema 11:</strong> <strong>TODOS.</strong></li>
</ul>
</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 04/04/2016</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>TEMA 7.</strong> Sistema Métrico Decimal</span></p>
<p><strong>TEMA 8.</strong> Proporcionalidad numérica</p>
<p><strong>TEMA 9.</strong> Ángulos, circunferencias y círculos</p>
<p><strong>TEMA 10.</strong> Polígonos</p>
<p><strong>TEMA 11.</strong> Funciones y gráficas</p>
<p> </p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1</strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Naturales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Divisibilidad_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Decimales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1eso/Enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<a href="archivos_ies/15_16/pendientes/mat/1eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a><hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Sistema_metrico_decimal_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Proporcionalidad_pocentaje_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Recta_y_angulos_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1eso/Figuras_planas_y_espaciales_resueltos.pdf" target="_blank">UNIDAD 10</a><br /></span></p>
<a href="archivos_ies/15_16/pendientes/mat/1eso/Tablas_graficas_azar_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 11</span></a></td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 2º ESO</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Pendientes_2_ESO_Temas_1_a_6.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></strong></span><span style="font-size: 13px;"> Solamente los siguientes números por tema:</span></li>
<ul>
<li><strong>Tema 1:</strong> 1,6,9,10,12,16,17,18.</li>
<li><strong>Tema 2:</strong>  1,3,5,6,7, 11,13,15,16.</li>
<li><strong>Tema 3:</strong> 1,2,5,9,11,12,16,17,18,20.</li>
<li><strong>Tema 4:</strong>  1,5,8,11,12,13,16,19.</li>
<li><strong>Tema 5:</strong>  1,2,3,4,7,8,9, 10,11,12.</li>
<li></li>
</ul>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 21/01/2016</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números enteros</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>2.</strong> Fracciones</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>3.</strong> Números decimales</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>4.</strong> Sistema sexagesimal</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>5.</strong> Expresiones algebráicas</p>
<p> </p>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/14_15/mate/pendientes_2_eso_parte_2c.pdf" target="_blank" style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> (Actualizado 13/10/15)</strong></span></li>
<li><strong>Y los del Tema 6:</strong>  1,6,7,8,9,13,14,15.</li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 07/04/2016</strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>6.</strong><span style="font-weight: normal;"> Ecu</span><span style="color: inherit; font-family: inherit; font-size: inherit;">aciones de primer y segundo grado</span></strong></span></p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>7.</strong> Sistemas de ecuaciones</span></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>8.</strong> Proporcionalidad numérica</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>9.</strong> Proporcionalidad geométrica</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>10.</strong> Figuras planas. Áreas</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;"> </strong></p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Divisibilidad_enteros_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Fracciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 2</span></a></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Sist_numeracion_decimal_sis_sexa_resueltos.pdf" target="_blank">UNIDADES 3 Y 4</a></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/2eso/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<br /><hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-decoration: underline;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span></a></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Sistemas_ecuaciones_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Proporcionalidad_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/2eso/Pitagoras_semejanza_resueltos.pdf" target="_blank">UNIDAD 9 Y 10</a></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 3º ESO</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a></span></span><span style="color: #2f310d;"> extraídos del siguiente DOCUMENTO COMPLETO.</span><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_1_a_5.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></span></strong></a></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong style="color: #222222; font-family: arial,sans-serif; font-size: inherit; text-align: left; text-decoration: underline;">PRIMER EXAMEN: 18/01/2016</strong></span></p>
<p><strong>UNIDAD 1.</strong> Números racionales</p>
<p><span style="font-size: inherit;"><strong>UNIDAD 2.</strong> Números reales</span></p>
<p><strong>UNIDAD 3.</strong> Polinomios</p>
<p><strong>UNIDAD 4.</strong> Ecuaciones de primer y segundo grado </p>
<p><strong>UNIDAD 5.</strong> Sistemas de ecuaciones</p>
<p><strong> </strong></p>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/seleccion_ejericicios_pendientes_3eso.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a> </span></span></strong><strong><span style="color: #2f310d;">extraídos del siguiente DOCUMENTO COMPLETO.</span></strong><br /><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><br /></span></span></strong></li>
<li><a href="archivos_ies/14_15/mate/Pendientes_3_ESO_Temas_6_a_10.pdf" target="_blank"><strong><span style="text-decoration: underline;"><span style="color: #2f310d;">DESCARGAR DOCUMENTO COMPLETO.</span></span></strong></a></li>
<li></li>
</ul>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SEGUNDO EXAMEN: 04/04/2016</strong></span></p>
<p><strong>UNIDAD 6.</strong> Proporcionalidad numérica</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 7.</strong> Progresiones</span></p>
<p><strong>UNIDAD 8.</strong> Lugares geométricos. Figuras planas</p>
<p><strong>UNIDAD 9.</strong> Cuerpos geométricos .</p>
<p><strong> </strong></p>
</td>
<td>
<p style="text-align: center;"> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/Racionales_reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDADES 1 Y 2</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/polinomios_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/sistemas_de_ecuaciones_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<hr />
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/proporcionalidad_resueltos.pdf" target="_blank"><span style="text-align: left; color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 6</span> </a></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Progresiones_resueltos.pdf" target="_blank">UNIDAD: 7</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Problemas_metricos_plano_resueltos.pdf" target="_blank">UNIDAD: 8</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/3eso/Figuras_espacio_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º BACH. MAT. CCNN.</h4>
</td>
<td>
<ul>
<li><strong><span style="text-decoration: underline;"><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/1bach_cnat_ptes_examen1.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a><br /></span></span></strong></li>
</ul>
<p style="text-align: center;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;"><span style="text-decoration: underline;">PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM</span><br /></strong></p>
<p><strong>UNIDAD 1:</strong> Números Reales</p>
<p><strong>UNIDAD 2:</strong> Ecuaciones, inecuaciones  y sistemas</p>
<p><strong>UNIDAD 3:</strong> Trigonometría</p>
<p><strong>UNIDAD 4:</strong> Números Complejos</p>
<p><strong>UNIDAD 5:</strong> Geometría analítica</p>
<p><strong>UNIDAD 6: </strong>Cónicas</p>
</td>
<td>
<ul>
<li><strong><span style="color: #2f310d;"><a href="archivos_ies/14_15/mate/1bach_cnat_ptes_examen2.pdf" target="_blank">EJERCICIOS PARA RESOLVER Y ENTREGAR</a><br /></span></strong></li>
</ul>
<p style="text-align: center;"><strong><span style="text-decoration: underline;">SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM</span><br /></strong></p>
<p><strong>UNIDAD 7:</strong> Funciones</p>
<p><strong>UNIDAD 8:</strong> Funciones elementales</p>
<p><strong>UNIDAD 9:</strong> Límite de una función.</p>
<p><strong>UNIDAD 10:</strong> Derivada de una función</p>
<p><strong> </strong></p>
</td>
<td style="text-align: center;">
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 1</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Algebra_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD: 2</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Trigonometria_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 3</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Complejos_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 4</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 5</span></a></p>
<p><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Geometria_analitica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 6<br /></span></a></p>
<hr />
<p><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Funciones_elementales_resueltos.pdf" target="_blank">UNIDADES: 7 Y 8</a></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Limites_continuidad_resueltos.pdf" target="_blank">UNIDAD 9</a><br /></span></p>
<p><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cn/Derivadas_resueltos.pdf" target="_blank">UNIDAD 10</a></span></p>
</td>
</tr>
<tr>
<td>
<h4 style="text-align: center;">PENDIENTES DE 1º BACH. MAT. CCSS.</h4>
</td>
<td>
<ul>
<li><span style="text-decoration: underline;"><strong><a href="archivos_ies/15_16/pendientes/mat/Primer_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></span></li>
</ul>
<p style="text-align: center;"><strong style="color: #222222; font-family: arial, sans-serif; font-size: inherit; text-align: left;"><span style="text-decoration: underline;">PRIMER EXAMEN: 20/01/2016 - 11.15 horas en el SUM</span></strong></p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD </strong><strong>1:</strong> Números Reales</p>
<p><strong style="color: inherit; font-family: inherit; font-size: inherit;">UNIDAD 2</strong><strong>:</strong> Aritmética Mercantil</p>
<p><span style="color: inherit; font-family: inherit; font-size: inherit;"><strong>UNIDAD 3:</strong> Polinomios y fracciones algebraicas</span></p>
<p><strong>UNIDAD 4:</strong> Ecuaciones, inecuaciones y sistemas</p>
<p class="MsoNormal" style="page-break-before: always;"> </p>
</td>
<td>
<ul>
<li><strong><a href="archivos_ies/15_16/pendientes/mat/Segundo_examen_pendientes_1Bach_CCSSI.pdf" target="_blank"><span style="color: #2f310d;">EJERCICIOS PARA RESOLVER Y ENTREGAR</span></a></strong></li>
</ul>
<p style="text-align: center;"><strong><span style="text-decoration: underline;">SEGUNDO EXAMEN: 06/04/2016 - 11.15 horas en el SUM</span></strong></p>
<p style="text-align: center;"> </p>
<p><strong>UNIDAD 5:</strong> Funciones</p>
<p><strong>UNIDAD 6:</strong> <span style="color: inherit; font-family: inherit; font-size: inherit;">Funciones elementales</span></p>
<p><strong>UNIDAD 7:</strong> Límite de una función</p>
<p><strong>UNIDAD 8:</strong> Derivada de una función</p>
<p><strong>UNIDAD 9:</strong> Estadística unidimensional</p>
</td>
<td>
<p> <span style="text-decoration: underline;"><strong>EJERCICIOS RESUELTOS PARA PRACTICAR:</strong></span></p>
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 1<br /></strong></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Reales_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 1</span></a></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Aritmetica_mercantil_resueltos.pdf" target="_blank"><span style="text-align: left;"><span style="text-align: left;">UNIDAD 2</span></span></a></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 3</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Algebra_resueltos.pdf" target="_blank">UNIDAD 4</a><br /></span></p>
<hr />
<p style="text-align: center;"><span style="text-decoration: underline;"><strong>EXAMEN 2<br /></strong></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Funciones_elementales_resueltos.pdf" target="_blank">UNIDAD 5</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Funciones_tri_exp_log_resueltos.pdf" target="_blank">UNIDAD 6</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Limites_continuidad_ramas_resueltos.pdf" target="_blank">UNIDAD 7</a><br /></span></p>
<p style="text-align: center;"><span style="text-align: left;"><a href="archivos_ies/15_16/pendientes/mat/1bach_cs/Derivadas_resueltos.pdf" target="_blank">UNIDAD 8</a><br /></span></p>
<p style="text-align: center;"><a href="archivos_ies/15_16/matematicas/Estadistica_resueltos.pdf" target="_blank"><span style="text-align: left;">UNIDAD 9</span></a></p>
</td>
</tr>
</tbody>
</table>
</div>
<div style="text-align: justify; background-color: #e7eaad; float: left; width: 793.796875px;"><strong style="color: #414141;"><span style="color: #2a2a2a; font-weight: normal;"> </span></strong></div></div>
</div>
<div class="cleared"></div>
<div class="art-postmetadatafooter">
<div class="art-postfootericons art-metadata-icons">
<span class="art-postcategoryicon">Categoría: <span class="art-post-metadata-category-parent">Matemáticas</span> / <span class="art-post-metadata-category-name">Matemáticas</span></span>
</div>
</div>
</div>

		<div class="cleared"></div>
    </div>
</div>
</div>";s:4:"head";a:10:{s:5:"title";s:68:"IES MIGUEL DE CERVANTES - GRANADA. CURSO 2014/15 - PROYECTOS DEL IES";s:11:"description";s:95:"Instituto de ESO, Bachillerato y Ciclo superior de Granada ubicado en el barrio de Bola de Oro.";s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:9:"text/html";}s:8:"standard";a:3:{s:8:"keywords";N;s:6:"rights";N;s:6:"author";s:14:"Miguel Anguita";}}s:5:"links";a:0:{}s:11:"styleSheets";a:1:{s:27:"/media/system/css/modal.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:5:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/system/js/caption.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:33:"/media/system/js/mootools-more.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:25:"/media/system/js/modal.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:1:{s:15:"text/javascript";s:219:"window.addEvent('load', function() {
				new JCaption('img.caption');
			});
		window.addEvent('domready', function() {

			SqueezeBox.initialize({});
			SqueezeBox.assign($$('a.modal'), {
				parse: 'rel'
			});
		});";}s:6:"custom";a:0:{}}s:7:"pathway";a:0:{}s:6:"module";a:0:{}}
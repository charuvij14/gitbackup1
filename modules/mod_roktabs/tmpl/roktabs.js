/**
 * RokTabs Module
 *
 * @package		Joomla
 * @subpackage	RokTabs Module
 * @copyright Copyright (C) 2009 RocketTheme. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see RT-LICENSE.php
 * @author RocketTheme, LLC
 *
 */

var RokTabsOptions = {
	'mouseevent': [], 'duration': [], 'transition': [], 'auto': [], 'delay': [], 
	'type': [], 'arrows': [], 'tabsScroll': [], 'linksMargins': [], 'navscroll': [],
	'marginCompensation': []
};


var RokTabsOptions = {
    'mouseevent': [],
    'duration': [],
    'transition': [],
    'auto': [],
    'delay': [],
    'type': [],
    'arrows': [],
    'tabsScroll': [],
    'linksMargins': [],
    'navscroll': [],
    'marginCompensation': []
};
var RokTabs = new Class({
    version: '1.8 (mt1.2)',
    Implements: [Options, Events],
    options: {
        'scroll': RokTabsOptions
    },
    initialize: function (b) {
        this.setOptions({
            'scroll': RokTabsOptions
        });
        if (!this.options.scroll.navscroll || !this.options.scroll.navscroll.length) {
            this.options.scroll.navscroll = [];
            (RokTabsOptions['duration'].length).times(function () {
                this.options.scroll.navscroll.push(true)
            }.bind(this))
        }
        this.containers = $$('.roktabs-container-inner');
        this.tabsWrapper = $$('.roktabs-links');
        this.tabs = $$('.roktabs-links ul');
        this.panels = $$('.roktabs-container-wrapper');
        this.outer = $$(this.tabsWrapper.getParent());
        this.wrapper = $$(this.outer.getParent());
        this.fx = [];
        this.current = [];
        this.timer = [];
        this.tabsSize = [];
        this.tabScroll = [];
        this.panels.each(function (a, i) {
            this.current[i] = 0;
            if (!this.options.scroll.mouseevent[i]) this.options.scroll.mouseevent[i] = 'click';
            a.setStyle('width', (Browser.opera) ? 30000 : 50000);
            if (Browser.ie) {
                a.setStyles({
                    'filter': 'inherit',
                    'position': 'relative'
                }).getChildren().setStyles({
                    'filter': 'inherit',
                    'position': 'relative'
                })
            }
        }, this);
        this.attachEvents()
    },
    attachEvents: function () {
        var g, self = this;
        this.tabs.each(function (c, i) {
            if (!this.options.scroll.navscroll[i]) this.tabsWrapper.addClass('roktabs-links-noscroll');
            this.outer[i].addEvents({
                'mouseenter': function () {
                    if (self.options['scroll'].auto[i]) self.stop(i)
                },
                'mouseleave': function () {
                    if (self.options['scroll'].auto[i]) self.start(i)
                }
            });
            this.fx[i] = new Fx.Scroll(this.panels[i].getParent(), {
                link: 'cancel',
                wheelStops: false,
                duration: this.options['scroll'].duration[i],
                transition: this.options['scroll'].transition[i]
            });
            this.fx[i].set(0, false);
            g = 0;
            this.containers[i].setStyle('width', this.wrapper[i].getStyle('width').toInt() - this.tabsWrapper[i].getParent().getStyle('border-left-width').toInt() - this.tabsWrapper[i].getParent().getStyle('border-right-width').toInt());
            c.getElements('li').each(function (a, j) {
                var b = this.panels[i].getChildren()[j];
                b.setStyle('width', ((Browser.ie4) ? this.wrapper[i] : this.outer[i]).getStyle('width').toInt() - b.getStyle('padding-left').toInt() - b.getStyle('padding-left').toInt() - b.getStyle('margin-left').toInt() - b.getStyle('margin-left').toInt());
                g += a.getSize().x;
                if (this.options['scroll'].marginCompensation[i]) {
                    g += a.getStyle('margin-left').toInt() + a.getStyle('margin-right').toInt()
                }
                a.setStyle('cursor', 'pointer').addEvents({
                    'mouseenter': this.mouseenter.bind(this, [a, b, i, j]),
                    'mouseleave': this.mouseleave.bind(this, [a, b, i, j]),
                    'mousedown': this.mousedown.bind(this, [a, b, i, j]),
                    'mouseup': this.mouseup.bind(this, [a, b, i, j])
                })
            }, this);
            this.tabsSize[i] = [c.getSize().x, g];
            var d = this.outer[i].getElement('.roktabs-arrows');
            if (this.options['scroll'].arrows[i]) {
                var e = d.getElement('.previous');
                var f = d.getElement('.next')
            };
            if (this.options['scroll'].auto[i]) {
                this.start(i)
            };
            if (this.tabsSize[i][1] > this.tabsSize[i][0] && this.options.scroll.navscroll[i]) this.tabScroller(i)
        }, this);
        return this
    },
    mouseenter: function (a, b, c, d) {
        if (a[0]) {
            d = a[3];
            c = a[2];
            b = a[1];
            a = a[0]
        };
        a.addClass('hover').addClass('over');
        this.fireEvent('mouseenter', [a, b, c, d]);
        if (RokTabsOptions.mouseevent[c] == 'mouseenter') {
            this.mousedown(a, b, c, d, true);
            this.mouseup(a, b, c, d, true)
        }
    },
    mouseleave: function (a, b, c, d) {
        if (a[0]) {
            d = a[3];
            c = a[2];
            b = a[1];
            a = a[0]
        };
        a.removeClass('hover').removeClass('over').removeClass('down').removeClass('up');
        this.fireEvent('mouseleave', [a, b, c, d]);
        if (RokTabsOptions.mouseevent[c] == 'mouseenter') this.mouseup(a, b, c, d, true)
    },
    mousedown: function (a, b, c, d, e) {
        if (a[0]) {
            e = a[4];
            d = a[3];
            c = a[2];
            b = a[1];
            a = a[0]
        };
        a.removeClass('up').addClass('down');
        if (this.options['scroll'].type[c] == 'scrolling') {
            this.fx[c].options.duration = RokTabsOptions.duration[c];
            this.fx[c].options.link = 'cancel';
            this.fx[c].toElement(b)
        } else {
            var f = this;
            var g = new Fx.Tween(this.fx[c].element, {
                duration: RokTabsOptions.duration[c] / 2,
                link: 'cancel',
                onComplete: function () {
                    if (!this.to[0].value) {
                        f.fx[c].options.duration = 0;
                        f.fx[c].toElement(b);
                        g.start('opacity', 1)
                    }
                }
            });
            g.cancel().start('opacity', 0)
        };
        this.fireEvent('mousedown', [a, b, c, d])
    },
    mouseup: function (a, b, c, d, e) {
        if (a[0]) {
            e = a[4];
            d = a[3];
            c = a[2];
            b = a[1];
            a = a[0]
        };
        if (RokTabsOptions.mouseevent[c] != 'click' && !e) return;
        this.tabs[c].getElements('li').removeClass('active');
        a.removeClass('down').addClass('up').addClass('active');
        this.current[c] = d;
        this.fireEvent('mouseup', [a, b, c, d])
    },
    click: function (a, b, c, d, e) {
        if (a[0]) {
            e = a[4];
            d = a[3];
            c = a[2];
            b = a[1];
            a = a[0]
        };
        return a.fireEvent('mousedown', [a, b, c, d], e).fireEvent('mouseup', [a, b, c, d]).fireEvent('mouseleave', [a, b, c, d])
    },
    start: function (a) {
        clearTimeout(this.timer[a]);
        var b = this.next.bind(this, a);
        this.timer[a] = b.periodical(this.options.scroll.delay[a])
    },
    stop: function (a) {
        clearTimeout(this.timer[a])
    },
    next: function (a) {
        var b = this.tabs.getElements('li');
        var c = this.current[a] + 1,
            next = b[a][c],
            tab;
        if (next) tab = next;
        else {
            tab = b[a][0];
            c = 0
        };
        return this.click(tab, this.panels[a], a, c)
    },
    previous: function (a) {
        var b = this.tabs.getElements('li');
        var c = this.current[a] - 1,
            prev = b[a][c],
            tab;
        if (prev) tab = prev;
        else {
            tab = b[a][b.length];
            c = b.length
        };
        return this.click(tab, this.panels[a], a, c)
    },
    goTo: function (a, b) {
        var c = this.tabs.getElements('li');
        var d = c[a][b],
            tab;
        if (d) tab = d;
        else {
            tab = c[a][0];
            current = 0
        };
        var e = this.panels[a].getChildren()[b];
        if (this.options.scroll.mouseevent[a] == 'mouseenter') this.mouseenter(tab, e, a, d, true);
        return this.click(tab, e, a, d, true)
    },
    tabView: function (a, b) {
        if (b == 'hide') this.tabs[a].setStyle('display', 'none');
        else this.tabs[a].setStyle('display', '')
    },
    tabPosition: function (a, b) {
        var c = this.tabsWrapper[a];
        switch (b) {
        case 'top':
            c.inject(c.getParent(), 'top');
            c.getFirst().removeProperty('class').addClass('roktabs-top');
            break;
        case 'bottom':
        default:
            c.inject(c.getParent());
            c.getFirst().removeProperty('class').addClass('roktabs-bottom')
        }
    },
    tabScroller: function (b) {
        var c = this.tabs[b],
            self = this;
        var d = c.getParent();
        (2).times(function () {
            self.tabsSize[b][1] = 0;
            c.getChildren().each(function (a) {
                if (window.ie) a.getFirst().inject(a);
                self.tabsSize[b][1] += a.getSize().x + a.getStyle('margin-left').toInt() + a.getStyle('margin-right').toInt() + a.getStyle('padding-left').toInt() + a.getStyle('padding-right').toInt() + a.getStyle('border-left-width').toInt() + a.getStyle('border-right-width').toInt()
            }, this);
            c.setStyle('width', self.tabsSize[b][1] + ((Browser.firefox) ? 5 : (Browser.ie6) ? 0.5 : 0))
        }.bind(this));
        d.setStyles({
            'overflow': 'hidden',
            'width': this.tabsSize[b][0],
            'position': 'relative'
        });
        if (c.getSize().x > d.getSize().x) {
            var e = new Element('div', {
                'class': 'active-arrows'
            }).setStyle('position', 'relative').inject(d, 'before').adopt(d);
            var f = new Element('div', {
                'class': 'arrow-prev png'
            }).set('html', '<span><</span>').inject(e, 'top');
            var g = new Element('div', {
                'class': 'arrow-next png'
            }).set('html', '<span>></span>').inject(e);
            var h = {
                'prev': f.getStyle('width').toInt() + f.getStyle('margin-left').toInt() + f.getStyle('margin-right').toInt() + f.getStyle('border-left').toInt() + f.getStyle('border-right').toInt() + f.getStyle('padding-left').toInt() + f.getStyle('padding-right').toInt(),
                'next': g.getStyle('width').toInt() + g.getStyle('margin-left').toInt() + g.getStyle('margin-right').toInt() + g.getStyle('border-left').toInt() + g.getStyle('border-right').toInt() + g.getStyle('padding-left').toInt() + g.getStyle('padding-right').toInt()
            };
            var i = 0;
            if (this.options.scroll.linksMargins[b]) i = d.getStyle('margin-right').toInt();
            if (i < 0) i = Math.abs(i) / 2;
            d.setStyle('width', this.tabsSize[b][0] - i - h.prev - h.next);
            new Element('div', {
                'class': 'clear'
            }).inject(e);
            this.tabScroll[b] = {
                'speed': 70,
                'amount': 30,
                'current': 0
            };
            var j;
            g.addEvents({
                'mouseenter': function () {
                    clearTimeout(j);
                    this.addClass('arrow-next-hover');
                    j = self.tabScrollerAnim.periodical(self.tabScroll[b]['speed'], self, [b, d, true])
                },
                'mouseleave': function () {
                    this.removeClass('arrow-next-hover');
                    clearTimeout(j)
                }
            });
            f.addEvents({
                'mouseenter': function () {
                    clearTimeout(j);
                    this.addClass('arrow-prev-hover');
                    j = self.tabScrollerAnim.periodical(self.tabScroll[b]['speed'], self, [b, d, false])
                },
                'mouseleave': function () {
                    this.removeClass('arrow-prev-hover');
                    clearTimeout(j)
                }
            })
        }
    },
    tabScrollerAnim: function (a, b, c) {
        var d = b.getScrollSize().x,
            scrollAmount = b.getScroll().x;
        var e;
        if (c) e = scrollAmount + this.tabScroll[a]['amount'];
        else e = scrollAmount - this.tabScroll[a]['amount'];
        e = (e < 0) ? 0 : (e >= d) ? d : e;
        b.scrollTo(e, 0)
    }
});
var roktabs;
window.addEvent('load', function () {
    roktabs = new RokTabs()
})
<?php
/**
*Jw Player Module : mod_playerjr
* @version mod_playerjr$Id$
* @package mod_playerjr
* @subpackage default.php
* @author joomlarulez.
* @copyright (C) www.joomlarulez.com
* @license Limited  http://www.gnu.org/licenses/gpl.html
* @final 3.0.0
*/

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

//Check if the user have player file
if ($is_jwplayer_playlist_file != '') {

// Construct the Global parameter for the playlist
echo 
"<div align=\"center\">";
	// set JS
	$document->addScript( $jwplayeradvanced_pathway."player/5/jwplayer.js");
	// By pass the adsolution div id or not
	if ($is_jwplayer_playlist_adsenabled == true) {	
	echo
	"
		<div class='ltas-ad' id='mediaspacemod".$jwplayerclasspl_sfx."'>
			<div id='jwplayer".$jwplayerclasspl_sfx."'>".$is_jwplayer_flash."</div>
		</div>";
	} else {
	echo
	"
		<div id='jwplayer".$jwplayerclasspl_sfx."'>".$is_jwplayer_flash."</div>";
	}
	// Load JWEmbedderConfig framework
	include (JPATH_BASE.DS.'modules'.DS.'mod_playerjr'.DS.'includes'.DS.'framework'.DS.'JWEmbedderConfig.php');
	echo 
	"
</div>";
// Valid or not the joomlarulezlink
if ($is_jwplayer_playlist_joomlarulezlink == '1') {			
echo 
"
<div align=\"center\">
	Powered By: <a class=\"blank\" href=\"http://www.joomlarulez.com\" target=\"_blank\">http://www.joomlarulez.com</a>
</div>
";
}
}
<?php
/**
*Jw Player Module : mod_playerjr
* @version mod_playerjr$Id$
* @package mod_playerjr
* @subpackage mod_playerjr.php
* @author joomlarulez.
* @copyright (C) www.joomlarulez.com
* @license Limited  http://www.gnu.org/licenses/gpl.html
* @final 3.0.0
*/

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

// re define DS constant for J3.0 +
if (!(defined('DS'))) {
    define( 'DS', DIRECTORY_SEPARATOR );
}

// Include the syndicate functions only once
require_once( dirname(__FILE__).DS.'helper.php' );

$params = modplayerjr_Helper::getParams( $params );

//Joomla version
$jversion = new JVersion;
$jversion = $jversion->RELEASE;

// Module Parameters
$document = &JFactory::getDocument();

//get pathway
$jwplayer_pathway  = JURI::root()."modules/mod_playerjr/";
$jwplayer_pathway_DS = JPATH_BASE.DS.'modules'.DS.'mod_playerjr'.DS;

$jwplayeradvanced_pathway  = JURI::root()."media/jwadvanced/";
$jwplayeradvanced_pathway_DS  = JPATH_BASE.DS.'media'.DS.'jwadvanced'.DS;
	

// Module ID
$jwplayerclasspl_sfx = $module->id;

// Flash Install
$moduleflash_sfx = $params->get('PlaylistFlashinstall');
if ($moduleflash_sfx == '1') {
	$is_jwplayer_flash = "You must have <a href=\"http://get.adobe.com/flashplayer\">the Adobe Flash Player</a> installed to view this player.";
} else {
	$is_jwplayer_flash = "";
}

// link Parameters
$is_jwplayer_playlist_joomlarulezlink = $params->get('Playlistjoomlarulezlink');

// set array flashvars
$is_jwplayer_flashvars = array();

//set player
$is_jwplayer_flashvars['flashplayer'] = $jwplayeradvanced_pathway."player/5/player.swf";

// Playlist url
$is_jwplayer_playlist_file = $params->get('mod_plfile');

//set source mode
$is_jwplayer_playlist_select = $params->get('mod_plselect');

if($is_jwplayer_playlist_select == '1') {
	$is_jwplayer_playlisttype = "file";
} else if ($is_jwplayer_playlist_select == '0') {
	$is_jwplayer_playlisttype = "playlistfile";
	// Replace youtube playlist with gdata youtube playlist
	$is_jwplayer_playlist_file = str_replace("http://www.youtube.com/playlist?list=PL", "http://gdata.youtube.com/feeds/api/playlists/", $is_jwplayer_playlist_file);
} else {
	$is_jwplayer_playlisttype = "file";
}

//set final source
$is_jwplayer_flashvars[$is_jwplayer_playlisttype] = $is_jwplayer_playlist_file;

//set  flashwars with no default mode
$count = 5;
$index1 = 'height';
$index_defaut1 = '240';
$index2 = 'width';
$index_defaut2 = '320';
$index3 = 'wmode';
$index_defaut3 = 'opaque';
$index4 = 'playlistsize';
$index_defaut4 = '180';
for ($i = 1; $i < $count; $i++) {
	$is_jwplayer_flashvars[${'index'.$i}] = $params->get("Playlist".${'index'.$i}, ${'index_defaut'.$i});
}

//set  simple flaswars
$count = 19;
$index1 = 'playlist';
$index_defaut1 = 'none';
$index2 = 'controlbar';
$index_defaut2 = 'over';
$index3 = 'bandwidth';
$index_defaut3 = '5000';
$index4 = 'bufferlength';
$index_defaut4 = '1';
$index5 = 'repeat';
$index_defaut5 = 'none';
$index6 = 'stretching';
$index_defaut6 = 'uniform';
$index7 = 'volume';
$index_defaut7 = '90';
$index8 = 'backcolor';
$index_defaut8 = '';
$index9 = 'frontcolor';
$index_defaut9 = '';
$index10 = 'lightcolor';
$index_defaut10 = '';
$index11 = 'screencolor';
$index_defaut11 = '';
$index12 = 'streamer';
$index_defaut12 = '';
$index13 = 'start';
$index_defaut13 = '0';
$index14 = 'image';
$index_defaut14 = '';
//set  true false flaswars
$index15 = 'autostart';
$index_defaut15 = '0';
$index16 = 'shuffle';
$index_defaut16 = '0';
$index17 = 'smoothing';
$index_defaut17 = '1';
$index18 = 'icons';
$index_defaut18 = '1';
for ($i = 1; $i < $count; $i++) {
	$is_jwplayer_playlist_var = $params->get("Playlist".${'index'.$i}, ${'index_defaut'.$i});
	if ($is_jwplayer_playlist_var != '' && $is_jwplayer_playlist_var != ${'index_defaut'.$i}) {
		if ($i > 14) {
			$is_jwplayer_flashvars[${'index'.$i}] = $is_jwplayer_playlist_var ? 'true' : 'false' ;
		} else {
			$is_jwplayer_flashvars[${'index'.$i}] = $is_jwplayer_playlist_var;
		}
	}
}

// plugin set
$is_jwplayer_plugin_array = array();

// Plugin adsolution
$is_jwplayer_playlist_adschannelcode = $params->get('AdsolutionChannelcode');
$is_jwplayer_playlist_adsenabled = $params->get('AdsolutionPluginEnabled');
if($is_jwplayer_playlist_adsenabled == '1' && $is_jwplayer_playlist_adschannelcode != '') {
	$is_jwplayer_playlist_adsenabled = true;
	$is_jwplayer_plugin_array[] = 'ltas';
	$is_jwplayer_flashvars["ltas.cc"] = $is_jwplayer_playlist_adschannelcode;
}

require( JModuleHelper::getLayoutPath( 'mod_playerjr' ) );
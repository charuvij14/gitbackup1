<?php
/**
*Jw Player Module : mod_playerjr
* @version mod_playerjr$Id$
* @package mod_playerjr
* @subpackage install.script.php
* @author joomlarulez.
* @copyright (C) www.joomlarulez.com
* @license Limited  http://www.gnu.org/licenses/gpl.html
* @final 3.0.0
*/

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

class Mod_PlayerjrInstallerScript {
	function postflight($type, $parent) {
	// Rename manifest file
	$path = $parent->getParent()->getPath('extension_root');
	if (JFile::exists($path."/mod_playerjr.j25.xml")) {
			if ( JFile::exists($path."/mod_playerjr.xml")) {
				JFile::delete($path."/mod_playerjr.xml");
			}
			JFile::move($path."/mod_playerjr.j25.xml", $path."/mod_playerjr.xml");
		}
	}
}
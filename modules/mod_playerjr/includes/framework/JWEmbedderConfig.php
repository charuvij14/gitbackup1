<?php
/**
*Jw Player Module Advanced : mod_playerjr
* @version mod_playerjr$Id$
* @package mod_playerjr
* @subpackage JWEmbedderConfig.php
* @author joomlarulez.
* @copyright (C) www.joomlarulez.com
* @license Limited  http://www.gnu.org/licenses/gpl.html
* @final 3.0.0
*/

/** ensure this file is being included by a parent file */
defined( '_JEXEC' ) or die( 'Direct Access to this location is not allowed.' );

			echo
			"
			<script type='text/javascript'>
				jwplayer('jwplayer".$jwplayerclasspl_sfx."').setup({";
					//set flashvars
					$i = 1;
					while (list($key, $value) = each($is_jwplayer_flashvars)) {
					if ($i > 1)
					{
					echo
					",";
					}
					echo
					"
					'".$key."': '".$value."'";
					$i++;
					}
					reset($is_jwplayer_flashvars);
					//load plugin for html5 5.6
					if (!(empty($is_jwplayer_plugin_array))) {
					$i = 1;
					echo
					",
					'plugins': {";
					while (list($key, $value) = each($is_jwplayer_plugin_array)) {
					if ($i > 1)
					{
					echo
					",";
					}
					echo
					"
						'".$value."' : {}";
					$i++;
					}
					echo
					"	
					}";
					reset($is_jwplayer_plugin_array);
					}
			echo
			"
				});
			</script>
			";
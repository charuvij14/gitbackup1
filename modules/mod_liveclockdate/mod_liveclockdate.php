<?
/**
* LIVE CLOCK DATE - Joomla Module
* Version			: 2.2
* Created by		: RBO Team - RumahBelanja.com
* Created on		: December 2008 (for Joomla 1.5.x) - Dec 16th, 2010 (for Joomla 1.6.x) - Dec 1st, 2011 (for Joomla 1.7.x)
* Updated on		: Dec 01st, 2011
* Package			: Joomla 1.7.x
* License			: http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/
// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__).'/helper.php');

//$list = modLiveClockDateHelper::getList($params);
require(JModuleHelper::getLayoutPath('mod_liveclockdate'));
?>
<?php
/**
* LIVE CLOCK DATE - Joomla Module
* Version			: 2.2
* Created by		: RBO Team - RumahBelanja.com
* Created on		: December 2008 (for Joomla 1.5.x) - Dec 16th, 2010 (for Joomla 1.6.x) - Dec 1st, 2011 (for Joomla 1.7.x)
* Updated on		: Dec 01st, 2011
* Package			: Joomla 1.7.x
* License			: http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
*/
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');


$width			= $params->get( 'width', 300);
$font_face		= $params->get( 'font_face', 'tohama, verdana, arial' );
$font_size		= $params->get( 'font_size', 10 );
$font_color		= $params->get( 'font_color', '#000000' );
$back_color		= $params->get( 'back_color', '#FFFFFF' );
//$pre_text		= $params->get( 'pre_text', 'The time is: ' );
//$sep_text		= $params->get( 'sep_text', 'on' );
$hoursystem		= $params->get( 'hoursystem', 1 );
$clockupdate	= $params->get( 'clockupdate', 1  );
$displaydate	= $params->get( 'displaydate', 1 );
$styledate		= $params->get( 'styledate', 1 );
?>
<script language="javascript">
	var myfont_face = "<?php echo $font_face; ?>"; 
	var myfont_size = "<?php echo $font_size; ?>";
	var myfont_color = "<?php echo $font_color; ?>";
	var myback_color = "<?php echo $back_color; ?>";
	var mypre_text = "<?php echo JText::_('THETIMEIS'); ?>";
	var mysep_text = "<?php echo JText::_('MOD_LCD_ON'); ?>";
	var mywidth = <?php echo $width; ?>;
	var my12_hour = <?php echo $hoursystem; ?>;
	var myupdate = <?php echo $clockupdate; ?>;
	var DisplayDate = <?php echo $displaydate; ?>;
	var StyleDate = <?php echo $styledate; ?>;

// Browser detect code
        var ie4=document.all
        var ns4=document.layers
        var ns6=document.getElementById&&!document.all

// Global varibale definitions:
	var dn = "";
	var mn = "<?php echo JText::_('MOD_LCD_TH'); ?>";
	var old = "";

// The following arrays contain data which is used in the clock's
	var DaysOfWeek = new Array(7);
		DaysOfWeek[0] = "<?php echo JText::_( 'SUNDAY' );?>";
		DaysOfWeek[1] = "<?php echo JText::_( 'MONDAY' );?>";
		DaysOfWeek[2] = "<?php echo JText::_( 'TUESDAY' );?>";
		DaysOfWeek[3] = "<?php echo JText::_( 'WEDNESDAY' );?>";
		DaysOfWeek[4] = "<?php echo JText::_( 'THURSDAY' );?>";
		DaysOfWeek[5] = "<?php echo JText::_( 'FRIDAY' );?>";
		DaysOfWeek[6] = "<?php echo JText::_( 'SATURDAY' );?>";

	var MonthsOfYear = new Array(12);
		MonthsOfYear[0] = "<?php echo JText::_( 'JANUARY' );?>";
		MonthsOfYear[1] = "<?php echo JText::_( 'FEBRUARY' );?>";
		MonthsOfYear[2] = "<?php echo JText::_( 'MARCH' );?>";
		MonthsOfYear[3] = "<?php echo JText::_( 'APRIL' );?>";
		MonthsOfYear[4] = "<?php echo JText::_( 'MAY' );?>";
		MonthsOfYear[5] = "<?php echo JText::_( 'JUNE' );?>";
		MonthsOfYear[6] = "<?php echo JText::_( 'JULY' );?>";
		MonthsOfYear[7] = "<?php echo JText::_( 'AUGUST' );?>";
		MonthsOfYear[8] = "<?php echo JText::_( 'SEPTEMBER' );?>";
		MonthsOfYear[9] = "<?php echo JText::_( 'OCTOBER' );?>";
		MonthsOfYear[10] = "<?php echo JText::_( 'NOVEMBER' );?>";
		MonthsOfYear[11] = "<?php echo JText::_( 'DECEMBER' );?>";

// This array controls how often the clock is updated,
// based on your selection in the configuration.
	var ClockUpdate = new Array(3);
		ClockUpdate[0] = 0;
		ClockUpdate[1] = 1000;
		ClockUpdate[2] = 60000;

// For Version 4+ browsers, write the appropriate HTML to the
// page for the clock, otherwise, attempt to write a static
// date to the page.
	if (ie4||ns6) { document.write('<span id="LiveClockIE" style="width:'+mywidth+'px; background-color:'+myback_color+'"></span>'); }
	else if (document.layers) { document.write('<ilayer bgColor="'+myback_color+'" id="ClockPosNS" visibility="hide"><layer width="'+mywidth+'" id="LiveClockNS"></layer></ilayer>'); }
	else { old = "true"; show_clock(); }

// The main part of the script:
	function show_clock() {
		if (old == "die") { return; }
	
	//show clock in NS 4
		if (ns4)
                document.ClockPosNS.visibility="show"
	// Get all our date variables:
		var Digital = new Date();
		var day = Digital.getDay();
		var mday = Digital.getDate();
		var month = Digital.getMonth();
		var hours = Digital.getHours();

		var minutes = Digital.getMinutes();
		var seconds = Digital.getSeconds();

	// Fix the "mn" variable if needed:
		if (mday == 1) { mn = "<?php echo JText::_( 'MOD_LCD_ST' );?>"; }
		else if (mday == 2) { mn = "<?php echo JText::_( 'MOD_LCD_ND' );?>"; }
		else if (mday == 3) { mn = "<?php echo JText::_( 'MOD_LCD_RD' );?>"; }
		else if (mday == 21) { mn = "<?php echo JText::_( 'MOD_LCD_ST' );?>"; }
		else if (mday == 22) { mn = "<?php echo JText::_( 'MOD_LCD_ND' );?>"; }
		else if (mday == 23) { mn = "<?php echo JText::_( 'MOD_LCD_RD' );?>"; }
		else if (mday == 31) { mn = "<?php echo JText::_( 'MOD_LCD_ST' );?>"; }

	// Set up the hours for either 24 or 12 hour display:
		if (my12_hour) {
			dn = "<?php echo JText::_( 'AM' );?>";
			if (hours > 12) { dn = "<?php echo JText::_( 'PM' );?>"; hours = hours - 12; }
			if (hours == 0) { hours = 12; }
		} else {
			dn = "";
		}
		if (minutes <= 9) { minutes = "0"+minutes; }
		if (seconds <= 9) { seconds = "0"+seconds; }

	// This is the actual HTML of the clock. If you're going to play around
	// with this, be careful to keep all your quotations in tact. edit by CoreTemplate.com
		//Time-Date
		if (!StyleDate) {
		myclock = '';
		myclock += '<font style="color:'+myfont_color+'; font-family:'+myfont_face+'; font-size:'+myfont_size+'pt;">';
		myclock += mypre_text+' ';
		myclock += hours+':'+minutes;
		if ((myupdate < 2) || (myupdate == 0)) { myclock += ':'+seconds; }
		myclock += ' '+dn;
		if (DisplayDate) { 
		myclock += ' '+mysep_text;
		myclock += ' '+DaysOfWeek[day]+', '+mday+mn+' '+MonthsOfYear[month]; }
		myclock += '</font>';
		}
		if (old == "true") {
			document.write(myclock);
			old = "die";
			return;
		}
		
		//Date-Time
		if (StyleDate) {
		myclock = '';
		myclock += '<font style="color:'+myfont_color+'; font-family:'+myfont_face+'; font-size:'+myfont_size+'pt;">';
		if (DisplayDate) { 
		//myclock += ' '+mysep_text;
		myclock += DaysOfWeek[day]+', '+mday+mn+' '+MonthsOfYear[month]; }
		myclock += ' - '+mypre_text;
		myclock += ' '+hours+':'+minutes;
		if ((myupdate < 2) || (myupdate == 0)) { myclock += ':'+seconds; }
		myclock += ' '+dn;
		myclock += '</font>';
		}
		if (old == "true") {
			document.write(myclock);
			old = "die";
			return;
		}
	//end edit by RBO Team
	// Write the clock to the layer:
		if (ns4) {
			clockpos = document.ClockPosNS;
			liveclock = clockpos.document.LiveClockNS;
			liveclock.document.write(myclock);
			liveclock.document.close();
		} else if (ie4) {
			LiveClockIE.innerHTML = myclock;
		} else if (ns6){
			document.getElementById("LiveClockIE").innerHTML = myclock;
                }            

	if (myupdate != 0) { setTimeout("show_clock()",ClockUpdate[myupdate]); }
}

</script>
<?php 

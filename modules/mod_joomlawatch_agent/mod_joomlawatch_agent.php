<?php


/**
 * JoomlaWatch - A real-time ajax joomla monitor and live stats
 * @version 1.2.x
 * @package JoomlaWatch
 * @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
 * @copyright (C) 2007 by Matej Koval - All rights reserved!
 * @website http://www.codegravity.com
 **/

/** ensure this file is being included by a parent file */

if( !defined( '_JEXEC' ) && !defined( '_VALID_MOS' ) ) die( 'Restricted access' );

require_once ("components" . DS . "com_joomlawatch" . DS . "config.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.block.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.block.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.cache.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.config.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.goal.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.goal.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.helper.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.stat.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.stat.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.visit.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.visit.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.trend.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.db.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.log.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.ip2country.php");


$redirURI = addslashes(@ $_SERVER[JOOMLAWATCH_SERVER_URI_KEY]);
$uri = addslashes(@ $_SERVER['REQUEST_URI']);

if (@ $redirURI && @ substr($redirURI, -9, 9) != "index.php")
    $uri = $redirURI;

$joomlaWatch = new JoomlaWatch();

require_once ("components" . DS . "com_joomlawatch" . DS . "lang" . DS . $joomlaWatch->config->getLanguage().".php");

if (JOOMLAWATCH_DEBUG) {
    echo ("<span style='color: #ff3333'>"._JW_DESC_DEBUG."</span><br/>");
}

$joomlaWatchHTML = new JoomlaWatchHTML();
$joomlaWatch->block->checkPostRequestForSpam($_POST);
$joomlaWatch->visit->insertVisit($joomlaWatch->config->getLiveSite());

/* 
 * The following piece of code identifies the userAgent and inserts the backlink to codegravity.com
 * 
 * I would really appreciate, that you would keep the this unchanged. 
 *
 * It took me some time to create and maintain this component and to share it with everyone. 
 *
 * This is the least you could kindly do. Thank you. 
 *
 *
 * Matej Koval
*/


$nofollow = "";
if ($joomlaWatch->config->isAdFree() && $joomlaWatch->config->getCheckboxValue("JOOMLAWATCH_FRONTEND_NOFOLLOW")) {
    $nofollow = "rel='nofollow'";
}

if ($joomlaWatch->config->isAdFree()) {
    $title = "";
} else {
    $title="Realtime website traffic tracker, online visitor stats and hit counter";
}

if (!($joomlaWatch->config->isAdFree() && $joomlaWatch->config->getCheckboxValue("JOOMLAWATCH_FRONTEND_NO_BACKLINK"))) {
    echo ("<a href='http://www.codegravity.com' target='_blank' $nofollow title='".$title."'>");
}

echo ("<img src='" . $joomlaWatch->config->getLiveSite() . "components/com_joomlawatch/img.php?rand=" . (rand() % 100000) . "' border='0' alt='$title' title='$title' />");

if (!($joomlaWatch->config->isAdFree() && !$joomlaWatch->config->getCheckboxValue("JOOMLAWATCH_FRONTEND_NO_BACKLINK"))) {
    echo ("</a>");
}
?>

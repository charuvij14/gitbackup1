<?php


/**
* JoomlaWatch - A real-time ajax joomla monitor and live stats
* @version 1.2.x
* @package JoomlaWatch
* @license http://www.gnu.org/licenses/gpl-3.0.txt 	GNU General Public License v3
* @copyright (C) 2007 by Matej Koval - All rights reserved! 
* @website http://www.codegravity.com
**/

/** ensure this file is being included by a parent file */
if( !defined( '_JEXEC' ) && !defined( '_VALID_MOS' ) ) die( 'Restricted access' ); 

require_once ("components" . DS . "com_joomlawatch" . DS . "config.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.block.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.block.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.cache.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.config.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.goal.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.goal.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.helper.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.stat.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.stat.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.visit.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.visit.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.trend.html.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.db.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.joomlawatch.log.php");
require_once ("components" . DS . "com_joomlawatch" . DS . "class.ip2country.php");


$redirURI = addslashes(@ $_SERVER[JOOMLAWATCH_SERVER_URI_KEY]);
$uri = addslashes(@ $_SERVER['REQUEST_URI']);

if (@ $redirURI && @ substr($redirURI, -9, 9) != "index.php")
	$uri = $redirURI;

$joomlaWatch = new JoomlaWatch();
require_once ("components" . DS . "com_joomlawatch" . DS . "lang" . DS . $joomlaWatch->config->getLanguage().".php");

if (JOOMLAWATCH_DEBUG) {
	echo (_JW_DESC_DEBUG);
}

$joomlaWatchHTML = new JoomlaWatchHTML();
$joomlaWatchStatHTML = new JoomlaWatchStatHTML($joomlaWatch);

echo $joomlaWatchStatHTML->renderFrontendUsers();

?>
